<?php  // $Id: chartlib.class.php,v 1.8.3.5 2008/07/07 11:27:50 tasoulis7 Exp $

///////////////////////////////////////////////////////////////////////////
//                                                                       //
// NOTICE OF COPYRIGHT                                                   //
//                                                                       //
// Moodle - Modular Object-Oriented Dynamic Learning Environment         //
//          http://moodle.com                                            //
//                                                                       //
// Copyright (C) 2001-3001 Martin Dougiamas        http://dougiamas.com  //
//           (C) 2001-3001 Anastasios Pournias (tasoulis7)               //
//                                                                       //
// This program is free software; you can redistribute it and/or modify  //
// it under the terms of the GNU General Public License as published by  //
// the Free Software Foundation; either version 2 of the License, or     //
// (at your option) any later version.                                   //
//                                                                       //
// This program is distributed in the hope that it will be useful,       //
// but WITHOUT ANY WARRANTY; without even the implied warranty of        //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         //
// GNU General Public License for more details:                          //
//                                                                       //
//          http://www.gnu.org/copyleft/gpl.html                         //
//                                                                       //
///////////////////////////////////////////////////////////////////////////

/// We need to add this to allow "our" PEAR package to work smoothly
/// without modifying one bit, putting it in the 1st place of the
/// include_path to be localised by Moodle without problems
ini_set('include_path', $CFG->libdir.'/pear' . PATH_SEPARATOR . ini_get('include_path'));

require_once 'Image/Graph.php';

/**
* Default class for graph charts.
* 
* This class acts as a wrapper over the PEAR Image_Graph and OLE libraries
* maintaining Moodle functions isolated from underlying code.
*/
class MoodleImageGraph {

    var $pear_image_graph;

    /* Constructs one Moodle Image Graph object.
     */
    function MoodleImageGraph() {
        $this->pear_image_graph = null;   
    }

    /* Constructs one Moodle Graph.
     * @param mixed $params paramaters to pass to the constructor
     */
    function &new_graph($params = null) {
        // Internally, create one PEAR Image_Graph class
        $this->pear_image_graph =& Image_Graph::factory('graph', $params);
        return $this->pear_image_graph;   
    }
    
    /* Set graph's font
     * @param string $type The font type
     * @param integer $size The font size
     * @return mixed font object
     */
    function &set_font(&$graph, $type, $size) {
        // add a TrueType font
        $font =& $graph->addNew('font', $type);     
        // set the font size
        $font->setSize($size);
        return $font;
    }
    
    /* Add font to graph
     * @param mixed $graph The graph object
     * @param mixed $font The font object
     */
    function add_font(&$graph, &$font) {
        $graph->setFont($font);
    }

    /* Set graph's background color
     * @param mixed $graph The graph object
     * @param mixed $color The background color for the graph
     */
    function set_bgcolor(&$graph, $color) {
        $graph->setBackgroundColor($color); 
    }
    
    /* Get plotarea
     * @return mixed graph's plotarea 
     */
    function get_plotarea() {
        $plotarea = Image_Graph::factory('plotarea');
        return $plotarea;
    }
    
    /* Get legend
     * @return mixed graph's legend
     */
    function get_legend() {
        $legend = Image_Graph::factory('legend');
        return $legend;
    }
    
    /* Set graph's plotareas.This function creates the specific plotareas needed by quiz report analysis plugin
     * @param mixed $graph The graph object
     * @param string $title1 The title for the 1st plotarea
     * @param string $title2 The title for the 2nd plotarea
     * @param integer $tfont1 The font size for the 1st title
     * @param integer $tfont2 The font size for the 2nd title
     * @param integer $tsize1 The size for the 1st title
     * @param integer $tsize2 The size for the 2nd title
     * @param integer $psize1 The size for the 1st plotarea
     * @param integer $psize2 The size for the 2nd plotarea
     * @param integer $split The percent that the 2 plotareas are splitted in the graph
     * @param mixed $parea1 The first plotarea
     * @param mixed $parea2 The second plotarea
     * @param mixed $legend1 The 1st legend
     * @param mixed $legend2 The 2nd legend
     */
    function set_plotarea(&$graph, $title1, $title2, $tfont1, $tfont2, $tsize1, $tsize2, $psize1, $psize2, $split, &$parea1, &$parea2, &$legend1, &$legend2) {
        $graph->add(
            Image_Graph::horizontal(
                Image_Graph::vertical(
                    Image_Graph::factory('title',array($title1, $tfont1)),
                    Image_Graph::vertical($parea1, $legend1, $psize1),
                    $tsize1
                ),          
            Image_Graph::vertical(
                Image_Graph::factory('title',array($title2, $tfont2)),
                    Image_Graph::vertical($parea2, $legend2, $psize2),
                    $tsize2 
                ),
                $split                  
            )
        );
    }
    
	function set_plotarea_single(&$graph, $title1, $tfont1, $tsize1, $psize1, $split, &$parea1, &$legend1) {
        $graph->add(
            Image_Graph::horizontal(

                    Image_Graph::factory('title',array($title1, $tfont1)),
                    Image_Graph::vertical($parea1, $legend1, $psize1),
                    $tsize1
              
            )
        );
    }
    /* Set plotarea for legend
     * @param mixed $plotarea The plotarea object
     * @param mixed $legend The legend object
     */
    function set_legend_plotarea(&$plotarea, &$legend) {
        $legend->setPlotarea($plotarea);
    }
    
    /* Get axisX for a specific plotarea
     * @param mixed $plotarea The plotarea
     * @param mixed $option option for label
     * @return mixed plotarea's X-axis
     */
    function &get_axisX(&$plotarea, $option) {
        $axisx =& $plotarea->getAxis(IMAGE_GRAPH_AXIS_X);
        $axisx->setLabelOption('showtext',$option);
        return $axisx;
    }
    
    /* Get axisY for a specific plotarea
     * @param mixed $plotarea The plotarea
     * @param integer $max The maximum value for axisY
     * @param integer $interval The label interval for axisY
     * @return mixed plotarea's Y-axis
     */
    function &get_axisY(&$plotarea, $max, $interval) {
        $axisy =& $plotarea->getAxis(IMAGE_GRAPH_AXIS_Y);
        $axisy->_setmaximum($max);
        $axisy->setLabelInterval($interval);
        return $axisy;
    }
    
    /* Create a new dataset for bar graphs
     * @param string $label The label for the dataset
     * @param string $name The name for the dataset
     * @param mixed $data The data needed
     * @return mixed the needeed dataset for the bar graph
     */
    function create_bardataset($label, $name, $data) {
        $dataset[$label] = Image_Graph::factory('dataset', array(&$data));
        $dataset[$label]->setName($name);
        return $dataset;   
    }
    
    /* Create a new dataset for pie graphs
     * @return mixed the needeed dataset for the pie graph
     */
    function &create_piedataset() {
        $dataset =& Image_Graph::factory('dataset');
        return $dataset;   
    }
    /* Adds data to a dataset for bar graphs
     * @param string $label The label for the dataset
     * @param string $name The name for the dataset
     * @param mixed $data The data needed
     * @param mixed $dataset The dataset needed
     */
    function set_bardataset($label, $name, $data, &$dataset) {
        $dataset[$label] = Image_Graph::factory('dataset', array(&$data));
        $dataset[$label]->setName($name);  
    }
    
     /* Adds data to a dataset for pie graphs
     * @param string $label The label for the dataset
     * @param string $name The name for the dataset
     * @param mixed $data The data needed
     * @param mixed $dataset The dataset needed
     */
    function set_piedataset($name, $data, $label, &$dataset) {
        if (!empty($data)) {
            $dataset->addPoint($name, $data, $label);   
        }
    }
    
    /* Add a new bar graph plot
     * @param mixed $plotarea The plotarea 
     * @param mixed $type The type of the chart
     * @param mixed $dataset The dataset needed for the plot
     * @return mixed $plot The plot for bar graph
     */
    function &add_barplot(&$plotarea, $type, $dataset) {
        // create the 1st plot as smoothed area chart using the 1st dataset
        if (!empty($type)) {
            $plot =& $plotarea->addNew('bar', array($dataset, $type));    
        } else {
            $plot =& $plotarea->addNew('bar', array($dataset));
        }       
        return $plot;
    }
    
    /* Add a new pie graph plot
     * @param mixed $plotarea The plotarea
     * @param mixed $dataset The dataset needed for the plot
     * @return mixed $plot The plot for pie graph
     */
    function &add_pieplot(&$plotarea, &$dataset) {
        // create the 1st plot as smoothed area chart using the 1st dataset
        $plot =& $plotarea->addNew('pie', array(&$dataset));
        return $plot;
    }
    
    /* Create a fill array for the graph
     * @return mixed $fillarray The specific fillarray
     */
    function &create_farray() {
        $fillarray =& Image_Graph::factory('Image_Graph_Fill_Array');
        return $fillarray;
    }
    
    /* Adds color to a specific fillarray
     * @param mixed $fillarray The fillarray needed
     * @param string $color The color
     * @param string $label The label
     */
    function add_color(&$fillarray, $color, $label) {
        $fillarray->addColor($color, $label);
    }
    
    /* Sets line color to a plot
     * @param mixed $plot The plot
     * @param string $color The color
     */
    function set_line_color(&$plot, $color) {
        $plot->setLineColor($color);
    }
    
    /* sets a standard fill style to a plot
     * @param mixed $plot The plot
     * @param mixed $fillarray The fillarray
     */
    function set_fill_style(&$plot, &$fillarray) {
         $plot->setFillStyle($fillarray);
    }

    /* Creates a Y-axis data value marker for bar graphs
     * @param mixed $plot The plot needed
     * @param integer $x parameter needed for axis-X
     * @param integer $y parameter needed for axis-Y
     * @return mixed $pointingmarker The specific pointing marker
     */
    function &create_barmarker(&$plot, $x, $y) {
        $marker =& $plot->addNew('Image_Graph_Marker_Value', IMAGE_GRAPH_VALUE_Y);
        // format value marker labels as percentage values
        $marker->setDataPreprocessor(Image_Graph::factory('Image_Graph_DataPreprocessor_Formatted', '%0.2f%%'));
		// create a pin-point marker type
        $pointingmarker =& $plot->addNew('Image_Graph_Marker_Pointing', array($x, $y, &$marker));
        return $pointingmarker;
    }
    
    /* Creates a Y-axis data value marker for pie graphs
     * @param mixed $plot The plot
     * @param integer $value parameter needed for pie graph
     * @return mixed $pointingmarker The specific pointing marker
     */
    function &create_piemarker(&$plot ,$value) {
        $marker =& $plot->addNew('Image_Graph_Marker_Value', IMAGE_GRAPH_PCT_Y_TOTAL);
        // format value marker labels as percentage values
        $marker->setDataPreprocessor(Image_Graph::factory('Image_Graph_DataPreprocessor_Formatted', '%0.2f%%'));
        // create a pin-point marker type
        $pointingmarker =& $plot->addNew('Image_Graph_Marker_Pointing_Angular', array($value, &$marker));
        return $pointingmarker;
    }
    
    /* Use the marker on the plot
     * @param mixed $plot The plot needed
     * @param mixed $marker The data value marker
     * @param string $font The marker's font size
     */
    function set_marker(&$plot, &$marker, $font) {
        $plot->setMarker($marker);
		$marker->setFontSize($font);
    }
    
    /* Selects specific data
     * @param mixed $plot The plot needed
     */
    function set_data_selector(&$plot) {
        // Do not show zero values on the graph
        $plot->setDataSelector(Image_Graph::factory('Image_Graph_DataSelector_NoZeros'));
    }
    
    /* Set the radius for the graph
     * @param mixed $plotarea The plotarea needed
     * @param integer $value the specific radius
     */
    function set_radius(&$plotarea, $value) {
        $plotarea->Radius = $value;
    }
     
    /* Hide Axis for a specific plotarea
     * @param mixed $plotarea The plotarea needed
     */
    function hide_axis(&$plotarea) {
        $plotarea->hideAxis();
    }
    
    /* Set various options for a specific plotarea
     * @param mixed $plotarea The plotarea needed
     * @param integer $optiona The 1st option
     * @param integer $optionb The 2nd option
     */
    function set_plotoptions(&$plotarea, $optiona, $optionb) {
        $plotarea->explode($optiona);
        $plotarea->setStartingAngle($optionb);
    }
       
    /* Output the graph
     * @param mixed $graph The graph object
     * @param string $filename The filename to save the graph
     */
    function draw(&$graph, $filename) {
        $graph->done(array('filename' => $filename)); 
    }
}
?>
