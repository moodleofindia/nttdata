<?php 		 
require_once("../config.php");
require_once($CFG->dirroot .'/course/lib.php');
  if ($CFG->forcelogin) {
        require_login();
    } else {
        user_accesstime_log();
    } 

$id= optional_param('id',0, PARAM_INT);
$userid=$USER->id;
$enrolled=get_records_sql("select * from mdl_enrol_skillsoft where userid=$userid and status in('2')");

if (empty($id)) {

    $id=0;
} 

  print_header($SITE->fullname, $SITE->fullname, 'home', '',
                 '<meta name="description" content="'. strip_tags(format_text($SITE->summary, FORMAT_HTML)) .'" />',
                 true, '', user_login_string($SITE).$langmenu);


?>
<head>
<script type="text/javascript" src="jquery.idTabs.min.js"></script> 
<style type="text/css">
.myButton {
	-moz-box-shadow:inset 0px 1px 3px 0px #f0f5f4;
	-webkit-box-shadow:inset 0px 1px 3px 0px #f0f5f4;
	box-shadow:inset 0px 1px 3px 0px #f0f5f4;
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #C0C0C0 ), color-stop(1, #C0C0C0 ));
	background:-moz-linear-gradient(top, #C0C0C0  5%, #C0C0C0  100%);
	background:-webkit-linear-gradient(top, #C0C0C0  5%, #C0C0C0  100%);
	background:-o-linear-gradient(top, #C0C0C0  5%, #C0C0C0  100%);
	background:-ms-linear-gradient(top, #C0C0C0  5%, #C0C0C0  100%);
	background:linear-gradient(to bottom, #C0C0C0  5%, #C0C0C0  100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#C0C0C0 ', endColorstr='#C0C0C0 ',GradientType=0);
	background-color:#C0C0C0 ;
	-moz-border-radius:5px;
	-webkit-border-radius:5px;	
	border-radius:5px;
	border:1px solid #566963;
	display:inline-block;
	cursor:pointer;
	color:#ffffff;
	font-family:Arial;
	font-size:15px;
	font-weight:bold;
	padding:11px 23px;
	text-decoration:none;
	text-shadow:0px -1px 0px #2b665e;
}
.myButton:hover {
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #C0C0C0 ), color-stop(1, #C0C0C0 ));
	background:-moz-linear-gradient(top, #C0C0C0  5%, #C0C0C0  100%);
	background:-webkit-linear-gradient(top, #C0C0C0  5%, #C0C0C0  100%);
	background:-o-linear-gradient(top, #C0C0C0  5%, #C0C0C0  100%);
	background:-ms-linear-gradient(top, #C0C0C0  5%, #C0C0C0  100%);
	background:linear-gradient(to bottom, #C0C0C0  5%, #C0C0C0  100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#C0C0C0 ', endColorstr='#C0C0C0 ',GradientType=0);
	background-color:#C0C0C0 ;
}
.myButton:active {
	position:relative;
	top:1px;
}

.myButton1 {
	-moz-box-shadow:inset 0px 1px 3px 0px #f0f7fa;
	-webkit-box-shadow:inset 0px 1px 3px 0px #f0f7fa;
	box-shadow:inset 0px 1px 3px 0px #f0f7fa;
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #C0C0C0 ), color-stop(1, #C0C0C0 ));
	background:-moz-linear-gradient(top, #C0C0C0  5%, #C0C0C0  100%);
	background:-webkit-linear-gradient(top, #C0C0C0  5%, #C0C0C0  100%);
	background:-o-linear-gradient(top, #C0C0C0  5%, #C0C0C0  100%);
	background:-ms-linear-gradient(top, #C0C0C0  5%, #C0C0C0  100%);
	background:linear-gradient(to bottom, #C0C0C0  5%, #C0C0C0  100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#C0C0C0 ', endColorstr='#C0C0C0 ',GradientType=0);
	background-color:green;
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border-radius:5px;
	border:1px solid #057fd0;
	display:inline-block;
	cursor:pointer;
	color:#ffffff;
	font-family:Arial;
	font-size:15px;
	font-weight:bold;
	padding:11px 23px;
	text-decoration:none;
	text-shadow:0px -1px 0px #5b6178;
}
.myButton1:hover {
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #C0C0C0 ), color-stop(1, #C0C0C0 ));
	background:-moz-linear-gradient(top, #C0C0C0  5%, #C0C0C0  100%);
	background:-webkit-linear-gradient(top, #C0C0C0  5%, #C0C0C0  100%);
	background:-o-linear-gradient(top, #C0C0C0  5%, #C0C0C0  100%);
	background:-ms-linear-gradient(top, #C0C0C0  5%, #C0C0C0  100%);
	background:linear-gradient(to bottom, #C0C0C0  5%, #C0C0C0  100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#C0C0C0 ', endColorstr='#C0C0C0 ',GradientType=0);
	background-color:#C0C0C0 ;
}
.myButton1:active {
	position:relative;
	top:1px;
}



</style>

</head>

	<body>
	
			<table   width="80%" align="center"  cellpadding="1" cellspacing="1" style="margin-top:10px; background-color:#F8F8F8 ;">
				
				<tr >
					<td >
						<p>
							<font size="2.5">Skillsoft is our largest external training provider and the course offerings from Skillsoft are integrated into our LMS, allowing users to take the courses without logging into another system.
											You must have a Skillport license in order to complete the training here. Please visit the <a href="https://groups.portal.nttdatainc.com/support/hr/sitemap/training/Pages/Skillport.aspx">SkillPort page on the Global Training Portal</a> for more information about the Skillsoft program.
											Visit the <a href="<?php echo $CFG->wwwroot; ?>/file.php/1/faq.htm">FAQ section here in the LMS </a>for more information about accessing Skillsoft courses. Answers to specific questions about what is contained in Skillport are available here in the<a href="https://groups.portal.nttdatainc.com/support/hr/sitemap/training/Pages/Skillport.aspx"> FAQ posted on the Skillport portal page</a>.
							</font>
						</p>
					</td>			
				</tr>
			</table>

			</br>
			<?php
				if($enrolled){
			?>
			<table   align="center"  cellpadding="1" cellspacing="1" style="margin-top:10px; background-color:#F8F8F8;">
				<tr >
					<td >
						<p>You have access for all SkillSoft course modules. Clink on the below link to login to SkillPort using your Americas\portalid and password.</p>
					</td>
				</tr>
			</table>
			</br>
				
			<table   align="center"  cellpadding="1" cellspacing="1" >
				<tr >
					<td >
						<a href="https://nttdata-americas.skillport.com/skillportfe/custom/login/nttdata-americas/login.action" class="myButton1" target="_blank">Click to Login</a>
					</td>
				</tr>
			</table>
			</br>
			<?php
				} else {
			?>
			
			<table   align="center"  cellpadding="1" cellspacing="1" style="margin-top:10px; background-color:#F8F8F8;">
				<tr >
					<td >
						<p>Currently you require a licence to access the SkillSoft course. Click on the below button to request for a SkillSoft licence.</p>
					</td>
				</tr>
			</table>
			</br>
			
			<table   align="center"  cellpadding="1" cellspacing="1" style="margin-top:10px;">
				<tr >
					<td >
						<a href="/course/enrol.php?id=3818" class="myButton" target="_blank">Request access</a>
					</td>
				</tr>
			</table>
			
			<?php
				}
			?>
							
							
							
<?php
print_footer('home');     // Please do not modify this line
?>

</body>
</html>
									