<?php

require_once '../../config.php';
require_login();
require_once 'durationlib.php';
require_once 'completionlib.php';

$sortby = optional_param('sortby', 'timestart', PARAM_ALPHA); // column to sort by
$option = required_param('option', PARAM_INT); 
$sd = optional_param('startdate',  PARAM_INT);
$ed =  optional_param('enddate', PARAM_RAW);

$userid=$USER->id;
$yearlabel="";
$elearningcount=0;
/* Course completion year wise */
$coursecompletedcurrent=getcompletionDate($userid,'2016/4/1','2017/4/1');
$coursecompletedprevious=getcompletionDate($userid,'2015/4/1','2016/4/1');
$coursecompletedprevious2=getcompletionDate($userid,'2014/4/1','2015/4/1');
/* End of course completion count */

if($option==6)
{
$sd='2011/4/1';
$ed ='2012/3/31';
$yearlabel="April 2011 to Mar 2012";
}
if($option==5)
{
$sd='2012/4/1';
$ed ='2013/4/1';
$yearlabel="from April 2012 to Mar 2013";
}
if($option==4)
{
$sd='2013/4/1';
$ed ='2014/4/1';
$yearlabel="from April 2013 to Mar 2014";
}
if($option==3)
{
$sd='2014/4/1';
$ed ='2015/4/1';
$yearlabel="from April 2014 to Mar 2015";
}
if($option==2)
{
$sd='2015/4/1';
$ed ='2016/4/1';
$yearlabel="from April 2015 to Mar 2016";
}
if($option==1)
{
$sd='2016/4/1';
$ed ='2017/4/1';
$yearlabel="from April 2016 to Mar 2017";
}
if($option==0)
{
$sd='2000/4/1';
$ed ='2020/3/31';
$yearlabel="for All dates";
}
function date_compare($a, $b)
{
    $t1 = strtotime($a['datetime']);
    $t2 = strtotime($b['datetime']);
    return $t1 - $t2;
}

$pagetitle = 'My Trainings';
$navlinks[] = array('name' => $pagetitle, 'link' => '', 'type' => 'activityinstance');
$navigation = build_navigation($navlinks);
print_header_simple($pagetitle, '', $navigation);

?>
<script type="text/javascript" src="jquery.idTabs.min.js"></script> 
<style type="text/css">


.usual li { list-style:none; float:left; }
.usual ul a {
  display:block;
  padding:6px 10px;
  text-decoration:none!important;
  margin:1px;
  margin-left:0;
  font:10px Verdana;
  color:#FFF;
  background:#444;
}
.usual ul a:hover {
  color:#0000;
  background:#9999FF;
  }
.usual ul a.selected {
  margin-bottom:0;
  color:#ffff;
  background:#6666FF;
  border-bottom:1px solid snow;
  cursor:default;
  }
.usual div {
  padding:10px 10px 8px 10px;
  *padding-top:3px;
  *margin-top:-15px;
  clear:left;
  font:10pt Georgia;
}
.usual div a { color:#000; font-weight:bold; }

#usual2 { background:#0A0A0A; border:1px solid #1A1A1A; }
#usual2 a { background:#222; }
#usual2 a:hover { background:#000; }
#usual2 a.selected { background:snow; }
#tabs3 { background:#FF9; }

.tabled {
	font-family:Arial, Helvetica, sans-serif;
	font-size:10px;

}

.tabled tr td {

}



#box-table-b
{
	font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
	font-size: 12px;
	margin: 20px;
	text-align: center;
	border-collapse: collapse;
	border-top: 8px solid #9baff1;
	border-bottom: 8px solid #9baff1;
}
#box-table-b th
{
	font-size: 13px;
	font-weight: normal;
	padding: 8px;
	background: #D1DEE6;
	border-right: 1px solid #9baff1;
	border-left: 1px solid #9baff1;
	color: #039;
}
#box-table-b td
{
	padding: 8px;
	background: #F0F3F5; 
	border-right: 1px solid #aabcfe;
	border-left: 1px solid #aabcfe;
	color: #669;
}


.tabler a:link {
	color: #666;
	font-weight: bold;
	text-decoration:none;
}
.tabler a:visited {
	color: #999999;
	font-weight:bold;
	text-decoration:none;
}
.tabler a:active,
.tabler a:hover {
	color: #bd5a35;
	text-decoration:underline;
}
.tabler {
	font-family:Arial, Helvetica, sans-serif;
	color:#666;
	font-size:11px;
	text-shadow: 1px 1px 0px #fff;
	background:#eaebec;
	margin:20px;
	border:#ccc 1px solid;

	-moz-border-radius:3px;
	-webkit-border-radius:3px;
	border-radius:3px;

	-moz-box-shadow: 0 1px 2px #d1d1d1;
	-webkit-box-shadow: 0 1px 2px #d1d1d1;
	box-shadow: 0 1px 2px #d1d1d1;
}
.tabler th {
	padding:21px 25px 22px 25px;
	border-top:1px solid #fafafa;
	border-bottom:1px solid #e0e0e0;

	background: #ededed;
	background: -webkit-gradient(linear, left top, left bottom, from(#ededed), to(#ebebeb));
	background: -moz-linear-gradient(top,  #ededed,  #ebebeb);
}
.tabler tfoot {
	padding:21px 25px 22px 25px;
	border-top:1px solid #fafafa;
	border-bottom:1px solid #e0e0e0;

	background: #ededed;
	background: -webkit-gradient(linear, left top, left bottom, from(#ededed), to(#ebebeb));
	background: -moz-linear-gradient(top,  #ededed,  #ebebeb);
}
.tabler th:first-child{
	text-align: left;
	padding-left:20px;
}
.tabler tr:first-child th:first-child{
	-moz-border-radius-topleft:3px;
	-webkit-border-top-left-radius:3px;
	border-top-left-radius:3px;
}
.tabler tr:first-child th:last-child{
	-moz-border-radius-topright:3px;
	-webkit-border-top-right-radius:3px;
	border-top-right-radius:3px;
}
.tabler tr{
	text-align: center;
	padding-left:20px;
}
.tabler tr td:first-child{
	text-align: left;
	padding-left:20px;
	border-left: 0;
}
.tabler tr td {
	padding:14px;
	border-top: 1px solid #ffffff;
	border-bottom:1px solid #e0e0e0;
	border-left: 1px solid #e0e0e0;
	
	background: #fafafa;
	background: -webkit-gradient(linear, left top, left bottom, from(#fbfbfb), to(#fafafa));
	background: -moz-linear-gradient(top,  #fbfbfb,  #fafafa);
}
.tabler tr.even td{
	background: #f6f6f6;
	background: -webkit-gradient(linear, left top, left bottom, from(#f8f8f8), to(#f6f6f6));
	background: -moz-linear-gradient(top,  #f8f8f8,  #f6f6f6);
}
.tabler tr:last-child td{
	border-bottom:0;
}
.tabler tr:last-child td:first-child{
	-moz-border-radius-bottomleft:3px;
	-webkit-border-bottom-left-radius:3px;
	border-bottom-left-radius:3px;
}
.tabler tr:last-child td:last-child{
	-moz-border-radius-bottomright:3px;
	-webkit-border-bottom-right-radius:3px;
	border-bottom-right-radius:3px;
}
.tabler tr:hover td{
	background: #f2f2f2;
	background: -webkit-gradient(linear, left top, left bottom, from(#f2f2f2), to(#f0f0f0));
	background: -moz-linear-gradient(top,  #f2f2f2,  #f0f0f0);	
}


#rounded-corner
{
	font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
	font-size: 12px;
	margin: 45px;
	width: 250px;
	text-align: left;
	border-collapse: collapse;
}
#rounded-corner thead th.rounded-company
{
	background: #b9c9fe url('table-images/left.png') left -1px no-repeat;
}
#rounded-corner thead th.rounded-q4
{
	background: #b9c9fe url('table-images/right.png') right -1px no-repeat;
}
#rounded-corner th
{
	padding: 8px;
	font-weight: normal;
	font-size: 13px;
	color: #039;
	background: #b9c9fe;
}
#rounded-corner td
{
	padding: 8px;
	background: #e8edff;
	border-top: 1px solid #fff;
	color: #669;
}
#rounded-corner tfoot td.rounded-foot-left
{
	background: #e8edff url('table-images/botleft.png') left bottom no-repeat;
}
#rounded-corner tfoot td.rounded-foot-right
{
	background: #e8edff url('table-images/botright.png') right bottom no-repeat;
}
#rounded-corner tbody tr:hover td
{
	background: #d0dafd;
}
</style>
<?php

$countCompleted=0;
$countInprogress=0;
$countEnrolled=0;
$manditorytraining=0;
/* Points for Gamification
$assignmentpoints = get_record_sql("SELECT points from mdl_gamification_points where type='assignment' and action='complete'");
$quizpoints = get_record_sql("SELECT points from mdl_gamification_points where type='quiz' and action='complete'");
$resourcepoints = get_record_sql("SELECT points from mdl_gamification_points where type='resource' and action='complete'");
$scormpoints = get_record_sql("SELECT points from mdl_gamification_points where type='scorm' and action='complete'");
$classroompoints = get_record_sql("SELECT points from mdl_gamification_points where type='classroom' and action='complete'");
$skillsoftpoints = get_record_sql("SELECT points from mdl_gamification_points where type='skillsoft' and action='complete'");
$mplayerpoints = get_record_sql("SELECT points from mdl_gamification_points where type='mplayer' and action='complete'");
*/
$manditorytrainingtemp = get_record_sql("SELECT mandatorytrainings from mdl_user where id=$USER->id");

if($manditorytrainingtemp->mandatorytrainings>0)
{
$manditorytraining=$manditorytrainingtemp->mandatorytrainings;
}
else
{
$manditorytraining=0;
}

$externaltrainingcount = get_record_sql("SELECT count(s.programename) as count
from mdl_classroom_sessions s
join mdl_classroom_submissions on mdl_classroom_submissions.sessionid = s.id
and s.datetimeknown=1 and s.classroom=1284
and mdl_classroom_submissions.attend=1 
join mdl_user u on u.id=mdl_classroom_submissions.userid and u.id=$userid"); 


	$recordsCompleted = get_records_sql("SELECT c.id as id,c.fullname AS 'Course',cc.name as 'Category',
(SELECT FROM_UNIXTIME(max(mdl_grade_grades.timemodified),'%b %d %Y')
from mdl_grade_grades JOIN mdl_grade_items
ON mdl_grade_items.id = mdl_grade_grades.itemid
where mdl_grade_items.courseid = c.id and mdl_grade_grades.userid=u.id
and mdl_grade_grades.timemodified >= unix_timestamp('$sd')
and mdl_grade_grades.timemodified < unix_timestamp('$ed')
)as 'CompletedDate'
FROM mdl_course AS c JOIN mdl_context AS ctx
ON c.id = ctx.instanceid   JOIN mdl_role_assignments AS ra
ON ra.contextid = ctx.id JOIN mdl_user AS u
ON u.id = ra.userid JOIN mdl_grade_grades AS gg
ON gg.userid = u.id JOIN mdl_grade_items AS gi
ON gi.id = gg.itemid and gg.finalgrade >= gi.grademax
JOIN mdl_course_categories AS cc ON cc.id = c.category and cc.path not like('/49%') and cc.path not like('/154%')
WHERE  gi.courseid = c.id and u.id=$userid AND gi.itemtype = 'course' order by gg.id desc");

$recordsInprogress = get_records_sql("SELECT c.id as id,c.fullname AS 'Course',cc.name as 'Category'
FROM mdl_course AS c JOIN mdl_context AS ctx
ON c.id = ctx.instanceid and c.visible=1 JOIN mdl_role_assignments AS ra
ON ra.contextid = ctx.id JOIN mdl_user AS u
ON u.id = ra.userid JOIN mdl_course_categories AS cc ON cc.id = c.category and cc.path not like('/49%') and cc.path not like('/154%')
WHERE  u.id=$userid and c.id not in(SELECT c.id
		FROM mdl_course AS c JOIN mdl_context AS ctx
		ON c.id = ctx.instanceid JOIN mdl_role_assignments AS ra
		ON ra.contextid = ctx.id JOIN mdl_user AS u
		ON u.id = ra.userid JOIN mdl_grade_grades AS gg
		ON gg.userid = u.id JOIN mdl_grade_items AS gi
		ON gi.id = gg.itemid and gg.finalgrade >= gi.grademax
		WHERE  gi.courseid = c.id AND gi.itemtype = 'course' and u.id=$userid)");

$recordsILTCompleted = get_records_sql("SELECT mdl_classroom_sessions_dates.id,
s.programename as 'Course',
truncate(s.duration/60,1) as 'Duration',
'Completed' as 'Grade',
s.sessioncategory as Category,
FROM_UNIXTIME(max(mdl_classroom_sessions_dates.timefinish),'%b %d %Y') as 'CompletedDate'
from mdl_classroom_sessions s
join mdl_classroom_sessions_dates on s.id=mdl_classroom_sessions_dates.sessionid
and s.datetimeknown=1 and s.status='Completed'
and mdl_classroom_sessions_dates.timestart >= unix_timestamp('$sd')
and mdl_classroom_sessions_dates.timefinish < unix_timestamp('$ed')
join mdl_classroom on mdl_classroom.id=s.classroom
join mdl_classroom_submissions on mdl_classroom_submissions.sessionid = s.id
and mdl_classroom_submissions.attend=1
join mdl_course c on c.id=mdl_classroom.course
JOIN mdl_course_categories AS cc ON cc.id = c.category and cc.path like('/154%')
join mdl_user u on u.id=mdl_classroom_submissions.userid and u.id=$userid
group by s.id,u.id ORDER BY mdl_classroom_sessions_dates.timefinish desc;");

$recordsSkillSoft= get_records_sql("SELECT sc.id, u.username as PortalID,
trim(sc.name) as 'Course','Skillsoft' as 'Category',
trim(sc.name) as 'Asset',
sc.duration/60 as 'Duration',
st.value as 'Grade',
DATE_FORMAT(FROM_UNIXTIME(at.value),'%b %d %Y') as 'CompletedDate'
FROM mdl_skillsoft_au_track AS st
JOIN mdl_skillsoft AS sc ON sc.id=st.skillsoftid
JOIN mdl_user AS u ON st.userid=u.id
join mdl_skillsoft_au_track AS at
on at.userid=u.id and at.skillsoftid=sc.id and at.element='[SUMMARY]completed'
and at.value >= unix_timestamp('$sd')
and at.value < unix_timestamp('$ed')
where st.element='[CORE]lesson_status' and st.value in ('completed','passed') and u.id=$userid
group by sc.assetid
order by st.timemodified desc");
//Naga added to display the non matched assetid details

$recordsSkillSoft_nonmatched= get_records_sql("SELECT sc.id, u.username as PortalID,
trim(sc.name) as 'Course','Skillsoft' as 'Category',
trim(sc.name) as 'Asset',
sc.duration/60 as 'Duration',
st.value as 'Grade',
DATE_FORMAT(FROM_UNIXTIME(at.value),'%b %d %Y') as 'CompletedDate'
FROM mdl_skillsoft_au_track_nonmatched AS st
JOIN mdl_skillsoft_nonmatched AS sc ON sc.id=st.skillsoftid
JOIN mdl_user AS u ON st.userid=u.id
join mdl_skillsoft_au_track_nonmatched AS at
on at.userid=u.id and at.skillsoftid=sc.id and at.element='[SUMMARY]completed'
and at.value >= unix_timestamp('$sd')
and at.value < unix_timestamp('$ed')
where st.element='[CORE]lesson_status' and st.value in ('completed','passed') and u.id=$userid
group by sc.assetid
order by st.timemodified desc");
	
$recordsScorm = get_records_sql("SELECT st.id, u.username as PortalID,
trim(c.fullname) as 'Course',
trim(sc.name) as 'Asset',
sc.duration/60 as 'Duration',
st.value as 'Grade',
FROM_UNIXTIME(st.timemodified,'%b %d %Y') as 'CompletedDate'
FROM mdl_scorm_scoes_track AS st
JOIN mdl_scorm_scoes ssc on ssc.id=st.scoid and ssc.scormtype='sco'
JOIN mdl_scorm AS sc ON sc.id=st.scormid
JOIN mdl_course AS c ON c.id=sc.course
JOIN mdl_user AS u ON st.userid=u.id and u.id=$userid
and st.timemodified >= unix_timestamp('$sd')
and st.timemodified < unix_timestamp('$ed')
where st.element in('cmi.core.lesson_status','cmi.completion_status')  and st.value in ('completed','passed') group by st.scormid,u.id order by st.timemodified desc");     //Naga added "group by st.scormid,u.id" to restrict the repeation of courses

$recordsResource = get_records_sql("SELECT g.id, u.username as PortalID,
trim(c.fullname) as 'Course',
trim(r.name) as 'Asset',
r.duration/60 as 'Duration',
'viewed' as 'Grade',
DATE_FORMAT(FROM_UNIXTIME(g.timemodified),'%b %d %Y') as 'CompletedDate'
FROM mdl_grade_grades g join mdl_grade_items i
on  g.itemid=i.id
and g.rawgrade=g.rawgrademax
and i.itemmodule in ('resource')
and g.timemodified >= unix_timestamp('$sd')
and g.timemodified < unix_timestamp('$ed')
join mdl_resource r on r.id=i.iteminstance
JOIN mdl_course AS c ON c.id=r.course
join mdl_user u on u.id=g.userid and u.id=$userid order by g.timemodified desc");

$recordsMplayer = get_records_sql("SELECT m.id,u.username as PortalID,
trim(c.fullname) as 'Course',
trim(m.name) as 'Asset',
m.duration/60 as 'Duration',
'viewed' as 'Grade',
DATE_FORMAT(FROM_UNIXTIME(mdl_grade_grades.timemodified),'%b %d %Y') AS 'CompletedDate'
FROM mdl_grade_grades join mdl_grade_items
on  mdl_grade_grades.itemid=mdl_grade_items.id
and mdl_grade_grades.rawgrade=mdl_grade_grades.rawgrademax
and mdl_grade_items.itemmodule='mplayer'
and mdl_grade_grades.timemodified >= unix_timestamp('$sd')
and mdl_grade_grades.timemodified < unix_timestamp('$ed')
join mdl_mplayer m on m.id=mdl_grade_items.iteminstance
join mdl_user u on u.id=mdl_grade_grades.userid and u.id=$userid
JOIN mdl_course AS c ON c.id=m.course order by mdl_grade_grades.timemodified desc");

$recordsILT = get_records_sql("SELECT mdl_classroom_sessions_dates.id,u.username as PortalID,
trim(c.fullname) as 'Course',
trim(s.programename) as 'Asset',
s.duration/60 as 'Duration',
'attended' as 'Grade',
s.location as 'Location',
FROM_UNIXTIME(max(mdl_classroom_sessions_dates.timefinish),'%b %d %Y') 'CompletedDate'
from mdl_classroom_sessions s
join mdl_classroom_sessions_dates on s.id=mdl_classroom_sessions_dates.sessionid
and s.datetimeknown=1 and s.status='Completed'
and mdl_classroom_sessions_dates.timestart >= unix_timestamp('$sd')
and mdl_classroom_sessions_dates.timefinish < unix_timestamp('$ed')
join mdl_classroom on mdl_classroom.id=s.classroom
join mdl_classroom_submissions on mdl_classroom_submissions.sessionid = s.id
and mdl_classroom_submissions.attend=1
join mdl_course c on c.id=mdl_classroom.course
join mdl_user u on u.id=mdl_classroom_submissions.userid and u.id=$userid
group by s.id,u.id ORDER BY mdl_classroom_sessions_dates.timefinish desc;");



$recordsQuiz= get_records_sql("SELECT u.username as PortalID,
trim(c.fullname) as 'Course',
trim(i.itemname) as 'Asset',
q.timelimit/60 as 'Duration',
ROUND(g.finalgrade, 0) as 'Grade',
DATE_FORMAT(FROM_UNIXTIME(g.timemodified),'%b %d %Y') as 'CompletedDate'
FROM mdl_grade_grades g
INNER JOIN mdl_user u ON g.userid = u.id and u.id=$userid
INNER JOIN mdl_grade_items i
ON g.itemid = i.id
INNER JOIN mdl_course c on c.id = i.courseid
inner join mdl_quiz q on q.course=c.id and q.id=i.iteminstance
WHERE (i.itemname IS NOT NULL)AND (i.itemtype = 'mod')AND
(i.itemmodule = 'quiz')
AND (g.finalgrade >= 0)
and g.timemodified >= unix_timestamp('$sd')
and g.timemodified < unix_timestamp('$ed')
order by g.timemodified desc
");

$recordsAssignment= get_records_sql("SELECT s.id,u.username as PortalID,
trim(c.fullname) as 'Course',
trim(a.name) as 'Asset',
a.duration/60 as 'Duration',
ROUND(s.grade, 0) as 'Grade',
DATE_FORMAT(FROM_UNIXTIME(s.timemodified),'%b %d %Y') as 'CompletedDate'
FROM mdl_assignment_submissions s join mdl_assignment a
on s.assignment=a.id and s.timemodified >0 join mdl_course c
on c.id=a.course join mdl_user u
on u.id=s.userid and u.id=$userid
and s.timemodified >= unix_timestamp('$sd')
and s.timemodified < unix_timestamp('$ed')
order by s.timemodified desc");

$courseCount=0;
$completed = array();
if ($recordsCompleted) {


        foreach($recordsCompleted as $record) {

                $completed[] = $record;
				if($record->CompletedDate)
				{
					$elearningcount ++;
					$courseCount ++;
				}
                continue;

        }

}




if ($recordsILTCompleted) {


        foreach($recordsILTCompleted as $record) {

                $completed[] = $record;
				$courseCount ++;
                continue;

        }

}

$nbcompleted = count($completed);

$skillsoft = array();
if ($recordsSkillSoft) {


        foreach($recordsSkillSoft as $record) {

                $skillsoft[] = $record;
				$completed[] = $record;
                continue;

        }

}
//$nbskillsoft = count($skillsoft);
//naga added for non matched skillsoft courses
$skillsoft_nonmatched = array();
if ($recordsSkillSoft_nonmatched) {


        foreach($recordsSkillSoft_nonmatched as $record) {

                $skillsoft_nonmatched[] = $record;
				$completed[] = $record;
                continue;

        }

}


$nbskillsoft = count($skillsoft)+count($skillsoft_nonmatched);


$inprogress = array();
if ($recordsInprogress) {


        foreach($recordsInprogress as $record) {

                $inprogress[] = $record;
                continue;

        }

}

$scorm = array();
if ($recordsScorm) {


        foreach($recordsScorm as $record) {

                $scorm[] = $record;
                continue;

        }

}
$nbscorm = count($scorm);


$resource = array();
if ($recordsResource) {


        foreach($recordsResource as $record) {

                $resource[] = $record;
                continue;

        }

}
$nbresource = count($resource);

$mplayer = array();
if ($recordsMplayer) {


        foreach($recordsMplayer as $record) {

                $mplayer[] = $record;
                continue;

        }

}
$nbmplayer = count($mplayer);


$ILT = array();
if ($recordsILT) {


        foreach($recordsILT as $record) {

                $ILT[] = $record;
                continue;

        }

}
$nbILT = count($ILT);




$quiz = array();
if ($recordsQuiz) {


        foreach($recordsQuiz as $record) {

                $quiz[] = $record;
                continue;

        }

}
$nbquiz = count($quiz);


$assignment = array();
if ($recordsAssignment) {


        foreach($recordsAssignment as $record) {

                $assignment[] = $record;
                continue;

        }

}
$nbassignment = count($assignment);

 
 /*Gamification Computing the total points and calculations 
$totalpoints=$nbassignment*$assignmentpoints->points+$nbquiz*$quizpoints->points+$nbskillsoft*$skillsoftpoints->points+$nbILT*$classroompoints->points+$nbmplayer*$mplayerpoints->points+$nbresource*$resourcepoints->points+$nbscorm*$scormpoints->points;

$level=get_record_sql("select max(level) as count from mdl_gamification_levels where points <=$totalpoints");
	if(!$level->count)
	{
	$level->count=0;
	}

$nextl=$level->count+1;
$nextlevel=get_record_sql("select points as count from mdl_gamification_levels where level=$nextl");
$missingpoints=$nextlevel->count-$totalpoints;
*/

$empduration=getdurationDate($userid,'2016/4/1','2017/4/1');
$durationpreviousyear= getdurationDate($userid,'2015/4/1','2016/4/1');
$durationpreviousyear2= getdurationDate($userid,'2014/4/1','2015/4/1');
$reqduration=$manditorytraining-$empduration;
$durationcurrentyear= $empduration;


$userduration= $durationcurrentyear;
$starvalue=$userduration*100/$manditorytraining;
$durationto5star=$manditorytraininghours-$durationcurrentyear;
/*end of the calculations */

 function print_completed($completed,$userid) {
    global $sortbylink, $CFG;


    print '<table id="box-table-b" cellspacing="0" width="100%" summary=""><tr>';
    print '<th align="left">Course</th>';
    print '<th align="left">Category</th>';
	print '<th align="center">Completed Date</th>';
	print '<th align="center">Status</th>';
	
    print '</tr>';

	$finalstatus="";
	$progress=0;
	

	
    foreach ($completed as $rec) {


		if($rec->CompletedDate)
		{
        print '<td align="left">'.format_string($rec->Course).'</td>';
        print '<td align="left">'.format_string($rec->Category).'</td>';
		print '<td align="center">'.format_string($rec->CompletedDate).'</td>';
		print '<td align="center"><img src="icon/tick.png" alt="Completed" width="35" height="35"></td>';
		
		}

        print '</tr>';				
    }
		foreach ($ILTCompleted as $rec1) {
		print '<tr>';	

        print '<td align="left">'.format_string($rec1->Course).'</td>';
        print '<td align="left">'.format_string($rec1->Category).'</td>';
		print '<td align="center">'.format_string($rec1->CompletedDate).'</td>';		
		print '<td align="center"><img src="icon/tick.png" alt="Completed" width="35" height="35"></td>';
		

		print '</tr>';		

	}
    print '</table>';
	$countInprogress=$countEnrolled-$countCompleted;
}
 
 
  function print_inprogress($inprogress,$userid) {
    global $sortbylink, $CFG;


    print '<table class="tabler"  cellspacing="0" width="100%" summary=""><tr>';
    print '<th align="left">Course</th>';
    print '<th align="left">Category</th>';
	print '<th align="center">Action</th>';
	
    print '</tr>';

	$finalstatus="";
	$progress=0;
	

	
    foreach ($inprogress as $rec) {

        print '<td align="left">'.format_string($rec->Course).'</td>';
        print '<td align="left">'.format_string($rec->Category).'</td>';
		print '<td align="center"><a href="'.$CFG->wwwroot.'/course/view.php?id='.$rec->id.'" target="new"><img src="icon/warning.png" title="Go to course" width="35" height="35"></a></td>';


        print '</tr>';		
			
    }

    print '</table>';
}
 
 
 
 /**
 * Print the session scorm in a nicely formatted table.
 */
function print_scorm($scorm) {
    global $sortbylink, $CFG;
	$sum=0;

    print '<table class="tabler" cellspacing="0" width="100%" summary=""><tr>';
    print '<th align="left">Course</th>';
    print '<th align="left">Learning Asset</th>';
	print '<th align="center">Duration</th>';
	print '<th align="center">Status</th>';
	print '<th align="center">Completed Date</th>';
    print '</tr>';

    $even = false;
    foreach ($scorm as $rec) {
        if ($even) {
            print '<tr style="background-color: #eeeeee">';
        }
        else {
            print '<tr>';
        }
		$even = !$even;
        print '<td align="left">'.format_string($rec->Course).'</td>';
        print '<td align="left">'.format_string($rec->Asset).'</td>';
		print '<td>'.format_string($rec->Duration).'</td>';
		print '<td>'.format_string($rec->Grade).'</td>';
		print '<td>'.format_string($rec->CompletedDate).'</td>';
        print '</tr>';	
		$sum = $sum+$rec->Duration;		
    }
	print '<tfoot><tr><td colspan="5"><img src="icon/graf.png" width="18" height="18"> You received '.sprintf("%.2f",$sum).' hours completing E-Learning modules.</td></tr></tfoot>';
    print '</table>';
}

function print_resource($resource) {
    global $sortbylink, $CFG;


    print '<table class="tabler" cellspacing="0" width="100%" summary=""><tr>';
    print '<th align="left">Course</th>';
    print '<th align="left">Links and Files</th>';
	print '<th align="center">Duration</th>';
	print '<th align="center">Status</th>';
	print '<th align="center">Completed Date</th>';
    print '</tr>';

    $even = false;
    foreach ($resource as $rec) {
        if ($even) {
            print '<tr style="background-color: #eeeeee">';
        }
        else {
            print '<tr>';
        }
		$even = !$even;
        print '<td align="left">'.format_string($rec->Course).'</td>';
        print '<td align="left">'.format_string($rec->Asset).'</td>';
		print '<td>'.format_string($rec->Duration).'</td>';
		print '<td>'.format_string($rec->Grade).'</td>';
		print '<td>'.format_string($rec->CompletedDate).'</td>';

        print '</tr>';				
    		$sum = $sum+$rec->Duration;		
    }
	print '<tfoot><tr><td colspan="5"><img src="icon/graf.png" width="18" height="18"> You received '.sprintf("%.2f",$sum).' hours visiting links and resources.</td></tr></tfoot>';
    print '</table>';
}

function print_mplayer($mplayer) {
    global $sortbylink, $CFG;


    print '<table class="tabler" cellspacing="0" width="100%" summary=""><tr>';
    print '<th align="left">Course</th>';
    print '<th align="left">Learning Asset</th>';
	print '<th align="center">Duration</th>';
	print '<th align="center">Status/Grade</th>';
	print '<th align="center">Completed Date</th>';
    print '</tr>';

    $even = false;
    foreach ($mplayer as $rec) {
        if ($even) {
            print '<tr style="background-color: #eeeeee">';
        }
        else {
            print '<tr>';
        }
		$even = !$even;
        print '<td align="left">'.format_string($rec->Course).'</td>';
        print '<td align="left">'.format_string($rec->Asset).'</td>';
		print '<td>'.format_string($rec->Duration).'</td>';
		print '<td>'.format_string($rec->Grade).'</td>';
		print '<td>'.format_string($rec->CompletedDate).'</td>';

        print '</tr>';				
    		$sum = $sum+$rec->Duration;		
    }
	print '<tfoot><tr><td colspan="5"><img src="icon/graf.png" width="18" height="18"> You received '.sprintf("%.2f",$sum).' hours viewing media assets.</td></tr></tfoot>';
    print '</table>';
}

function print_ILT($ILT) {
    global $sortbylink, $CFG;


    print '<table class="tabler" cellspacing="0" width="100%" summary=""><tr>';
    print '<th align="left">Course</th>';
    print '<th align="left">Location</th>';
	print '<th align="center">Duration</th>';
	print '<th align="center">Status</th>';
	print '<th align="center">Completed Date</th>';
    print '</tr>';

    $even = false;
    foreach ($ILT as $rec) {
        if ($even) {
            print '<tr style="background-color: #eeeeee">';
        }
        else {
            print '<tr>';
        }
		$even = !$even;
        print '<td align="left">'.format_string($rec->Asset).'</td>';
        print '<td align="left">'.format_string($rec->Location).'</td>';
		print '<td>'.format_string($rec->Duration).'</td>';
		print '<td>'.format_string($rec->Grade).'</td>';
		print '<td>'.format_string($rec->CompletedDate).'</td>';

        print '</tr>';				
    		$sum = $sum+$rec->Duration;		
    }
	print '<tfoot><tr><td colspan="5"><img src="icon/graf.png" width="18" height="18"> You received '.sprintf("%.2f",$sum).' hours attending instructor led and webinar training.</td></tr></tfoot>';
    print '</table>';
}
//Naga Added to print the non-macthed & matched skillsoft courses

function print_SkillSoft($skillsoft,$skillsoft_nonmatched) {
    global $sortbylink, $CFG;


    print '<table class="tabler" cellspacing="0" width="100%" summary=""><tr>';
    print '<th align="left">Course</th>';
    print '<th align="left">SkillSoft Asset</th>';
	print '<th align="center">Duration</th>';
	print '<th align="center">Status/Grade</th>';
	print '<th align="center">Completed Date</th>';
    print '</tr>';

    $even = false;
    foreach ($skillsoft as $rec) {
        if ($even) {
            print '<tr style="background-color: #eeeeee">';
        }
        else {
            print '<tr>';
        }
		$even = !$even;
        print '<td align="left">'.format_string($rec->Course).'</td>';
        print '<td align="left">'.format_string($rec->Asset).'</td>';
		print '<td>'.format_string($rec->Duration).'</td>';
		print '<td>'.format_string($rec->Grade).'</td>';
		print '<td>'.format_string($rec->CompletedDate).'</td>';

        print '</tr>';				
    		$sum1 = $sum1+$rec->Duration;		
    }
	  foreach ($skillsoft_nonmatched as $rec) {
        if ($even) {
            print '<tr style="background-color: #eeeeee">';
        }
        else {
            print '<tr>';
        }
		$even = !$even;
        print '<td align="left">'.format_string($rec->Asset).'</td>';
        print '<td align="left">'.format_string($rec->Asset).'</td>';
		print '<td>'.format_string($rec->Duration).'</td>';
		print '<td>'.format_string($rec->Grade).'</td>';
		print '<td>'.format_string($rec->CompletedDate).'</td>';

        print '</tr>';				
    		$sum = $sum+$rec->Duration;		
    }
	print '<tfoot><tr><td colspan="5"><img src="icon/graf.png" width="18" height="18"> You received '.sprintf("%.2f",$sum+$sum1).' hours completing Skill Soft modules.</td></tr></tfoot>';
    print '</table>';
}

// function print_SkillSoft($skillsoft) {
    // global $sortbylink, $CFG;


    // print '<table class="tabler" cellspacing="0" width="100%" summary=""><tr>';
    // print '<th align="left">Course</th>';
    // print '<th align="left">SkillSoft Asset</th>';
	// print '<th align="center">Duration</th>';
	// print '<th align="center">Status/Grade</th>';
	// print '<th align="center">Completed Date</th>';
    // print '</tr>';

    // $even = false;
    // foreach ($skillsoft as $rec) {
        // if ($even) {
            // print '<tr style="background-color: #eeeeee">';
        // }
        // else {
            // print '<tr>';
        // }
		// $even = !$even;
        // print '<td align="left">'.format_string($rec->Course).'</td>';
        // print '<td align="left">'.format_string($rec->Asset).'</td>';
		// print '<td>'.format_string($rec->Duration).'</td>';
		// print '<td>'.format_string($rec->Grade).'</td>';
		// print '<td>'.format_string($rec->CompletedDate).'</td>';

        // print '</tr>';				
    		// $sum = $sum+$rec->Duration;		
    // }
	// print '<tfoot><tr><td colspan="5"><img src="icon/graf.png" width="18" height="18"> You received '.sprintf("%.2f",$sum).' hours completing Skill Soft modules.</td></tr></tfoot>';
    // print '</table>';
// }

function print_quiz($quiz) {
    global $sortbylink, $CFG;


    print '<table class="tabler" cellspacing="0" width="100%" summary=""><tr>';
    print '<th align="left">Course</th>';
    print '<th align="left">Quiz</th>';
	print '<th align="center">Duration</th>';
	//print '<th align="center">Grade</th>';
	print '<th align="center">Attempted Date</th>';
    print '</tr>';

    $even = false;
    foreach ($quiz as $rec) {
        if ($even) {
            print '<tr style="background-color: #eeeeee">';
        }
        else {
            print '<tr>';
        }
		$even = !$even;
        print '<td align="left">'.format_string($rec->Course).'</td>';
        print '<td align="left">'.format_string($rec->Asset).'</td>';
		print '<td>'.format_string($rec->Duration).'</td>';
		//print '<td>'.format_string($rec->Grade).'</td>';
		print '<td>'.format_string($rec->CompletedDate).'</td>';

        print '</tr>';				
    		$sum = $sum+$rec->Duration;		
    }
	print '<tfoot><tr><td colspan="5"><img src="icon/graf.png" width="18" height="18"> You received '.sprintf("%.2f",$sum).' hours taking up online assesments.</td></tr></tfoot>';
    print '</table>';
}

function print_assignment($assignment) {

    global $sortbylink, $CFG;


    print '<table class="tabler" cellspacing="0" width="100%" summary=""><tr>';
    print '<th align="left">Course</th>';
    print '<th align="left">Assignment</th>';
	print '<th align="center">Duration</th>';
	print '<th align="center">Grade</th>';
	print '<th align="center">Uploaded Date</th>';
    print '</tr>';

    $even = false;
    foreach ($assignment as $rec) {
        if ($even) {
            print '<tr style="background-color: #eeeeee">';
        }
        else {
            print '<tr>';
        }
		$even = !$even;
        print '<td align="left">'.format_string($rec->Course).'</td>';
        print '<td align="left">'.format_string($rec->Asset).'</td>';
		print '<td>'.format_string($rec->Duration).'</td>';
		print '<td>'.format_string($rec->Grade).'</td>';
		print '<td>'.format_string($rec->CompletedDate).'</td>';

        print '</tr>';				
    		$sum = $sum+$rec->Duration;		
    }
	print '<tfoot><tr><td colspan="5"><img src="icon/graf.png" width="18" height="18"> You received '.sprintf("%.2f",$sum).' hours uploading assignments.</td></tr></tfoot>';
    print '</table>';
}



//////////
?>


<table align="center"  cellspacing=0 cellpadding=0
 style='float:center;border-collapse:collapse;border:none;mso-border-alt:solid windowtext .5pt;
 mso-yfti-tbllook:1184;mso-padding-alt:0in 5.4pt 0in 5.4pt'>
 <tr >
  <td  valign=top style='padding:0in 5.4pt 0in 5.4pt'>
  <table  align="center"  cellspacing=0 cellpadding=0
   style='border-collapse:collapse;border:none;mso-padding-alt:0in 5.4pt 0in 5.4pt'>
   <tr  align="center" >
    <td width=100 align="center"  valign="center" rowspan=2 >
      <?php echo '<a href="'.$CFG->wwwroot.'/user/view.php?id='.$userid.'&amp;course='.$COURSE->id.'"><img height="150" width="150" src="'.$CFG->wwwroot.'/user/pix.php?file=/'.$userid.'/f1.jpg" width="80px" height="80px" align="center" title="'.$USER->firstname.' '.$USER->lastname.'" alt="'.$USER->firstname.' '.$USER->lastname.'" /></a>'; ?>

	<?php

		if($userduration<=0)
		{
		echo '<img src="icon\stargrey.jpg" alt="stars" title="Earn 5 star by completing '.$manditorytraining.' hours of training" />';
		}
		else if($starvalue<=20)
		{
		echo '<img src="icon\star1.gif" alt="stars" title="Earn 5 star by completing '.$manditorytraining.' hours of training" />';
		}
		else if($starvalue<=40)
		{
		echo '<img src="icon\star2.gif" alt="stars" title="Earn 5 star by completing '.$manditorytraining.' hours of training" />';
		}
		else if($starvalue<=60)
		{
		echo '<img src="icon\star3.gif" alt="stars" title="Earn 5 star by completing '.$manditorytraining.' hours of training" />';
		}
		else if($starvalue<=80)
		{
		echo '<img src="icon\star4.gif" alt="stars" title="Earn 5 star by completing '.$manditorytraining.' hours of training" />';
		}
		else if($starvalue>80)
		{
		echo '<img src="icon\star5.gif" alt="stars" title="Earn 5 star by completing '.$manditorytraining.' hours of training" />';
		}
		
	?>

	</td>
	<td></td>
    <td></td>
	<td>
<!-- Gamification table to show the level
<TABLE BORDER="0" cellpadding="0" CELLSPACING="0">
<TR>

<TD WIDTH="180" HEIGHT="180" BACKGROUND="icon/levelcupreport.png" >


<span style="color:#000080;font-size:20px;font-family:tahoma,geneva,sans-serif;">
<?php echo $level->count; ?></span>
</TD>

</TR>
</TABLE> -->
	</td>
   </tr>
   <tr style='mso-yfti-irow:1;height:6.75pt'>
    <td  colspan=3 valign=top align=right style='padding:0in 5.4pt 0in 5.4pt;
    height:6.75pt'>
<!--Status message to the user -->
	<p align="left"><span style='font-size:12.0pt;line-height:115%;font-family:
"Comic Sans MS";color:#17375E;mso-themecolor:text2;mso-themeshade:191;
mso-style-textfill-fill-color:#17375E;mso-style-textfill-fill-themecolor:text2;
mso-style-textfill-fill-alpha:100.0%;mso-style-textfill-fill-colortransforms:
lumm=75000'>Dear <?php echo $USER->firstname.' '.$USER->lastname; ?> <br/> </span>
<?php
if($USER->city=="MISI" || $USER->city=="DSKLS")
{
?>
<span style='font-size:12.0pt;line-height:
115%;font-family:"Comic Sans MS";color:#17375E;mso-themecolor:text2;mso-themeshade:
191;mso-style-textfill-fill-color:#17375E;mso-style-textfill-fill-themecolor:
text2;mso-style-textfill-fill-alpha:100.0%;mso-style-textfill-fill-colortransforms:
lumm=75000'>We are unable to identify an annual training goal for you.   </span><span style='font-size:16.0pt;
line-height:115%;font-family:"Comic Sans MS";color:#558ED5;mso-themecolor:text2;
mso-themetint:153;mso-style-textfill-fill-color:#558ED5;mso-style-textfill-fill-themecolor:
text2;mso-style-textfill-fill-alpha:100.0%;mso-style-textfill-fill-colortransforms:
"lumm=60000 lumo=40000"'>Please consult with your manager.</span>
<?php
}
else if($USER->employee_type="Employee" && $USER->grade>0 && $USER->grade<16)
{ ?>

<span style='font-size:12.0pt;line-height:
115%;font-family:"Comic Sans MS";color:#17375E;mso-themecolor:text2;mso-themeshade:
191;mso-style-textfill-fill-color:#17375E;mso-style-textfill-fill-themecolor:
text2;mso-style-textfill-fill-alpha:100.0%;mso-style-textfill-fill-colortransforms:
lumm=75000'>As per your grade </span><span style='font-size:12.0pt;
line-height:115%;font-family:"Comic Sans MS";color:#17375E;mso-themecolor:text2;
mso-themeshade:191;mso-style-textfill-fill-color:#17375E;mso-style-textfill-fill-themecolor:
text2;mso-style-textfill-fill-alpha:100.0%;mso-style-textfill-fill-colortransforms:
lumm=75000'> your minimum learning and development hours are </span><span style='font-size:16.0pt;
line-height:115%;font-family:"Comic Sans MS";color:#558ED5;mso-themecolor:text2;
mso-themetint:153;mso-style-textfill-fill-color:#558ED5;mso-style-textfill-fill-themecolor:
text2;mso-style-textfill-fill-alpha:100.0%;mso-style-textfill-fill-colortransforms:
"lumm=60000 lumo=40000"'><?php echo $manditorytraining; ?>   </span>
<span style='font-size:12.0pt;line-height:
115%;font-family:"Comic Sans MS";color:#17375E;mso-themecolor:text2;mso-themeshade:
191;mso-style-textfill-fill-color:#17375E;mso-style-textfill-fill-themecolor:
text2;mso-style-textfill-fill-alpha:100.0%;mso-style-textfill-fill-colortransforms:
lumm=75000'>for 2016-17. </span>

</br>
<?php
} ?>
<span style='font-size:12.0pt;line-height:115%;font-family:
"Comic Sans MS";color:#17375E;mso-themecolor:text2;mso-themeshade:191;
mso-style-textfill-fill-color:#17375E;mso-style-textfill-fill-themecolor:text2;
mso-style-textfill-fill-alpha:100.0%;mso-style-textfill-fill-colortransforms:
lumm=75000'> You have completed  </span>

<span style='font-size:20.0pt;
line-height:115%;font-family:"Comic Sans MS";color:#E46C0A;mso-themecolor:accent6;
mso-themeshade:191;mso-style-textfill-fill-color:#E46C0A;mso-style-textfill-fill-themecolor:
accent6;mso-style-textfill-fill-alpha:100.0%;mso-style-textfill-fill-colortransforms:
lumm=75000'><?php echo $empduration; ?></span><span style='font-size:12.0pt;line-height:115%;font-family:
"Comic Sans MS";color:#17375E;mso-themecolor:text2;mso-themeshade:191;
mso-style-textfill-fill-color:#17375E;mso-style-textfill-fill-themecolor:text2;
mso-style-textfill-fill-alpha:100.0%;mso-style-textfill-fill-colortransforms:
lumm=75000'> training hours to date. </span><br/>
<!------ -->


<span style='font-size:12.0pt;
line-height:115%;font-family:"Comic Sans MS";color:#17375E;mso-themecolor:text2;
mso-themeshade:191;mso-style-textfill-fill-color:#17375E;mso-style-textfill-fill-themecolor:
text2;mso-style-textfill-fill-alpha:100.0%;mso-style-textfill-fill-colortransforms:
lumm=75000'><o:p></o:p></span></p></br>

<!-- end of status message -->

	<!-- Duration and points field -->
	<table class="tabled" border=1 cellspacing=0 cellpadding=0
 style='border-collapse:collapse;mso-table-layout-alt:fixed;border:none;
 mso-border-alt:solid windowtext .5pt;mso-yfti-tbllook:1184;mso-padding-alt:
 0in 5.4pt 0in 5.4pt'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;height:3.2pt'>
  <td width=47 valign=bottom style='width:35.3pt;border:solid #A6A6A6 1.0pt;
  mso-border-themecolor:background1;mso-border-themeshade:166;border-bottom:
  solid #DDD9C3 1.0pt;mso-border-bottom-themecolor:background2;mso-border-bottom-themeshade:
  230;mso-border-alt:solid #A6A6A6 .5pt;mso-border-themecolor:background1;
  mso-border-themeshade:166;mso-border-bottom-alt:solid #DDD9C3 .5pt;
  mso-border-bottom-themecolor:background2;mso-border-bottom-themeshade:230;
  background:#984806;mso-background-themecolor:accent6;mso-background-themeshade:
  128;padding:0in 5.4pt 0in 5.4pt;height:3.2pt'>
  <p class=MsoNormal align=center style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
  auto;text-align:center;mso-line-height-alt:3.2pt'><span style='mso-bidi-font-family:
  Calibri;mso-bidi-theme-font:minor-latin;color:white;mso-themecolor:background1'>2016 - 2017</span></p>
  </td>
  <td width=19 rowspan=2 style='width:14.2pt;border:none;border-right:solid #A6A6A6 1.0pt;
  mso-border-right-themecolor:background1;mso-border-right-themeshade:166;
  mso-border-left-alt:solid #A6A6A6 .5pt;mso-border-left-themecolor:background1;
  mso-border-left-themeshade:166;mso-border-left-alt:solid #A6A6A6 .5pt;
  mso-border-left-themecolor:background1;mso-border-left-themeshade:166;
  mso-border-right-alt:solid #A6A6A6 .5pt;mso-border-right-themecolor:background1;
  mso-border-right-themeshade:166;padding:0in 5.4pt 0in 5.4pt;height:3.2pt'>
  <p class=MsoNormal align=center style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
  auto;text-align:center;mso-line-height-alt:3.2pt'><span style='mso-bidi-font-family:
  Calibri;mso-bidi-theme-font:minor-latin;color:white;mso-themecolor:background1'></span></p>
  </td>
  <td width=66 valign=bottom style='width:49.55pt;border-top:solid #A6A6A6 1.0pt;
  mso-border-top-themecolor:background1;mso-border-top-themeshade:166;
  border-left:none;border-bottom:solid #DDD9C3 1.0pt;mso-border-bottom-themecolor:
  background2;mso-border-bottom-themeshade:230;border-right:solid #A6A6A6 1.0pt;
  mso-border-right-themecolor:background1;mso-border-right-themeshade:166;
  mso-border-left-alt:solid #A6A6A6 .5pt;mso-border-left-themecolor:background1;
  mso-border-left-themeshade:166;mso-border-alt:solid #A6A6A6 .5pt;mso-border-themecolor:
  background1;mso-border-themeshade:166;mso-border-bottom-alt:solid #DDD9C3 .5pt;
  mso-border-bottom-themecolor:background2;mso-border-bottom-themeshade:230;
  background:gray;mso-background-themecolor:background1;mso-background-themeshade:
  128;padding:0in 5.4pt 0in 5.4pt;height:3.2pt'>
  <p class=MsoNormal align=center style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
  auto;text-align:center;mso-line-height-alt:3.2pt'><span style='mso-bidi-font-family:
  Calibri;mso-bidi-theme-font:minor-latin;color:white;mso-themecolor:background1'>2015 - 2016</span></p>
  </td>
  <td width=19 rowspan=2 style='width:14.2pt;border:none;border-right:solid #A6A6A6 1.0pt;
  mso-border-right-themecolor:background1;mso-border-right-themeshade:166;
  mso-border-left-alt:solid #A6A6A6 .5pt;mso-border-left-themecolor:background1;
  mso-border-left-themeshade:166;mso-border-left-alt:solid #A6A6A6 .5pt;
  mso-border-left-themecolor:background1;mso-border-left-themeshade:166;
  mso-border-right-alt:solid #A6A6A6 .5pt;mso-border-right-themecolor:background1;
  mso-border-right-themeshade:166;padding:0in 5.4pt 0in 5.4pt;height:3.2pt'>
  <p class=MsoNormal align=center style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
  auto;text-align:center;mso-line-height-alt:3.2pt'><span style='mso-bidi-font-family:
  Calibri;mso-bidi-theme-font:minor-latin;color:white;mso-themecolor:background1'><u1:p>&nbsp;</u1:p></span></p>
  </td>
  <td width=66 valign=bottom style='width:49.7pt;border-top:solid #A6A6A6 1.0pt;
  mso-border-top-themecolor:background1;mso-border-top-themeshade:166;
  border-left:none;border-bottom:solid #DDD9C3 1.0pt;mso-border-bottom-themecolor:
  background2;mso-border-bottom-themeshade:230;border-right:solid #A6A6A6 1.0pt;
  mso-border-right-themecolor:background1;mso-border-right-themeshade:166;
  mso-border-left-alt:solid #A6A6A6 .5pt;mso-border-left-themecolor:background1;
  mso-border-left-themeshade:166;mso-border-alt:solid #A6A6A6 .5pt;mso-border-themecolor:
  background1;mso-border-themeshade:166;mso-border-bottom-alt:solid #DDD9C3 .5pt;
  mso-border-bottom-themecolor:background2;mso-border-bottom-themeshade:230;
  background:#365F91;mso-background-themecolor:accent1;mso-background-themeshade:
  191;padding:0in 5.4pt 0in 5.4pt;height:3.2pt'>
  <p class=MsoNormal align=center style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
  auto;text-align:center;mso-line-height-alt:3.2pt'><span style='mso-bidi-font-family:
  Calibri;mso-bidi-theme-font:minor-latin;color:white;mso-themecolor:background1'>2014 - 2015<u1:p></u1:p></span><u2:p></u2:p><o:p></o:p></p>
  </td>
  <td width=19 rowspan=2 style='width:14.2pt;border:none;border-right:solid #ffffff 1.0pt;
  mso-border-right-themecolor:background1;mso-border-right-themeshade:166;
  mso-border-left-alt:solid #ffffff .5pt;mso-border-left-themecolor:background1;
  mso-border-left-themeshade:166;mso-border-left-alt:solid #ffffff .5pt;
  mso-border-left-themecolor:background1;mso-border-left-themeshade:166;
  mso-border-right-alt:solid #ffffff.5pt;mso-border-right-themecolor:background1;
  mso-border-right-themeshade:166;padding:0in 5.4pt 0in 5.4pt;height:3.2pt'>
  <p class=MsoNormal align=center style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
  auto;text-align:center;mso-line-height-alt:3.2pt'><span style='mso-bidi-font-family:
  Calibri;mso-bidi-theme-font:minor-latin;color:white;mso-themecolor:background1'><u1:p>&nbsp;</u1:p></span></p>
  </td>
  <!-- Gamification points header
  <td width=66 valign=bottom style='width:49.7pt;border-top:solid #A6A6A6 1.0pt;
  mso-border-top-themecolor:background1;mso-border-top-themeshade:166;
  border-left:none;border-bottom:solid #DDD9C3 1.0pt;mso-border-bottom-themecolor:
  background2;mso-border-bottom-themeshade:230;border-right:solid #A6A6A6 1.0pt;
  mso-border-right-themecolor:background1;mso-border-right-themeshade:166;
  mso-border-left-alt:solid #A6A6A6 .5pt;mso-border-left-themecolor:background1;
  mso-border-left-themeshade:166;mso-border-alt:solid #A6A6A6 .5pt;mso-border-themecolor:
  background1;mso-border-themeshade:166;mso-border-bottom-alt:solid #DDD9C3 .5pt;
  mso-border-bottom-themecolor:background2;mso-border-bottom-themeshade:230;
  background:#76923C;mso-background-themecolor:accent3;mso-background-themeshade:
  191;padding:0in 5.4pt 0in 5.4pt;height:3.2pt'>
  <p class=MsoNormal align=center style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
  auto;text-align:center;mso-line-height-alt:3.2pt'><span style='mso-bidi-font-family:
  Calibri;mso-bidi-theme-font:minor-latin;color:white;mso-themecolor:background1'>Points</span><u2:p></u2:p><o:p></o:p></p>
  </td>
  <td width=19 rowspan=2 style='width:14.2pt;border:none;border-right:solid #ffffff 1.0pt;
  mso-border-right-themecolor:background1;mso-border-right-themeshade:166;
  mso-border-left-alt:solid #ffffff .5pt;mso-border-left-themecolor:background1;
  mso-border-left-themeshade:166;mso-border-left-alt:solid #ffffff .5pt;
  mso-border-left-themecolor:background1;mso-border-left-themeshade:166;
  mso-border-right-alt:solid #ffffff .5pt;mso-border-right-themecolor:background1;
  mso-border-right-themeshade:166;padding:0in 5.4pt 0in 5.4pt;height:3.2pt'>
  <p class=MsoNormal align=center style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
  auto;text-align:center;mso-line-height-alt:3.2pt'><span style='mso-bidi-font-family:
  Calibri;mso-bidi-theme-font:minor-latin;color:white;mso-themecolor:background1'><u1:p>&nbsp;</u1:p></span></p>
  </td> -->

 </tr>
 <tr style='mso-yfti-irow:1;mso-yfti-lastrow:yes;height:35.0pt'>
  <td width=66 style='width:49.55pt;border:solid #DDD9C3 1.0pt;mso-border-themecolor:
  background2;mso-border-themeshade:230;border-top:none;mso-border-top-alt:
  solid #DDD9C3 .5pt;mso-border-top-themecolor:background2;mso-border-top-themeshade:
  230;mso-border-alt:solid #DDD9C3 .5pt;mso-border-themecolor:background2;
  mso-border-themeshade:230;background:#FBD4B4;mso-background-themecolor:accent6;
  mso-background-themetint:102;padding:0in 5.4pt 0in 5.4pt;height:35.0pt'>
  <p class=MsoNormal align=center style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
  auto;text-align:center'><span style='font-size:16.0pt;font-family:"Comic Sans MS";
  mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin;color:#E46C0A;
  mso-themecolor:accent6;mso-themeshade:191;mso-style-textfill-fill-color:#E46C0A;
  mso-style-textfill-fill-themecolor:accent6;mso-style-textfill-fill-alpha:
  100.0%;mso-style-textfill-fill-colortransforms:lumm=75000'>
<?php echo $durationcurrentyear; ?></span></p>
  </td>
  <td width=66 style='width:49.55pt;border-top:none;border-left:none;
  border-bottom:solid #DDD9C3 1.0pt;mso-border-bottom-themecolor:background2;
  mso-border-bottom-themeshade:230;border-right:solid #DDD9C3 1.0pt;mso-border-right-themecolor:
  background2;mso-border-right-themeshade:230;mso-border-top-alt:solid #DDD9C3 .5pt;
  mso-border-top-themecolor:background2;mso-border-top-themeshade:230;
  mso-border-left-alt:solid #DDD9C3 .5pt;mso-border-left-themecolor:background2;
  mso-border-left-themeshade:230;mso-border-alt:solid #DDD9C3 .5pt;mso-border-themecolor:
  background2;mso-border-themeshade:230;background:#F2F2F2;mso-background-themecolor:
  background1;mso-background-themeshade:242;padding:0in 5.4pt 0in 5.4pt;
  height:35.0pt'>
  <p class=MsoNormal align=center style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
  auto;text-align:center'><span style='font-size:16.0pt;font-family:"Comic Sans MS";
  mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin;color:#7F7F7F;
  mso-themecolor:background1;mso-themeshade:128;mso-style-textfill-fill-color:
  #7F7F7F;mso-style-textfill-fill-themecolor:background1;mso-style-textfill-fill-alpha:
  100.0%;mso-style-textfill-fill-colortransforms:lumm=50000'>
  <?php echo $durationpreviousyear; ?>
  </span></p>
  </td>
  
  <td width=66 style='width:49.7pt;border-top:none;border-left:none;border-bottom:
  solid #DDD9C3 1.0pt;mso-border-bottom-themecolor:background2;mso-border-bottom-themeshade:
  230;border-right:solid #DDD9C3 1.0pt;mso-border-right-themecolor:background2;
  mso-border-right-themeshade:230;mso-border-top-alt:solid #DDD9C3 .5pt;
  mso-border-top-themecolor:background2;mso-border-top-themeshade:230;
  mso-border-left-alt:solid #DDD9C3 .5pt;mso-border-left-themecolor:background2;
  mso-border-left-themeshade:230;mso-border-alt:solid #DDD9C3 .5pt;mso-border-themecolor:
  background2;mso-border-themeshade:230;background:#DBE5F1;mso-background-themecolor:
  accent1;mso-background-themetint:51;padding:0in 5.4pt 0in 5.4pt;height:35.0pt'>
  <p class=MsoNormal align=center style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
  auto;text-align:center'><span style='font-size:16.0pt;font-family:"Comic Sans MS";
  mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin;color:#31859C;
  mso-themecolor:accent5;mso-themeshade:191;mso-style-textfill-fill-color:#31859C;
  mso-style-textfill-fill-themecolor:accent5;mso-style-textfill-fill-alpha:
  100.0%;mso-style-textfill-fill-colortransforms:lumm=75000'><?php echo $durationpreviousyear2; ?></span></p>
  </td>
  <!--
  <td width=66 style='width:49.7pt;border-top:none;border-left:none;border-bottom:
  solid #DDD9C3 1.0pt;mso-border-bottom-themecolor:background2;mso-border-bottom-themeshade:
  230;border-right:solid #DDD9C3 1.0pt;mso-border-right-themecolor:background2;
  mso-border-right-themeshade:230;mso-border-top-alt:solid #DDD9C3 .5pt;
  mso-border-top-themecolor:background2;mso-border-top-themeshade:230;
  mso-border-left-alt:solid #DDD9C3 .5pt;mso-border-left-themecolor:background2;
  mso-border-left-themeshade:230;mso-border-alt:solid #DDD9C3 .5pt;mso-border-themecolor:
  background2;mso-border-themeshade:230;background:#EAF1DD;mso-background-themecolor:
  accent3;mso-background-themetint:51;padding:0in 5.4pt 0in 5.4pt;height:35.0pt'>
  <p class=MsoNormal align=center style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
  auto;text-align:center'><span style='font-size:16.0pt;font-family:"Comic Sans MS";
  mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin;color:#77933C;
  mso-themecolor:accent3;mso-themeshade:191;mso-style-textfill-fill-color:#77933C;
  mso-style-textfill-fill-themecolor:accent3;mso-style-textfill-fill-alpha:
  100.0%;mso-style-textfill-fill-colortransforms:lumm=75000'> </span></p>
  </td> -->

 </tr>
</table>
	<!-- -->
    </td>
   </tr>
   <tr style='mso-yfti-irow:2;mso-yfti-lastrow:yes'>
    <td width=246 colspan=2 valign=top >
    <table id="rounded-corner" summary="2007 Major IT Companies' Profit">
    <thead>
    	<tr>
        	<th scope="col" class="rounded-company">Financial Year</th>
            <th scope="col" class="rounded-q1">Courses</th>
        </tr>
    </thead>
        <tfoot>
    	<tr>
        	<tr align="right"><td colspan="2" class="rounded-foot-left"><span style="font-size:9.0pt;color:#229E88"><i>Total course completion on LMS<i></span></td>

        </tr>
    </tfoot>
    <tbody>
        <tr>
        	<td>2016 - 2017</td>
            <td align="center"><?php echo $coursecompletedcurrent; ?></td>

        </tr>
        <tr>
        	<td>2015 - 2016</td>
            <td align="center"><?php echo $coursecompletedprevious; ?></td>

        </tr>
        <tr>
        	<td>2014 - 2015</td>
            <td align="center"><?php echo $coursecompletedprevious2; ?></td>

        </tr>
    </tbody>
</table>
	

    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal'><o:p></o:p></p>
    </td>
    <td width=246 valign=top >
    <table id="rounded-corner" summary="2007 Major IT Companies' Profit">
    <thead>
    	<tr>
        	<th scope="col" class="rounded-company">Internal Training</th>
            <th scope="col" class="rounded-q1">Count</th>
        </tr>
    </thead>

    <tfoot>
    	<tr align="right"><td colspan="2" class="rounded-foot-left"><span style="font-size:9.0pt;color:#229E88"><i>Training <?php echo $yearlabel; ?><i></span></td>

        </tr>
    </tfoot>

    <tbody>
        <tr>
        	<td>E-Learning Course</td>
            <td align="center"><?php echo $elearningcount; ?></td>

        </tr>
        <tr>
        	<td>Instructor Led Training</td>
            <td align="center"><?php echo $nbILT; ?></td>

        </tr>
        <tr>
        	<td>Total</td>
            <td align="center"><?php echo $elearningcount+$nbILT; ?></td>

        </tr>
    </tbody>
</table>
    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal'><o:p></o:p></p>
    </td>
    <td width=246 valign=top >
    <table id="rounded-corner" summary="">
    <thead>
    	<tr>
        	<th scope="col" class="rounded-company">External Training</th>
            <th scope="col" class="rounded-q1">Count</th>
        </tr>
    </thead>
  
    <tbody>
        <tr>
        	<td>Skill Soft</td>
            <td align="center"><?php echo $nbskillsoft; ?></td>

        </tr>
        <tr>
        	<td>External ILT</td>
            <td align="center"><?php echo $externaltrainingcount->count; ?></td> 

        </tr>
        <tr>
        	<td>Total</td>
            <td align="center"><?php echo $nbskillsoft+$externaltrainingcount->count; ?></td>

        </tr>
		<tfoot>
    	<tr align="right"><td colspan="2" class="rounded-foot-left"><span style="font-size:9.0pt;color:#229E88"><i>Training <?php echo $yearlabel; ?></i></span></td>

        </tr>
    </tfoot>
    </tbody>
</table>
    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;
    line-height:normal'><o:p></o:p></p>
    </td>
   </tr>
   <![if !supportMisalignedColumns]>
   <tr height=0>
    <td width=190 style='border:none'></td>
    <td width=53 style='border:none'></td>
    <td width=244 style='border:none'></td>
    <td width=244 style='border:none'></td>
   </tr>
   <![endif]>
  </table>
  <font color="red"><marquee  behavior="scroll" scrollamount="5" direction="right" onmouseover="this.stop();" onmouseout="this.start();" style="background: white;width: 939px;margin-left: 49px;">
  <b>Training History Will Not Available until 31-OCT-2016</b></marquee></font>
 <!--<form method="get" action=""><table width="100%"><tr><td> <p >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b style="mso-bidi-font-weight:normal">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Select a date range :

	<select name="option">
	<option value="" selected="selected">Select</option>
	<option value="1" >2016 - 2017</option>
	<option value="2" >2015 - 2016</option>
	<option value="3" >2014 - 2015</option>
	<option value="4" >2013 - 2014</option>
	<option value="5" >2012 - 2013</option>
	<option value="6" >2011 - 2012</option>
	<option value="0" >All Dates</option></select>
                 <input type="submit" value="Submit" /></form></span> 
</td><td align="right">
</td></tr></table>-->
  
  
  <div id="usual1" class="usual"> 
  <ul> 
     <li><a href="#tab1" class="selected">Detail report</a></li> 
    <li><a href="#tab2">In progress</a></li> 
    <li><a href="#tab3" >Completed</a></li>
  </ul> 
  <div id="tab3" style="display: block; ">
  <?php 
  print '<form method="get" action=""><table width="100%"><tr><td> <p >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b style="mso-bidi-font-weight:normal"><span
 style="font-size:14.0pt;line-height:80%;
color:#669EED">Completed courses</span></b></p>';
	if ($courseCount>0)
	{
	print '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-size:12.0pt;color:#229E88">Below is the list of courses and programs you have completed.</span>
	<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-size:12.0pt;color:#FA8202">Course completed '.$yearlabel.'.</span><br/>
</td>';
	}
	else
	{
	print '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-size:12.0pt;color:#993355">There are no course listed as completed for you for the financial year '.$yearlabel.'.</span>
	<br/>';
	}
print '<td align="right"><a href="'.$CFG->wwwroot.'/course/catalogue.php?id=6" title="Go to the course catalogue" target="_blank"><img src="icon\addcourses.png" title="Add more courses to your training plan" ></a>
</td></tr></table>';
	
	if ($courseCount>0)
	{
	print_completed($completed,$userid);
	}
?>
   
  </div> 
  <div id="tab2" style="display: none; ">
  <?php
    print '<form method="get" action=""><table width="100%"><tr><td> <p >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b style="mso-bidi-font-weight:normal"><span
 style="font-size:14.0pt;line-height:80%;
color:#669EED">Inprogress Courses</span></b></p>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-size:12.0pt;color:#229E88">Below are the courses and programs you have enrolled and yet to completed.</span>

</td><td align="right">
<a href="'.$CFG->wwwroot.'/course/catalogue.php?id=6" title="Go to the course catalogue" target="_blank"><img src="icon\addcourses.png" title="Add more courses to your training plan" ></a>
</td></tr></table>';
print_inprogress($inprogress,$userid);
?>
  
  </div> 
  <div id="tab1" style="display: none; ">
  <?php

print '<table width="100%"><tr><td>
<p >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b style="mso-bidi-font-weight:normal"><span
 style="font-size:14.0pt;line-height:80%;
color:#669EED">Detail Report</span></b></p>';

if (($nbscorm > 0)||($nbresource > 0)||($nbmplayer > 0)||($nbILT > 0)||($nbskillsoft > 0)||($nbquiz > 0)||($nbassignment > 0))
{
print'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-size:12.0pt;color:#229E88">Below is the detail list of activities you have completed in various courses and programs.</span><br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-size:12.0pt;color:#FA8202">Activities completed '.$yearlabel.'.</span>
<br/>';
}
else
{
print '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-size:12.0pt;color:#993355">There are no modules listed as completed for you for the financial year '.$yearlabel.'.</span>
	<br/>';
}
if ($nbscorm > 0) {
print '<table width="100%"><tr><td>


<p >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b style="mso-bidi-font-weight:normal"><span
 style="font-size:12.0pt;line-height:80%;
color:#669EED">E-Learning modules</span></b></p></td>

<td align="right">
<img src="icon\plus.jpg" title="You have completed '.$nbscorm.' E-Learning modules '.$yearlabel.'" width="32" height="32">
<span style="font-size:10.0pt;color:#229E88"></span>
</td>
</tr></table>';


    print_scorm($scorm);	
}

if ($nbresource > 0) {
print '<table width="100%"><tr><td>


<p >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b style="mso-bidi-font-weight:normal"><span
 style="font-size:12.0pt;line-height:80%;
color:#669EED">Link & Resources</span></b></p></td>

<td align="right">
<img src="icon\plus.jpg" title="You have visited '.$nbresource.' Resources '.$yearlabel.'" width="32" height="32">
<span style="font-size:10.0pt;color:#229E88"></span>
</td>
</tr></table>';

print_resource($resource);
}
if ($nbmplayer > 0) {

print '<table width="100%"><tr><td><p >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b style="mso-bidi-font-weight:normal"><span
 style="font-size:12.0pt;line-height:80%;
color:#669EED">Streaming Media</span></b></p></td><td align="right">
<img src="icon\plus.jpg" title="You have viewed '.$nbmplayer.' Streaming Media '.$yearlabel.'" width="32" height="32">
<span style="font-size:10.0pt;color:#229E88"></span>
</td></tr></table>';

print_mplayer($mplayer);
}
if ($nbILT > 0) {
print '<table width="100%"><tr><td><p >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b style="mso-bidi-font-weight:normal"><span
 style="font-size:12.0pt;line-height:80%;
color:#669EED">Instructor Led Training</span></b></p></td><td align="right">
<img src="icon\plus.jpg" title="You have attended '.$nbILT.' Instructor Led Trainings '.$yearlabel.'" width="32" height="32">
<span style="font-size:10.0pt;color:#229E88"></span>
</td></tr></table>';


print_ILT($ILT);
}
if ($nbskillsoft > 0) {
print '<table width="100%"><tr><td><p >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b style="mso-bidi-font-weight:normal"><span
 style="font-size:12.0pt;line-height:80%;
color:#669EED">SkillSoft</span></b></p></td><td align="right">
<img src="icon\plus.jpg" title="You have completed '.$nbskillsoft.' SkillSoft assets '.$yearlabel.'" width="32" height="32">
<span style="font-size:10.0pt;color:#229E88"></span>
</td></tr></table>';


//Naga added for non macthed skillsoft courses
print_SkillSoft($skillsoft,$skillsoft_nonmatched);
//print_SkillSoft_nonmatched($skillsoft_nonmatched);
}
if ($nbquiz > 0) {
print '<table width="100%"><tr><td><p >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b style="mso-bidi-font-weight:normal"><span
 style="font-size:12.0pt;line-height:80%;
color:#669EED">Online Quiz</span></b></p></td><td align="right">
<img src="icon\plus.jpg" title="You have taken '.$nbquiz.' Online Quizs '.$yearlabel.'" width="32" height="32">
<span style="font-size:10.0pt;color:#229E88"></span>
</td></tr></table>';


print_quiz($quiz);
}
if ($nbassignment > 0) {
print '<table width="100%"><tr><td> <p >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b style="mso-bidi-font-weight:normal"><span
 style="font-size:12.0pt;line-height:80%;color:#669EED">Assignments</span></b></p></td><td align="right">
<img src="icon\plus.jpg" title="You have uploaded '.$nbassignment.' Assignments '.$yearlabel.'" width="32" height="32">
<span style="font-size:10.0pt;color:#229E88"></span>
</td></tr></table>';

print_assignment($assignment);
}

?>
  
  </div> 
</div> 
 
<script type="text/javascript"> 
  $("#usual1 ul").idTabs(); 
</script>


    
  </td>
 </tr>
</table></table>

<?php


/////////////


print_footer();

?>
