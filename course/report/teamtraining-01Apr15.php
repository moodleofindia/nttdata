<?php

require_once '../../config.php';
require_login();
require_once($CFG->libdir . '/game/badgelib.php');
require_once($CFG->libdir . '/game/pointlib.php');
require_once 'durationlib.php';
require_once 'completionlib.php';
$yearlabel="";
$sortby = optional_param('sortby', 'timestart', PARAM_ALPHA); // column to sort by
$option = optional_param('option', PARAM_INT); 
$sd = optional_param('startdate',  PARAM_INT);
$ed =  optional_param('enddate', PARAM_RAW);
$portalid = optional_param('portalid', PARAM_INT);
$userid=$USER->id;

$yearlabel="";
$elearningcount=0;
/* Course completion year wise */
$coursecompletedcurrent=getcompletionDate($userid,'2014/3/31','2015/4/1');
$coursecompletedprevious=getcompletionDate($userid,'2013/3/31','2014/4/1');
$coursecompletedprevious2=getcompletionDate($userid,'2012/3/31','2013/4/1');
/* End of course completion count */


if($option==6)
{

$sd='2009/4/1';
$ed ='2010/4/1';
$yearlabel="April 2009 to Mar 2010";
}
if($option==5)
{
$sd='2010/4/1';
$ed ='2011/4/1';
$yearlabel="April 2010 to Mar 2011";
}
if($option==4)
{
$sd='2011/4/1';
$ed ='2012/4/1';
$yearlabel="from April 2011 to Mar 2012";
}
if($option==3)
{
$sd='2012/4/1';
$ed ='2013/4/1';
$yearlabel="from April 2012 to Mar 2013";
}
if($option==2)
{
$sd='2013/4/1';
$ed ='2014/4/1';
$yearlabel="from April 2013 to Mar 2014";
}
if($option==1)
{
$sd='2014/4/1';
$ed ='2015/4/1';
$yearlabel="from April 2014 to Mar 2015";
}
if($option==0)
{
$sd='2000/4/1';
$ed ='2020/3/31';
$yearlabel="for All dates";
}
function date_compare($a, $b)
{
    $t1 = strtotime($a['datetime']);
    $t2 = strtotime($b['datetime']);
    return $t1 - $t2;
}

$pagetitle = 'My Team Trainings';
$navlinks[] = array('name' => $pagetitle, 'link' => '', 'type' => 'activityinstance');
$navigation = build_navigation($navlinks);
print_header_simple($pagetitle, '', $navigation);

?>
<script type="text/javascript" src="jquery.idTabs.min.js"></script> 
<style type="text/css">


.usual li { list-style:none; float:left; }
.usual ul a {
  display:block;
  padding:6px 10px;
  text-decoration:none!important;
  margin:1px;
  margin-left:0;
  font:10px Verdana;
  color:#FFF;
  background:#444;
}
.usual ul a:hover {
  color:#0000;
  background:#9999FF;
  }
.usual ul a.selected {
  margin-bottom:0;
  color:#ffff;
  background:#6666FF;
  border-bottom:1px solid snow;
  cursor:default;
  }
.usual div {
  padding:10px 10px 8px 10px;
  *padding-top:3px;
  *margin-top:-15px;
  clear:left;
  font:10pt Georgia;
}
.usual div a { color:#000; font-weight:bold; }

#usual2 { background:#0A0A0A; border:1px solid #1A1A1A; }
#usual2 a { background:#222; }
#usual2 a:hover { background:#000; }
#usual2 a.selected { background:snow; }
#tabs3 { background:#FF9; }

.tabled {
	font-family:Arial, Helvetica, sans-serif;
	font-size:10px;

}

.tabled tr td {

}



#box-table-b
{
	font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
	font-size: 12px;
	margin: 20px;
	text-align: center;
	border-collapse: collapse;
	border-top: 8px solid #9baff1;
	border-bottom: 8px solid #9baff1;
}
#box-table-b th
{
	font-size: 13px;
	font-weight: normal;
	padding: 8px;
	background: #D1DEE6;
	border-right: 1px solid #9baff1;
	border-left: 1px solid #9baff1;
	color: #039;
}
#box-table-b td
{
	padding: 8px;
	background: #F0F3F5; 
	border-right: 1px solid #aabcfe;
	border-left: 1px solid #aabcfe;
	color: #669;
}


.tabler a:link {
	color: #666;
	font-weight: bold;
	text-decoration:none;
}
.tabler a:visited {
	color: #999999;
	font-weight:bold;
	text-decoration:none;
}
.tabler a:active,
.tabler a:hover {
	color: #bd5a35;
	text-decoration:underline;
}
.tabler {
	font-family:Arial, Helvetica, sans-serif;
	color:#666;
	font-size:11px;
	text-shadow: 1px 1px 0px #fff;
	background:#eaebec;
	margin:20px;
	border:#ccc 1px solid;

	-moz-border-radius:3px;
	-webkit-border-radius:3px;
	border-radius:3px;

	-moz-box-shadow: 0 1px 2px #d1d1d1;
	-webkit-box-shadow: 0 1px 2px #d1d1d1;
	box-shadow: 0 1px 2px #d1d1d1;
}
.tabler th {
	padding:21px 25px 22px 25px;
	border-top:1px solid #fafafa;
	border-bottom:1px solid #e0e0e0;

	background: #ededed;
	background: -webkit-gradient(linear, left top, left bottom, from(#ededed), to(#ebebeb));
	background: -moz-linear-gradient(top,  #ededed,  #ebebeb);
}
.tabler tfoot {
	padding:21px 25px 22px 25px;
	border-top:1px solid #fafafa;
	border-bottom:1px solid #e0e0e0;

	background: #ededed;
	background: -webkit-gradient(linear, left top, left bottom, from(#ededed), to(#ebebeb));
	background: -moz-linear-gradient(top,  #ededed,  #ebebeb);
}
.tabler th:first-child{
	text-align: left;
	padding-left:20px;
}
.tabler tr:first-child th:first-child{
	-moz-border-radius-topleft:3px;
	-webkit-border-top-left-radius:3px;
	border-top-left-radius:3px;
}
.tabler tr:first-child th:last-child{
	-moz-border-radius-topright:3px;
	-webkit-border-top-right-radius:3px;
	border-top-right-radius:3px;
}
.tabler tr{
	text-align: center;
	padding-left:20px;
}
.tabler tr td:first-child{
	text-align: left;
	padding-left:20px;
	border-left: 0;
}
.tabler tr td {
	padding:14px;
	border-top: 1px solid #ffffff;
	border-bottom:1px solid #e0e0e0;
	border-left: 1px solid #e0e0e0;
	
	background: #fafafa;
	background: -webkit-gradient(linear, left top, left bottom, from(#fbfbfb), to(#fafafa));
	background: -moz-linear-gradient(top,  #fbfbfb,  #fafafa);
}
.tabler tr.even td{
	background: #f6f6f6;
	background: -webkit-gradient(linear, left top, left bottom, from(#f8f8f8), to(#f6f6f6));
	background: -moz-linear-gradient(top,  #f8f8f8,  #f6f6f6);
}
.tabler tr:last-child td{
	border-bottom:0;
}
.tabler tr:last-child td:first-child{
	-moz-border-radius-bottomleft:3px;
	-webkit-border-bottom-left-radius:3px;
	border-bottom-left-radius:3px;
}
.tabler tr:last-child td:last-child{
	-moz-border-radius-bottomright:3px;
	-webkit-border-bottom-right-radius:3px;
	border-bottom-right-radius:3px;
}
.tabler tr:hover td{
	background: #f2f2f2;
	background: -webkit-gradient(linear, left top, left bottom, from(#f2f2f2), to(#f0f0f0));
	background: -moz-linear-gradient(top,  #f2f2f2,  #f0f0f0);	
}


#rounded-corner
{
	font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
	font-size: 12px;
	margin: 45px;
	width: 250px;
	text-align: left;
	border-collapse: collapse;
}
#rounded-corner thead th.rounded-company
{
	background: #b9c9fe url('table-images/left.png') left -1px no-repeat;
}
#rounded-corner thead th.rounded-q4
{
	background: #b9c9fe url('table-images/right.png') right -1px no-repeat;
}
#rounded-corner th
{
	padding: 8px;
	font-weight: normal;
	font-size: 13px;
	color: #039;
	background: #b9c9fe;
}
#rounded-corner td
{
	padding: 8px;
	background: #e8edff;
	border-top: 1px solid #fff;
	color: #669;
}
#rounded-corner tfoot td.rounded-foot-left
{
	background: #e8edff url('table-images/botleft.png') left bottom no-repeat;
}
#rounded-corner tfoot td.rounded-foot-right
{
	background: #e8edff url('table-images/botright.png') right bottom no-repeat;
}
#rounded-corner tbody tr:hover td
{
	background: #d0dafd;
}
</style>
<?php

$countCompleted=0;
$countInprogress=0;
$countEnrolled=0;
$manditorytraining=0;
/* Points for Gamification
$assignmentpoints = get_record_sql("SELECT points from mdl_gamification_points where type='assignment' and action='complete'");
$quizpoints = get_record_sql("SELECT points from mdl_gamification_points where type='quiz' and action='complete'");
$resourcepoints = get_record_sql("SELECT points from mdl_gamification_points where type='resource' and action='complete'");
$scormpoints = get_record_sql("SELECT points from mdl_gamification_points where type='scorm' and action='complete'");
$classroompoints = get_record_sql("SELECT points from mdl_gamification_points where type='classroom' and action='complete'");
$skillsoftpoints = get_record_sql("SELECT points from mdl_gamification_points where type='skillsoft' and action='complete'");
$mplayerpoints = get_record_sql("SELECT points from mdl_gamification_points where type='mplayer' and action='complete'");
*/





$portalid = $_POST['portalid'];
	

if($portalid){
$recordsCompleted =get_records_sql("SELECT * from mdl_user where username=$portalid ");
}
else{
$recordsCompleted = get_records_sql("SELECT * from mdl_user where manager_portalid=$USER->username and auth='ldap'");
}

$courseCount=0;
$completed = array();
if ($recordsCompleted) {


        foreach($recordsCompleted as $record) {

                $completed[] = $record;
				if($record->CompletedDate)
				{
					$elearningcount ++;
					$courseCount ++;
				}
                continue;

        }

}

$empduration=getdurationDate($userid,'2014/3/31','2015/4/1');
$durationpreviousyear= getdurationDate($userid,'2013/3/31','2014/4/1');
$durationpreviousyear2= getdurationDate($userid,'2012/3/31','2013/4/1');
$reqduration=$manditorytraining-$empduration;
$durationcurrentyear= $empduration;


$userduration= $durationcurrentyear;
$starvalue=$userduration*100/$manditorytraining;
$durationto5star=$manditorytraininghours-$durationcurrentyear;
/*end of the calculations */

// Function to print the table "By Naga".

 function print_completed($completed,$option) {
    global $sortbylink, $CFG;


    print '<table class="tabler" id="box-table-b" cellspacing="1" width="65%" summary=""><tr>';
    print '<th align="left">Portal ID</th>';
    print '<th align="left">Name</th>';
	//print '<th align="left">Grade</th>';
	print '<th align="left">RU</th>';
	print '<th align="left" width="5%">Completed Courses Count</th>';
    print '<th align="left" width="5%">Completed Training Hours</th>';
	if($option==1){
	print '<th align="left" width="5%">Required Training Hours</th>';
	}
	print '<th align="left">Points</th>';
	print '<th align="left" width="5%">Badges Count</th>';
	if($option==1){
	print '<th align="left" width="5%">Mandatory Training Hours</th>';
	}
	print '<th align="left">Reports</th>';
    print '</tr>';

	$finalstatus="";
	$progress=0;
	

	
    foreach ($completed as $rec) {
	$return=process_system_badges($rec->id);
	
	

if($option==6)
{

$sd='2009/4/1';
$ed ='2010/4/1';
$coursecompletedcurrent=getcompletionDate($rec->id,$sd,$ed);
$empduration=getdurationDate($rec->id,$sd,$ed);
$badges = badge_count_useryear($rec->id,$sd,$ed);
$points = point_count_useryear($rec->id,$sd,$ed);
$yearlabel="April 2009 to Mar 2010";
}
if($option==5)
{
$sd='2010/4/1';
$ed ='2011/4/1';

$coursecompletedcurrent=getcompletionDate($rec->id,$sd,$ed);
$empduration=getdurationDate($rec->id,$sd,$ed);
$badges = badge_count_useryear($rec->id,$sd,$ed);
$points = point_count_useryear($rec->id,$sd,$ed);
$yearlabel="April 2010 to Mar 2011";
}
if($option==4)
{
$sd='2011/4/1';
$ed ='2012/4/1';
$coursecompletedcurrent=getcompletionDate($rec->id,$sd,$ed);
$empduration=getdurationDate($rec->id,$sd,$ed);
$badges = badge_count_useryear($rec->id,$sd,$ed);
$points = point_count_useryear($rec->id,$sd,$ed);
$yearlabel="from April 2011 to Mar 2012";
}
if($option==3)
{
$sd='2012/4/1';
$ed ='2013/4/1';
$coursecompletedcurrent=getcompletionDate($rec->id,$sd,$ed);
$empduration=getdurationDate($rec->id,$sd,$ed);
$badges = badge_count_useryear($rec->id,$sd,$ed);
$points = point_count_useryear($rec->id,$sd,$ed);
$yearlabel="from April 2012 to Mar 2013";
}
if($option==2)
{
$sd='2013/4/1';
$ed ='2014/4/1';
$coursecompletedcurrent=getcompletionDate($rec->id,$sd,$ed);
$empduration=getdurationDate($rec->id,$sd,$ed);
$badges = badge_count_useryear($rec->id,$sd,$ed);
$points = point_count_useryear($rec->id,$sd,$ed);
$yearlabel="from April 2013 to Mar 2014";
}
if($option==1)
{
$sd='2014/4/1';
$ed ='2015/4/1';
$coursecompletedcurrent=getcompletionDate($rec->id,$sd,$ed);
$empduration=getdurationDate($rec->id,$sd,$ed);
$badges = badge_count_useryear($rec->id,$sd,$ed);
$points = point_count_useryear($rec->id,$sd,$ed);
$yearlabel="from April 2014 to Mar 2015";

}
if($option==0)
{
$sd='2000/4/1';
$ed ='2020/3/31';
$coursecompletedcurrent=getcompletionDate($rec->id,$sd,$ed);
$empduration=getdurationDate($rec->id,$sd,$ed);
$badges = badge_count_useryear($rec->id,$sd,$ed);
$points = point_count_useryear($rec->id,$sd,$ed);
$yearlabel="for All dates";
}
		
		
		//$points = point_count_user($rec->id);
		$manditorytrainingtemp = get_record_sql("SELECT mandatorytrainings from mdl_user where username=$rec->username ");
		$starvalue=$empduration*100/($manditorytrainingtemp->mandatorytrainings);
		
		
		print '<form action="" method="post">';
		 
        print '<td align="left">'.format_string($rec->username).'</td>';
        print '<td align="left">'.format_string($rec->firstname.'  '.$rec->lastname).'</td>';
		//print '<td align="left">'.format_string($rec->grade).'</td>';
		 print '<td align="left">'.format_string($rec->RU).'</td>';
        print '<td align="left">'.format_string($coursecompletedcurrent).'</td>';
        print '<td align="left">'.format_string($empduration).'</td>';
		if($option==1){
		
		print '<td align="left">'.format_string($manditorytrainingtemp->mandatorytrainings).'</td>';
	 
		}
		if($points){
		print '<td align="left">'.format_string($points ).'</td>';}
		else {
		$points=0;
		print '<td align="left">'.format_string($points ).'</td>';}
		//print '<td align="left">'.format_string($badges).'</td>';
		 print '<td align="left"><a title="Click to view the badges list " href="'.$CFG->wwwroot.'/course/report/userbadge.php?option='.$option.'&user='.$rec->id.'" target="_blank">'.format_string($badges).' </a></td>';
		if($option==1){
		if($empduration<=0)
		{
		
		print '<td align="center"><a title="'.$rec->firstname.' completed '.$starvalue.'% of Training hours" ><img src="icon\empty.png"  width="85" height="25"></a></td>';
		}
		else if($starvalue<=25)
		{
	
		print '<td align="center"><a title="'.$rec->firstname.' completed '.$starvalue.'% of Training hours" ><img src="icon\red.png"  width="85" height="25"></a></td>';
		}
		else if($starvalue<=50)
		{
		
		print '<td align="center"><a title="'.$rec->firstname.' completed '.$starvalue.'% of Training hours" ><img src="icon\orange.png"  width="85" height="25"></a></td>';
		}
		
		else if($starvalue<=75)
		{
	
		print '<td align="center"><a title="'.$rec->firstname.' completed '.$starvalue.'% of Training hours" ><img src="icon\amber.png"  width="85" height="25"></a></td>';
		}
		else if($starvalue>=100 ||$starvalue<=100)
		{
	
		print '<td align="center"><a title="'.$rec->firstname.' completed '.$starvalue.'% of Training hours" ><img src="icon\green.png"  width="85" height="25"></a></td>';
		}
		}
		print '<td align="center"><a title="Click to view the training history details" href="'.$CFG->wwwroot.'/course/report/userreport.php?option='.$option.'&user='.$rec->id.'" target="_blank"><img src="icon/report.jpg" width="35" height="35"></a></td>';
		
		

        print '</tr>';	
		
print '</form>';		
    }
		
    print '</table>';
	//$countInprogress=$countEnrolled-$countCompleted;
}
 


//////////
?>



 <form method="get" action=""><table width="100%"><tr><td> <p >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b style="mso-bidi-font-weight:normal" >Please select a date range to view your team completion records.<br></br> <b style="mso-bidi-font-weight:normal">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Select a date range :

	<select name="option">
	<option value=""   >Select</option>
	<option value="1" >2014 - 2015</option>
	<option value="2" >2013 - 2014</option>
	<option value="3" >2012 - 2013</option>
	<option value="4" >2011 - 2012</option>
	<option value="5" >2010 - 2011</option>
	<option value="6" >2009 - 2010</option>
	<option value="0" >All Dates</option></select>
                 <input type="submit" value="Submit" />
</td><td align="right">
</td></tr></table>
  
  </form>
  <?php
  $role=get_record_sql("select * from mdl_role_assignments where userid=$userid and roleid=16 and contextid=1");
  if($role){
  print '<p >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b style="mso-bidi-font-weight:normal ">Please enter the user portalid to view the completion records. <b style="mso-bidi-font-weight:normal">';
  print' <form method="post" action=""><table width="100%"><tr><td> <p >&nbsp;&nbsp;&nbsp;<b style="mso-bidi-font-weight:normal">&nbsp;&nbsp;Enter Portal Id :

	 <input type="text" name="portalid" id="portalid" >
                 <input type="submit" value="Submit" />
</td></tr></table>
  
  </form>';
  }
  ?>
  <div id="usual1" class="usual"> 
  
  <?php 
 
  print '<form method="get" action=""><table width="100%"><tr><td> <p >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b style="mso-bidi-font-weight:normal"><span
 style="font-size:14.0pt;line-height:80%;
color:#669EED">Team Members Training History</span></b></p>';
	
	

	print '<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-size:12.0pt;color:#FA8202">Team Training History Report </span> <b><span  style="font-size:15.0pt;color:#FA8202"> '.$yearlabel.'.</span></b><br/>
	
	</td></tr></table></form>';
	
	print_completed($completed,$option) ;
	
?>
   

 
</div> 
 


    
 

<?php


/////////////


print_footer();

?>
