<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="/theme/standard/styles.php" />
<link rel="stylesheet" type="text/css" href="/theme/aardvark-svc/styles.php" />
 
<!--[if IE 7]>
    <link rel="stylesheet" type="text/css" href="http://10.248.95.11/theme/standard/styles_ie7.css" />
<![endif]-->
<!--[if IE 6]>
    <link rel="stylesheet" type="text/css" href="http://10.248.95.11/theme/standard/styles_ie6.css" />
<![endif]-->

<?php // $Id: index.php,v 1.12.2.3 2008/12/01 20:02:27 skodak Exp $

// Display user activity reports for a course (totals)

    require_once('../../../config.php');
    require_once($CFG->dirroot.'/course/lib.php');

    $id = required_param('id',PARAM_INT);       // course id

    if (!$course = get_record('course', 'id', $id)) {
        error('Course id is incorrect.');
    }
	//Naga commented below line to ignore the re-directions from the info utton
   // require_login($course);
    $context = get_context_instance(CONTEXT_COURSE, $course->id);


  

    $showlastaccess = true;
    $hiddenfields = explode(',', $CFG->hiddenuserfields);


  //  $navlinks = array();
  //  $navlinks[] = array('name' => $strreports, 'link' => "../../report.php?id=$course->id", 'type' => 'misc');
 //   $navlinks[] = array('name' => $stractivityreport, 'link' => null, 'type' => 'misc');
 //   $navigation = build_navigation($navlinks);

    echo '<table id="outlinetable" class="generaltable boxaligncenter" cellpadding="5">
	<tr>';
    echo '<th class="header c0" scope="col" align="left">'.$course->fullname.'<br/></th>';
	
    echo '</tr>';

    $modinfo = get_fast_modinfo($course);


    $ri = 0;
    $prevsecctionnum = 0;
	echo '<tr"><td><h3>';
				echo $course->summary;
				echo '<br/>';
				
				echo '</h3>';
				echo '<b>Learning Resources and Activities</b><br/>';
				echo '</td></tr>';
    foreach ($modinfo->sections as $sectionnum=>$section) {
        foreach ($section as $cmid) {
            $cm = $modinfo->cms[$cmid];
            if ($cm->modname == 'label') {
                continue;
            }
            if (!$cm->uservisible) {
                continue;
            }
    
                
                

            $dimmed = $cm->visible ? '' : 'class="dimmed"';
            $modulename = get_string('modulename', $cm->modname);
            echo '<tr class="r'.$ri++.'">';			
			echo "<td class=\"cell c0 actvity\"><img src=\"$CFG->modpixpath/$cm->modname/icon.gif\" class=\"icon\" alt=\"$modulename\" />";
	

            if ($cm->modname == 'classroom') 
			{
				include_once($CFG->dirroot.'/mod/classroom/lib.php');
				//echo "<td class=\"cell c0 actvity\"><img src=\"$CFG->modpixpath/$cm->modname/icon.gif\" class=\"icon\" alt=\"$modulename\" />";
			//	echo facetoface_print_coursemodule_info($mod);
			 echo "<a $dimmed title=\"$modulename\" href=\"$CFG->wwwroot/mod/$cm->modname/view.php?id=$cm->id\">".format_string($cm->name)."<br/>".classroom_print_coursemodule_info($cm)."</a></td>";
			}
			else
			{
			//Uncomment to enable links
            //echo "<a $dimmed title=\"$modulename\" href=\"$CFG->wwwroot/mod/$cm->modname/view.php?id=$cm->id\">".format_string($cm->name)."</a></td>";
			echo "<a $dimmed title=\"$modulename\">".format_string($cm->name)."</a></td>";
			}
  
            echo '</tr>';
        }
    }
    echo '</table>';

 //   print_footer($course);


?>
