<?php
require_once '../../config.php';
//require_login();

require_once 'completionlib.php';
require_once($CFG->libdir . '/game/badgelib.php');
require_once($CFG->libdir . '/game/pointlib.php');
$option = optional_param('option', PARAM_INT); 
$sd = optional_param('startdate',  PARAM_INT);
$ed =  optional_param('enddate', PARAM_RAW);
$user = optional_param('user', PARAM_INT);

$userid=$user;
$username=get_record_sql("select * from mdl_user where id=$userid");
$return=process_system_badges($userid);
$yearlabel="";

$badges = array();


if($option==6)
{
$sd='2011/4/1';
$ed ='2012/4/1';
$yearlabel="April 2011 to Mar 2012";

}
if($option==5)
{
$sd='2012/4/1';
$ed ='2013/4/1';
$yearlabel="from April 2012 to Mar 2013";

}
if($option==4)
{
$sd='2013/4/1';
$ed ='2014/4/1';
$yearlabel="from April 2013 to Mar 2014";

}
if($option==3)
{
$sd='2014/4/1';
$ed ='2015/4/1';
$yearlabel="from April 2014 to Mar 2015";

}
if($option==2)
{
$sd='2015/4/1';
$ed ='2016/4/1';
$yearlabel="from April 2015 to Mar 2016";
$badgecounter=badge_count_useryear($userid,$sd,$ed);
}
if($option==1)
{
$sd='2016/4/1';
$ed ='2017/4/1';
$yearlabel="from April 2016 to Mar 2017";
$badgecounter=badge_count_useryear($userid,$sd,$ed);
}
if($option==0)
{
$sd='2000/4/1';
$ed ='2020/3/31';
$yearlabel="for All dates";

}

$badgename='Badges';
$pagetitle = $username->firstname."'s" .' '.$badgename;
$navlinks[] = array('name' => $pagetitle, 'link' => '', 'type' => 'activityinstance');
$navigation = build_navigation($navlinks);
print_header_simple($pagetitle, '', $navigation);

?>

<style type="text/css">


.usual li { list-style:none; float:left; }
.usual ul a {
  display:block;
  padding:6px 10px;
  text-decoration:none!important;
  margin:1px;
  margin-left:0;
  font:10px Verdana;
  color:#FFF;
  background:#444;
}
.usual ul a:hover {
  color:#0000;
  background:#9999FF;
  }
.usual ul a.selected {
  margin-bottom:0;
  color:#ffff;
  background:#6666FF;
  border-bottom:1px solid snow;
  cursor:default;
  }
.usual div {
  padding:10px 10px 8px 10px;
  *padding-top:3px;
  *margin-top:-15px;
  clear:left;
  font:10pt Georgia;
}
.usual div a { color:#000; font-weight:bold; }

#usual2 { background:#0A0A0A; border:1px solid #1A1A1A; }
#usual2 a { background:#222; }
#usual2 a:hover { background:#000; }
#usual2 a.selected { background:snow; }
#tabs3 { background:#FF9; }

.tabled {
	font-family:Arial, Helvetica, sans-serif;
	font-size:10px;

}

.tabled tr td {

}



#box-table-b
{
	font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
	font-size: 12px;
	margin: 20px;
	text-align: center;
	border-collapse: collapse;
	border-top: 8px solid #9baff1;
	border-bottom: 8px solid #9baff1;
}
#box-table-b th
{
	font-size: 13px;
	font-weight: normal;
	padding: 8px;
	background: #D1DEE6;
	border-right: 1px solid #9baff1;
	border-left: 1px solid #9baff1;
	color: #039;
}
#box-table-b td
{
	padding: 8px;
	background: #F0F3F5; 
	border-right: 1px solid #aabcfe;
	border-left: 1px solid #aabcfe;
	color: #669;
}


.tabler a:link {
	color: #666;
	font-weight: bold;
	text-decoration:none;
}
.tabler a:visited {
	color: #999999;
	font-weight:bold;
	text-decoration:none;
}
.tabler a:active,
.tabler a:hover {
	color: #bd5a35;
	text-decoration:underline;
}
.tabler {
	font-family:Arial, Helvetica, sans-serif;
	color:#666;
	font-size:11px;
	text-shadow: 1px 1px 0px #fff;
	background:#eaebec;
	margin:20px;
	border:#ccc 1px solid;

	-moz-border-radius:3px;
	-webkit-border-radius:3px;
	border-radius:3px;

	-moz-box-shadow: 0 1px 2px #d1d1d1;
	-webkit-box-shadow: 0 1px 2px #d1d1d1;
	box-shadow: 0 1px 2px #d1d1d1;
}
.tabler th {
	padding:21px 25px 22px 25px;
	border-top:1px solid #fafafa;
	border-bottom:1px solid #e0e0e0;

	background: #ededed;
	background: -webkit-gradient(linear, left top, left bottom, from(#ededed), to(#ebebeb));
	background: -moz-linear-gradient(top,  #ededed,  #ebebeb);
}
.tabler tfoot {
	padding:21px 25px 22px 25px;
	border-top:1px solid #fafafa;
	border-bottom:1px solid #e0e0e0;

	background: #ededed;
	background: -webkit-gradient(linear, left top, left bottom, from(#ededed), to(#ebebeb));
	background: -moz-linear-gradient(top,  #ededed,  #ebebeb);
}
.tabler th:first-child{
	text-align: left;
	padding-left:20px;
}
.tabler tr:first-child th:first-child{
	-moz-border-radius-topleft:3px;
	-webkit-border-top-left-radius:3px;
	border-top-left-radius:3px;
}
.tabler tr:first-child th:last-child{
	-moz-border-radius-topright:3px;
	-webkit-border-top-right-radius:3px;
	border-top-right-radius:3px;
}
.tabler tr{
	text-align: center;
	padding-left:20px;
}
.tabler tr td:first-child{
	text-align: left;
	padding-left:20px;
	border-left: 0;
}
.tabler tr td {
	padding:14px;
	border-top: 1px solid #ffffff;
	border-bottom:1px solid #e0e0e0;
	border-left: 1px solid #e0e0e0;
	
	background: #fafafa;
	background: -webkit-gradient(linear, left top, left bottom, from(#fbfbfb), to(#fafafa));
	background: -moz-linear-gradient(top,  #fbfbfb,  #fafafa);
}
.tabler tr.even td{
	background: #f6f6f6;
	background: -webkit-gradient(linear, left top, left bottom, from(#f8f8f8), to(#f6f6f6));
	background: -moz-linear-gradient(top,  #f8f8f8,  #f6f6f6);
}
.tabler tr:last-child td{
	border-bottom:0;
}
.tabler tr:last-child td:first-child{
	-moz-border-radius-bottomleft:3px;
	-webkit-border-bottom-left-radius:3px;
	border-bottom-left-radius:3px;
}
.tabler tr:last-child td:last-child{
	-moz-border-radius-bottomright:3px;
	-webkit-border-bottom-right-radius:3px;
	border-bottom-right-radius:3px;
}
.tabler tr:hover td{
	background: #f2f2f2;
	background: -webkit-gradient(linear, left top, left bottom, from(#f2f2f2), to(#f0f0f0));
	background: -moz-linear-gradient(top,  #f2f2f2,  #f0f0f0);	
}

.tableb { 
	font-family:Arial, Helvetica, sans-serif;
	color:#666;
	font-size:11px;
	text-shadow: 1px 1px 0px #fff;
	background:#eaebec;
	margin:20px;
	border:#ccc 1px solid;

	-moz-border-radius:3px;
	-webkit-border-radius:3px;
	border-radius:3px;

	-moz-box-shadow: 0 1px 2px #d1d1d1;
	-webkit-box-shadow: 0 1px 2px #d1d1d1;
	box-shadow: 0 1px 2px #d1d1d1;
}
		
.tableb td, th { 
	border: 1px solid transparent; /* No more visible border */
	height: 30px; 
	transition: all 0.3s;  /* Simple transition for hover effect */
}
		
	
.tableb td {
	padding:14px;
	border-top: 1px solid #ffffff;
	border-bottom:1px solid #e0e0e0;
	border-left: 1px solid #e0e0e0;	
	background: #fafafa;
	background: -webkit-gradient(linear, left top, left bottom, from(#fbfbfb), to(#fafafa));
	background: -moz-linear-gradient(top,  #fbfbfb,  #fafafa);
	text-align: center;
}

/* Cells in even rows (2,4,6...) are one color */		
.tableb tr:nth-child(even) td { background: #F1F1F1; }   

/* Cells in odd rows (1,3,5...) are another (excludes header cells)  */		
.tableb tr:nth-child(odd) td { background: #FEFEFE; }  
		
.tableb tr td:hover { background: #00FF99; color: #FFF; } /* Hover cell effect! */


#rounded-corner
{
	font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
	font-size: 12px;
	margin: 45px;
	width: 250px;
	text-align: left;
	border-collapse: collapse;
}
#rounded-corner thead th.rounded-company
{
	background: #b9c9fe url('table-images/left.png') left -1px no-repeat;
}
#rounded-corner thead th.rounded-q4
{
	background: #b9c9fe url('table-images/right.png') right -1px no-repeat;
}
#rounded-corner th
{
	padding: 8px;
	font-weight: normal;
	font-size: 13px;
	color: #039;
	background: #b9c9fe;
}
#rounded-corner td
{
	padding: 8px;
	background: #e8edff;
	border-top: 1px solid #fff;
	color: #669;
}
#rounded-corner tfoot td.rounded-foot-left
{
	background: #e8edff url('table-images/botleft.png') left bottom no-repeat;
}
#rounded-corner tfoot td.rounded-foot-right
{
	background: #e8edff url('table-images/botright.png') right bottom no-repeat;
}
#rounded-corner tbody tr:hover td
{
	background: #d0dafd;
}

a.tooltip {outline:none; }
a.tooltip strong {line-height:30px;}
a.tooltip:hover {text-decoration:none;} 
a.tooltip span {
	padding: 10px;
    z-index:10;display:none; padding:14px 20px;
    margin-top:-30px; margin-left:28px;
    width:240px; line-height:16px;
}
a.tooltip:hover span{
	padding: 10px;
    display:inline; position:absolute; color:#111;
    border:1px solid #DCA; background:#fffAF0;}
.callout {z-index:20;position:absolute;top:30px;border:0;left:-12px;}
    
/*CSS3 extras*/
a.tooltip span
{
    border-radius:4px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
        
    -moz-box-shadow: 5px 5px 8px #CCC;
    -webkit-box-shadow: 5px 5px 8px #CCC;
    box-shadow: 5px 5px 8px #CCC;
}
</style>
<?php


if($option==6)
{
$sd='2011/4/1';
$ed ='2012/4/1';
$yearlabel="April 2011 to Mar 2012";
$badgecounter=badge_count_useryear($userid,$sd,$ed);
//$records=badge_select_user($userid,$sd,$ed);
}
if($option==5)
{
$sd='2012/4/1';
$ed ='2013/4/1';
$yearlabel="from April 2012 to Mar 2013";
$badgecounter=badge_count_useryear($userid,$sd,$ed);
//$records=badge_select_user($userid,$sd,$ed);
}
if($option==4)
{
$sd='2013/4/1';
$ed ='2014/4/1';
$yearlabel="from April 2013 to Mar 2014";
$badgecounter=badge_count_useryear($userid,$sd,$ed);
//$records=badge_select_user($userid,$sd,$ed);
}
if($option==3)
{
$sd='2014/4/1';
$ed ='2015/4/1';
$yearlabel="from April 2014 to Mar 2015";
$badgecounter=badge_count_useryear($userid,$sd,$ed);
//$records=badge_select_user($userid,$sd,$ed);
}
if($option==2)
{
$sd='2015/4/1';
$ed ='2016/4/1';
$yearlabel="from April 2015 to Mar 2016";
$badgecounter=badge_count_useryear($userid,$sd,$ed);
//$records=badge_select_user($userid,$sd,$ed);
}
if($option==1)
{
$sd='2016/4/1';
$ed ='2017/4/1';
$yearlabel="from April 2016 to Mar 2017";
$badgecounter=badge_count_useryear($userid,$sd,$ed);
//$records=badge_select_user($userid,$sd,$ed);
}
if($option==0)
{
$sd='2000/4/1';
$ed ='2020/3/31';
$yearlabel="for All dates";
//$records=badge_select_user($userid,$sd,$ed);
$badgecounter=badge_count_useryear($userid,$sd,$ed);
}


 
 //Gamification Computing the total points and calculations 
$points = point_count_user($userid);
/* Count for levels
$level=get_record_sql("select max(level) as count from mdl_gamification_levels where points <=$points");
	if(!$level->count)
	{
	$level->count=0;
	}
	$level->count=2;
$nextl=$level->count+1;
$nextlevel=get_record_sql("select points as count from mdl_gamification_levels where level=$nextl");
$missingpoints=$nextlevel->count-$points;

/*end of the calculations */


 ?>
 

 <form method="get" action=""><table width="100%"><tr><td> <p >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b style="mso-bidi-font-weight:normal" >Please select a date range to view your team completion records.<br></br> <b style="mso-bidi-font-weight:normal">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Select a date range :

	<select name="option">
	<option value=""   >Select</option>
	<option value="1" >2016 - 2017</option>
	<option value="2" >2015 - 2016</option>
	<option value="3" >2014 - 2015</option>
	<option value="4" >2013 - 2014</option>
	<option value="5" >2012 - 2013</option>
	<option value="6" >2011 - 2012</option>
	<option value="0" >All Dates</option></select>
	<input type="hidden" name="user" value="<?php echo $userid; ?>" />

	
                 <input type="submit" value="Submit"  />
</td><td align="right">
</td></tr></table>
  
  </form>
<br></br>
<table align="center"  cellspacing=0 cellpadding=0
 style='float:center;border-collapse:collapse;border:none;mso-border-alt:solid windowtext .5pt;
 mso-yfti-tbllook:1184;mso-padding-alt:0in 5.4pt 0in 5.4pt'>
 <tr >
  <td  valign=top style='padding:0in 5.4pt 0in 5.4pt'>
<?php 
$badges = array();

if ($records=badge_select($userid)) {

        foreach($records as $record) {


                $badges[] = $record;
					
                continue;

        }
}
?>
<span style='font-size:16.0pt;
line-height:115%;font-family:"Comic Sans MS";color:#558ED5;mso-themecolor:text2;
mso-themetint:153;mso-style-textfill-fill-color:#558ED5;mso-style-textfill-fill-themecolor:
text2;mso-style-textfill-fill-alpha:100.0%;mso-style-textfill-fill-colortransforms:
"lumm=60000 lumo=40000"'><?php 
echo $username->firstname; ?> obtain (<?php echo $badgecounter;?>) Badges <?php echo  $yearlabel;?></span>
<br></br><br></br>
<span style='font-size:16.0pt;
line-height:115%;font-family:"Comic Sans MS";color:#558ED5;mso-themecolor:text2;
mso-themetint:153;mso-style-textfill-fill-color:#558ED5;mso-style-textfill-fill-themecolor:
text2;mso-style-textfill-fill-alpha:100.0%;mso-style-textfill-fill-colortransforms:
"lumm=60000 lumo=40000"'>Please find the All Employee  Badges below </span>
<?php


	$row=0;
if($badgecounter>0)
{

   print'<table border="0" class="tableb"  width="100%" cellpadding="3" cellspacing="5">';

    foreach ($badges as $badge) 
	{
		if($row % 4 ==0)
		{
		print'<tr><td>';
		print'<a href="#" class="tooltip">
		<img src="'.$CFG->wwwroot.'/file.php/1/badges/'.$badge->badge.'" alt="" width="100" height="100">
			<span>
			<p align="left">'.$badge->comments.'</p>
			</span>
        </a>';		
		print'</td>';
					
		}
		else
		{
		print'<td>';
		print'<a href="#" class="tooltip">
		<img src="'.$CFG->wwwroot.'/file.php/1/badges/'.$badge->badge.'" alt="" width="100" height="100">
			<span>
			<p align="left">'.$badge->comments.'</p>
			</span>
        </a>';		
		print'</td>';
		
		}
		$row++;
	}
	print'</tr></table>';
}
?>


<br></br>
<br></br>

<?php


/////////////


print_footer();

?>
