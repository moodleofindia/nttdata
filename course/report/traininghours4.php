<?php

// Displays sessions for which the current user is a "teacher" (can see attendees' list)
// as well as the ones where the user is signed up (i.e. a "student")

require_once '../../config.php';
require_once($CFG->libdir.'/adminlib.php');
include_once($CFG->dirroot.'/mnet/lib.php');
//require_once 'durationlib.php';

$sortby = optional_param('sortby', 'timestart', PARAM_ALPHA); // column to sort by
$option = optional_param('option', PARAM_INT); 
$sd = optional_param('startdate',  PARAM_INT);
$ed =  optional_param('enddate', PARAM_RAW);


$selectportalid=optional_param('text',$USER->username, PARAM_TEXT);
// $action = optional_param('action','', PARAM_ALPHA); // one of: '', export
// $format = optional_param('format','ods', PARAM_ALPHA); // one of: ods, xls
$action = optional_param('action',          '', PARAM_ALPHA); // one of: '', export
$format = optional_param('format',       'ods', PARAM_ALPHA); // one of: ods, xls

$page         = optional_param('page', 0, PARAM_INT);
$perpage      = optional_param('perpage',30000, PARAM_INT);

 $startyear  = optional_param('startyear',  strftime('%Y', $timenow), PARAM_INT);
 $startmonth = optional_param('startmonth', strftime('%m', $timenow), PARAM_INT);
 $startday   = optional_param('startday',   strftime('%d', $timenow), PARAM_INT);
 $endyear    = optional_param('endyear',    strftime('%Y', $timelater), PARAM_INT);
 $endmonth   = optional_param('endmonth',   strftime('%m', $timelater), PARAM_INT);
 $endday     = optional_param('endday',     strftime('%d', $timelater), PARAM_INT);

require_login();

$records = array();


if($option==2){
$Userrecords=array();
$Userrecords = get_records_sql("SELECT u.id,u.username as PortalID,
u.firstname as  EmpFirstName,
u.lastname as  EmpLastName,
u.country as EmpCountry,
u.source as LegacyCompany,
u.bu as BU,
u.ru as RU,
u.manager_portalid as ManagerPortalID,
(select {$CFG->prefix}user.firstname from {$CFG->prefix}user where username=u.manager_portalid
)as ManagerFirstName,
(select {$CFG->prefix}user.lastname from {$CFG->prefix}user where username=u.manager_portalid
)as ManagerLastName,
u.hireddate as HiredDate,
m.hours as RequiredTrainingHours
from {$CFG->prefix}user u
join {$CFG->prefix}grade_training m on u.grade=m.grade 
where u.username=087357 and u.auth = 'ldap'and u.employee_type in ('Employee','EMPLOYEE','REGULAR EMPLOYEE') group by u.id ;");
$dates = array();
if ($Userrecords) {
        foreach($Userrecords as $record	) {
		  $empduration=getreport($record->id,'2012/4/1','2012/12/31');
		        
				$record->AquiredTrainingHours = $empduration;
              $dates[] = $record;
			 
                continue;
		}
}
$nodata = count($dates);
}else{
$Userrecords=array();
$Userrecords = get_records_sql("SELECT u.id,u.username as PortalID,
u.firstname as  EmpFirstName,
u.lastname as  EmpLastName,
u.country as EmpCountry,
u.source as LegacyCompany,
u.bu as BU,
b.budescription as BUDescription,
u.ru as RU,
u.manager_portalid as ManagerPortalID,
(select {$CFG->prefix}user.firstname from {$CFG->prefix}user where username=u.manager_portalid
)as ManagerFirstName,
(select {$CFG->prefix}user.lastname from {$CFG->prefix}user where username=u.manager_portalid
)as ManagerLastName,
u.hireddate as HiredDate,
m.hours as RequiredTrainingHours
from {$CFG->prefix}user u
join {$CFG->prefix}grade_training m on u.grade=m.grade 
join {$CFG->prefix}bu_description b on b.bu=u.bu
where   u.auth = 'ldap'and u.employee_type in ('Employee','EMPLOYEE','REGULAR EMPLOYEE','E','R') group by u.id ;");
$dates = array();
if ($Userrecords) {
        foreach($Userrecords as $record	) {

		        
              $dates[] = $record;
			 
                continue;
		}
}
$nodata = count($dates);
}

$perpage = 30000;



function print_table($dates,$includebookings){
// //Printed
     print '<table border="1" width="100%" height="500%" CELLPADDING="2"  summary="'.get_string('sessiondatestable', 'block_classroom').'"><tr>';
	print '<th>'.get_string('portalid','block_classroom').'</th>';
	print '<th>'.get_string('employeefirstname','block_classroom').'</th>';
    print '<th>'.get_string('employeelastname','block_classroom').'</th>';
	print '<th>'.get_string('country','block_classroom').'</th>';
	print '<th>'.get_string('legacycompany','block_classroom').'</th>';
    print '<th>'.get_string('bu','block_classroom').'</th>';
	print '<th>'.get_string('budescription','block_classroom').'</th>';
    print '<th>'.get_string('ru','block_classroom').'</th>';
	print '<th>'.get_string('managerportalid','block_classroom').'</th>';
	print '<th>'.get_string('managerfirstname','block_classroom').'</th>';
	print '<th>'.get_string('managerlastname','block_classroom').'</th>';
	print '<th>'.get_string('hireddate','block_classroom').'</th>';
	print '<th>'.get_string('requiredtraininghours','block_classroom').'</th>';
	print '<th>'.get_string('aquiredtraininghours','block_classroom').'</th>';
		print '</tr>';
	$even = false; // used to colour rows
    foreach ($dates as $date) {
            
		
       if ($even) {
            print '<tr style="background-color: #eeeeee">';
			}
        else {
            print '<tr>';
			}
        print '<td>'.format_string($date->PortalID).'</td>';
        print '<td>'.format_string($date->EmpFirstName).'</td>';
		print '<td>'.format_string($date->EmpLastName).'</td>';
		print '<td>'.format_string($date->EmpCountry).'</td>';
		print '<td>'.format_string($date->LegacyCompany).'</td>';
		print '<td>'.format_string($date->BU).'</td>';
		print '<td>'.format_string($date->BUDescription).'</td>';
		print '<td>'.format_string($date->RU).'</td>';
		print '<td>'.format_string($date->ManagerPortalID).'</td>';
		print '<td>'.format_string($date->ManagerFirstName).'</td>';
		print '<td>'.format_string($date->ManagerLastName).'</td>';
		print '<td>'.format_string($date->HiredDate).'</td>';
		print '<td>'.format_string($date->RequiredTrainingHours).'</td>';
		print '<td>'.format_string($date->AquiredTrainingHours).'</td>';
		print '</tr>';
	}
    print '</table>';
 }
 function getreport($userid,$starttime,$endtime) {
        global $CFG;
		$total=0;
		
		$resource = get_record_sql("SELECT sum(r.duration) as 'Duration'
		FROM mdl_grade_grades g join mdl_grade_items i
		on  g.itemid=i.id
		and g.rawgrade=g.rawgrademax
		and i.itemmodule in ('resource')
		join mdl_resource r on r.id=i.iteminstance
		JOIN mdl_course AS c ON c.id=r.course
		join mdl_user u on u.id=g.userid and u.id=$userid
		and g.timemodified > unix_timestamp('$starttime')
		and g.timemodified < unix_timestamp('$endtime')");
		$total=$resource->Duration;
		
		$assignment = get_record_sql("SELECT sum(a.duration) as 'Duration'
		FROM mdl_assignment_submissions s join mdl_assignment a
		on s.assignment=a.id and s.timemodified >0 join mdl_course c
		on c.id=a.course join mdl_user u
		on u.id=s.userid and u.id=$userid
		and s.timemodified > unix_timestamp('$starttime')
		and s.timemodified < unix_timestamp('$endtime')");
		$total+=$assignment->Duration;
						
        return sprintf("%.0f",$total);
    }

 function export_spreadsheet($dates, $format,$includebookings) {
    global $CFG;
$option = optional_param('option', PARAM_INT); 
    $timenow = time();
    $timeformat = str_replace(' ', '_', get_string('strftimedate'));
    $downloadfilename = clean_filename('Resource_Assignment_'.$option.'_'.userdate($timenow, $timeformat));

    if ('ods' === $format) {
        // OpenDocument format (ISO/IEC 26300)
        require_once($CFG->dirroot.'/lib/odslib.class.php');
        $downloadfilename .= '.ods';
        $workbook = new MoodleODSWorkbook('-');
    }
    else {
        // Excel format
        require_once($CFG->dirroot.'/lib/excellib.class.php');
        $downloadfilename .= '.xls';
        $workbook = new MoodleExcelWorkbook('-');
    }

    $workbook->send($downloadfilename);
    $worksheet =& $workbook->add_worksheet(get_string('sessionlist', 'block_classroom'));

    // Heading (first row)
	$worksheet->write_string(0, 0, get_string('portalid','block_classroom'));
	$worksheet->write_string(0, 1, get_string('employeefirstname','block_classroom'));
    $worksheet->write_string(0, 2, get_string('employeelastname','block_classroom'));
	$worksheet->write_string(0, 3, get_string('country','block_classroom'));
	$worksheet->write_string(0, 4, get_string('legacycompany','block_classroom'));
    $worksheet->write_string(0, 5, get_string('bu','block_classroom'));
	$worksheet->write_string(0, 6, get_string('budescription','block_classroom'));
    $worksheet->write_string(0, 7, get_string('ru','block_classroom'));
	$worksheet->write_string(0, 8, get_string('managerportalid','block_classroom'));
	$worksheet->write_string(0, 9, get_string('managerfirstname','block_classroom'));
	$worksheet->write_string(0, 10, get_string('managerlastname','block_classroom'));
	$worksheet->write_string(0, 11, get_string('hireddate','block_classroom'));
	$worksheet->write_string(0, 12, get_string('requiredtraininghours','block_classroom'));
	$worksheet->write_string(0, 13, get_string('aquiredtraininghours','block_classroom'));
	
  

    if (!empty($dates)) {
        $i = 0;
        foreach ($dates as $date) {
            $i++;
			if($option=='Quater1'){
				 $empduration=getreport($date->id,'2013/4/1','2013/6/30');
				 }
				 if($option=='Quater2'){
				 $empduration=getreport($date->id,'2013/4/1','2013/9/30');
				 }
				 if($option=='Quater3'){
				 $empduration=getreport($date->id,'2013/4/1','2013/12/31');
				 }
				 if($option=='Quater4'){
				 $empduration=getreport($date->id,'2013/4/1','2014/3/31');
				 }
			$date->AquiredTrainingHours = $empduration;
        $worksheet->write_string($i, 0, $date->PortalID);
        $worksheet->write_string($i, 1, $date->EmpFirstName);
		$worksheet->write_string($i, 2, $date->EmpLastName);
		$worksheet->write_string($i, 3, $date->EmpCountry);
		$worksheet->write_string($i, 4, $date->LegacyCompany);
		$worksheet->write_string($i, 5, $date->BU);
		$worksheet->write_string($i, 6, $date->BUDescription);
		$worksheet->write_string($i, 7, $date->RU);
		$worksheet->write_string($i, 8, $date->ManagerPortalID);
		$worksheet->write_string($i, 9, $date->ManagerFirstName);
		$worksheet->write_string($i, 10, $date->ManagerLastName);
		$worksheet->write_string($i, 11, $date->HiredDate);
		$worksheet->write_string($i, 12, $date->RequiredTrainingHours);
		$worksheet->write_string($i, 13, $date->AquiredTrainingHours);
		
			      
        }
    }

    $workbook->close();
}
if ('export' == $action) {
    export_spreadsheet($dates, $format,true);
    exit;
}

$pagetitle = 'Training History';
$navlinks[] = array('name' => $pagetitle, 'link' => '', 'type' => 'activityinstance');
$navigation = build_navigation($navlinks);
print_header_simple($pagetitle, '', $navigation);
global $CFG;
print '<form method="get" action="">';
?>
 <html>
 <body>
 <form method="get" action=""><table width="100%"><tr><td> <p ><b style="mso-bidi-font-weight:normal">
Select a date range :

	<select name="option">
	<option value="2" >Select</option>
	<option value="Quater1" >Quater1</option>
	<option value="Quater2" >Quater2</option>
	<option value="Quater3" >Quater3</option>
	<option value="Quater4" >Quater4</option>
                 <input type="submit" value="Submit" /></form></span> 
</td><td align="right">
</td></tr></table>
  </body>
  </html></br></br>
<?php


if ($nodata > 1) {
	//print_paging_bar($nodata, $page, $perpage, "?perpage=$perpage&amp;");
print '<h3>'.'Click on export to get the Training History Report of Users:'.'</h3>';
	echo '<p>&nbsp;</p>';
    //print_paging_bar($nodata, $page, $perpage, "perpage=$perpage&amp;");

	
	print '<h3>'.get_string('exportrecords', 'block_classroom').'</h3>';
    print '<form method="post" action=""><p>';
	print '<input type="hidden" name="startyear" value="'.$startyear.'" />';
    print '<input type="hidden" name="startmonth" value="'.$startmonth.'" />';
    print '<input type="hidden" name="startday" value="'.$startday.'" />';
    print '<input type="hidden" name="endyear" value="'.$endyear.'" />';
    print '<input type="hidden" name="endmonth" value="'.$endmonth.'" />';
    print '<input type="hidden" name="endday" value="'.$endday.'" />';
    print '<input type="hidden" name="sortby" value="'.$sortby.'" />';
    print '<input type="hidden" name="action" value="export" />';

    print get_string('format', 'classroom').':&nbsp;';
    print '<select name="format">';
    print '<option value="excel" selected="selected">'.get_string('excelformat', 'classroom').'</option>';
   // print '<option value="ods">'.get_string('odsformat', 'classroom').'</option>';
    print '</select>';

    print ' <input type="submit" value="'.get_string('exporttofile', 'classroom').'" /></p></form>';
	// echo '<p>&nbsp;</p>';
	// echo '<p>&nbsp;</p>';
	    // print_table($dates,true);
}else {
    print '<p>'.get_string('norecords', 'block_classroom').'</p>';
}
print_footer();
?>
