<?php

	require_once '../../config.php';
    require_once($CFG->libdir.'/gradelib.php');


    function getduration($userid) {
        global $CFG;
		$total=0;
		$scorm = get_record_sql("SELECT sum(sc.duration) as 'Duration'
		FROM mdl_scorm_scoes_track AS st
		JOIN mdl_scorm_scoes ssc on ssc.id=st.scoid and ssc.scormtype='sco'
		JOIN mdl_scorm AS sc ON sc.id=st.scormid
		JOIN mdl_course AS c ON c.id=sc.course
		JOIN mdl_user AS u ON st.userid=u.id and u.id=$userid
		where st.element in('cmi.core.lesson_status','cmi.completion_status')
		and st.value in ('completed','passed')");
		$total=$scorm->Duration;
		
		
		$resource = get_record_sql("SELECT sum(r.duration) as 'Duration'
		FROM mdl_grade_grades g join mdl_grade_items i
		on  g.itemid=i.id
		and g.rawgrade=g.rawgrademax
		and i.itemmodule in ('resource')
		join mdl_resource r on r.id=i.iteminstance
		JOIN mdl_course AS c ON c.id=r.course
		join mdl_user u on u.id=g.userid and u.id=$userid");
		$total+=$resource->Duration;
		
		
		$mplayer = get_record_sql("SELECT sum(m.duration) as 'Duration'
		FROM mdl_grade_grades join mdl_grade_items
		on  mdl_grade_grades.itemid=mdl_grade_items.id
		and mdl_grade_grades.rawgrade=mdl_grade_grades.rawgrademax
		and mdl_grade_items.itemmodule='mplayer'
		join mdl_mplayer m on m.id=mdl_grade_items.iteminstance
		join mdl_user u on u.id=mdl_grade_grades.userid and u.id=$userid
		JOIN mdl_course AS c ON c.id=m.course");
		$total+=$mplayer->Duration;
		
		
		$ilt = get_record_sql("select sum(duration) as 'Duration' from mdl_classroom_sessions where
		mdl_classroom_sessions.id in(SELECT distinct(s.id)
		from mdl_classroom_sessions s
		join mdl_classroom_sessions_dates on s.id=mdl_classroom_sessions_dates.sessionid
		and s.datetimeknown=1 and s.status='Completed'
		join mdl_classroom on mdl_classroom.id=s.classroom
		join mdl_classroom_submissions on mdl_classroom_submissions.sessionid = s.id
		and mdl_classroom_submissions.attend=1
		join mdl_course c on c.id=mdl_classroom.course
		join mdl_user u on u.id=mdl_classroom_submissions.userid and u.id=$userid)");
		$total+=$ilt->Duration;
		
		
		$skillsoft = get_records_sql("SELECT st.id,sc.duration as 'Duration'
		FROM mdl_skillsoft_au_track AS st
		JOIN mdl_skillsoft AS sc ON sc.id=st.skillsoftid
		JOIN mdl_user AS u ON st.userid=u.id
		join mdl_skillsoft_au_track AS at
		on at.userid=u.id and at.skillsoftid=sc.id and at.element='[SUMMARY]completed'
		where st.element='[CORE]lesson_status' and st.value in ('completed','passed') and u.id=$userid
		group by sc.assetid");

		$skillsoftduration=0;
		if ($skillsoft) {
        foreach($skillsoft as $record) {

					$skillsoftduration = $skillsoftduration + $record->Duration;

			}

		}
		
		$total+=$skillsoftduration;
//Naga Added for Non matched
		$skillsoft_nonmatched = get_records_sql("SELECT st.id,sc.duration as 'Duration'
		FROM mdl_skillsoft_au_track_nonmatched AS st
		JOIN mdl_skillsoft_nonmatched AS sc ON sc.id=st.skillsoftid
		JOIN mdl_user AS u ON st.userid=u.id
		join mdl_skillsoft_au_track_nonmatched AS at
		on at.userid=u.id and at.skillsoftid=sc.id and at.element='[SUMMARY]completed'
		where st.element='[CORE]lesson_status' and st.value in ('completed','passed') and u.id=$userid
		group by sc.assetid");

		$skillsoftduration_nonmatched=0;
		if ($skillsoft_nonmatched) {
        foreach($skillsoft_nonmatched as $record) {

					$skillsoftduration_nonmatched = $skillsoftduration_nonmatched + $record->Duration;

			}

		}
		
		$total+=$skillsoftduration_nonmatched;
		
		$quiz = get_record_sql("SELECT sum(q.timelimit) as 'Duration'
		FROM mdl_grade_grades g
		INNER JOIN mdl_user u ON g.userid = u.id and u.id=$userid
		INNER JOIN mdl_grade_items i
		ON g.itemid = i.id
		INNER JOIN mdl_course c on c.id = i.courseid
		inner join mdl_quiz q on q.course=c.id and q.id=i.iteminstance
		WHERE (i.itemname IS NOT NULL)AND (i.itemtype = 'mod')AND
		(i.itemmodule = 'quiz')
		AND (g.finalgrade >= 0)");
		$total+=$quiz->Duration;
		
		
		$assignment = get_record_sql("SELECT sum(a.duration) as 'Duration'
		FROM mdl_assignment_submissions s join mdl_assignment a
		on s.assignment=a.id and s.timemodified >0 join mdl_course c
		on c.id=a.course join mdl_user u
		on u.id=s.userid and u.id=$userid");
		$total+=$assignment->Duration;
				
        return sprintf("%.0f",$total/60);
    }
	
	
	 function getdurationDate($userid,$starttime,$endtime) {
        global $CFG;
		$total=0;
		
		$scorm = get_records_sql("SELECT u.id,st.id,(sc.duration)/60 as 'Duration'
		FROM mdl_scorm_scoes_track AS st
		JOIN mdl_scorm_scoes ssc on ssc.id=st.scoid and ssc.scormtype='sco'
		JOIN mdl_scorm AS sc ON sc.id=st.scormid
		JOIN mdl_course AS c ON c.id=sc.course
		JOIN mdl_user AS u ON st.userid=u.id and u.id=$userid 
		and st.timemodified >= unix_timestamp('$starttime')
		and st.timemodified < unix_timestamp('$endtime')
		where st.element in('cmi.core.lesson_status','cmi.completion_status')
		and st.value in ('completed','passed') group by st.scormid,u.id");
		$scormduration=0;
		if ($scorm) {
        foreach($scorm as $record) {
		
                $scormduration = $scormduration + $record->Duration;
                continue;
			}

		}
		$total= $scormduration;
		
		
		$resource = get_record_sql("SELECT sum(r.duration)/60 as 'Duration'
		FROM mdl_grade_grades g join mdl_grade_items i
		on  g.itemid=i.id
		and g.rawgrade=g.rawgrademax
		and i.itemmodule in ('resource')
		join mdl_resource r on r.id=i.iteminstance
		JOIN mdl_course AS c ON c.id=r.course
		join mdl_user u on u.id=g.userid and u.id=$userid
		and g.timemodified >= unix_timestamp('$starttime')
		and g.timemodified < unix_timestamp('$endtime')");
		$total+=$resource->Duration;
		
		
		$mplayer = get_record_sql("SELECT sum(m.duration)/60 as 'Duration'
		FROM mdl_grade_grades join mdl_grade_items
		on  mdl_grade_grades.itemid=mdl_grade_items.id
		and mdl_grade_grades.rawgrade=mdl_grade_grades.rawgrademax
		and mdl_grade_items.itemmodule='mplayer'
		join mdl_mplayer m on m.id=mdl_grade_items.iteminstance
		join mdl_user u on u.id=mdl_grade_grades.userid and u.id=$userid
		and mdl_grade_grades.timemodified >= unix_timestamp('$starttime')
		and mdl_grade_grades.timemodified < unix_timestamp('$endtime')
		JOIN mdl_course AS c ON c.id=m.course");
		$total+=$mplayer->Duration;
				
		
		$ilt = get_record_sql("select sum(duration)/60 as 'Duration' from mdl_classroom_sessions where
		mdl_classroom_sessions.id in(SELECT distinct(s.id)
		from mdl_classroom_sessions s
		join mdl_classroom_sessions_dates on s.id=mdl_classroom_sessions_dates.sessionid
		and s.datetimeknown=1 and s.status='Completed'
		join mdl_classroom on mdl_classroom.id=s.classroom
		join mdl_classroom_submissions on mdl_classroom_submissions.sessionid = s.id
		and mdl_classroom_submissions.attend=1
		join mdl_course c on c.id=mdl_classroom.course
		join mdl_user u on u.id=mdl_classroom_submissions.userid and u.id=$userid
		and mdl_classroom_sessions_dates.timestart >= unix_timestamp('$starttime')
		and mdl_classroom_sessions_dates.timefinish < unix_timestamp('$endtime'))");
		$total+=$ilt->Duration;
		
		
		$skillsoft = get_records_sql("SELECT sc.id, u.username as PortalID,
		trim(sc.name) as 'Course','Skillsoft' as 'Category',
		trim(sc.name) as 'Asset',
		sc.duration/60 as 'Duration',
		st.value as 'Grade',
		DATE_FORMAT(FROM_UNIXTIME(at.value),'%b %d %Y') as 'CompletedDate'
		FROM mdl_skillsoft_au_track AS st
		JOIN mdl_skillsoft AS sc ON sc.id=st.skillsoftid
		JOIN mdl_user AS u ON st.userid=u.id
		join mdl_skillsoft_au_track AS at
		on at.userid=u.id and at.skillsoftid=sc.id and at.element='[SUMMARY]completed'
		and at.value >= unix_timestamp('$starttime')
		and at.value < unix_timestamp('$endtime')
		where st.element='[CORE]lesson_status' and st.value in ('completed','passed') and u.id=$userid
		group by sc.assetid");

		$skillsoftduration=0;
		if ($skillsoft) {
        foreach($skillsoft as $record) {

                $skillsoftduration = $skillsoftduration + $record->Duration;
                continue;
			}

		}
		
		$total+=$skillsoftduration;
		
//Naga Added to add the Non matched skillsoft course duration
//Naga removed and cc.path like('/49%') from the skillsoft query to display all the skillsoft modules in programs/certification		
		$skillsoft_nonmatched = get_records_sql("SELECT sc.id, u.username as PortalID,
		trim(sc.name) as 'Asset',
		sc.duration/60 as 'Duration',
		st.value as 'Grade',
		DATE_FORMAT(FROM_UNIXTIME(at.value),'%b %d %Y') as 'CompletedDate'
		FROM mdl_skillsoft_au_track_nonmatched AS st
		JOIN mdl_skillsoft_nonmatched AS sc ON sc.id=st.skillsoftid
		JOIN mdl_user AS u ON st.userid=u.id
		join mdl_skillsoft_au_track_nonmatched AS at
		on at.userid=u.id and at.skillsoftid=sc.id and at.element='[SUMMARY]completed'
		and at.value >= unix_timestamp('$starttime')
		and at.value < unix_timestamp('$endtime')
		where st.element='[CORE]lesson_status' and st.value in ('completed','passed') and u.id=$userid
		group by sc.assetid");

		$skillsoftduration_nonmatched=0;
		if ($skillsoft_nonmatched) {
        foreach($skillsoft_nonmatched as $record) {

                $skillsoftduration_nonmatched = $skillsoftduration_nonmatched + $record->Duration;
                continue;
			}

		}
		
		$total+=$skillsoftduration_nonmatched;
		
		$quiz = get_record_sql("SELECT sum(q.timelimit)/60 as 'Duration'
		FROM mdl_grade_grades g
		INNER JOIN mdl_user u ON g.userid = u.id and u.id=$userid
		INNER JOIN mdl_grade_items i
		ON g.itemid = i.id
		INNER JOIN mdl_course c on c.id = i.courseid
		inner join mdl_quiz q on q.course=c.id and q.id=i.iteminstance
		WHERE (i.itemname IS NOT NULL)AND (i.itemtype = 'mod')AND
		(i.itemmodule = 'quiz')
		AND (g.finalgrade >= 0) 
		and g.timemodified >= unix_timestamp('$starttime')
		and g.timemodified < unix_timestamp('$endtime')");
		$total+=$quiz->Duration;
		
		
		$assignment = get_record_sql("SELECT sum(a.duration)/60 as 'Duration'
		FROM mdl_assignment_submissions s join mdl_assignment a
		on s.assignment=a.id and s.timemodified >0 join mdl_course c
		on c.id=a.course join mdl_user u
		on u.id=s.userid and u.id=$userid
		and s.timemodified >= unix_timestamp('$starttime')
		and s.timemodified < unix_timestamp('$endtime')");
		$total+=$assignment->Duration;
				
        return sprintf("%.0f",$total);
    }
	

?>
