<?php

// Displays sessions for which the current user is a "teacher" (can see attendees' list)
// as well as the ones where the user is signed up (i.e. a "student")

require_once '../../config.php';
require_once($CFG->libdir.'/adminlib.php');
include_once($CFG->dirroot.'/mnet/lib.php');


$sortby = optional_param('sortby', 'timestart', PARAM_ALPHA); // column to sort by
$option = optional_param('option', PARAM_INT); 
$sd = optional_param('startdate',  PARAM_INT);
$ed =  optional_param('enddate', PARAM_RAW);


$selectportalid=optional_param('text',$USER->username, PARAM_TEXT);
// $action = optional_param('action','', PARAM_ALPHA); // one of: '', export
// $format = optional_param('format','ods', PARAM_ALPHA); // one of: ods, xls
$action = optional_param('action',          '', PARAM_ALPHA); // one of: '', export
$format = optional_param('format',       'ods', PARAM_ALPHA); // one of: ods, xls

$page         = optional_param('page', 0, PARAM_INT);
$perpage      = optional_param('perpage',30000, PARAM_INT);

 $startyear  = optional_param('startyear',  strftime('%Y', $timenow), PARAM_INT);
 $startmonth = optional_param('startmonth', strftime('%m', $timenow), PARAM_INT);
 $startday   = optional_param('startday',   strftime('%d', $timenow), PARAM_INT);
 $endyear    = optional_param('endyear',    strftime('%Y', $timelater), PARAM_INT);
 $endmonth   = optional_param('endmonth',   strftime('%m', $timelater), PARAM_INT);
 $endday     = optional_param('endday',     strftime('%d', $timelater), PARAM_INT);

require_login();

$records = array();




$Userrecords=array();
$Userrecords = get_records_sql("SELECT PortalID as PortalID,
EmpFirstName as  EmpFirstName,
EmpLastName as  EmpLastName,
EmpCountry as EmpCountry,
LegacyCompany as LegacyCompany,
BU as BU,
BUDescription as BUDescription,
RU as RU,
ManagerPortalID as ManagerPortalID,
ManagerFirstName as ManagerFirstName,
ManagerLastName as ManagerLastName,
HiredDate as HiredDate,
RequiredTrainingHours as RequiredTrainingHours,
sum(scorm + resource+ ilt + mplayer + quiz + assignment + skillsoft+skillsoft_nonmatched) as AquiredTrainingHours
from mdl_training_report   group by id;");
$dates = array();
if ($Userrecords) {
        foreach($Userrecords as $record	) {
		      $dates[] = $record;
			 
                continue;
		}
}
$nodata = count($dates);

$perpage = 30000;



 
 function export_spreadsheet($dates, $format,$includebookings) {
    global $CFG;

    $timenow = time();
    $timeformat = str_replace(' ', '_', get_string('strftimedate'));
    $downloadfilename = clean_filename('User Report_'.userdate($timenow, $timeformat));

    if ('ods' === $format) {
        // OpenDocument format (ISO/IEC 26300)
        require_once($CFG->dirroot.'/lib/odslib.class.php');
        $downloadfilename .= '.ods';
        $workbook = new MoodleODSWorkbook('-');
    }
    else {
        // Excel format
        require_once($CFG->dirroot.'/lib/excellib.class.php');
        $downloadfilename .= '.xls';
        $workbook = new MoodleExcelWorkbook('-');
    }

    $workbook->send($downloadfilename);
    $worksheet =& $workbook->add_worksheet(get_string('sessionlist', 'block_classroom'));

    // Heading (first row)
	$worksheet->write_string(0, 0, get_string('portalid','block_classroom'));
	$worksheet->write_string(0, 1, get_string('employeefirstname','block_classroom'));
    $worksheet->write_string(0, 2, get_string('employeelastname','block_classroom'));
	$worksheet->write_string(0, 3, get_string('country','block_classroom'));
	$worksheet->write_string(0, 4, get_string('legacycompany','block_classroom'));
    $worksheet->write_string(0, 5, get_string('bu','block_classroom'));
	$worksheet->write_string(0, 6, get_string('budescription','block_classroom'));
    $worksheet->write_string(0, 7, get_string('ru','block_classroom'));
	$worksheet->write_string(0, 8, get_string('managerportalid','block_classroom'));
	$worksheet->write_string(0, 9, get_string('managerfirstname','block_classroom'));
	$worksheet->write_string(0, 10, get_string('managerlastname','block_classroom'));
	$worksheet->write_string(0, 11, get_string('hireddate','block_classroom'));
	$worksheet->write_string(0, 12, get_string('requiredtraininghours','block_classroom'));
	$worksheet->write_string(0, 13, get_string('aquiredtraininghours','block_classroom'));
	
  

    if (!empty($dates)) {
        $i = 0;
        foreach ($dates as $date) {
            $i++;
			
        $worksheet->write_string($i, 0, $date->PortalID);
        $worksheet->write_string($i, 1, $date->EmpFirstName);
		$worksheet->write_string($i, 2, $date->EmpLastName);
		$worksheet->write_string($i, 3, $date->EmpCountry);
		$worksheet->write_string($i, 4, $date->LegacyCompany);
		$worksheet->write_string($i, 5, $date->BU);
		$worksheet->write_string($i, 6, $date->BUDescription);
		$worksheet->write_string($i, 7, $date->RU);
		$worksheet->write_string($i, 8, $date->ManagerPortalID);
		$worksheet->write_string($i, 9, $date->ManagerFirstName);
		$worksheet->write_string($i, 10, $date->ManagerLastName);
		$worksheet->write_string($i, 11, $date->HiredDate);
		$worksheet->write_string($i, 12, $date->RequiredTrainingHours);
		$worksheet->write_string($i, 13, $date->AquiredTrainingHours);
		
			      
        }
    }

    $workbook->close();
}
if ('export' == $action) {
    export_spreadsheet($dates, $format,true);
    exit;
}

$pagetitle = 'Training History';
$navlinks[] = array('name' => $pagetitle, 'link' => '', 'type' => 'activityinstance');
$navigation = build_navigation($navlinks);
print_header_simple($pagetitle, '', $navigation);
global $CFG;

if ($nodata > 1) {
	//print_paging_bar($nodata, $page, $perpage, "?perpage=$perpage&amp;");
print '<h3>'.'Click on export to get the Training History Report of Users:'.'</h3>';
	echo '<p>&nbsp;</p>';
    //print_paging_bar($nodata, $page, $perpage, "perpage=$perpage&amp;");

	
	print '<h3>'.get_string('exportrecords', 'block_classroom').'</h3>';
    print '<form method="post" action=""><p>';
	print '<input type="hidden" name="startyear" value="'.$startyear.'" />';
    print '<input type="hidden" name="startmonth" value="'.$startmonth.'" />';
    print '<input type="hidden" name="startday" value="'.$startday.'" />';
    print '<input type="hidden" name="endyear" value="'.$endyear.'" />';
    print '<input type="hidden" name="endmonth" value="'.$endmonth.'" />';
    print '<input type="hidden" name="endday" value="'.$endday.'" />';
    print '<input type="hidden" name="sortby" value="'.$sortby.'" />';
    print '<input type="hidden" name="action" value="export" />';

    print get_string('format', 'classroom').':&nbsp;';
    print '<select name="format">';
    print '<option value="excel" selected="selected">'.get_string('excelformat', 'classroom').'</option>';
    print '<option value="ods">'.get_string('odsformat', 'classroom').'</option>';
    print '</select>';

    print ' <input type="submit" value="'.get_string('exporttofile', 'classroom').'" /></p></form>';
	// echo '<p>&nbsp;</p>';
	// echo '<p>&nbsp;</p>';
	    // print_table($dates,true);
}else {
    print '<p>'.get_string('norecords', 'block_classroom').'</p>';
}
print_footer();
?>
