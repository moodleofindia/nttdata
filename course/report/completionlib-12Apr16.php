<?php

	require_once '../../config.php';
    require_once($CFG->libdir.'/gradelib.php');


    function getcompletion($userid) {
        global $CFG;
		$recordsCompleted = get_record_sql("SELECT count(c.id) as id,
		(SELECT FROM_UNIXTIME(max(mdl_grade_grades.timemodified),'%b %d %Y')
		from mdl_grade_grades JOIN mdl_grade_items
		ON mdl_grade_items.id = mdl_grade_grades.itemid
		where mdl_grade_items.courseid = c.id and mdl_grade_grades.userid=u.id
		)as 'CompletedDate'
		FROM mdl_course AS c JOIN mdl_context AS ctx
		ON c.id = ctx.instanceid and c.visible=1 JOIN mdl_role_assignments AS ra
		ON ra.contextid = ctx.id JOIN mdl_user AS u
		ON u.id = ra.userid JOIN mdl_grade_grades AS gg
		ON gg.userid = u.id JOIN mdl_grade_items AS gi
		ON gi.id = gg.itemid and gg.finalgrade >= gi.grademax
		JOIN mdl_course_categories AS cc ON cc.id = c.category and cc.path not like('/49%') and cc.path not like('/154%')
		WHERE  gi.courseid = c.id and u.id=$userid  AND gi.itemtype = 'course' order by gg.id desc");
		
		$recordsSkillSoft= get_records_sql("SELECT sc.id
		FROM mdl_skillsoft_au_track AS st
		JOIN mdl_skillsoft AS sc ON sc.id=st.skillsoftid
		JOIN mdl_course AS c ON c.id=sc.course
		JOIN mdl_course_categories AS cc ON cc.id = c.category 
		JOIN mdl_user AS u ON st.userid=u.id
		join mdl_skillsoft_au_track AS at
		on at.userid=u.id and at.skillsoftid=sc.id and at.element='[SUMMARY]completed'
		where st.element='[CORE]lesson_status' and st.value in ('completed','passed') and u.id=$userid
		group by sc.assetid
		order by st.timemodified desc");

		$recordsILTCompleted = get_record_sql("SELECT count(distinct s.id) as id
		from mdl_classroom_sessions s
		join mdl_classroom_sessions_dates on s.id=mdl_classroom_sessions_dates.sessionid
		and s.datetimeknown=1 and s.status='Completed'
		join mdl_classroom on mdl_classroom.id=s.classroom
		join mdl_classroom_submissions on mdl_classroom_submissions.sessionid = s.id
		and mdl_classroom_submissions.attend=1
		join mdl_course c on c.id=mdl_classroom.course
		JOIN mdl_course_categories AS cc ON cc.id = c.category and cc.path like('/154%')
		join mdl_user u on u.id=mdl_classroom_submissions.userid and u.id=$userid");
			$completed = 0;
			$coursecount=0;
			if ($recordsSkillSoft) {
        foreach($recordsSkillSoft as $record) {

					$coursecount ++;
			}

		}
		$completed = $recordsILTCompleted->id+$recordsCompleted->id+$coursecount;
        return $completed;
    }
	
	
	 function getcompletionDate($userid,$sd,$ed) {
        global $CFG;
		$recordsCompleted = get_records_sql("SELECT distinct c.id as id,
		(SELECT FROM_UNIXTIME(max(mdl_grade_grades.timemodified),'%b %d %Y')
		from mdl_grade_grades JOIN mdl_grade_items
		ON mdl_grade_items.id = mdl_grade_grades.itemid
		where mdl_grade_items.courseid = c.id and mdl_grade_grades.userid=u.id
	  and mdl_grade_grades.timemodified > unix_timestamp('$sd')
		and mdl_grade_grades.timemodified < unix_timestamp('$ed')
		)as 'CompletedDate'
		FROM mdl_course AS c JOIN mdl_context AS ctx
		ON c.id = ctx.instanceid and c.visible=1 JOIN mdl_role_assignments AS ra
		ON ra.contextid = ctx.id JOIN mdl_user AS u
		ON u.id = ra.userid JOIN mdl_grade_grades AS gg
		ON gg.userid = u.id JOIN mdl_grade_items AS gi
		ON gi.id = gg.itemid and gg.finalgrade >= gi.grademax
		JOIN mdl_course_categories AS cc ON cc.id = c.category and cc.path not like('/49%') and cc.path not like('/154%')
		WHERE  gi.courseid = c.id and u.id=$userid  AND gi.itemtype = 'course'");
		
		$recordsSkillSoft= get_records_sql("SELECT sc.id
		FROM mdl_skillsoft_au_track AS st
		JOIN mdl_skillsoft AS sc ON sc.id=st.skillsoftid
		JOIN mdl_course AS c ON c.id=sc.course
		JOIN mdl_course_categories AS cc ON cc.id = c.category 
		JOIN mdl_user AS u ON st.userid=u.id
		join mdl_skillsoft_au_track AS at
		on at.userid=u.id and at.skillsoftid=sc.id and at.element='[SUMMARY]completed'
		and at.value > unix_timestamp('$sd')
		and at.value < unix_timestamp('$ed')
		where st.element='[CORE]lesson_status' and st.value in ('completed','passed') and u.id=$userid
		group by sc.assetid
		order by st.timemodified desc");

		$recordsILTCompleted = get_record_sql("SELECT count(distinct s.id) as id
		from mdl_classroom_sessions s
		join mdl_classroom_sessions_dates on s.id=mdl_classroom_sessions_dates.sessionid
		and s.datetimeknown=1 and s.status='Completed'
		and mdl_classroom_sessions_dates.timestart > unix_timestamp('$sd')
		and mdl_classroom_sessions_dates.timefinish < unix_timestamp('$ed')
		join mdl_classroom on mdl_classroom.id=s.classroom
		join mdl_classroom_submissions on mdl_classroom_submissions.sessionid = s.id
		and mdl_classroom_submissions.attend=1
		join mdl_course c on c.id=mdl_classroom.course
		JOIN mdl_course_categories AS cc ON cc.id = c.category and cc.path like('/154%')
		join mdl_user u on u.id=mdl_classroom_submissions.userid and u.id=$userid");
		$completed =0;
		$coursecount=0;
		if ($recordsCompleted) {
        foreach($recordsCompleted as $record) {

				if($record->CompletedDate)
				{
					$coursecount ++;
				}
                continue;
			}

		}

		if ($recordsSkillSoft) {
        foreach($recordsSkillSoft as $record) {

					$coursecount ++;
			}

		}		
			$completed=$recordsILTCompleted->id+$coursecount;
			return $completed;
    }
	

?>
