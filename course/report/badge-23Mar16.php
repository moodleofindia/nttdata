<?php
require_once '../../config.php';
require_login();

require_once 'completionlib.php';
require_once($CFG->libdir . '/game/badgelib.php');
require_once($CFG->libdir . '/game/pointlib.php');


$userid=$USER->id;
$return=process_system_badges($userid);
$yearlabel="";



$pagetitle = 'Badges';
$navlinks[] = array('name' => $pagetitle, 'link' => '', 'type' => 'activityinstance');
$navigation = build_navigation($navlinks);
print_header_simple($pagetitle, '', $navigation);

?>

<style type="text/css">


.usual li { list-style:none; float:left; }
.usual ul a {
  display:block;
  padding:6px 10px;
  text-decoration:none!important;
  margin:1px;
  margin-left:0;
  font:10px Verdana;
  color:#FFF;
  background:#444;
}
.usual ul a:hover {
  color:#0000;
  background:#9999FF;
  }
.usual ul a.selected {
  margin-bottom:0;
  color:#ffff;
  background:#6666FF;
  border-bottom:1px solid snow;
  cursor:default;
  }
.usual div {
  padding:10px 10px 8px 10px;
  *padding-top:3px;
  *margin-top:-15px;
  clear:left;
  font:10pt Georgia;
}
.usual div a { color:#000; font-weight:bold; }

#usual2 { background:#0A0A0A; border:1px solid #1A1A1A; }
#usual2 a { background:#222; }
#usual2 a:hover { background:#000; }
#usual2 a.selected { background:snow; }
#tabs3 { background:#FF9; }

.tabled {
	font-family:Arial, Helvetica, sans-serif;
	font-size:10px;

}

.tabled tr td {

}



#box-table-b
{
	font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
	font-size: 12px;
	margin: 20px;
	text-align: center;
	border-collapse: collapse;
	border-top: 8px solid #9baff1;
	border-bottom: 8px solid #9baff1;
}
#box-table-b th
{
	font-size: 13px;
	font-weight: normal;
	padding: 8px;
	background: #D1DEE6;
	border-right: 1px solid #9baff1;
	border-left: 1px solid #9baff1;
	color: #039;
}
#box-table-b td
{
	padding: 8px;
	background: #F0F3F5; 
	border-right: 1px solid #aabcfe;
	border-left: 1px solid #aabcfe;
	color: #669;
}


.tabler a:link {
	color: #666;
	font-weight: bold;
	text-decoration:none;
}
.tabler a:visited {
	color: #999999;
	font-weight:bold;
	text-decoration:none;
}
.tabler a:active,
.tabler a:hover {
	color: #bd5a35;
	text-decoration:underline;
}
.tabler {
	font-family:Arial, Helvetica, sans-serif;
	color:#666;
	font-size:11px;
	text-shadow: 1px 1px 0px #fff;
	background:#eaebec;
	margin:20px;
	border:#ccc 1px solid;

	-moz-border-radius:3px;
	-webkit-border-radius:3px;
	border-radius:3px;

	-moz-box-shadow: 0 1px 2px #d1d1d1;
	-webkit-box-shadow: 0 1px 2px #d1d1d1;
	box-shadow: 0 1px 2px #d1d1d1;
}
.tabler th {
	padding:21px 25px 22px 25px;
	border-top:1px solid #fafafa;
	border-bottom:1px solid #e0e0e0;

	background: #ededed;
	background: -webkit-gradient(linear, left top, left bottom, from(#ededed), to(#ebebeb));
	background: -moz-linear-gradient(top,  #ededed,  #ebebeb);
}
.tabler tfoot {
	padding:21px 25px 22px 25px;
	border-top:1px solid #fafafa;
	border-bottom:1px solid #e0e0e0;

	background: #ededed;
	background: -webkit-gradient(linear, left top, left bottom, from(#ededed), to(#ebebeb));
	background: -moz-linear-gradient(top,  #ededed,  #ebebeb);
}
.tabler th:first-child{
	text-align: left;
	padding-left:20px;
}
.tabler tr:first-child th:first-child{
	-moz-border-radius-topleft:3px;
	-webkit-border-top-left-radius:3px;
	border-top-left-radius:3px;
}
.tabler tr:first-child th:last-child{
	-moz-border-radius-topright:3px;
	-webkit-border-top-right-radius:3px;
	border-top-right-radius:3px;
}
.tabler tr{
	text-align: center;
	padding-left:20px;
}
.tabler tr td:first-child{
	text-align: left;
	padding-left:20px;
	border-left: 0;
}
.tabler tr td {
	padding:14px;
	border-top: 1px solid #ffffff;
	border-bottom:1px solid #e0e0e0;
	border-left: 1px solid #e0e0e0;
	
	background: #fafafa;
	background: -webkit-gradient(linear, left top, left bottom, from(#fbfbfb), to(#fafafa));
	background: -moz-linear-gradient(top,  #fbfbfb,  #fafafa);
}
.tabler tr.even td{
	background: #f6f6f6;
	background: -webkit-gradient(linear, left top, left bottom, from(#f8f8f8), to(#f6f6f6));
	background: -moz-linear-gradient(top,  #f8f8f8,  #f6f6f6);
}
.tabler tr:last-child td{
	border-bottom:0;
}
.tabler tr:last-child td:first-child{
	-moz-border-radius-bottomleft:3px;
	-webkit-border-bottom-left-radius:3px;
	border-bottom-left-radius:3px;
}
.tabler tr:last-child td:last-child{
	-moz-border-radius-bottomright:3px;
	-webkit-border-bottom-right-radius:3px;
	border-bottom-right-radius:3px;
}
.tabler tr:hover td{
	background: #f2f2f2;
	background: -webkit-gradient(linear, left top, left bottom, from(#f2f2f2), to(#f0f0f0));
	background: -moz-linear-gradient(top,  #f2f2f2,  #f0f0f0);	
}

.tableb { 
	font-family:Arial, Helvetica, sans-serif;
	color:#666;
	font-size:11px;
	text-shadow: 1px 1px 0px #fff;
	background:#eaebec;
	margin:20px;
	border:#ccc 1px solid;

	-moz-border-radius:3px;
	-webkit-border-radius:3px;
	border-radius:3px;

	-moz-box-shadow: 0 1px 2px #d1d1d1;
	-webkit-box-shadow: 0 1px 2px #d1d1d1;
	box-shadow: 0 1px 2px #d1d1d1;
}
		
.tableb td, th { 
	border: 1px solid transparent; /* No more visible border */
	height: 30px; 
	transition: all 0.3s;  /* Simple transition for hover effect */
}
		
	
.tableb td {
	padding:14px;
	border-top: 1px solid #ffffff;
	border-bottom:1px solid #e0e0e0;
	border-left: 1px solid #e0e0e0;	
	background: #fafafa;
	background: -webkit-gradient(linear, left top, left bottom, from(#fbfbfb), to(#fafafa));
	background: -moz-linear-gradient(top,  #fbfbfb,  #fafafa);
	text-align: center;
}

/* Cells in even rows (2,4,6...) are one color */		
.tableb tr:nth-child(even) td { background: #F1F1F1; }   

/* Cells in odd rows (1,3,5...) are another (excludes header cells)  */		
.tableb tr:nth-child(odd) td { background: #FEFEFE; }  
		
.tableb tr td:hover { background: #00FF99; color: #FFF; } /* Hover cell effect! */


#rounded-corner
{
	font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
	font-size: 12px;
	margin: 45px;
	width: 250px;
	text-align: left;
	border-collapse: collapse;
}
#rounded-corner thead th.rounded-company
{
	background: #b9c9fe url('table-images/left.png') left -1px no-repeat;
}
#rounded-corner thead th.rounded-q4
{
	background: #b9c9fe url('table-images/right.png') right -1px no-repeat;
}
#rounded-corner th
{
	padding: 8px;
	font-weight: normal;
	font-size: 13px;
	color: #039;
	background: #b9c9fe;
}
#rounded-corner td
{
	padding: 8px;
	background: #e8edff;
	border-top: 1px solid #fff;
	color: #669;
}
#rounded-corner tfoot td.rounded-foot-left
{
	background: #e8edff url('table-images/botleft.png') left bottom no-repeat;
}
#rounded-corner tfoot td.rounded-foot-right
{
	background: #e8edff url('table-images/botright.png') right bottom no-repeat;
}
#rounded-corner tbody tr:hover td
{
	background: #d0dafd;
}

a.tooltip {outline:none; }
a.tooltip strong {line-height:30px;}
a.tooltip:hover {text-decoration:none;} 
a.tooltip span {
	padding: 10px;
    z-index:10;display:none; padding:14px 20px;
    margin-top:-30px; margin-left:28px;
    width:240px; line-height:16px;
}
a.tooltip:hover span{
	padding: 10px;
    display:inline; position:absolute; color:#111;
    border:1px solid #DCA; background:#fffAF0;}
.callout {z-index:20;position:absolute;top:30px;border:0;left:-12px;}
    
/*CSS3 extras*/
a.tooltip span
{
    border-radius:4px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
        
    -moz-box-shadow: 5px 5px 8px #CCC;
    -webkit-box-shadow: 5px 5px 8px #CCC;
    box-shadow: 5px 5px 8px #CCC;
}
</style>
<?php




 
 //Gamification Computing the total points and calculations 
$points = point_count_user($userid);
/* Count for levels
$level=get_record_sql("select max(level) as count from mdl_gamification_levels where points <=$points");
	if(!$level->count)
	{
	$level->count=0;
	}
	$level->count=2;
$nextl=$level->count+1;
$nextlevel=get_record_sql("select points as count from mdl_gamification_levels where level=$nextl");
$missingpoints=$nextlevel->count-$points;

/*end of the calculations */


 ?>



<table align="center"  cellspacing=0 cellpadding=0
 style='float:center;border-collapse:collapse;border:none;mso-border-alt:solid windowtext .5pt;
 mso-yfti-tbllook:1184;mso-padding-alt:0in 5.4pt 0in 5.4pt'>
 <tr >
  <td  valign=top style='padding:0in 5.4pt 0in 5.4pt'>
  <table  align="center"  cellspacing=0 cellpadding=0
   style='border-collapse:collapse;border:none;mso-padding-alt:0in 5.4pt 0in 5.4pt'>
   <tr  align="center" >
    <td width=100 align="center"  valign="center" rowspan=2 >
      <?php echo '<a href="'.$CFG->wwwroot.'/user/view.php?id='.$userid.'&amp;course='.$COURSE->id.'"><img height="150" width="150" src="'.$CFG->wwwroot.'/user/pix.php?file=/'.$userid.'/f1.jpg" width="80px" height="80px" align="center" title="'.$USER->firstname.' '.$USER->lastname.'" alt="'.$USER->firstname.' '.$USER->lastname.'" /></a>'; ?>

	<?php


		
	?>

	</td>
	<td></td>
    <td></td>
	<td valign=top align=right style='padding:0in 5.4pt 0in 5.4pt;
    height:6.75pt'>
<!-- Gamification table to show the level 
<?php
/*
if($level->count==1)
{
echo '<img src="icon/levelcupreport1.png">';
}
if($level->count==2)
{
echo '<img src="icon/levelcupreport2.png">';
}
if($level->count==3)
{
echo '<img src="icon/levelcupreport3.png">';
}
if($level->count==4)
{
echo '<img src="icon/levelcupreport4.png">';
}
if($level->count==5)
{
echo '<img src="icon/levelcupreport5.png">';
}
*/
?> -->
	</td>
   </tr>
   <tr style='mso-yfti-irow:1;height:6.75pt'>
    <td  colspan=3 valign=top align=right style='padding:0in 5.4pt 0in 5.4pt;
    height:6.75pt'>
<!--Status message to the user -->
	<p align="left"><span style='font-size:12.0pt;line-height:115%;font-family:
"Comic Sans MS";color:#17375E;mso-themecolor:text2;mso-themeshade:191;
mso-style-textfill-fill-color:#17375E;mso-style-textfill-fill-themecolor:text2;
mso-style-textfill-fill-alpha:100.0%;mso-style-textfill-fill-colortransforms:
lumm=75000'>Dear <?php echo $USER->firstname.' '.$USER->lastname; ?> <br/>You have a total of </span><span style='font-size:20.0pt;
line-height:115%;font-family:"Comic Sans MS";color:#E46C0A;mso-themecolor:accent6;
mso-themeshade:191;mso-style-textfill-fill-color:#E46C0A;mso-style-textfill-fill-themecolor:
accent6;mso-style-textfill-fill-alpha:100.0%;mso-style-textfill-fill-colortransforms:
lumm=75000'><?php echo $points; ?></span><span style='font-size:12.0pt;line-height:115%;font-family:
"Comic Sans MS";color:#17375E;mso-themecolor:text2;mso-themeshade:191;
mso-style-textfill-fill-color:#17375E;mso-style-textfill-fill-themecolor:text2;
mso-style-textfill-fill-alpha:100.0%;mso-style-textfill-fill-colortransforms:
lumm=75000'> training points and you have earned <span style='font-size:20.0pt;
line-height:115%;font-family:"Comic Sans MS";color:#E46C0A;mso-themecolor:accent6;
mso-themeshade:191;mso-style-textfill-fill-color:#E46C0A;mso-style-textfill-fill-themecolor:
accent6;mso-style-textfill-fill-alpha:100.0%;mso-style-textfill-fill-colortransforms:
lumm=75000'><?php echo badge_count_user($userid);?></span> exciting badges. </span><br/><span style='font-size:16.0pt;
line-height:115%;font-family:"Comic Sans MS";color:#558ED5;mso-themecolor:text2;
mso-themetint:153;mso-style-textfill-fill-color:#558ED5;mso-style-textfill-fill-themecolor:
text2;mso-style-textfill-fill-alpha:100.0%;mso-style-textfill-fill-colortransforms:
"lumm=60000 lumo=40000"'>
<?php
if ($records=badge_select_latest($userid)) {

  foreach ($records as $badge) 
	{
	print '<table >
	<tr><td><img src="icon/congrats.png">
	</td><td><img src="'.$CFG->wwwroot.'/file.php/1/badges/'.$badge->badge.'" alt="" width="100" height="100"></td>
	<td width="400"><p align="left">You have been recently awarded the '.$badge->name.'. </p><p align="left">'.$badge->comments.'</p></td>
	</tr></table>';					
	}

}
?>
</span><span style='font-size:12.0pt;
line-height:115%;font-family:"Comic Sans MS";color:#17375E;mso-themecolor:text2;
mso-themeshade:191;mso-style-textfill-fill-color:#17375E;mso-style-textfill-fill-themecolor:
text2;mso-style-textfill-fill-alpha:100.0%;mso-style-textfill-fill-colortransforms:
lumm=75000'><o:p></o:p></span>

</p></br>

<!-- end of status message -->

  </td>
 </tr>
</table>
<?php 
$badges = array();
$badgecounter=0;
if ($records=badge_select($userid)) {

        foreach($records as $record) {


                $badges[] = $record;
					 $badgecounter += 1;
                continue;

        }
}
?>

<span style='font-size:16.0pt;
line-height:115%;font-family:"Comic Sans MS";color:#558ED5;mso-themecolor:text2;
mso-themetint:153;mso-style-textfill-fill-color:#558ED5;mso-style-textfill-fill-themecolor:
text2;mso-style-textfill-fill-alpha:100.0%;mso-style-textfill-fill-colortransforms:
"lumm=60000 lumo=40000"'><?php echo $USER->firstname."'s"; ?> Badges(<?php echo $badgecounter;?>) </span>
<?php


	$row=0;
if($badgecounter>0)
{
   print'<table border="0" class="tableb"  width="100%" cellpadding="3" cellspacing="5">';

    foreach ($badges as $badge) 
	{
		if($row % 4 ==0)
		{
		print'<tr><td>';
		print'<a href="#" class="tooltip">
		<img src="'.$CFG->wwwroot.'/file.php/1/badges/'.$badge->badge.'" alt="" width="100" height="100">
			<span>
			<p align="left">'.$badge->comments.'</p>
			</span>
        </a>';		
		print'</td>';
					
		}
		else
		{
		print'<td>';
		print'<a href="#" class="tooltip">
		<img src="'.$CFG->wwwroot.'/file.php/1/badges/'.$badge->badge.'" alt="" width="100" height="100">
			<span>
			<p align="left">'.$badge->comments.'</p>
			</span>
        </a>';		
		print'</td>';
		
		}
		$row++;
	}
	print'</tr></table>';
}
?>
<span style='font-size:14.0pt;
line-height:115%;font-family:"Comic Sans MS";color:#558ED5;mso-themecolor:text2;
mso-themetint:153;mso-style-textfill-fill-color:#558ED5;mso-style-textfill-fill-themecolor:
text2;mso-style-textfill-fill-alpha:100.0%;mso-style-textfill-fill-colortransforms:
"lumm=60000 lumo=40000"'>Below are some exciting Badges you can earn</span><br/>
In addition to these standard badges, you may also earn badges on completing specific courses and learning modules.<br/>
We will roll out new badges every quarter or upon request from program owners.
<?php

   print'<table border="0" class="tabler"  width="100%" cellpadding="3" cellspacing="5">
     <tr>
		<td>';
print'<p>Gold Club Member </p>';
		print'<a href="#" class="tooltip">
		<img src="'.$CFG->wwwroot.'/file.php/1/badges/Gold-club.png" alt="" width="100" height="100">
			<span>
			<p align="left">Earn this badge by becoming a Gold Club member.</p>
			</span>
        </a>';		
		print'</td>
		
		<td>';
print'<p>Gold Club Graduate </p>';
		print'<a href="#" class="tooltip">
		<img src="'.$CFG->wwwroot.'/file.php/1/badges/Gold club.png" alt="" width="100" height="100">
			<span>
			<p align="left">Earn this badge by completing the Gold Club Development Plan.</p>
			</span>
			</a>';

		print'</td>
		<td>';
		//
print'<p>Star Club Member </p>';		
		print'<a href="#" class="tooltip">
		<img src="'.$CFG->wwwroot.'/file.php/1/badges/Star-club.png" alt="" width="100" height="100">
			<span>
			<p align="left">Earn this badge by becoming a Star Club member.</p>
			</span>
			</a>';

		print'</td>
		<td>';
		
print'<p>Star Club Graduate</p>';	
		print'<a href="#" class="tooltip">
		<img src="'.$CFG->wwwroot.'/file.php/1/badges/Star club.png" alt="" width="100" height="100">
			<span>
			<p align="left">Earn this badge by completing the Star Club Development Plan.</p>
			</span>
			</a>';

		print'</td>
		
	</tr>
   <tr>
		<td>';
print'<p>KNOW THE CODE BADGE</p>';
		print'<a href="#" class="tooltip">
		<img src="'.$CFG->wwwroot.'/file.php/1/badges/cobc2015_locked.png" alt="" width="100" height="100">
			<span>
			<p align="left">Earn this badge by completing CoBC 2015.</p>
			</span>
        </a>';		
		print'</td>
		
		<td>';
print'<p>Time & Expense BADGE</p>';
		print'<a href="#" class="tooltip">
		<img src="'.$CFG->wwwroot.'/file.php/1/badges/time&ex_locked.png" alt="" width="100" height="100">
			<span>
			<p align="left">Earn this badge by completing Time & Expense.</p>
			</span>
			</a>';

		print'</td>
		<td>';
		//
print'<p>PME(NA) BADGE</p>';		
		print'<a href="#" class="tooltip">
		<img src="'.$CFG->wwwroot.'/file.php/1/badges/pmenorthamerica_locked.png" alt="" width="100" height="100">
			<span>
			<p align="left">Earn this badge by completing People Management Essentials (NA).</p>
			</span>
			</a>';

		print'</td>
		<td>';
		
print'<p>PME(India) BADGE</p>';	
		print'<a href="#" class="tooltip">
		<img src="'.$CFG->wwwroot.'/file.php/1/badges/pmeindia_locked.png" alt="" width="100" height="100">
			<span>
			<p align="left">Earn this badge by completing People Management Essentials (India).</p>
			</span>
			</a>';

		print'</td>
		
	</tr>
	<tr>
		<td>';
print'<p>IDENTITY BADGE</p>';
		print'<a href="#" class="tooltip">
		<img src="'.$CFG->wwwroot.'/file.php/1/badges/identity_locked.png" alt="" width="100" height="100">
			<span>
			<p align="left">Earn this badge by keeping your profile up to date. Update your time zone and picture.</p>
			</span>
        </a>';		
		print'</td>
		
		<td>';
print'<p>SCOUT BADGE</p>';
		print'<a href="#" class="tooltip">
		<img src="'.$CFG->wwwroot.'/file.php/1/badges/scout_locked.png" alt="" width="100" height="100">
			<span>
			<p align="left">Are you a frequent visitor to LMS? Earn the badge by visiting courses across weeks.</p>
			</span>
			</a>';

		print'</td>
		<td>';
		//
print'<p>VISION AND VALUES 2015</p>';		
		print'<a href="#" class="tooltip">
		<img src="'.$CFG->wwwroot.'/file.php/1/badges/ValuesWeek2015.png" alt="" width="100" height="100">
			<span>
			<p align="left">Earn this badge by completing NTT DATA Values Week 2015 program.</p>
			</span>
			</a>';

		print'</td>
		<td>';
		
print'<p>Global Information Security</p>';	
		print'<a href="#" class="tooltip">
		<img src="'.$CFG->wwwroot.'/file.php/1/badges/security_locked_2015.png" alt="" width="100" height="100">
			<span>
			<p align="left">Earn this badge by completing NTT DATA Global Information Security and Privacy Awareness Training 2015.</p>
			</span>
			</a>';

		print'</td>
		
	</tr>
	<tr>
		<td>';
print'<p>SMART BADGE</p>';		
		print'<a href="#" class="tooltip">
		<img src="'.$CFG->wwwroot.'/file.php/1/badges/smartstudent_locked.png" alt="" width="100" height="100">
			<span>
			<p align="left">You can earn this badge by completing +5000 pts.</p>
			</span>
			</a>';

		print'</td>
		<td>';
print'<p>NLCI BRONZE TROPHY</p>';
		print'<a href="#" class="tooltip">
		<img src="'.$CFG->wwwroot.'/file.php/1/badges/nlcibronze_locked.png" alt="" width="100" height="100">
			<span>
			<p align="left">Get this trophy by completing at least 2 NLCI certifications.</p>
			</span>
			</a>';

		print'</td>
		<td>';
		//
print'<p>NLCI SILVER TROPHY</p>';
		print'<a href="#" class="tooltip">
		<img src="'.$CFG->wwwroot.'/file.php/1/badges/nlcisilver_locked.png" alt="" width="100" height="100">
			<span>
			<p align="left">Get this trophy by completing at least 4 NLCI certifications.</p>
			</span>
			</a>';

		print'</td>
		<td>';
		//

print'<p>CSR BADGE</p>';
		print'<a href="#" class="tooltip">
		<img src="'.$CFG->wwwroot.'/file.php/1/badges/csr_locked.png" alt="" width="100" height="100">
		<span>
			<p align="left">Did you take part in any CSR event if yes, you earn this badge.</p>
			</span>
			</a>';

		print'</td>
	</tr>

</table></table>';
  ?>

    

<?php


/////////////


print_footer();

?>
