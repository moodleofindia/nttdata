<?PHP 
      
$string['formatmonths'] = 'Monthly format';
$string['monthhide'] = 'Hide months';
$string['monthlyoutline'] = 'Monthly outline';
$string['monthshow'] = 'Show months';
$string['namemonths'] = 'Month';
$string['showallmonths'] = 'Show all months';
$string['showonlymonth'] = 'Show only month';

?>
