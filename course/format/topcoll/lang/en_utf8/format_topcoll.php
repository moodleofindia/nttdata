<?php // $Id: format_topcoll.php,v 1.2.2.3 2009/08/21 19:13:38 gb2048 Exp $

// Used by the Moodle Core for identifing the format and displaying in the list of formats for a course in its settings.
$string['nametopcoll']='Collapsed Topics';

$string['formattopcoll']='Collapsed Topics';

// Used in format.php
$string['topcolltoggle']='';

?>
