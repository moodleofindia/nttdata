<?php  // $Id: index.php,v 1.201.2.10 2009/04/25 21:18:24 stronk7 Exp $
       // index.php - the front page.

    require_once("../config.php");
    require_once("lib.php");
    require_once("../lib/blocklib.php");

  
    if ($CFG->forcelogin) {
        require_login();
    } else {
        user_accesstime_log();
    }
/*
    if ($CFG->rolesactive) { // if already using roles system
        if (has_capability('moodle/site:config', get_context_instance(CONTEXT_SYSTEM))) {
            if (moodle_needs_upgrading()) {
                redirect($CFG->wwwroot .'/'. $CFG->admin .'/index.php');
            }
        } else if (!empty($CFG->mymoodleredirect)) {    // Redirect logged-in users to My Moodle overview if required
            if (isloggedin() && $USER->username != 'guest') {
                redirect($CFG->wwwroot .'/my/index.php');
            }
        }
    } else { // if upgrading from 1.6 or below
        if (isadmin() && moodle_needs_upgrading()) {
            redirect($CFG->wwwroot .'/'. $CFG->admin .'/index.php');
        }
    }
*/

    if (get_moodle_cookie() == '') {
        set_moodle_cookie('nobody');   // To help search for cookies on login page
    }

    if (!empty($USER->id)) {
        add_to_log(SITEID, 'course', 'view', 'view.php?id='.SITEID, SITEID);
    }


    print_header($SITE->fullname, $SITE->fullname, 'home', '',
                 '<meta name="description" content="'. strip_tags(format_text($SITE->summary, FORMAT_HTML)) .'" />',
                 true, '', user_login_string($SITE).$langmenu);

?>

<br/>
<table border="1" cellspacing="1" cellpadding="1" width="100%"><tbody><tr><td valign="top" width="25%"><p align="center"><img style="129px" title="Skillsoft-logo" border="0" hspace="0" alt="Skillsoft-logo" src="https://lmsqa.portal.nttdatainc.com/file.php/1/Skillsoft-logo.jpg" width="311" height="129" /></p></td><td valign="top" width="75%"><p align="left"><font size="2">The courses listed in this part of the catalog are part of the external training offerings. This means that you need specific rights or accounts to access the material in this part of the catalog. At this point, Skillsoft is our largest external training provider and the course offerings from Skillsoft are integrated into our LMS, allowing users to take the courses without logging into another system. </font></p><p align="left"><font size="2">You must have a Skillport license in order to complete the training here. Please visit the </font><a href="https://groups.portal.keane.com/sites/Training/SP/Pages/default.aspx"><font size="2">SkillPort page on the Global Training Portal</font></a><font size="2"> for more information about the Skillsoft program. </font></p><p align="left"><font size="2">Not sure if you have a license or not? Simply launch a course. If you have a license, you will gain access to your desired course. Otherwise, you will be prompted to request a license and given the </font><a href="https://groups.portal.keane.com/sites/Training/SP/Pages/default.aspx"><font size="2">guidelines for requesting access</font></a><font size="2">. </font></p><p align="left"><font size="2">Visit the </font><a href="https://lmsqa.portal.nttdatainc.com/LMSfaq.html"><font size="2">FAQ section here in the LMS</font></a><font size="2"> for more information about accessing Skillsoft courses. Answers to specific questions about what is contained in Skillport are available here in the </font><a href="https://groups.portal.keane.com/sites/Training/SP/Pages/default.aspx"><font size="2">FAQ posted on the Skillport portal page</font></a><font size="2">. </font></p><p align="left"><font size="2" color="red">We are scheduled to perform an upgrade to the Skillport system during the Atlantic Time Zone work day on January 10, 2012.  The system will go down in the early morning Atlantic time and will remain unavailable until the upgrade is complete.</font><font size="2" color="red">We apologize in advance for any inconvenience.  Thank you for planning your training access around this important upgrade.</font></p></td></tr></tbody></table></p></div><table border="0" cellspacing="2" cellpadding="4" >
</table>
<table id="layout-table" summary="layout">

  <tr>

  <td>

   <?php
  include('accordion_skillsoft_menu.php');
  ?>
  </td>
  </tr>
  </table>
 

<?php
    print_footer('home');     // Please do not modify this line
?>
