<?php  // $Id: index.php,v 1.201.2.10 2009/04/25 21:18:24 stronk7 Exp $
       // index.php - the front page.

    require_once("../config.php");
    require_once("lib.php");
    require_once("../lib/blocklib.php");

  
    if ($CFG->forcelogin) {
        require_login();
    } else {
        user_accesstime_log();
    }
/*
    if ($CFG->rolesactive) { // if already using roles system
        if (has_capability('moodle/site:config', get_context_instance(CONTEXT_SYSTEM))) {
            if (moodle_needs_upgrading()) {
                redirect($CFG->wwwroot .'/'. $CFG->admin .'/index.php');
            }
        } else if (!empty($CFG->mymoodleredirect)) {    // Redirect logged-in users to My Moodle overview if required
            if (isloggedin() && $USER->username != 'guest') {
                redirect($CFG->wwwroot .'/my/index.php');
            }
        }
    } else { // if upgrading from 1.6 or below
        if (isadmin() && moodle_needs_upgrading()) {
            redirect($CFG->wwwroot .'/'. $CFG->admin .'/index.php');
        }
    }
*/

    if (get_moodle_cookie() == '') {
        set_moodle_cookie('nobody');   // To help search for cookies on login page
    }

    if (!empty($USER->id)) {
        add_to_log(SITEID, 'course', 'view', 'view.php?id='.SITEID, SITEID);
    }


    print_header($SITE->fullname, $SITE->fullname, 'home', '',
                 '<meta name="description" content="'. strip_tags(format_text($SITE->summary, FORMAT_HTML)) .'" />',
                 true, '', user_login_string($SITE).$langmenu);

?>

<br/>
<table id="layout-table" summary="layout">
<tr>
<td>


<br><br><br>
    <div align="right">
<div style="border-width:2px;border-style:solid;border-color:#cccccc;width:190px"><a 

title="Usability Survey" 

href="https://lmsqa.portal.nttdatainc.com/mod/questionnaire/view.php?id=5468" 

target="_blank"><img title="Usability Survey" height="141" alt="Usability Survey" 

hspace="0" src="https://lmsqa.portal.nttdatainc.com/file.php/1/Usability_Survey_3.JPG" 

width="190" border="0"  /></a></div>
</div>
<img src="image001.gif"/>&nbsp;<B style='font-family:"Calibri";font-size:11.0pt'>Course Summary</b>
<pre> <span style='font-size:11.0pt;font-family:"Calibri";color="cc0000"'>            The course summary will give users a brief explanation of the material to be covered in the program.
</span></pre>
<img src="image002.gif"/>&nbsp;<b style='font-family:"Calibri";font-size:11.0pt'>Instructor Led Training(ILT)</b>
<pre> <span style='font-size:11.0pt;font-family:"Calibri";color="cc0000"'>            The course is offered as Instructor Led Training (ILT).  Availability may be limited to specific geographies.
</span></pre>

<img src="image003.gif"/>&nbsp;<b style='font-family:"Calibri";font-size:11.0pt'>E-Learning</b>
<pre> <span style='font-size:11.0pt;font-family:"Calibri"'>            The course is offered as E-Learning.  There may not be options for Instructor Led Training with this program.
</span></pre>
<img src="image002.gif"/><img src="image003.gif"/>&nbsp;<b style='font-family:"Calibri";font-size:11.0pt'>Instructor Led Training (ILT) + E-Learning
</b>
<pre> <span style='font-size:11.0pt;font-family:"Calibri"'>            The course is offered as both Instructor Led Training (ILT) and E-Learning.  To enroll in an ILT version of the course, 
               refer to the calendar for available sessions in respective locations. To complete the program using E-Learning, 
               participants must complete all of the modules given under e-Learning option of the course.


</span>
</pre>

</td>
</tr>
  <tr>

  <td>

   <?php
  include('accordion_course_menu.php');
  ?>
  </td>
  </tr>
  </table>
 

<?php
    print_footer('home');     // Please do not modify this line
?>
