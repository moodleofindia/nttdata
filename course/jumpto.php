<?php  
require('../config.php');

$jump = required_param('jump', PARAM_RAW);

$PAGE->set_url('/course/jumpto.php');

if (!confirm_sesskey()) {
    print_error('confirmsesskeybad');
}

if (strpos($jump, '/') === 0) {
    redirect(new moodle_url($jump));
} else {
    print_error('error');
} 

?>
