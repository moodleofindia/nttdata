	<style>
		div.accordion_column { line-height: 1.8; margin-top:20px; margin-bottom:0px; font-size:16px; font-weight:bold; color:#002569; width:auto; border-bottom:1px solid #002569; }
		dl {line-height: 1.5; margin-top:0px; margin-bottom:0px; }
		
		dt { line-height: 1.5; margin-top:0px; margin-bottom:0px; background-color:#ffffff; border:1px solid #ffffff; }
		dt a { color:#002569; font-size:15px; }
		dt:hover { line-height: 1.5; margin-top:0px; margin-bottom:0px; background-color:#e3e9ef; border:1px solid #e3e9ef; }
		dt.invisible:hover {  color:#ffffff;   hcolor:#ffffff;  line-height: 1.5; margin-top:0px; margin-bottom:0px; background-color:#cccccc; border:1px solid #cccccc; }
        dt a.visible:hover  { color:#002569; text-decoration:none; }
		dt a.invisible:hover  { color:#ffffff; background-color:#cccccc; text-decoration:none; }
		
		a.visible { color:#002569; }
		a.invisible { color:#888888; }
		a:focus { outline: none;}
		
		dd { margin-left:17px; border:1px solid #e3e9ef; height:auto; }
		dd a.visible:hover { color:#006ab5; }
		dd a.invisible:hover { color:#ffffff; hcolor:#ffffff; }
		dd a:focus { outline: none;}
		
		.images { display:inline; vertical-align:middle;}
		img.courseicons { width:16px; height:16px; margin-right:7px; }

	</style>

<table width="100%" align="center" cellspacing="15">
	<?php

    //Get IMG-Alt-Strings
    if (empty($strsummary)) {
        $strelearn = 'This course is offered as E-Learning.';
        $strilt = 'This course is offered only as Instructor Led Training (ILT).';
        $strsummary = get_string('summary');
    }

$catid = optional_param('catid', 20, PARAM_INT);

	//Get course categories
	//if($catid)
	//{
	//$result = get_records_sql("SELECT *
   //                                 FROM {$CFG->prefix}course_categories
   //                                 WHERE  id='$catid'  AND visible ='1'ORDER BY `{$CFG->prefix}course_categories`.`sortorder` ASC  ");
	//}
	//else{
   
	$result = get_records_sql("SELECT *
                                    FROM {$CFG->prefix}course_categories
                                    WHERE  id=$catid AND visible ='1'ORDER BY `{$CFG->prefix}course_categories`.`sortorder` ASC  ");
	//}

    //Print all categories for HEADER
	foreach ($result as $course_category) {
        $category_name = $course_category->name;
        $category_id = $course_category->id;
        echo '<tr>';
        echo '  <div class="accordion_column" valign="top">';
        echo '      <img src="' . $CFG->themewww .'/'. current_theme() . '/pix/i/course3.gif">' . $category_name;;
        echo '  </div>';

       
            $result2 = get_records_sql("SELECT *
                                            FROM {$CFG->prefix}course_categories
                                            WHERE parent = '$category_id' AND visible = '1' ORDER BY name ASC");
        

        //Print all subcatetories for expandable subheadings
        foreach ($result2 as $course_category2) {

            $contextcat = get_context_instance(CONTEXT_COURSECAT, $course_category2->id);
            $subcontextcat = get_child_contexts($contextcat);
            
            foreach ($subcontextcat as $temp){
                if (!isset($searchparam)){
                    $searchparam .= "contextid = " . $temp->id;
                }else{
                    $searchparam .= " OR contextid = " . $temp->id;
                }
            }

            

            $subcontextes = get_records_sql("SELECT *
                                                FROM {$CFG->prefix}role_assignments
                                                WHERE ( " . $searchparam . " ) AND userid = " . $USER->id);
            unset($searchparam);
            
            if (!ensure_context_subobj_present($course_category2->id, CONTEXT_COURSECAT) and ($course_category2->visible or $subcontextes or $isadmin or $catadmin)){
            $category_name2 = $course_category2->name;
            $category_id2 = $course_category2->id;

            //Get visibility of course
            $vis = get_records_sql("SELECT *
                                        FROM {$CFG->prefix}course_categories
                                        WHERE id = $category_id2 AND visible = '1'");
                
            //CSS for invisible
            if ($vis){
                $catcss = 'class="visible"';
            }else{
                $catcss = 'class="invisible"';
            }
            
            echo '    <dl>';

            if ($vis){
                echo '        <dt>';
            }else{
                echo '        <dt class="invisible">';
            }

          
                echo '            <a ' . $catcss . ' href="' . $CFG->wwwroot . '/course/category.php?id=' . $category_id2 . '"><img alt="spacer" hspace="2" width="12" height="13" src="'.$CFG->pixpath.'/spacer.gif" />' . $category_name2 . '</a>';
         

            echo '        </dt>';

          
                $result3 = get_records_sql("SELECT *
                                                FROM {$CFG->prefix}course
                                                WHERE category = '$category_id2' AND visible = '1' ORDER BY fullname ASC");
												
            

            if($result3){
                //Print all courses off subcategories
                foreach ($result3 as $course_category3) {
                    $context = get_context_instance(CONTEXT_COURSE, $course_category3->id);
                    if(has_capability('moodle/course:manageactivities', $context) or $course_category3->visible == 1){
                        $course_name = $course_category3->fullname;
                        $course_id = $course_category3->id;

						$ilt=get_record_sql("SELECT id FROM mdl_classroom where course='$course_id'");
						$elearn=get_record_sql("SELECT id FROM mdl_scorm where course='$course_id'");
                        $iltid=$ilt->id;
						$elearnid=$elearn->id;
						if(!isset($ddset)){
                            echo '        <dd>';
                            $ddset = 1;
                        }

                        //Get visibility of course
                        $vis = get_records_sql("SELECT *
                                                    FROM {$CFG->prefix}course
                                                    WHERE id = $course_id AND visible = '1'");

                        //CSS for invisible
                        if ($vis){
                            $catcss = 'class="visible"';
                        }else{
                            $catcss = 'class="invisible"';
                        }

                        //Get Images
                        echo '<div class="images">';
                        if ($course_category3->summary) {
                            link_to_popup_window ('/course/info.php?id='.$course_category3->id, 'courseinfo',
                                              '<img class="courseicons" alt="'.$strsummary.'" src="'.$CFG->pixpath.'/i/info.gif" />',
                                               400, 500, $strsummary);
                        } else {
                            echo '<img class="courseicons" alt="" src="'.$CFG->pixpath.'/i/noinfo.gif" />';
                        }
                        if ($ilt) {
                            echo '<a title="'.$strilt.'" href="'.$CFG->wwwroot.'/mod/classroom/view.php?f='.$iltid.'">';
                            echo '<img class="courseicons" alt="'.$strilt.'" src="'.$CFG->pixpath.'/i/guest.gif" /></a>';
                        }else {
                            echo '<img class="courseicons" alt="" src="'.$CFG->pixpath.'/i/noguest.gif" />';
                        }
                        if ($elearn ) {
                            echo '<a title="'.$strelearn.'" href="'.$CFG->wwwroot.'/mod/scorm/view.php?id='.$elearnid.'">';
                            echo '<img class="courseicons" alt="'.$strelearn.'" src="'.$CFG->pixpath.'/i/elearn.gif" /></a>';
                        }else {
                            echo '<img class="courseicons" alt="" src="'.$CFG->pixpath.'/i/noelearn.gif" />';
                        }
                        echo '</div>';

                        //Course
                        echo '<a ' . $catcss . ' href="' . $CFG->wwwroot . '/course/view.php?id=' . $course_id . '">' . $course_name . '</a>';
                        echo '<br />';

                    }
                }
			
                //Print any remaining sub-categories and highlist to indicate a category rather than course
                
                    $result4 = get_records_sql("SELECT *
                                                    FROM {$CFG->prefix}course_categories
                                                    WHERE parent = '$category_id2' AND visible = '1' ORDER BY name ASC");
                
                foreach ($result4 as $course_category4) {
			
				
                   $category_name4 = $course_category4->name;
                   $category_id4 = $course_category4->id;
                  $vis = get_records_sql("SELECT *
                                             FROM {$CFG->prefix}course
                                           WHERE id = $course_id AND visible = '1'");
                    ?>
                    <li class="course">
                        <a style="text-indent:25px" href="<?php print $CFG->wwwroot; ?>/course/category.php?id=<?php print $category_id4; ?>"><?php print $category_name4; ?></a>
                    </li> 
                    <?php 
                }

                if(isset($ddset)){
                  echo "</dd>";  
                }
                unset($ddset);
            }
            
			else
			{
			if(!isset($ddset)){
                            echo '        <dd>';
                            $ddset = 1;
                        }
			 $result5 = get_records_sql("SELECT *
                                                    FROM {$CFG->prefix}course_categories
                                                    WHERE parent = '$category_id2' AND visible = '1' ORDER BY name ASC");
                
                foreach ($result5 as $course_category5) {
                    $category_name5 = $course_category5->name;
                    $category_id5 = $course_category5->id;
                    $vis = get_records_sql("SELECT *
                                                FROM {$CFG->prefix}course
                                                WHERE id = $course_id AND visible = '1'");

					?>

                    <li class="course">
                        <a style="text-indent:25px" href="<?php print $CFG->wwwroot; ?>/course/courselistnew.php?catid=<?php print $category_id2; ?>"><?php print $category_name5; ?></a>
                    </li>
					
                    <?php
                }
				 if(isset($ddset)){
                  echo "</dd>";  
                }
                unset($ddset);
			}
            echo "</dl>";
          }
        }
        echo "</tr>";
    }
	?>
</table>
<br />
