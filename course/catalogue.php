<?php

require_once("../config.php");
require_once($CFG->dirroot .'/course/lib.php');
define("TREEVIEW_LIB_PATH","../lib/dbtreeview");
require_once(TREEVIEW_LIB_PATH . '/dbtreeview.php');

require_once($CFG->libdir.'/adminlib.php');
include_once($CFG->dirroot.'/mnet/lib.php');
//include_once(__DIR__.'/category_country.php');

  if ($CFG->forcelogin) {
        require_login();
    } else {
        user_accesstime_log();
    }

$id= optional_param('id',0, PARAM_INT);
$userid=optional_param('userid',0, PARAM_INT);
if (empty($id)) {

    $id=0;
}
class MyHandler implements RequestHandler{


	public function handleChildrenRequest(ChildrenRequest $req){

		$attributes = $req->getAttributes();


		if(!isset($attributes['code'])){
			die("error: attribute code not given");
		}
		$parentCode = $attributes['code'];

		$link = mysql_connect('10.234.7.223', 'ReportUser', 'Report@123')
    		   or die("Unable to connect to database.");
		mysql_select_db('lms') or die("Could not select database");

		if(!mysql_query("SET CHARACTER SET utf8")){
			throw new Exception('Could not set character set UTF-8.');
		}
		$rec=sprintf("SELECT parent FROM mdl_course_categories where visible=1 and parent=$parentCode");

		$recresult = mysql_query($rec) or die("Query failed 3");
		$num_course = mysql_num_rows($recresult);



	if($num_course<1)
	{
	$query = sprintf("SELECT * FROM mdl_course where visible=1 and category=$parentCode order by sortorder asc");
	}
	else
	{
		$query = sprintf("SELECT * FROM mdl_course_categories where  visible=1 and parent=$parentCode");
	}
		//,
			//		mysql_escape_string($parentCode));


		$result = mysql_query($query) or die("Query failed 1");

		$nodes=array();


		/**
		* Formating display of treemenu
		* -----------------------------------------------------------------------------------------
		* First display all the tree that has a sub menu
		*
		* To format tree menu yo uneed to initialize the get element with the following parameter
		*	singlecols 		=>  if 1 with display only once teh course name
		*	noicon 			=>  if 1 it will only display icon for the first row for the course
		*	onlymodule		=>  if 1 it will only display module name not the course name with module name
		*/
		$_GET['singlecols'] = 1;
		$_GET['noicon'] 	= 1;
		$_GET['onlymodule'] = 2;
		$_GET['linkactivity'] = 0;


		$_query = sprintf("SELECT * FROM mdl_course_categories where visible=1 and parent=$parentCode");
		$_result = mysql_query($_query);
		while ($line = mysql_fetch_assoc($_result)) {

			$code = $line["id"];
			$text = $line["name"];

			$node = DBTreeView::createTreeNode(
			$text, array("code"=>$code));

			$nodes[] = $node;
		}

		// get the course listing
		$query = sprintf("SELECT * FROM mdl_course where visible=1 and category=$parentCode order by sortorder asc");
		$result = mysql_query($query);
		$rowssnum=mysql_num_rows($result);
		if(mysql_num_rows($result) > 0) {

			$displayname = '<div ><table border="0" class="tablea" id="csscolors"><thead>';

			$displayname .= '<th class="tablea" valign="top">Info</th>';
			$displayname .= '<th class="tablea" valign="top">Course</th>';
			$displayname .= '<th class="tablea" valign="top" >Mode</th>';
			$displayname .= '<th class="tablea" valign="top">Language</th>';
			$displayname .= '<th class="tablea" valign="top">Duration</th>';
			$displayname .= '<th class="tablea" valign="top" >Points</th>';
			$displayname .= '<th class="tablea" valign="top">Rating</th>';
			//$displayname .= '<th class="tablea" valign="top" >Progress Bar</th>';
			$displayname .= '<th class="tablea" valign="top" >Last Updated</th>';
			$displayname .= '<th class="tablea" valign="top">Action</th>';
			$displayname .= '</thead>';
		}


		while ($line = mysql_fetch_assoc($result)) {

			$_mysection = 0;
			$code = $line["id"];
			$name=$line["fullname"];
			$USERID=$_SESSION['treeuserid'];



			$status = "SELECT gg.finalgrade,gi.grademax
FROM mdl_course c JOIN mdl_grade_items AS gi
ON gi.courseid = c.id JOIN mdl_grade_grades AS gg
on gg.itemid=gi.id AND gi.itemtype = 'course' and c.id=$code and gg.userid = $USERID";
$statusresult = mysql_query($status) or die("Status retreval failed");

			$graderow = mysql_fetch_array($statusresult);



			/**
			* Getting course rating
			*/
			$courseid = $code;
			$res = '<img src="'.$CFG->wwwroot.'/blocks/rate_course/graphic/rating_graphic.php?courseid='. $courseid.'" alt=""/>';


			/**
			* Collectc course related details and populate in the tree menu.
			* Funcation:
			* ----------------------------------------
			* 	1. Get course information using course id in object
			*	2. Get Module information for the given course
			*
			* Improvement
			* ------------------------------------------------
			* Can have cache management, it will reduce the mysql load.
			*/

			$_course = get_record('course', 'id', $code);
			$typeofcourse=$_course->type;
			$clanguage=$_course->lang;

			ob_start();
			$modinfo = get_fast_modinfo_visible($_course);

			ob_end_clean();

			$text1 = $text;
			$_mysection = 0;
			$totalgap = 0;


			reset($modinfo->sections);
			$duration=0;
			$pointsss=0;
			$timemodifieds=array();

			foreach ($modinfo->sections as $sectionnum=>$section) {
				$durations=0;
			    $pointss=0;
				//$x=0;
				foreach ($section as $cmid) {
					$cm = $modinfo->cms[$cmid];

					if ($cm->modname == 'label') 	{ continue; }
					if (!$cm->uservisible) 			{ continue; }

					// getting information related to the section

					if($cm->modname=='resource')
					{
					$query	=get_record_sql("select duration,point,from_unixtime(timemodified) as timemodified from mdl_{$cm->modname} where name='$cm->name' and id='$cm->instance' and nongrade!='nongradable'");
					}
					else
					{
					$query	=get_record_sql("select duration,point,from_unixtime(timemodified) as timemodified from mdl_{$cm->modname} where name='$cm->name' and id='$cm->instance'");
					}
					if($duration==0)
					{
					$queryc	=get_record_sql("select from_unixtime(timemodified) as timemodified from mdl_course where id='$_course->id' and (timemodified !='' || timemodified !=NULL)");
					$timeesc=$queryc->timemodified;
					$timeec=explode(' ',$timeesc);
					if($timeec[0]!=null)
					{
					$timemodifieds[]=$timeec[0];
					}
					}
					$durations=$durations+$query->duration;
					$pointss=$pointss+$query->point;
					$timees=$query->timemodified;
					$timee=explode(' ',$timees);
					if($timee[0]!=null)
					{
					$timemodifieds[]=$timee[0];
					}




					}
					//$x++;
					$duration=$duration+$durations;
					$pointsss=$pointsss+$pointss;
				}

					if(count($timemodifieds)>0)
					{
					$timemodifiedd=max($timemodifieds);
					}


					//infoicon
					$infoicon = '<A HREF="'.$CFG->wwwroot.'\course\report\outline\preview.php?id='.$code.'" onClick="return popup(this)"><img src="'.$CFG->wwwroot.'/lib/dbtreeview/media/info.PNG" alt="More information" height="20" width="20" /> </A> ';


					
					$heading = '<a href="'.$CFG->wwwroot.'/course/view.php?id='.$code.'" target="_blank">'.$name.'</a>';
					$_mysection++;

					$class = '';

					if($_mysection == $totalgap) {
						// create gap between 2 course
						$class= 'coursegap';
					}
					//$modulename = get_string('modulename', $cm->modname);

					//code modified by srinu
					if(isset($_GET['noicon']) && $_GET['noicon'] == 1 && $_mysection != 1) {
						$displayname .= '<tr><td class="tablea" valign="top">&nbsp;</td>';
						break;
					} else {
						$displayname .= '<tr><td class="tablea" valign="top">' . $infoicon . '</td>';
					}
					//code ended by srinu

				//	$courseiconkey = $CFG->wwwroot.'/course/images/enrolkey.png';
					if(isset($_GET['singlecols']) && $_GET['singlecols'] == 1) {
						// this logic will display only once the course and get the remaing list of activity
						if($_mysection == 1) {

							$displayname .= '<td  valign="top" class="tablea"    rowspan="' . $totalgap .'">' . $heading . '</td>';
						}
					} else {
						$displayname .= '<td  valign="top"   >' . $heading . '</td>';
					}




					$displayname .= '<td class="tablea" valign="top" >'. format_string(ucfirst($typeofcourse)) . '</td>';



					$languageicon=$CFG->wwwroot.'/course/images/languageicon.png';

					//$progressbaricon=$CFG->wwwroot.'/course/images/progressbaricon.png';
					$launchcourse=$CFG->wwwroot.'/course/images/launchcourse.png';

					$displayname .= '<td class="tablea" valign="top"><img src="' . $languageicon.'" />  </td>';


					if($graderow["finalgrade"] > 0)
					{
					$graderow["finalgrade"]=round((($graderow["finalgrade"]/$graderow["grademax"])*100),2);
					}
					else if($graderow["finalgrade"]=="" || $graderow["finalgrade"] ==0)
					{
					$graderow["finalgrade"]=0;
					}

					$hoursdur=(round($duration/60,2))." Hrs";

					//srinu ended code
					$displayname .= '<td class="tablea" valign="top" >' . format_string($hoursdur) . ' </td>';
					$displayname .= '<td class="tablea" valign="top" > +' . format_string($pointsss) . ' Pts </td>';
					$displayname .= '<td class="tablea" valign="top" > ' . $res . '  </td>';

// $displayname .= '<td class="tablea" valign="top" style="width:250px;">
// <div style="width:160px;">
// <div class="meter orange nostripes" style="width:120px;float:left;">
			// <span style="width: '.$graderow["finalgrade"].'%"></span>
// </div>
// <div style="width:40px;float:right;font-size:15px;color:green;background-color:#FFFFFF;">'.$graderow["finalgrade"].'%</div>
// </div>

// </td>';

					if(count($timemodifieds)>0)
					{
					$displayname .= '<td class="tablea" valign="top" >' . date("d M Y",strtotime(format_string($timemodifiedd))) . '</td>';
					}
					else
					{
					$displayname .= '<td class="tablea" valign="top" ></td>';
					}
					$displayname .= '<td class="tablea" valign="top" ><a href="'.$CFG->wwwroot.'/course/view.php?id='.$code.'" target="_blank"><img src="' . $launchcourse.'" /> </a> </td>';

					$displayname .= '</tr>';







			// moved node addition to above, to the folder level
		}

		if(mysql_num_rows($result)) {
			$displayname.='</table></div>';
			// Display node to server
			$node = DBTreeView::createTreeNode($displayname, array("code"=>$code .'-' . $cmid,"class" => 'innerchild'));
			$node->setHasChildren(false);
			/* $node->setClosedIcon($CFG->wwwroot."/lib/dbtreeview/media/course.PNG"); */
			// set empty icon for display
			$node->setClosedIcon($CFG->wwwroot."/lib/dbtreeview/media/empty.gif");
			$nodes[] = $node;

		}

		mysql_free_result($result);

		mysql_close($link);

		$response = DBTreeView::createChildrenResponse($nodes);
		return $response;
	}
}


try{
	DBTreeView::processRequest(new MyHandler());
}catch(Exception $e){
	echo("Error:". $e->getMessage());
}


  print_header($SITE->fullname, $SITE->fullname, 'home', '',
                 '<meta name="description" content="'. strip_tags(format_text($SITE->summary, FORMAT_HTML)) .'" />',
                 true, '', user_login_string($SITE).$langmenu);



print("<html>\n<head>\n ");
printf("<SCRIPT TYPE='text/javascript'>
function popup(mylink)
{
if (! window.focus)return true;
var href;
if (typeof(mylink) == 'string')
   href=mylink;
else
   href=mylink.href;
window.open(href, 'Preview', 'width=400,height=500,scrollbars=yes,toolbar=yes,left=200,top=50');
return false;
}
</SCRIPT>");
printf("<script src=\"%s/treeview.js\" type=\"text/javascript\"></script>\n",
			TREEVIEW_LIB_PATH);
printf("<script src=\"%s/di.js\" type=\"text/javascript\"></script>\n",
			TREEVIEW_LIB_PATH);

printf('<link href="%s/treeview.css" rel="stylesheet" type="text/css" media="screen"/>'."\n",
			TREEVIEW_LIB_PATH);



print("</head>\n");
?>





	<table align="left" border="1" cellpadding="1" cellspacing="1" >
			<tbody>

				<tr>
					<td>
						<table align="left"  cellpadding="1" cellspacing="1" style="margin-top:10px;">
							<tbody>
							<tr>
							<td style="border-bottom: 1px solid #CECECE;">
							The courses listed in this part of the catalog are part of the internal training offerings.  These courses represent required training for some or all of the organization.  All employees must complete the Global Code of Business Conduct each year.  Additional requirements exist for employees based on their role or business unit.  Some courses are available by invitation only and may not be available to every employee.  Messages will be listed in the course header where appropriate.
							<?php
							if($id==13){
							echo '<p><a href="https:lms.portal.nttdatainc.com/course/category.php?id=14">Individuals seeking NTTDATA PM Certification should click on the NLCI Certification tab.</a></p>';
							}
							?>
							<td>
							</tr>
								<tr>
									<td>
									</br>
									<?php

$_SESSION['treeuserid']=$USER->id;
$rootAttributes = array("code"=>$id);
$treeID = "treev1";
$tv = DBTreeView::createTreeView(
		$rootAttributes,
		TREEVIEW_LIB_PATH,
		$treeID);
$tv->setRootHTMLText("Catalogue");
$tv->setRootIcon($CFG->wwwroot."/lib/dbtreeview/media/book.gif");
$tv->printTreeViewScript();
?></td>
								</tr>
							</tbody>
						</table>




					</td>
				</tr>
			</tbody>

		</table>

</body>
</html>
  <?php
  print_footer('home');     // Please do not modify this line
  ?>
