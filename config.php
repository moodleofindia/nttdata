<?php  /// Moodle Configuration File 

unset($CFG);

$CFG->dbtype    = 'mysqli';
$CFG->dbhost    = 'localhost';
$CFG->dbname    = 'nttdata19';
$CFG->dbuser    = 'root';
$CFG->dbpass    = 'root';
$CFG->dbpersist =  false;
$CFG->prefix    = 'mdl_';

// $CFG->wwwroot   = 'http://lms101.portal.nttdatainc.com';
$CFG->wwwroot   = 'http://localhost/nttdata';
$CFG->dirroot   = '/home/scott/work/nttdata';
$CFG->dataroot   = '/home/scott/datafolder/nttdata';
$CFG->admin     = 'admin';


$CFG->allow_mass_enroll_feature=1;

$CFG->directorypermissions = 0777;  // try 02777 on a server in Safe Mode

date_default_timezone_set('America/New_York');

require_once("$CFG->dirroot/lib/setup.php");
// MAKE SURE WHEN YOU EDIT THIS FILE THAT THERE ARE NO SPACES, BLANK LINES,
// RETURNS, OR ANYTHING ELSE AFTER THE TWO CHARACTERS ON THE NEXT LINE.
?>
