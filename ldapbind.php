<?php 
// This example contains an error, which we will catch. 
$ld = ldap_connect("10.252.1.100"); 
ldap_set_option($ld, LDAP_OPT_PROTOCOL_VERSION,3);
ldap_set_option($ld, LDAP_OPT_REFERRALS,0);
$bind = ldap_bind($ld); 
// syntax error in filter expression (errno 87), 
// must be "objectclass=*" to work. 
$res =  @ldap_search($ld, "DC=IN,DC=KRES,DC=kEANE,DC=com,OU=EMPLOYEES,O=KEANE,CN=027634", "objectclass=*"); 
var_dump($res);  
if (!$res) { 
    printf("LDAP-Errno: %s<br>\n", ldap_errno($ld)); 
    printf("LDAP-Error: %s<br>\n", ldap_error($ld)); 
    die("Argh!<br>\n"); 
} 
$info = ldap_get_entries($ld, $res); 
printf("%d matching entries.<br>\n", $info["count"]); 
?>
