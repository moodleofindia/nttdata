<?php // CRON for Employee Data feed into LMS - Roy Philip Nov 2010

    ini_set('max_execution_time',0);
    $starttime = microtime();

/// The following is a hack necessary to allow this script to work well 
/// from the command line.

    define('FULLME', 'cron');


/// Do not set moodle cookie because we do not need it here, it is better to emulate session
    $nomoodlecookie = true;

/// The current directory in PHP version 4.3.0 and above isn't necessarily the
/// directory of the script when run from the command line. The require_once()
/// would fail, so we'll have to chdir()

    if (!isset($_SERVER['REMOTE_ADDR']) && isset($_SERVER['argv'][0])) {
        chdir(dirname($_SERVER['argv'][0]));
    }

    require_once(dirname(__FILE__) . '/../config.php');
    require_once($CFG->libdir.'/adminlib.php');
    require_once($CFG->libdir.'/gradelib.php');

/// Extra debugging (set in config.php)
    if (!empty($CFG->showcronsql)) {
        $db->debug = true;
    }
    if (!empty($CFG->showcrondebugging)) {
        $CFG->debug = DEBUG_DEVELOPER;
        $CFG->debugdisplay = true;
    }

/// extra safety
    @session_write_close();

/// check if execution allowed
    if (isset($_SERVER['REMOTE_ADDR'])) { // if the script is accessed via the web.
        if (!empty($CFG->cronclionly)) { 
            // This script can only be run via the cli.
            print_error('cronerrorclionly', 'admin');
            exit;
        }
        // This script is being called via the web, so check the password if there is one.
        if (!empty($CFG->cronremotepassword)) {
            $pass = optional_param('password', '', PARAM_RAW);
            if($pass != $CFG->cronremotepassword) {
                // wrong password.
                print_error('cronerrorpassword', 'admin'); 
                exit;
            }
        }
    }


/// emulate normal session
    $SESSION = new object();
    $USER = get_admin();      /// Temporarily, to provide environment for this script

/// ignore admins timezone, language and locale - use site deafult instead!
    $USER->timezone = $CFG->timezone;
    $USER->lang = '';
    $USER->theme = '';
    course_setup(SITEID);

/// send mime type and encoding
    if (check_browser_version('MSIE')) {
        //ugly IE hack to work around downloading instead of viewing
        @header('Content-Type: text/html; charset=utf-8');
        echo "<xmp>"; //<pre> is not good enough for us here
    } else {
        //send proper plaintext header
        @header('Content-Type: text/plain; charset=utf-8');
    }

/// no more headers and buffers
    while(@ob_end_flush());

/// increase memory limit (PHP 5.2 does different calculation, we need more memory now)
    @ini_set('memory_limit','256M');

/// Start output log

    $timenow  = time();
	
	

    mtrace("Server Time: ".date('r',$timenow)."\n\n");
	$filedate =  date("Ymd");  
	$file = 'c:\\data\\Navigator'.$filedate.'.log';	
	$timenow = date('r',$timenow)."\r\n";
	file_put_contents($file,$timenow, FILE_APPEND | LOCK_EX);

	mtrace("Starting HRDM feed update. \n");
    mtrace("Server Time: ".date('r',$timenow)."\n\n");

	
	$backup_filedate =  date("Ymd");  
	$backupfile = 'C:\inetpub\LMSdata\Navigator\Navigator Log'.$backup_filedate.'.xls';	
	rename ("C:\inetpub\LMSdata\Navigator\Navigator Log.xls", $backupfile);

    mtrace("Server Time: ".date('r',$timenow)."\n\n");
	$filedate =  date("Ymd");  
	$file = 'C:\inetpub\LMSdata\Navigator\Navigator.log';	
	$timenow = date('r',$timenow)."\r\n";
	file_put_contents($file,$timenow, FILE_APPEND | LOCK_EX);

	
	//Naga added for excel
	
	
						$xlshead=pack("s*", 0x809, 0x8, 0x0, 0x10, 0x0, 0x0);
						$xlsfoot=pack("s*", 0x0A, 0x00);
						function xlsCell($row,$col,$val) {
						  $len=strlen($val);
						  return pack("s*",0x204,8+$len,$row,$col,0x0,$len).$val;
						}

						$data.=xlsCell(0,0,"Update Error") . xlsCell(0,1,"Insert Error") . xlsCell(0,2,"Nologin Error") . xlsCell(0,3,"Ldap Error") ;

	

mtrace("Starting to update and insert missing employees... \n"); 
//Truncate the stageing table
$queryTruncateExit='truncate table mdl_navigator_feed;';	
	$queryUploadExit='load data local infile "C://data//EE_delta.csv" into table mdl_navigator_feed fields terminated by "," ENCLOSED BY "\"" lines terminated by "\r\n" ignore 1 lines '; 
				
	$resultTruncateExit = execute_sql($queryTruncateExit, $feedback=true);
	if($resultTruncateExit)
	{
		$resultUploadExit = execute_sql($queryUploadExit, $feedback=true);
		
	}
	else
	{
	mtrace("Operation failed during importing terminated employee records. \n");
	}
	
	$queryUpdate="UPDATE mdl_user u INNER JOIN mdl_navigator_feed t ON u.username = t.portalid set u.firstname=t.firstname,u.lastname=t.lastname,u.email=t.email,u.country = t.emp_work_country,
					u.BU=t.BU,u.LC=t.LC,u.practise=t.practise,u.costcenter=t.costcenter,
					u.grade = t.grade,u.employee_type=t.employee_type,u.manager_portalid=t.manager_portalid,u.source=t.acq_company,u.hireddate=t.hireddate,u.loaddate=t.loaddate,u.timeupdated='$timenow'  ;";	

	
	$queryInsert="insert into mdl_user (username,firstname,lastname,country,grade,email,BU,LC,practise,costcenter,employee_type,manager_portalid,source,hireddate,loaddate) 
														select portalid,firstname,lastname,emp_work_country,grade,email,BU,LC,practise,costcenter,employee_type,manager_portalid,acq_company,hireddate,loaddate  
														from mdl_navigator_feed  m 	where  m.portalid not in(select username from mdl_user);"; 
								
	//Query TO update the user records from stage table to LMS user table		
	$resultUpdate = execute_sql($queryUpdate, $feedback=true);
	if($resultUpdate)
	{
		mtrace("Employee records updated sucessfully ! \n");
		$text = 'The Employee Records updated successfully from stage table to LMS user table';
		$text1 = $text."\r\n";
	}
	else
	{
	
		mtrace("Error: Failed to update employee records. \n");
		$mysql_error1= ('Database Error update:' . mysql_error());
		echo $mysql_error1;
		$text = "Error: Employee Records are not updated successfully from stage table to LMS user table. ";
		
		$text1 = $text."\r\n".$mysql_error1."\r\n";
		$error_message='The Cron job is unable to update the user records from stage table into LMS user table.';
		$error_message1 = $error_message."\r\n".$mysql_error1."\r\n";
	}
	
	//Query TO insert the user records from stage table to LMS user table
	$resultInsert = execute_sql($queryInsert, $feedback=true);
	
	if($resultInsert)
	{
		mtrace("Employee records inserted sucessfully ! \n");
		$text = ' Employee Records inserted successfully from stage table to LMS user table.';
		$text2 = $text."\r\n";
	}
	else
	{
		mtrace("Error: Failed to insert employee records. \n");
		$mysql_error2= ('Database Error update:' . mysql_error());
		echo $mysql_error2;
		$text = 'Error: Employee Records are not inserted successfully from stage table to LMS user table.';
		$text2 = $text."\r\n".$mysql_error2."\r\n";
		$error_message2='The Cron job is unable to insert the user records from stage table into LMS user table.';
		$error_message3 = $error_message2."\r\n".$mysql_error2."\r\n";
	}	
    //Query TO select the user records from stage table based upon the load date
	$user_sttaus1=get_records_sql("select portalid,auth from mdl_navigator_feed ");
	foreach ($user_sttaus1 as $user_sttaus)
	{
		if($user_sttaus->auth=='N')
		{
					//Query TO update the users status to nologin in LMS user table
					$status_update= "update mdl_user set auth='nologin' where username='$user_sttaus->username'";
					
					$resultcountryUpdate1 = execute_sql($status_update, $feedback=true);
						
						if($resultcountryUpdate1)
						{
							mtrace(" user status updated successfully to Nologin ! \n");
							$text = 'The Employee status has been updated to nologin successfully in LMS user table. ';
							$text3 = $text."\r\n";
						
						}
						else
						{
							mtrace("Error: Failed to update user status to Nologin. \n");
							$mysql_error3= ('Database Error update:' . mysql_error());
							$text = "Error:The Employee status ". '$user_sttaus->username'. "unable updated to nologin in LMS user table .";
							$text3 = $text."\r\n".$mysql_error3."\r\n";
							$error_message4="The Cron job is unable to update the employee status to nologin for ". '$user_sttaus->username'. "into LMS user table.";
							$error_message5 = $error_message4."\r\n".$mysql_error3."\r\n";
						}
					
		}
		
		else
		{
					//Query TO update the users status to ldap in LMS user table
					$status_update= "update mdl_user set auth='ldap' where username='$user_sttaus->username' ;";
					
					$resultcountryUpdate1 = execute_sql($status_update, $feedback=true);
						
						if($resultcountryUpdate1)
						{
							mtrace(" user status updated successfully to ldap ! \n");
							$text = 'The Employee status has been updated to ldap  in LMS user table successfully ';
							$text4 = $text."\r\n";
						}
						else
						{
							mtrace("Error: Failed to update user status to ldap. \n");
							$mysql_error4= ('Database Error update:' . mysql_error());
							$text = "Error:The Employee status ". '$user_sttaus->username'. "unable updated to ldap in LMS user table. ";
							$text4 = $text."\r\n".$mysql_error4."\r\n";
							$error_message6="The Cron job is unable to update the employee status to ldap for ". '$user_sttaus->username'. "into LMS user table.";
							$error_message7 = $error_message6."\r\n".$mysql_error4."\r\n";
						}
		}
		
	}			
	//Convert the employee city to DSKLS based upon the last digit of the costcenter
   $dskls_city= "update mdl_user set city='DSKLS' where right(costcenter,1) in ('5','6') and length (costcenter) >=10";
   $dskls_city_Sql = execute_sql($dskls_city, $feedback=true);
	// Naga Added ... to calculate the Mandatory Training Hours based on the Hired date in between 2015-16 FY.	
    $gradeupdate='update mdl_user u INNER JOIN mdl_grade_training t on u.grade=t.grade set mandatorytrainings=hours ;';
	$resultgradeupdate = execute_sql($gradeupdate, $feedback=false);
	
	$gradeupdate1='update mdl_user u INNER JOIN mdl_grade_training t on u.grade=t.grade set mandatorytrainings=round((hours*11)/12) where u.hireddate <= "2016-05-15" and u.hireddate >= "2016-04-16";';
	$resultgradeupdate1 = execute_sql($gradeupdate1, $feedback=false);
	$gradeupdate2='update mdl_user u INNER JOIN mdl_grade_training t on u.grade=t.grade set mandatorytrainings=round((hours*10)/12) where u.hireddate <= "2016-06-15" and u.hireddate >= "2016-05-16";';
	$resultgradeupdate2 = execute_sql($gradeupdate2, $feedback=false);
	$gradeupdate3='update mdl_user u INNER JOIN mdl_grade_training t on u.grade=t.grade set mandatorytrainings=round((hours*9)/12) where u.hireddate <= "2016-07-15" and u.hireddate >= "2016-06-16";';
	$resultgradeupdate3 = execute_sql($gradeupdate3, $feedback=false);
	$gradeupdate4='update mdl_user u INNER JOIN mdl_grade_training t on u.grade=t.grade set mandatorytrainings=round((hours*8)/12) where u.hireddate <= "2016-08-15" and u.hireddate >= "2016-07-16";';
	$resultgradeupdate4 = execute_sql($gradeupdate4, $feedback=false);
	$gradeupdate5='update mdl_user u INNER JOIN mdl_grade_training t on u.grade=t.grade set mandatorytrainings=round((hours*7)/12) where u.hireddate <= "2016-09-15" and u.hireddate >= "2016-08-16";';
	$resultgradeupdate5 = execute_sql($gradeupdate5, $feedback=false);
	$gradeupdate6='update mdl_user u INNER JOIN mdl_grade_training t on u.grade=t.grade set mandatorytrainings=round((hours*6)/12) where u.hireddate <= "2016-10-15" and u.hireddate >= "2016-09-16";';
	$resultgradeupdate6 = execute_sql($gradeupdate6, $feedback=false);
	$gradeupdate7='update mdl_user u INNER JOIN mdl_grade_training t on u.grade=t.grade set mandatorytrainings=round((hours*5)/12) where u.hireddate <= "2016-11-15" and u.hireddate >= "2016-10-16";';
	$resultgradeupdate7 = execute_sql($gradeupdate7, $feedback=false);
	$gradeupdate8='update mdl_user u INNER JOIN mdl_grade_training t on u.grade=t.grade set mandatorytrainings=round((hours*4)/12) where u.hireddate <= "2016-12-15" and u.hireddate >= "2016-11-16";';
	$resultgradeupdate8 = execute_sql($gradeupdate8, $feedback=false);
	$gradeupdate9='update mdl_user u INNER JOIN mdl_grade_training t on u.grade=t.grade set mandatorytrainings=round((hours*3)/12) where u.hireddate <= "2017-01-15" and u.hireddate >= "2016-12-16";';
	$resultgradeupdate9 = execute_sql($gradeupdate9, $feedback=false);
	$gradeupdate10='update mdl_user u INNER JOIN mdl_grade_training t on u.grade=t.grade set mandatorytrainings=round((hours*2)/12) where u.hireddate <= "2017-02-15" and u.hireddate >= "2017-01-16";';
	$resultgradeupdate10 = execute_sql($gradeupdate10, $feedback=false);
	$gradeupdate11='update mdl_user u INNER JOIN mdl_grade_training t on u.grade=t.grade set mandatorytrainings=round((hours*1)/12) where u.hireddate <= "2017-03-15" and u.hireddate >= "2017-02-16";';
	$resultgradeupdate11 = execute_sql($gradeupdate11, $feedback=false);
	
	//Completed Naga...
	
	
  
	
								$rowNumber=$rowNumber+1;
								$data.=xlsCell($rowNumber,0,$error_message1) .xlsCell($rowNumber,1,$error_message2) . xlsCell($rowNumber,2,$error_message5) . xlsCell($rowNumber,3,$error_message7) ; 
		
		$message = $error_message1."\r\n".$error_message2."\r\n".$error_message5."\r\n".$error_message7;
		
		$message1= "The Inputfeed Job is having few errors and please find the atatched file for the errors.";
		file_put_contents($file, $message, FILE_APPEND | LOCK_EX);
	
	$employeeMessage='<html xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:w="urn:schemas-microsoft-com:office:word" xmlns:m="http://schemas.microsoft.com/office/2004/12/omml" xmlns="http://www.w3.org/TR/REC-html40"><head><META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=us-ascii"><meta name=Generator content="Microsoft Word 14 (filtered medium)"><style><!--
						/* Font Definitions */
						@font-face
							{font-family:"Microsoft Sans Serif";
							panose-1:2 11 6 4 2 2 2 2 2 4;}
						@font-face
							{font-family:Calibri;
							panose-1:2 15 5 2 2 2 4 3 2 4;}
						/* Style Definitions */
						p.MsoNormal, li.MsoNormal, div.MsoNormal
							{margin-top:0in;
							margin-right:0in;
							margin-bottom:10.0pt;
							margin-left:0in;
							line-height:115%;
							font-size:11.0pt;
							font-family:"Calibri","sans-serif";}
						a:link, span.MsoHyperlink
							{mso-style-priority:99;
							color:blue;
							text-decoration:underline;}
						a:visited, span.MsoHyperlinkFollowed
							{mso-style-priority:99;
							color:purple;
							text-decoration:underline;}
						span.EmailStyle17
							{mso-style-type:personal-compose;
							font-family:"Calibri","sans-serif";
							color:windowtext;}
						.MsoChpDefault
							{mso-style-type:export-only;
							font-family:"Calibri","sans-serif";}
						@page WordSection1
							{size:8.5in 11.0in;
							margin:1.0in 1.0in 1.0in 1.0in;}
						div.WordSection1
							{page:WordSection1;}
						--></style><!--[if gte mso 9]><xml>
						<o:shapedefaults v:ext="edit" spidmax="1026" />
						</xml><![endif]--><!--[if gte mso 9]><xml>
						<o:shapelayout v:ext="edit">
						<o:idmap v:ext="edit" data="1" />
						</o:shapelayout></xml><![endif]--></head><body lang=EN-US link=blue vlink=purple>
						<div class=WordSection1><p class=MsoNormal>Hi ,<o:p></o:p></p>
						<p class=MsoNormal>'.$message1.'.&nbsp; 
						<span style="color:#953735;mso-style-textfill-fill-color:#953735;mso-style-textfill-fill-alpha:100.0%">
						Please check once and do the needful at the earliest. </span>&nbsp;<o:p></o:p></p><p class=MsoNormal>Thank you.<o:p></o:p></p></div></body></html>';
						
						$euser = get_record('user','id','37417');
						$uniqueid=  substr(md5(uniqid(rand(), true)),16,16);
						$filen="Navigator Log";
						$filename="$filen.xls";
						$conte=$xlshead . $data . $xlsfoot;//This is previous not there please place it
						//file_put_contents("C://data//".$filename, $conte);//This is the command to save your file in server
						file_put_contents("C://inetpub//LMSdata//Navigator//".$filename, $conte);//This is the command to save your file in server
						$content = chunk_split(base64_encode($conte));
						$uid = md5(uniqid(time())); 
						$agname="Name Of Recipent";
						$header .= "Navigator Log\r\n";
						$header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\r\n\r\n";
						$header .= "This is a multi-part message in MIME format.\r\n";
						$header .= "--".$uid."\r\n";
						$header .= "Content-type:text/html; charset=iso-8859-1\r\n";
						$header .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
						$header .= $employeeMessage."\r\n\r\n";
						$header .= "--".$uid."\r\n";
						$header .= "Content-Type: application/vnd.ms-excel; name=\"".$filename."\"\r\n";
						$header .= "Content-Transfer-Encoding: base64\r\n";
						$header .= "Content-Disposition: attachment; filename=\"".$filename."\"\r\n\r\n";
						$header .= $content."\r\n\r\n";
						$header .= "--".$uid."--";
						$tmpfile = '/Navigator/Navigator Log.xls';
						$files='Navigator Log.xls';
						
						// $tmpfile = '/Navigator/Navigator.log';
						// $files='Navigator.log';
						// if ($files){
						// echo "have the file";
						// }
						if (strlen( trim($message))>=10 && !empty(trim($message)))
						 {
							// //Sending error message to the respective team
							email_to_user( $euser,'', 'Input Feed job failed',$employeeMessage,$employeeMessage,$tmpfile,$files);
						 }
	
    //Unset session variables and destroy it
    @session_unset();
    @session_destroy();


    $difftime = microtime_diff($starttime, microtime());
    mtrace("Execution took ".$difftime." seconds"); 

		
  		
	
	
						
	
	  mtrace("Finished HRDM feed update. \n");
/// finish the IE hack
    if (check_browser_version('MSIE')) {
        echo "</xmp>";
    }

?>
