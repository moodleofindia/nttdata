<?php 

    ini_set('max_execution_time',0);
	$totalstarttime = microtime();

    define('FULLME', 'cron');

    $nomoodlecookie = true;

    if (!isset($_SERVER['REMOTE_ADDR']) && isset($_SERVER['argv'][0])) {
        chdir(dirname($_SERVER['argv'][0]));
    }

    require_once(dirname(__FILE__) . '/../config.php');



    @session_write_close();


    $SESSION = new object();
    $USER = get_admin();      
    $USER->timezone = $CFG->timezone;
    course_setup(SITEID);

	@header('Content-Type: text/plain; charset=utf-8');
	 
    while(@ob_end_flush());

    @ini_set('memory_limit','1024M');


    $timenow  = time();

    mtrace("Server Time: ".date('r',$timenow)."\n\n");
	$filedate =  date("Ymd");  
	$file = 'c:\\data\\formb'.$filedate.'.log';	
	$timenow = date('r',$timenow)."\r\n";
	$timedb=time();
	file_put_contents($file,$timenow, FILE_APPEND | LOCK_EX);

    mtrace("Starting activity modules");
	get_mailer('buffer');
  
    //code for trigger an email to employee after 30 days of training
	$squery=get_records_sql("SELECT * FROM mdl_classroom_sessions where externaltraining='1' and status='completed'");
	
	foreach($squery as $skey=>$sval)
	{
	$resquery=get_records_sql("SELECT * FROM mdl_classroom_submissions where sessionid='$sval->id' and attend='1'");
	foreach($resquery as $keys=>$values)
	{
	$sessionid=$values->sessionid;
	$sesdates = get_record('classroom_sessions_dates', 'sessionid', $sessionid, '','','','', 'timestart, timefinish');
	$userid=$values->userid;
	$timemodified=$sesdates->timefinish;
	$attend=$values->attend;
	$endtime=date("Y-m-d",$timemodified);
	$start = new DateTime($endtime);
	$end = new DateTime("now");
	$interval = date_diff($start, $end);
	echo $countdays=$interval->format('%R%a');
	echo "<br>";
	if($countdays=='31')
	{
$addusers = get_record('user', 'id', $userid, '','','','', 'id, firstname, lastname,username,country');
$fullname = fullname($addusers, true);
$portalid = $addusers->username;
$subject1='Thanks for attended the session';
$sub=$sval->programename;
$fromaddress = 'Training Helpdesk';
$subject= $subject1.'   '.$sub;
				   $dear='Dear';
				   $name=$fullname.' PortalID '.$portalid.",";
				    $message="<div style='margin-top:10px;'><br>$subject.</div><div style='clear:both;'></div><br><div style='margin:top:10px;'>Please Provide feedback by using the below link.<br>
					$CFG->wwwroot/teff/feedback2.php?s=$sval->id&u=$userid</div>";

	$erolmessage= '<html xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:w="urn:schemas-microsoft-com:office:word" xmlns:m="http://schemas.microsoft.com/office/2004/12/omml" xmlns="http://www.w3.org/TR/REC-html40"><head><META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=us-ascii"><meta name=Generator content="Microsoft Word 14 (filtered medium)"><style><!--
/* Font Definitions */
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
/* Style Definitions */
p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin:0in;
	margin-bottom:.0001pt;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";}
a:link, span.MsoHyperlink
	{mso-style-priority:99;
	color:blue;
	text-decoration:underline;}
a:visited, span.MsoHyperlinkFollowed
	{mso-style-priority:99;
	color:purple;
	text-decoration:underline;}
span.EmailStyle17
	{mso-style-type:personal-compose;
	font-family:"Calibri","sans-serif";
	color:windowtext;}
.MsoChpDefault
	{mso-style-type:export-only;
	font-family:"Calibri","sans-serif";}
	font-family:"Calibri","sans-serif";}
@page WordSection1
	{size:8.5in 11.0in;
	margin:1.0in 1.0in 1.0in 1.0in;}
div.WordSection1
	{page:WordSection1;}
--></style><!--[if gte mso 9]><xml>

<o:shapedefaults v:ext="edit" spidmax="1026" />
</xml><![endif]--><!--[if gte mso 9]><xml>
<o:shapelayout v:ext="edit">
<o:idmap v:ext="edit" data="1" />
</o:shapelayout></xml><![endif]--></head><body lang=EN-US link=blue vlink=purple><div class=WordSection1><p class=MsoNormal>'.$dear.'    '.$name.'                                                                           '.$message.'<o:p></o:p></p></div></body></html>';
$touser = get_record('user','id',$addusers->id);  // to give touser mail address

email_to_user( $touser,$fromaddress ,trim($subject), $erolmessage,$erolmessage);
	
	}
	
	}
	}
	//code completed for trigger an email to employee after 30 days of training
	
	//code for trigger an email to requestor after 30 days of training
	$squery=get_records_sql("SELECT * FROM mdl_classroom_sessions where externaltraining='1' and status='completed' and requestor!=''");
	
	foreach($squery as $skey=>$sval)
	{
	$resquery=get_records_sql("SELECT * FROM mdl_classroom_submissions where sessionid='$sval->id' and attend='1' order by id limit 1");
	
	foreach($resquery as $values)
	{
	$sessionid=$values->sessionid;
	$sesdates = get_record('classroom_sessions_dates', 'sessionid', $sessionid, '','','','', 'timestart, timefinish');
	
	$userid=$sval->requestor;
	$timemodified=$sesdates->timefinish;
	$attend=$values->attend;
	$endtime=date("Y-m-d",$timemodified);
	$start = new DateTime($endtime);
	$end = new DateTime("now");
	$interval = date_diff($start, $end);
	echo $countdays=$interval->format('%R%a');
	echo "<br>";
	if($countdays=='31')
	{
$addusers = get_record('user', 'username', $userid, '','','','', 'id, firstname, lastname,username,country');
$fullname = fullname($addusers, true);
$portalid = $addusers->username;
$subject1='Thanks for requested the session';
$sub=$sval->programename;
$fromaddress = 'Training Helpdesk';
$subject= $subject1.'   '.$sub;
				   $dear='Dear';
				   $name=$fullname.' PortalID '.$portalid.",";
				    $message="<div style='margin-top:10px;'><br>$subject.</div><div style='clear:both;'></div><br><div style='margin:top:10px;'>Please Provide feedback by using the below link.<br>
					$CFG->wwwroot/teff/feedback3.php?s=$sval->id&u=$addusers->id</div>";

	$erolmessage= '<html xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:w="urn:schemas-microsoft-com:office:word" xmlns:m="http://schemas.microsoft.com/office/2004/12/omml" xmlns="http://www.w3.org/TR/REC-html40"><head><META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=us-ascii"><meta name=Generator content="Microsoft Word 14 (filtered medium)"><style><!--
/* Font Definitions */
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
/* Style Definitions */
p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin:0in;
	margin-bottom:.0001pt;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";}
a:link, span.MsoHyperlink
	{mso-style-priority:99;
	color:blue;
	text-decoration:underline;}
a:visited, span.MsoHyperlinkFollowed
	{mso-style-priority:99;
	color:purple;
	text-decoration:underline;}
span.EmailStyle17
	{mso-style-type:personal-compose;
	font-family:"Calibri","sans-serif";
	color:windowtext;}
.MsoChpDefault
	{mso-style-type:export-only;
	font-family:"Calibri","sans-serif";}
	font-family:"Calibri","sans-serif";}
@page WordSection1
	{size:8.5in 11.0in;
	margin:1.0in 1.0in 1.0in 1.0in;}
div.WordSection1
	{page:WordSection1;}
--></style><!--[if gte mso 9]><xml>

<o:shapedefaults v:ext="edit" spidmax="1026" />
</xml><![endif]--><!--[if gte mso 9]><xml>
<o:shapelayout v:ext="edit">
<o:idmap v:ext="edit" data="1" />
</o:shapelayout></xml><![endif]--></head><body lang=EN-US link=blue vlink=purple><div class=WordSection1><p class=MsoNormal>'.$dear.'    '.$name.'                                                                           '.$message.'<o:p></o:p></p></div></body></html>';
$touser = get_record('user','id',$addusers->id);  // to give touser mail address

email_to_user( $touser,$fromaddress ,trim($subject), $erolmessage,$erolmessage);
	
	}
	
	}
	}
	//code completed for trigger an email to employee after 30 days of training
			
	$difftime = microtime_diff($totalstarttime, microtime())."\r\n";
		
    mtrace("Execution took ".$difftime." seconds"); 		
	
	$text = 'Total Execution took '.$difftime.'seconds';
	$text = $text."\r\n";
	
	file_put_contents($file, $text, FILE_APPEND | LOCK_EX);
	
	//Unset session variables and destroy it
    @session_unset();
    @session_destroy();
	

    get_mailer('close');
    mtrace("Finished form b cron");

	// finish the IE hack
    if (check_browser_version('MSIE')) {
        echo "</xmp>";
    }

?>


