<?php 
    ini_set('max_execution_time',0);
    $starttime = microtime();

    define('FULLME', 'cron');

    $nomoodlecookie = true;

    if (!isset($_SERVER['REMOTE_ADDR']) && isset($_SERVER['argv'][0])) {
        chdir(dirname($_SERVER['argv'][0]));
    }
	require_once(dirname(__FILE__) . '/../config.php');
    require_once($CFG->libdir.'/adminlib.php');
    require_once($CFG->libdir.'/gradelib.php');

    if (!empty($CFG->showcronsql)) {
        $db->debug = true;
    }
    if (!empty($CFG->showcrondebugging)) {
        $CFG->debug = DEBUG_DEVELOPER;
        $CFG->debugdisplay = true;
    }

/// extra safety
    @session_write_close();

/// check if execution allowed
    if (isset($_SERVER['REMOTE_ADDR'])) { // if the script is accessed via the web.
        if (!empty($CFG->cronclionly)) { 
            // This script can only be run via the cli.
            print_error('cronerrorclionly', 'admin');
            exit;
        }
        // This script is being called via the web, so check the password if there is one.
        if (!empty($CFG->cronremotepassword)) {
            $pass = optional_param('password', '', PARAM_RAW);
            if($pass != $CFG->cronremotepassword) {
                // wrong password.
                print_error('cronerrorpassword', 'admin'); 
                exit;
            }
        }
    }


/// emulate normal session
    $SESSION = new object();
    $USER = get_admin();      /// Temporarily, to provide environment for this script

/// ignore admins timezone, language and locale - use site deafult instead!
    $USER->timezone = $CFG->timezone;
    $USER->lang = '';
    $USER->theme = '';
    course_setup(SITEID);

/// send mime type and encoding
    if (check_browser_version('MSIE')) {
        //ugly IE hack to work around downloading instead of viewing
        @header('Content-Type: text/html; charset=utf-8');
        echo "<xmp>"; //<pre> is not good enough for us here
    } else {
        //send proper plaintext header
        @header('Content-Type: text/plain; charset=utf-8');
    }

/// no more headers and buffers
    while(@ob_end_flush());

/// increase memory limit (PHP 5.2 does different calculation, we need more memory now)
    @raise_memory_limit('1000M');

/// Start output log

    $timenow  = time();

    mtrace("Server Time: ".date('r',$timenow)."\n\n");

	

    mtrace('Starting main gradebook job ...');

					$reminderUserID = "SELECT u.id as id,concat(u.firstname,' ',u.lastname) as name,u.username as portalid
					FROM mdl_course AS c inner JOIN mdl_context AS ctx 
					ON c.id = ctx.instanceid and c.id=7044 inner JOIN mdl_role_assignments AS ra
					ON ra.contextid = ctx.id and ra.roleid='5'
					inner JOIN mdl_user AS u  ON u.id = ra.userid and u.auth='ldap'
                        inner JOIN mdl_groups_members m on m.userid=u.id
                        and m.groupid in(878,879,864,865,866,867,868,874,875,876,877,869,870,871,872,873)";
					
					if ($courseUserRS = get_recordset_sql($reminderUserID))
					{
						while ($UserId = rs_fetch_next_record($courseUserRS)) 
						{
						
							$employeeMessage='<html xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:w="urn:schemas-microsoft-com:office:word" xmlns:m="http://schemas.microsoft.com/office/2004/12/omml" xmlns="http://www.w3.org/TR/REC-html40"><head><META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=us-ascii"><meta name=Generator content="Microsoft Word 14 (filtered medium)"><style><!--
/* Font Definitions */
@font-face
	{font-family:"Microsoft Sans Serif";
	panose-1:2 11 6 4 2 2 2 2 2 4;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
/* Style Definitions */
p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:10.0pt;
	margin-left:0in;
	line-height:115%;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";}
a:link, span.MsoHyperlink
	{mso-style-priority:99;
	color:blue;
	text-decoration:underline;}
a:visited, span.MsoHyperlinkFollowed
	{mso-style-priority:99;
	color:purple;
	text-decoration:underline;}
span.EmailStyle17
	{mso-style-type:personal-compose;
	font-family:"Calibri","sans-serif";
	color:windowtext;}
.MsoChpDefault
	{mso-style-type:export-only;
	font-family:"Calibri","sans-serif";}
@page WordSection1
	{size:8.5in 11.0in;
	margin:1.0in 1.0in 1.0in 1.0in;}
div.WordSection1
	{page:WordSection1;}
--></style><!--[if gte mso 9]><xml>
<o:shapedefaults v:ext="edit" spidmax="1026" />
</xml><![endif]--><!--[if gte mso 9]><xml>
<o:shapelayout v:ext="edit">
<o:idmap v:ext="edit" data="1" />
</o:shapelayout></xml><![endif]--></head><body lang=EN-US link=blue vlink=purple>
<div class=WordSection1><p class=MsoNormal>Dear '.$UserId->name.',<o:p></o:p></p>
<p class=MsoNormal>You are receiving this message because you have not completed your required Code of Business Conduct 2013 training program.&nbsp; 
<span style="color:#953735;mso-style-textfill-fill-color:#953735;mso-style-textfill-fill-alpha:100.0%">
Our records indicate that you are '.$UserId->name.' and '.$UserId->portalid.'.&nbsp; You are on the list of &#8220;Not Complete&#8221; for the Code of Business Conduct. </span>&nbsp;Please log in to the LMS and complete the course <a href="https://lmsqa.portal.nttdatainc.com/course/view.php?id=7044">https://lmsqa.portal.nttdatainc.com/course/view.php?id=7044</a>.<o:p></o:p></p><p class=MsoNormal>Thank you,<o:p></o:p></p><p class=MsoNormal>NTT DATA Compliance Team<o:p></o:p></p></div></body></html>';

							$notakenUserID = "select gg.finalgrade as grade from mdl_grade_grades AS gg
							JOIN mdl_grade_items AS gi
							ON gi.id = gg.itemid and gg.finalgrade = gi.grademax
							AND gi.itemtype = 'course' and gi.courseid=7044
							and gg.userid=$UserId->id";
							$nogradeuser=get_record_sql($notakenUserID);
							if((!$nogradeuser->grade)or($nogradeuser->grade<=0))
							{
							$euser = get_record('user','id',$UserId->id);
							
							email_to_user( $euser,'', 'Code of Business Conduct 2013 training program',$employeeMessage,$employeeMessage);
							mtrace('Mail initiated for employee '.$UserId->name);
							}							
						}
					}
					rs_close($courseUserRS);
	
	//--------------------------------------------------------------------------------------------

	$reminderUserIDm = "SELECT distinct manager_portalid as manager FROM mdl_user
					where mdl_user.id in (SELECT u.id
					FROM mdl_course AS c inner JOIN mdl_context AS ctx
					ON c.id = ctx.instanceid and c.id=7044 inner JOIN mdl_role_assignments AS ra
					ON ra.contextid = ctx.id and ra.roleid='5'
					inner JOIN mdl_user AS u
					ON u.id = ra.userid and u.auth='ldap'
           inner JOIN mdl_groups_members m on m.userid=u.id
           and m.groupid in(878,879,864,865,866,867,868,874,875,876,877,869,870,871,872,873))";
					
	if ($courseManagerRS = get_recordset_sql($reminderUserIDm))
		{
			while ($Manager = rs_fetch_next_record($courseManagerRS)) 
			{
				
				$managerPortal=0;
				$userM = "SELECT u.id as id,concat(u.firstname,' ',u.lastname) as name,u.username as portalid
					FROM mdl_course AS c inner JOIN mdl_context AS ctx
					ON c.id = ctx.instanceid and c.id=7044 inner JOIN mdl_role_assignments AS ra
					ON ra.contextid = ctx.id and ra.roleid='5'
					inner JOIN mdl_user AS u
					ON u.id = ra.userid and u.auth='ldap' and u.manager_portalid=$Manager->manager
					inner JOIN mdl_groups_members m on m.userid=u.id
					and m.groupid in(878,879,864,865,866,867,868,874,875,876,877,869,870,871,872,873)";
					$employeeInfo='';
				if ($userMRS = get_recordset_sql($userM))
				{
					
					while ($userRS = rs_fetch_next_record($userMRS)) 
					{
							$notakenUserID = "select gg.finalgrade as grade from mdl_grade_grades AS gg
							JOIN mdl_grade_items AS gi
							ON gi.id = gg.itemid and gg.finalgrade = gi.grademax
							AND gi.itemtype = 'course' and gi.courseid=7044
							and gg.userid=$userRS->id";
							$nogradeuser=get_record_sql($notakenUserID);
							if((!$nogradeuser->grade)or($nogradeuser->grade<=0))
							{					
							$employeeInfo.='<li>'.$userRS->portalid.' '.$userRS->name.'</li><br/>';
							$managerPortal=1;
							}
					
					}

					if($managerPortal>0)
					{

					$managerUser = get_record('user','username',$Manager->manager);
					$managerUserName=$managerUser->firstname.' '.$managerUser->lastname;
					
				$managerMessage='<html xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:w="urn:schemas-microsoft-com:office:word" xmlns:m="http://schemas.microsoft.com/office/2004/12/omml" xmlns="http://www.w3.org/TR/REC-html40"><head><META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=us-ascii"><meta name=Generator content="Microsoft Word 14 (filtered medium)"><style><!--
				/* Font Definitions */
				@font-face
				{font-family:Calibri;
				panose-1:2 15 5 2 2 2 4 3 2 4;}
				/* Style Definitions */
				p.MsoNormal, li.MsoNormal, div.MsoNormal
				{margin-top:0in;
				margin-right:0in;
				margin-bottom:10.0pt;
				margin-left:0in;
				line-height:115%;
				font-size:11.0pt;
				font-family:"Calibri","sans-serif";}
				a:link, span.MsoHyperlink
				{mso-style-priority:99;
				color:blue;
				text-decoration:underline;}
				a:visited, span.MsoHyperlinkFollowed
				{mso-style-priority:99;
				color:purple;
				text-decoration:underline;}
				span.EmailStyle17
				{mso-style-type:personal-compose;
				font-family:"Calibri","sans-serif";
				color:windowtext;}
				.MsoChpDefault
				{mso-style-type:export-only;
				font-family:"Calibri","sans-serif";}
				@page WordSection1
				{size:8.5in 11.0in;
				margin:1.0in 1.0in 1.0in 1.0in;}
				div.WordSection1
				{page:WordSection1;}
				--></style><!--[if gte mso 9]><xml>
				<o:shapedefaults v:ext="edit" spidmax="1026" />
				</xml><![endif]--><!--[if gte mso 9]><xml>
				<o:shapelayout v:ext="edit">
				<o:idmap v:ext="edit" data="1" />
				</o:shapelayout></xml><![endif]--></head><body lang=EN-US link=blue vlink=purple>
				<div class=WordSection1><p class=MsoNormal>Dear '.$managerUserName.' <o:p></o:p></p>
				<p class=MsoNormal>You are receiving this message because one or more of your employees have not 
				completed their required Code of Business Conduct 2013 training program.&nbsp; 
				The following of your direct reports are on the list of Not Complete for the 
				Code of Business Conduct.</p>
				<p >
				'.$employeeInfo.'
				</p><br/><p class=MsoNormal>Please remind your employees to log in to the LMS and complete 
				the course <a href="https://lmsqa.portal.nttdatainc.com/course/view.php?id=7044">
				https://lmsqa.portal.nttdatainc.com/course/view.php?id=7044</a>.<o:p></o:p></p><p class=MsoNormal>
				Thank you,<o:p></o:p></p><p class=MsoNormal>NTT DATA Compliance Team<o:p></o:p></p>
				<p class=MsoNormal><o:p>&nbsp;</o:p></p></div></body></html>';
						
						email_to_user( $managerUser,'', 'Code of Business Conduct 2013 training program',$managerMessage,$managerMessage);
						mtrace('Mail initiated for manager '.$managerUserName);

					}
				}
			}
		}
	
    mtrace('done.');


    //Unset session variables and destroy it
    @session_unset();
    @session_destroy();

    mtrace("Cron script completed correctly");

    $difftime = microtime_diff($starttime, microtime());
    mtrace("Execution took ".$difftime." seconds"); 

/// finish the IE hack
    if (check_browser_version('MSIE')) {
        echo "</xmp>";
    }

?>
