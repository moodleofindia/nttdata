<?php // $Id: user_bulk_enrol.php
/**
* script for bulk user multy enrol operations
*/
require_once('../../config.php');
require_once($CFG->libdir.'/adminlib.php');

$sort = optional_param('sort', 'fullname', PARAM_ALPHA); //Sort by full name
$dir  = optional_param('dir', 'asc', PARAM_ALPHA);		 //Order to sort (ASC)

admin_externalpage_setup('userbulk');
require_capability('moodle/user:delete', get_context_instance(CONTEXT_SYSTEM));

$return = $CFG->wwwroot.'/'.$CFG->admin.'/user/user_bulk.php';

//If no users selected then return to user_bulk.php
if (empty($SESSION->bulk_users)) { 
    redirect($return);
}
$users = $SESSION->bulk_users; 	//Get users to display
$usertotal = get_users(false); 	//Total number of users registered
$usercount = count($users);		//number of users

admin_externalpage_print_header();

//take user info
foreach ($users as $key => $id) {
    $user = get_record('user', 'id', $id, null, null, null, null, 'id, firstname, lastname, lastaccess');
    $user->fullname = fullname($user, true);
    unset($user->firstname);
    unset($user->lastname);
    $users[$key] = $user;
}

// Need to sort by date
function sort_compare($a, $b) {
    global $sort, $dir;
    if($sort == 'lastaccess') {
        $rez = $b->lastaccess - $a->lastaccess;
    } else {
        $rez = strcasecmp(@$a->$sort, @$b->$sort);
    }
    return $dir == 'desc' ? -$rez : $rez;
}
usort($users, 'sort_compare');

//Take courses data (id, shortname, and fullname)
$courses = get_courses_page('all', 'c.sortorder ASC', 'c.id,c.shortname,c.fullname', $totalcount);
$table->width = "95%";
$columns = array('fullname');
foreach ($courses as $acourse)
{
	$columns[]=$acourse->shortname;
}

//Print columns headers from table
foreach ($columns as $column) {
    $strtitle = get_string($column);
    if ($sort != $column) {
        $columnicon = '';
        $columndir = 'asc';
    } else {
        $columndir = $dir == 'asc' ? 'desc' : 'asc';
        $columnicon = ' <img src="'.$CFG->pixpath.'/t/'.($dir == 'asc' ? 'down' : 'up' ).'.gif" alt="" />';
    }
    $table->head[] = '<a href="user_bulk_enrol.php?sort='.$column.'&amp;dir='.$columndir.'">'.$strtitle.'</a>'.$columnicon;
    $table->align[] = 'left';
}

if( isset( $_POST["processed"] ) )
{
  //Process data form here
  $total = count($courses) * count($users);

  for ( $i = 0; $i < $total; $i++ )
  {
  	$strName = "selected".$i;
  	$selected = $_POST[$strName];
  	$ids = explode(' ', $selected);
  	
  	//echo "id[0]=#".$ids[0]."#id[1]=#".$ids[1]."#id[2]=#".$ids[2]."#";
    if( !empty($ids[2]) )
  	{
  		$context = get_context_instance(CONTEXT_COURSE, $ids[1]);
  		//echo "R:#".$context->id."#";
		//5 = estudent, id user, no group, context id
	    //role_assign($roleid, $userid, $groupid, $contextid, $timestart=0, $timeend=0, $hidden=0, $enrol='manual',$timemodified='')
		if( role_assign(5, $ids[0], 0, $context->id) )
			continue;
  	}
  	else
  	{
  		if( empty($ids[1] ) )
  		{
//  			echo "no hace nada<Br>";
  			continue;
  		}
  		//delete role
  		$context = get_context_instance(CONTEXT_COURSE, $ids[1]);
	  	role_unassign(5, $ids[0], 0, $context->id);
//		echo "desenrola";
  	}  	
//		echo "<br>";
  }
}
echo 	'<script type="text/javascript">
		function my_check(cadena, id1, id2, stat)
		   {
				box = eval("document.multienrol." + cadena);
				box.value = (id1 + " " + id2 + " " + "false");
		   }
		   </script>';

//Form beginning  
echo "<form id=\"multienrol\" name=\"multienrol\" method=\"post\" action=\"user_bulk_enrol.php\">";
echo "<input type=\"hidden\" name=\"processed\" value\"yes\"/>";

$count = 0;
foreach($users as $user) 
{
    $temparray = array (
        '<a href="'.$CFG->wwwroot.'/user/view.php?id='.$user->id.'&amp;course='.SITEID.'">'.$user->fullname.'</a>'   
    );
	$mycourses = get_my_courses($user->id);
	$i = 1;
	foreach($courses as $acourse)
	{
		$temparray[$i]  = '<input type="hidden" name="selected'.$count.'" value="'.$user->id.' '.$acourse->id.'" />';
		if( isset( $mycourses[$acourse->id] ) )
		{
			$temparray[$i] .= "<input type='checkbox' name='selected".$count."' checked='yes' onclick='my_check(\"selected".$count."\",".$user->id.",".$acourse->id.",false)' />";
		}
		else
		{
			$temparray[$i] .= '<input type="checkbox" name="selected'.$count.'" value="'.$user->id.' '.$acourse->id.' '.true.'" />';
		}
		$i++;
		$count++;
	}
	$table->data[] = $temparray;
}
print_heading("$usercount / $usertotal ".get_string('users'));
print_table($table);

//Form ending
echo '<div class="continuebutton">';
echo '<input type="submit" name"multienrolsubmit" value="save changes">';
echo '</div>';
echo "</form>";

admin_externalpage_print_footer();

?>