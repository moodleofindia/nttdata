<?php 

    ini_set('max_execution_time',0);
	$totalstarttime = microtime();

    define('FULLME', 'cron');

    $nomoodlecookie = true;

    if (!isset($_SERVER['REMOTE_ADDR']) && isset($_SERVER['argv'][0])) {
        chdir(dirname($_SERVER['argv'][0]));
    }

    require_once(dirname(__FILE__) . '/../config.php');



    @session_write_close();


    $SESSION = new object();
    $USER = get_admin();      
    $USER->timezone = $CFG->timezone;
    course_setup(SITEID);

	@header('Content-Type: text/plain; charset=utf-8');
	 
    while(@ob_end_flush());

    @ini_set('memory_limit','1024M');


    $timenow  = time();

    mtrace("Server Time: ".date('r',$timenow)."\n\n");
	$filedate =  date("Ymd");  
	$file = 'c:\\data\\subcourselog'.$filedate.'.log';	
	$timenow = date('r',$timenow)."\r\n";
	$timedb=time();
	file_put_contents($file,$timenow, FILE_APPEND | LOCK_EX);

    mtrace("Starting activity modules");
	get_mailer('buffer');
  
            $libfile = "$CFG->dirroot/mod/subcourse/lib.php";
            if (file_exists($libfile)) {
                include_once($libfile);
                $cron_function = "subcourse_cron";
				
				if (function_exists($cron_function)) {
                    mtrace("Processing module function $cron_function ...", '');
                    $pre_dbqueries = null;
                    if (!empty($PERF->dbqueries)) {
                        $pre_dbqueries = $PERF->dbqueries;
                        $pre_time      = microtime(1);
                    }
                    if ($cron_function()) {
					$timedb=mysql_real_escape_string($timedb);
					$upquery=mysql_query("UPDATE mdl_modules set lastcron=$timedb where id=26");
					if (!$upquery) {
							echo mysql_error();
                            mtrace("Error: could not update timestamp for Subcourse");
                        }
					
                    }
                    @ini_set('max_execution_time',0);
                    mtrace("done.");
                }
            }
			
	$difftime = microtime_diff($totalstarttime, microtime())."\r\n";
		
    mtrace("Execution took ".$difftime." seconds"); 		
	
	$text = 'Total Execution took '.$difftime.'seconds';
	$text = $text."\r\n";
	
	file_put_contents($file, $text, FILE_APPEND | LOCK_EX);
	
	//Unset session variables and destroy it
    @session_unset();
    @session_destroy();
	

    get_mailer('close');
    mtrace("Finished Subcourse module");

	// finish the IE hack
    if (check_browser_version('MSIE')) {
        echo "</xmp>";
    }

?>


