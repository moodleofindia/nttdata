<?php 
    ini_set('max_execution_time',0);
    $starttime = microtime();

    define('FULLME', 'cron');

    $nomoodlecookie = true;

    if (!isset($_SERVER['REMOTE_ADDR']) && isset($_SERVER['argv'][0])) {
        chdir(dirname($_SERVER['argv'][0]));
    }
	require_once(dirname(__FILE__) . '/../config.php');
    require_once($CFG->libdir.'/adminlib.php');
    require_once($CFG->libdir.'/gradelib.php');

    if (!empty($CFG->showcronsql)) {
        $db->debug = true;
    }
    if (!empty($CFG->showcrondebugging)) {
        $CFG->debug = DEBUG_DEVELOPER;
        $CFG->debugdisplay = true;
    }

/// extra safety
    @session_write_close();

/// check if execution allowed
    if (isset($_SERVER['REMOTE_ADDR'])) { // if the script is accessed via the web.
        if (!empty($CFG->cronclionly)) { 
            // This script can only be run via the cli.
            print_error('cronerrorclionly', 'admin');
            exit;
        }
        // This script is being called via the web, so check the password if there is one.
        if (!empty($CFG->cronremotepassword)) {
            $pass = optional_param('password', '', PARAM_RAW);
            if($pass != $CFG->cronremotepassword) {
                // wrong password.
                print_error('cronerrorpassword', 'admin'); 
                exit;
            }
        }
    }


/// emulate normal session
    $SESSION = new object();
    $USER = get_admin();      /// Temporarily, to provide environment for this script

/// ignore admins timezone, language and locale - use site deafult instead!
    $USER->timezone = $CFG->timezone;
    $USER->lang = '';
    $USER->theme = '';
    course_setup(SITEID);

/// send mime type and encoding
    if (check_browser_version('MSIE')) {
        //ugly IE hack to work around downloading instead of viewing
        @header('Content-Type: text/html; charset=utf-8');
        echo "<xmp>"; //<pre> is not good enough for us here
    } else {
        //send proper plaintext header
        @header('Content-Type: text/plain; charset=utf-8');
    }

/// no more headers and buffers
    while(@ob_end_flush());

/// increase memory limit (PHP 5.2 does different calculation, we need more memory now)
    @ini_set('memory_limit','1000M');

/// Start output log

    $timenow  = time();

    mtrace("Server Time: ".date('r',$timenow)."\n\n");

	

    mtrace('Starting main gradebook job ...');

//$courseUserRS = get_recordset_sql($reminderUserID);
//Naga added to send the details in excel sheet 
$xlshead=pack("s*", 0x809, 0x8, 0x0, 0x10, 0x0, 0x0);
$xlsfoot=pack("s*", 0x0A, 0x00);
function xlsCell($row,$col,$val) {
  $len=strlen($val);
  return pack("s*",0x204,8+$len,$row,$col,0x0,$len).$val;
}

$data.=xlsCell(0,0,"Name") . xlsCell(0,1,"Portal ID") . xlsCell(0,2,"Course Name") . xlsCell(0,3,"Course Completion date") . xlsCell(0,4,"Status") . xlsCell(0,5,"Question") . xlsCell(0,6,"Response") ;
$rowNumber=0;
//$query = "SELECT t.id as id ,t.value as value, u.username as portalid FROM mdl_scorm_scoes_track t join mdl_user u on u.id=t.userid where t.scormid=772 and t.element='cmi.core.lesson_status'";
//$query = "select t.value,s.name,u.username,concat(u.firstname,' ',u.lastname)as name from mdl_scorm_scoes_track t join mdl_scorm s on s.id=t.scormid join mdl_user u on u.id=t.userid where t.scormid=1306 and t.element='cmi.interactions_0.student_response'";
$query="select concat(u.firstname,' ',u.lastname)as name,u.username as portalid,s.name as coursename,from_unixtime(gg.timemodified) as completiondate,t.value as response from mdl_scorm_scoes_track t join mdl_scorm s on s.id=t.scormid join mdl_user u on u.id=t.userid JOIN mdl_grade_items AS gi on gi.itemname=s.name 
join mdl_grade_grades AS gg on gg.userid= t.userid and gi.id = gg.itemid and gg.finalgrade <= gi.grademax where t.scormid=1380 and t.element='cmi.interactions_0.student_response' and t.userid not in (select distinct(userid) from mdl_scorm_scoes_track where value='SENT TO LEGAL' and scormid=1380)";
$query1=mysql_query($query);

  while($books=  mysql_fetch_object($query1))
  {
  $rowNumber=$rowNumber+1;
  $name=$books->name;
   $portalid=$books->portalid;
  $coursename=$books->coursename;
    $completiondate=$books->completiondate;
	$question1='Please describe any known conflicts of interest,if any';
	  $response=$books->response;
  $data.=xlsCell($rowNumber,0,$name) . xlsCell($rowNumber,1,$portalid) . xlsCell($rowNumber,2,$coursename) . xlsCell($rowNumber,3,$completiondate). xlsCell($rowNumber,4,'Completed') . xlsCell($rowNumber,5,$question1) . xlsCell($rowNumber,6,$response); 

  }

//$uniqueid=  time();
//$filen="CoBC VP Responses".$uniqueid;
$filen="CoBC VP Responses";
$filename="$filen.xls";
$conte=$xlshead . $data . $xlsfoot;//This is previous not there please place it
//file_put_contents("C://data//".$filename, $conte);//This is the command to save your file in server
file_put_contents("C://inetpub//LMSdata//COBC//".$filename, $conte);//This is the command to save your file in server
$content = chunk_split(base64_encode($conte));
$uid = md5(uniqid(time())); 
// $mailto="nagabhushanam11.morla@nttdata.com";
// $from_mail = "Gokula.Krishnan@nttdata.com";
$message1 ='<html xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:w="urn:schemas-microsoft-com:office:word" xmlns:m="http://schemas.microsoft.com/office/2004/12/omml" xmlns="http://www.w3.org/TR/REC-html40"><head><META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=us-ascii"><meta name=Generator content="Microsoft Word 14 (filtered medium)"><style><!--
/* Font Definitions */
@font-face
	{font-family:"Microsoft Sans Serif";
	panose-1:2 11 6 4 2 2 2 2 2 4;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
/* Style Definitions */
p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:10.0pt;
	margin-left:0in;
	line-height:115%;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";}
a:link, span.MsoHyperlink
	{mso-style-priority:99;
	color:blue;
	text-decoration:underline;}
a:visited, span.MsoHyperlinkFollowed
	{mso-style-priority:99;
	color:purple;
	text-decoration:underline;}
span.EmailStyle17
	{mso-style-type:personal-compose;
	font-family:"Calibri","sans-serif";
	color:windowtext;}
.MsoChpDefault
	{mso-style-type:export-only;
	font-family:"Calibri","sans-serif";}
@page WordSection1
	{size:8.5in 11.0in;
	margin:1.0in 1.0in 1.0in 1.0in;}
div.WordSection1
	{page:WordSection1;}
--></style><!--[if gte mso 9]><xml>
<o:shapedefaults v:ext="edit" spidmax="1026" />
</xml><![endif]--><!--[if gte mso 9]><xml>
<o:shapelayout v:ext="edit">
<o:idmap v:ext="edit" data="1" />
</o:shapelayout></xml><![endif]--></head><body lang=EN-US link=blue vlink=purple>
<div class=WordSection1><p class=MsoNormal >Hi,<o:p></o:p></p><p class=MsoNormal>This message is generated from the CATALYS system capturing Code of Business Conduct input from the Vice Presidents version of the course.  Attached are the reports for the current week.  Please acknowledge receipt of the attached and confirm that you have the records presented here. 
Your tracking system will be the system of record and the entries will be replaced in CATALYS with the message accordingly.<o:p></o:p></p>
<p class=MsoNormal>Should you have any questions about this data feed, please contact Janet Lahlou, Talent Development.&nbsp; 
<p class=MsoNormal>Thank you,<o:p></o:p></p><p class=MsoNormal>CATALYS System Team<o:p></o:p></p></div></body></html>';

$message2 ='<html xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:w="urn:schemas-microsoft-com:office:word" xmlns:m="http://schemas.microsoft.com/office/2004/12/omml" xmlns="http://www.w3.org/TR/REC-html40"><head><META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=us-ascii"><meta name=Generator content="Microsoft Word 14 (filtered medium)"><style><!--
/* Font Definitions */
@font-face
	{font-family:"Microsoft Sans Serif";
	panose-1:2 11 6 4 2 2 2 2 2 4;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
/* Style Definitions */
p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:10.0pt;
	margin-left:0in;
	line-height:115%;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";}
a:link, span.MsoHyperlink
	{mso-style-priority:99;
	color:blue;
	text-decoration:underline;}
a:visited, span.MsoHyperlinkFollowed
	{mso-style-priority:99;
	color:purple;
	text-decoration:underline;}
span.EmailStyle17
	{mso-style-type:personal-compose;
	font-family:"Calibri","sans-serif";
	color:windowtext;}
.MsoChpDefault
	{mso-style-type:export-only;
	font-family:"Calibri","sans-serif";}
@page WordSection1
	{size:8.5in 11.0in;
	margin:1.0in 1.0in 1.0in 1.0in;}
div.WordSection1
	{page:WordSection1;}
--></style><!--[if gte mso 9]><xml>
<o:shapedefaults v:ext="edit" spidmax="1026" />
</xml><![endif]--><!--[if gte mso 9]><xml>
<o:shapelayout v:ext="edit">
<o:idmap v:ext="edit" data="1" />
</o:shapelayout></xml><![endif]--></head><body lang=EN-US link=blue vlink=purple>
<div class=WordSection1><p class=MsoNormal >Hi,<o:p></o:p></p><p class=MsoNormal>This message is generated from the CATALYS system capturing Code of Business Conduct input from the Vice Presidents version of the course.  There are no new records for the  VP level Course.
<o:p></o:p></p>
<p class=MsoNormal>Should you have any questions about this data feed, please contact Janet Lahlou, Talent Development.&nbsp; 
<p class=MsoNormal>Thank you,<o:p></o:p></p><p class=MsoNormal>CATALYS System Team<o:p></o:p></p></div></body></html>';
// $header = "From: Place Website Name <".$from_mail.">\r\n";
// $header .= "Reply-To: ".$from_mail."\r\n";
//$header .= "MIME-Version: 1.0\r\n";
$header .= "CoBC 2014 Training program Responses";
$header .= "CoBC 2014 Training program Responses\r\n";
$header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\r\n\r\n";
$header .= "This is a multi-part message in MIME format.\r\n";
$header .= "--".$uid."\r\n";
$header .= "Content-type:text/html; charset=iso-8859-1\r\n";
$header .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
$header .= $message1."\r\n\r\n";
$header .= "--".$uid."\r\n";
$header .= "Content-Type: application/vnd.ms-excel; name=\"".$filename."\"\r\n";
$header .= "Content-Transfer-Encoding: base64\r\n";
$header .= "Content-Disposition: attachment; filename=\"".$filename."\"\r\n\r\n";
$header .= $content."\r\n\r\n";
$header .= "--".$uid."--";

$tmpfile = '/COBC/'.$filename;
$file=$filename;

$reminderUserID = "SELECT * from mdl_user where username in ('COBC14 ResponsetoLegal')";
					
					if ($courseUserRS = get_recordset_sql($reminderUserID))
					{
						while ($UserId = rs_fetch_next_record($courseUserRS)) 
						{
							mtrace($UserId->id);
							$euser = get_record('user','id',$UserId->id);
								if(empty($query)){
	
									email_to_user( $euser,'','COBC Compliance Team Report from LMS',$message2,$message2);
													}
													else{
									email_to_user( $euser,'','COBC Compliance Team Report from LMS',$message1,$message1,$tmpfile,$file);
														}
																	//email_to_user( $euser,'CoBC 2014 training program Responses',$message1);
						
						}
					}	
    mtrace('done.');


    //Unset session variables and destroy it
    @session_unset();
    @session_destroy();

    mtrace("Cron script completed correctly");

    $difftime = microtime_diff($starttime, microtime());
    mtrace("Execution took ".$difftime." seconds"); 

/// finish the IE hack
    if (check_browser_version('MSIE')) {
        echo "</xmp>";
    }

?>
