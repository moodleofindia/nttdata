<?php  // $Id: index.php,v 1.201.2.10 2009/04/25 21:18:24 stronk7 Exp $
       // index.php - the front page.



    require_once('../../../config.php');
    require_once($CFG->dirroot .'/course/lib.php');
    require_once($CFG->dirroot .'/lib/blocklib.php');
	
 function csverror($message, $link='') {
        global $CFG, $SESSION;
    
        print_header(get_string('error'));
        echo '<br />';
    
        $message = clean_text($message);
    
        print_simple_box('<span style="font-family:monospace;color:#000000;">'.$message.'</span>', 'center', '', '#FFBBBB', 5, 'errorbox');
    
        print_footer();
        die;
    }
	
    if (empty($SITE)) {
        redirect($CFG->wwwroot .'/'. $CFG->admin .'/index.php');
    }


    if ($CFG->forcelogin) {
        require_login();
    } else {
        user_accesstime_log();
    }
// Check permissions.
require_login();
$systemcontext = get_context_instance(CONTEXT_SYSTEM);
require_capability('report/cpd:userview', $systemcontext);


    print_header($SITE->fullname, $SITE->fullname, 'home', '',
                 '<meta name="description" content="'. strip_tags(format_text($SITE->summary, FORMAT_HTML)) .'" />',
                 true, '', user_login_string($SITE).$langmenu);

?>
<html>
<head>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>

<script type="text/javascript" src="ddaccordion.js">



</script>
<script type="text/javascript">
var newwindow;
function poptastic(url)
{
	newwindow=window.open(url,'name','height=700,width=1000');
	if (window.focus) {newwindow.focus()}
}
</script>
<script type="text/javascript">


ddaccordion.init({
	headerclass: "submenuheader", //Shared CSS class name of headers group
	contentclass: "submenu", //Shared CSS class name of contents group
	revealtype: "click", //Reveal content when user clicks or onmouseover the header? Valid value: "click", "clickgo", or "mouseover"
	mouseoverdelay: 200, //if revealtype="mouseover", set delay in milliseconds before header expands onMouseover
	collapseprev: true, //Collapse previous content (so only one open at any time)? true/false 
	defaultexpanded: [], //index of content(s) open by default [index1, index2, etc] [] denotes no content
	onemustopen: false, //Specify whether at least one header should be open always (so never all headers closed)
	animatedefault: false, //Should contents open by default be animated into view?
	persiststate: true, //persist state of opened contents within browser session?
	toggleclass: ["", ""], //Two CSS classes to be applied to the header when it's collapsed and expanded, respectively ["class1", "class2"]
	togglehtml: ["suffix", "<img src='plus.gif' class='statusicon' />", "<img src='minus.gif' class='statusicon' />"], //Additional HTML added to the header when it's collapsed and expanded, respectively  ["position", "html1", "html2"] (see docs)
	animatespeed: "fast", //speed of animation: integer in milliseconds (ie: 200), or keywords "fast", "normal", or "slow"
	oninit:function(headers, expandedindices){ //custom code to run when headers have initalized
		//do nothing
	},
	onopenclose:function(header, index, state, isuseractivated){ //custom code to run whenever a header is opened or closed
		//do nothing
	}
})


</script>


<style type="text/css">

.glossymenu{
margin: 5px 0;
padding: 0;
width: 300px; /*width of menu*/
border: 1px solid #9A9A9A;
border-bottom-width: 0;
}

.glossymenu a.menuitem{
background: black url(glossyback.gif) repeat-x bottom left;
font: bold 14px "Lucida Grande", "Trebuchet MS", Verdana, Helvetica, sans-serif;
color: white;
display: block;
position: relative; /*To help in the anchoring of the ".statusicon" icon image*/
width: auto;
padding: 4px 0;
padding-left: 10px;
text-decoration: none;
}


.glossymenu a.menuitem:visited, .glossymenu .menuitem:active{
color: white;
}

.glossymenu a.menuitem .statusicon{ /*CSS for icon image that gets dynamically added to headers*/
position: absolute;
top: 5px;
right: 5px;
border: none;
}

.glossymenu a.menuitem:hover{
background-image: url(glossyback2.gif);
}

.glossymenu div.submenu{ /*DIV that contains each sub menu*/
background: white;
}

.glossymenu div.submenu ul{ /*UL of each sub menu*/
list-style-type: none;
margin: 0;
padding: 0;
}

.glossymenu div.submenu ul li{
border-bottom: 1px solid blue;
}

.glossymenu div.submenu ul li a{
display: block;
font: normal 13px "Lucida Grande", "Trebuchet MS", Verdana, Helvetica, sans-serif;
color: black;
text-decoration: none;
padding: 2px 0;
padding-left: 10px;
}

.glossymenu div.submenu ul li a:hover{
background: #DFDCCB;
colorz: white;
}

</style>
</head>
<body>


<table>
<tr>

<td>
<div class="glossymenu">

<a class="menuitem submenuheader" href="" >ILT India Reports</a>
<div class="submenu">
	<ul>
<li><a href="javascript:poptastic('http://lms102.kds.keane.com:8080/Reports/frameset?__report=GlobalMonthlyStatus.rptdesign');">Global Monthly Status report</a></li>
<li><a href="javascript:poptastic('http://lms102.kds.keane.com:8080/Reports/frameset?__report=Feedback.rptdesign');">ILT Feedback report - India</a></li>
<li><a href="javascript:poptastic('http://lms102.kds.keane.com:8080/Reports/frameset?__report=WebinarFeedback.rptdesign');">Webinar Feedback report</a></li>
<li><a href="javascript:poptastic('http://lms102.kds.keane.com:8080/Reports/frameset?__report=ELearning.rptdesign');">E-Learning report</a></li>
<li><a href="javascript:poptastic('http://lms102.kds.keane.com:8080/Reports/frameset?__report=ELearningbyCourse.rptdesign');">E-Learning by Course Report</a></li>
<li><a href="javascript:poptastic('http://lms102.kds.keane.com:8080/Reports/frameset?__report=Resource.rptdesign');">Resource Report</a></li>
<li><a href="javascript:poptastic('http://lms102.kds.keane.com:8080/Reports/frameset?__report=SkillsoftCourseCompletion.rptdesign');">Skillsoft Course Completion Report</a></li>
<li><a href="javascript:poptastic('http://lms102.kds.keane.com:8080/Reports/frameset?__report=SkillsoftUserProgress.rptdesign');">Skillsoft User Progress Report</a></li>
<li><a href="javascript:poptastic('http://lms102.kds.keane.com:8080/Reports/frameset?__report=EmployeeFeedback.rptdesign');">Employee feedback report</a></li>
<li><a href="javascript:poptastic('http://lms102.kds.keane.com:8080/Reports/frameset?__report=MissingFeedback.rptdesign');">Missing Feedback report</a></li>
<li><a href="javascript:poptastic('http://lms102.kds.keane.com:8080/Reports/frameset?__report=ILTUserTraining.rptdesign');">ILT User Training report</a></li>
<li><a href="javascript:poptastic('http://lms102.kds.keane.com:8080/Reports/frameset?__report=TrainersSession.rptdesign');">Trainers Session report</a></li>
<li><a href="javascript:poptastic('http://lms102.kds.keane.com:8080/Reports/frameset?__report=ILTUserTrainingbyUser.rptdesign');">ILT User Training Report by User</a></li>
<li><a href="javascript:poptastic('http://lms102.kds.keane.com:8080/Reports/frameset?__report=TrainerReportbyTrainer.rptdesign');">Trainer Report by trainer</a></li>
<li><a href="javascript:poptastic('http://lms102.kds.keane.com:8080/Reports/frameset?__report=ILTCalender.rptdesign');">ILT Calendar</a></li>
<li><a href="javascript:poptastic('http://lms102.kds.keane.com:8080/Reports/frameset?__report=MonthlyStatus.rptdesign');">Monthly ILT status report - India</a></li>
<li><a href="javascript:poptastic('http://lms102.kds.keane.com:8080/Reports/frameset?__report=DSR.rptdesign');">Daily status report - India</a></li>
<li><a href="javascript:poptastic('http://lms102.kds.keane.com:8080/Reports/frameset?__report=CertificationObjectiveTestResults.rptdesign');">Certification Objective Test Results report</a></li>
<li><a href="javascript:poptastic('http://lms102.kds.keane.com:8080/Reports/frameset?__report=CertificationCourseCount.rptdesign');">Certification Course Count report</a></li>
<li><a href="javascript:poptastic('http://lms102.kds.keane.com:8080/Reports/frameset?__report=CertificationCount.rptdesign');">Certification Count report</a></li>
<li><a href="javascript:poptastic('http://lms102.kds.keane.com:8080/Reports/frameset?__report=ActiveCertifiedUser.rptdesign');">Active Certified User Report</a></li>
<li><a href="javascript:poptastic('http://lms102.kds.keane.com:8080/Reports/frameset?__report=CancelReasons.rptdesign');">Cancel Reasons report</a></li>
<li><a href="javascript:poptastic('http://lms102.kds.keane.com:8080/Reports/frameset?__report=TrainingHistory.rptdesign');">Consolidated Training History</a></li>
<li><a href="javascript:poptastic('http://lms102.kds.keane.com:8080/Reports/frameset?__report=ELearningTime.rptdesign');">E-Learning Time Report</a></li>
<li><a href="javascript:poptastic('http://lms102.kds.keane.com:8080/Reports/frameset?__report=Quiz.rptdesign');">Quiz report</a></li>
<li><a href="javascript:poptastic('http://lms102.kds.keane.com:8080/Reports/frameset?__report=Assignment.rptdesign');">Assignment Report</a></li>
<li><a href="javascript:poptastic('http://lms102.kds.keane.com:8080/Reports/frameset?__report=ILTAbsentees.rptdesign');">ILT Absentees Report</a></li>
<li><a href="javascript:poptastic('http://lms102.kds.keane.com:8080/Reports/frameset?__report=TakeAttendance.rptdesign');">ILT Attendance Report</a></li>
<li><a target = "_blank" href="https://lmsqa.portal.nttdatainc.com/DashboardReport.php">Dashboard Report</a></li>
	
	</ul>
</div>


</td>
</tr>
</table>

</body>
<?php
    print_footer('home');     // Please do not modify this line
?>