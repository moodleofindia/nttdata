<?xml version='1.0' encoding='utf-8'?>
<Session version="1.0" id="30543939_2447941947" client="WindowsUpdateAgent" options="816" currentPhase="1" lastSuccessfulState="Complete" pendingFollower="true" retry="true" Queued="2016/09/16/13:55:55" Started="2016/09/16/13:55:55" Complete="2016/09/17/12:02:22" status="0x0">
    <Tasks>
        <Phase seq="1">
            <package id="Package_for_KB3080079~31bf3856ad364e35~amd64~~6.1.1.1" name="KB3080079" targetState="Installed" options="4"/>
        </Phase>
    </Tasks>
    <Actions>
        <Phase seq="1" rebootRequired="true" Staged="2016/09/16/13:56:21" Installed="2016/09/16/13:57:23" ShutdownStart="2016/09/17/11:52:31" ShutdownFinish="2016/09/17/11:55:45" Startup="2016/09/17/11:58:58" StartupFinish="2016/09/17/12:02:22">
            <Stage package="Package_32_for_KB3080079~31bf3856ad364e35~amd64~~6.1.1.1" update="3080079-86_neutral_LDR"/>
            <Stage package="Package_32_for_KB3080079~31bf3856ad364e35~amd64~~6.1.1.1" update="3080079-87_neutral_LDR"/>
            <Stage package="Package_33_for_KB3080079~31bf3856ad364e35~amd64~~6.1.1.1" update="3080079-88_neutral_LDR"/>
            <Stage package="Package_33_for_KB3080079~31bf3856ad364e35~amd64~~6.1.1.1" update="3080079-89_neutral_LDR"/>
            <Stage package="Package_51_for_KB3080079~31bf3856ad364e35~amd64~~6.1.1.1" update="3080079-124_neutral_LDR"/>
            <Stage package="Package_51_for_KB3080079~31bf3856ad364e35~amd64~~6.1.1.1" update="3080079-125_neutral_LDR"/>
            <Stage package="Package_51_for_KB3080079~31bf3856ad364e35~amd64~~6.1.1.1" update="3080079-126_neutral_GDR"/>
            <Stage package="Package_52_for_KB3080079~31bf3856ad364e35~amd64~~6.1.1.1" update="3080079-127_neutral_GDR"/>
            <Stage package="Package_52_for_KB3080079~31bf3856ad364e35~amd64~~6.1.1.1" update="3080079-128_neutral_LDR"/>
            <Stage package="Package_52_for_KB3080079~31bf3856ad364e35~amd64~~6.1.1.1" update="3080079-129_neutral_LDR"/>
            <Stage package="Package_53_for_KB3080079~31bf3856ad364e35~amd64~~6.1.1.1" update="3080079-130_neutral_LDR"/>
            <Stage package="Package_53_for_KB3080079~31bf3856ad364e35~amd64~~6.1.1.1" update="3080079-131_neutral_LDR"/>
            <Stage package="Package_53_for_KB3080079~31bf3856ad364e35~amd64~~6.1.1.1" update="3080079-132_neutral_GDR"/>
            <Stage package="Package_54_for_KB3080079~31bf3856ad364e35~amd64~~6.1.1.1" update="3080079-133_neutral_LDR"/>
            <Stage package="Package_54_for_KB3080079~31bf3856ad364e35~amd64~~6.1.1.1" update="3080079-134_neutral_LDR"/>
            <Stage package="Package_54_for_KB3080079~31bf3856ad364e35~amd64~~6.1.1.1" update="3080079-135_neutral_GDR"/>
            <Stage package="Package_55_for_KB3080079~31bf3856ad364e35~amd64~~6.1.1.1" update="3080079-136_neutral_LDR"/>
            <Stage package="Package_55_for_KB3080079~31bf3856ad364e35~amd64~~6.1.1.1" update="3080079-137_neutral_LDR"/>
            <Stage package="Package_55_for_KB3080079~31bf3856ad364e35~amd64~~6.1.1.1" update="3080079-138_neutral_GDR"/>
            <Stage package="Package_80_for_KB3080079~31bf3856ad364e35~amd64~~6.1.1.1" update="3080079-211_neutral_LDR"/>
            <Stage package="Package_80_for_KB3080079~31bf3856ad364e35~amd64~~6.1.1.1" update="3080079-212_neutral_LDR"/>
            <Stage package="Package_80_for_KB3080079~31bf3856ad364e35~amd64~~6.1.1.1" update="3080079-213_neutral_GDR"/>
            <Stage package="Package_81_for_KB3080079~31bf3856ad364e35~amd64~~6.1.1.1" update="3080079-214_neutral_LDR"/>
            <Stage package="Package_81_for_KB3080079~31bf3856ad364e35~amd64~~6.1.1.1" update="3080079-215_neutral_LDR"/>
            <Stage package="Package_119_for_KB3080079~31bf3856ad364e35~amd64~~6.1.1.1" update="3080079-355_neutral_LDR"/>
            <Stage package="Package_119_for_KB3080079~31bf3856ad364e35~amd64~~6.1.1.1" update="3080079-356_neutral_LDR"/>
            <Stage package="Package_120_for_KB3080079~31bf3856ad364e35~amd64~~6.1.1.1" update="3080079-357_neutral_LDR"/>
            <Stage package="Package_120_for_KB3080079~31bf3856ad364e35~amd64~~6.1.1.1" update="3080079-358_neutral_LDR"/>
            <Stage package="Package_121_for_KB3080079~31bf3856ad364e35~amd64~~6.1.1.1" update="3080079-359_neutral_LDR"/>
            <Stage package="Package_121_for_KB3080079~31bf3856ad364e35~amd64~~6.1.1.1" update="3080079-360_neutral_LDR"/>
            <Stage package="Package_121_for_KB3080079~31bf3856ad364e35~amd64~~6.1.1.1" update="3080079-361_neutral_GDR"/>
            <Stage package="Package_122_for_KB3080079~31bf3856ad364e35~amd64~~6.1.1.1" update="3080079-362_neutral_LDR"/>
            <Stage package="Package_122_for_KB3080079~31bf3856ad364e35~amd64~~6.1.1.1" update="3080079-363_neutral_LDR"/>
            <Stage package="Package_123_for_KB3080079~31bf3856ad364e35~amd64~~6.1.1.1" update="3080079-364_neutral_LDR"/>
            <Stage package="Package_123_for_KB3080079~31bf3856ad364e35~amd64~~6.1.1.1" update="3080079-365_neutral_LDR"/>
            <Stage package="Package_124_for_KB3080079~31bf3856ad364e35~amd64~~6.1.1.1" update="3080079-366_neutral_LDR"/>
            <Stage package="Package_124_for_KB3080079~31bf3856ad364e35~amd64~~6.1.1.1" update="3080079-367_neutral_LDR"/>
            <Stage package="Package_125_for_KB3080079~31bf3856ad364e35~amd64~~6.1.1.1" update="3080079-368_neutral_LDR"/>
            <Stage package="Package_125_for_KB3080079~31bf3856ad364e35~amd64~~6.1.1.1" up