<?php 
    ini_set('max_execution_time',0);
    $starttime = microtime();

    define('FULLME', 'cron');

    $nomoodlecookie = true;

    if (!isset($_SERVER['REMOTE_ADDR']) && isset($_SERVER['argv'][0])) {
        chdir(dirname($_SERVER['argv'][0]));
    }
	require_once(dirname(__FILE__) . '/../config.php');
    require_once($CFG->libdir.'/adminlib.php');
    require_once($CFG->libdir.'/gradelib.php');

    if (!empty($CFG->showcronsql)) {
        $db->debug = true;
    }
    if (!empty($CFG->showcrondebugging)) {
        $CFG->debug = DEBUG_DEVELOPER;
        $CFG->debugdisplay = true;
    }

/// extra safety
    @session_write_close();

/// check if execution allowed
    if (isset($_SERVER['REMOTE_ADDR'])) { // if the script is accessed via the web.
        if (!empty($CFG->cronclionly)) { 
            // This script can only be run via the cli.
            print_error('cronerrorclionly', 'admin');
            exit;
        }
        // This script is being called via the web, so check the password if there is one.
        if (!empty($CFG->cronremotepassword)) {
            $pass = optional_param('password', '', PARAM_RAW);
            if($pass != $CFG->cronremotepassword) {
                // wrong password.
                print_error('cronerrorpassword', 'admin'); 
                exit;
            }
        }
    }


/// emulate normal session
    $SESSION = new object();
    $USER = get_admin();      /// Temporarily, to provide environment for this script

/// ignore admins timezone, language and locale - use site deafult instead!
    $USER->timezone = $CFG->timezone;
    $USER->lang = '';
    $USER->theme = '';
    course_setup(SITEID);

/// send mime type and encoding
    if (check_browser_version('MSIE')) {
        //ugly IE hack to work around downloading instead of viewing
        @header('Content-Type: text/html; charset=utf-8');
        echo "<xmp>"; //<pre> is not good enough for us here
    } else {
        //send proper plaintext header
        @header('Content-Type: text/plain; charset=utf-8');
    }

/// no more headers and buffers
    while(@ob_end_flush());

/// increase memory limit (PHP 5.2 does different calculation, we need more memory now)
    @raise_memory_limit('1000M');

/// Start output log

    $timenow  = time();

    mtrace("Server Time: ".date('r',$timenow)."\n\n");

	

    mtrace('Starting main gradebook job ...');

					$reminderUserID = "SELECT u.id as Userid,u.username as PortalID,
										u.firstname as  EmpFirstName,
										u.lastname as  EmpLastName,
										u.country as EmpCountry,
										u.source as LegacyCompany,
										u.bu as BU,
										u.grade as Grade,
										u.ru as RU,
										u.manager_portalid as ManagerPortalID,
										(select mdl_user.firstname from mdl_user where username=u.manager_portalid
										)as ManagerFirstName,
										(select mdl_user.lastname from mdl_user where username=u.manager_portalid
										)as ManagerLastName,
										u.hireddate as HiredDate,
										u.mandatorytrainings as RequiredTrainingHours
										from mdl_user u
										where  u.auth = 'ldap' and u.city not in('MISI','DSKLS') group by u.id ;";
					
					if ($courseUserRS = get_records_sql($reminderUserID))
					{
		
						  $deletesql='delete from  mdl_training_report;';
						  execute_sql($deletesql,$feedback=true);
						  foreach($courseUserRS as $record	)
						  {
							  $record->EmpFirstName = mysql_real_escape_string($record->EmpFirstName);
							  $record->EmpLastName = mysql_real_escape_string($record->EmpLastName);
							  $record->ManagerFirstName = mysql_real_escape_string($record->ManagerFirstName);
							  $record->ManagerLastName = mysql_real_escape_string($record->ManagerLastName);
							  $resultDisableEmployees = insert_record('training_report', $record);
							if($resultDisableEmployees)
							{
								mtrace("Employee inserted sucessfully ! \n");
							}
							else
							{
								mtrace("Error. Employee not inserted sucessfully ! \n");
							}
						}
					}
					//rs_close($courseUserRS);
	
	//--------------------------------------------------------------------------------------------

	
	
				mtrace('done.');
				$userid="select id,Userid,PortalID from mdl_training_report";
				if ($scormdu = get_records_sql($userid))
				{
					foreach($scormdu as $record	)
					{
					//$recodid=$record->Userid
					
						$scorm = get_records_sql("SELECT  u.id,st.id,(sc.duration) as 'Duration'
												FROM mdl_scorm_scoes_track AS st
												JOIN mdl_scorm_scoes ssc on ssc.id=st.scoid and ssc.scormtype='sco'
												JOIN mdl_scorm AS sc ON sc.id=st.scormid
												JOIN mdl_course AS c ON c.id=sc.course
												JOIN mdl_user AS u ON st.userid=u.id and u.id=$record->Userid
												and st.timemodified >= unix_timestamp('2015/4/1')
												and st.timemodified < unix_timestamp('2016/03/1')
												where st.element='cmi.core.lesson_status'
												and st.value in ('completed','passed') group by st.scormid,u.id");
					
					$scormduration=0;
					if ($scorm)
						{
							foreach($scorm as $recordval) 
							{
					
								$scormduration = $scormduration + $recordval->Duration;
								continue;
							}
							$insertscorm="update mdl_training_report set scorm = $scormduration where Userid = $record->Userid";

							$resultInsert = execute_sql($insertscorm, $feedback=true);
						
						
						}
				
					}
				}
				mtrace('scorm.');
				$userid="select id,Userid,PortalID from mdl_training_report";
				if ($resourcedu = get_records_sql($userid))
				{
					foreach($resourcedu as $record	) 
					{
					//$recodid=$record->Userid
						$resource = get_records_sql("SELECT  u.id,  sum(r.duration) as 'Duration'
													FROM mdl_grade_grades g join mdl_grade_items i
													on  g.itemid=i.id
													and g.rawgrade=g.rawgrademax
													and i.itemmodule in ('resource')
													join mdl_resource r on r.id=i.iteminstance
													JOIN mdl_course AS c ON c.id=r.course
													join mdl_user u on u.id=g.userid and u.id=$record->Userid
													and g.timemodified >= unix_timestamp('2015/4/1')
													and g.timemodified < unix_timestamp('2016/03/1')");
						if ($resource)
						{
							foreach($resource as $nascorm	) 
							{
								$insertscorm="update mdl_training_report set resource = $nascorm->Duration where Userid = $record->Userid";
								$resultInsert = execute_sql($insertscorm, $feedback=true);

							}
						
						}
					}
				
				}
				mtrace('resource.');
				$userid="select id,Userid,PortalID from mdl_training_report";
				if ($mplayerdu = get_records_sql($userid))
				{
					foreach($mplayerdu as $record	) 
					{
					//$recodid=$record->Userid
						$mplayer = get_records_sql("SELECT  u.id,  sum(m.duration) as 'Duration'
													FROM mdl_grade_grades join mdl_grade_items
													on  mdl_grade_grades.itemid=mdl_grade_items.id
													and mdl_grade_grades.rawgrade=mdl_grade_grades.rawgrademax
													and mdl_grade_items.itemmodule='mplayer'
													join mdl_mplayer m on m.id=mdl_grade_items.iteminstance
													join mdl_user u on u.id=mdl_grade_grades.userid and u.id=$record->Userid
													and mdl_grade_grades.timemodified >= unix_timestamp('2015/4/1')
													and mdl_grade_grades.timemodified < unix_timestamp('2016/03/1')
													JOIN mdl_course AS c ON c.id=m.course");
						if ($mplayer)
						{
							foreach($mplayer as $nascorm) 
							{
								$insertscorm="update mdl_training_report set mplayer = $nascorm->Duration where Userid = $record->Userid";
								$resultInsert = execute_sql($insertscorm, $feedback=true);

							}
						
						}
					}
				
				}
				mtrace('mplayer.');
				$userid="select id,Userid,PortalID from mdl_training_report";
				if ($iltdu = get_records_sql($userid))
				{
					foreach($iltdu as $record	) 
					{
					//$recodid=$record->Userid
						$ilt = get_records_sql("SELECT  mdl_classroom_sessions.id,  sum(duration) as 'Duration' from mdl_classroom_sessions where
												mdl_classroom_sessions.id in(SELECT distinct(s.id)
												from mdl_classroom_sessions s
												join mdl_classroom_sessions_dates on s.id=mdl_classroom_sessions_dates.sessionid
												and s.datetimeknown=1 and s.status='Completed'
												join mdl_classroom on mdl_classroom.id=s.classroom
												join mdl_classroom_submissions on mdl_classroom_submissions.sessionid = s.id
												and mdl_classroom_submissions.attend=1
												join mdl_course c on c.id=mdl_classroom.course
												join mdl_user u on u.id=mdl_classroom_submissions.userid and u.id=$record->Userid
												and mdl_classroom_sessions_dates.timestart >= unix_timestamp('2015/4/1')
												and mdl_classroom_sessions_dates.timefinish < unix_timestamp('2016/03/1'))");
						if ($ilt)
						{
							foreach($ilt as $nascorm	)
							{
								$insertscorm="update mdl_training_report set ilt = $nascorm->Duration where Userid = $record->Userid";
								$resultInsert = execute_sql($insertscorm, $feedback=true);

							}
						
						}
					}
				
				}
				mtrace('ilt.');
				$userid="select id,Userid,PortalID from mdl_training_report";
				if ($quizdu = get_records_sql($userid))
				{
					foreach($quizdu as $record	)
					{
					//$recodid=$record->Userid
						$quiz = get_records_sql("SELECT  u.id,  sum(q.timelimit) as 'Duration'
												FROM mdl_grade_grades g
												INNER JOIN mdl_user u ON g.userid = u.id and u.id=$record->Userid
												INNER JOIN mdl_grade_items i
												ON g.itemid = i.id
												INNER JOIN mdl_course c on c.id = i.courseid
												inner join mdl_quiz q on q.course=c.id and q.id=i.iteminstance
												WHERE (i.itemname IS NOT NULL)AND (i.itemtype = 'mod')AND
												(i.itemmodule = 'quiz')
												AND (g.finalgrade >= 0) 
												and g.timemodified >= unix_timestamp('2015/4/1')
												and g.timemodified < unix_timestamp('2016/03/1')");
						if ($quiz)
						{
							foreach($quiz as $nascorm	) 
							{
								$insertscorm="update mdl_training_report set quiz = $nascorm->Duration where Userid = $record->Userid";
								$resultInsert = execute_sql($insertscorm, $feedback=true);

							}
						
						}
					}
				
				}
				mtrace('quiz.');
				$userid="select id,Userid,PortalID from mdl_training_report";
				if ($assignmentdu = get_records_sql($userid))
				{
					foreach($assignmentdu as $record	)
					{
					//$recodid=$record->Userid
						
					$assignment = get_records_sql("SELECT  u.id,  sum(a.duration) as 'Duration'
													FROM mdl_assignment_submissions s join mdl_assignment a
													on s.assignment=a.id and s.timemodified >0 join mdl_course c
													on c.id=a.course join mdl_user u
													on u.id=s.userid and u.id=$record->Userid
													and s.timemodified >= unix_timestamp('2015/4/1')
													and s.timemodified < unix_timestamp('2016/03/1')");
						if ($assignment)
						{
							foreach($assignment as $nascorm	) 
							{
								$insertscorm="update mdl_training_report set assignment = $nascorm->Duration where Userid = $record->Userid";
								$resultInsert = execute_sql($insertscorm, $feedback=true);

							}
						
						}
					}
				
				}
				mtrace('assignment.');
				$userid="select id,Userid,PortalID from mdl_training_report";
				if ($skillsoftdu = get_records_sql($userid))
				{
					foreach($skillsoftdu as $record	)
					{
					//$recodid=$record->Userid
						
					$skillsoft = get_records_sql("SELECT sc.id, u.username as PortalID,
													sc.duration as 'Duration'
													FROM mdl_skillsoft_au_track AS st
													JOIN mdl_skillsoft AS sc ON sc.id=st.skillsoftid
													JOIN mdl_course AS c ON c.id=sc.course
													JOIN mdl_course_categories AS cc ON cc.id = c.category 
													JOIN mdl_user AS u ON st.userid=u.id
													join mdl_skillsoft_au_track AS at
													on at.userid=u.id and at.skillsoftid=sc.id and at.element='[SUMMARY]completed'
													and at.value >= unix_timestamp('2015/4/1')
													and at.value < unix_timestamp('2016/03/1')
													where st.element='[CORE]lesson_status' and st.value in ('completed','passed') and u.id=$record->Userid
													group by sc.assetid");
					 $skillsoftduration=0;
						if ($skillsoft)
						{
							foreach($skillsoft as $nascorm	) 
							{
								$skillsoftduration = $skillsoftduration + $nascorm->Duration;
											
								$insertscorm="update mdl_training_report set skillsoft =  $skillsoftduration where Userid = $record->Userid";
								$resultInsert = execute_sql($insertscorm, $feedback=true);

							}
						
						}
					}
				
				}
				mtrace('skillsoft.');
				
				//Naga added for Non Matched skillsoft records
				
				$userid="select id,Userid,PortalID from mdl_training_report";
				if ($skillsoftdu_nonmatched = get_records_sql($userid))
				{
					foreach($skillsoftdu_nonmatched as $record	) 
					{
					//$recodid=$record->Userid
						
					$skillsoft_nonmatched = get_records_sql("SELECT sc.id, u.username as PortalID,
																sc.duration as 'Duration'
																FROM mdl_skillsoft_au_track_nonmatched AS st
																JOIN mdl_skillsoft_nonmatched AS sc ON sc.id=st.skillsoftid
																JOIN mdl_course AS c ON c.id=sc.course
																JOIN mdl_course_categories AS cc ON cc.id = c.category 
																JOIN mdl_user AS u ON st.userid=u.id
																join mdl_skillsoft_au_track_nonmatched AS at
																on at.userid=u.id and at.skillsoftid=sc.id and at.element='[SUMMARY]completed'
																and at.value >= unix_timestamp('2015/4/1')
																and at.value < unix_timestamp('2016/03/1')
																where st.element='[CORE]lesson_status' and st.value in ('completed','passed') and u.id=$record->Userid
																group by sc.assetid");
					 $skillsoftduration_nonmatched=0;
						if ($skillsoft_nonmatched)
						{
							foreach($skillsoft_nonmatched as $nascorm	)
							{
								$skillsoftduration_nonmatched = $skillsoftduration_nonmatched + $nascorm->Duration;
												
								$insertscorm="update mdl_training_report set skillsoft_nonmatched =  $skillsoftduration_nonmatched where Userid = $record->Userid";
								$resultInsert = execute_sql($insertscorm, $feedback=true);

							}
						
						}
					}
				
				}
	mtrace('skillsoft_nonmatched.');
 mtrace('done.');
 
    //Unset session variables and destroy it
    @session_unset();
    @session_destroy();

    mtrace("Cron script completed correctly");

    $difftime = microtime_diff($starttime, microtime());
    mtrace("Execution took ".$difftime." seconds"); 

/// finish the IE hack
    if (check_browser_version('MSIE')) {
        echo "</xmp>";
    }

?>
