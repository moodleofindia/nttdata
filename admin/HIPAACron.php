﻿<?php 
    ini_set('max_execution_time',0);
    $starttime = microtime();

    define('FULLME', 'cron');

    $nomoodlecookie = true;

    if (!isset($_SERVER['REMOTE_ADDR']) && isset($_SERVER['argv'][0])) {
        chdir(dirname($_SERVER['argv'][0]));
    }
	require_once(dirname(__FILE__) . '/../config.php');
    require_once($CFG->libdir.'/adminlib.php');
    require_once($CFG->libdir.'/gradelib.php');

    if (!empty($CFG->showcronsql)) {
        $db->debug = true;
    }
    if (!empty($CFG->showcrondebugging)) {
        $CFG->debug = DEBUG_DEVELOPER;
        $CFG->debugdisplay = true;
    }

/// extra safety
    @session_write_close();

/// check if execution allowed
    if (isset($_SERVER['REMOTE_ADDR'])) { // if the script is accessed via the web.
        if (!empty($CFG->cronclionly)) { 
            // This script can only be run via the cli.
            print_error('cronerrorclionly', 'admin');
            exit;
        }
        // This script is being called via the web, so check the password if there is one.
        if (!empty($CFG->cronremotepassword)) {
            $pass = optional_param('password', '', PARAM_RAW);
            if($pass != $CFG->cronremotepassword) {
                // wrong password.
                print_error('cronerrorpassword', 'admin'); 
                exit;
            }
        }
    }


/// emulate normal session
    $SESSION = new object();
    $USER = get_admin();      /// Temporarily, to provide environment for this script

/// ignore admins timezone, language and locale - use site deafult instead!
    $USER->timezone = $CFG->timezone;
    $USER->lang = '';
    $USER->theme = '';
    course_setup(SITEID);

/// send mime type and encoding
    if (check_browser_version('MSIE')) {
        //ugly IE hack to work around downloading instead of viewing
        @header('Content-Type: text/html; charset=utf-8');
        echo "<xmp>"; //<pre> is not good enough for us here
    } else {
        //send proper plaintext header
        @header('Content-Type: text/plain; charset=utf-8');
    }

/// no more headers and buffers
    while(@ob_end_flush());

/// increase memory limit (PHP 5.2 does different calculation, we need more memory now)
    @ini_set('memory_limit','1000M');

/// Start output log

    $timenow  = time();

    mtrace("Server Time: ".date('r',$timenow)."\n\n");

	

    mtrace('Starting main gradebook job ...');

					$reminderUserID = "	SELECT u.id as id,concat(u.firstname,' ',u.lastname) as name,u.username as portalid,from_unixtime(r.timemodified) as enroldate
									FROM mdl_role_assignments r
									inner JOIN mdl_course AS c ON  c.id=7478 
									inner JOIN mdl_context AS ctx ON c.id = ctx.instanceid 
									inner join mdl_user u 
									on r.contextid =ctx.id AND u.deleted = 0 AND u.id = r.userid AND r.roleid = 5" ;
					
					if ($courseUserRS = get_recordset_sql($reminderUserID))
					{
						while ($UserId = rs_fetch_next_record($courseUserRS)) 
						{
						$after7="select unix_timestamp(('$UserId->enroldate') + interval '7' day)  as after7";
						$after7days=get_record_sql($after7);
							$tim      ="select unix_timestamp(now()) as now";
							$time=get_record_sql($tim);
							
							$employeeMessage='<html xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:w="urn:schemas-microsoft-com:office:word" xmlns:m="http://schemas.microsoft.com/office/2004/12/omml" xmlns="http://www.w3.org/TR/REC-html40"><head><META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=us-ascii"><meta name=Generator content="Microsoft Word 14 (filtered medium)"><style><!--
/* Font Definitions */
@font-face
	{font-family:"Microsoft Sans Serif";
	panose-1:2 11 6 4 2 2 2 2 2 4;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
/* Style Definitions */
p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:10.0pt;
	margin-left:0in;
	line-height:115%;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";}
a:link, span.MsoHyperlink
	{mso-style-priority:99;
	color:blue;
	text-decoration:underline;}
a:visited, span.MsoHyperlinkFollowed
	{mso-style-priority:99;
	color:purple;
	text-decoration:underline;}
span.EmailStyle17
	{mso-style-type:personal-compose;
	font-family:"Calibri","sans-serif";
	color:windowtext;}
.MsoChpDefault
	{mso-style-type:export-only;
	font-family:"Calibri","sans-serif";}
@page WordSection1
	{size:8.5in 11.0in;
	margin:1.0in 1.0in 1.0in 1.0in;}
div.WordSection1
	{page:WordSection1;}
--></style><!--[if gte mso 9]><xml>
<o:shapedefaults v:ext="edit" spidmax="1026" />
</xml><![endif]--><!--[if gte mso 9]><xml>
<o:shapelayout v:ext="edit">
<o:idmap v:ext="edit" data="1" />
</o:shapelayout></xml><![endif]--></head><body lang=EN-US link=blue vlink=purple>
<div class=WordSection1><p class=MsoNormal>Dear '.$UserId->name.',<o:p></o:p></p>
<p class=MsoNormal>This message is to remind you that you have been enrolled in NTT DATA’s HIPAA compliance training program for over one week and have not successfully completed the program. 
You must complete the program with a score of 80 or higher.  Please complete this annual mandatory program within the next week. 
The program takes less than one hour to complete and will walk you through HIPAA and your responsibilities under the law and as a member of NTT DATA’s project teams working with Protected Health Information (PHI).&nbsp; 
<o:p></o:p></p><p class=MsoNormal><span style="color:#953735;mso-style-textfill-fill-color:#953735;mso-style-textfill-fill-alpha:100.0%">
HIPAA training URL: https://lmsqa.portal.nttdatainc.com/course/view.php?id=7478 </span>&nbsp;<o:p></o:p></p><p class=MsoNormal>Your business unit may elect to enforce significant penalties for non-compliance with the HIPAA training mandate.  Should you have questions regarding the necessity of taking this training, please contact your manager.  Should you have issues accessing the course, please reach out to the training help desk via email Training.Helpdesk@nttdata.com<o:p></o:p></p><p class=MsoNormal>Thank you,<o:p></o:p></p><p class=MsoNormal>NTT DATA Talent Development Team<o:p></o:p></p></div></body></html>';
							
							$notakenUserID = "select gg.finalgrade as grade from mdl_grade_grades AS gg
							JOIN mdl_grade_items AS gi
							ON gi.id = gg.itemid and gg.finalgrade >= gi.grademax
							AND gi.itemtype = 'course' and gi.courseid=7478 
							and gg.userid=$UserId->id";
							$nogradeuser=get_record_sql($notakenUserID);
							if((!$nogradeuser->grade)or($nogradeuser->grade<=0))
							{
							$euser = get_record('user','id',$UserId->id);
							if($time->now > $after7days->after7){
							email_to_user( $euser,'', 'HIPAA training program 2014',$employeeMessage,$employeeMessage);
							mtrace('Mail initiated for employee '.$UserId->name);}
							}							
						}
					}
					rs_close($courseUserRS);
	
	//--------------------------------------------------------------------------------------------

	
	
    mtrace('done.');


    //Unset session variables and destroy it
    @session_unset();
    @session_destroy();

    mtrace("Cron script completed correctly");

    $difftime = microtime_diff($starttime, microtime());
    mtrace("Execution took ".$difftime." seconds"); 

/// finish the IE hack
    if (check_browser_version('MSIE')) {
        echo "</xmp>";
    }

?>
