<?php // CRON for Employee Data feed into LMS - Roy Philip Nov 2010

ini_set('max_execution_time', 0);
$starttime = microtime();

/// The following is a hack necessary to allow this script to work well 
/// from the command line.

define('FULLME', 'cron');


/// Do not set moodle cookie because we do not need it here, it is better to emulate session
$nomoodlecookie = true;

/// The current directory in PHP version 4.3.0 and above isn't necessarily the
/// directory of the script when run from the command line. The require_once()
/// would fail, so we'll have to chdir()

if (!isset($_SERVER['REMOTE_ADDR']) && isset($_SERVER['argv'][0])) {
    chdir(dirname($_SERVER['argv'][0]));
}

require_once(dirname(__FILE__) . '/../config.php');
require_once($CFG->libdir . '/adminlib.php');
require_once($CFG->libdir . '/gradelib.php');

/// Extra debugging (set in config.php)
if (!empty($CFG->showcronsql)) {
    $db->debug = true;
}
if (!empty($CFG->showcrondebugging)) {
    $CFG->debug        = DEBUG_DEVELOPER;
    $CFG->debugdisplay = true;
}


/// extra safety
@session_write_close();

/// check if execution allowed
if (isset($_SERVER['REMOTE_ADDR'])) { // if the script is accessed via the web.
    if (!empty($CFG->cronclionly)) {
        // This script can only be run via the cli.
        print_error('cronerrorclionly', 'admin');
        exit;
    }
    // This script is being called via the web, so check the password if there is one.
    if (!empty($CFG->cronremotepassword)) {
        $pass = optional_param('password', '', PARAM_RAW);
        if ($pass != $CFG->cronremotepassword) {
            // wrong password.
            print_error('cronerrorpassword', 'admin');
            exit;
        }
    }
}


/// emulate normal session
$SESSION = new object();
$USER    = get_admin(); /// Temporarily, to provide environment for this script

/// ignore admins timezone, language and locale - use site deafult instead!
$USER->timezone = $CFG->timezone;
$USER->lang     = '';
$USER->theme    = '';
course_setup(SITEID);

/// send mime type and encoding
if (check_browser_version('MSIE')) {
    //ugly IE hack to work around downloading instead of viewing
    @header('Content-Type: text/html; charset=utf-8');
    echo "<xmp>"; //<pre> is not good enough for us here
} else {
    //send proper plaintext header
    @header('Content-Type: text/plain; charset=utf-8');
}

/// no more headers and buffers
while (@ob_end_flush());

/// increase memory limit (PHP 5.2 does different calculation, we need more memory now)
@ini_set('memory_limit', '1024M');

/// Start output log



$timenow = time();

$backup_filedate = date("Ymd");
$backupfile      = 'C:\inetpub\LMSdata\Navigator\Navigator Log' . $backup_filedate . '.xls';
rename("C:\inetpub\LMSdata\Navigator\Navigator Log.xls", $backupfile);

mtrace("Server Time: " . date('r', $timenow) . "\n\n");
$filedate = date("Ymd");
$file     = 'C:\inetpub\LMSdata\Navigator\Navigator.log';
$timenow  = date('r', $timenow) . "\r\n";
file_put_contents($file, $timenow, FILE_APPEND | LOCK_EX);


//Naga added for excel


$xlshead = pack("s*", 0x809, 0x8, 0x0, 0x10, 0x0, 0x0);
$xlsfoot = pack("s*", 0x0A, 0x00);
function xlsCell($row, $col, $val)
{
    $len = strlen($val);
    return pack("s*", 0x204, 8 + $len, $row, $col, 0x0, $len) . $val;
}

$data .= xlsCell(0, 0, "PortalID") . xlsCell(0, 1, "Firstname Error") . xlsCell(0, 2, "lastname Error") . xlsCell(0, 3, "email Error") . xlsCell(0, 4, "country Error") . xlsCell(0, 5, "grade Error") . xlsCell(0, 6, "employee_type Error") . xlsCell(0, 7, "manager_portalid Error") . xlsCell(0, 8, "source Error") . xlsCell(0, 9, "BU Error") . xlsCell(0, 10, "costcenter Error") . xlsCell(0, 11, "hireddate Error") . xlsCell(0, 12, "loaddate Error") . xlsCell(0, 13, "Insert Error Message") . xlsCell(0, 14, "Portalid Error");



//Completed

// Updating and inserting user records

mtrace("Starting to update and insert missing employees... \n");
//Truncate the stageing table
$get_date                     = 'SUBDATE(CURDATE(),1)';
$get_latest_recordsfrom_stage = get_records_sql("Select portalid,firstname,middle_name,lastname,work_city,emp_work_country,grade,email,BU,costcenter,employee_type,manager_portalid,acq_company,hireddate,LC,practise,loaddate 
														from  mdl_navigator_feed ");
$rowNumber                    = 0;

//If loop started
if ($get_latest_recordsfrom_stage) {
    
    foreach ($get_latest_recordsfrom_stage as $latest_records) {
        
        if (is_numeric($latest_records->portalid)) {
            
            if (!get_record_sql("Select username from  mdl_user where username= '$latest_records->portalid'")) {
                $queryInsert = "insert into mdl_user (username,firstname,lastname,city,country,grade,email,BU,LC,practise,costcenter,employee_type,manager_portalid,source,hireddate,loaddate,timecreated) 
														select portalid,firstname,lastname,work_city,emp_work_country,grade,email,BU,LC,practise,costcenter,employee_type,manager_portalid,acq_company,hireddate,loaddate,loaddate  from mdl_navigator_feed  m 
														where m.portalid='$latest_records->portalid' and m.portalid not in(select username from mdl_user);";
                
                if (!$resultInsert = execute_sql($queryInsert, $feedback = true)) {
                    $error_message        = 'Database Error:' . mysql_error();
                    $error_message_insert = $error_message;
                }
            } else {
                $latest_records->firstname = mysql_real_escape_string(utf8_encode($latest_records->firstname));
                $latest_records->lastname  = mysql_real_escape_string(utf8_encode($latest_records->lastname . $latest_records->middle_name));
                
                //Query TO update the user records from stage table to LMS user table		
                
                $queryUpdate_firstname     = "UPDATE mdl_user u INNER JOIN mdl_navigator_feed t ON u.username = t.portalid set u.firstname='$latest_records->firstname'  where t.portalid='$latest_records->portalid';";
                $queryUpdate_lastname      = "UPDATE mdl_user u INNER JOIN mdl_navigator_feed t ON u.username = t.portalid set u.lastname='$latest_records->lastname'  where t.portalid='latest_records->portalid';";
                $queryUpdate_LC            = "UPDATE mdl_user u INNER JOIN mdl_navigator_feed t ON u.username = t.portalid set u.LC=t.LC  where t.portalid='$latest_records->portalid';";
                $queryUpdate_practise      = "UPDATE mdl_user u INNER JOIN mdl_navigator_feed t ON u.username = t.portalid set u.practise=t.practise  where t.portalid='$latest_records->portalid';";
                $queryUpdate_email         = "UPDATE mdl_user u INNER JOIN mdl_navigator_feed t ON u.username = t.portalid set u.email=t.email  where t.portalid='$latest_records->portalid';";
                $queryUpdate_country       = "UPDATE mdl_user u INNER JOIN mdl_navigator_feed t ON u.username = t.portalid set u.country=t.emp_work_country  where t.portalid='$latest_records->portalid';";
                $queryUpdate_grade         = "UPDATE mdl_user u INNER JOIN mdl_navigator_feed t ON u.username = t.portalid set u.grade=t.grade  where t.portalid='$latest_records->portalid';";
                $queryUpdate_employee_type = "UPDATE mdl_user u INNER JOIN mdl_navigator_feed t ON u.username = t.portalid set u.employee_type=t.employee_type  where t.portalid='$latest_records->portalid';";
                $queryUpdate_source      = "UPDATE mdl_user u INNER JOIN mdl_navigator_feed t ON u.username = t.portalid set u.source=t.acq_company  where t.portalid='$latest_records->portalid';";
                $queryUpdate_BU          = "UPDATE mdl_user u INNER JOIN mdl_navigator_feed t ON u.username = t.portalid set u.BU=t.BU  where t.portalid='$latest_records->portalid';";
                $queryUpdate_costcenter  = "UPDATE mdl_user u INNER JOIN mdl_navigator_feed t ON u.username = t.portalid set u.costcenter=t.costcenter  where t.portalid='$latest_records->portalid';";
                $queryUpdate_city        = "UPDATE mdl_user u INNER JOIN mdl_navigator_feed t ON u.username = t.portalid set u.city=t.work_city  where t.portalid='$latest_records->portalid';";
                $queryUpdate_hireddate   = "UPDATE mdl_user u INNER JOIN mdl_navigator_feed t ON u.username = t.portalid set u.hireddate=t.hireddate  where t.portalid='$latest_records->portalid';";
                $queryUpdate_loaddate    = "UPDATE mdl_user u INNER JOIN mdl_navigator_feed t ON u.username = t.portalid set u.loaddate=t.loaddate  where t.portalid='$latest_records->portalid';";
                $queryUpdate_timeupdated = "UPDATE mdl_user u INNER JOIN mdl_navigator_feed t ON u.username = t.portalid set u.timeupdated=t.loaddate  where t.portalid='$latest_records->portalid';";
                $queryUpdate_manager_portalid = "UPDATE mdl_user u INNER JOIN mdl_navigator_feed t ON u.username = t.portalid set u.manager_portalid=t.manager_portalid  where t.portalid='$latest_records->portalid';";
                    
                $queryUpdate_manager_portalid_sql = execute_sql($queryUpdate_manager_portalid, $feedback = true);
                $queryUpdate_firstname_sql     = execute_sql($queryUpdate_firstname, $feedback = true);
                $queryUpdate_lastname_sql      = execute_sql($queryUpdate_lastname, $feedback = true);
                $queryUpdate_email_sql         = execute_sql($queryUpdate_email, $feedback = true);
                $queryUpdate_country_sql       = execute_sql($queryUpdate_country, $feedback = true);
                $queryUpdate_grade_sql         = execute_sql($queryUpdate_grade, $feedback = true);
                $queryUpdate_employee_type_sql = execute_sql($queryUpdate_employee_type, $feedback = true);
                $queryUpdate_timeupdated_sql   = execute_sql($queryUpdate_timeupdated, $feedback = true);
                $queryUpdate_source_sql        = execute_sql($queryUpdate_source, $feedback = true);
                $queryUpdate_BU_sql            = execute_sql($queryUpdate_BU, $feedback = true);
                $queryUpdate_costcenter_sql    = execute_sql($queryUpdate_costcenter, $feedback = true);
                $queryUpdate_hireddate_sql     = execute_sql($queryUpdate_hireddate, $feedback = true);
                $queryUpdate_loaddate_sql      = execute_sql($queryUpdate_loaddate, $feedback = true);
                $queryUpdate_city_sql          = execute_sql($queryUpdate_city, $feedback = true);
                $queryUpdate_LC_sql            = execute_sql($queryUpdate_LC, $feedback = true);
                $queryUpdate_practise_Sql      = execute_sql($queryUpdate_practise, $feedback = true);
                
                
                if ($queryUpdate_firstname_sql) {
                    mtrace("Employee Firstname updated sucessfully ! \n");
                    $text  = 'The Employee Firstname Records updated successfully from stage table to LMS user table';
                    $text1 = $text . "\r\n";
                } else {
                    
                    mtrace("Error: Failed to update employee Firstname records. \n");
                    $firstname_mysql_error = ('Database Error update:' . mysql_error());
                    mtrace($firstname_mysql_error);
                    $text = "Error: Employee firstname Records are not updated  from stage table to LMS user table. " . 'Portalid in Stage table ' . $latest_records->portalid . ' Firstname in stage table ' . $latest_records-> firstname;
                    
                    $text1          = $text . "\r\n" . $firstname_mysql_error . "\r\n";
                    $error_message  = 'The Cron job is unable to update the user records from stage table into LMS user table.';
                    $error_message1 = $text . "\r\n" . $firstname_mysql_error . "\r\n";
                }
                if ($queryUpdate_lastname_sql) {
                    mtrace("Employee Lastname updated sucessfully ! \n");
                    $text  = 'The Employee Lastname Records updated successfully from stage table to LMS user table';
                    $text2 = $text . "\r\n";
                } else {
                    
                    mtrace("Error: Failed to update employee Lastname records. \n");
                    $lastname_mysql_error = ('Database Error update:' . mysql_error());
                    mtrace($lastname_mysql_error);
                    $text = "Error: Employee firstname Records are not updated  from stage table to LMS user table. " . 'Portalid in Stage table ' . $latest_records->portalid . ' Firstname in stage table ' . $latest_records-> firstname;
                    
                    $text2          = $text . "\r\n" . $lastname_mysql_error . "\r\n";
                    $error_message  = 'The Cron job is unable to update the user records from stage table into LMS user table.';
                    $error_message2 = $text . "\r\n" . $lastname_mysql_error . "\r\n";
                }
                if ($queryUpdate_email_sql) {
                    mtrace("Employee email updated sucessfully ! \n");
                    $text  = 'The Employee email Records updated successfully from stage table to LMS user table';
                    $text3 = $text . "\r\n";
                } else {
                    
                    mtrace("Error: Failed to update employee email records. \n");
                    $email_mysql_error = ('Database Error update:' . mysql_error());
                    mtrace($email_mysql_error);
                    //	$text = "Error: Employee firstname Records are not updated  from stage table to LMS user table. ".  .'$latest_records->portalid'. .'$t.firstname';
                    
                    $text3          = $text . "\r\n" . $email_mysql_error . "\r\n";
                    $error_message  = 'The Cron job is unable to update the user email records from stage table into LMS user table.';
                    $error_message3 = $text . "\r\n" . $email_mysql_error . "\r\n";
                }
                if ($queryUpdate_country_sql) {
                    mtrace("Employee country updated sucessfully ! \n");
                    $text  = 'The Employee country Records updated successfully from stage table to LMS user table';
                    $text4 = $text . "\r\n";
                } else {
                    
                    mtrace("Error: Failed to update employee country records. \n");
                    $country_mysql_error = ('Database Error update:' . mysql_error());
                    mtrace($country_mysql_error);
                    $text = "Error: Employee country Records are not updated  from stage table to LMS user table. " . 'Portalid in Stage table ' . $latest_records->portalid . ' country in stage table ' . $latest_records->emp_work_country;
                    
                    $text4          = $text . "\r\n" . $country_mysql_error . "\r\n";
                    $error_message  = 'The Cron job is unable to update the user country records from stage table into LMS user table.';
                    $error_message4 = $text . "\r\n" . $country_mysql_error . "\r\n";
                }
                if ($queryUpdate_grade_sql) {
                    mtrace("Employee grade updated sucessfully ! \n");
                    $text  = 'The Employee grade Records updated successfully from stage table to LMS user table';
                    $text5 = $text . "\r\n";
                } else {
                    
                    mtrace("Error: Failed to update employee grade records. \n");
                    $grade_mysql_error = ('Database Error update:' . mysql_error());
                    mtrace($grade_mysql_error);
                    $text = "Error: Employee grade Records are not updated  from stage table to LMS user table. " . 'Portalid in Stage table ' . $latest_records->portalid . ' grade in stage table ' . $latest_records-> grade;
                    
                    $text5          = $text . "\r\n" . $grade_mysql_error . "\r\n";
                    $error_message  = 'The Cron job is unable to update the user grade records from stage table into LMS user table.';
                    $error_message5 = $text . "\r\n" . $grade_mysql_error . "\r\n";
                }
                if ($queryUpdate_employee_type) {
                    mtrace("Employee employee_type updated sucessfully ! \n");
                    $text  = 'The user employee_type Records updated successfully from stage table to LMS user table';
                    $text6 = $text . "\r\n";
                } else {
                    
                    mtrace("Error: Failed to update user employee_type records. \n");
                    $employee_type_mysql_error = ('Database Error update:' . mysql_error());
                    mtrace($employee_type_mysql_error);
                    $text = "Error: Employee employee_type Records are not updated  from stage table to LMS user table. " . 'Portalid in Stage table ' . $latest_records->portalid . ' employee_type in stage table ' . $latest_records-> employee_type;
                    
                    $text6          = $text . "\r\n" . $employee_type_mysql_error . "\r\n";
                    $error_message  = 'The Cron job is unable to update the user employee type records from stage table into LMS user table.';
                    $error_message6 = $text . "\r\n" . $employee_type_mysql_error . "\r\n";
                }
                                  
                    if ($queryUpdate_manager_portalid_sql) {
                        mtrace("Employee manager_portalid updated sucessfully ! \n");
                        $text  = 'The Employee manager_portalid Records updated successfully from stage table to LMS user table';
                        $text7 = $text . "\r\n";
                    }
                 else {
                    
                    mtrace("Error: Failed to update employee manager_portalid records. \n");
                    $manager_portalid_mysql_error = ('Database Error update:' . mysql_error());
                    mtrace($manager_portalid_mysql_error);
                    $text10 = "Error: Employee manager_portalid Records are not updated  from stage table to LMS user table. " . 'Portalid in Stage table ' . $latest_records->portalid . ' manager_portalid in stage table ' . $latest_records-> manager_portalid;
                    
                    $text70         = $text10 . "\r\n" . $manager_portalid_mysql_error . "\r\n";
                    $error_message  = 'The Cron job is unable to update the user manager_portalid records from stage table into LMS user table.';
                    $error_message7 = $text10 . "\r\n" . $manager_portalid_mysql_error . "\r\n";
                }
                if ($queryUpdate_source_sql) {
                    mtrace("Employee source records updated sucessfully ! \n");
                    $text  = 'The Employee source Records updated successfully from stage table to LMS user table';
                    $text8 = $text . "\r\n";
                } else {
                    
                    mtrace("Error: Failed to update employee source records. \n");
                    $source_mysql_error = ('Database Error update:' . mysql_error());
                    mtrace($source_mysql_error);
                    $text = "Error: Employee source Records are not updated  from stage table to LMS user table. " . 'Portalid in Stage table ' . $latest_records->portalid . ' source in stage table ' . $latest_records-> acq_company;
                    
                    $text8          = $text . "\r\n" . $source_mysql_error . "\r\n";
                    $error_message  = 'The Cron job is unable to update the user source records from stage table into LMS user table.';
                    $error_message8 = $text . "\r\n" . $source_mysql_error . "\r\n";
                }
                if ($queryUpdate_BU_sql) {
                    mtrace("Employee BU records updated sucessfully ! \n");
                    $text  = 'The Employee BU Records updated successfully from stage table to LMS user table';
                    $text9 = $text . "\r\n";
                } else {
                    
                    mtrace("Error: Failed to update employee BU Records. \n");
                    $BU_mysql_error = ('Database Error update:' . mysql_error());
                    mtrace($BU_mysql_error);
                    $text = "Error: Employee BU Records are not updated  from stage table to LMS user table. " . 'Portalid in Stage table ' . $latest_records->portalid . ' BU in stage table ' . $latest_records-> BU;
                    
                    $text9          = $text . "\r\n" . $BU_mysql_error . "\r\n";
                    $error_message  = 'The Cron job is unable to update the user BU records from stage table into LMS user table.';
                    $error_message9 = $text . "\r\n" . $BU_mysql_error . "\r\n";
                }
                if ($queryUpdate_costcenter_sql) {
                    mtrace("Employee costcenter records updated sucessfully ! \n");
                    $text   = 'The Employee costcenter Records updated successfully from stage table to LMS user table';
                    $text10 = $text . "\r\n";
                } else {
                    
                    mtrace("Error: Failed to update employee costcenter  records. \n");
                    $costcenter_mysql_error = ('Database Error update:' . mysql_error());
                    mtrace($costcenter_mysql_error);
                    $text = "Error: Employee costcenter Records are not updated  from stage table to LMS user table. " . 'Portalid in Stage table ' . $latest_records->portalid . ' costcenter in stage table ' . $latest_records-> costcenter;
                    
                    $text10          = $text . "\r\n" . $costcenter_mysql_error . "\r\n";
                    $error_message   = 'The Cron job is unable to update the user costcenter records from stage table into LMS user table.';
                    $error_message10 = $text . "\r\n" . $costcenter_mysql_error . "\r\n";
                }
                if ($queryUpdate_hireddate_sql) {
                    mtrace("Employee hireddate records updated sucessfully ! \n");
                    $text   = 'The Employee hireddate Records updated successfully from stage table to LMS user table';
                    $text11 = $text . "\r\n";
                } else {
                    
                    mtrace("Error: Failed to update employee hireddate records. \n");
                    $hireddate_mysql_error = ('Database Error update:' . mysql_error());
                    mtrace($hireddate_mysql_error);
                    $text = "Error: Employee hireddate Records are not updated  from stage table to LMS user table. " . 'Portalid in Stage table ' . $latest_records->portalid . ' hireddate in stage table ' . $latest_records-> hireddate;
                    
                    $text11          = $text . "\r\n" . $hireddate_mysql_error . "\r\n";
                    $error_message   = 'The Cron job is unable to update the user hireddate records from stage table into LMS user table.';
                    $error_message11 = $text . "\r\n" . $hireddate_mysql_error . "\r\n";
                }
                
                //For Loaddate
                if ($queryUpdate_loaddate_sql) {
                    mtrace("Employee loaddate records updated sucessfully ! \n");
                    $text   = 'The Employee loaddate Records updated successfully from stage table to LMS user table';
                    $text12 = $text . "\r\n";
                } else {
                    
                    mtrace("Error: Failed to update employee loaddate records. \n");
                    $loaddate_mysql_error = ('Database Error update:' . mysql_error());
                    mtrace($loaddate_mysql_error);
                    $text = "Error: Employee loaddate Records are not updated  from stage table to LMS user table. " . 'Portalid in Stage table ' . $latest_records->portalid . ' loaddate in stage table ' . $latest_records-> loaddate;
                    
                    $text12          = $text . "\r\n" . $loaddate_mysql_error . "\r\n";
                    $error_message   = 'The Cron job is unable to update the user records from stage table into LMS user table.';
                    $error_message12 = $text . "\r\n" . $loaddate_mysql_error . "\r\n";
                }
				
            }
        } else {
            mtrace("Error: The portalid is not numeric. \n");
            $loaddate_mysql_error = ('Database Error update:' . mysql_error());
            mtrace($loaddate_mysql_error);
            $text = "Error: Employee portalid is not numeric in stage table . " . 'Portalid in Stage table ' . $latest_records->portalid;
            
            $text13          = $text . "\r\n" . $loaddate_mysql_error . "\r\n";
            $error_message   = 'The portalid is not numeric.';
            $error_message13 = $text . "\r\n" . $loaddate_mysql_error . "\r\n";
        }
        $rowNumber = $rowNumber + 1;
        $data .= xlsCell($rowNumber, 0, $latest_records->portalid) . xlsCell($rowNumber, 1, $error_message1) . xlsCell($rowNumber, 2, $error_message2) . xlsCell($rowNumber, 3, $error_message3) . xlsCell($rowNumber, 4, $error_message4) . xlsCell($rowNumber, 5, $error_message5) . xlsCell($rowNumber, 6, $error_message6) . xlsCell($rowNumber, 7, $error_message7) . xlsCell($rowNumber, 8, $error_message8) . xlsCell($rowNumber, 9, $error_message9) . xlsCell($rowNumber, 10, $error_message10) . xlsCell($rowNumber, 11, $error_message11) . xlsCell($rowNumber, 12, $error_message12) . xlsCell($rowNumber, 13, $error_message_insert) . xlsCell($rowNumber, 14, $error_message13);
        
    }
}

//If loop completed
//Convert the employee city to DSKLS based upon the last digit of the costcenter

$dskls_city     = "update mdl_user set city='DSKLS' where right(costcenter,1) in ('5','6') and length (costcenter) >=10";
$dskls_city_Sql = execute_sql($dskls_city, $feedback = true);


$status_update_N = "update mdl_user set auth='nologin' where username in(select portalid from mdl_navigator_feed  where auth='N')";

$result_update_n = execute_sql($status_update_N, $feedback = true);

$status_update_Y = "update mdl_user set auth='ldap' where username in(select portalid from mdl_navigator_feed  where auth='Y')";

$result_update_y = execute_sql($status_update_Y, $feedback = true);




// Naga Added ... to calculate the Mandatory Training Hours based on the Hired date in between 2015-16 FY.	
$gradeupdate       = 'update mdl_user u INNER JOIN mdl_grade_training t on u.grade=t.grade set mandatorytrainings=hours ;';
$resultgradeupdate = execute_sql($gradeupdate, $feedback = false);

$gradeupdate1        = 'update mdl_user u INNER JOIN mdl_grade_training t on u.grade=t.grade set mandatorytrainings=round((hours*11)/12) where u.hireddate <= "2016-05-15" and u.hireddate >= "2016-04-16";';
$resultgradeupdate1  = execute_sql($gradeupdate1, $feedback = false);
$gradeupdate2        = 'update mdl_user u INNER JOIN mdl_grade_training t on u.grade=t.grade set mandatorytrainings=round((hours*10)/12) where u.hireddate <= "2016-06-15" and u.hireddate >= "2016-05-16";';
$resultgradeupdate2  = execute_sql($gradeupdate2, $feedback = false);
$gradeupdate3        = 'update mdl_user u INNER JOIN mdl_grade_training t on u.grade=t.grade set mandatorytrainings=round((hours*9)/12) where u.hireddate <= "2016-07-15" and u.hireddate >= "2016-06-16";';
$resultgradeupdate3  = execute_sql($gradeupdate3, $feedback = false);
$gradeupdate4        = 'update mdl_user u INNER JOIN mdl_grade_training t on u.grade=t.grade set mandatorytrainings=round((hours*8)/12) where u.hireddate <= "2016-08-15" and u.hireddate >= "2016-07-16";';
$resultgradeupdate4  = execute_sql($gradeupdate4, $feedback = false);
$gradeupdate5        = 'update mdl_user u INNER JOIN mdl_grade_training t on u.grade=t.grade set mandatorytrainings=round((hours*7)/12) where u.hireddate <= "2016-09-15" and u.hireddate >= "2016-08-16";';
$resultgradeupdate5  = execute_sql($gradeupdate5, $feedback = false);
$gradeupdate6        = 'update mdl_user u INNER JOIN mdl_grade_training t on u.grade=t.grade set mandatorytrainings=round((hours*6)/12) where u.hireddate <= "2016-10-15" and u.hireddate >= "2016-09-16";';
$resultgradeupdate6  = execute_sql($gradeupdate6, $feedback = false);
$gradeupdate7        = 'update mdl_user u INNER JOIN mdl_grade_training t on u.grade=t.grade set mandatorytrainings=round((hours*5)/12) where u.hireddate <= "2016-11-15" and u.hireddate >= "2016-10-16";';
$resultgradeupdate7  = execute_sql($gradeupdate7, $feedback = false);
$gradeupdate8        = 'update mdl_user u INNER JOIN mdl_grade_training t on u.grade=t.grade set mandatorytrainings=round((hours*4)/12) where u.hireddate <= "2016-12-15" and u.hireddate >= "2016-11-16";';
$resultgradeupdate8  = execute_sql($gradeupdate8, $feedback = false);
$gradeupdate9        = 'update mdl_user u INNER JOIN mdl_grade_training t on u.grade=t.grade set mandatorytrainings=round((hours*3)/12) where u.hireddate <= "2017-01-15" and u.hireddate >= "2016-12-16";';
$resultgradeupdate9  = execute_sql($gradeupdate9, $feedback = false);
$gradeupdate10       = 'update mdl_user u INNER JOIN mdl_grade_training t on u.grade=t.grade set mandatorytrainings=round((hours*2)/12) where u.hireddate <= "2017-02-15" and u.hireddate >= "2017-01-16";';
$resultgradeupdate10 = execute_sql($gradeupdate10, $feedback = false);
$gradeupdate11       = 'update mdl_user u INNER JOIN mdl_grade_training t on u.grade=t.grade set mandatorytrainings=round((hours*1)/12) where u.hireddate <= "2017-03-15" and u.hireddate >= "2017-02-16";';
$resultgradeupdate11 = execute_sql($gradeupdate11, $feedback = false);

//Completed Naga...






$difftime = microtime_diff($starttime, microtime());
mtrace("Execution took " . $difftime . " seconds");

$Total_Records    = get_records_sql("Select  count(portalid) as count1 from mdl_navigator_feed ;");
$Updated_Rec_LMS  = get_records_sql("Select  count(id) as count1 from mdl_user where unix_timestamp(timeupdated) >= unix_timestamp(curdate());");
$Inserted_Rec_LMS = get_records_sql("Select  count(id) as count1 from mdl_user where unix_timestamp(timecreated) >= unix_timestamp(curdate());");

foreach ($Total_Records as $Total_Records1) {
    $total_count = $Total_Records1->count1;
}
foreach ($Updated_Rec_LMS as $Total_Records1) {
    $total_updated_count = $Total_Records1->count1;
}
foreach ($Inserted_Rec_LMS as $Total_Records1) {
    $total_inserted_count = $Total_Records1->count1;
}

$Issued_Records = ($total_count) - ($total_updated_count + $total_inserted_count);

$text6 = 'Total Execution took ' . $difftime . 'seconds';

$text = $text1 . "\r\n" . $text2 . "\r\n" . $text3 . "\r\n" . $text4 . "\r\n" . $text5 . "\r\n" . $text6 . "\r\n";

//	file_put_contents($file, $text, FILE_APPEND | LOCK_EX);

//Getting the error message to send in the mail
$message = $error_message1 . "\r\n" . $error_message2 . "\r\n" . $error_message3 . "\r\n" . $error_message4 . "\r\n" . $error_message5 . "\r\n" . $error_message6 . "\r\n" . $error_message7 . "\r\n" . $error_message8 . "\r\n" . $error_message9 . "\r\n" . $error_message10 . "\r\n" . $error_message11 . "\r\n" . $error_message12 . "\r\n" . $error_message13 . "\r\n" . $error_message14;

file_put_contents($file, $message, FILE_APPEND | LOCK_EX);
$message1        = "The Navigator feed Job completed and please find the summery below. ";
$employeeMessage = '<html xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:w="urn:schemas-microsoft-com:office:word" xmlns:m="http://schemas.microsoft.com/office/2004/12/omml" xmlns="http://www.w3.org/TR/REC-html40"><head><META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=us-ascii"><meta name=Generator content="Microsoft Word 14 (filtered medium)"><style><!--
						/* Font Definitions */
						@font-face
							{font-family:"Microsoft Sans Serif";
							panose-1:2 11 6 4 2 2 2 2 2 4;}
						@font-face
							{font-family:Calibri;
							panose-1:2 15 5 2 2 2 4 3 2 4;}
						/* Style Definitions */
						p.MsoNormal, li.MsoNormal, div.MsoNormal
							{margin-top:0in;
							margin-right:0in;
							margin-bottom:10.0pt;
							margin-left:0in;
							line-height:115%;
							font-size:11.0pt;
							font-family:"Calibri","sans-serif";}
						a:link, span.MsoHyperlink
							{mso-style-priority:99;
							color:blue;
							text-decoration:underline;}
						a:visited, span.MsoHyperlinkFollowed
							{mso-style-priority:99;
							color:purple;
							text-decoration:underline;}
						span.EmailStyle17
							{mso-style-type:personal-compose;
							font-family:"Calibri","sans-serif";
							color:windowtext;}
						.MsoChpDefault
							{mso-style-type:export-only;
							font-family:"Calibri","sans-serif";}
						@page WordSection1
							{size:8.5in 11.0in;
							margin:1.0in 1.0in 1.0in 1.0in;}
						div.WordSection1
							{page:WordSection1;}
						--></style><!--[if gte mso 9]><xml>
						<o:shapedefaults v:ext="edit" spidmax="1026" />
						</xml><![endif]--><!--[if gte mso 9]><xml>
						<o:shapelayout v:ext="edit">
						<o:idmap v:ext="edit" data="1" />
						</o:shapelayout></xml><![endif]--></head><body lang=EN-US link=blue vlink=purple>
						<div class=WordSection1><p class=MsoNormal>Hi ,<o:p></o:p></p>
						<p class=MsoNormal>' . $message1 . '.&nbsp; 
						&nbsp;<o:p></o:p></p>
						<table border="1" style="width:50%">
						  <tr>
							<td>Total Records</td>
							<td>Total Updated Records</td>		
							<td>Total Inserted Records</td>
							<td>Total Issued Records</td>
						  </tr>
						  <tr>
							<td>' . $total_count . '</td>
							<td>' . $total_updated_count . '</td>		
							<td>' . $total_inserted_count . '</td>
							<td>' . $Issued_Records . '</td>
						  </tr>
						  
						</table>
						</br>
						</br>
						<p class=MsoNormal>Thank you,<o:p></o:p></p><p class=MsoNormal>CATALYS System<o:p></o:p></p></div></body></html>';

$euser    = get_record('user', 'id', '37417');
$uniqueid = substr(md5(uniqid(rand(), true)), 16, 16);
$filen    = "Navigator Log";
$filename = "$filen.xls";
$conte    = $xlshead . $data . $xlsfoot; //This is previous not there please place it
//file_put_contents("C://data//".$filename, $conte);//This is the command to save your file in server
file_put_contents("C://inetpub//LMSdata//Navigator//" . $filename, $conte); //This is the command to save your file in server
$content = chunk_split(base64_encode($conte));
$uid     = md5(uniqid(time()));
$agname  = "Name Of Recipent";
$header .= "Navigator Log\r\n";
$header .= "Content-Type: multipart/mixed; boundary=\"" . $uid . "\"\r\n\r\n";
$header .= "This is a multi-part message in MIME format.\r\n";
$header .= "--" . $uid . "\r\n";
$header .= "Content-type:text/html; charset=iso-8859-1\r\n";
$header .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
$header .= $employeeMessage . "\r\n\r\n";
$header .= "--" . $uid . "\r\n";
$header .= "Content-Type: application/vnd.ms-excel; name=\"" . $filename . "\"\r\n";
$header .= "Content-Transfer-Encoding: base64\r\n";
$header .= "Content-Disposition: attachment; filename=\"" . $filename . "\"\r\n\r\n";
$header .= $content . "\r\n\r\n";
$header .= "--" . $uid . "--";
$tmpfile = '/Navigator/Navigator Log.xls';
$files   = 'Navigator Log.xls';


if (strlen(trim($message)) >= 10 && !empty(trim($message))) {
    // //Sending error message to the respective team
    email_to_user($euser, '', 'Navigator Feed job Summery', $employeeMessage, $employeeMessage, $tmpfile, $files);
} else {
    email_to_user($euser, '', 'Navigator Feed job Summery', $employeeMessage, $employeeMessage);
}


mtrace("Finished HRDM feed update. \n");

$queryTruncateExit = 'truncate table mdl_navigator_feed;';

$resultTruncateExit = execute_sql($queryTruncateExit, $feedback = true);
//Unset session variables and destroy it
@session_unset();
@session_destroy();
/// finish the IE hack
if (check_browser_version('MSIE')) {
    echo "</xmp>";
}

?>
