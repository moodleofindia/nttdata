<?php


$html = '<html><body><div style="width:100%;font-family:arial;"> <div style="width:100%;height:5%;background-color:#548dd4;border-bottom:1px solid white;">
<div style="width:80%;float:left;text-align:center;color:white;margin-top:10px;font-weight:bold;">Training Effectiveness</div>
<div style="width:20%;float:right;color:white;margin-top:10px;font-weight:bold;">PR – 059E<br>Version 1.00</div>
</div>
<div style="clear:both;"></div>
<div style="width:100%;height:4%;border-top:1px solid white;background-color:#548dd4;">
<div style="width:80%;float:left;color:white;margin-top:10px;font-weight:bold;">Training Details</div>
<div style="width:20%;float:right;color:white;"></div>
</div>
<div style="clear:both;"></div>
<div style="width:100%;">
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#777777;float:left;">
<div style="width:50%;float:left;color:white;margin-top:10px;font-weight:bold;">Training Program Name</div>
</div>
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#FFFFFF;float:right;">
<div style="float:left;font-weight:bold;"><input type="text" style="width:500px;height:36px;" name="prgname"value='.$programname.' /></div>
</div>
</div>
<div style="clear:both;";></div>
<div style="width:100%;">
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#777777;float:left;">
<div style="width:50%;float:left;color:white;margin-top:10px;font-weight:bold;">Skill Improvement Area</div>
</div>
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#FFFFFF;float:right;">
<div style="float:left;font-weight:bold;"><input type="text" style="width:500px;height:36px;" name="skillname"value='.$skillimprovearea.' /></div>
</div>
</div>
<div style="clear:both;";></div>
<div style="width:100%;">
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#777777;float:left;">
<div style="width:50%;float:left;color:white;margin-top:10px;font-weight:bold;">Requestor / Nominator Name</div>
</div>
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#FFFFFF;float:right;">
<div style="float:left;font-weight:bold;"><input type="text" style="width:500px;height:36px;" name="reqname"value='.$requestorname.' /></div>
</div>
</div>
<div style="clear:both;";></div>
<div style="width:100%;">
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#777777;float:left;">
<div style="width:50%;float:left;color:white;margin-top:10px;font-weight:bold;">Training Location</div>
</div>
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#FFFFFF;float:right;">
<div style="float:left;font-weight:bold;"><input type="text" style="width:500px;height:36px;" name="tlocation"value='.$tlocation.' /></div>
</div>
</div>
<div style="clear:both;";></div>
<div style="width:100%;">
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#777777;float:left;">
<div style="width:50%;float:left;color:white;margin-top:10px;font-weight:bold;">Training Start Date</div>
</div>
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#FFFFFF;float:right;">
<div style="float:left;font-weight:bold;"><input type="text" style="width:500px;height:36px;" name="tstart" value='.$timestart.' /></div>
</div>
</div>
<div style="clear:both;";></div>
<div style="width:100%;">
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#777777;float:left;">
<div style="width:50%;float:left;color:white;margin-top:10px;font-weight:bold;">Training End Date</div>
</div>
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#FFFFFF;float:right;">
<div style="float:left;font-weight:bold;"><input type="text" style="width:500px;height:36px;" name="tfinish" value='.$timefinish.' /></div>
</div>
</div>
<div style="clear:both;";></div>
<div style="width:100%;">
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#777777;float:left;">
<div style="width:50%;float:left;color:white;margin-top:10px;font-weight:bold;">Duration</div>
</div>
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#FFFFFF;float:right;">
<div style="float:left;font-weight:bold;"><input type="text" style="width:500px;height:36px;" name="duration" value='.$duration.' /></div>
</div>
</div>
<div style="clear:both;";></div>
<div style="width:100%;">
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#777777;float:left;">
<div style="width:50%;float:left;color:white;margin-top:10px;font-weight:bold;">Trainer Name</div>
</div>
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#FFFFFF;float:right;">
<div style="float:left;font-weight:bold;"><input type="text" style="width:500px;height:36px;" name="duration" value='.$trainername.' /></div>
</div>
</div>
<div style="clear:both;";></div>
<div style="width:100%;height:4%;border-top:1px solid white;background-color:#548dd4;">
<div style="width:80%;float:left;color:white;margin-top:10px;font-weight:bold;">Please provide the feedback below</div>
<div style="width:20%;float:right;color:white;"></div>
</div>
<div style="border:1px solid gray;border-radius:5px;">
<div style="clear:both;"></div>
<div style="width:100%;height:4%;border-top:1px solid white;background-color:#548dd4;">
<div style="width:50%;float:left;color:white;margin-top:10px;font-weight:bold;">Question</div>
<div style="width:20%;float:left;color:white;margin-top:10px;font-weight:bold;">Rating</div>
<div style="width:30%;float:left;color:white;margin-top:10px;font-weight:bold;float:right;">Comments</div>
</div>
<div style="clear:both;"></div>
<div style="width:100%;height:4%;border-top:1px solid white;">
<div style="width:50%;float:left;margin-top:10px;font-weight:bold;">Are you clear with the objectives of the Training Program?</div>
<div style="width:20%;float:left;margin-top:10px;font-weight:bold;">
<select name="q1" required>
<option value="">Choose item</option>
<option value="1">Yes</option>
<option value="2">No</option>
</select>
</div>
<div style="width:30%;float:left;margin-top:10px;font-weight:bold;float:right;"><textarea required name="q1comments" rows="4" cols="30"></textarea></div>
</div>
<div style="clear:both;"></div>
<div style="width:100%;height:4%;border-top:1px solid white;">
<div style="width:50%;float:left;margin-top:10px;font-weight:bold;">Choose your current Skill level</div>
<div style="width:20%;float:left;margin-top:10px;font-weight:bold;">
<select name="q2" required>
<option value="">Choose item</option>
<option value="0">Poor</option>
<option value="1">Average</option>
<option value="2">Good</option>
<option value="3">Excellent</option>
</select>
</div>
<div style="width:30%;float:left;margin-top:10px;font-weight:bold;float:right;"><textarea required name="q2comments" rows="4" cols="30"></textarea></div>
</div>
<div style="clear:both;"></div>
<div style="width:100%;height:4%;border-top:1px solid white;">
<div style="width:50%;float:left;margin-top:10px;font-weight:bold;">List your expectations from the Training Program</div>
<div style="width:50%;float:left;margin-top:10px;font-weight:bold;float:right;"><textarea required name="q3comments" rows="8" cols="55"></textarea></div>
</div>
<div style="clear:both;"></div>
<div style="width:100%;height:4%;border-top:1px solid white;">
<div style="width:50%;float:left;margin-top:10px;font-weight:bold;"></div>
<div style="width:50%;float:left;margin-top:10px;font-weight:bold;float:right;"><input type="submit" name="submit"  value="SUBMIT"/>&nbsp;<input type="reset" name="reset" value="RESET"/></div>
</div>
<div style="clear:both;"></div>
</div></div></body></html>'
;


//==============================================================
//==============================================================
//==============================================================

include("../mpdf.php");
$mpdf=new mPDF('c'); 

$mpdf->WriteHTML($html);
$mpdf->Output();
exit;

//==============================================================
//==============================================================
//==============================================================


?>