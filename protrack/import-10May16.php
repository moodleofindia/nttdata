<?php

require_once('../config.php');

if(isset($_POST["Import"])){
		
		$uploadstatus = true;
		echo "Loading Data...";
		
		$filename=$_FILES["file"]["tmp_name"];
		
		 if($_FILES["file"]["size"] > 0)
		 {
			 $file = fopen($filename, "r");
			//Retrieve the New insert Key for the current upload
			$query_insert_key = 'select max(insertkey) as insertkey from mdl_protrack_upload_temp';
			$insertkey_res = mysql_fetch_array(mysql_query($query_insert_key));
			$insertkey = ++$insertkey_res[insertkey];
						
	         while (($emapData = fgetcsv($file, 10000, ",")) !== FALSE)
	         {
			 
			 //Validate portal ID and fetch Userid in LMS
			 $queryuserid = "select id from mdl_user where username ="."'$emapData[1]'";
					
			 $user = mysql_query($queryuserid);
			 $userids = mysql_fetch_array($user);
			 $userid = $userids[id];
				if(!$userid)
				{
				$userid = null;
				$comment = 'Invalid Portal ID/User does not exist';
				}
				else{
				$comment = 'Successfully imported';
				}
				
			//Convert to date ann time format before inserting into tables
			
				$newdate=date("Y/m/d",strtotime($emapData[4]));
				$hourminute=date("h:m:s",strtotime($emapData[5]));
				
	          //It will insert a row to our staging table from our csv file`
	           $sql = "INSERT INTO `lms`.`mdl_protrack_upload_temp`
						(`name`,
						`portalid`,
						`userid`,
						`sessionname`,
						`trainingtype`,
						`date`,
						`timestart`,
						`duration`,
						`venue`,
						`location`,
						`trainername`,
						`trainerportalid`,
						`insertkey`,
						`comment`)
						VALUES
						(
						'$emapData[0]',
						'$emapData[1]',
						'$userid',
						'$emapData[2]',
						'$emapData[3]',
						'$newdate',
						'$emapData[5]',
						'$emapData[6]',
						'$emapData[7]',
						'$emapData[8]',
						'$emapData[9]',
						'$emapData[10]',
						'$insertkey',
						'$comment')";
				
			  
			  //Insert into the staging table only it is a valid user.
			  if($emapData[1]<>'' || $emapData[1]<>null)
	          {
			  $result = mysql_query($sql);
			  }
			  
			  //we are using mysql_query function. it returns a resource on true else False on error
			  if(! $result )
				{
					echo "<script type=\"text/javascript\">
							alert(\"Invalid File:Please Upload CSV File.\");
							window.location = \"index.php\"
						</script>";
				
				}

			}
	         fclose($file);
	        }
		 
	}	 
	
	// get the last classroom session ID
	$auto_increment = mysql_query("SELECT `AUTO_INCREMENT` as id
							FROM  INFORMATION_SCHEMA.TABLES
							WHERE TABLE_SCHEMA = 'lms'
							AND   TABLE_NAME   = 'mdl_classroom_sessions';") or die(mysql_error());

	 $sessionid = mysql_fetch_array($auto_increment);
	 $sessionid = $sessionid[id];

	//update classroom tables
				
				$SQLSUBMISSIONSSELECT = "SELECT * FROM mdl_protrack_upload_temp where insertkey =$insertkey";
			
				$result_set_tables =  mysql_query($SQLSUBMISSIONSSELECT);
				while($classroomsessions = mysql_fetch_array($result_set_tables))
				{
				
				$queryuploadstatus = true;
				$sql_submission = "INSERT INTO mdl_classroom_submissions(classroom,sessionid,userid,grade,
									mailedconfirmation,mailedreminder,mailedfeedback,mailedabsentees,discountcode,timegraded,
									timecreated,timemodified,timecancelled,notificationtype,attend,cancelreasons)
									VALUES(2126,$sessionid,$classroomsessions[userid],0,1,1,1,0,'no',1,1 ,1,1,1,1,'none');";
					
				/* Naga commnented to add venue & duration
				$sql_sessions = "insert into mdl_classroom_sessions(programename,trainingtype,duration,classroom,status,datetimeknown)
								values('$classroomsessions[sessionname]','$classroomsessions[trainingtype]',TIME_TO_SEC('$classroomsessions[duration]')/60,1,'Completed',1);";
								
								*/
				$sql_sessions = "insert into mdl_classroom_sessions(programename,trainingtype,duration,location,venue,classroom,status,datetimeknown)
								values('$classroomsessions[sessionname]','$classroomsessions[trainingtype]','$classroomsessions[duration]','$classroomsessions[location]','$classroomsessions[venue]',2126,'Completed',1);";
					
					
				$sql_session_date = "insert into mdl_classroom_sessions_dates(sessionid,timestart,timefinish) values
									($sessionid,UNIX_TIMESTAMP('$classroomsessions[date]'),UNIX_TIMESTAMP('$classroomsessions[date]'));";
					
				$sql_trainers = "insert into mdl_classroom_trainners(sessionid,userid,classroom) values
								($sessionid,(select id from mdl_user where username = '$classroomsessions[trainerportalid]'),2126);";
				
				
				//The Transaction to insert into the classroom tables starts here. 
				
				$null = mysql_query("START TRANSACTION");
				$begin = mysql_query("BEGIN");
				$mdl_classroom_submissions = mysql_query($sql_submission);
				$mdl_classroom_sessions = mysql_query($sql_sessions);
				$mdl_classroom_sessions_dates = mysql_query($sql_session_date);
				$mdl_classroom_trainners = mysql_query($sql_trainers);
				$end = mysql_query("END");
				
				// End Transaction here.
				
				
				//If any insertion into table fails turn on the queryuploadstatus flag to call the ROLLBACK query.
				//Also the upload status flag is used to notify the overall status of the import.
				
				if(!$mdl_classroom_submissions || !$mdl_classroom_sessions || !$mdl_classroom_sessions_dates || !$mdl_classroom_trainners){
				$queryuploadstatus = false;
				$uploadstatus = false;
				}
				
				if($queryuploadstatus){
				$commit = mysql_query("COMMIT");
				}
				else{
				$rollback = mysql_query("ROLLBACK");
				$error = "update mdl_protrack_upload_temp set comment =\"Error Uploading\",insertkey = $insertkey  where id=$classroomsessions[id]";
				mysql_query($error);
				}
					
				
				$sessionid++;//Increment Session ID for the next user's insertion.	
				}
	$course->id =1;		
	
	add_to_log($course->id, 'protrack', 'insert', "protrack/import.php", "InsertKey=$insertkey");

	
	//Display the status of the upload as an Alert.
   if($uploadstatus & $_FILES["file"]["size"] > 0){
		echo "<script type=\"text/javascript\">
				alert(\"CSV File has been successfully Imported.\");
				window.location = \"index.php\"
				</script>";	
		}
		else if ($_FILES["file"]["size"] <= 0){
		echo "<script type=\"text/javascript\">
				alert(\"Invalid File:Please upload a CSV File\");
				window.location = \"index.php\"
				</script>";
		}
		else{
		echo "<script type=\"text/javascript\">
				alert(\"CSV File has errors. Please recheck and try again\");
				window.location = \"index.php\"
				</script>";	
		}

?>		 