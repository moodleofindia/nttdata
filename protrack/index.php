<!DOCTYPE html>
<?php
require_once('../config.php');
require_login();
session_start(); 
$pagetitle = 'Import Pro-track data To CATALYS Database';
$navlinks[] = array('name' => $pagetitle, 'link' => '', 'type' => 'activityinstance');
$navigation = build_navigation($navlinks);
print_header_simple($pagetitle, '', $navigation);	


?>	
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Import Pro-track data To CATALYS Database </title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="Import Excel File To MySql Database Using php">
<!--
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="css/bootstrap-responsive.min.css">
		<link rel="stylesheet" href="css/bootstrap-custom.css">
-->


	</head>
	<body>    

	<!-- Navbar
    ================================================== 

	<div class="navbar navbar-inverse navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container"> 
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<a class="brand" href="#">Import Pro-track data To CATALYS Database</a>
				
			</div>
		</div>
	</div> -->

	<div id="wrap">
	<div class="container">
		<div class="row">
			<div class="span3 hidden-phone"></div>
			<div class="span6" id="form-login">
				<form class="form-horizontal well" action="import.php" method="post" name="upload_excel" enctype="multipart/form-data">
					<fieldset>
						<legend>Import CSV/Excel file</legend>
						<div class="control-group">
							<div class="control-label">
								<label>CSV/Excel File:</label>
							</div>
							<div class="controls">
								<input type="file" name="file" id="file" class="input-large">
							</div>
						</div>
						<br>
							<div class="control-group">
							<div class="controls">
							<button type="submit" id="submit" name="Import" class="btn btn-primary button-loading" data-loading-text="Loading...">Upload</button>
							</div>
						</div>
				</form>
						<br>
						<form action="export.php" method="post" name="export_excel">
						<button type="submit" id="export" name="export" class="btn btn-primary button-loading" data-loading-text="Loading...">Export Errors to Excel File</button>
						</form>
						<br>
					</fieldset>
				
				
			</div>
			<div class="span3 hidden-phone"></div>
		</div>

		
		<br>
		<br>
		<label> Last Transaction Details</label>
		<br>
		<br>
		<table border = "1" color ="#DCDCDC" cellspacing="10" width="100%">
			<thead bgcolor = "#DCDCDC" color ="white">
				  	<tr>
				  		<th>Name</th>
				  		<th>Portal ID</th>
				  		<th>Name of the training</th>
				  		<th>Type</th>
				  		<th>Date</th>
				  		<th>TimeStart</th>
				  		<th>Duration</th>
						<th>Venue</th>
						<th>Location</th>
				  		<th>Trainer</th>
				  		<th>Portal ID of trainer</th>
				  		<th>Status</th>
				  	</tr>	
				  </thead>
			<?php
				
				$query_display_key = 'select max(insertkey) as insertkey from mdl_protrack_upload_temp';
				$display_key_res = mysql_fetch_array(mysql_query($query_display_key));
				$display_key = $display_key_res[insertkey];
				
				$SQLSELECT = "SELECT * FROM mdl_protrack_upload_temp where insertkey =".$display_key;
				$result_set =  mysql_query($SQLSELECT);
				while($row = mysql_fetch_array($result_set))
				{
				?>
			
					<tr>
						<td><?php echo $row['name']; ?></td>
						<td><?php echo $row['portalid']; ?></td>
						<td><?php echo $row['sessionname']; ?></td>
						<td><?php echo $row['trainingtype']; ?></td>
						<td><?php echo $row['date']; ?></td>
						<td><?php echo $row['timestart']; ?></td>
						<td><?php echo $row['duration']; ?></td>
						<td><?php echo $row['venue']; ?></td>
						<td><?php echo $row['location']; ?></td>
						<td><?php echo $row['trainername']; ?></td>
						<td><?php echo $row['trainerportalid']; ?></td>
						<td><?php echo $row['comment']; ?></td>
					

					</tr>
				<?php
				}
	
			?>
		</table>
				<br>
		<br>
		
	</div>

	</div>

	</body>

</html>

<?php
print_footer();
?>