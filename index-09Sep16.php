
<?php  // $Id: index.php,v 1.201.2.10 2009/04/25 21:18:24 stronk7 Exp $
       // index.php - the front page.

///////////////////////////////////////////////////////////////////////////
//                                                                       //
// NOTICE OF COPYRIGHT                                                   //
//                                                                       //
// Moodle - Modular Object-Oriented Dynamic Learning Environment         //
//          http://moodle.org                                            //
//                                                                       //
// Copyright (C) 1999 onwards  Martin Dougiamas  http://moodle.com       //
//                                                                       //
// This program is free software; you can redistribute it and/or modify  //
// it under the terms of the GNU General Public License as published by  //
// the Free Software Foundation; either version 2 of the License, or     //
// (at your option) any later version.                                   //
//                                                                       //
// This program is distributed in the hope that it will be useful,       //
// but WITHOUT ANY WARRANTY; without even the implied warranty of        //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         //
// GNU General Public License for more details:                          //
//                                                                       //
//          http://www.gnu.org/copyleft/gpl.html                         //
//                                                                       //
///////////////////////////////////////////////////////////////////////////




    require_once('config.php');
    require_once($CFG->dirroot .'/course/lib.php');
    require_once($CFG->dirroot .'/lib/blocklib.php');

    if (empty($SITE)) {
        redirect($CFG->wwwroot .'/'. $CFG->admin .'/index.php');
    }

    // Bounds for block widths
    // more flexible for theme designers taken from theme config.php
    $lmin = (empty($THEME->block_l_min_width)) ? 100 : $THEME->block_l_min_width;
    $lmax = (empty($THEME->block_l_max_width)) ? 210 : $THEME->block_l_max_width;
    $rmin = (empty($THEME->block_r_min_width)) ? 100 : $THEME->block_r_min_width;
    $rmax = (empty($THEME->block_r_max_width)) ? 210 : $THEME->block_r_max_width;

    define('BLOCK_L_MIN_WIDTH', $lmin);
    define('BLOCK_L_MAX_WIDTH', $lmax);
    define('BLOCK_R_MIN_WIDTH', $rmin);
    define('BLOCK_R_MAX_WIDTH', $rmax);

    // check if major upgrade needed - also present in login/index.php
    if ((int)$CFG->version < 2006101100) { //1.7 or older
        @require_logout();
        redirect("$CFG->wwwroot/$CFG->admin/");
    }
    // Trigger 1.9 accesslib upgrade?
    if ((int)$CFG->version < 2007092000 
        && isset($USER->id) 
        && is_siteadmin($USER->id)) { // this test is expensive, but is only triggered during the upgrade
        redirect("$CFG->wwwroot/$CFG->admin/");
    }

    if ($CFG->forcelogin) {
        require_login();
    } else {
        user_accesstime_log();
    }

    if ($CFG->rolesactive) { // if already using roles system
        if (has_capability('moodle/site:config', get_context_instance(CONTEXT_SYSTEM))) {
            if (moodle_needs_upgrading()) {
                redirect($CFG->wwwroot .'/'. $CFG->admin .'/index.php');
            }
        } else if (!empty($CFG->mymoodleredirect)) {    // Redirect logged-in users to My Moodle overview if required
            if (isloggedin() && $USER->username != 'guest') {
                redirect($CFG->wwwroot .'/my/index.php');
            }
        }
    } else { // if upgrading from 1.6 or below
        if (isadmin() && moodle_needs_upgrading()) {
            redirect($CFG->wwwroot .'/'. $CFG->admin .'/index.php');
        }
    }


    if (get_moodle_cookie() == '') {
        set_moodle_cookie('nobody');   // To help search for cookies on login page
    }

    if (!empty($USER->id)) {
        add_to_log(SITEID, 'course', 'view', 'view.php?id='.SITEID, SITEID);
    }

    if (empty($CFG->langmenu)) {
        $langmenu = '';
    } else {
        $currlang = current_language();
        $langs = get_list_of_languages();
        $langlabel = get_accesshide(get_string('language'));
        $langmenu = popup_form($CFG->wwwroot .'/index.php?lang=', $langs, 'chooselang', $currlang, '', '', '', true, 'self', $langlabel);
    }

    $PAGE       = page_create_object(PAGE_COURSE_VIEW, SITEID);
    $pageblocks = blocks_setup($PAGE);
    $editing    = $PAGE->user_is_editing();
    $preferred_width_left  = bounded_number(BLOCK_L_MIN_WIDTH, blocks_preferred_width($pageblocks[BLOCK_POS_LEFT]),
                                            BLOCK_L_MAX_WIDTH);
    $preferred_width_right = bounded_number(BLOCK_R_MIN_WIDTH, blocks_preferred_width($pageblocks[BLOCK_POS_RIGHT]),
                                            BLOCK_R_MAX_WIDTH);
    print_header($SITE->fullname, $SITE->fullname, 'home', '',
                 '<meta name="description" content="'. strip_tags(format_text($SITE->summary, FORMAT_HTML)) .'" />',
                 true, '', user_login_string($SITE).$langmenu);

?>


<table id="layout-table" summary="layout">
  <tr>

  <?php
    $lt = (empty($THEME->layouttable)) ? array('left', 'middle', 'right') : $THEME->layouttable;
    foreach ($lt as $column) {
        switch ($column) {
            case 'left':
    if (blocks_have_content($pageblocks, BLOCK_POS_LEFT) || $editing) {
        echo '<td style="width: '.$preferred_width_left.'px;" id="left-column">';
        print_container_start();
        blocks_print_group($PAGE, $pageblocks, BLOCK_POS_LEFT);
        print_container_end();
        echo '</td>';
    }
            break;
            case 'middle':
    echo '<td id="middle-column">'. skip_main_destination();

    print_container_start();
?>

	<div id="extruderLeft" class="{title:'+ Points', url:'<?php echo $CFG->themewww .'/'. current_theme() ?>/pannel/parts/extruderLeft.php'}"></div>

<?php 

?>

<head>

<SCRIPT TYPE="text/javascript">
<!--
function popup(mylink, windowname)
{
if (! window.focus)return true;
var href;
if (typeof(mylink) == 'string')
   href=mylink;
else
   href=mylink.href;
window.open(href, windowname, 'width=800,height=500,scrollbars=yes');
return false;
}
//-->
</SCRIPT>
  <!-- SlidesJS Optional: If you'd like to use this design -->
  <link rel="stylesheet" type="text/css" href="./frontpage/css/style.css">
  <style>


    #slides {
      display: none
    }

    #slides .slidesjs-navigation {
      margin-top:5px;
    }

    a.slidesjs-next,
    a.slidesjs-previous,
    a.slidesjs-play,
    a.slidesjs-stop {
      background-image: url(/frontpage/img/btns-next-prev.png);
      background-repeat: no-repeat;
      display:block;
      width:12px;
      height:18px;
      overflow: hidden;
      text-indent: -9999px;
      float: left;
      margin-right:5px;
    }

    a.slidesjs-next {
      margin-right:10px;
      background-position: -12px 0;
    }

    a:hover.slidesjs-next {
      background-position: -12px -18px;
    }

    a.slidesjs-previous {
      background-position: 0 0;
    }

    a:hover.slidesjs-previous {
      background-position: 0 -18px;
    }

    a.slidesjs-play {
      width:15px;
      background-position: -25px 0;
    }

    a:hover.slidesjs-play {
      background-position: -25px -18px;
    }

    a.slidesjs-stop {
      width:18px;
      background-position: -41px 0;
    }

    a:hover.slidesjs-stop {
      background-position: -41px -18px;
    }

    .slidesjs-pagination {
      margin: 7px 0 0;
      float: right;
      list-style: none;
    }

    .slidesjs-pagination li {
      float: left;
      margin: 0 1px;
    }

    .slidesjs-pagination li a {
      display: block;
      width: 13px;
      height: 0;
      padding-top: 13px;
      background-image: url(/frontpage/img/pagination.png);
      background-position: 0 0;
      float: left;
      overflow: hidden;
    }

    .slidesjs-pagination li a.active,
    .slidesjs-pagination li a:hover.active {
      background-position: 0 -13px
    }

    .slidesjs-pagination li a:hover {
      background-position: 0 -26px
    }

    #slides a:link,
    #slides a:visited {
      color: #333
    }

    #slides a:hover,
    #slides a:active {
      color: #9e2020
    }

    .navbar {
      overflow: hidden
    }
  </style>
  <!-- End SlidesJS Optional-->

 
</head>

<!--Change By Raghu -->
<?php 
function tzdelta ( $iTime = 0 ) 
{ 
if ( 0 == $iTime ) { $iTime = time(); } 
$ar = localtime ( $iTime ); 
$ar[5] += 1900; $ar[4]++; 
$iTztime = gmmktime ( $ar[2], $ar[1], $ar[0], $ar[4], $ar[3], $ar[5], $ar[8] ); 
return ( $iTztime - $iTime )/3600; 
}

$timeto=time();
$timezonul=$USER->timezone;
if (($timezonul == '99') AND (tzdelta()>=0) ) { $timezonul = "+".tzdelta() ;}
else if (($timezonul == '99') AND (tzdelta()<0)) {$timezonul = tzdelta();}

$dates = userdate($timeto,get_string('strftimedateminute'),$timezonul);
$strtime=preg_replace('/- /', '-', $dates);
$time_value=get_record_sql("SELECT unix_timestamp('$strtime') as usercurrecttime");
$time= $time_value->usercurrecttime;

$sel_value=get_record_sql("SELECT timeclose,timeopen FROM mdl_quiz where id=327");
$timeclose= $sel_value->timeclose;
$timeopen= $sel_value->timeopen;
if(($timeclose > $time) && ($timeopen<= $time)){
$totalreportscountrecords = count_records_sql("SELECT COUNT(id) FROM mdl_quiz_attempts where userid=$USER->id and quiz=327");
if($totalreportscountrecords>=1){

$selid=get_record_sql("select max(id) as ID from mdl_quiz_attempts where userid=$USER->id and quiz=327");
$maxid=$selid->ID;
$usersel =get_record_sql("select timefinish as timefinished from mdl_quiz_attempts where id=$maxid");
$usertimefinished=$usersel->timefinished;
if($usertimefinished!='0'){
$currentdayss="select DAYNAME(from_unixtime('$usertimefinished')) as lastday";
$currentdays=get_record_sql($currentdayss);
$usercmpltday=$currentdays->lastday;

switch ($usercmpltday) {
    case "Monday":
        $i=6;
        break;
    case "Tuesday":
        $i=5;
        break;
    case "Wednesday":
        $i=4;
        break;
	case "Thursday":
        $i=3;
        break;
	case "Friday":
        $i=2;
        break;
	case "Saturday":
        $i=1;
        break;
	case "Sunday":
        $i=0;
        break;
    default:
        echo " ";
		break;
}
$latattemptsel="select unix_timestamp((from_unixtime(timefinish)) + interval $i day) as lastattempt from mdl_quiz_attempts where id=$maxid";
$latattemptdata=get_record_sql($latattemptsel);
if ($time > $latattemptdata->lastattempt) {?>
<body onLoad="popup('https://lms.portal.nttdatainc.com/mod/quiz/view.php?id=19566&game=1')">
<?php } } else{?>
<body onLoad="open_on_entrance();">
<?php } } else {?>
<body onLoad="popup('https://lms.portal.nttdatainc.com/mod/quiz/view.php?id=19566&game=1')">
<?php } } ?>

<script type="text/javascript">
function open_on_entrance(url,name)
{ 
   window.open('https://lms.portal.nttdatainc.com/mod/quiz/message.php','Catalys', 'width=480,height=100,left=10px,top=10px');
}
</script>
<!--Code End Raghu-->	
	

  <!-- SlidesJS Required: Start Slides -->
  <!-- The container is used to define the width of the slideshow -->
  <div class="container">
    <div id="slides">
      <img src="<?php echo $CFG->wwwroot;?>/file.php/1/frontpage/img/banner1.jpg" >
      <img src="<?php echo $CFG->wwwroot;?>/file.php/1/frontpage/img/banner2.jpg" >
      <img src="<?php echo $CFG->wwwroot;?>/file.php/1/frontpage/img/banner3.jpg" >
    <a href="https://lms.portal.nttdatainc.com/course/view.php?id=8446"> <img src="<?php echo $CFG->wwwroot;?>/file.php/1/frontpage/img/banner4.jpg" width='100%' height='100%'></a>
	     </div>
  </div>
  <!-- End SlidesJS Required: Start Slides -->

  <!-- SlidesJS Required: Link to jQuery -->

  <!-- End SlidesJS Required -->

  <!-- SlidesJS Required: Link to jquery.slides.js -->
  <script src="/frontpage/js/jquery.slides.min.js"></script>
  <!-- End SlidesJS Required -->

  <!-- SlidesJS Required: Initialize SlidesJS with a jQuery doc ready -->
  <script>
    $(function() {
      $('#slides').slidesjs({
        width: 400,
        height: 100,
        play: {
          active: true,
          auto: true,
          interval: 8000,
          swap: true
        }
      });
    });
  </script>
  <!-- End SlidesJS Required -->
  <!-- Page peal  -->
  <!--
<script type="text/javascript" src="<?php echo $CFG->wwwroot;?>/theme/aardvark-svc/jQuery.pagePeel.1.2.1.js"></script>

<script type="text/javascript">
	$(document).ready(function(){
		// alert ('jQuery running');
		
		$('#pagePeel').pagePeel({
			introAnim: true
		});
		
			
		$('a').linkControl({overlay:true});
	});
</script>
      <div id="pagePeel"></div>  -->
<div class="container clearfix fitvids">
<div class="home-wrap clearfix">

<div class="extendwrap lightgrey-bg padding20 margin25">
<div class="one-half">
<p class="font22 margin5 centered blue-text">Course Catalogue</a></p>
<p class="font16 centered line12em">Best of all training worlds</p>
<div class="one-third centered">
<p class="margin0"><img src="<?php echo $CFG->wwwroot;?>/frontpage/img/catalogue.png"></p>
<p class="uppercase margin0 line12em"><strong><a href="<?php echo $CFG->wwwroot;?>/course/catalogue.php?id=6" target="_new">E Learning<br>
Catalogue</a></strong></p>
<p class="line12em">More than 250+ courses in IT,Project Management,Behavioral and Process</p>
</div>
<div class="one-third centered">
<p class="margin0"><img src="<?php echo $CFG->wwwroot;?>/frontpage/img/calendar.png"></p>
<p class="uppercase margin0 line12em"><strong><a href="<?php echo $CFG->wwwroot;?>/blocks/classroom/currentcalender.php" target="_new">ILT<br>
Calendar</a></strong></p>
<p class="line12em">Sign up for classroom training in your location</p>
</div>
<div class="one-third column-last centered">
<p class="margin0"><img src="<?php echo $CFG->wwwroot;?>/frontpage/img/skillsoft.png"></p>
<p class="uppercase margin0 line12em"><strong><a href="<?php echo $CFG->wwwroot;?>/course/skillsoftcatalogue.php?id=49" target="_blank">SkillSoft<br>
Catalogue</a></strong></p>
<p class="line12em">More than 5000+ courses in various disciplines</p>
</div>
<div class="clear"></div>
</div>
<div align="center" class="one-half column-last">
<img height="250" width="260" src="<?php echo $CFG->wwwroot;?>/frontpage/img/catplay.jpg"  class="alignright size-full wp-image-2771">
</div>
<div class="clear"></div>
</div>
<div class="extendwrap padding20 margin25">
<div align="center" class="one-half">
<img height="250" width="260" src="<?php echo $CFG->wwwroot;?>/frontpage/img/certplay.jpg" >
</div>
<div class="one-half column-last">
<p class="font22 margin5 centered blue-text">Programs and Certifications</p>
<p class="font16 centered line12em">For custom and advanced E-Learning Projects</p>
<div class="one-third centered">
<p class="margin0"><img src="<?php echo $CFG->wwwroot;?>/frontpage/img/nlci.png"></p>
<p class="uppercase margin0 line12em"><a href="<?php echo $CFG->wwwroot;?>/course/catalogue.php?id=9" target="_blank"><strong>NLCI Certifications</strong></a></p>
<p class="line12em">Certifications on various streams and practices</p>
</div>
<div class="one-third centered">
<p class="margin0"><img src="<?php echo $CFG->wwwroot;?>/frontpage/img/programs.png"></p>
<p class="uppercase margin0 line12em"><a href="<?php echo $CFG->wwwroot;?>/course/catalogue.php?id=114" target="_blank"><strong>Training Programs</strong></a></p>
<p class="line12em">A variety of development programs leveraging the best of training resources and on the job experience.</p>
</div>
<div class="one-third column-last centered">
<p class="margin0"><img src="<?php echo $CFG->wwwroot;?>/frontpage/img/externalCert.png"></p>
<p class="uppercase margin0 line12em"><a href="<?php echo $CFG->wwwroot;?>/course/skillsoftcatalogue.php?id=1000000" target="_blank"><strong>External Certifications</strong></a></p>
<p class="line12em">Prepare for external certifications using SkillSoft courses and Books 24x7</p>
</div>
<div class="clear"></div>
</div>
<div class="clear"></div>
</div>

<div class="extendwrap lightgrey-bg padding20 margin25">
<p class="centered font22 blue-text margin5">CATALYS Training &amp; Support</p>
<p class="centered font16">Video based training in CATALYS support and FAQ</p>
<div class="one-third centered">
<p><img src="<?php echo $CFG->wwwroot;?>/frontpage/img/tour.png"></p>
<p class="uppercase margin5"><a href="<?php echo $CFG->wwwroot;?>/course/view.php?id=7007" target="_blank"><strong>Take a tour</strong></a></p>
<p>Know your CATALYS features
</p>
</div>
<div class="one-third centered">
<p><img src="<?php echo $CFG->wwwroot;?>/frontpage/img/faq.png"></p>
<p class="uppercase margin5"><a href="<?php echo $CFG->wwwroot;?>/file.php/1/faq.htm" target="_blank"><strong>FAQ</strong></a></p>
<p>Frequently asked questions on CATALYS operations<br>
and Video resources</p>
</div>
<div class="one-third column-last centered">
<p><img src="<?php echo $CFG->wwwroot;?>/frontpage/img/contact.png"></p>
<p class="uppercase margin5"><a href="<?php echo $CFG->wwwroot;?>/contactus/contact.php" target="_blank"><strong>Contact Us</strong></a></p>
<p>Use the form to reach our various helpdesk's with queries</p>
</div>
</div>
  
</div>


	

    
</div>
<!-- /home-projects -->      	


        
  	



<!-- END home-wrap -->   


</body>



	
<?php
    if ($SITE->numsections > 0) {

        if (!$section = get_record('course_sections', 'course', $SITE->id, 'section', 1)) {
            delete_records('course_sections', 'course', $SITE->id, 'section', 1); // Just in case
            $section->course = $SITE->id;
            $section->section = 1;
            $section->summary = '';
            $section->sequence = '';
            $section->visible = 1;
            $section->id = insert_record('course_sections', $section);
        }

        if (!empty($section->sequence) or !empty($section->summary) or $editing) {
            print_box_start('generalbox sitetopic');

            /// If currently moving a file then show the current clipboard
            if (ismoving($SITE->id)) {
                $stractivityclipboard = strip_tags(get_string('activityclipboard', '', addslashes($USER->activitycopyname)));
                echo '<p><font size="2">';
                echo "$stractivityclipboard&nbsp;&nbsp;(<a href=\"course/mod.php?cancelcopy=true&amp;sesskey=$USER->sesskey\">". get_string('cancel') .'</a>)';
                echo '</font></p>';
            }

            $options = NULL;
            $options->noclean = true;
            echo format_text($section->summary, FORMAT_HTML, $options);

            if ($editing) {
                $streditsummary = get_string('editsummary');
                echo "<a title=\"$streditsummary\" ".
                     " href=\"course/editsection.php?id=$section->id\"><img src=\"$CFG->pixpath/t/edit.gif\" ".
                     " class=\"iconsmall\" alt=\"$streditsummary\" /></a><br /><br />";
            }

            get_all_mods($SITE->id, $mods, $modnames, $modnamesplural, $modnamesused);
            print_section($SITE, $section, $mods, $modnamesused, true);

            if ($editing) {
                print_section_add_menus($SITE, $section->section, $modnames);
            }
            print_box_end();
			 print_course_search('', false, 'short');
        }
    }

    if (isloggedin() and !isguest() and isset($CFG->frontpageloggedin)) {
        $frontpagelayout = $CFG->frontpageloggedin;
    } else {
        $frontpagelayout = $CFG->frontpage;
    }

    foreach (explode(',',$frontpagelayout) as $v) {
        switch ($v) {     /// Display the main part of the front page.
            case FRONTPAGENEWS:
                if ($SITE->newsitems) { // Print forums only when needed
                    require_once($CFG->dirroot .'/mod/forum/lib.php');

                    if (! $newsforum = forum_get_course_forum($SITE->id, 'news')) {
                        error('Could not find or create a main news forum for the site');
                    }

                    if (!empty($USER->id)) {
                        $SESSION->fromdiscussion = $CFG->wwwroot;
                        $subtext = '';
                        if (forum_is_subscribed($USER->id, $newsforum)) {
                            if (!forum_is_forcesubscribed($newsforum)) {
                                $subtext = get_string('unsubscribe', 'forum');
                            }
                        } else {
                            $subtext = get_string('subscribe', 'forum');
                        }
                        print_heading_block($newsforum->name);
                        echo '<div class="subscribelink"><a href="mod/forum/subscribe.php?id='.$newsforum->id.'">'.$subtext.'</a></div>';
                    } else {
                        print_heading_block($newsforum->name);
                    }

                    forum_print_latest_discussions($SITE, $newsforum, $SITE->newsitems, 'plain', 'p.modified DESC');
                }
            break;

            case FRONTPAGECOURSELIST:

                if (isloggedin() and !has_capability('moodle/site:config', get_context_instance(CONTEXT_SYSTEM)) and !isguest() and empty($CFG->disablemycourses)) {
                    print_heading_block(get_string('mycourses'));
                    print_my_moodle();
                } else if ((!has_capability('moodle/site:config', get_context_instance(CONTEXT_SYSTEM)) and !isguest()) or (count_records('course') <= FRONTPAGECOURSELIMIT)) {
                    // admin should not see list of courses when there are too many of them
                    print_heading_block(get_string('availablecourses'));
                    print_courses(0);
                }
            break;

            case FRONTPAGECATEGORYNAMES:

                print_heading_block(get_string('categories'));
                print_box_start('generalbox categorybox');
              //print_whole_category_list(NULL, NULL, NULL, -1, false);
				include('./theme/'. current_theme() .'/accordion_course_menu/accordion_course_menu.php');
                print_box_end();
               
            break;

            case FRONTPAGECATEGORYCOMBO:

                print_heading_block(get_string('categories'));
                print_box_start('generalbox categorybox');
                print_whole_category_list(NULL, NULL, NULL, -1, true);
                print_box_end();
                print_course_search('', false, 'short');
            break;

            case FRONTPAGETOPICONLY:    // Do nothing!!  :-)
            break;

        }
        echo '<br />';
    }

    print_container_end();

    echo '</td>';
            break;
            case 'right':
    // The right column
    if (blocks_have_content($pageblocks, BLOCK_POS_RIGHT) || $editing || $PAGE->user_allowed_editing()) {
        echo '<td style="width: '.$preferred_width_right.'px;" id="right-column">';
        print_container_start();
        if ($PAGE->user_allowed_editing()) {
            echo '<div style="text-align:center">'.update_course_icon($SITE->id).'</div>';
            echo '<br />';
        }
        blocks_print_group($PAGE, $pageblocks, BLOCK_POS_RIGHT);
        print_container_end();
        echo '</td>';
    }
            break;
        }
    }
?>

  </tr>
</table>
 
      
<?php
    print_footer('home');     // Please do not modify this line
?>
