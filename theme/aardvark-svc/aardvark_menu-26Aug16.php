<link href="/theme/aardvark-svc/tree.css" rel="stylesheet" type="text/css" />
<script src="/theme/aardvark-svc/tree.js" type="text/javascript"></script>
<style>
ol.tree_new
{
	padding: 0 0 0 0px;
	width: 100px;
}
	li 
	{ 
		position: relative; 
		margin-left: 0px;
		list-style: none;
	}
	li.file
	{
		margin-left: -1px !important;
	}
		li.file a
		{
			background: url(document.png) 0 0 no-repeat;
			color: #fff;
			padding-left: 0px;
			text-decoration: none;
			display: block;
		}
		li.file a[href *= '.pdf']	{ background: url(document.png) 0 0 no-repeat; }
		li.file a[href *= '.html']	{ background: url(document.png) 0 0 no-repeat; }
		li.file a[href $= '.css']	{ background: url(document.png) 0 0 no-repeat; }
		li.file a[href $= '.js']		{ background: url(document.png) 0 0 no-repeat; }
	li input
	{
		position: absolute;
		left: 0;
		margin-left: 0;
		opacity: 0;
		z-index: 2;
		cursor: pointer;
		height: 1em;
		width: 1em;
		top: 0;
	}
		li input + ol
		{
			background: url(/theme/aardvark-svc/dist/toggle-small-expand.png) 30px 0 no-repeat;
			margin: -0.938em 0 0 -44px; /* 15px */
			height: 1em;
		}
		li input + ol > li { display: none; margin-left: 0px !important; padding-left: 1px; }
	li label
	{
		background: url(/theme/aardvark-svc/images/menu/blue-arrow.gif) 10px 1px no-repeat;
		cursor: pointer;
		display: block;
		padding-left: 25px;
	}

	li input:checked + ol
	{
		background: url(/theme/aardvark-svc/dist/toggle-small.png) 33px 5px no-repeat;
		margin: -1.25em 0 0 -44px; /* 20px */
		padding: 1.263em 0 0 42px;
		height: auto;
	}
	#tree_menu_new li:hover {
	border: 0px solid #777777;
	background: none;
	}
		li input:checked + ol > li { display: block; margin: 0 0 0.125em;  /* 2px */}
		li input:checked + ol > li:last-child { margin: 0 0 0.063em; /* 1px */ }
</style>
<div id="dropdown" class="yuimenubar yuimenubarnav">
<div>
<div>
    <ul id="menu">
    
    <li><a href="/index.php" class="drop">Home</a><!-- Begin Home Item -->
    
        
    
    </li><!-- End Home Item -->

	 <li><a href="#" class="drop">Course Catalogue</a><!-- Begin 4 columns Item -->
    
        <div class="dropdown_4columns"><!-- Begin 4 columns container -->

   
            
            <div class="col_1">
            
            <h3>Internal Offerings</h3>
			<ul id="treemenu1" ><!-- deleted ** class="treeview" -->
			<li  ><a href="/course/catalogue.php?id=1000004">Technical</a></li>
			<li  ><a href="/mod/resource/view.php?id=12897">Project Management - Global Delivery</a></li>
			<li  ><a href="/mod/resource/view.php?id=17692">Project Management - Enterprise Services</a></li>
			<li  ><a href="/mod/resource/view.php?id=19552">Project Synergy</a></li>
			<li  ><a href="/mod/resource/view.php?id=18133">Navigator</a></li>
			<li  ><a href="/course/catalogue.php?id=24">Behavioral</a></li>
			<li  ><a href="/course/catalogue.php?id=26">Process</a></li>
			<li  ><a href="/course/catalogue.php?id=110">Compliance</a></li>
			
	</ul>
	       


            </div>
        <div class="col_1">
            
                <h3>External Offerings</h3>
                <ul>
                    <li><a href="/course/skillsoftcatalogue.php?id=49">Skill Soft</a></li>
                   
                </ul>   
                 
            </div>
			 </div>
			</li>
			
	 <li><a href="#" class="drop">Programs</a><!-- Begin 4 columns Item -->
    
        <div class="dropdown_5columns"><!-- Begin 4 columns container -->
         <div class="col_3">

                <img src="/homefiles/lead.gif" width="70" height="70" class="img_left imgshadow" alt="" />
                <p><a href="#"></a><a href="/course/programcatalogue.php?id=114">A variety of development programs leveraging the best of training resources and on the job experience. Click here to view.</a></p>
    
                
            </div>

        </div><!-- End 4 columns container -->
    
    </li><!-- End 4 columns Item -->
    <li ><a href="#" class="drop">NLCI Certifications </a><!-- Begin 5 columns Item -->
    
        <div class="dropdown_5columns"><!-- Begin 5 columns container -->
       
            
        
            <div class="col_5">
                <h3>List of available NLCI Certifications
				<?php if(get_records_sql("SELECT id,userid,roleid FROM mdl_role_assignments  where roleid in('1')  and contextid=1 and userid='$USER->id'"))
		{ ?>
				<a href="../../../menu/menu_dom.php" style="margin-left: 253px;margin-top: -13px;"><img src="/theme/aardvark-svc/images/menu/images.png"></a>
				<?php } ?></h3>
				
            </div>
           <br/>
		  
	<!-- starting of jquery tree view -->
	<div class="col_6">
	<h3>India</h3>
             
<?php 
	 $qry="SELECT * FROM treeview_items1 where status='1'";
 $result=mysql_query($qry);


 $arrayCategories = array();

 while($row = mysql_fetch_assoc($result)){ 
 $arrayCategories[$row['Id']] = array("parent_id" => $row['parent_id'], "name" => $row['name'], "link" => $row['link']);   
  }
//createTree($arrayCategories, 0);

 function createTree($array, $currentParent, $currLevel = 0, $prevLevel = -1) {

foreach ($array as $categoryId => $category) {

if ($currentParent == $category['parent_id']) {                       

    if ($currLevel > $prevLevel) echo " <ol class='tree_new'> "; 

    if ($currLevel == $prevLevel) echo " </li> ";

    echo '<li id="tree_menu_new" style="width: 167px;margin-top:0px;border:none;text-align:left;"> <label for="subfolder2">';
	if($category['link']!=""){
	echo '<a href="'.$category['link'].'" style="margin-left:1px;" >';
	} else{
	echo '<a href="javascript:void(0);" style="margin-left:1px;">';
	}
	
	echo $category['name'].'</a></label> <input type="checkbox" id="subfolder2"/>';

    if ($currLevel > $prevLevel) { $prevLevel = $currLevel; }

    $currLevel++; 

    createTree ($array, $categoryId, $currLevel, $prevLevel);

    $currLevel--;               
    }   

}

if ($currLevel == $prevLevel) echo " </li>  </ol> ";

}   
?>
<div style="margin-left:-5px;width=190px;">
<?php
 if(mysql_num_rows($result)!=0)
 {
?>
<?php 

createTree($arrayCategories, 0); ?>
<?php } ?>
</div>
</div> 

<!-- end of jquey tree view -->
            <div class="col_6">
            
                <h3>Rest of the World</h3>
            <?php 
	 $qry="SELECT * FROM treeview_items2 where status='1'";
     $result=mysql_query($qry);
     $arrayCategoriess = array();

 while($row = mysql_fetch_assoc($result)){ 
 $arrayCategoriess[$row['Id']] = array("parent_id" => $row['parent_id'], "name" => $row['name'], "link" => $row['link']);   
  }
//createTree($arrayCategories, 0);

 function createTrees($array, $currentParent, $currLevel = 0, $prevLevel = -1) {

foreach ($array as $categoryId => $category) {

if ($currentParent == $category['parent_id']) {                       

    if ($currLevel > $prevLevel) echo " <ol class='tree_new'> "; 

    if ($currLevel == $prevLevel) echo " </li> ";

    echo '<li id="tree_menu_new"  style="width: 163px;margin-top:0px;border:none;text-align:left;"> <label for="subfolder2">';
	if($category['link']!=""){
	echo '<a href="'.$category['link'].'" style="margin-left:1px;" >';
	} else{
	echo '<a href="javascript:void(0);" style="margin-left:1px;">';
	}
	
	echo $category['name'].'</a></label> <input type="checkbox" id="subfolder2"/>';

    if ($currLevel > $prevLevel) { $prevLevel = $currLevel; }

    $currLevel++; 

    createTree ($array, $categoryId, $currLevel, $prevLevel);

    $currLevel--;               
    }   

}

if ($currLevel == $prevLevel) echo " </li>  </ol> ";

}   
?>
<div style="margin-left:1px">
<?php
 if(mysql_num_rows($result)!=0)
 {
?>
<?php 

createTrees($arrayCategoriess, 0); ?>
<?php } ?>
</div>
<br/>

        </div><!-- End 5 columns container -->
</div>
    </li><!-- End 5 columns Item -->


	<li><a href="#" class="drop">Calendar</a>
    
		<div class="dropdown_1column">
        
                <div class="col_1">
                
                    <ul class="simple">
                        <li><a href="/blocks/classroom/currentcalender.php">Current Month</a></li>
                        <li><a href="/blocks/classroom/monthlycalender.php">Next Month</a></li>
                        <li><a href="/blocks/classroom/mysessions.php">Search & Signup</a></li>
                        <li><a href="/mod/classroom/sessionsUser.php?f=1295">Register Project Training</a></li>
                        <li><a href="/mod/classroom/sessionsRegister.php?f=1284">Register External Training</a></li>
						<li><a href="/blocks/classroom/myiltraining.php">My ILT Sessions</a></li>
						<li><a href="/blocks/classroom/ilttrainer.php">Take attendance</a></li>
                        <li><a href="/mod/resource/view.php?id=587">Help on Signup</a></li>
                    </ul>  
                </div>
                
		</div>
        
	</li>
	<li><a href="#" class="drop">Collaborate</a>
    
		<div class="dropdown_1column">
        
                <div class="col_1">
                
                    <ul class="simple">
                        <li><a href="/mod/forum/view.php?f=2">Discussion Board</a></li>
                        <li><a href="/blog/index.php?userid=<?php echo $USER->id; ?>&courseid=1">MyBlogs</a></li>
                        <li><a href="/notes/index.php?course=1&user=<?php echo $USER->id; ?>">MyNotes</a></li>						
                    </ul>  
                </div>
                
		</div>
        
	</li>
	<li><a href="#" class="drop">CSR</a>
    
		<div class="dropdown_1column">
        
                <div class="col_1">
                
                    <ul class="simple">
                        <li><a href="/mod/csr/csractivity.php" target="_blank">My CSR</a></li>
                        <li><a href="/mod/csr/calendar/view.php" target ="_blank">Calendar</a></li>
                        <li><a href="/mod/csr/sessions_user.php?f=1" target ="_blank">Individual activity</a></li>						
                    </ul>  
                </div>
                
		</div>
        
	</li>


</ul>


</div>
</div>
	</div>

