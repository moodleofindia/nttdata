	<style>
		div.accordion_column { line-height: 1.8; margin-top:20px; margin-bottom:0px; font-size:16px; font-weight:bold; color:#002569; background-color:#e3e9ef; width:auto; border-bottom:1px solid #002569; }
		dl {line-height: 1.5; margin-top:0px; margin-bottom:0px; }
		
		dt { line-height: 1.5; margin-top:0px; margin-bottom:0px; background-color:#ffffff; border:1px solid #ffffff; }
		dt a { color:#002569; font-size:15px; }
		dt:hover { line-height: 1.5; margin-top:0px; margin-bottom:0px; background-color:#e3e9ef; border:1px solid #e3e9ef; }
		dt.invisible:hover {  color:#ffffff;   hcolor:#ffffff;  line-height: 1.5; margin-top:0px; margin-bottom:0px; background-color:#cccccc; border:1px solid #cccccc; }
        dt a.visible:hover  { color:#002569; text-decoration:none; }
		dt a.invisible:hover  { color:#ffffff; background-color:#cccccc; text-decoration:none; }
		
		a.visible { color:#002569; }
		a.invisible { color:#888888; }
		a:focus { outline: none;}
		
		dd { margin-left:17px; border:1px solid #e3e9ef; height:auto; }
		dd a.visible:hover { color:#006ab5; }
		dd a.invisible:hover { color:#ffffff; hcolor:#ffffff; }
		dd a:focus { outline: none;}
		
		.images { display:inline; vertical-align:middle;}
		img.courseicons { width:16px; height:16px; margin-right:7px; }

	</style>

<table width="100%" align="center" cellspacing="15">
	<?php

    //Get IMG-Alt-Strings
    if (empty($strsummary)) {
        $strallowguests = get_string('allowguests');
        $strrequireskey = get_string('requireskey');
        $strsummary = get_string('summary');
    }

    //Get user role assignments
	$isteacher = get_records_sql("SELECT `id`
                                    FROM `{$CFG->prefix}role_assignments`
                                    WHERE userid = " . $USER->id . " AND (roleid = 1 OR roleid = 2 OR roleid = 3 OR roleid = 4 OR roleid = 8)");

    $isadmin = get_records_sql("SELECT `id`
                                    FROM `{$CFG->prefix}role_assignments`
                                    WHERE userid = " . $USER->id . " AND roleid = 1 AND contextid = 1");

	//Get course categories
    if($isteacher){
		$result = get_records_sql("SELECT *
                                    FROM {$CFG->prefix}course_categories
                                    WHERE parent = '0' ORDER BY `{$CFG->prefix}course_categories`.`sortorder` ASC ");
	}else{
		$result = get_records_sql("SELECT *
                                    FROM {$CFG->prefix}course_categories
                                    WHERE parent = '0' AND visible ='1'ORDER BY `{$CFG->prefix}course_categories`.`sortorder` ASC  ");
	}

    //Print all categories for HEADER
	foreach ($result as $course_category) {
        $category_name = $course_category->name;
        $category_id = $course_category->id;
        echo '<tr>';
        echo '  <div class="accordion_column" valign="top">';
        echo '      <img src="' . $CFG->themewww .'/'. current_theme() . '/pix/i/course3.gif">' . $category_name;;
        echo '  </div>';

        if($isteacher){
            $result2 = get_records_sql("SELECT *
                                            FROM {$CFG->prefix}course_categories
                                            WHERE parent = '$category_id' ORDER BY name ASC");
        }else{
            $result2 = get_records_sql("SELECT *
                                            FROM {$CFG->prefix}course_categories
                                            WHERE parent = '$category_id' AND visible = '1' ORDER BY name ASC");
        }

        //Print all subcatetories for expandable subheadings
        foreach ($result2 as $course_category2) {

            $contextcat = get_context_instance(CONTEXT_COURSECAT, $course_category2->id);
            $subcontextcat = get_child_contexts($contextcat);
            
            foreach ($subcontextcat as $temp){
                if (!isset($searchparam)){
                    $searchparam .= "contextid = " . $temp->id;
                }else{
                    $searchparam .= " OR contextid = " . $temp->id;
                }
            }

            $catadmin = has_capability('moodle/category:update', $contextcat);

            $subcontextes = get_records_sql("SELECT *
                                                FROM {$CFG->prefix}role_assignments
                                                WHERE ( " . $searchparam . " ) AND userid = " . $USER->id);
            unset($searchparam);
            
            if (!ensure_context_subobj_present($course_category2->id, CONTEXT_COURSECAT) and ($course_category2->visible or $subcontextes or $isadmin or $catadmin)){
            $category_name2 = $course_category2->name;
            $category_id2 = $course_category2->id;

            //Get visibility of course
            $vis = get_records_sql("SELECT *
                                        FROM {$CFG->prefix}course_categories
                                        WHERE id = $category_id2 AND visible = '1'");
                
            //CSS for invisible
            if ($vis){
                $catcss = 'class="visible"';
            }else{
                $catcss = 'class="invisible"';
            }
            
            echo '    <dl>';

            if ($vis){
                echo '        <dt>';
            }else{
                echo '        <dt class="invisible">';
            }

            if ($catadmin){
                echo '            <img style="cursor:pointer;" onclick="location.href=\'' . $CFG->wwwroot . '/course/category.php?id=' . $category_id2 . '\'" alt="courseedit" hspace="2" width="12" height="13" src="'.$CFG->pixpath.'/i/edit.gif" /><a ' . $catcss . ' href="' . $CFG->wwwroot . '/course/category.php?id=' . $category_id2 . '">' . $category_name2 . '</a>';
            }else{
                echo '            <a ' . $catcss . ' href="' . $CFG->wwwroot . '/course/category.php?id=' . $category_id2 . '"><img alt="spacer" hspace="2" width="12" height="13" src="'.$CFG->pixpath.'/spacer.gif" />' . $category_name2 . '</a>';
            }

            echo '        </dt>';

            if($isteacher){
                $result3 = get_records_sql("SELECT *
                                                FROM {$CFG->prefix}course
                                                WHERE category = '$category_id2' ORDER BY fullname ASC");
            }else{
                $result3 = get_records_sql("SELECT *
                                                FROM {$CFG->prefix}course
                                                WHERE category = '$category_id2' AND visible = '1' ORDER BY fullname ASC");
            }

            if($result3){
                //Print all courses off subcategories
                foreach ($result3 as $course_category3) {
                    $context = get_context_instance(CONTEXT_COURSE, $course_category3->id);
                    if(has_capability('moodle/course:manageactivities', $context) or $course_category3->visible == 1){
                        $course_name = $course_category3->fullname;
                        $course_id = $course_category3->id;

                        if(!isset($ddset)){
                            echo '        <dd>';
                            $ddset = 1;
                        }

                        //Get visibility of course
                        $vis = get_records_sql("SELECT *
                                                    FROM {$CFG->prefix}course
                                                    WHERE id = $course_id AND visible = '1'");

                        //CSS for invisible
                        if ($vis){
                            $catcss = 'class="visible"';
                        }else{
                            $catcss = 'class="invisible"';
                        }

                        //Get Images
                        echo '<div class="images">';
                        if ($course_category3->summary) {
                            link_to_popup_window ('/course/info.php?id='.$course_category3->id, 'courseinfo',
                                              '<img class="courseicons" alt="'.$strsummary.'" src="'.$CFG->pixpath.'/i/info.gif" />',
                                               400, 500, $strsummary);
                        } else {
                            echo '<img class="courseicons" alt="" src="'.$CFG->pixpath.'/i/noinfo.gif" />';
                        }
                        if ($course_category3->password) {
                            echo '<a title="'.$strrequireskey.'" href="'.$CFG->wwwroot.'/course/view.php?id='.$course_category3->id.'">';
                            echo '<img class="courseicons" alt="'.$strrequireskey.'" src="'.$CFG->pixpath.'/i/key.gif" /></a>';
                        }else {
                            echo '<img class="courseicons" alt="" src="'.$CFG->pixpath.'/i/nokey.gif" />';
                        }
                        if ($course_category3->guest ) {
                            echo '<a title="'.$strallowguests.'" href="'.$CFG->wwwroot.'/course/view.php?id='.$course_category3->id.'">';
                            echo '<img class="courseicons" alt="'.$strallowguests.'" src="'.$CFG->pixpath.'/i/guest.gif" /></a>';
                        }else {
                            echo '<img class="courseicons" alt="" src="'.$CFG->pixpath.'/i/noguest.gif" />';
                        }
                        echo '</div>';

                        //Course
                        echo '<a ' . $catcss . ' href="' . $CFG->wwwroot . '/course/view.php?id=' . $course_id . '">' . $course_name . '</a>';
                        echo '<br />';

                    }
                }
                //Print any remaining sub-categories and highlist to indicate a category rather than course
                if($isteacher){
                    $result4 = get_records_sql("SELECT *
                                                    FROM {$CFG->prefix}course_categories
                                                    WHERE parent = '$category_id2' ORDER BY name ASC");
                }else{
                    $result4 = get_records_sql("SELECT *
                                                    FROM {$CFG->prefix}course_categories
                                                    WHERE parent = '$category_id2' AND visible = '1' ORDER BY name ASC");
                }
                foreach ($result4 as $course_category4) {
                    $category_name4 = $course_category4->name;
                    $category_id4 = $course_category4->id;
                    $vis = get_records_sql("SELECT *
                                                FROM {$CFG->prefix}course
                                                WHERE id = $course_id AND visible = '1'");
                    ?>
                    <li class="course">
                        <a style="text-indent:25px" href="<?php print $CFG->wwwroot; ?>/course/category.php?id=<?php print $category_id4; ?>"><?php print $category_name4; ?></a>
                    </li>
                    <?php
                }

                if(isset($ddset)){
                  echo "</dd>";  
                }
                unset($ddset);
                
            }
            echo "</dl>";
          }
        }
        echo "</tr>";
    }
	?>
</table>
<br />
