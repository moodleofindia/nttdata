<head>
    <link rel="stylesheet" href="https://lmsqa.portal.nttdatainc.com/theme/aardvark-svc/treecode/jquery.treeview.css" />
    <!--<script src="https://lmsqa.portal.nttdatainc.com/theme/aardvark-svc/treecode/jquery.min.js"></script>-->
	<script src="https://lmsqa.portal.nttdatainc.com/theme/aardvark-svc/treecode/jquery.treeview.js"></script>
    <script type="text/javascript" src="https://lmsqa.portal.nttdatainc.com/theme/aardvark-svc/treecode/demo.js"></script>
</head>
<div id="dropdown" class="yuimenubar yuimenubarnav">
<div>
<div>
    <ul id="menu">
    
    <li><a href="/index.php" class="drop">Home</a><!-- Begin Home Item -->
    
        
    
    </li><!-- End Home Item -->

	 <li><a href="#" class="drop">Course Catalogue</a><!-- Begin 4 columns Item -->
    
        <div class="dropdown_4columns"><!-- Begin 4 columns container -->

   
            
            <div class="col_1">
            
            <h3>Internal Offerings</h3>
			<ul id="treemenu1" ><!-- deleted ** class="treeview" -->
			<li  ><a href="/course/catalogue.php?id=1000004">Technical</a></li>
			<li  ><a href="/mod/resource/view.php?id=12897">Project Management - Global Delivery</a></li>
			<li  ><a href="/mod/resource/view.php?id=17692">Project Management - Enterprise Services</a></li>
			<li  ><a href="/mod/resource/view.php?id=19552">Project Synergy</a></li>
			<li  ><a href="/mod/resource/view.php?id=18133">Navigator</a></li>
			<li  ><a href="/course/catalogue.php?id=24">Behavioral</a></li>
			<li  ><a href="/course/catalogue.php?id=26">Process</a></li>
			<li  ><a href="/course/catalogue.php?id=110">Compliance</a></li>
			
	</ul>
	       


            </div>
        <div class="col_1">
            
                <h3>External Offerings</h3>
                <ul>
                    <li><a href="/course/skillsoftcatalogue.php?id=49">Skill Soft</a></li>
                   
                </ul>   
                 
            </div>
			 </div>
			</li>
			
	 <li><a href="#" class="drop">Programs</a><!-- Begin 4 columns Item -->
    
        <div class="dropdown_5columns"><!-- Begin 4 columns container -->
         <div class="col_3">

                <img src="/homefiles/lead.gif" width="70" height="70" class="img_left imgshadow" alt="" />
                <p><a href="#"></a><a href="/course/programcatalogue.php?id=114">A variety of development programs leveraging the best of training resources and on the job experience. Click here to view.</a></p>
    
                
            </div>

        </div><!-- End 4 columns container -->
    
    </li><!-- End 4 columns Item -->
    <li ><a href="#" class="drop">NLCI Certifications </a><!-- Begin 5 columns Item -->
    
        <div class="dropdown_5columns"><!-- Begin 5 columns container -->
       
            
        
            <div class="col_5">
                <h3>List of available NLCI Certifications
				<?php if(get_records_sql("SELECT id,userid,roleid FROM mdl_role_assignments  where roleid in('1')  and contextid=1 and userid='$USER->id'"))
		{ ?>
				<a href="../../../menu/menu_dom.php" style="margin-left: 253px;margin-top: -13px;"><img src="/theme/aardvark-svc/images/menu/images.png"></a>
				<?php } ?></h3>
				
            </div>
           <br/>
		  
	<!-- starting of jquery tree view -->
	<div class="col_6">
	<h3>India</h3>
             
 <?php 
	 $qry="SELECT Id,parent_id,name,link FROM mdl_treeview_item_dom where status='1'";
     $result=mysql_query($qry);


     $arrayCategories = array();

 while($row = mysql_fetch_assoc($result)){ 
 $arrayCategories[$row['Id']] = array("parent_id" => $row['parent_id'], "name" => $row['name'], "link" => $row['link']);   
  }
//createTree($arrayCategories, 0);

 function createTree($array, $currentParent, $currLevel = 0, $prevLevel = -1) {

foreach ($array as $categoryId => $category) {

if ($currentParent == $category['parent_id']) {                       

    if ($currLevel > $prevLevel) echo " <ul id='navigation' style='margin-left: 12px;'> "; 

    if ($currLevel == $prevLevel) echo " </li> ";

    echo '<li> <label>';
	if($category['link']!=""){
	echo '<img src="/theme/aardvark-svc/images/menu/arrow.gif" style="margin-left: -6px;"><a href="'.$category['link'].'" style="margin-top: -21px;margin-left: 3px;line-height: 15px;">';
	} else{
	echo '<a href="javascript:void(0)" style="margin-left: 5px;margin-top: 2px;line-height: 15px;">';
	}
	
	echo $category['name'].'</a></label>';

    if ($currLevel > $prevLevel) { $prevLevel = $currLevel; }

    $currLevel++; 

    createTree ($array, $categoryId, $currLevel, $prevLevel);

    $currLevel--;               
    }   

}

if ($currLevel == $prevLevel) echo " </li>  </ul> ";

}   
?>
<div style="margin-left:5px;width=190px;">
<?php
 if(mysql_num_rows($result)!=0)
 {
?>
<?php 

createTree($arrayCategories, 0); ?>
<?php } ?>
</div>
</div> 

<!-- end of jquey tree view -->
            <div class="col_6">
            
                <h3>Rest of the World</h3>
<?php 
	 $qry="SELECT Id,parent_id,name,link FROM mdl_treeview_item_intl where status='1'";
     $result=mysql_query($qry);
     $arrayCategoriess = array();

 while($row = mysql_fetch_assoc($result)){ 
 $arrayCategoriess[$row['Id']] = array("parent_id" => $row['parent_id'], "name" => $row['name'], "link" => $row['link']);   
  }
//createTree($arrayCategories, 0);

 function createTrees($array, $currentParent, $currLevel = 0, $prevLevel = -1) {

foreach ($array as $categoryId => $category) {

if ($currentParent == $category['parent_id']) {                       

    if ($currLevel > $prevLevel) echo " <ul id='navigations' style='margin-left: 12px;'> "; 

    if ($currLevel == $prevLevel) echo " </li> ";

    echo '<li> <label>';
	if($category['link']!=""){
	echo '<img src="/theme/aardvark-svc/images/menu/arrow.gif"><a href="'.$category['link'].'" style="margin-top: -21px;margin-left: 10px;">';
	} else{
	echo '<a href="javascript:void(0)" style="margin-left: 5px;margin-top: 2px;line-height: 15px;">';
	}
	
	echo $category['name'].'</a></label>';

    if ($currLevel > $prevLevel) { $prevLevel = $currLevel; }

    $currLevel++; 

    createTree ($array, $categoryId, $currLevel, $prevLevel);

    $currLevel--;               
    }   

}

if ($currLevel == $prevLevel) echo " </li>  </ul> ";

}   
?>
<div style="margin-left:2px">
<?php
 if(mysql_num_rows($result)!=0)
 {
?>
<?php 

createTrees($arrayCategoriess, 0); ?>
<?php } ?>
</div>
<br/>

        </div><!-- End 5 columns container -->
</div>
    </li><!-- End 5 columns Item -->


	<li><a href="#" class="drop">Calendar</a>
    
		<div class="dropdown_1column">
        
                <div class="col_1">
                
                    <ul class="simple">
                        <li><a href="/blocks/classroom/currentcalender.php">Current Month</a></li>
                        <li><a href="/blocks/classroom/monthlycalender.php">Next Month</a></li>
                        <li><a href="/blocks/classroom/mysessions.php">Search & Signup</a></li>
                        <li><a href="/mod/classroom/sessionsUser.php?f=1295">Register Project Training</a></li>
                        <li><a href="/mod/classroom/sessionsRegister.php?f=1284">Register External Training</a></li>
						<li><a href="/blocks/classroom/myiltraining.php">My ILT Sessions</a></li>
						<li><a href="/blocks/classroom/ilttrainer.php">Take attendance</a></li>
                        <li><a href="/mod/resource/view.php?id=587">Help on Signup</a></li>
                    </ul>  
                </div>
                
		</div>
        
	</li>
	<li><a href="#" class="drop">Collaborate</a>
    
		<div class="dropdown_1column">
        
                <div class="col_1">
                
                    <ul class="simple">
                        <li><a href="/mod/forum/view.php?f=2">Discussion Board</a></li>
                        <li><a href="/blog/index.php?userid=<?php echo $USER->id; ?>&courseid=1">MyBlogs</a></li>
                        <li><a href="/notes/index.php?course=1&user=<?php echo $USER->id; ?>">MyNotes</a></li>						
                    </ul>  
                </div>
                
		</div>
        
	</li>
	<li><a href="#" class="drop">CSR</a>
    
		<div class="dropdown_1column">
        
                <div class="col_1">
                
                    <ul class="simple">
                        <li><a href="/mod/csr/csractivity.php" target="_blank">My CSR</a></li>
                        <li><a href="/mod/csr/calendar/view.php" target ="_blank">Calendar</a></li>
                        <li><a href="/mod/csr/sessions_user.php?f=1" target ="_blank">Individual activity</a></li>						
                    </ul>  
                </div>
                
		</div>
        
	</li>


</ul>


</div>
</div>
	</div>

