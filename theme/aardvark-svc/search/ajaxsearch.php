<head>
<script type='text/javascript' src="<?php echo $CFG->themewww .'/'. current_theme() ?>/search/jquery.autocomplete.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $CFG->themewww .'/'. current_theme() ?>/search/jquery.autocomplete.css" />

<script type="text/javascript">
$().ready(function() {
	
	$("#course").autocomplete("<?php echo $CFG->themewww .'/'. current_theme() ?>/search/get_course_autocomplete.php", {
		width: 260,
		matchContains: true,
		mustMatch: true,
		//minChars: 0,
		//multiple: true,
		//highlight: false,
		//multipleSeparator: ",",
		selectFirst: false
	});
	
	$("#course").result(function(event, data, formatted) {
		$("#search").val(data[0]);
	});
});
</script>
</head>
<div>
	<form autocomplete="off" action="<?php echo $CFG->wwwroot?>/course/search.php">

			<input class="searchbox"  size=33 type="text" value="Search courses ..." name="search" id="course" 
			onblur="if(this.value=='')this.value='Search courses ...';" 
onfocus="if(this.value=='Search courses ...')this.value='';"/>		
<input type="image" src="<?php echo $CFG->themewww .'/'. current_theme() ?>/images/default/goblue.png" alt="Go" />

	</form>
</div>

