
 
  <link rel="stylesheet" href="<?php echo $CFG->themewww .'/'. current_theme() ?>/profilemenustyle.css" type="text/css" media="screen"/>



<div class="profilepic" id="profilepic">
        <?PHP

echo '<a href="'.$CFG->wwwroot.'/user/view.php?id='.$USER->id.'&amp;course='.$COURSE->id.'"><img src="'.$CFG->wwwroot.'/user/pix.php?file=/'.$USER->id.'/f1.jpg" width="80px" height="80px" title="'.$USER->firstname.' '.$USER->lastname.'" alt="'.$USER->firstname.' '.$USER->lastname.'" /></a>'; 

?>
      </div>

<div class="profilename" id="profilename">
    <?PHP
	
	    function get_content () {
        global $USER, $CFG, $SESSION, $COURSE;
        $wwwroot = '';
        $signup = '';}

        if (empty($CFG->loginhttps)) {
            $wwwroot = $CFG->wwwroot;
        } else {
            $wwwroot = str_replace("http://", "https://", $CFG->wwwroot);
        }
        
	
if (!isloggedin() or isguestuser()) {
echo '<a href="'.$CFG->wwwroot.'/login/loginLMS.php">'.get_string('loggedinnot').'</a>';
} 
else {
if($USER->city=="MISI" || $USER->city=="DSKLS")
{
echo '<br/>';
}
else if($USER->employee_type="Employee" && $USER->grade>0 && $USER->grade<16)
{
echo '<a id="go" rel="leanModal" name="signup" href="#signup" title="Know your mandatory training hours"><img src="'.$CFG->wwwroot.'/theme/aardvark-svc/images/infoBaloon2.png" height="33" width="37"></a>';
}
else
{
echo '<br/>';
}
echo '<a href="'.$CFG->wwwroot.'/user/view.php?id='.$USER->id.'&amp;course='.$COURSE->id.'">'.$USER->firstname.' '.$USER->lastname.'</a>';
}		


?>
    </div>
    
    
 
      
<div class="profileoptions" id="profileoptions">
    
    
   
 <?PHP
				
if (!isloggedin() or isguestuser()) {
echo '<ul>';
echo '<li><a href="'.$CFG->wwwroot.'/login/loginLMS.php">'.get_string('login').'</a></li>';
echo '</ul>';



} else {

?>
<table align="right"   cellspacing=0 cellpadding=0>
 <tr style='height:31.0pt'>
  <td valign=top >


<?php
$using_ie7 = (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 7.') !== FALSE);
$managerportal=get_record_sql("select * from mdl_user where manager_portalid=$USER->username");

if($using_ie7)
{
if($managerportal){
echo '<p>
<a title="Update your time zone, picture and more" href="'.$CFG->wwwroot.'/user/edit.php?id='.$USER->id.'&amp;course='.$COURSE->id.'">Update Profile</a>
&nbsp;<a title="List of Trainings you have taken" href="'.$CFG->wwwroot.'/course/report/index.php?option=1">My Trainings</a>
<a title="Your Team training report" href="'.$CFG->wwwroot.'/course/report/teamtraining.php?option=1">Team Trainings</a>
&nbsp;<a title="Your badges and points" href="'.$CFG->wwwroot.'/course/report/badge.php?id='.$COURSE->id.'">My Badges</a>
&nbsp;<a title="Link to how to and help videos" href="'.$CFG->wwwroot.'/course/view.php?id=7007">Help Videos</a>
&nbsp;<a title="Frequently Asked Questions" href="'.$CFG->wwwroot.'/LMSfaq.html">FAQ</a>
&nbsp;<a title="Contact us with your queries and issues" href="'.$CFG->wwwroot.'/contactus/contact.php">Contact Us</a>
&nbsp;<a  href="'.$CFG->wwwroot.'/login/logout.php?sesskey='.sesskey().'">Logout</a>
</p>';
}else{

echo '<p>
<a title="Update your time zone, picture and more" href="'.$CFG->wwwroot.'/user/edit.php?id='.$USER->id.'&amp;course='.$COURSE->id.'">Update Profile</a>
&nbsp;<a title="List of Trainings you have taken" href="'.$CFG->wwwroot.'/course/report/index.php?option=1">My Trainings</a>

&nbsp;<a title="Your badges and points" href="'.$CFG->wwwroot.'/course/report/badge.php?id='.$COURSE->id.'">My Badges</a>
&nbsp;<a title="Link to how to and help videos" href="'.$CFG->wwwroot.'/course/view.php?id=7007">Help Videos</a>
&nbsp;<a title="Frequently Asked Questions" href="'.$CFG->wwwroot.'/LMSfaq.html">FAQ</a>
&nbsp;<a title="Contact us with your queries and issues" href="'.$CFG->wwwroot.'/contactus/contact.php">Contact Us</a>
&nbsp;<a  href="'.$CFG->wwwroot.'/login/logout.php?sesskey='.sesskey().'">Logout</a>
</p>';
}

}
{
if($managerportal){
echo '<div class="menu">
            <div class="itemm">
                <a class="linkm icon_mailm"></a>
                <div class="item_contentm">

                    <p>
                        <a title="Contact us with your queries and issues" href="'.$CFG->wwwroot.'/contactus/contact.php">Send us your Query/Issue</a>
                       

                    </p>
                </div>
            </div>
            <div class="itemm">
                <a class="linkm icon_helpm"></a>
                <div class="item_contentm">
   
                    <p>
						<a title="Link to how to and help videos" href="'.$CFG->wwwroot.'/course/view.php?id=7007">Help Videos</a>
                        <a title="Frequently Asked Questions" href="'.$CFG->wwwroot.'/LMSfaq.html">FAQ</a>
                        

                    </p>
                </div>
            </div>
            <div class="itemm">
                <a class="linkm icon_photosm"></a>
                <div class="item_contentm">

                    <p>
                        <a title="List of Trainings you have taken" href="'.$CFG->wwwroot.'/course/report/index.php?option=1">My Trainings</a>						
						 <a title="Your Team training report" href="'.$CFG->wwwroot.'/course/report/teamtraining.php?option=1">Team Trainings</a>
                        <a title="Your badges and points" href="'.$CFG->wwwroot.'/course/report/badge.php?id='.$COURSE->id.'">My Badges</a>

                    </p>
                </div>
            </div>
            <div class="itemm">
                <a class="linkm icon_homem"></a>
                <div class="item_contentm">
                    <p>
                        <a href="'.$CFG->wwwroot.'">Home</a>
						  <a title="Update your time zone, picture and more" href="'.$CFG->wwwroot.'/user/edit.php?id='.$USER->id.'&amp;course='.$COURSE->id.'">Update Profile</a>
                        <a href="'.$CFG->wwwroot.'/login/logout.php?sesskey='.sesskey().'">Logout</a>
                       
                    </p>
                </div>
            </div>
        </div>';


}

else{
echo '<div class="menu">
            <div class="itemm">
                <a class="linkm icon_mailm"></a>
                <div class="item_contentm">

                    <p>
                        <a title="Contact us with your queries and issues" href="'.$CFG->wwwroot.'/contactus/contact.php">Send us your Query/Issue</a>
                       

                    </p>
                </div>
            </div>
            <div class="itemm">
                <a class="linkm icon_helpm"></a>
                <div class="item_contentm">
   
                    <p>
						<a title="Link to how to and help videos" href="'.$CFG->wwwroot.'/course/view.php?id=7007">Help Videos</a>
                        <a title="Frequently Asked Questions" href="'.$CFG->wwwroot.'/LMSfaq.html">FAQ</a>
                        

                    </p>
                </div>
            </div>
            <div class="itemm">
                <a class="linkm icon_photosm"></a>
                <div class="item_contentm">

                    <p>
                        <a title="List of Trainings you have taken" href="'.$CFG->wwwroot.'/course/report/index.php?option=1">My Trainings</a>
						
						

                        <a title="Your badges and points" href="'.$CFG->wwwroot.'/course/report/badge.php?id='.$COURSE->id.'">My Badges</a>

                    </p>
                </div>
            </div>
            <div class="itemm">
                <a class="linkm icon_homem"></a>
                <div class="item_contentm">
                    <p>
                        <a href="'.$CFG->wwwroot.'">Home</a>
						  <a title="Update your time zone, picture and more" href="'.$CFG->wwwroot.'/user/edit.php?id='.$USER->id.'&amp;course='.$COURSE->id.'">Update Profile</a>
                        <a href="'.$CFG->wwwroot.'/login/logout.php?sesskey='.sesskey().'">Logout</a>
                       
                    </p>
                </div>
            </div>
        </div>';



}
}
}

?>
  
	<!-- Profile Menu -->

     
        <script>
            $('.itemm').hover(
                function(){
                    var $this = $(this);
                    expandPM($this);
                },
                function(){
                    var $this = $(this);
                    collapsePM($this);
                }
            );
            function expandPM($elem){
                var angle = 0;
                var t = setInterval(function () {
                    if(angle == 1440){
                        clearInterval(t);
                        return;
                    }
                    angle += 40;
                    $('.linkm',$elem).stop().animate({rotate: '+=-40deg'}, 0);
                },10);
                $elem.stop().animate({width:'210px'}, 1000)
                .find('.item_contentm').fadeIn(400,function(){
                    $(this).find('p').stop(true,true).fadeIn(600);
                });
            }
            function collapsePM($elem){
                var angle = 1440;
                var t = setInterval(function () {
                    if(angle == 0){
                        clearInterval(t);
                        return;
                    }
                    angle -= 40;
                    $('.linkm',$elem).stop().animate({rotate: '+=40deg'}, 0);
                },10);
                $elem.stop().animate({width:'52px'}, 1000)
                .find('.item_contentm').stop(true,true).fadeOut().find('p').stop(true,true).fadeOut();
            }
        </script>

    </div>
	  </td>
 </tr>
 
 <tr style='height:31.9pt'>
  <td  valign=top >

<?php
include("search\ajaxsearch.php");
?>


 
  </td>
 </tr>
</table>


