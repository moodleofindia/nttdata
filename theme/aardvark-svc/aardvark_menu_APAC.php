<link href="/theme/aardvark-svc/tree.css" rel="stylesheet" type="text/css" />
<script src="/theme/aardvark-svc/tree.js" type="text/javascript"></script>

<div id="dropdown" class="yuimenubar yuimenubarnav">
<div>
<div>
    <ul id="menu">
    
    <li><a href="#" class="drop">Home</a><!-- Begin Home Item -->
    
        <div class="dropdown_2columns"><!-- Begin 2 columns container -->
    
            <div class="col_2">
                <h3>Welcome !</h3>
            </div>
    
            <div class="col_2">
                <p style="font-size:10pt">Welcome to NTT Data LMS.</p>             
                <p style="font-size:9pt">Training and performance support in a virtual environment for NTT Data employees. Access to GEMs, and Skillport are included in the virtual offerings.</p>             
            </div>
    
            <div class="col_2">
                <h3>Select a Home page</h3>
            </div>
          
            <div class="col_1">
			<ul>
			 <table cellspacing="10">
			 <tr>
			 <td>
            <img src="/homefiles/lms.JPG" width="57" height="47" alt="" /></td>
			<td><a href="/index.php">
			<li> LMS Home Page</li></a></td>
			</tr>
			<tr>
            <td><img src="/homefiles/c.JPG" width="57" height="47" alt="" /></td>
			<td><p><a href="/KLCI_Certifications.php"><li> NLCI</li></a></p></td>
			</tr>
			<tr>
            <td><img src="/homefiles/g.JPG" width="57" height="47" alt="" /></td>
			<td><p><a href="/course/category.php?id=110"><li> GEMs
</li></a></p></td>
			</tr>
			<tr>
            <td><img src="/homefiles/s.JPG" width="57" height="47" alt="" /></td>
			<td><p><a href="/course/category.php?id=49"><li> SkillSoft</li></a></p></td>
			</tr>
			 </table>
			</ul>
            </div>
            
            
          
        </div><!-- End 2 columns container -->
    
    </li><!-- End Home Item -->

	 <li><a href="#" class="drop">Course Catalogue</a><!-- Begin 4 columns Item -->
    
        <div class="dropdown_4columns"><!-- Begin 4 columns container -->

   
            
            <div class="col_1">
            
            <h3>Internal Offerings</h3>
			<ul id="treemenu1" ><!-- deleted ** class="treeview" -->
			<li  ><a href="/course/catalogue.php?id=1000004">Technical</a></li>
			<li  ><a href="/course/catalogue.php?id=13">Project Management - Global Delivery</a></li>
			<li  ><a href="/mod/resource/view.php?id=17692">Project Management - Enterprise Services</a></li>
			<li  ><a href="/mod/resource/view.php?id=18133">Project Synergy/Navigator</a></li>
			<li  ><a href="/course/catalogue.php?id=24">Behavioral</a></li>
			<li  ><a href="/course/catalogue.php?id=26">Process</a></li>
			<li  ><a href="/course/catalogue.php?id=110">Compliance</a></li>
			<li  ><a href="/course/catalogue.php?id=1001144">Legacy Intelli Courses</a></li>
	</ul>
	       


            </div>
        <div class="col_1">
            
                <h3>External Offerings</h3>
                <ul>
                    <li><a href="/course/skillsoftcatalogue.php?id=49">Skill Soft</a></li>
                   
                </ul>   
                 
            </div>
			 </div>
			</li>
			
	 <li><a href="#" class="drop">Programs</a><!-- Begin 4 columns Item -->
    
        <div class="dropdown_5columns"><!-- Begin 4 columns container -->
         <div class="col_3">

                <img src="/homefiles/lead.gif" width="70" height="70" class="img_left imgshadow" alt="" />
                <p><a href="#"></a><a href="/course/programcatalogue.php?id=114">A variety of development programs leveraging the best of training resources and on the job experience. Click here to view.</a></p>
    
                
            </div>

        </div><!-- End 4 columns container -->
    
    </li><!-- End 4 columns Item -->
    <li ><a href="#" class="drop">APAC Programs</a><!-- Begin 5 columns Item -->
    
        <div class="dropdown_5columns"><!-- Begin 5 columns container -->
       
            
        
            <div class="col_5">
                <h3>List of available APAC Programs</h3>
            </div>
           <br/>
		  
	<!-- starting of jquery tree view -->
	<div class="col_6">
	
           <script language="javascript">
      <!-- addExpandCollapseAll(); -->
        startParentNode("Code of conduct");
        		 
					addNode("Global code of business conduct","/course/view.php?id=6057","_parent");
		 
		endParentNode();
          
 </script>
	
				
    </div>    
		
	 </div>

<!-- end of jquey tree view -->
            
	<li><a href="#" class="drop">Calendar</a>
    
		<div class="dropdown_1column">
        
                <div class="col_1">
                
                    <ul class="simple">
                        <li><a href="/blocks/classroom/currentcalender.php">Current Month</a></li>
                        <li><a href="/blocks/classroom/monthlycalender.php">Next Month</a></li>
                        <li><a href="/blocks/classroom/mysessions.php">Search & Signup</a></li>
						<li><a href="/blocks/classroom/myiltraining.php">My ILT Sessions</a></li>
						<li><a href="/blocks/classroom/ilttrainer.php">Take attendance</a></li>
                        <li><a href="/mod/resource/view.php?id=587">Help on Signup</a></li>
                    </ul>  
                </div>
                
		</div>
        
	</li>
	<li><a href="#" class="drop">Collaborate</a>
    
		<div class="dropdown_1column">
        
                <div class="col_1">
                
                    <ul class="simple">
                        <li><a href="/mod/forum/view.php?f=2">Discussion Board</a></li>
                        <li><a href="/blog/index.php?userid=<?php echo $USER->id; ?>&courseid=1">MyBlogs</a></li>
                        <li><a href="/notes/index.php?course=1&user=<?php echo $USER->id; ?>">MyNotes</a></li>						
                    </ul>  
                </div>
                
		</div>
        
	</li>
	<li><a href="#" class="drop">CSR</a>
    
		<div class="dropdown_1column">
        
                <div class="col_1">
                
                    <ul class="simple">
                        <li><a href="/mod/csr/csractivity.php" target="_blank">My CSR</a></li>
                        <li><a href="/mod/csr/calendar/view.php" target ="_blank">Calendar</a></li>
                        <li><a href="/mod/csr/sessions_user.php?f=1" target ="_blank">Individual activity</a></li>						
                    </ul>  
                </div>
                
		</div>
        
	</li>


</ul>


</div>
</div>
	</div>

