<!DOCTYPE html>
<html>
	<head>

		<title>jQuery Impromptu with PureCSS</title>


		
		<link rel="stylesheet" media="all" type="text/css" href="../themes/base.css" />		
		
		
		<script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="../jquery-impromptu.js"></script>

		<script type="text/javascript">
			function openprompt(){
			
				$.prompt("Proceeding may be <?php echo 'test..'; ?> good for your site..", {
			title: "Are you Ready?",
			buttons: { "OK": true  }
			
			}
			
				);
			}

		</script>
	</head>

	<body>
	<?php
	echo '<SCRIPT type="text/javascript">openprompt();</SCRIPT>';
?>
	</body>
</html>