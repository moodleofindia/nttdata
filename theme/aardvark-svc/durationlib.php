<?php

	require_once '../../config.php';
    require_once($CFG->libdir.'/gradelib.php');


    function getduration($userid) {
        global $CFG;
		$total=0;
		$scorm = get_record_sql("SELECT sum(sc.duration) as 'Duration'
		FROM mdl_scorm_scoes_track AS st
		JOIN mdl_scorm_scoes ssc on ssc.id=st.scoid and ssc.scormtype='sco'
		JOIN mdl_scorm AS sc ON sc.id=st.scormid
		JOIN mdl_course AS c ON c.id=sc.course
		JOIN mdl_user AS u ON st.userid=u.id and u.id=$userid
		where st.element='cmi.core.lesson_status'
		and st.value in ('completed','passed')");
		$total=$scorm->Duration;
		
		
		$resource = get_record_sql("SELECT sum(r.duration) as 'Duration'
		FROM mdl_grade_grades g join mdl_grade_items i
		on  g.itemid=i.id
		and g.rawgrade=g.rawgrademax
		and i.itemmodule in ('resource')
		join mdl_resource r on r.id=i.iteminstance
		JOIN mdl_course AS c ON c.id=r.course
		join mdl_user u on u.id=g.userid and u.id=$userid");
		$total+=$resource->Duration;
		
		
		$mplayer = get_record_sql("SELECT sum(m.duration) as 'Duration'
		FROM mdl_grade_grades join mdl_grade_items
		on  mdl_grade_grades.itemid=mdl_grade_items.id
		and mdl_grade_grades.rawgrade=mdl_grade_grades.rawgrademax
		and mdl_grade_items.itemmodule='mplayer'
		join mdl_mplayer m on m.id=mdl_grade_items.iteminstance
		join mdl_user u on u.id=mdl_grade_grades.userid and u.id=$userid
		JOIN mdl_course AS c ON c.id=m.course");
		$total+=$mplayer->Duration;
		
		
		$ilt = get_record_sql("select sum(duration) as 'Duration' from mdl_classroom_sessions where
		mdl_classroom_sessions.id in(SELECT distinct(s.id)
		from mdl_classroom_sessions s
		join mdl_classroom_sessions_dates on s.id=mdl_classroom_sessions_dates.sessionid
		and s.datetimeknown=1 and s.status='Completed'
		join mdl_classroom on mdl_classroom.id=s.classroom
		join mdl_classroom_submissions on mdl_classroom_submissions.sessionid = s.id
		and mdl_classroom_submissions.attend=1
		join mdl_course c on c.id=mdl_classroom.course
		join mdl_user u on u.id=mdl_classroom_submissions.userid and u.id=$userid)");
		$total+=$ilt->Duration;
		
		
		$skillsoft = get_record_sql("SELECT sum(s.duration) as 'Duration'
		FROM mdl_grade_grades join mdl_grade_items
		on  mdl_grade_grades.itemid=mdl_grade_items.id
		and mdl_grade_grades.rawgrade>=mdl_grade_items.grademax
		and mdl_grade_items.itemmodule='skillsoft'
		join mdl_course on mdl_course.id=mdl_grade_items.courseid
		join mdl_user u on u.id=mdl_grade_grades.userid and u.id=$userid
		join mdl_skillsoft s on s.course=mdl_course.id");
		$total+=$skillsoft->Duration;
		
		
		$quiz = get_record_sql("SELECT sum(q.timelimit) as 'Duration'
		FROM mdl_grade_grades g
		INNER JOIN mdl_user u ON g.userid = u.id and u.id=$userid
		INNER JOIN mdl_grade_items i
		ON g.itemid = i.id
		INNER JOIN mdl_course c on c.id = i.courseid
		inner join mdl_quiz q on q.course=c.id
		WHERE (i.itemname IS NOT NULL)AND (i.itemtype = 'mod')AND
		(i.itemmodule = 'quiz')
		AND (g.finalgrade >= 0)");
		$total+=$quiz->Duration;
		
		
		$assignment = get_record_sql("SELECT sum(a.duration) as 'Duration'
		FROM mdl_assignment_submissions s join mdl_assignment a
		on s.assignment=a.id and s.timemodified >0 join mdl_course c
		on c.id=a.course join mdl_user u
		on u.id=s.userid and u.id=$userid");
		$total+=$assignment->Duration;
				
        return sprintf("%.0f",$total/60);
    }
	
	
	 function getdurationDate($userid,$starttime,$endtime) {
        global $CFG;
		$total=0;
		$scorm = get_record_sql("SELECT sum(sc.duration) as 'Duration'
		FROM mdl_scorm_scoes_track AS st
		JOIN mdl_scorm_scoes ssc on ssc.id=st.scoid and ssc.scormtype='sco'
		JOIN mdl_scorm AS sc ON sc.id=st.scormid
		JOIN mdl_course AS c ON c.id=sc.course
		JOIN mdl_user AS u ON st.userid=u.id and u.id=$userid 
		and st.timemodified > unix_timestamp('$starttime')
		and st.timemodified < unix_timestamp('$endtime')
		where st.element='cmi.core.lesson_status'
		and st.value in ('completed','passed')");
		$total=$scorm->Duration;
		
		
		$resource = get_record_sql("SELECT sum(r.duration) as 'Duration'
		FROM mdl_grade_grades g join mdl_grade_items i
		on  g.itemid=i.id
		and g.rawgrade=g.rawgrademax
		and i.itemmodule in ('resource')
		join mdl_resource r on r.id=i.iteminstance
		JOIN mdl_course AS c ON c.id=r.course
		join mdl_user u on u.id=g.userid and u.id=$userid
		and g.timemodified > unix_timestamp('$starttime')
		and g.timemodified < unix_timestamp('$endtime')");
		$total+=$resource->Duration;
		
		
		$mplayer = get_record_sql("SELECT sum(m.duration) as 'Duration'
		FROM mdl_grade_grades join mdl_grade_items
		on  mdl_grade_grades.itemid=mdl_grade_items.id
		and mdl_grade_grades.rawgrade=mdl_grade_grades.rawgrademax
		and mdl_grade_items.itemmodule='mplayer'
		join mdl_mplayer m on m.id=mdl_grade_items.iteminstance
		join mdl_user u on u.id=mdl_grade_grades.userid and u.id=$userid
		and mdl_grade_grades.timemodified > unix_timestamp('$starttime')
		and mdl_grade_grades.timemodified < unix_timestamp('$endtime')
		JOIN mdl_course AS c ON c.id=m.course");
		$total+=$mplayer->Duration;
				
		
		$ilt = get_record_sql("select sum(duration) as 'Duration' from mdl_classroom_sessions where
		mdl_classroom_sessions.id in(SELECT distinct(s.id)
		from mdl_classroom_sessions s
		join mdl_classroom_sessions_dates on s.id=mdl_classroom_sessions_dates.sessionid
		and s.datetimeknown=1 and s.status='Completed'
		join mdl_classroom on mdl_classroom.id=s.classroom
		join mdl_classroom_submissions on mdl_classroom_submissions.sessionid = s.id
		and mdl_classroom_submissions.attend=1
		join mdl_course c on c.id=mdl_classroom.course
		join mdl_user u on u.id=mdl_classroom_submissions.userid and u.id=$userid
		and mdl_classroom_sessions_dates.timestart > unix_timestamp('$starttime')
		and mdl_classroom_sessions_dates.timefinish < unix_timestamp('$endtime'))");
		$total+=$ilt->Duration;
		
		
		$skillsoft = get_record_sql("SELECT sum(s.duration) as 'Duration'
		FROM mdl_grade_grades join mdl_grade_items
		on  mdl_grade_grades.itemid=mdl_grade_items.id
		and mdl_grade_grades.rawgrade>=mdl_grade_items.grademax
		and mdl_grade_items.itemmodule='skillsoft'
		join mdl_course on mdl_course.id=mdl_grade_items.courseid
		join mdl_user u on u.id=mdl_grade_grades.userid and u.id=$userid
		and mdl_grade_grades.timemodified > unix_timestamp('$starttime')
		and mdl_grade_grades.timemodified < unix_timestamp('$endtime')
		join mdl_skillsoft s on s.course=mdl_course.id");
		$total+=$skillsoft->Duration;
		
		
		$quiz = get_record_sql("SELECT sum(q.timelimit) as 'Duration'
		FROM mdl_grade_grades g
		INNER JOIN mdl_user u ON g.userid = u.id and u.id=$userid
		INNER JOIN mdl_grade_items i
		ON g.itemid = i.id
		INNER JOIN mdl_course c on c.id = i.courseid
		inner join mdl_quiz q on q.course=c.id
		WHERE (i.itemname IS NOT NULL)AND (i.itemtype = 'mod')AND
		(i.itemmodule = 'quiz')
		AND (g.finalgrade >= 0) 
		and g.timemodified > unix_timestamp('$starttime')
		and g.timemodified < unix_timestamp('$endtime')");
		$total+=$quiz->Duration;
		
		
		$assignment = get_record_sql("SELECT sum(a.duration) as 'Duration'
		FROM mdl_assignment_submissions s join mdl_assignment a
		on s.assignment=a.id and s.timemodified >0 join mdl_course c
		on c.id=a.course join mdl_user u
		on u.id=s.userid and u.id=$userid
		and s.timemodified > unix_timestamp('$starttime')
		and s.timemodified < unix_timestamp('$endtime')");
		$total+=$assignment->Duration;
				
        return sprintf("%.0f",$total/60);
    }
	

?>
