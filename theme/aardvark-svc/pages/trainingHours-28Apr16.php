
<?php 
require_once('../../../config.php');
function getdurationDate($userid,$starttime,$endtime) {
        global $CFG;
		$total=0;
		$scorm = get_records_sql("SELECT u.id,st.id,(sc.duration)/60 as 'Duration'
									FROM mdl_scorm_scoes_track AS st
									JOIN mdl_scorm_scoes ssc on ssc.id=st.scoid and ssc.scormtype='sco'
									JOIN mdl_scorm AS sc ON sc.id=st.scormid
									JOIN mdl_course AS c ON c.id=sc.course
									JOIN mdl_user AS u ON st.userid=u.id and u.id=$userid 
									and st.timemodified >= unix_timestamp('$starttime')
									and st.timemodified < unix_timestamp('$endtime')
									where st.element in('cmi.core.lesson_status','cmi.completion_status')
									and st.value in ('completed','passed') group by st.scormid,u.id");
		$scormduration=0;
		if ($scorm) {
        foreach($scorm as $record) {
		
                $scormduration = $scormduration + $record->Duration;
                continue;
			}

		}
		$total= $scormduration;
		
		
		$resource = get_record_sql("SELECT sum(r.duration)/60 as 'Duration'
									FROM mdl_grade_grades g join mdl_grade_items i
									on  g.itemid=i.id
									and g.rawgrade=g.rawgrademax
									and i.itemmodule in ('resource')
									join mdl_resource r on r.id=i.iteminstance
									JOIN mdl_course AS c ON c.id=r.course
									join mdl_user u on u.id=g.userid and u.id=$userid
									and g.timemodified >= unix_timestamp('$starttime')
									and g.timemodified < unix_timestamp('$endtime')");
		$total+=$resource->Duration;
		
		
		$mplayer = get_record_sql("SELECT sum(m.duration)/60 as 'Duration'
									FROM mdl_grade_grades join mdl_grade_items
									on  mdl_grade_grades.itemid=mdl_grade_items.id
									and mdl_grade_grades.rawgrade=mdl_grade_grades.rawgrademax
									and mdl_grade_items.itemmodule='mplayer'
									join mdl_mplayer m on m.id=mdl_grade_items.iteminstance
									join mdl_user u on u.id=mdl_grade_grades.userid and u.id=$userid
									and mdl_grade_grades.timemodified >= unix_timestamp('$starttime')
									and mdl_grade_grades.timemodified < unix_timestamp('$endtime')
									JOIN mdl_course AS c ON c.id=m.course");
		$total+=$mplayer->Duration;
				
		
		$ilt = get_record_sql("select sum(duration)/60 as 'Duration' from mdl_classroom_sessions where
								mdl_classroom_sessions.id in(SELECT distinct(s.id)
								from mdl_classroom_sessions s
								join mdl_classroom_sessions_dates on s.id=mdl_classroom_sessions_dates.sessionid
								and s.datetimeknown=1 and s.status='Completed'
								join mdl_classroom on mdl_classroom.id=s.classroom
								join mdl_classroom_submissions on mdl_classroom_submissions.sessionid = s.id
								and mdl_classroom_submissions.attend=1
								join mdl_course c on c.id=mdl_classroom.course
								join mdl_user u on u.id=mdl_classroom_submissions.userid and u.id=$userid
								and mdl_classroom_sessions_dates.timestart >= unix_timestamp('$starttime')
								and mdl_classroom_sessions_dates.timefinish < unix_timestamp('$endtime'))");
		$total+=$ilt->Duration;
		
		
		$skillsoft = get_records_sql("SELECT sc.id, u.username as PortalID,
										trim(c.fullname) as 'Course',cc.name as 'Category',
										trim(sc.name) as 'Asset',
										sc.duration/60 as 'Duration',
										st.value as 'Grade',
										DATE_FORMAT(FROM_UNIXTIME(at.value),'%b %d %Y') as 'CompletedDate'
										FROM mdl_skillsoft_au_track AS st
										JOIN mdl_skillsoft AS sc ON sc.id=st.skillsoftid
										JOIN mdl_course AS c ON c.id=sc.course
										JOIN mdl_course_categories AS cc ON cc.id = c.category 
										JOIN mdl_user AS u ON st.userid=u.id
										join mdl_skillsoft_au_track AS at
										on at.userid=u.id and at.skillsoftid=sc.id and at.element='[SUMMARY]completed'
										and at.value > unix_timestamp('$starttime')
										and at.value < unix_timestamp('$endtime')
										where st.element='[CORE]lesson_status' and st.value in ('completed','passed') and u.id=$userid
										group by sc.assetid");

		$skillsoftduration=0;
		if ($skillsoft) {
        foreach($skillsoft as $record) {

                $skillsoftduration = $skillsoftduration + $record->Duration;
                continue;
			}

		}
		
		$total+=$skillsoftduration;
		
		//Naga added for Non macthed skillsoft courses	
		$skillsoft_nonmatched = get_records_sql("SELECT sc.id, u.username as PortalID,
													trim(c.fullname) as 'Course',cc.name as 'Category',
													trim(sc.name) as 'Asset',
													sc.duration/60 as 'Duration',
													st.value as 'Grade',
													DATE_FORMAT(FROM_UNIXTIME(at.value),'%b %d %Y') as 'CompletedDate'
													FROM mdl_skillsoft_au_track_nonmatched AS st
													JOIN mdl_skillsoft_nonmatched AS sc ON sc.id=st.skillsoftid
													JOIN mdl_course AS c ON c.id=sc.course
													JOIN mdl_course_categories AS cc ON cc.id = c.category 
													JOIN mdl_user AS u ON st.userid=u.id
													join mdl_skillsoft_au_track_nonmatched AS at
													on at.userid=u.id and at.skillsoftid=sc.id and at.element='[SUMMARY]completed'
													and at.value >= unix_timestamp('$starttime')
													and at.value < unix_timestamp('$endtime')
													where st.element='[CORE]lesson_status' and st.value in ('completed','passed') and u.id=$userid
													group by sc.assetid");

		$skillsoftduration_nonmatched=0;
		if ($skillsoft_nonmatched) {
        foreach($skillsoft_nonmatched as $record) {

                $skillsoftduration_nonmatched = $skillsoftduration_nonmatched + $record->Duration;
                continue;
			}

		}
		
		$total+=$skillsoftduration_nonmatched;
		
		
		$quiz = get_record_sql("SELECT sum(q.timelimit)/60 as 'Duration'
								FROM mdl_grade_grades g
								INNER JOIN mdl_user u ON g.userid = u.id and u.id=$userid
								INNER JOIN mdl_grade_items i
								ON g.itemid = i.id
								INNER JOIN mdl_course c on c.id = i.courseid
								inner join mdl_quiz q on q.course=c.id and q.id=i.iteminstance
								WHERE (i.itemname IS NOT NULL)AND (i.itemtype = 'mod')AND
								(i.itemmodule = 'quiz')
								AND (g.finalgrade >= 0) 
								and g.timemodified >= unix_timestamp('$starttime')
								and g.timemodified < unix_timestamp('$endtime')");
		$total+=$quiz->Duration;
		
		
		$assignment = get_record_sql("SELECT sum(a.duration)/60 as 'Duration'
										FROM mdl_assignment_submissions s join mdl_assignment a
										on s.assignment=a.id and s.timemodified >0 join mdl_course c
										on c.id=a.course join mdl_user u
										on u.id=s.userid and u.id=$userid
										and s.timemodified >= unix_timestamp('$starttime')
										and s.timemodified < unix_timestamp('$endtime')");
		$total+=$assignment->Duration;
				
        return sprintf("%.0f",$total);
    }
$empduration=getdurationDate($USER->id,'2016/4/1','2017/4/1');
$manditorytrainingtemp = get_record_sql("SELECT mandatorytrainings from mdl_user where id=$USER->id");

if($manditorytrainingtemp->mandatorytrainings>0)
{
$manditorytraining=$manditorytrainingtemp->mandatorytrainings;
}
else
{
$manditorytraining=0;
}
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>


</head>

<body>

<div>



	
		<table  border=0 cellspacing=0 cellpadding=0 
 style='width:350.75pt;border:none'>
 <tr >
  <td  style='padding:0in 1.4pt 0in 5.4pt;height:45.75pt'>
<?php echo '<a href="'.$CFG->wwwroot.'/user/view.php?id='.$userid.'&amp;course='.$COURSE->id.'"><img height="100" width="100" src="'.$CFG->wwwroot.'/user/pix.php?file=/'.$userid.'/f1.jpg" width="100px" height="100px" align="center" title="'.$USER->firstname.' '.$USER->lastname.'" alt="'.$USER->firstname.' '.$USER->lastname.'" /></a>'; ?>
  </td> 

  <td width=320 style='width:260.3pt;padding:0in 1.4pt 0in 5.4pt;height:45.75pt;align:left'>
  <p></p>
    <p   style='text-align:left;line-height:normal'><span style='font-size:10.0pt;
  font-family:"Comic Sans MS";mso-bidi-font-family:Arial;color:#17375E'>Dear <?php echo $USER->firstname.' '.$USER->lastname;?></span></p>
  <span style='font-size:8.0pt;font-family:"Comic Sans MS"'>
  
<?php
if($USER->city=="MISI" || $USER->city=="DSKLS")
{
?>
<span style='font-size:10.0pt;font-family:"Comic Sans MS";color:#17375E'>We are unable to identify an annual training goal for you. </span><span style='font-size:12.0pt;font-family:"Comic Sans MS";color:#17375E'>Please consult with your manager.</span>
<?php
}
else if($USER->employee_type="Employee" && $USER->grade>0 && $USER->grade<16)
{ 
?>
  </span><span style='font-size:10.0pt;font-family:"Comic Sans MS";color:#17375E'>As per your grade,  </span><span style='font-size:12.0pt;font-family:"Comic Sans MS";color:#17375E'> your minimum learning and development hours are </span><span
  style='font-size:12.0pt;font-family:"Comic Sans MS";color:#E46C0A'><?php echo $manditorytraining; ?></span><span 
  style='font-size:10.0pt;font-family:"Comic Sans MS";color:#17375E'>
  for 2016 - 2017.</span>
 <?php
 }
 ?>

  <p   style='text-align:left;line-height:normal'><span style='font-size:10.0pt;
  font-family:"Comic Sans MS";mso-bidi-font-family:Arial;color:#17375E'>You have completed </span><span
  style='font-size:12.0pt;font-family:"Comic Sans MS";color:#E46C0A'><?php echo $empduration; ?></span><span
  style='font-size:10.0pt;font-family:"Comic Sans MS";color:#17375E'> training hours to date. </span>
  <br/></p>
  </td>
 </tr>
 <tr><a class="modal_close" href="#"></a></tr>
</table>
    



</div>

</body>
</html>
