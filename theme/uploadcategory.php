<?php


    define('TOPIC_FIELD','/^(topic)([0-9]|[1-4][0-9]|5[0-2])$/');
    define('TEACHER_FIELD','/^(teacher)([1-9]+\d*)(_account|_role)$/');

    require_once('../config.php');
    require_once("../course/lib.php");
    require_once("$CFG->libdir/blocklib.php");
    $courseid=-1;

    function csverror($message, $link='') {
        global $CFG, $SESSION;
    
        print_header(get_string('error'));
        echo '<br />';
    
        $message = clean_text($message);
    
        print_simple_box('<span style="font-family:monospace;color:#000000;">'.$message.'</span>', 'center', '', '#FFBBBB', 5, 'errorbox');
    
        if (!$link) {
            if ( !empty($SESSION->fromurl) ) {
                $link = $SESSION->fromurl;
                unset($SESSION->fromurl);
            } else {
                $link = $CFG->wwwroot .'/';
            }
        }
        print_continue($link);
        print_footer();
        die;
    }

    function getdefaultcategory() {
        static $mysqlresource1;
        static $cat;

        global $CFG;
        global $USER;

        ($mysqlresource1=mysql_query('SELECT MIN(`id`) FROM `'.$CFG->prefix.'course_categories`')) or csverror('You have no category table!','uploadcategory.php?sesskey='.$USER->sesskey); 

            if (mysql_num_rows($mysqlresource1)) {
                $cat = mysql_fetch_row($mysqlresource1);
                return $cat[0];
            } else {
                return 1; // *SHOULD* be the Misc category?
            }
    }
    function checkisint($supposedint) {
        return ((string)intval($supposedint) == $supposedint) ? true : false;
    }
    function checkisstring($supposedstring) {
        $supposedstring = trim($supposedstring); // Is it just spaces?
        return (strlen($supposedstring) == 0) ? false : true;
    }
    function validateas($value, $validatename, $lineno, $fieldname = '') {
  
        // Validates each field based on information in the $validate array
        global $USER;
        global $validate;
        
        ($fieldname=='') and ($fieldname=$validatename);
                                
       

        $format = $validate[$validatename];

        switch($format[0]) {

        case 1: // Category
            if (checkisint($value)) {
              // It's a Category ID Number
                categoryexists_ex($value) or csverror('Invalid value for field '.$fieldname.' (No Category with ID '.$value.'). '.
                          get_string('erroronline', 'error', $lineno) .". ".
                          get_string('processingstops', 'error'), 
                          'uploadcategory.php?sesskey='.$USER->sesskey);
            } elseif(checkisstring($value)) {
               // It's a Category Path string
               
               $value=trim(str_replace('\\','/',$value)," \t\n\r\0\x0B/");
               // Clean path, ensuring all slashes are forward ones
               
               (strlen($value)>0) or csverror('Invalid value for field '.$fieldname.' (Path string not set). '.
                          get_string('erroronline', 'error', $lineno) .". ".
                          get_string('processingstops', 'error'), 
                          'uploadcategory.php?sesskey='.$USER->sesskey);
                
                unset ($cats);
                $cats=explode('/',$value); // Break up path into array
                
                (count($cats)>0) or csverror('Invalid value for field '.$fieldname.' (Path string "'.$value.'" invalid - not delimited correctly). '.
                          get_string('erroronline', 'error', $lineno) .". ".
                          get_string('processingstops', 'error'), 
                          'uploadcategory.php?sesskey='.$USER->sesskey);
                          
                foreach ($cats as $n => $item) { // Validate the path
                  
                  $item=trim($item); // Remove whitespace
                  
                  (strlen($item) <= 255) or csverror('Invalid value for field '.$fieldname.' (Category name "'.$item.'" length &gt; 30). '.
                                      get_string('erroronline', 'error', $lineno) .". ".
                                      get_string('processingstops', 'error'), 
                                      'uploadcategory.php?sesskey='.$USER->sesskey);
                  
                  checkisstring($item) or csverror('Invalid value for field '.$fieldname.' (Path string "'.$value.'" invalid - category name at position '.($n+1).' as shown is invalid). '.
                      get_string('erroronline', 'error', $lineno) .". ".
                      get_string('processingstops', 'error'), 
                      'uploadcategory.php?sesskey='.$USER->sesskey); 
                }
                
                $value=$cats; // Return the array
                unset ($cats);
               
            } else {
               csverror('Invalid value for field '.$fieldname.' (not an integer or string). '.
                      get_string('erroronline', 'error', $lineno) .". ".
                      get_string('processingstops', 'error'), 
                      'uploadcategory.php?sesskey='.$USER->sesskey); 
            }
        break;        

        default:
            csverror('Coding Error: Bad field validation type: "'.$format[0].'"','uploadcategory.php?sesskey='.$USER->sesskey);
        break;
        }

        return $value;

    }
    function categoryexists_ex($categoryid) {
        // Does category with given id exist?
        global $CFG;
        static $mysqlresource1;
        if ($mysqlresource1=mysql_query('SELECT `id` FROM `'.$CFG->prefix.'course_categories` WHERE `id`='.mysql_real_escape_string($categoryid))){
            return mysql_num_rows($mysqlresource1) ? true : false;
        } else {
            return false;
        }
    }

    function microtime_float()
    {
        // In case we don't have php5
        list($usec, $sec) = explode(" ", microtime());
        return ((float)$usec + (float)$sec);
    }
    function mystr($value)
    {
        // Prepare $value for inclusion in SQL Query
        return '\''.mysql_real_escape_string($value).'\'';;
    }
    
    function fast_get_category_ex($hname, &$hstatus, $hparent=0)
    {
        // Find category with the given name and parentID, or create it, in both cases returning a category ID
        /* $hstatus:
            -1  :   Failed to create category
            1   :   Existing category found
            2   :   Created new category successfully
        */
        
        static $mysqlresource1;
        static $mysqlresource2;
        static $cat;
        
        static $cname;
        
        global $CFG;
        global $USER;
        
        $cname = mystr($hname);
        
        // Check if a category with the same name and parent ID already exists
        $mysqlresource1=mysql_query('SELECT `id` FROM `'.$CFG->prefix.'course_categories` WHERE `name` = '.$cname.' AND `parent` = '.$hparent);
            if (mysql_num_rows($mysqlresource1)) {
                $cat = mysql_fetch_row($mysqlresource1);
                $hstatus=1;
                return $cat[0];
            } else {
                // Create it - moodle does set sortorder to 999, and doesn't use the description field
                if (!($mysqlresource2=mysql_query('INSERT INTO `'.$CFG->prefix.'course_categories` (`name`,`description`,`parent`,`sortorder`,`coursecount`,`visible`,`timemodified`) VALUES ('.$cname.',\'\','.$hparent.',999,0,1,0);'))) {
                  // Failed!
                    $hstatus=-1;
                    return -1;
                } else {
                    $hstatus=2;
                    return mysql_insert_id();
                }
            }      
    }
    

    require_login();
    
    if (!isadmin()) {
        csverror('You must be an administrator to edit courses in this way.');
    }

/*    if (!confirm_sesskey()) {
        csverror(get_string('confirmsesskeybad', 'error'));
    } */

    if (! $site = get_site()) {
        csverror('Could not find site-level course');
    }

    if (!$adminuser = get_admin()) {
        csverror('Could not find site admin');
    }

    ini_set('max_execution_time',300); // Up the php timeout
    $time_start = microtime_float(); // Just for timing


    $stradministration = get_string('administration');
    $strchoose = get_string('choose');
    $struploadcourses = 'Upload Courses';

    $csv_encode = '/\&\#44/';
    if (isset($CFG->CSV_DELIMITER)) {        
        $csv_delimiter = '\\' . $CFG->CSV_DELIMITER;
        $csv_delimiter2 = $CFG->CSV_DELIMITER;

        if (isset($CFG->CSV_ENCODE)) {
            $csv_encode = '/\&\#' . $CFG->CSV_ENCODE . '/';
        }
    } else {
        $csv_delimiter = '\,';
        $csv_delimiter2 = ',';
    }

/// Print the header

    print_header("$site->shortname: $struploadcourses", $site->fullname, 
                 "<a href=\"index.php\">$stradministration</a> -> $struploadcourses");


/// If a file has been uploaded, then process it

    
    require_once($CFG->dirroot.'/lib/uploadlib.php');
    
    $um = new upload_manager('coursefile',false,false,null,false,0);
    if ($um->preprocess_files()) {
  
        if (!isset($um->files['coursefile']))
            csverror('Upload Error!', 'uploadcategory.php?sesskey='.$USER->sesskey);
        
        $filename = $um->files['coursefile']['tmp_name'];
        
        // Everything to Unix Line Endings
        $text = my_file_get_contents($filename);
        $text = preg_replace('!\r\n?!',"\n",$text);
        if ($fp = fopen($filename, "w")) {
            fwrite($fp,$text);
            unset($text); // Memory!
            fclose($fp);
        } else {
            csverror('File I/O Error! (1)', 'uploadcategory.php?sesskey='.$USER->sesskey);
        }

        if (!$fp = fopen($filename, "r")) {
            csverror('File I/O Error! (2)', 'uploadcategory.php?sesskey='.$USER->sesskey);
        } 

        // make arrays of fields for error checking
        $defaultcategory = getdefaultcategory();
        $defaultmtime = time();

        $required = array('category' => $defaultcategory);

		$validate = array('category' => array(1));
        $header = fgetcsv($fp, 1024);
        // check for valid field names

        if (($header[0]==null)&&(count($line)<1)) // Blank Line?
            csverror('First line must be the CSV header', 'uploadcategory.php?sesskey='.$USER->sesskey);  
        
        foreach ($header as $i => $h) {
            if ($h==null)
                csverror('Null CSV columns are not permitted in header', 'uploadcategory.php?sesskey='.$USER->sesskey);
                        if (preg_match(TOPIC_FIELD,$h)) { // Regex defined header names
                        } elseif (preg_match(TEACHER_FIELD,$h)) {                         
                        } else {
                if (!(isset($required[$h]) || isset($optional[$h]))) 
                    csverror(get_string('invalidfieldname', 'error', $h), 'uploadcategory.php?sesskey='.$USER->sesskey);
    
                if (isset($required[$h])) $required[$h] = true; // Mark Field as present
            }
        }
        // check for required fields
        foreach ($required as $key => $value) {
            if ($value != true) 
                csverror(get_string('fieldrequired', 'error', $key), 'uploadcategory.php?sesskey='.$USER->sesskey);
        }

        $fieldcount = count($header);
        $lineno=2;
        unset($bulkcourses);

        while (($line = fgetcsv($fp, 1024)) !== false) {
            if (($line[0]!=null)||(count($line)>1)) { // Blank Line?
                if (count($line) > $fieldcount)
                    csverror('Too many actual values. '.
                              get_string('erroronline', 'error', $lineno) .". ".
                              get_string('processingstops', 'error'), 
                              'uploadcategory.php?sesskey='.$USER->sesskey);
                foreach ($header as $i => $h) {
                    // Is line complete?
                    if (!isset($line[$i])) {
                        csverror(get_string('missingfield', 'error', $h). ". ".
                              get_string('erroronline', 'error', $lineno) .". ".
                              get_string('processingstops', 'error'), 
                              'uploadcategory.php?sesskey='.$USER->sesskey);
                    }
                }
                unset($coursetocreate);
                unset($coursetopics);
                unset($courseteachers);
                foreach ($optional as $key => $value) { // Set course array to defaults
                    $coursetocreate[$key] = $value;
                }
                foreach ($line as $key => $value) { // Validate each value
                    $cf = $header[$key];
                    if (preg_match(TOPIC_FIELD,$cf,$matches)) {
                      $coursetopics[$matches[2]] = validateas($value,$matches[1], $lineno, $cf);
                    } elseif (preg_match(TEACHER_FIELD,$cf,$matches)) {
                      $tmp=validateas($value,$matches[1].$matches[3], $lineno, $cf);
                      (isset($tmp)&&($tmp!='')) and
                          ($courseteachers[$matches[2]][$matches[3]] = $tmp);
                    } else {
                        $coursetocreate[$cf] = validateas($value, $cf, $lineno); // Accept value if it passed validation
                    }
                }
                $coursetocreate['topics'] = $coursetopics;
				
                if (isset($courseteachers))
                foreach ($courseteachers as $key => $value) // Deep validate course teacher info on second pass
                {
                  if (isset($value) && (count($value) > 0)){
                    if (!(isset($value['_account'])&&checkisint($value['_account'])))
                         csverror('Invalid value for field teacher'.$key.' - other fields were specified but required teacher'.$key.'_account was null. '.
                          get_string('erroronline', 'error', $lineno) .". ".
                          get_string('processingstops', 'error'), 
                          'uploadcategory.php?sesskey='.$USER->sesskey);
                    // Hardcoded default values (that are as close to moodle's UI as possible)
                    // and we can't assume PHP5 so no pointers!
                    if (!isset($value['_role']))
                        $courseteachers[$key]['_role'] = '';
                  }
                }
                $coursetocreate['teachers_enrol'] = $courseteachers;
                $bulkcourses[] = $coursetocreate; // Merge into array
            }
            $lineno++;
        }

        fclose($fp);
        
        if ((!isset($bulkcourses)) or (count($bulkcourses)==0))
            csverror('No courses were parsed from CSV', 'uploadcategory.php?sesskey='.$USER->sesskey);
        else
            notify('Parsed '.count($bulkcourses).' course(s) from CSV','notifysuccess');
            
        // Running Status Totals
        
        $t = 0; // Read courses
        $s = 0; // Skipped courses
        $n = 0; // Created courses
        $p = 0; // Broken courses (failed halfway through
        
        $cat_e = 0; // Errored categories
        $cat_c = 0; // Created categories

        foreach ($bulkcourses as $bulkcourse) {
            // Try to create the course
            
              
                $coursetocategory = 0; // Category ID
                
                if (is_array($bulkcourse['category'])) {
                    // Course Category creation routine as a category path was given
                    
                    $curparent=0;
                    $curstatus=0;
    
                    foreach ($bulkcourse['category'] as $catindex => $catname) {
                      $curparent = fast_get_category_ex($catname,$curstatus,$curparent);
                        switch ($curstatus) {
                          case 1: // Skipped the category, already exists
                          break;
                          case 2: // Created a category
                            $cat_c++;
                          break;
                          default:
                            $cat_e+=count($bulkcourse['category']) - $catindex;
                            $coursetocategory=-1;
                            notify('An error occured creating category with name '.$catname.', meaning a total '.(count($bulkcourse['category']) - $catindex).' category(ies) failed','notifyproblem');
                          break 2;
                        }      
                    }
                    ($coursetocategory==-1) or $coursetocategory = $curparent;
                    // Last category created will contain the actual course
                } else {
                    // It's just a straight category ID
                    $coursetocategory=$bulkcourse['category'];
                }
                      
            $t++;
        }    

       

      
        
        if ($cat_e > 0)
            notify ($cat_e.' new category(ies) failed due to errors','notifyproblem');
            
        if ($cat_c > 0) {
            notify ($cat_c.' new category(ies) were created','notifysuccess');
            notify ('You may wish to manually Re-Sort the categories','notifysuccess');
            
            // We don't automatically sort alphabetically, as the somewhat clunky category sorting
            // method used by moodle could annoy the user
        }
        
        
    
       
               

        $time_end = microtime_float();

        notify('Total Execution Time: '.round(($time_end - $time_start),2).' s','notifysuccess');

        echo '<hr />';
        
    } else {
    
    // Print Help
    
    ?><center><p>Upload an <a href="http://www.rfc-editor.org/rfc/rfc4180.txt" target="_blank">RFC4180</a>-Compliant CSV file.<br />Valid fields for each course are:</p><table border="1" style="font-family:monospace;font-size:8pt;width:500px;">
<tr><th>Field</th><th>Value</th></tr>
<tr><td>category</td><td>[Forward]Slash-Delimited Category &quot;Path&quot; String (new categories are created as necessary) OR Integer Database Category ID</td></tr>
</table></center>
<?php

    }

/// Print the form
    print_heading('Upload Courses');
    $maxuploadsize = get_max_upload_file_size();
    echo '<center>';
    echo '<form method="post" enctype="multipart/form-data" action="uploadcategory.php">'.
         $strchoose.':<input type="hidden" name="MAX_FILE_SIZE" value="'.$maxuploadsize.'">'.
         '<input type="hidden" name="sesskey" value="'.$USER->sesskey.'">'.
         '<input type="file" name="coursefile" size="30">'.
         '<input type="submit" value="Upload">'.
         '</form></br>';
    echo '</center>';
    print_footer($course);



function my_file_get_contents($filename, $use_include_path = 0) {
/// Returns the file as one big long string

    $data = "";
    $file = @fopen($filename, "rb", $use_include_path);
    if ($file) {
        while (!feof($file)) {
            $data .= fread($file, 1024);
        }
        fclose($file);
    }
    return $data;
}

?>