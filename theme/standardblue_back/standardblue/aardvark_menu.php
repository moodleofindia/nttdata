<link href="https://lms.portal.keane.com/theme/standardblue/tree.css" rel="stylesheet" type="text/css" />
<script src="https://lms.portal.keane.com/theme/standardblue/tree.js" type="text/javascript"></script>

<div id="dropdown" class="yuimenubar yuimenubarnav">
<div>
<div>
    <ul id="menu">
    
    <li><a href="#" class="drop">Home</a><!-- Begin Home Item -->
    
        <div class="dropdown_2columns"><!-- Begin 2 columns container -->
    
            <div class="col_2">
                <h3>Welcome !</h3>
            </div>
    
            <div class="col_2">
                <p style="font-size:10pt">Welcome to Keane LMS.</p>             
                <p style="font-size:9pt">Training and performance support in a virtual environment for Keane employees. Access to KGEMs, and Skillport are included in the virtual offerings.</p>             
            </div>
    
            <div class="col_2">
                <h3>Select a Home page</h3>
            </div>
          
            <div class="col_1">
			<ul>
			 <table cellspacing="10">
			 <tr>
			 <td>
            <img src="https://lms.portal.keane.com//homefiles/lms.JPG" width="57" height="47" alt="" /></td>
			<td><a href="https://lms.portal.keane.com//LMShome.php">
			<li>LMS Home Page</li></a></td>
			</tr>
			<tr>
            <td><img src="https://lms.portal.keane.com//homefiles/c.JPG" width="57" height="47" alt="" /></td>
			<td><p><a href="https://lms.portal.keane.com//KLCI_Certifications.php"><li>KLCI</li></a></p></td>
			</tr>
			<tr>
            <td><img src="https://lms.portal.keane.com//homefiles/g.JPG" width="57" height="47" alt="" /></td>
			<td><p><a href="https://lms.portal.keane.com/course/category.php?id=110"><li>KGEMs
</li></a></p></td>
			</tr>
			<tr>
            <td><img src="https://lms.portal.keane.com//homefiles/s.JPG" width="57" height="47" alt="" /></td>
			<td><p><a href="https://lms.portal.keane.com//course/category.php?id=49"><li>SkillSoft</li></a></p></td>
			</tr>
			 </table>
			</ul>
            </div>
            
            
          
        </div><!-- End 2 columns container -->
    
    </li><!-- End Home Item -->

	 <li><a href="#" class="drop">Course Catalogue</a><!-- Begin 4 columns Item -->
    
        <div class="dropdown_4columns"><!-- Begin 4 columns container -->

   
            
            <div class="col_1">
            
            <h3>Internal Offerings</h3>
			<ul id="treemenu1" ><!-- deleted ** class="treeview" -->
			<li  ><a href="https://lms.portal.keane.com/course/courselistnew.php?catid=143">Technical</a></li>
			<li  ><a href="https://lms.portal.keane.com/course/courselistnew.php?catid=13">Project Management</a></li>
			<li  ><a href="https://lms.portal.keane.com/course/courselistnew.php?catid=24">Behavioral</a></li>
			<li  ><a href="https://lms.portal.keane.com/course/courselistnew.php?catid=26">Process</a></li>
			<li  ><a href="https://lms.portal.keane.com/course/category.php?id=110">Compliance</a></li>
			
	</ul>
	       


            </div>
        <div class="col_1">
            
                <h3>External Offerings</h3>
                <ul>
                    <li><a href="https://lms.portal.keane.com/course/skillsoftlist.php?catid=49">Skill Soft</a></li>
                    <li><a href="http://skillport.books24x7.com/login.asp?ic=0">Books 24x7</a></li>
                </ul>   
                 
            </div>
			 </div>
			</li>
			
	 <li><a href="#" class="drop">Programs</a><!-- Begin 4 columns Item -->
    
        <div class="dropdown_4columns"><!-- Begin 4 columns container -->
         <div class="col_3">

                <img src="https://lms.portal.keane.com//homefiles/lead.gif" width="70" height="70" class="img_left imgshadow" alt="" />
                <p><a href="#"></a><a href="https://lms.portal.keane.com/course/programlist.php?catid=114">A variety of development programs leveraging the best of training resources and on the job experience. Click here to view.</a></p>
    
                
            </div>

        </div><!-- End 4 columns container -->
    
    </li><!-- End 4 columns Item -->
    <li ><a href="#" class="drop">KLCI Certifications </a><!-- Begin 5 columns Item -->
    
        <div class="dropdown_5columns"><!-- Begin 5 columns container -->
       
            
        
            <div class="col_5">
                <h3>List of available KLCI Certifications</h3>
            </div>
           <br/>
		  
	<!-- starting of jquery tree view -->
	<div class="col_6">
	<h3>India</h3>
             
	<DIV style="margin-left:20px">
	<script language="javascript">
      <!-- addExpandCollapseAll(); -->
     startParentNode("ADM");
        startParentNode("Java");
			 
					addNode("Java Developer","https://lms.portal.keane.com/course/category.php?id=10","_parent");
			 
					addNode("Advanced Java Developer","https://lms.portal.keane.com/course/category.php?id=18","_parent");
			 
					addNode("Java Junior Designer","https://lms.portal.keane.com/course/category.php?id=19","_parent");
					 
            
		endParentNode();
		startParentNode(".Net");
			 
					addNode(".Net Developer","https://lms.portal.keane.com/course/category.php?id=11","_parent");
		 
					addNode("Advanced .Net Developer","https://lms.portal.keane.com/course/category.php?id=20","_parent");
			 
					addNode(".Net Junior Designer","https://lms.portal.keane.com/course/category.php?id=127","_parent");
	       
		endParentNode();
		startParentNode("Mainframe");
			 
					addNode("Mainframe Application Developer","https://lms.portal.keane.com/course/category.php?id=135","_parent");
             
		endParentNode();
	endParentNode();
	startParentNode("EAS");
         	
					addNode("Oracle","https://lms.portal.keane.com/course/category.php?id=16","_parent");
		
					addNode("SAP ","https://lms.portal.keane.com/course/category.php?id=44","_parent");
		 
					addNode("PeopleSoft","https://lms.portal.keane.com/course/category.php?id=65","_parent");
		 
	endParentNode();
	startParentNode("Project Management");
        
					addNode("PM Certification","https://lms.portal.keane.com/course/category.php?id=14","_parent");
		 
	endParentNode();
	startParentNode("ADM Testing");
        
					addNode("Keane Certified Test Manager","https://lms.portal.keane.com/course/category.php?id=32","_parent");
		
					addNode("Keane Certified Test Professional","https://lms.portal.keane.com/course/category.php?id=23","_parent");
		
	endParentNode();
	startParentNode("TIS Certification");	
					addNode("TIS","https://lms.portal.keane.com/course/category.php?id=34","_parent");
	endParentNode();
	startParentNode("Business Intelligence");	
					addNode("Business Intelligence","https://lms.portal.keane.com/course/category.php?id=58","_parent");
	endParentNode();
             
 </script>
	
				
    </div>    
		
	 </div>

<!-- end of jquey tree view -->
            <div class="col_6">
            
                <h3>Rest of the World</h3>
           <script language="javascript">
      <!-- addExpandCollapseAll(); -->
     startParentNode("ADM");
        startParentNode("Java");			
			 
					addNode("Java Junior Designer","https://lms.portal.keane.com/course/category.php?id=19","_parent");
		    
		endParentNode();
		startParentNode(".Net");
			 
					addNode(".Net Junior Designer","https://lms.portal.keane.com/course/category.php?id=127","_parent");

		endParentNode();
	endParentNode();
	</script>
<br/>

        </div><!-- End 5 columns container -->
</div>
    </li><!-- End 5 columns Item -->


	<li><a href="#" class="drop">Calendar</a>
    
		<div class="dropdown_1column">
        
                <div class="col_1">
                
                    <ul class="simple">
                        <li><a href="https://lms.portal.keane.com/blocks/classroom/currentcalender.php">Current Month</a></li>
                        <li><a href="https://lms.portal.keane.com/blocks/classroom/monthlycalender.php">Next Month</a></li>
                        <li><a href="https://lms.portal.keane.com/blocks/classroom/mysessions.php">Search & Signup</a></li>
						<li><a href="https://lms.portal.keane.com/blocks/classroom/ilttrainer.php">Take attendance</a></li>
                        <li><a href="https://lms.portal.keane.com/mod/resource/view.php?id=587">Help on Signup</a></li>
                    </ul>  
                </div>
                
		</div>
        
	</li>
	<li><a href="#" class="drop">Collaborate</a>
    
		<div class="dropdown_1column">
        
                <div class="col_1">
                
                    <ul class="simple">
                        <li><a href="https://lms.portal.keane.com/mod/forum/view.php?f=2">Discussion Board</a></li>
                        <li><a href="https://lms.portal.keane.com/blog/index.php?userid=7957&courseid=1">MyBlogs</a></li>
                        <li><a href="https://lms.portal.keane.com/notes/index.php?course=1&user=7957">MyNotes</a></li>						
                    </ul>  
                </div>
                
		</div>
        
	</li>


</ul>


</div>
</div>
	</div>

