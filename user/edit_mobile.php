
<?php // $Id: edit.php,v 1.167.2.16 2009/09/26 19:16:53 skodak Exp $

    require_once('../config.php');
    require_once($CFG->libdir.'/gdlib.php');
    require_once($CFG->dirroot.'/user/edit_form_mobile.php');
    require_once($CFG->dirroot.'/user/editlibmobile.php');
    require_once($CFG->dirroot.'/user/profile/lib.php');

    httpsrequired();
	$mkey = $_GET['mk'];
    $userid = optional_param('id', $USER->id, PARAM_INT);    // user id
    $course = optional_param('course', SITEID, PARAM_INT);   // course id (defaults to Site)
    //$cancelemailchange = optional_param('cancelemailchange', false, PARAM_INT);   // course id (defaults to Site)

    if (!$course = get_record('course', 'id', $course)) {
        error('Course ID was incorrect');
    }

    if ($course->id != SITEID) {
        require_login($course);
    } else if (!isloggedin()) {
        if (empty($SESSION->wantsurl)) {
            $SESSION->wantsurl = $CFG->httpswwwroot.'/user/edit_mobile.php?mk='.$mkey;
        }
        redirect($CFG->httpswwwroot.'/login/index.php');
    }

    // Guest can not edit
    if (isguestuser()) {
        print_error('guestnoeditprofile');
    }

	
    // The user profile we are editing

	if (!$user = get_record_sql("select id,firstname,lastname,email,country,description from mdl_user where id=$userid"))
	{
	  error('User ID was incorrect');
	}



    // Guest can not be edited
    if (isguestuser($user)) {
        print_error('guestnoeditprofile');
    }



    if ($course->id == SITEID) {
        $coursecontext = get_context_instance(CONTEXT_SYSTEM);   // SYSTEM context
    } else {
        $coursecontext = get_context_instance(CONTEXT_COURSE, $course->id);   // Course context
    }
    $systemcontext   = get_context_instance(CONTEXT_SYSTEM);
    $personalcontext = get_context_instance(CONTEXT_USER, $user->id);


    if ($user->deleted) {
        print_header();
        print_heading(get_string('userdeleted'));
        print_footer($course);
        die;
    }






    //create form
    $userform = new user_edit_form();
    if (empty($user->country)) {
        // MDL-16308 - we must unset the value here so $CFG->country can be used as default one
        unset($user->country);
    }
    $userform->set_data($user);




    if ($usernew = $userform->get_data()) {

        add_to_log($course->id, 'user', 'update', "edit_mobile.php?id=$user->id&course=$course->id", '');




        $usernew->timemodified = time();

        if (!update_record('user', $usernew)) {
            error('Error updating user record');
        }
		else
		{
		redirect("$CFG->wwwroot/user/response_mobile.php");
		}




        // reload from db
        $usernew = get_record('user', 'id', $user->id);
        events_trigger('user_updated', $usernew);

        if ($USER->id == $user->id) {
            // Override old $USER session variable if needed
            foreach ((array)$usernew as $variable => $value) {
                $USER->$variable = $value;
            }
        }

    }


/// Display page header
    $streditmyprofile = get_string('editmyprofile');
    $strparticipants  = get_string('participants');
    $userfullname     = fullname($user, true);


    print_header($course->fullname, "");




        $userform->display();



?>
