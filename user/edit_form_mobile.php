<?php //$Id: edit_form.php,v 1.24.2.13 2009/09/27 19:13:22 skodak Exp $

require_once($CFG->dirroot.'/lib/formslib.php');

class user_edit_form extends moodleform {

    // Define the form
    function definition () {
        global $CFG, $COURSE;

        $mform =& $this->_form;
      //  $this->set_upload_manager(new upload_manager('imagefile', false, false, null, false, 0, true, true, false));
        //Accessibility: "Required" is bad legend text.
        $strgeneral  = 'Update Mobile Key';
        $strrequired = get_string('required');

        /// Add some extra hidden fields
        $mform->addElement('hidden', 'id');
        $mform->setType('id', PARAM_INT);
        $mform->addElement('hidden', 'course', $COURSE->id);
        $mform->setType('course', PARAM_INT);

        /// Print the required moodle fields first
        $mform->addElement('header', 'moodle', $strgeneral);

        /// shared fields
        useredit_shared_definition($mform);



        $this->add_action_buttons(false, 'Register Key');
    }

    function definition_after_data() {
        global $CFG;

        $mform =& $this->_form;
        $userid = $mform->getElementValue('id');

        // if language does not exist, use site default lang
        if ($langsel = $mform->getElementValue('lang')) {
            $lang = reset($langsel);
            // missing _utf8 in language, add it before further processing. MDL-11829 MDL-16845
            if (strpos($lang, '_utf8') === false) {
                $lang = $lang . '_utf8';
                $lang_el =& $mform->getElement('lang');
                $lang_el->setValue($lang);
            }
            // check lang exists
            if (!file_exists($CFG->dataroot.'/lang/'.$lang) and
              !file_exists($CFG->dirroot .'/lang/'.$lang)) {
                $lang_el =& $mform->getElement('lang');
                $lang_el->setValue($CFG->lang);
            }
        }

  


    }

    function validation($usernew, $files) {
        global $CFG;

        $errors = parent::validation($usernew, $files);

        $usernew = (object)$usernew;
        $user    = get_record('user', 'id', $usernew->id);

     

        /// Next the customisable profile fields
        $errors += profile_validation($usernew, $files);

        return $errors;
    }

    function get_um() {
        return $this->_upload_manager;
    }
}

?>
