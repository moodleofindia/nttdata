<?php  //$Id: editmobilelib.php,v 1.11.2.12 2009/10/06 19:04:04 skodak Exp $



function useredit_load_preferences(&$user, $reload=true) {
    global $USER;

    if (!empty($user->id)) {
        if ($reload and $USER->id == $user->id) {
            // reload preferences in case it was changed in other session
            unset($USER->preference);
        }
        
        if ($preferences = get_user_preferences(null, null, $user->id)) {
            foreach($preferences as $name=>$value) {
                $user->{'preference_'.$name} = $value;
            }
        }
    }
}

function useredit_shared_definition(&$mform) {
    global $CFG, $USER;
	$mkey = $_GET['mk'];
    $user = get_record('user', 'id', $USER->id);
    useredit_load_preferences($user, false);

    $strrequired = get_string('required');

    $nameordercheck = new object();
    $nameordercheck->firstname = 'a';
    $nameordercheck->lastname  = 'b';
    if (fullname($nameordercheck) == 'b a' ) {  // See MDL-4325
        $mform->addElement('text', 'lastname',  get_string('lastname'),  'maxlength="100" size="30"');
        $mform->addElement('text', 'firstname', get_string('firstname'), 'maxlength="100" size="30"');
    } else {
        $mform->addElement('text', 'firstname', get_string('firstname'), 'maxlength="100" size="30"');
        $mform->addElement('text', 'lastname',  get_string('lastname'),  'maxlength="100" size="30"');
    }

    $mform->addRule('firstname', $strrequired, 'required', null, 'client');
    $mform->setType('firstname', PARAM_NOTAGS);

    $mform->addRule('lastname', $strrequired, 'required', null, 'client');
    $mform->setType('lastname', PARAM_NOTAGS);




    $choices = get_list_of_countries();
    $choices= array(''=>get_string('selectacountry').'...') + $choices;
    $mform->addElement('select', 'country', get_string('selectacountry'), $choices);
    $mform->addRule('country', $strrequired, 'required', null, 'client');
    if (!empty($CFG->country)) {
        $mform->setDefault('country', $CFG->country);
    }

    $choices = get_list_of_timezones();
    $choices['99'] = get_string('serverlocaltime');
    if ($CFG->forcetimezone != 99) {
        $mform->addElement('static', 'forcedtimezone', get_string('timezone'), $choices[$CFG->forcetimezone]);
    } else {
        $mform->addElement('select', 'timezone', get_string('timezone'), $choices);
        $mform->setDefault('timezone', '99');
    }

    $mform->addElement('select', 'lang', get_string('preferredlanguage'), get_list_of_languages());
    $mform->setDefault('lang', $CFG->lang);

    if (!empty($CFG->allowuserthemes)) {
        $choices = array();
        $choices[''] = get_string('default');
        $choices += get_list_of_themes();
        $mform->addElement('select', 'theme', get_string('preferredtheme'), $choices);
        $mform->setAdvanced('theme');
    }

    $mform->addElement('htmleditor', 'description', get_string('userdescription'));
    $mform->setType('description', PARAM_CLEAN);
    $mform->setHelpButton('description', array('text', get_string('helptext')));
	$mform->addElement('text', 'mobilekey', 'Mobile key', 'maxlength="255" size="30"');
	$mform->setDefault('mobilekey',$mkey);


}

?>
