<?php
require_once('../config.php');
session_name("lmscontactusform");
session_start();

$str='';
if($_SESSION['errStr'])
{
	$str='<div class="error">'.$_SESSION['errStr'].'</div>';
	unset($_SESSION['errStr']);
}

$success='';
if($_SESSION['sent'])
{
	$success='<h1>Thank you!</h1> </br>
	<p>Our helpdesk will reach you as soon as we see your mail. In case of issues we would resolve the same within 48 hours.</p>';
	$css='<style type="text/css">#contact-form{display:none;}</style>';
	
	unset($_SESSION['sent']);
}
?>
<?php
    $pagetitle = format_string('Contact US');
    $navlinks[] = array('name' => $pagetitle, 'link' => '', 'type' => 'activityinstance');
	$navigation = build_navigation($navlinks);
	print_header_simple($pagetitle, '', $navigation);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>


<link rel="stylesheet" type="text/css" href="<?php echo $CFG->wwwroot; ?>/contactus/jqtransformplugin/jqtransform.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $CFG->wwwroot; ?>/contactus/formValidator/validationEngine.jquery.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $CFG->wwwroot; ?>/contactus/contact.css" />

<script type="text/javascript" src="<?php echo $CFG->wwwroot; ?>/contactus/jqtransformplugin/jquery.jqtransform.js"></script>
<script type="text/javascript" src="<?php echo $CFG->wwwroot; ?>/contactus/formValidator/jquery.validationEngine.js"></script>

<script type="text/javascript" src="<?php echo $CFG->wwwroot; ?>/contactus/script.js"></script>
<script language="JavaScript" type="text/javascript">
function submitform()
{
var a = document.getElementById("subject");
var b = document.getElementById("type");
//alert(a.options[a.selectedIndex].value);
var message=document.getElementById('message').value;
var name=document.getElementById('name').value;
var email=document.getElementById('email').value;
var subject =a.options[a.selectedIndex].value;


<?php
$message=($_POST['message']);
$name=($_POST['name']);
$email=($_POST['email']);

$subject1=($_POST['subject']);
$type=($_POST['type']);
$name1=$USER->firstname.' '.$USER->lastname;
$email1=$USER->email;
if(empty($name1))
{
$name2=$name;
}else{
$name2=$name1;
}
if(empty($email1))
{
$email2=$email;
}else{
$email2=$email1;
}
$subject= $name1.'   '.$type;
$subject2=$name1.':'.$subject1;
// mail content 
$erge ='<html xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:w="urn:schemas-microsoft-com:office:word" xmlns:m="http://schemas.microsoft.com/office/2004/12/omml" xmlns="http://www.w3.org/TR/REC-html40"><head><META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=us-ascii"><meta name=Generator content="Microsoft Word 14 (filtered medium)"><style><!--
/* Font Definitions */
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
/* Style Definitions */
p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin:0in;
	margin-bottom:.0001pt;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";}
a:link, span.MsoHyperlink
	{mso-style-priority:99;
	color:blue;
	text-decoration:underline;}
a:visited, span.MsoHyperlinkFollowed
	{mso-style-priority:99;
	color:purple;
	text-decoration:underline;}
span.EmailStyle17
	{mso-style-type:personal-compose;
	font-family:"Calibri","sans-serif";
	color:windowtext;}
.MsoChpDefault
	{mso-style-type:export-only;
	font-family:"Calibri","sans-serif";}
	font-family:"Calibri","sans-serif";}
@page WordSection1
	{size:8.5in 11.0in;
	margin:1.0in 1.0in 1.0in 1.0in;}
div.WordSection1
	{page:WordSection1;}
--></style><!--[if gte mso 9]><xml>

<o:shapedefaults v:ext="edit" spidmax="1026" />
</xml><![endif]--><!--[if gte mso 9]><xml>
<o:shapelayout v:ext="edit">
<o:idmap v:ext="edit" data="1" />
</o:shapelayout></xml><![endif]--></head><body lang=EN-US link=blue vlink=purple><div class=WordSection1><p class=MsoNormal>'.$subject2.'&nbsp;</o:p></p><p class=MsoNormal><o:p>&nbsp;</o:p></p><p class=MsoNormal>'.$message.'<o:p></o:p></p></div></body></html>';

	switch($subject1)
	{
	case ('Accessing A Course/Program' ||'Accessing A Course/Program - Program Enrollment' || 'Accessing A Course/Program - Course Player'||'Accessing A Course/Program - Finding a Course in Catalog'||'Accessing the Calendar'||'Accessing the Calendar - Time Not Correct'||'Accessing the Calendar - Sign up for session'||'Accessing the Calendar - Next Course Offering'||'Accessing the Calendar - Training specific to my location'
			||'Registering My training'||'Registering My training - Register project specific training'||'Registering My training - Register external training'||'My Training History'||'My Training History - Missing Item from History'||'My Training History - Training Time Not Correct'||'Application glitches'||'Others'):
				$touser = get_record('user','id','1'); // to give touser mail address
				email_to_user( $touser,$USER,$subject, $erge,$erge);
				break;
	case 'Accessing A Course/Program - Access for SkillSoft course' :
				$touser = get_record('user','id','41692');  // to give touser mail address
				email_to_user( $touser,$USER,$subject, $erge,$erge);
				break;
	case 'Accessing A Course/Program - Access for NLCI certification' :
				$touser = get_record('user','id','41693');   // to give touser mail address
				email_to_user( $touser,$USER,$subject, $erge,$erge);
				break;
	
	
			
	}
		?>
			
}	
</script>

</head>

<body>

<div id="main-container">

	<div id="form-container">
    <h1 class="contactus">Send us an email ..</h1>
    <h2 class="contactus">Do you have a query, suggestion or have any issues regarding your training. We value your opinion and would like to hear from you. We will respond to your queries and resolve issues within <font color="#FF4000">48 hours</font>. You may want to visit the <a target="_blank" href="<?php echo $CFG->wwwroot; ?>/LMSfaq.html"><font size="2" color="green">FAQ</font>
	</a>page or check the <a target="_blank" href="<?php echo $CFG->wwwroot; ?>/mod/forum/view.php?f=2"><font size="2" color="green">Discussion Board</font></a> for similar issues. </h2>

    <form   name="contact-form" id="contact-form" method="post" action="contact.php">

      <table width="100%" border="0" cellspacing="0" cellpadding="5">
        <tr>
          <td width="15%"><label  for="name">From</label></td>
          <td width="70%"><input c type="text" size=40 class="validate[required,custom[onlyLetter]]" name="name" id="name" disabled value="<?php echo $USER->firstname.' '; echo $USER->lastname; ?>"  /></td>
          <td width="15%" id="errOffset">&nbsp;</td>
        </tr>
        <tr>
          <td><label  for="email">Email</label></td>
          <td><input  type="text" size=40 class="validate[required,custom[email]]" name="email" id="email" value="<?php echo $USER->email;?>" /></td>
          <td>&nbsp;</td>
        </tr>
		<tr>
		 <td><label  for="type">Type</label></td>
		
		 <td><input  type="radio" name="type" value="query"><label >I have a Query</label> 
<input class="contactus" type="radio" name="type" value="issue"><label >I have an Issue</label>
<input class="contactus" type="radio" name="type" value="suggestion"><label >I have a Suggestion</label>

</td>
 </tr>
        <tr>
          <td><label  for="subject">Subject</label></td>
          <td><select  size=70 name="subject" id="subject">
            <option value="" selected="selected"> - Choose -</option>
			<option value="Accessing A Course/Program" >Accessing A Course/Program</option>
			<option value="Accessing A Course/Program - Program Enrollment">&nbsp &nbsp - Program Enrollment</option>
			<option value="Accessing A Course/Program - Course Player">&nbsp &nbsp - Course Player</option>
			<option value="Accessing A Course/Program - Finding a Course in Catalog">&nbsp &nbsp - Finding a Course in Catalog</option>
			<option value="Accessing A Course/Program - Access for SkillSoft course">&nbsp &nbsp - Access for SkillSoft course</option>
			<option value="Accessing A Course/Program - Access for NLCI certification">&nbsp &nbsp - Access for NLCI certification</option>
			
			<option value="Accessing the Calendar">Accessing the Calendar</option>
			<option value="Accessing the Calendar - Time Not Correct">&nbsp &nbsp - Time Not Correct</option>
			<option value="Accessing the Calendar - Sign up for session">&nbsp &nbsp - Sign up for session</option>
			<option value="Accessing the Calendar - Next Course Offering">&nbsp &nbsp - Next Course Offering</option>
			<option value="Accessing the Calendar - Training specific to my location">&nbsp &nbsp - Training specific to my location</option>
			
			<option value="Registering My training">Registering My training</option>
			<option value="Registering My training - Register project specific training">&nbsp &nbsp - Register project specific training</option>
			<option value="Registering My training - Register external training">&nbsp &nbsp - Register external training</option>
			
			<option value="My Training History">My Training History</option>
			<option value="My Training History - Missing Item from History">&nbsp &nbsp - Missing Item from History</option>
			<option value="My Training History - Training Time Not Correct">&nbsp &nbsp - Training Time Not Correct</option>
			
						
			<option value="Application glitches">Application glitches</option>
			<option value="Others">Others</option>


          </select>          </td>
          <td>&nbsp;</td>
        </tr>

        <tr>
          <td valign="top"><label  for="message">Message</label></td>
          <td><textarea  name="message" id="message" class="validate[required]" cols="78" rows="10"></textarea></td>
          <td valign="top">&nbsp;</td>
        </tr>

        <tr>
          <td valign="top">&nbsp;</td>
		 <td align="right" > <input  type="reset" name="button2" id="button2" value="Reset" />
         <input  type="submit" name="button" id="button" value="Submit" onclick="submitform();" />
          
          
          <?=$str?>          <img id="loading" src="img/ajax-load.gif" width="16" height="16" alt="loading" /></td>
        </tr>
      </table>
      </form>
      <?=$success?>
    </div>


</div>

</body>
</html>
