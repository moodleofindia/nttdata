﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
				xmlns:msxsl="urn:schemas-microsoft-com:xslt" 
				xmlns:websoft="http://www.websoft.ru" 
				version="1.0">
<!--
'*	102_pathbuilder#.xsl 
'*	Copyright (c) Websoft, 2006.  All rights reserved.
-->
<xsl:output method="xml" encoding="utf-8" omit-xml-declaration="yes"/>
<xsl:param name="imagesFolder"></xsl:param>
<!--		 Template: Root    -->
<xsl:template match="/">
	<xsl:apply-templates select="params"/>
</xsl:template>
<!--		 Template: Params    --> 
<xsl:template match="params">
<div id="pathbuilderButton">
<input id="pbNextButton" type="button" value="Next question" style="width:150px; height:24px; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:11px; background-color:#00CC66;color: #FFFFFF; font-weight: bold"/>
</div>
</xsl:template>
</xsl:stylesheet>
