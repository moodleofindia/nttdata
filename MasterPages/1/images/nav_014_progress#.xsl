﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:msxsl="urn:schemas-microsoft-com:xslt"
				xmlns:websoft="http://www.websoft.ru"
				version="1.0">
<!--
'*	nav_014_progress#.xsl
'*	Copyright (c) Websoft, 2007.  All rights reserved.
-->
<xsl:output method="xml" encoding="utf-8" omit-xml-declaration="yes"/>
<xsl:param name="objectID"></xsl:param>
<xsl:param name="imagesFolder"></xsl:param>
<xsl:param name="moduleImagesFolder"></xsl:param>
<!--		 Template: Root    -->
<xsl:template match="/">
	<xsl:apply-templates select="params"/>
</xsl:template>
<!--		 Template: Params    -->
<xsl:template match="params">
<div>
	<xsl:attribute name="style">background-color: <xsl:value-of select="bgcolor"/>; border-color: <xsl:value-of select="bordercolor"/>; border-width: <xsl:value-of select="borderwidth"/>; border-style: solid; padding: <xsl:value-of select="padding"/>px;</xsl:attribute>
	<table cellpadding="0" border="0" align="center">
		<xsl:attribute name="cellspacing"><xsl:value-of select="offset"/></xsl:attribute>
		<tr>
			<td>
				<img border="0">
					<xsl:attribute name="src">
						<xsl:if test="standard='yes'"><xsl:value-of select="$imagesFolder"/>icon_visited.gif</xsl:if>
						<xsl:if test="standard='no'"><xsl:value-of select="substring-before($moduleImagesFolder,'images\')"/><xsl:value-of select="translate(n1_img,'/','\')"/></xsl:if>
					</xsl:attribute>
				</img>
			</td>
			<td>
				<img border="0">
					<xsl:attribute name="src">
						<xsl:if test="standard='yes'"><xsl:value-of select="$imagesFolder"/>icon_visited.gif</xsl:if>
						<xsl:if test="standard='no'"><xsl:value-of select="substring-before($moduleImagesFolder,'images\')"/><xsl:value-of select="translate(n1_img,'/','\')"/></xsl:if>
					</xsl:attribute>
				</img>
			</td>
			<td>
				<img border="0">
					<xsl:attribute name="src">
						<xsl:if test="standard='yes'"><xsl:value-of select="$imagesFolder"/>icon_current.gif</xsl:if>
						<xsl:if test="standard='no'"><xsl:value-of select="substring-before($moduleImagesFolder,'images\')"/><xsl:value-of select="translate(n2_img,'/','\')"/></xsl:if>
					</xsl:attribute>
				</img>
			</td>
			<td><img border="0"><xsl:attribute name="src"><xsl:if test="standard='yes'"><xsl:value-of select="$imagesFolder"/>icon_unvisited.gif</xsl:if><xsl:if test="standard='no'"><xsl:value-of select="substring-before($moduleImagesFolder,'images\')"/><xsl:value-of select="translate(n3_img,'/','\')"/></xsl:if></xsl:attribute></img></td>
			<td><img border="0"><xsl:attribute name="src"><xsl:if test="standard='yes'"><xsl:value-of select="$imagesFolder"/>icon_unvisited.gif</xsl:if><xsl:if test="standard='no'"><xsl:value-of select="substring-before($moduleImagesFolder,'images\')"/><xsl:value-of select="translate(n3_img,'/','\')"/></xsl:if></xsl:attribute></img></td>
			<td><img border="0"><xsl:attribute name="src"><xsl:if test="standard='yes'"><xsl:value-of select="$imagesFolder"/>icon_unvisited.gif</xsl:if><xsl:if test="standard='no'"><xsl:value-of select="substring-before($moduleImagesFolder,'images\')"/><xsl:value-of select="translate(n3_img,'/','\')"/></xsl:if></xsl:attribute></img></td>
			<td><img border="0"><xsl:attribute name="src"><xsl:if test="standard='yes'"><xsl:value-of select="$imagesFolder"/>icon_unvisited.gif</xsl:if><xsl:if test="standard='no'"><xsl:value-of select="substring-before($moduleImagesFolder,'images\')"/><xsl:value-of select="translate(n3_img,'/','\')"/></xsl:if></xsl:attribute></img></td>
			<td><img border="0"><xsl:attribute name="src"><xsl:if test="standard='yes'"><xsl:value-of select="$imagesFolder"/>icon_unvisited.gif</xsl:if><xsl:if test="standard='no'"><xsl:value-of select="substring-before($moduleImagesFolder,'images\')"/><xsl:value-of select="translate(n3_img,'/','\')"/></xsl:if></xsl:attribute></img></td>
			<td><img border="0"><xsl:attribute name="src"><xsl:if test="standard='yes'"><xsl:value-of select="$imagesFolder"/>icon_unvisited.gif</xsl:if><xsl:if test="standard='no'"><xsl:value-of select="substring-before($moduleImagesFolder,'images\')"/><xsl:value-of select="translate(n3_img,'/','\')"/></xsl:if></xsl:attribute></img></td>
			<td><img border="0"><xsl:attribute name="src"><xsl:if test="standard='yes'"><xsl:value-of select="$imagesFolder"/>icon_unvisited.gif</xsl:if><xsl:if test="standard='no'"><xsl:value-of select="substring-before($moduleImagesFolder,'images\')"/><xsl:value-of select="translate(n3_img,'/','\')"/></xsl:if></xsl:attribute></img></td>
		</tr>
	</table>
</div>
</xsl:template>

</xsl:stylesheet>
