﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:msxsl="urn:schemas-microsoft-com:xslt"
				xmlns:websoft="http://www.websoft.ru"
				version="1.0">
<!--
'*	102_notifier.xsl
'*	Copyright (c) Websoft, 2007.  All rights reserved.
-->
<xsl:output method="xml" encoding="utf-8" omit-xml-declaration="yes"/>
<xsl:param name="imagesFolder"></xsl:param>
<xsl:param name="objectID"></xsl:param>
<!--		 Template: Root    -->
<xsl:template match="/">
	<xsl:apply-templates select="params"/>
</xsl:template>
<!--		 Template: Params    -->
<xsl:template match="params">

<xsl:if test="appearance='arrow'">
	<img border="0">
		<xsl:attribute name="style">display: none;</xsl:attribute>
		<xsl:attribute name="id"><xsl:value-of select="$objectID"/>_notifier</xsl:attribute>
		<xsl:attribute name="src"><xsl:value-of select="$imagesFolder"/><xsl:value-of select="arrow_color"/>/arrow_<xsl:value-of select="arrow_dir"/>.gif</xsl:attribute>
	</img>
</xsl:if>
<xsl:if test="appearance='area'">
	<table border="0" cellspacing="0" cellpadding="0" width="100%" height="100%">
		<xsl:attribute name="id"><xsl:value-of select="$objectID"/>_notifier</xsl:attribute>
		<xsl:attribute name="style">display: none;</xsl:attribute>
		<tr>
			<td>
				<xsl:attribute name="style">
					background-color: <xsl:value-of select="bgcolor"/>;
					filter:alpha(opacity=<xsl:value-of select="opacity"/>);
					-moz-opacity: 0.<xsl:value-of select="opacity"/>;
					opacity: 0.<xsl:value-of select="opacity"/>;
					<xsl:if test="borderstyle!='none'">
						border-color: <xsl:value-of select="bordercolor"/>;
						border-width: <xsl:value-of select="borderwidth"/>px;
						border-style: <xsl:value-of select="borderstyle"/>;
					</xsl:if>
				</xsl:attribute>
				<br/>
			</td>
	  </tr>
	</table>
</xsl:if>
<xsl:if test="appearance='corners'">
	<table border="0" cellspacing="0" cellpadding="0" width="100%" height="100%">
		<xsl:attribute name="id"><xsl:value-of select="$objectID"/>_notifier</xsl:attribute>
		<xsl:attribute name="style">display: none;</xsl:attribute>
		<tr>
			<td>
				<xsl:attribute name="style">
					border-left-color: <xsl:value-of select="corner_color"/>;
					border-left-width: <xsl:value-of select="corner_width"/>px;
					border-left-style: solid;
					border-top-color: <xsl:value-of select="corner_color"/>;
					border-top-width: <xsl:value-of select="corner_width"/>px;
					border-top-style: solid;
					width: <xsl:value-of select="corner_size"/>px;
					height: <xsl:value-of select="corner_size"/>px;
				</xsl:attribute>
				<img border="0">
					<xsl:attribute name="src"><xsl:value-of select="$imagesFolder"/>1blank.gif</xsl:attribute>
					<xsl:attribute name="width"><xsl:value-of select="corner_size"/></xsl:attribute>
					<xsl:attribute name="height"><xsl:value-of select="corner_size"/></xsl:attribute>
				</img>
			</td>
			<td>
				<xsl:attribute name="style">
					width: 100%;
					height: <xsl:value-of select="corner_size"/>px;
				</xsl:attribute>
				<img border="0">
					<xsl:attribute name="src"><xsl:value-of select="$imagesFolder"/>1blank.gif</xsl:attribute>
					<xsl:attribute name="width"><xsl:value-of select="corner_size"/></xsl:attribute>
					<xsl:attribute name="height"><xsl:value-of select="corner_size"/></xsl:attribute>
				</img>
			</td>
			<td>
				<xsl:attribute name="style">
					border-right-color: <xsl:value-of select="corner_color"/>;
					border-right-width: <xsl:value-of select="corner_width"/>px;
					border-right-style: solid;
					border-top-color: <xsl:value-of select="corner_color"/>;
					border-top-width: <xsl:value-of select="corner_width"/>px;
					border-top-style: solid;
					width: <xsl:value-of select="corner_size"/>px;
					height: <xsl:value-of select="corner_size"/>px;
				</xsl:attribute>
				<img border="0">
					<xsl:attribute name="src"><xsl:value-of select="$imagesFolder"/>1blank.gif</xsl:attribute>
					<xsl:attribute name="width"><xsl:value-of select="corner_size"/></xsl:attribute>
					<xsl:attribute name="height"><xsl:value-of select="corner_size"/></xsl:attribute>
				</img>
			</td>
	  </tr>
		<tr>
			<td>
				<xsl:attribute name="style">
					width: <xsl:value-of select="corner_size"/>px;
				</xsl:attribute>
				<img border="0">
					<xsl:attribute name="src"><xsl:value-of select="$imagesFolder"/>1blank.gif</xsl:attribute>
					<xsl:attribute name="width"><xsl:value-of select="corner_size"/></xsl:attribute>
					<xsl:attribute name="height"><xsl:value-of select="corner_size"/></xsl:attribute>
				</img>
			</td>
			<td>
				<xsl:attribute name="style">
					width: 100%;
					height: 100%;
				</xsl:attribute>
				<img border="0">
					<xsl:attribute name="src"><xsl:value-of select="$imagesFolder"/>1blank.gif</xsl:attribute>
					<xsl:attribute name="width">1</xsl:attribute>
					<xsl:attribute name="height">1</xsl:attribute>
				</img>
			</td>
			<td>
				<xsl:attribute name="style">
					width: <xsl:value-of select="corner_size"/>px;
				</xsl:attribute>
				<img border="0">
					<xsl:attribute name="src"><xsl:value-of select="$imagesFolder"/>1blank.gif</xsl:attribute>
					<xsl:attribute name="width"><xsl:value-of select="corner_size"/></xsl:attribute>
					<xsl:attribute name="height"><xsl:value-of select="corner_size"/></xsl:attribute>
				</img>
			</td>
	  </tr>
		<tr>
			<td>
				<xsl:attribute name="style">
					border-left-color: <xsl:value-of select="corner_color"/>;
					border-left-width: <xsl:value-of select="corner_width"/>px;
					border-left-style: solid;
					border-bottom-color: <xsl:value-of select="corner_color"/>;
					border-bottom-width: <xsl:value-of select="corner_width"/>px;
					border-bottom-style: solid;
					width: <xsl:value-of select="corner_size"/>px;
					height: <xsl:value-of select="corner_size"/>px;
				</xsl:attribute>
				<img border="0">
					<xsl:attribute name="src"><xsl:value-of select="$imagesFolder"/>1blank.gif</xsl:attribute>
					<xsl:attribute name="width"><xsl:value-of select="corner_size"/></xsl:attribute>
					<xsl:attribute name="height"><xsl:value-of select="corner_size"/></xsl:attribute>
				</img>
			</td>
			<td>
				<xsl:attribute name="style">
					width: 100%;
					height: <xsl:value-of select="corner_size"/>px;
				</xsl:attribute>
				<img border="0">
					<xsl:attribute name="src"><xsl:value-of select="$imagesFolder"/>1blank.gif</xsl:attribute>
					<xsl:attribute name="width"><xsl:value-of select="corner_size"/></xsl:attribute>
					<xsl:attribute name="height"><xsl:value-of select="corner_size"/></xsl:attribute>
				</img>
			</td>
			<td>
				<xsl:attribute name="style">
					border-right-color: <xsl:value-of select="corner_color"/>;
					border-right-width: <xsl:value-of select="corner_width"/>px;
					border-right-style: solid;
					border-bottom-color: <xsl:value-of select="corner_color"/>;
					border-bottom-width: <xsl:value-of select="corner_width"/>px;
					border-bottom-style: solid;
					width: <xsl:value-of select="corner_size"/>px;
					height: <xsl:value-of select="corner_size"/>px;
				</xsl:attribute>
				<img border="0">
					<xsl:attribute name="src"><xsl:value-of select="$imagesFolder"/>1blank.gif</xsl:attribute>
					<xsl:attribute name="width"><xsl:value-of select="corner_size"/></xsl:attribute>
					<xsl:attribute name="height"><xsl:value-of select="corner_size"/></xsl:attribute>
				</img>
			</td>
	  </tr>
	</table>
</xsl:if>

</xsl:template>
</xsl:stylesheet>
