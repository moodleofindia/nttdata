<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:msxsl="urn:schemas-microsoft-com:xslt"
				xmlns:websoft="http://www.websoft.ru"
				version="1.0">
<!--
'*	splash_description#.xsl
'*	Copyright (c) Websoft, 2007.  All rights reserved.
-->
<xsl:output method="xml" encoding="utf-8" omit-xml-declaration="yes"/>
<xsl:param name="objectID"></xsl:param>
<xsl:param name="imagesFolder"></xsl:param>
<!--		 Template: Root    -->
<xsl:template match="/">
	<xsl:apply-templates select="params"/>
</xsl:template>
<!--		 Template: Params    -->
<xsl:template match="params">
<div style="display:inline; z-index:100; cursor: hand; cursor: pointer;">
	<xsl:attribute name="id"><xsl:value-of select="$objectID"/>_help_en</xsl:attribute>
	<img border="0">
		<xsl:attribute name="id"><xsl:value-of select="$objectID"/>_himg_en</xsl:attribute>
		<xsl:attribute name="alt"><xsl:value-of select="alt_open"/></xsl:attribute>
		<xsl:attribute name="src">
			<xsl:if test="standard='yes'"><xsl:value-of select="$imagesFolder"/>button_descr1.gif</xsl:if>
			<xsl:if test="standard='no'"><xsl:value-of select="n1_img"/></xsl:if>
		</xsl:attribute>
		<xsl:attribute name="onclick">
			var oPopup = document.getElementById('<xsl:value-of select="$objectID"/>'+'_popup');
			var oParent = document.getElementById('boardFrame');
			if(oPopup==null) {
				var oMatrix = document.getElementById('<xsl:value-of select="$objectID"/>'+'_matrix');
				oPopup = oMatrix.cloneNode(true);
				oPopup.setAttribute('id','<xsl:value-of select="$objectID"/>'+'_popup');
				oParent.appendChild(oPopup);
				oPopup.style.display = 'inline';
				oPopup.style.top = <xsl:value-of select="taby"/>+'px';
				oPopup.style.left = <xsl:value-of select="tabx"/>+'px';
				oPopup.style.zIndex = 900;
			} else {
				if(oPopup.style.display!='none') {
					oPopup.style.display = 'none';
					return false;
				} else {
					oPopup.style.display = 'inline';
				}
			}
			if('<xsl:value-of select="onewin"/>'=='yes') {
				var aoDivs = oParent.getElementsByTagName('div');
				for(var i=0;i&lt;aoDivs.length;i++) {
					if(aoDivs[i].getAttribute('popup')=='1') {
						if(aoDivs[i]==oPopup) continue;
						if(aoDivs[i].style.display!='none') aoDivs[i].style.display = 'none';
					}
				}
			}
			return false;
		</xsl:attribute>
	</img>
</div>
<xsl:variable name="bcolor">
	<xsl:if test="standardcolor='yes'">#4F82B2</xsl:if>
	<xsl:if test="standardcolor='no'"><xsl:value-of select="bordercolor"/></xsl:if>
</xsl:variable>
<div>
	<xsl:attribute name="popup">1</xsl:attribute>
	<xsl:attribute name="id"><xsl:value-of select="$objectID"/>_matrix</xsl:attribute>
	<xsl:attribute name="style">position:absolute; z-index: 900; top: <xsl:value-of select="taby"/>px; left: <xsl:value-of select="tabx"/>px; display:none; visibility:visible; width: <xsl:value-of select="tabwidth"/>px; height: <xsl:value-of select="tabheight"/>px;</xsl:attribute>
	<table cellspacing="0" cellpadding="10">
		<xsl:attribute name="style">width: <xsl:value-of select="tabwidth"/>px; height: <xsl:value-of select="tabheight"/>px; border: <xsl:value-of select="$bcolor"/> solid 2px;</xsl:attribute>
		<tr>
			<td width="90%" bgcolor="#FFFFFF" valign="top" align="center">
				<xsl:attribute name="id"><xsl:value-of select="$objectID"/>_header</xsl:attribute>
				<xsl:value-of select="tabheader" disable-output-escaping="yes"/>
			</td>
			<td bgcolor="#FFFFFF" valign="top" align="right">
				<img border="0">
					<xsl:attribute name="id"><xsl:value-of select="$objectID"/>_desr_close</xsl:attribute>
					<xsl:attribute name="alt"><xsl:value-of select="alt_close"/></xsl:attribute>
					<xsl:attribute name="src">
						<xsl:if test="standard='no'"><xsl:value-of select="n2_img"/></xsl:if>
						<xsl:if test="standard='yes'"><xsl:value-of select="$imagesFolder"/>close_window.gif</xsl:if>
					</xsl:attribute>
					<xsl:attribute name="onclick">
						var oPopup = document.getElementById('<xsl:value-of select="$objectID"/>'+'_popup');
						oPopup.style.display = 'none';
						return false;
					</xsl:attribute>
					<xsl:attribute name="style">cursor:hand; cursor:pointer</xsl:attribute>
				</img>
			</td>
		</tr>
		<tr>
			<td bgcolor="#FFFFFF" valign="top" colspan="2">
				<div>
					<xsl:attribute name="style">overflow-y: scroll; overflow: -moz-scrollbars-vertical; width:100%; background-color:#FFFFFF; height: <xsl:value-of select="string(number(tabheight)-65)"/>px; </xsl:attribute>
					<xsl:value-of select="tabtext" disable-output-escaping="yes"/>
				</div>
			</td>
		</tr>
	</table>
</div>
</xsl:template>
</xsl:stylesheet>
