﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
				xmlns:msxsl="urn:schemas-microsoft-com:xslt" 
				xmlns:websoft="http://www.websoft.ru" 
				version="1.0">
<!--
'*	101_recorder#.xsl
'*	Copyright (c) Websoft, 2006.  All rights reserved.
-->
<xsl:output method="xml" encoding="utf-8" omit-xml-declaration="yes"/>
<xsl:param name="objectID"></xsl:param>

<!--		 Template: Root    -->
<xsl:template match="/">
	<xsl:apply-templates select="params"/>
</xsl:template>
<!--		 Template: Params    --> 
<xsl:template match="params">
<div style="display:none;">
<XML id="testData">
	<tdata>
		<test>
		<xsl:attribute name="container"><xsl:value-of select="$objectID"/></xsl:attribute>
		<xsl:attribute name="id"><xsl:value-of select="tid"/></xsl:attribute>
		<xsl:attribute name="chapter"><xsl:value-of select="chapter"/></xsl:attribute>
		<xsl:attribute name="topic"><xsl:value-of select="topic"/></xsl:attribute>
		<xsl:if test="group=''"><xsl:attribute name="group">nogroup</xsl:attribute></xsl:if>
		<xsl:if test="group!=''"><xsl:attribute name="group"><xsl:value-of select="group"/></xsl:attribute></xsl:if>
		<xsl:attribute name="mandatory"><xsl:value-of select="mandatory"/></xsl:attribute>
			<scales>
				<xsl:for-each select="scales/item">
					<scale>
						<xsl:attribute name="ttype"><xsl:value-of select="ttype/text()"/></xsl:attribute>
					</scale>
				</xsl:for-each>
			</scales>
			<scoring>
				<xsl:if test="scored='no'">
					<xsl:attribute name="scored">no</xsl:attribute>
				</xsl:if>
				<xsl:if test="scored!='no'">
					<xsl:attribute name="scored">yes</xsl:attribute>
					<scoreboards>
						<xsl:for-each select="scoreboards/item">
							<scoreboard>
								<xsl:attribute name="id"><xsl:value-of select="scoreboard/text()"/></xsl:attribute>
								<xsl:attribute name="maxscore"><xsl:value-of select="maxscore/text()"/></xsl:attribute>
							</scoreboard>
						</xsl:for-each>
					</scoreboards>
				</xsl:if>
			</scoring>
			<scenario>			
				<xsl:for-each select="scenario/item">
					<action>
						<xsl:attribute name="num"><xsl:value-of select="position()"/></xsl:attribute>
						<xsl:attribute name="id"><xsl:value-of select="stepID/text()"/></xsl:attribute>
						<xsl:if test="timed/text()='yes'">
							<xsl:attribute name="timed">yes</xsl:attribute>
							<xsl:attribute name="timer"><xsl:value-of select="steptimer/text()"/></xsl:attribute>
						</xsl:if>
						<xsl:if test="timed/text()!='yes'">
							<xsl:attribute name="timed">no</xsl:attribute>
						</xsl:if>
						<xsl:attribute name="attempts"><xsl:value-of select="attempts/text()"/></xsl:attribute>
						<xsl:if test="skip/text()='yes'">
							<xsl:attribute name="skip">yes</xsl:attribute>
						</xsl:if>
						<xsl:for-each select="variants/item">
							<variant>
							<xsl:attribute name="num"><xsl:value-of select="position()"/></xsl:attribute>
							<xsl:attribute name="id"><xsl:value-of select="varID/text()"/></xsl:attribute>
							<xsl:attribute name="nextstepid"><xsl:value-of select="next_step/text()"/></xsl:attribute>
							<xsl:attribute name="nextvarid"><xsl:value-of select="next_var/text()"/></xsl:attribute>
							<xsl:attribute name="src"><xsl:value-of select="src/text()"/></xsl:attribute>
							<xsl:if test="checkforevent/text()='yes'">
								<xsl:attribute name="event"><xsl:value-of select="event/text()"/></xsl:attribute>
							</xsl:if>
							<xsl:if test="checkforinput/text()='yes'">
								<checkstring>
									<xsl:if test="case/text()='yes'">
										<xsl:attribute name="case">yes</xsl:attribute>
									</xsl:if>
									<xsl:value-of select="src_string/text()"/>
								</checkstring>
							</xsl:if>
							</variant>
						</xsl:for-each>
					</action>
				</xsl:for-each>
			</scenario>		
		</test>
	</tdata>
</XML>
</div>
</xsl:template>
</xsl:stylesheet>
