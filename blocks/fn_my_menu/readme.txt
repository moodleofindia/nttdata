Block My Menu

The block is made for Moodle 1.9.x


NOTE: This block was originally adapted from the Course Menu block, which was created by Sandor Lukacs, NetSapiensis AB (sandor.lukacs@netsapiensis.se).

############################################################################################################

How it works:

The following functions may be toggled on/off via the block settings (accessed via the block
header):

 o my Courses: Lists all courses that the user is enrolled
 o myMessenger: Indicates unread messages. Links to Messenger.
 o myFiles: Personal storage area - this feature is currently not working (do not use)
 o myProfile: Links to user profile
 o myBlog: Link to user blog


Site level configuration (Site Admin > Blocks > FN myMenu) defines the site-wide default
settings which can be changed at the course level (i.e. courses can have their own setup
option)



############################################################################################################

Installation:


This block follows standard Moodle Block install instructions 

-copy the fn_my_menu folder in the into your moodle blocks folder
-copy the files in lang folder into your moodle lang folder.
-visit the Admin page in Moodle to activate it



#############################################################################################################

Notes:

This block is part of the MoodleFN series (www.moodlefn.knet.ca)

Sponsor: K-Net (www.knet.ca)
Designer: Fernando Oliveira (fernandooliveira@knet.ca)
Coder: Mike Churchward (www.oktech.ca)



