<?php

class block_fn_my_menu extends block_base {

    var $tot;
    var $id;

    function init() {
        global $CFG;

        $this->title = get_string('blockname','block_fn_my_menu');
        $this->content_type = BLOCK_TYPE_TEXT;
        $this->version = 2007120100;

        if (!isset($CFG->block_fn_my_menu_mycoursesdef)) {
            $CFG->block_fn_my_menu_mycoursesdef = 1;
        }
        if (!isset($CFG->block_fn_my_menu_messagesdef)) {
            $CFG->block_fn_my_menu_messagesdef = 1;
        }
        if (!isset($CFG->block_fn_my_menu_profiledef)) {
            $CFG->block_fn_my_menu_profiledef = 1;
        }
        if (!isset($CFG->block_fn_my_menu_blogmenudef)) {
            $CFG->block_fn_my_menu_blogmenudef = 1;
        }

        if (!isset($CFG->block_fn_my_menu_myfilesdef)) {
            $CFG->block_fn_my_menu_myfilesdef = 0;
        }
        if (!isset($CFG->block_fn_my_menu_id0)) {
            $CFG->block_fn_my_menu_id0 = 'mycourses';
            $CFG->block_fn_my_menu_id1 = 'messages';
            $CFG->block_fn_my_menu_id2 = 'profile';
            $CFG->block_fn_my_menu_id3 = 'blogmenu';
            $CFG->block_fn_my_menu_id4 = 'myfiles';
        }
        if (!isset($CFG->block_fn_my_menu_maxbytes)) {
            $CFG->block_fn_my_menu_maxbytes = get_max_upload_file_size($CFG->maxbytes);
        }
    }

    function specialization() {
        global $CFG;

        /// Set up the display title.
        if (!empty($this->config->displaytitle)) {
            $this->title = $this->config->displaytitle;
        } else {
            $this->title = get_string('displaytitle', 'block_fn_my_menu');
        }

        if (!empty($this->instance->pageid)) {
            $this->context = get_context_instance(CONTEXT_COURSE, $this->instance->pageid);
        }
        if (empty($this->context)) {
            $this->context = get_context_instance(CONTEXT_COURSE, SITEID);
        }

        $this->showadmin = has_capability('moodle/course:update', $this->context);
    }

    function instance_allow_config() {
        return true;
    }

    function instance_config_print() {
        global $CFG;

        // $this->config->idK = k-th entry in the tree
        // to simplify cod writing we'll use an array
        $id = array();
        $needinit = false;
        for ($i=0; $i<5; $i++) {
        /// If any config item is not set, reinitialize to default so they can be set.
            if (empty($this->config->{'id'.$i})) {
                $needinit = true;
                break;
            } else {
                $id[$i] = $this->config->{'id'.$i};
            }
        }
        if ($needinit) {
            $id[0] = $this->config->id0 = $CFG->block_fn_my_menu_id0;
            $id[1] = $this->config->id1 = $CFG->block_fn_my_menu_id1;
            $id[2] = $this->config->id2 = $CFG->block_fn_my_menu_id2;
            $id[3] = $this->config->id3 = $CFG->block_fn_my_menu_id3;
            $id[4] = $this->config->id4 = $CFG->block_fn_my_menu_id4;
        }

        foreach ($id as $name) {
            if (!isset($this->config->{$name})) {
                $this->config->{$name} = ($CFG->{'block_fn_my_menu_'.$name.'def'} ? 'show' : 'hide');
            }
        }

    	$this->course = get_record('course', 'id', $this->instance->pageid);
        global $CFG;
	    $sectiongroup = $this->course->format;
	    if (empty($CFG->block_fn_my_menu_groupsections)) {
	        $sectiongroup = "tree";
	    }

        // eliminate the ones that are not used
        $tot = 5;
        for ($i = 0; $i < $tot; $i++) {
            if (($id[$i] == "myprofile")&&(isguest())) {
                for ($j = $i; $j < 4; $j++) {
                    $id[$j] = $id[$j+1];
                }
                $id[4] = "myprofile";
                $i--;
                $tot--;
            }
        }
        $this->tot = $tot;
        $this->id  = $id;

	    if (!$this->instance_allow_multiple() && !$this->instance_allow_config()) {
            return false;
        }
        global $CFG;
        if (is_file($CFG->dirroot .'/blocks/'. $this->name() .'/config_instance.html')) {
            print_simple_box_start('center', '', '', 5, 'blockconfiginstance');
            include($CFG->dirroot .'/blocks/'. $this->name() .'/config_instance.html');
            print_simple_box_end();
        } else {
            notice(get_string('blockconfigbad'), str_replace('blockaction=', 'dummy=', qualified_me()));
        }

        return true;
    }

    function has_config() {
        return true;
    }

    function config_print() {
        global $CFG, $THEME;

        $id = array();
        $needinit = false;
        for ($i=0; $i<5; $i++) {
            $id[$i] = $CFG->{'block_fn_my_menu_id'.$i};
        }

        $this->tot = 5;
        $this->id  = $id;

        return parent::config_print();
    }

    function handle_config($config) {
        foreach ($config as $name => $value) {
            set_config($name, $value);
        }
        return true;
    }

    function get_content() {
        global $USER, $CFG, $THEME, $SITE;
        require_once($CFG->dirroot.'/mod/forum/lib.php');
	    require_once($CFG->dirroot.'/course/lib.php');
		
		$this->course = get_record('course', 'id', $this->instance->pageid);
		$currentcourseid = $this->course->id;
		$record = get_records_sql("SELECT ra.id FROM lms.mdl_role_assignments ra
									JOIN mdl_role r ON r.id = ra.roleid
									JOIN mdl_context c ON ra.contextid = c.id
									JOIN mdl_course co ON c.instanceid = co.id
									where ra.roleid=5 and ra.userid=$USER->id and co.id=$currentcourseid");
		if (!$record)
		{
			return false;
		}
		
		
        if (!isloggedin() || isguest()) {
            return false;
        }

        $this->course = get_record('course', 'id', $this->instance->pageid);

        if($this->content !== NULL) {
            return $this->content;
        }

        if ($this->course->format == 'topics') {
            $format = 'topic';
        }
        else {
            $format = 'week';
        }

        $this->content = new stdClass;
        $this->content->text = '';
        $this->content->footer = '';

        if (!empty($this->message)) {
            if ($this->showadmin) {
                $this->content->text = $this->message;
            }
            return $this->content;
        }

        /// Add the treemenu lib.
        if (!class_exists('HTML_TreeMenu')) {
            require_once($CFG->dirroot.'/blocks/fn_my_menu/HTML_TreeMenu-1.2.0/TreeMenu.php');
            require_js($CFG->wwwroot.'/blocks/fn_my_menu/HTML_TreeMenu-1.2.0/TreeMenu.js');
        }

        //start the tree

        /// Prefix the pix directory relative to where the javascript file is physically located
        /// (only relative will work; trust me)
        $reldir = '../../../../';
        $nicon      = $reldir.'blocks/fn_my_menu/icons/folder.gif';
        $eicon      = $reldir.'blocks/fn_my_menu/icons/folder-expanded.gif';

        $this->menu = new HTML_TreeMenu();

        if (file_exists($CFG->dirroot.'/theme/FNmain/pix/breadcrumb.gif')) {
            $homeicon = $reldir.'theme/FNmain/pix/breadcrumb.gif';
            $homeoicon = $reldir.'theme/FNmain/pix/breadcrumb.gif';
        } else {
            $homeicon = $reldir.'blocks/fn_my_menu/icons/home.gif';
            $homeoicon = $reldir.'blocks/fn_my_menu/icons/home.gif';
        }
        $cssclass = 'treeMenuDefault';

        $mnode = new HTML_TreeNode(array('text' => $SITE->shortname, 'link' => $CFG->wwwroot, 'icon' => $homeicon, 'isDynamic' => false,
                                   'cssClass' => $cssclass, 'expandedIcon' => $homeoicon, 'expanded' => true, 'width' => 16, 'height' => 16));

        //print sections

        $id = array();
        $id[0] = isset($this->config->id0) ? $this->config->id0 : $CFG->block_fn_my_menu_id0;
        $id[1] = isset($this->config->id1) ? $this->config->id1 : $CFG->block_fn_my_menu_id1;
        $id[2] = isset($this->config->id2) ? $this->config->id2 : $CFG->block_fn_my_menu_id2;
        $id[3] = isset($this->config->id3) ? $this->config->id3 : $CFG->block_fn_my_menu_id3;
        $id[4] = isset($this->config->id4) ? $this->config->id4 : $CFG->block_fn_my_menu_id4;

        foreach ($id as $i => $name) {
            if (!isset($this->config->{$name})) {
                $this->config->{$name} = ($CFG->{'block_fn_my_menu_'.$name.'def'} ? 'show' : 'hide');
            }
        }

        $grouppic = $reldir.'pix/i/group.gif';

        // add them to the tree in the order from configuration
        for ($i = 0; $i <= 4; $i++) {

            if (($id[$i] == "mycourses") && !empty($this->config->mycourses) && ($this->config->mycourses == "show")) {
                if (!$courses = get_my_courses($USER->id, NULL, 'id,shortname,format,visible')) {
                    $courses = array();
                }
                $label = !empty($CFG->block_fn_my_menu_mycoursesname) ? $CFG->block_fn_my_menu_mycoursesname : get_string('mycourses', 'block_fn_my_menu');
                $cnode = &$mnode->addItem(new HTML_TreeNode(array('text' => ' '.$label, 'link' => '',
                                                                  'icon' => $nicon, 'expandedIcon' => $eicon, 'width' => 16, 'height' => 16)));
                $curl = $CFG->wwwroot.'/course/view.php?id=';
                foreach ($courses as $course) {
                    if ((empty($CFG->block_fn_my_menu_courselink) ||
                        ($CFG->block_fn_my_menu_courselink != $course->id))) {
                        if ($this->course->id == $course->id) {
                            $class = ' fnmymenu-coursesel';
                        } else {
                            $class = 'fnmymenu-course';
                        }
                        $cname = '<span class="'.$class.'"> '.$course->shortname.'</span>';
                        $cnode->addItem(new HTML_TreeNode(array('text' => $cname, 'link' => $curl.$course->id,
                                          'icon' => $reldir.'blocks/fn_my_menu/icons/courseact.gif',
                                          'expandedIcon' => $reldir.'blocks/fn_my_menu/icons/courseact.gif',
                                          'cssClass' => $cssclass, 'width' => 16, 'height' => 16)));
                    }
                }
            }

	        if ($id[$i] == "messages") {
			    if (!empty($this->config->messages) && $this->config->messages=="show") {
                    if ($nummess = $this->count_unread_messages($USER->id)) {
                        $nummess = ' ('.$nummess.')';
                        $icon = "$reldir/blocks/fn_my_menu/icons/messages_blink.gif";
                    } else {
                        $nummess = '';
                        $icon = "$reldir/blocks/fn_my_menu/icons/messages.gif";
                    }
                    $link = "\" onclick=\"return openpopup(\'/message/index.php?course={$this->course->id}\', \'message\', \'menubar=0,location=0,scrollbars,status,resizable,width=400,height=500\', 0);\"";
                    $mnode->addItem(new HTML_TreeNode(array('text' => ' '.get_string('messages', 'block_fn_my_menu').$nummess,
                                                            'link' => $link,
                                                            'icon' => $icon,
                                                            'expandedIcon' => $icon,
                                                            'cssClass' => $cssclass, 'width' => 16, 'height' => 16)));
                }
            }

	        if ($id[$i] == "profile") {
				if (!isguest()) {
		            if (!empty($this->config->profile) && $this->config->profile=="show") {
                        if ($USER->picture) {
                            $uicon = $reldir.'user/pix.php/'.$USER->id.'/f2.jpg';
                        } else {
                            $uicon = $reldir.'pix/u/f2.png';
                        }
                        $mnode->addItem(new HTML_TreeNode(array('text' => ' '.get_string('profile','block_fn_my_menu'),
                                                                'link' => $CFG->wwwroot.'/user/view.php?id='.$USER->id.'&course='.$this->course->id,
                                                                'icon' => $uicon, 'expandedIcon' => $uicon,
                                                                'cssClass' => $cssclass, 'width' => 16, 'height' => 16)));
                    }
		        }
            }

            if ($id[$i] == "blogmenu") {
                if (!empty($this->config->blogmenu) && $this->config->blogmenu=="show") {
                    $mnode->addItem(new HTML_TreeNode(array('text' => ' '.get_string('blogmenu', 'block_fn_my_menu'),
                                                            'link' => $CFG->wwwroot.'/blog/index.php?userid='.$USER->id.'&courseid=1',
                                                            'icon' => $reldir.'blocks/fn_my_menu/icons/blog.gif',
                                                            'expandedIcon' => $reldir.'blocks/fn_my_menu/icons/blog.gif',
                                                            'cssClass' => $cssclass, 'width' => 16, 'height' => 16)));
                }
            }

            if ($id[$i] == "myfiles") {
                if (!empty($this->config->myfiles) && $this->config->myfiles == "show") {
                    /// Add My Files
                    $mnode->addItem(new HTML_TreeNode(array('text' => ' My Files <span style="font-size: 75%;">(n.a.)</span>',
//                                                            'link' => $CFG->wwwroot.'/userfiles/index.php?id='.$USER->id,
                                                            'link' => '',
                                                            'icon' => $reldir.'pix/f/explore.gif',
                                                            'expandedIcon' => $reldir.'pix/f/explore.gif',
                                                            'cssClass' => $cssclass, 'width' => 16, 'height' => 16)));
                }
            }
        }

        $this->menu->addItem($mnode);
        $treeMenu = &new HTML_TreeMenu_DHTML($this->menu, array('images' => $CFG->wwwroot.'/blocks/fn_my_menu/HTML_TreeMenu-1.2.0/images',
                                                          'defaultClass' => 'treeMenuDefault'));

        $this->content->text = $treeMenu->toHTML();

        return $this->content;
    }

    function get_scriptless_content() {
        $this->course = get_record('course', 'id', $this->instance->pageid);
        global $CFG;
        require_once($CFG->dirroot.'/blocks/fn_my_menu/xtree/xtree.php');

        $sections = get_all_sections($this->course->id);
        if ($this->course->format == 'topics') {
            $format = 'topic';

        }
        else {
            $format = 'week';
        }

        $text = '<noscript>';
        $text .= '<table align="center" width="100%">';

        $text .='<tr class="webfx-tree-item"><td align="center"><img src="'.$CFG->wwwroot.'/blocks/fn_my_menu/icons/configure.gif" alt="Control Panel" /></td>';
        $text .='<td valign="top"><a href="'.$CFG->wwwroot.'/blocks/fn_my_menu/controls/controls.php?id='.$this->course->id.'">Control Panel</a></td></tr>';
        if (isediting($this->course->id)) {
            $text .='<tr class="webfx-tree-item"><td align="center"><img src="'.$CFG->pixpath.'/i/edit.gif" alt="'.get_string('turneditingoff').'" /></td>';
            $text .='<td valign="top"><a href="view.php?id='.$this->course->id.'&amp;edit=off">'.get_string('turneditingoff').'</a></td></tr>';
        }
        else {
            $text .='<tr class="webfx-tree-item"><td align="center"><img src="'.$CFG->pixpath.'/i/edit.gif" alt="'.get_string('turneditingon').'" /></td>';
            $text .='<td valign="top"><a href="view.php?id='.$this->course->id.'&amp;edit=on">'.get_string('turneditingon').'</a></td></tr>';
        }
        if ($this->course->showgrades) {
            $text .='<tr class="webfx-tree-item"><td align="center"><img src="'.$CFG->pixpath.'/i/grades.gif" alt="'.get_string('gradebook','grades').'" /></td>';
            $text .='<td valign="top"><a href="$CFG->wwwroot/grade/index.php?id='.$this->course->id.'">'.get_string('gradebook','grades').'</a></td></tr>';

        }


        if (!empty($sections)) {
            foreach($sections as $section) {
                if ($section->visible && $section->section > 0 && $section->section <= $this->course->numsections) {
                    $summary = truncate_description($section->summary);
                    if (empty($summary)) {
                      $summary = get_string("name{$this->course->format}").' '.$section->section;
                    }
                    $text .='<tr class="webfx-tree-item"><td align="center"><img src="../blocks/fn_my_menu/icons/file.gif" alt="'.str_replace('"','&quot;',$summary).'" /></td>';
                    $text .='<td valign="top"><a href="'.$CFG->wwwroot.'/course/view.php?id='.$this->course->id.'&'.$format.'='.$section->section.'">'.$summary.'</a></td></tr>';
                }
            }
        }
        // output a link to the calendar
        $text .='<tr class="webfx-tree-item"><td align="center"><img src="../blocks/fn_my_menu/icons/cal.gif" alt="'.get_string('calendar','calendar').'" /></td>';
        $text .='<td valign="top"><a href="'.$CFG->wwwroot.'/calendar/view.php?view=upcoming&amp;course='.$this->course->id.'">'.get_string('calendar', 'calendar').'</a></td></tr>';
        // output a link to show all topics/weeks
        $text .='<tr class="webfx-tree-item"><td align="center"><img src="../blocks/fn_my_menu/icons/viewall.gif" alt="'.get_string('showallsections','block_fn_my_menu').'" /></td>';
        $text .='<td><a href="'.$CFG->wwwroot.'/course/view.php?id='.$this->course->id.'&'.$format.'=all" alt="'.get_string("showallsections",'block_fn_my_menu').'">'.get_string("showallsections",'block_fn_my_menu').'</a></td></tr></table>';
        $text .= '</noscript>';
        return $text;
    }

	// truncates the description to fit within the given $max_size. Splitting on tags and \n's where possible
	// @param $string: string to truncate
	// @param $max_size: length of largest piece when done
	// @param $trunc: string to append to truncated pieces
	function truncate_description($string, $max_size=20, $trunc = '...') {
	    $split_tags = array('<br>','<BR>','<Br>','<bR>','</dt>','</dT>','</Dt>','</DT>','</p>','</P>', '<BR />', '<br />', '<bR />', '<Br />');
	    $temp = $string;

	    foreach($split_tags as $tag) {
	    	list($temp) = explode($tag, $temp, 2);
	    }
	    $rstring = strip_tags($temp);

	    $rstring = html_entity_decode($rstring);

	    if (strlen($rstring) > $max_size) {
	        $rstring = chunk_split($rstring, ($max_size-strlen($trunc)), "\n");
	        $temp = explode("\n", $rstring);
	        // catches new lines at the beginning
	        if (trim($temp[0]) != '') {
	            $rstring = trim($temp[0]).$trunc;
	        }
	        else {
	           $rstring = trim($temp[1]).$trunc;
	        }
	    }
	    if (strlen($rstring) > $max_size) {
	        $rstring = substr($rstring, 0, ($max_size - strlen($trunc))).$trunc;
	    }
	    elseif($rstring == '') {
	        // we chopped everything off... lets fall back to a failsafe but harsher truncation
	        $rstring = substr(trim(strip_tags($string)),0,($max_size - strlen($trunc))).$trunc;
	    }

	    // single quotes need escaping
	    return str_replace("'", "\\'", $rstring);
	}

	function xtree_format($string){

	    $newstring = str_replace(chr(13),' ',str_replace(chr(10),' ',$string));
	    return $newstring;
	}

    function count_unread_messages($userid=0) {
        global $CFG, $USER;

        if ($userid == 0) {
            $userid = $USER->id;
        }

        return count_records_sql("SELECT COUNT(m.useridfrom) as count
                                  FROM {$CFG->prefix}message m
                                  WHERE m.useridto = '$userid'");
    }
}

?>