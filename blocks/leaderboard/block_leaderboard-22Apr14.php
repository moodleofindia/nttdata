<?php


   require_once($CFG->dirroot.'/mod/assignment/lib.php');

   class block_leaderboard extends block_base {

      function init () {
         global $CFG;
	 $this->title = get_string('blockname', 'block_leaderboard').' | '.date('M Y');
	 $this->version = 2007031700;
	 $this->config->maxchars = 12;

	 $this->config_save($this->config);
      } // function init()

      function has_config() {
         return true;
      } // function has_config()

      function get_content() {

         global $db, $COURSE,$USER, $CFG, $config;

         if ($this->content !== NULL) {
	    return $this->content;
         }
	  
	 $this->content = new stdClass;

	 // get courses of the current user
	 $courses = get_my_courses($USER->id);
         if ((empty($courses) || !is_array($courses) || count($courses) == 0)) {
            $this->content->text .= get_string('no courses', 'block_leaderboard');
         } else {

            // get assignments of the current user in his courses
			if ($COURSE->id >1)
			{
			$Teammembers = get_records_sql("SELECT u.id, u.username,concat(u.firstname,' ',u.lastname) as name,sum(p.point) As points FROM mdl_game_point p join mdl_user u on p.userid=u.id and p.course=$COURSE->id and MONTH(FROM_UNIXTIME(timeissued)) = MONTH(CURRENT_DATE()) and YEAR(FROM_UNIXTIME(timeissued)) = YEAR(CURRENT_DATE()) group by u.id order by points desc limit 5");
			}
			else
			{
			$Teammembers = get_records_sql("SELECT u.id, u.username,concat(u.firstname,' ',u.lastname) as name,sum(p.point) As points FROM mdl_game_point p join mdl_user u on p.userid=u.id and u.grade=$USER->grade and MONTH(FROM_UNIXTIME(timeissued)) = MONTH(CURRENT_DATE()) and YEAR(FROM_UNIXTIME(timeissued)) = YEAR(CURRENT_DATE()) group by u.id order by points desc limit 5");
			}
			
			
			$members = array();
			if ($Teammembers) {
			foreach($Teammembers as $record) {
                $members[] = $record;

                continue;
				}
			}
			else
			{
			$this->content->text .= '<table border="0" cellspacing="2" cellpadding="0" >';
			$this->content->text .= '<tr style="font-size:12px;padding:1px">
			 <td align="left"  >There are yet to score points.</td></tr>';
			$this->content->text .= '</table>';
			$this->content->footer = '';
			}



$this->content->text .= '<table border="0" cellspacing="2" cellpadding="0" >';
		foreach ($Teammembers as $member) {
	
             $this->content->text .= '<tr style="font-size:12px;padding:1px">
				<td align="left" width="20px" ><a href="'.$CFG->wwwroot.'/user/view.php?id='.$member->id.'&amp;course='.$COURSE->id.'"><img src="'.$CFG->wwwroot.'/user/pix.php?file=/'.$member->id.'/f1.jpg" width="35px" height="30px" title="' .$member->name.'" alt="'.$member->name.'" /></a>&nbsp</td>
                <td align="left" width="100px" > '.$member->name.'&nbsp</td>';
				if($member->points<500)
				{
				$this->content->text .= '<td align="right" ><font size="1"><img src="'.$wwwroot.'\blocks\leaderboard\level1.jpg" height="25" width="25"><br/> ' .$member->points. ' pts </font></td>';
				}
				else if(($member->points<1000)&&($member->points>500))
				{
				$this->content->text .= '<td align="right" ><font size="1"><img src="'.$wwwroot.'\blocks\leaderboard\level2.jpg" height="25" width="25"><br/> ' .$member->points. ' pts </font></td>';
				}
				else if(($member->points<3000)&&($member->points>1000))
				{
				$this->content->text .= '<td align="right" ><font size="1"><img src="'.$wwwroot.'\blocks\leaderboard\level3.jpg" height="25" width="25"><br/> ' .$member->points. ' pts </font></td>';
				}
				else if (($member->points<5000)||($member->points>3000))
				{
				$this->content->text .= '<td align="right" ><font size="1"><img src="'.$wwwroot.'\blocks\leaderboard\level4.png" height="25" width="25"><br/> ' .$member->points. ' pts </font></td>';
				}
				else if (($member->points<10000)||($member->points>5000))
				{
				$this->content->text .= '<td align="right" ><font size="1"><img src="'.$wwwroot.'\blocks\leaderboard\level5.png" height="25" width="25"><br/> ' .$member->points. ' pts </font></td>';
				}
				
				$this->content->text .='</tr>';       

		} 
$this->content->text .= '</table>';
$this->content->footer = '';
            


	 } // if courses

         return $this->content;

      } // function get_content()

      function specialisation() {
         $this->maxchars = $this->config->maxchars;
      }

      function instance_allow_config() {
         return true;
      } // function instance_allow_config()

      function config_save($data) {
         foreach ($data as $name => $value) {
            set_config($name, $value);
         }
         return true;
      } // function config_save()


   } // class block_leaderboard

?>
