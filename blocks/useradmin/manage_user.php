<?php // $Id$
    
    require_once('../../config.php');
    require_once('useradminlib.php');
    require_once($CFG->libdir.'/adminlib.php');
    
    
    // Login required
    require_login();

    // Requires capabilities to list users
    useradmin_require_capabilities_to_list();
    
//    // I'll check all capabilities here
//    if (!useradmin_has_required_capabilities()) {
//        print_error("You must be an administrator to edit users this way.");
//    }
    
    if (empty($CFG->loginhttps)) {
        $securewwwroot = $CFG->wwwroot;
    } else {
        $securewwwroot = str_replace('http:','https:',$CFG->wwwroot);
    }
    
    
    if (! $site = get_site()) {
        print_error("Could not find site-level course");
    }
    
    // Setup MNET enviromnent, if needed
    if (!isset($CFG->mnet_localhost_id)) {
        include_once $CFG->dirroot . '/mnet/lib.php';
        $env = new mnet_environment();
        $env->init();
        unset($env);
    }
    
    // Let's see if we have *any* mnet users. Just ask for a single record
    $mnet_users = get_records_select('user', " mnethostid != '{$CFG->mnet_localhost_id}' ", '', '*', '0', '1');
    if(is_array($mnet_users) && count($mnet_users) > 0) {
        $mnet_auth_users_exists = true;
    } else {
        $mnet_auth_users_exists = false;
    }
    
    // Load all available auth plugins (array by authtype)
    $authplugins = useradmin_get_available_auth_plugins();
    
    
    /// Get funcional parameters
    
    $newuser       = optional_param('newuser', 0, PARAM_BOOL);
    $delete        = optional_param('delete', 0, PARAM_INT);
    $confirm       = optional_param('confirm', '', PARAM_ALPHANUM);   //md5 confirmation hash
    $confirmuser   = optional_param('confirmuser', 0, PARAM_INT);
    $acl           = optional_param('acl', '0', PARAM_INT);           // id of user to tweak mnet ACL (requires $access)
    
    // Get filter/search params and hide/show fields from Session (if any)
    unset($filterparams);
    $filterparams = array();
    if (isset($USER->block_useradmin_filterparams)) {
        $filterparams = $USER->block_useradmin_filterparams;
    }
    unset($showfields);
    $showfields = array();
    if ( isset($USER->block_useradmin_showfields)) {
        $showfields = $USER->block_useradmin_showfields;
    }
    
    
    /// Setup defaults
    
    // Intilalize filters defaults
    foreach ($filterparams_default as $paramname =>$paramvalue) {
        if ( !isset($filterparams[$paramname]) )
        $filterparams[$paramname] = $paramvalue;
    }
    // Initialize Hide/show fieldsdefaults
    foreach ($showfields_defaults as $paramname => $paramvalue) {
        if ( !isset($showfields[$paramname]) )
        $showfields[$paramname] = $paramvalue;
    }
    // If any MNET user exists, enable mnethostname field regardless of default
    if ( $mnet_auth_users_exists ) {
        $showfields['mnethostname'] = 1;
    }
    
    
    /// Get filter parameters from request
    
    $clearallfilters = optional_param('clearallfilters', 0, PARAM_BOOL);
    // Clear all filters if needed (load defaults)
    if ( $clearallfilters ) {
        $sort = $filterparams_default['sort'];
        $page = $filterparams_default['page'];
        $search = $filterparams_default['search'];
        $lastinitial = $filterparams_default['lastinitial'];
        $firstinitial = $filterparams_default['firstinitial'];
        $contextlevel = $filterparams_default['contextlevel'];
        $contextinstanceid = $filterparams_default['contextinstanceid'];
        $donthaverole = $filterparams_default['donthaverole'];
        $roleid = $filterparams_default['roleid'];
        $mnethostid = $filterparams_default['mnethostid'];
        $filterconfirmed = $filterparams_default['filterconfirmed'];
        $filterauth = $filterparams_default['filterauth'];
    }
    // Get filter parameters
    else {
        $page         = useradmin_optional_param_clearing('page', $filterparams['page'], 0, PARAM_INT);
        $search       = useradmin_optional_param_clearing('search', $filterparams['search'], '', PARAM_RAW);
        $lastinitial  = useradmin_optional_param_clearing('lastinitial', $filterparams['lastinitial'], '', PARAM_CLEAN);
        $firstinitial = useradmin_optional_param_clearing('firstinitial', $filterparams['firstinitial'], '', PARAM_CLEAN);
        $contextlevel      = useradmin_optional_param_clearing('contextlevel', $filterparams['contextlevel'], 0, PARAM_INT);
        $contextinstanceid = useradmin_optional_param_clearing('contextinstanceid', $filterparams['contextinstanceid'], 0, PARAM_INT);
        $donthaverole = useradmin_optional_param_clearing('donthaverole', $filterparams['donthaverole'], 0, PARAM_BOOL);
        $roleid       = useradmin_optional_param_clearing('roleid', $filterparams['roleid'], 0, PARAM_INT);
        $mnethostid   = useradmin_optional_param_clearing('mnethostid', $filterparams['mnethostid'], '', PARAM_CLEAN);
        $filterconfirmed = useradmin_optional_param_clearing('filterconfirmed', $filterparams['filterconfirmed'], 0, PARAM_INT);
        $filterauth   = useradmin_optional_param_clearing('filterauth', $filterparams['filterauth'], '', PARAM_CLEAN); 
    
    
        $search = trim($search);
    }
    
    // Update $filterparams to save in Session, later
    $filterparams['page'] = $page;
    $filterparams['search'] = $search;
    $filterparams['lastinitial'] = $lastinitial;
    $filterparams['firstinitial'] = $firstinitial;
    $filterparams['contextlevel'] = $contextlevel;
    $filterparams['contextinstanceid'] = $contextinstanceid;
    $filterparams['donthaverole'] = $donthaverole;
    $filterparams['roleid'] = $roleid;
    $filterparams['mnethostid'] = $mnethostid;
    $filterparams['filterconfirmed'] = $filterconfirmed;
    $filterparams['filterauth'] = $filterauth;
    
    
    /// Get sorting and paging parameters from request
    
    $dir          = useradmin_optional_param_clearing('dir', $filterparams['dir'],'ASC', PARAM_ALPHA);
    $sort         = useradmin_optional_param_clearing('sort', $filterparams['sort'], 'firstname', PARAM_ALPHA);
    $perpage      = useradmin_optional_param_clearing('perpage', $filterparams['perpage'], 50, PARAM_INT);        // how many per page
    
    $filterparams['sort'] = $sort;
    $filterparams['dir'] = $dir;
    $filterparams['perpage'] = $perpage;
    
    
    /// Get Show/hide columns params
    
    // Show/hide form
//       as paging bars and bars of initials (using links and not forms) do not pass 'show_fullform' field back
//       The field is currently included in search and reset_search forms and is updated by hide/show switch 
    $showfields['_fullform'] = optional_param('show_fullform', $showfields['_fullform'], PARAM_BOOL);
    
    // Columns
    $showfields['username'] = optional_param('showusername', $showfields['username'], PARAM_BOOL);
    $showfields['email'] = optional_param('showemail', $showfields['email'], PARAM_BOOL);
    $showfields['institution'] = optional_param('showinstitution', $showfields['institution'], PARAM_BOOL);
    $showfields['department'] = optional_param('showdepartment', $showfields['department'], PARAM_BOOL);
    $showfields['city'] = optional_param('showcity', $showfields['city'], PARAM_BOOL);
    $showfields['country'] = optional_param('showcountry', $showfields['country'], PARAM_BOOL);
    $showfields['lastaccess'] = optional_param('showlastaccess', $showfields['lastaccess'], PARAM_BOOL);
    $showfields['auth'] = optional_param('showauth', $showfields['auth'], PARAM_BOOL);
    $showfields['mnethostname'] = optional_param('showmnethostname', $showfields['mnethostname'], PARAM_BOOL);
    $showfields['confirmed'] = optional_param('showconfirmed', $showfields['confirmed'], PARAM_BOOL);

    
    // Save filter/search and show/hide params in user Session
    $USER->block_useradmin_filterparams = $filterparams;
    $USER->block_useradmin_showfields = $showfields;
    
    
    /// Load some language strings
    
    $stredituser = get_string("edituser");
    $stradministration = get_string("administration");
    $strusers = get_string("users");
    $stredit   = get_string("edit");
    $strdelete = get_string("delete");
    $strconfirm = get_string("confirm");
    
    /// Page headers
    $struseradmin = get_string('userlist','block_useradmin');
	$strblockname = get_string('blockname','block_useradmin');
    
    $navlinks = array();
    $navlinks[] = array('name' => $struseradmin, 'link' => '', 'type' => 'misc');
    $navigation = build_navigation($navlinks);
    
    print_header($site->fullname.': '.$struseradmin,
                    $site->fullname,
//                    "$struseradmin",
                    $navigation,
                    '', '', true);
                        
    if ($confirmuser and confirm_sesskey()) {    /// Confirm the user, after confirmation
        if (!$user = get_record("user", "id", "$confirmuser")) {
            print_error("No such user!");
        }
    
        // Use auth plugin of the user to confirm
        $authplugin = $authplugins[$user->auth];
        // Check if confirm is available with this plugin
        if ( !$authplugin->can_confirm() ) {
            print_error('confirm not available with user\'s auth plugin','block_useradmin');
        } else {
            $result = $authplugin->user_confirm(addslashes($user->username), addslashes($user->secret));
        
            if ($result == AUTH_CONFIRM_OK or $result == AUTH_CONFIRM_ALREADY) {
                notify(get_string('userconfirmed', '', fullname($user, true)) );
            } else {
                notify(get_string('usernotconfirmed', '', fullname($user, true)));
            }
        }
    
    } else if ($delete and confirm_sesskey()) {              /// Delete a selected user, after confirmation
        if (!$user = get_record("user", "id", "$delete")) {
            print_error("No such user!");
        }

        $primaryadmin = get_admin();
        if ($user->id == $primaryadmin->id) {
            print_error("You are not allowed to delete the primary admin user!");
        }
                	
        if ($confirm != md5($delete)) {
            $fullname = fullname($user, true);
            print_heading(get_string('deleteuser', 'admin'));
            $optionsyes = array('delete'=>$delete, 'confirm'=>md5($delete), 'sesskey'=>sesskey());
//            notice_yesno(get_string('deletecheckfull', '', "'$fullname'"), "$securewwwroot/$CFG->admin/user.php", 'manage_user.php', $optionsyes, NULL, 'post', 'get');
            notice_yesno(get_string('deletecheckfull', '', "'$fullname'"), "./deleteuser.php", 'manage_user.php', $optionsyes, NULL, 'post', 'get');

            print_footer();
             
            die;
        } else if (data_submitted() and !$user->deleted) {
            //following code is also used in auth sync scripts
            $updateuser = new object();
            $updateuser->id           = $user->id;
            $updateuser->deleted      = 1;
            $updateuser->username     = addslashes("$user->email.".time());  // Remember it just in case
            $updateuser->email        = '';               // Clear this field to free it up
            $updateuser->idnumber     = '';               // Clear this field to free it up
            $updateuser->timemodified = time();
            if (update_record('user', $updateuser)) {
                // not sure if this is needed. unenrol_student($user->id);  // From all courses
                delete_records('role_assignments', 'userid', $user->id); // unassign all roles
                // remove all context assigned on this user?
                notify(get_string('deletedactivity', '', fullname($user, true)) );
            } else {
                notify(get_string('deletednot', '', fullname($user, true)));
            }
        }   
    } else if ($acl and confirm_sesskey()) {             /// Allow/Deny a selected user, after confirmation
        if (!$user = get_record('user', 'id', $acl)) {
            error("No such user.", '', true);
        }
        if (!is_mnet_remote_user($user)) {
            error('Users in the MNET access control list must be remote MNET users.');
        }
        $accessctrl = strtolower(required_param('accessctrl', PARAM_ALPHA));
        if ($accessctrl != 'allow' and $accessctrl != 'deny') {
            error('Invalid access parameter.');
        }
        $aclrecord = get_record('mnet_sso_access_control', 'username', $user->username, 'mnet_host_id', $user->mnethostid);
        if (empty($aclrecord)) {
            $aclrecord = new object();
            $aclrecord->mnet_host_id = $user->mnethostid;
            $aclrecord->username = $user->username;
            $aclrecord->accessctrl = $accessctrl;
            if (!insert_record('mnet_sso_access_control', $aclrecord)) {
                error("Database error - Couldn't modify the MNET access control list.", '', true);
            }
        } else {
            $aclrecord->accessctrl = $accessctrl;
            if (!update_record('mnet_sso_access_control', $aclrecord)) {
                error("Database error - Couldn't modify the MNET access control list.", '', true);
            }
        }
        $mnethosts = get_records('mnet_host', '', '', 'id', 'id,wwwroot,name');
        notify("MNET access control list updated: username '$user->username' from host '"
                    . $mnethosts[$user->mnethostid]->name
                    . "' access now set to '$accessctrl'.");        
    }
    
    
    /// Carry on with the user listing
    
    // Sort by "name" means by "firstname"
    if ($sort == "name") {
        $sort = "firstname";
    }
    
    // Execute query
    $usersearchcount = 0;
    $users = useradmin_get_users_listing($usersearchcount, $sort, $dir, $page*$perpage, $perpage, $search,
                $firstinitial, $lastinitial, $contextlevel, $contextinstanceid, $donthaverole, $roleid, 
                $mnethostid, $filterconfirmed, $filterauth );
    $usercount = useradmin_get_user_totalcount();
    $userpagedsearchcount = 0;
    if ( is_array($users) ) {
        $userpagedsearchcount = count($users);
    }

    $a = new stdClass;
    $a->searchcount = $usersearchcount;
    $a->pagecount = $userpagedsearchcount;
    $a->totalcount = $usercount;
    print_heading(get_string('usersontotalusers', 'block_useradmin', $a )  );
        
    // Prepare Seach description string
    $strsearchdesc = useradmin_search_description($search, $firstinitial, $lastinitial, $contextlevel, $contextinstanceid, $donthaverole, $roleid, 
                $mnethostid, $filterconfirmed, $filterauth);
     
    
    // Include Filter and Search form
    include('./filterform.html');
    
    flush();
    
    
    if (!$users) {
        print_heading(get_string("nousermatchingconditions", "block_useradmin"));
        $table = NULL;   
    } else {
    
        /// Prepare column headers
        $columns = array("firstname", "lastname", "username", "email",
                            "institution", "department", "city", "country",
                            "lastaccess",
                            "auth", "mnethostname",
                            "confirmed");
        foreach ($columns as $column) {
            // Column names (looks in global lang file and than in block lang file)
            if ( $column == 'confirmed' ) {
                $string[$column] = get_string( $column, 'block_useradmin');
            } else {
                $string[$column] = get_string("$column");
                if ( $string[$column] == "[[$column]]" ) {
                    $string[$column] = get_string( $column, 'block_useradmin');
                }
            }
            	
             
            if ($sort != $column) {
                $columnicon = "";
                if ($column == "lastaccess") {
                    $columndir = "DESC";
                } else {
                    $columndir = "ASC";
                }
            } else {
                $columndir = $dir == "ASC" ? "DESC":"ASC";
                if ($column == "lastaccess") {
                    $columnicon = $dir == "ASC" ? "up":"down";
                } else {
                    $columnicon = $dir == "ASC" ? "down":"up";
                }
                $columnicon = " <img src=\"$CFG->pixpath/t/$columnicon.gif\" alt=\"\" />";
            }
        
            /// Hide and show column headers
        
            // Get column label (if undef in global lang file, look in block lang file)
            $columnlabel = get_string($column);
            if ( $columnlabel == "[[$column]]" ) {
                $columnlabel = get_string($column, 'block_useradmin');
            }
            // Force getting confirmed label from block lang file
            if ( $column == 'confirmed' ) {
                $columnlabel = get_string($column, 'block_useradmin');
            }
            	
            // No hide for last and first name
            if ($column == 'firstname' ||  $column == 'lastname' ) {
                $$column = "<a href=\"manage_user.php?sort=$column&amp;dir=$columndir&amp;search="
                            .urlencode(stripslashes($search))."&amp;firstinitial=$firstinitial&amp;lastinitial=$lastinitial\">"
                            .$string[$column]."</a>$columnicon";
            }
            // for other column, if column is hidden show only 'show' icon w/ tooltip
            else if ( !$showfields[$column] ) {
                $showcolumnlabel = get_string('showfield','block_useradmin')." ".$columnlabel;
                $$column = "<a href=\"manage_user.php?show${column}=1\" class=\"tooltip\">"
                            ."<img src=\"$CFG->pixpath/t/switch_plus.gif\" alt=\"$showcolumnlabel\" title=\"$showcolumnlabel\" />"
                            ."<span>$columnlabel</span></a>";
            }
            // otherwise show column and 'hide' icon
            else {
                $hidecolumnlabel = get_string('hidefield','block_useradmin')." ".$columnlabel;
                $$column = "<a href=\"manage_user.php?show${column}=0\">"
                            ."<img src=\"$CFG->pixpath/t/switch_minus.gif\" alt=\"$hidecolumnlabel\" title=\"$hidecolumnlabel\" /></a> "
                            ."<a href=\"manage_user.php?sort=$column&amp;dir=$columndir&amp;search="
                            .urlencode(stripslashes($search))."&amp;firstinitial=$firstinitial&amp;lastinitial=$lastinitial\">"
                            .$string[$column]."</a>$columnicon";
            }
        }
        
        
        $countries = get_list_of_countries();
    
        foreach ($users as $key => $user) {
            if (!empty($user->country)) {
                $users[$key]->country = $countries[$user->country];
            }
        }
        if ($sort == "country") {  // Need to resort by full country name, not code
            foreach ($users as $user) {
                $susers[$user->id] = $user->country;
            }
            asort($susers);
            foreach ($susers as $key => $value) {
                $nusers[] = $users[$key];
            }
            $users = $nusers;
        }
    
        // Setup table
        $table->head = array ("$firstname / $lastname", $username, $email,
                    $institution, $department, $city, $country,
                    $lastaccess,
                    $auth, $mnethostname,
                    $confirmed, "");
        $table->align = array ("left", "left", "left",
                    "left", "left", "left", "left",
                    "left",
                    "left", "left",
                    "left", "right");
        $table->wrap = array ('nowrap', '', '',
                    '', '', '', '',
                    '',
                    '', '',
                    '', 'nowrap');
        $table->width = "98%";
    
        // Spacer for icons
        $iconspacer = "<img src=\"$securewwwroot/pix/spacer.gif\" width=\"11\" height=\"11\" border=\"0\" />";
    
        foreach ($users as $user) {
    
            // is the user remote?
            $isremoteuser = ($user->mnethostid != $CFG->mnet_localhost_id);
             
            // User Auth plugin instance
            $authplugin = $authplugins[$user->auth];
            
            // last access
            if ($user->lastaccess) {
                $strlastaccess = format_time(time() - $user->lastaccess);
            } else {
                $strlastaccess = get_string("never");
            }
        
            // Edit icon (only if user is local and has capabilities)
            $editbutton = '';
            if ( useradmin_has_capabilities_to_edit() ) {
                if ( $isremoteuser ) {
                    $editbutton = $iconspacer;
                } else {
                     $editbutton = "<a href=\"$securewwwroot/user/editadvanced.php?id=$user->id&amp;course=$site->id\"><img src=\"$securewwwroot/pix/t/edit.gif\" alt=\"$stredit\" title=\"$stredit\" /></a>";
                }
            }
             
            // Delete icon
            $deletebutton = '';
            if ( useradmin_has_capabilities_to_delete() ) {
                if ($user->id == $USER->id or $user->username == "changeme") {
                    $deletebutton = $iconspacer;
                } else {
                    $deletebutton = "<a href=\"?delete=$user->id&amp;sesskey=$USER->sesskey\"><img src=\"$securewwwroot/pix/t/delete.gif\" alt=\"$strdelete\" title=\"$strdelete\" /></a>";
                }
            }
    
            // Confirm icon and confirm string(only if local and user's auth allow confirm)
            $confirmbutton = '';
            $strisconfirmed = '';
            if ( useradmin_has_capabilities_to_edit() ) {
                if ( !$isremoteuser && $authplugin->can_confirm() ) {
                    if ( $user->confirmed == 0 ) {
                        $strisconfirmed = get_string('no');
                        $confirmbutton = "<a href=\"?confirmuser=$user->id&amp;sesskey=$USER->sesskey\"><img src=\"$securewwwroot/pix/t/clear.gif\" alt=\"$strconfirm\" title=\"$strconfirm\" /></a>";                
                    } else {
                        $strisconfirmed = get_string('yes');
                        $confirmbutton = $iconspacer;                
                    }
                } else {
                    $strisconfirmed = get_string('n_a','block_useradmin');
                    $confirmbutton = $iconspacer;
                }
            }
                
            // Only if remote users exists...
            $strremotehost = '';
            if ( useradmin_has_capabilities_to_edit() ) {            
                if ( $mnet_auth_users_exists ) {
                    if ( $isremoteuser ) {
                        // Allow/Deny button (form remote users only)
                        $accessctrl = 'allow';
                        if ($acl = get_record('mnet_sso_access_control', 'username', $user->username, 'mnet_host_id', $user->mnethostid)) {
                            $accessctrl = $acl->accessctrl;
                        }
                        $strallowdeny = get_string( $accessctrl ,'mnet');
                        $changeaccessto = ($accessctrl == 'deny' ? 'allow' : 'deny');
                        $strchangeto =  s(($changeaccessto == 'deny')?(get_string('allow_denymnetaccess', 'block_useradmin')):(get_string('deny_allowmnetaccess', 'block_useradmin')));
                        $allowdenyiconurl = "$securewwwroot/pix/t/". (($accessctrl == 'allow')?'go.gif':'stop.gif') ;
                        $allowdenybutton = "<a href=\"?acl={$user->id}&amp;accessctrl=$changeaccessto&amp;sesskey={$USER->sesskey}\"><img src=\"$allowdenyiconurl\" alt=\"$strchangeto\" title=\"$strchangeto\" /></a>";
                        
                        // Remote Host
                        $strremotehost .= s($user->mnethostname) . " $allowdenybutton";
                    }                 
                }
            }
    
    
            // Select checkbox (if current user has capabilities to assign roles)
            $selectcheck = '';
            if ( useradmin_has_capabilities_to_assign() ) {
                $selectcheck = '<input type="checkbox" name="user'.$user->id.'" />';
            }    
            // Icons
            $actionbuttons = $editbutton.' '.$deletebutton.' '.$confirmbutton; 
                       
            // Full name
            $fullname = fullname($user, true);
            
            // Edit user link (link only if user can edit)
            $edituserlink =  s($fullname);
            if ( useradmin_has_capabilities_to_edit() ) {
                $edituserlink = " <a href=\"$securewwwroot/user/view.php?id=$user->id&amp;course=$site->id\">$edituserlink</a>";
            } 
            
            $table->data[] = array ($selectcheck.$edituserlink,
                        useradmin_collapsable_text( s($user->username), $showfields['username'] ),
                        useradmin_collapsable_text( obfuscate_mailto($user->email, '', $user->emailstop), $showfields['email'], $user->email ),
                        useradmin_collapsable_text( s($user->institution), $showfields['institution'] ),
                        useradmin_collapsable_text( s($user->department), $showfields['department'] ),
                        useradmin_collapsable_text( s($user->city), $showfields['city'] ),
                        useradmin_collapsable_text( s($user->country), $showfields['country'] ),
                        useradmin_collapsable_text( s($strlastaccess), $showfields['lastaccess'] ),
                        useradmin_collapsable_text( s($user->auth), $showfields['auth'] ),
                        // Displays MNET host only if not localhost
                        useradmin_collapsable_text( $strremotehost, $showfields['mnethostname'] ),
                        useradmin_collapsable_text( $strisconfirmed, $showfields['confirmed'] ),
                        $actionbuttons);
        }
    }
    
//    // Button Export to CVS
//    echo '<div align="center">';
//    echo "<form id=\"exportUsersForm\" action=\"downloaduser.php\" method=\"get\" >";
//    echo "<input type=\"hidden\" name=\"usefilterparams\" value=\"1\" />";
//    echo "<input type=\"submit\" value=\"".get_string('exporttheseuserstofile','block_useradmin')."\"  ";
//    echo "</form>";
//    echo "</div>";
    
     // New user link (if has capabilities) and export to CVS
    $strnewuserandexportheading = '<table width="100%" border="0"><tr><td>';
    if ( useradmin_has_capabilities_to_create() ) {                
        $strnewuserandexportheading .= '<a href="'.$securewwwroot.'/user/editadvanced.php?id=-1">'.get_string('addnewuser').'</a>';
    }
    $strnewuserandexportheading .= '</td><td align="right">';    
    $strnewuserandexportheading .= '<a href="./downloaduser.php?id=-1&usefilterparams=1">'.get_string('exporttheseuserstofile','block_useradmin').'</a>';
    $strnewuserandexportheading .= '</td></tr></table>';
    print_heading($strnewuserandexportheading);
    
    if (!empty($table)  ) {
         
        // Form
        $strnouserselected = get_string("nouserselected","block_useradmin");
        $strnoactionselected = get_string("noactionselected","block_useradmin");
        echo "  <script Language=\"JavaScript\">
				<!--
				function checksubmit(form) {
					if ( !checkchecked(form) ) {
						alert ('$strnouserselected');
						return false;
					} else if ( document.getElementById('assign_contextlevel').value == ''
							     || document.getElementById('assign_contextinstanceid').value == ''
								 || document.getElementById('assign_roleid').value == '') {
						alert ('$strnoactionselected');
						return false;
					} 
					return true;
				}
				
				function checkchecked(form) {
				    var inputs = document.getElementsByTagName('INPUT');
				    var checked = false;
				    inputs = filterByParent(inputs, function() {return form;});
				    for(var i = 0; i < inputs.length; ++i) {
				        if(inputs[i].type == 'checkbox' && inputs[i].checked) {
				            checked = true;
				        }
				    }
				    return checked;
				}
				
				function submitMultiDelete(form) {
					if ( checkchecked(form) ) {
						document.getElementById('multi_action').value = 'delete';
						document.forms['usersform'].submit();
						return true;
					} else {
						alert ('$strnouserselected');
						return false;
					}
				}
				//-->
				</script>";
         
        echo '<form action="multiuserassignment.php" method="post" name="usersform" onSubmit="return checksubmit(this);">';
        echo '<input type="hidden" name="returnto" value="'.$_SERVER['REQUEST_URI'].'" />';
        echo '<input type="hidden" name="sesskey" value="'.$USER->sesskey.'" />';
         
        print_table($table);
    
        echo '<br />';
    
        // Show multi-user assignment form only if user has required capabilities
        if ( useradmin_has_capabilities_to_assign() ) {
            /// Multi-user action
            echo '<table width="98%"><tr><td align="left" valign="top">';
             
            // Select buttons
            echo '<input type="button" onclick="checkall()" value="'.get_string('selectall').'" /> ';
            echo '<input type="button" onclick="checknone()" value="'.get_string('deselectall').'" /> ';
             
            echo '</td><td align="right" valign="top">';
            	
        
            // "Callback" hidden fields for the form in the IFRAME (note that no default value is set
            echo "<input type=\"hidden\" id=\"assign_contextlevel\" name=\"assign_contextlevel\" value=\"\" />";
            echo "<input type=\"hidden\" id=\"assign_contextinstanceid\" name=\"assign_contextinstanceid\" value=\"\" />";
            echo "<input type=\"hidden\" id=\"assign_roleid\" name=\"assign_roleid\" value=\"0\" />";
            echo "<input type=\"hidden\" id=\"assign_unassign\" name=\"assign_unassign\" value=\"\" />";  // This is the action! (assign|unassing)
            	
            // TODO this IFRAME juggle should be converted in a clean AJAX menu chain
            $iframe_querystring = "_contextlevel=&_contextinstanceid="
        								 ."&_donthaverole=0&_roleid="
        						 		 ."&parent_contextlevel=assign_contextlevel&parent_contextinstanceid=assign_contextinstanceid"
        							     ."&parent_donthaverole=assign_unassign&parent_roleid=assign_roleid"
        						 		 ."&strprefix=assignrole_";
            // Button Execute Assignment
            echo '<input style="float: left;" type="submit" value="' . get_string('assignselectedusers', 'block_useradmin') . '" />';
        	echo "<iframe style='float: left;' src=\"contextfilterframe.php?$iframe_querystring\" width=\"800\" height=\"45\" frameborder=\"0\"  marginwidth=\"0\" marginheight=\"0\" >IFRAMEs support required ;-)</iframe>";
            
            echo '</td></tr><tr><td colspan="2" align="right">';
            
            // Button DELETE selected users
            echo '<input type="hidden" name="multi_action" id="multi_action" value="role_assign" />';
            echo '<input type="button" value="' . get_string('deleteselectedusers', 'block_useradmin') . '" onclick="submitMultiDelete(this)" />';
            
            echo '</td></tr></table>';
        }             
        echo "</form>";

//        // Button Export to CVS
//        echo '<div align="center">';
//        echo "<form id=\"exportUsersForm\" action=\"downloaduser.php\" method=\"get\" >";
//        echo "<input type=\"hidden\" name=\"usefilterparams\" value=\"1\" />";
//        echo "<input type=\"submit\" value=\"".get_string('exporttheseuserstofile','block_useradmin')."\"  ";
//        echo "</form>";
//        echo "</div>";
        

        print_paging_bar($usersearchcount, $page, $perpage, "?");  // Base URL is current page w/o any parameters (filter params are in Session)

        // New user link (if has capabilities) and export to CSV
        print_heading($strnewuserandexportheading);
    }
    
    
    print_footer();
?>
