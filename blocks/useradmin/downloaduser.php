<?php // $Id$
	/// Bulk user download

	require_once('../../config.php');
	require_once($CFG->libdir.'/uploadlib.php');
	require_once('./useradminlib.php');

	require_login();

//	// Fixme check roles
//	if (!isadmin()) {    
//	    error("You must be an administrator to edit users this way.");
//	}

	// Require capabilities to List users
	useradmin_require_capabilities_to_list();
	
	if (! $site = get_site()) {
	    error("Could not find site-level course");
	}
	if (!$adminuser = get_admin()) {
	    error("Could not find site admin");
	}
		

	/// Get defaults
	$defaultseparatoropt = 0;
	if (isset($CFG->CSV_DELIMITER)) {
		foreach ($fieldseparatoroptions as $opt => $sep) {
			if ( $CFG->CSV_DELIMITER == $sep ) {
				$defaultseparatoropt = $opt;	
			}
		}	
	}	
	$defaultfilencodingopt = 0; // UTF-8
	
	
	/// Retrieve parameters
	$download                   = optional_param('download',0,PARAM_BOOL);
	$maxnumberofcoursefield 	= optional_param('maxnumberofcoursefield', 0, PARAM_INT);
	$separatoropt				= optional_param('separatoropt', $defaultseparatoropt, PARAM_INT);
	$eolopt                     = optional_param('eolopt', 0, PARAM_INT);
	$includeunconfirmed         = optional_param('includeunconfirmed',0,PARAM_BOOL);
	$includeremote              = optional_param('includeremote',0,PARAM_BOOL);	
	$usefilterparams            = optional_param('usefilterparams',0,PARAM_BOOL); // Use filterparams in Session
	$filencodingopt             = optional_param('filencodingopt', $defaultfilencodingopt, PARAM_INT );
	
	$debug                      = optional_param('debug',0,PARAM_BOOL); // Output to browser page
	
	
	// Need to convert chars to Latin1?
	$utf82latin = False;
	if ( $filencodingopt ) {
	    $utf82latin = True;
	}
	
	/// Use filter params in Session (if $usefilterparams)
	if ( $usefilterparams ) {
        $filterparams = array();
	    if (isset($USER->block_useradmin_filterparams)) {
            $filterparams = $USER->block_useradmin_filterparams;
        } 
        // Use filters defaults for missing params
        foreach ($filterparams_default as $paramname =>$paramvalue) {
            if ( !isset($filterparams[$paramname]) )
            $filterparams[$paramname] = $paramvalue;
        }

        // Prepare Search description string
        $searchdescription = useradmin_search_description( $filterparams['search'], 
                                                           $filterparams['firstinitial'], 
                                                           $filterparams['lastinitial'], 
                                                           $filterparams['contextlevel'],
                                                           $filterparams['contextinstanceid'],
                                                           $filterparams['donthaverole'],
                                                           $filterparams['roleid'],
                                                           $filterparams['mnethostid'], 
                                                           $filterparams['filterconfirmed'],
                                                           $filterparams['filterauth']);
	}
	
	
	// EOL
	$eol = $eols[$eolopt];
	
	// CSV separator and encoding
	if ( !array_key_exists($separatoropt, $fieldseparatoroptions)  ) {
		 error("Invalid field separator");
	}
	$csv_delimiter= $fieldseparatoroptions[$separatoropt];
	$csv_encode = '/\&\#' . $separatorencodings[$separatoropt] . '/';
	
    // Large files are likely to take their time and memory. Let PHP know
    // that we'll take longer, and that the process should be recycled soon
    // to free up memory.
    @ini_set('max_execution_time',0);
    @raise_memory_limit("128M");
    if (function_exists('apache_child_terminate')) {
        @apache_child_terminate();
    }
	
    // if $download is specified, export file
    if ( $download ) {
        $filename = clean_filename("$site->shortname"."_users").'.csv';
        
        /// Send headers
        if ( !$debug ) {
            header("Content-Type: application/download\n");
            header("Content-Disposition: attachment; filename=\"$filename\"");
            header("Expires: 0");
            header("Cache-Control: must-revalidate,post-check=0,pre-check=0");
            header("Pragma: public");
        }
        
		/// Columns
		$columns = array("lastname", "firstname", "username", "password", "email", "idnumber",  
					"institution", "department", 
					"address", "city", "country",
					"icq", "skype", "yahoo", "aim", "msn",
					"phone1", "phone2", "url", 
					"lang", "emailstop");
					
	    /// Print names of all the fields
		$col = 0;
		foreach ($columns as $column) {		
			if ( $col > 0) {
				echo $csv_delimiter;
			}
			echo $column;
			$col++;
		}
		
		if ( $maxnumberofcoursefield ) {
			// Type and Courses columns ($maxnumberofcoursefield)
			for($i=1; $i<=$maxnumberofcoursefield; $i++) {
				echo $csv_delimiter."course".$i;
				echo $csv_delimiter."role".$i;
				$col++; $col++;
			}
		}		
		
		// EOL
		echo $eol;
		
		
		$usercount = 0;
		if ( $usefilterparams ) {
		     $users = useradmin_get_users_listing($usercount,'firstname','ASC',0,99999,
		                                                   $filterparams['search'], 
                                                           $filterparams['firstinitial'], 
                                                           $filterparams['lastinitial'], 
                                                           $filterparams['contextlevel'],
                                                           $filterparams['contextinstanceid'],
                                                           $filterparams['donthaverole'],
                                                           $filterparams['roleid'],
                                                           $filterparams['mnethostid'], 
                                                           $filterparams['filterconfirmed'],
                                                           $filterparams['filterauth']);        
		} 
		// Retrieve all Users
		else {            
		    $users = useradmin_get_all_users($includeunconfirmed, $includeremote);
		    $usercount = count($usercount);
		}
		
		// Retrieve all Roles of all Users in all Courses
		$users_courses_roles = useradmin_get_users_courses_roles();
		
		/// Loop all Users
		if ($users) {
			$row = 1;
	    	foreach ($users as $user) {
	    		$userArray = (array)$user;
	    		$col = 0;
				foreach ($columns as $column) {
				    
				    $value = $userArray[$column];
					// Password is always empty
					if ( $column == 'password' ) {
						$value = '';
					} else {					
					
    					// If needed, convert  UTF8 to Latin
    					if ( $utf82latin ) {
    					    $value = utf8_decode($userArray[$column]);    
    					}
					
					    // Replace CSV separator with encode					   
						$value = str_replace($csv_delimiter, $csv_encode, $value );
					}
					
					if ( $col > 0 ) {
						echo $csv_delimiter;
					}
					echo $value;

					$col++;
				} 
				
				if ( $maxnumberofcoursefield ) {
				    
				    // All Roles in all Courses of this user
				    if (isset($users_courses_roles[$user->id])) {
				        $user_courses_roles = $users_courses_roles[$user->id];
				    } else {
				        $user_courses_roles = array();   
				    }
				    
				    $c = 0; // Count course/role columns
				    if ( $user_courses_roles ) {
				        foreach ($user_courses_roles as $courseid => $course_roles) {
				        	foreach ($course_roles as $ucrole) {
                                // Output course and role columns				        	    
				        	    $value = str_replace($csv_delimiter, $csv_encode, $ucrole->courseshortname );
				        	    echo $csv_delimiter." ".$value;
				        	    
				        	    $value = str_replace($csv_delimiter, $csv_encode, $ucrole->roleshortname );
				        	    echo $csv_delimiter." ".$value;
				        	    
				        	    $c++;
				        	    
				        	    if ( $c >= $maxnumberofcoursefield ) {
				        	        break;   
				        	    }
				        	}
			        	    if ( $c >= $maxnumberofcoursefield ) {
			        	        break;   
			        	    }
				        }
				    }
				    
				    // padding with empty columns up to $maxnumberofcoursefield
				    for($i=$c; $i<$maxnumberofcoursefield; $i++ ) {
				        echo $csv_delimiter;
				        echo $csv_delimiter;    
				    }				    				    				    
				}				
				// EOL
				echo $eol;
				
	    		$row++;
	    	}
		}		
	    exit;
    }
    
    /// Show form


	/// Print the header
	
	$strdownloaduser = get_string('downloaduser','block_useradmin');
	$strblockname = get_string('blockname','block_useradmin');
	
    $navlinks = array();
    $navlinks[] = array('name' => $strblockname, 'link' => 'manage_user.php', 'type' => 'misc');
    $navlinks[] = array('name' => $strdownloaduser, 'link' => '', 'type' => 'misc');
    $navigation = build_navigation($navlinks);
    
	print_header($site->fullname.': '.$strdownloaduser, 
				 $site->fullname, 
//				 "$strdownloaduser",
				 $navigation,
				 '', '', true);
	    
    print_heading_with_help(get_string('downloaduser','block_useradmin'), 'downloaduser', 'block_useradmin');
	
    // Show Search description, if is going to use filter params
    if ( $usefilterparams ) {
        print_box($searchdescription);
    }
    
	print_box_start('center');	
	echo '<center>';
	echo '<form method="post" enctype="multipart/form-data" action="downloaduser.php">';
	echo '<table>';
	
	echo '<input type="hidden" name="download" value="1" />';
	
	// Pass-through debug and usefilterparams 
	echo "<input type=\"hidden\" name=\"debug\" value=\"$debug\" />";
	echo "<input type=\"hidden\" name=\"usefilterparams\" value=\"$usefilterparams\" />";
	
	// Choose Separator
	echo '<tr><td>' . get_string('fieldseparator', 'block_useradmin') . '</td><td>';
	choose_from_menu($fieldseparatormenuoptions, 'separatoropt', $separatoropt, '');
	echo '</td></tr>';
		
	// choose file encoding
	echo '<tr><td>' . get_string('filencoding', 'block_useradmin');
	helpbutton('filencoding', get_string('filencoding','block_useradmin'), 'block_useradmin');
	echo '</td><td>';
	choose_from_menu($filencodingmenuoptions, 'filencodingopt', $filencodingopt, '');
	echo '</td></tr>';
	
	
	// Max Number of course, group and type fields
	echo '<tr><td>' . get_string('maxnumberofcoursefield', 'block_useradmin') . '</td><td>';
	$maxnumberofcoursefieldopstion = array(0 => 0, 5 => 5, 10 => 10, 20 => 20);
	choose_from_menu($maxnumberofcoursefieldopstion, 'maxnumberofcoursefield', $maxnumberofcoursefield,'');
	echo '</td></tr>';

	// EOL
	echo '<tr><td>' . get_string('eol', 'block_useradmin') . '</td><td>';
	choose_from_menu($options_eol, 'eolopt', $eolopt,'');
	echo '</td></tr>';
	
	if ( !$usefilterparams ) {	
    	// Choose include unconfirmed
    	echo '<tr><td>' . get_string('includeunconfirmed', 'block_useradmin') . '</td><td>';
    	choose_from_menu_yesno('includeunconfirmed', $includeunconfirmed);
    	echo '</td></tr>';
    	
    	// Include remote
    	echo '<tr><td>' . get_string('includeremote', 'block_useradmin') . '</td><td>';
    	choose_from_menu_yesno('includeremote', $includeremote);
    	echo '</td></tr>';
	} else {
	    echo '<tr><td colspan="2"><hr size="0" width="100%" /></td></tr>';
	    echo '<tr><td colspan="2">' . $searchdescription . '</td></tr>';
	}
	
	echo '</table><br />';
		
	echo '<input type="submit" value="'.$strdownloaduser.'">';
	echo '</form><br />';
	
	echo '</center>';
	print_box_end();
	
	print_footer();
    
?>