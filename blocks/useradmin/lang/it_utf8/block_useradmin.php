<?php // $Id$
$string['blockname'] = 'Gestione Utenti';


$string['userlist'] = 'Elenco Utenti';
$string['uploaduser'] = 'Carica Utenti da file';
$string['downloaduser'] = 'Scarica file Utenti';
$string['eol'] = 'Terminatore di riga';
$string['doseol'] = 'DOS/Windows (CR+LF)';
$string['unixeol'] = 'Unix (LF)';
$string['maceol'] = 'Mac (CR)';
$string['includeunconfirmed'] = 'Includi utenti non confermati';
$string['includeremote'] = 'Includi utenti remoti (MNET)';
$string['filencoding'] = 'Codifica caratteri nel file';

$string['usersontotalusers'] = '$a->searchcount utenti su $a->totalcount ($a->pagecount in questa pagina)';
$string['userperpage'] = 'Utenti per pagina';
$string['searchin'] = 'Cerca in';
$string['or'] = 'oppure';
$string['clearsearch'] = 'Annulla ricerca';
$string['clearallfilters'] = 'Annulla tutte le condizioni di ricerca e ricarica';
$string['search'] = 'Esegui Ricerca';
$string['nousermatchingconditions'] = "Nessun utente soddisfa le condizioni";
$string['showfield'] = 'Espandi colonna $a';
$string['hidefield'] = 'Compatta colonna $a';
$string['mnethostname'] = 'Nodo MNET';
$string['auth'] = 'Autenticazione';

$string['assignrole'] = 'Assegna il Ruolo';
$string['unassignrole'] = 'Disassegna il Ruolo';
$string['assignrolehidden'] = 'Assegna il Ruolo (nascosto)';
$string['inthecontextof'] = 'nel Contesto';

$string['alreadyassigned'] = 'Già assegnato';
$string['notassigned'] = 'Non assegnato';
$string['wrongwillbeskipped'] = 'le assegnazioni/disassegnazioni errate saranno ignorate';
$string['userswillbedeleted'] = '<b>Questi utenti saranno cancellati definitivamente!</b> Una volta eseguita l\'operazione non potrà essere annullata.';
$string['deleteselectedusers'] = 'CANCELLA gli utenti selezionati';
$string['assignselectedusers'] = 'Modifica ruoli degli utenti selezionati';

$string['execute'] = 'Esegui';
$string['changescancelled'] = 'Modifiche annullate';

$string['linenumber'] = 'Riga'; 
$string['username'] = 'Username';     
$string['emaildisable'] = 'La mial di questo utente sarà disabilitata';
$string['fakemail'] = 'Generata falsa-mail';
$string['skipthisline'] = 'QUESTA RIGA DEL FILE VIENE SALTATA!';
$string['fakemails'] = 'False-mail generate (mail disabilitata)';
$string['invalidmails'] = 'Mail non valide';
$string['duplicatemails'] = 'Mail duplicate';
$string['newuseradded'] = 'Creato nuovo utente';
$string['usernotupdatederror'] = 'Errore aggiornando l\'utente';
$string['usernotaddedregistered'] = 'Utente già esistente. Nuovo utente non aggiunto';
$string['usernotaddederror'] = 'Utente non aggiunto. Errore sconosciuto';
$string['cannotassignroleincourse'] = 'Non puoi assegnare il ruolo $a->role nel corso $a->course';
$string['assignedroleincourse'] = 'Assegnato ruolo $a->role nel corso $a->course';
$string['enrolledasteacher'] = 'Iscritto al corso $a come Docente';
$string['enrolledasnoneditingteacher'] = 'Iscritto al corso $a come Docente Non-editor';
$string['enrolledasstudent'] = 'Iscritto al corso $a come Studente';
$string['enrolledwithdefaultrole'] = 'Iscritto al corso $a (con il ruolo di default)';
$string['mnethostidnotexists'] = 'Non esiste un peer MNET con id=$a';
$string['truncatefield'] = 'Campo $a->fieldname troppo lungo. Troncato a $a->length caratteri.';

$string['explain'] = '<h2>Leggere attentamente</h2>
<p><b>Formato del file per il caricamento utenti:</b>
<ul>
	<li>Usare come separatore dei campi il carattere <span style=\"font-size: larger; border: thin solid Red;; padding: 3px 3px 3px 3px;\"><b>$a->separator</b></span></li>
	<li>Campi obbligatori: <code>username, password, email, firstname</code> (Nome), <code>lastname</code> (Cognome)</li>
    <li>Campi opzionali, ma consigliati: <code>city, institution</code> (Località, Ente)</li>
	<li>L\'indirizzo e-mail è <b>obbligatorio</b> e deve essere <b>unico</b>.<br /> 	    
	    Se l\'utente non ha mail, inserire <b>$a->nomail</b> come indirizzo.<br />
		Verrà generato un indirizzo \"falso\" e la mail di questo utente sarà disabilitata</li>
    <li>Per iscrivere l\'utente ad un corso, utilizzare il campo <code>course1</code> inserendo il <b>Nome breve</b> del Corso,
		(scritto in maniera esatta)</li>
</ul></p>
<p>Per ulteriori dettagli leggere l\'Help di questa pagina</p>
';

$string['fieldseparator'] = 'Carattere separatore dei campi (formato testo)';
$string['comma'] = 'Virgola: \",\"';
$string['semicolon'] = 'Punto e virgola: \";\"';
$string['pipe'] = 'Pipe: \"|\"';
$string['tab'] = 'Tabulazione';
$string['nomailplaceholder'] = 'Stringa per \"nessuna mail\"';
$string['onlyalphanum'] = 'solo lettere e numeri, senza spazi';
$string['fakemaildomain'] = 'Finto dominio per le false mail';
$string['maxnumberofcoursefield'] = 'Numero massimo di campi <code>course</code>, <code>group</code>, <code>role</code> e <code>type</code>';

$string['uploadfile'] = 'Invia file';
$string['confirmed'] = 'Confermato';

$string['fakemailgeneration'] = 'Generazione false-mail';

$string['filterctx_contextlevel_label'] = 'Solo Utenti che in nel Contesto';
$string['filterctx_context_label'] = '';
$string['filterctx_donthaverole_label'] = '';
$string['filterctx_role_label'] = 'il Ruolo';
$string['filterctx_have'] = 'hanno';
$string['filterctx_havenot'] = 'NON hanno';

$string['assignrole_contextlevel_label'] = 'Nel Contesto';
$string['assignrole_context_label'] = '';
$string['assignrole_donthaverole_label'] = 'esegui';
$string['assignrole_role_label'] = 'del Ruolo';
$string['assignrole_have'] = 'ASSEGNA';
$string['assignrole_havenot'] = 'DISASSEGNA';
$string['assignrole_havehidden'] = 'ASSEGNA (nascosto)';


$string['filterbymnethost'] = 'Utenti MNET da';

$string['localhost'] = '(sito locale)';
$string['anyhost'] = 'Qualunque';

$string['CONTEXT_SYSTEM'] = 'Sito';
$string['CONTEXT_COURSECAT'] = 'Categoria Corsi';
$string['CONTEXT_COURSE'] = 'Corso';

$string['nouserselected'] = "Selezionare almeno un Utente";
$string['noactionselected'] = "Selezionare un\'operazione da eseguire"; // need escaping as this string is used in javascript
$string['n_a'] = 'n/a';
$string['unconfirm'] = 'Annulla conferma';

$string['deny_allowmnetaccess'] = 'Accesso attualmente Bloccato --> Sblocca';
$string['allow_denymnetaccess'] = 'Accesso attualmente Consentito --> Blocca';

$string['hide_showfilterform'] = 'Mostra/Nascondi modulo';

$string['searchconditions'] = '<strong>Condizioni filtro:</strong>';
$string['nosearchcondition'] = 'Nessuna condizione di filtro';
$string['searchbyfirstinitial'] = 'Nome inizia per \'<em>$a</em>\'';
$string['searchbylastinitial'] = 'Cognome inizia per \'<em>$a</em>\'';
$string['searchbystring'] = 'contiene \'<em>$a</em>\'';
$string['searchbycontext'] = '$a->donthaverole il Ruolo <em>$a->role</em> in $a->contextlevel <em>$a->context</em>';
$string['searchbyhost'] = 'utente remoto da <em>$a</em>';
$string['searchhave'] = 'ha';
$string['searchhavenot'] = 'NON ha';
$string['searchbyauth'] = 'Autenticazione <em>$a</em>';
$string['and'] = 'E';

$string['filterconfirmedusers'] = 'Utenti confermati';
$string['filterconfirmed_all'] = 'Tutti';
$string['filterconfirmed_confirmedonly'] = 'Solo Confermati';
$string['filterconfirmed_unconfirmedonly'] = 'Sono NON Confermati';

$string['filterauth'] = 'Autenticazione';
$string['anyauth'] = 'Qualunque';

$string['exporttheseuserstofile'] = 'Esporta gli utenti mostrati su file CSV';
$string['invalidfieldname_areyousure'] = '$a non è un nome di campo valido!<br />Controlla di aver scelto il Carattere Separatore giusto.';

$string['addedtogroup'] = 'Aggiunto al gruppo $a->group nel corso $a->course';
$string['addedtogroupnot'] = 'Non aggiunto al gruppo $a->group nel corso $a->course';
$string['addedtogroupnotenrolled'] = 'Non aggiunto al gruppo $a->group, perché non iscritto al corso $a->course';

$string['willbedeleted'] = 'Verrà eliminato!';
?>