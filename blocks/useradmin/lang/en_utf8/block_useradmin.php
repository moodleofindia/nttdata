<?php // $Id$
$string['blockname'] = 'User Administration';


$string['userlist'] = 'Browse list of Users';
$string['uploaduser'] = 'Upload Users from file';
$string['downloaduser'] = 'Download Users to file';
$string['eol'] = 'Line terinator';
$string['doseol'] = 'DOS/Windows (CR+LF)';
$string['unixeol'] = 'Unix (LF)';
$string['maceol'] = 'Mac (CR)';
$string['includeunconfirmed'] = 'Include unconfirmed users';
$string['includeremote'] = 'Include remote (MNET) users';
$string['filencoding'] = 'File character Encoding';

$string['usersontotalusers'] = '$a->searchcount on $a->totalcount users ($a->pagecount on this page)';

$string['userperpage'] = 'Users per page';
$string['searchin'] = 'Search in';
$string['or'] = 'or';
$string['clearsearch'] = 'Clear Search';
$string['clearallfilters'] = 'Clear all search conditions and reload';
$string['search'] = 'Search';
$string['nousermatchingconditions'] = "No User matching conditions";
$string['showfield'] = 'Expand column $a';
$string['hidefield'] = 'Hide column $a';
$string['mnethostname'] = 'MNET Peer';
$string['auth'] = 'Authentication';

$string['assignrole'] = 'Assign Role';
$string['assignrolehidden'] = 'Assign (hidden) Role';
$string['unassignrole'] = 'Unassign Role';
$string['inthecontextof'] = 'in the Context';

$string['alreadyassigned'] = 'Already assigned';
$string['notassigned'] = 'Not assigned';
$string['wrongwillbeskipped'] = 'wrong assignment/unassignment will be skipped';
$string['userswillbedeleted'] = '<b>These users will be completely deleted!</b> This operation cannot be undone';
$string['deleteselectedusers'] = 'DELETE selected users';
$string['assignselectedusers'] = 'Manage roles of selected users';

$string['execute'] = 'Execute';
$string['changescancelled'] = 'Cancel';

$string['linenumber'] = 'Row'; 
$string['username'] = 'Username';     
$string['emaildisable'] = 'E-mail of this user is disabled';
$string['fakemail'] = 'Generate fake mail address';
$string['skipthisline'] = 'FILE ROW SKIPPED!';
$string['fakemails'] = 'fake mail addresses generated (and mail disabled)';
$string['invalidmails'] = 'Invalid e-mail addresses';
$string['duplicatemails'] = 'Duplicate e-mail addresses';
$string['newuseradded'] = 'Added New User';
$string['usernotupdatederror'] = 'Error updating User';
$string['usernotaddedregistered'] = 'User already exists. No new user added';
$string['usernotaddederror'] = 'New User not added. Unknown Error';
$string['cannotassignroleincourse'] = 'You cannot assign role $a->role in course $a->course';
$string['assignedroleincourse'] = 'Assigned role $a->role in course $a->course';
$string['enrolledasteacher'] = 'Enrolled as Teacher to course $a';
$string['enrolledasnoneditingteacher'] = 'Enrolled as non-editing Teacher to course $a';
$string['enrolledasstudent'] = 'Enrolled as Student to course $a';
$string['enrolledwithdefaultrole'] = 'Enrolled to course $a (with default role)';
$string['mnethostidnotexists'] = 'A MNET peer with hostid=$a does not exist';
$string['truncatefield'] = 'Field $a->fieldname too long. Truncated at $a->length chars.';

$string['fieldseparator'] = 'Field separator character (text format)';
$string['comma'] = 'Comma: \",\"';
$string['semicolon'] = 'Semicolon: \";\"';
$string['pipe'] = 'Pipe: \"|\"';
$string['tab'] = 'Tab';
$string['nomailplaceholder'] = 'String for \"no mail\"';
$string['onlyalphanum'] = 'only letters and digits, no space';
$string['fakemaildomain'] = 'Fake domain to generate fake mail addresses';
$string['maxnumberofcoursefield'] = 'Max number of <code>course</code>, <code>group</code>, <code>role</code> and <code>type</code> fields';

$string['uploadfile'] = 'Upload file';
$string['confirmed'] = 'Confirmed';

$string['fakemailgeneration'] = 'Fake mail addess generation';

$string['filterctx_contextlevel_label'] = 'Only Users that in Context';
$string['filterctx_context_label'] = '';
$string['filterctx_donthaverole_label'] = '';
$string['filterctx_role_label'] = 'the Role';
$string['filterctx_have'] = 'have';
$string['filterctx_havenot'] = 'do NOT have';

$string['assignrole_contextlevel_label'] = 'In Context';
$string['assignrole_context_label'] = '';
$string['assignrole_donthaverole_label'] = 'do';
$string['assignrole_role_label'] = 'the Role';
$string['assignrole_have'] = 'ASSIGN';
$string['assignrole_havenot'] = 'UNASSIGN';
$string['assignrole_havehidden'] = 'ASSIGN (hidden)';

$string['filterbymnethost'] = 'MNET users from';

$string['localhost'] = '(local site)';
$string['anyhost'] = 'Any';

$string['CONTEXT_SYSTEM'] = 'Site';
$string['CONTEXT_COURSECAT'] = 'Course Category';
$string['CONTEXT_COURSE'] = 'Course';

$string['nouserselected'] = 'No User selected!';
$string['noactionselected'] = 'Select an action to execute';
$string['n_a'] = 'n/a';
$string['unconfirm'] = 'Cancel confirmation';

$string['deny_allowmnetaccess'] = 'Access currently Denied --> Allow';
$string['allow_denymnetaccess'] = 'Access currently Allowed --> Deny';

$string['hide_showfilterform'] = 'Hide/Show form';

$string['searchconditions'] = '<strong>Filter conditions:</strong>';
$string['nosearchcondition'] = 'No filter condition';
$string['searchbyfirstinitial'] = 'First Name begins with \'<em>$a</em>\'';
$string['searchbylastinitial'] = 'Last Name begins with \'<em>$a</em>\'';
$string['searchbystring'] = 'contain \'<em>$a</em>\'';
$string['searchbycontext'] = '$a->donthaverole the Role <em>$a->role</em> in $a->contextlevel <em>$a->context</em>';
$string['searchbyhost'] = 'remote user from <em>$a</em>';
$string['searchhave'] = 'ha';
$string['searchhavenot'] = 'NON ha';
$string['searchbyauth'] = 'authenticated by <em>$a</em>';
$string['and'] = 'AND';

$string['filterconfirmedusers'] = 'Confirmed users';
$string['filterconfirmed_all'] = 'Any';
$string['filterconfirmed_confirmedonly'] = 'Confirmed only';
$string['filterconfirmed_unconfirmedonly'] = 'Unconfirmed only';

$string['filterauth'] = 'Authentication mode';
$string['anyauth'] = 'Any';

$string['exporttheseuserstofile'] = 'Export these users to CSV file';
$string['invalidfieldname_areyousure'] = '$a is not a valid field name!<br />Check that you select the correct field Separator character';

$string['addedtogroup'] = 'Added to group $a->group in course $a->course';
$string['addedtogroupnot'] = 'Not added to group $a->group in course $a->course';
$string['addedtogroupnotenrolled'] = 'Not added to group $a->group, because not enrolled in course $a->course';

$string['willbedeleted'] = 'Will be deleted!';
?>