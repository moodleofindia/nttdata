<?php // $Id$
    
    require_once('../../config.php');
    require_once('useradminlib.php');
    require_once($CFG->libdir.'/adminlib.php');
    
    if (!confirm_sesskey()) {
        print_error('confirmsesskeybad');
    }
    
    // Login required
    require_login();
    
    // Requires capabilities to delete users
    useradmin_require_capabilities_to_delete();
    
    $delete       = optional_param('delete', 0, PARAM_INT);
    
    $returnto = './manage_user.php';
    
    /* Code below from admin/user.php */
    

    if (!$user = get_record('user', 'id', $delete)) {
        error("No such user!", '', true);
    }

    $primaryadmin = get_admin();
    if ($user->id == $primaryadmin->id) {
        error("You are not allowed to delete the primary admin user!", '', true);
    }

    if ($delete) {
        //following code is also used in auth sync scripts
        $updateuser = new object();
        $updateuser->id           = $user->id;
        $updateuser->deleted      = 1;
        $updateuser->username     = addslashes("$user->email.".time());  // Remember it just in case
        $updateuser->email        = '';               // Clear this field to free it up
        $updateuser->idnumber     = '';               // Clear this field to free it up
        $updateuser->timemodified = time();
        if (update_record('user', $updateuser)) {
            // Removing a user may have more requirements than just removing their role assignments.
            // Use 'role_unassign' to make sure that all necessary actions occur.
            role_unassign(0, $user->id);
            // remove all context assigned on this user?
            notify(get_string('deletedactivity', '', fullname($user, true)) );
        } else {
            notify(get_string('deletednot', '', fullname($user, true)));
        }
        // Redirect to calling page
        redirect($returnto, get_string('changessaved'));
    } else {
        // Redirect to calling page
        redirect($returnto, get_string('changescancelled', 'block_useradmin' ));        
    }
    
    
    print_footer();
?>
    