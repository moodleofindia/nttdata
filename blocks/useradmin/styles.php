/* $Id$ */
#blocks-useradmin-manage_user .cell {
  font-size: 0.85em !important;
  font-weight: normal !important;
} 
#blocks-useradmin-manage_user .header {
  font-size: 0.85em !important;
} 

#blocks-useradmin-manage_user .cell {
  padding: 2px 2px 2px 2px;
}

#blocks-useradmin-manage_user .cell a.dimmed {
  color: #aaaaaa !important;
}
#blocks-useradmin-manage_user .header {
  padding: 2px 2px 2px 2px;
}

.useradmin_error {
	color: #ff0000 !important;
}


/* Links w/ tooltip */
a.tooltip {
	position:relative;
}
a.tooltip span {
	display: none;
}
a.tooltip:hover span {
   /*display just on :hover state*/
   display: block;
   /*this positions it relative to the link*/
   position: absolute; 
   top: 1.2em; 
   left: -2em;
   /*you can adjust anything under here*/
   width: 15em; 
   padding: 2px;
   text-decoration: none !important;
   border: 1px solid #666666;
   background-color: #F5F5F5;
}

/* Column header tooltip */
.header a.tooltip:hover span {
	top: -2em;
	left: -1em;
	width: 12em;
}

