<?php
    
    require_once("../../config.php");
    require_once('useradminlib.php');
    
    if (!confirm_sesskey()) {
        print_error('confirmsesskeybad');
    }
    
    if (! $site = get_site()) {
        print_error("Could not find site-level course");
    }
    
    // Must be logged in and have all required capabilities
    require_login();
    
//    if (!useradmin_has_required_capabilities()) {
//        print_error("You must be an administrator to edit users this way.");
//    }

    useradmin_require_capabilities_to_assign();
    
    
    
    /// Get Parameters
    $multi_action = required_param('multi_action');
    
    if ( $multi_action == 'role_assign' ) {
        $assign_contextlevel = required_param('assign_contextlevel', PARAM_INT);
        $assign_contextinstanceid = required_param('assign_contextinstanceid', PARAM_INT);
        $assign_roleid = required_param('assign_roleid', PARAM_INT);
        $assign_unassign = required_param('assign_unassign', PARAM_INT);
    }
        
    $users 		 = optional_param('userid', array(), PARAM_INT); 	// array of user id
    $returnto    = required_param('returnto', PARAM_LOCALURL);  	// Calling page URL
    $cancel 	 = optional_param('cancel');						// Cancel action
    
    
    // Get Context and Role
    if ( $multi_action == 'role_assign' ) {
        $context = get_context_instance($assign_contextlevel, $assign_contextinstanceid);
        if ( !$context ) {
            print_error("Could not find Context",'block_useradmin');
        }
        $role = useradmin_get_role($assign_roleid);
        if ( !$role ) {
            print_error("Could not find Role",'block_useradmin');
        }
        
        // Get Language strings for Context and Role
        $contextlevels = useradmin_get_context_levels();
        $strcontextlevel =  $contextlevels[$assign_contextlevel];
        $contexts = useradmin_get_contexts_by_level($assign_contextlevel);
        $strcontext = $contexts[$assign_contextinstanceid];
        $roles = useradmin_get_available_roles($assign_contextlevel, $assign_contextinstanceid);
        $strrole = $roles[$assign_roleid];
    }    
    
    
    if ( $multi_action == 'delete' ) {
        $straction = get_string('deleteselectedusers', 'block_useradmin');
    } else if ($multi_action == 'role_assign') {
        if ($assign_unassign == 1) {
            $straction = get_string('unassignrole', 'block_useradmin');
        } else if ($assign_unassign == 2) {
             $straction = get_string('assignrolehidden', 'block_useradmin');
        } else {
            $straction = get_string('assignrole', 'block_useradmin');
        }
        $straction .= ' '. $strrole .' '. get_string('inthecontextof', 'block_useradmin') .' '. $strcontextlevel .': '. $strcontext .' ';        
    }
    
    
    
    /// On second submit, actually process enrolments
    if ((count($users) > 0) and ($form = data_submitted()) and confirm_sesskey()) {
        // Check cancel
        if ( $cancel ) {
            // Redirect to calling page
            redirect($returnto, get_string('changescancelled', 'block_useradmin' ));
        } else {
            // For each "userd" in the form...
            foreach ($form->userid as $userid => $value) {
                 
                
                if ( $multi_action == 'role_assign' ) {
                     // Check if user has/has not the role in the context
                    $user_has_role = userdmin_user_has_role_in_context($userid, $context->id, $role->id);
                    	
                    // Assign or Unassign role
                    if ( $assign_unassign == 1 && $user_has_role ) {
                        role_unassign( $role->id, $userid, 0, $context->id );
                    } else if ( $assign_unassign == 0 && !$user_has_role ) {
                        role_assign( $role->id, $userid, 0, $context->id );                                   
                    } else if ( $assign_unassign == 2 && !$user_has_role ) {
                        role_assign( $role->id, $userid, 0, $context->id, 0, 0, 1 );   // Assign as Hidden                                
                    }
                } else if ( $multi_action == 'delete') {
                    
                    // (Code beloy from admin/user.php)
                    
                    // Get user
                    if (!$usertodelete = get_record('user', 'id', $userid)) {
                        error("No such user!", '', true);
                    }
                    
                    // Avoid deleting Primary admin
                    $primaryadmin = get_admin();
                    if ($usertodelete->id == $primaryadmin->id) {
                        error("You are not allowed to delete the primary admin user!", '', true);
                    }
                        
                    //following code is also used in auth sync scripts
                    $updateuser = new object();
                    $updateuser->id           = $usertodelete->id;
                    $updateuser->deleted      = 1;
                    $updateuser->username     = addslashes("$usertodelete->email.".time());  // Remember it just in case
                    $updateuser->email        = '';               // Clear this field to free it up
                    $updateuser->idnumber     = '';               // Clear this field to free it up
                    $updateuser->timemodified = time();
                    if (update_record('user', $updateuser)) {
                        // Removing a user may have more requirements than just removing their role assignments.
                        // Use 'role_unassign' to make sure that all necessary actions occur.
                        role_unassign(0, $usertodelete->id);
                        // remove all context assigned on this user?
                        notify(get_string('deletedactivity', '', fullname($usertodelete, true)) );
                    } else {
                        notify(get_string('deletednot', '', fullname($usertodelete, true)));
                    }
                }
            }
    
            // Redirect to calling page
            redirect($returnto, get_string('changessaved'));
        }
    }
    
    /// On first submit, show table of students to enrol
	$strblockname = get_string('blockname','block_useradmin');
	
    $navlinks = array();
    $navlinks[] = array('name' => $strblockname, 'link' => 'manage_user.php', 'type' => 'misc');
    $navlinks[] = array('name' => $straction, 'link' => '', 'type' => 'misc');
    $navigation = build_navigation($navlinks);
    
    print_header("$site->shortname: $straction",
                $site->fullname,
//                $straction,
			    $navigation,
                "", "", true, "&nbsp;", navmenu($site));
    
    print_heading("$straction");
    
    echo "<form method=\"post\" action=\"multiuserassignment.php\" name=\"form\">\n";
    echo '<input type="hidden" name="sesskey" value="'.$USER->sesskey.'" />';
    echo '<input type="hidden" name="returnto" value="'.$returnto.'" />';
    
    if ( $multi_action == 'role_assign' ) {
        echo '<input type="hidden" name="assign_contextlevel" value="'.$assign_contextlevel.'" />';
        echo '<input type="hidden" name="assign_contextinstanceid" value="'.$assign_contextinstanceid.'" />';
        echo '<input type="hidden" name="assign_roleid" value="'.$assign_roleid.'" />';
        echo '<input type="hidden" name="assign_unassign" value="'.$assign_unassign.'" />';
    }
    echo '<input type="hidden" name="multi_action" value="'.$multi_action.'" />';
    
    $table->head  = array (get_string('fullname'),
                get_string('email'),
                get_string('institution'),
                get_string('department'),
                get_string('city'),
                "");
    $table->align = array ('left', 'left', 'left', 'left', 'left', 'center');
    $table->width = "90%";
    foreach ($_POST as $paramname => $paramvalue) {
        if (preg_match('/^user(\d+)$/',$paramname,$matches)) {
            $userid = $matches[1];
    
            // Simply skip users not existing
            if ( !($user = get_record("user","id", $userid)) ) {
                continue;
            }
    
            $message = 'ok';
            $isok = true;
            	
            if ( $multi_action == 'role_assign' ) {
                // Check if user has/has not the role in the context
                $user_has_role = userdmin_user_has_role_in_context($userid, $context->id, $role->id);
                if ( $user_has_role && !$assign_unassign ) {
                    $message = '<span class="useradmin_error">'.get_string('alreadyassigned', 'block_useradmin').'</span>';
                    $isok = false;
                } else if ( !$user_has_role && $assign_unassign ) {
                    $message = '<span class="useradmin_error">'.get_string('notassigned', 'block_useradmin').'</span>';
                    $isok = false;
                }
            } else if ($multi_action == 'delete' ) {
                $message = '<span class="useradmin_error">'.get_string('willbedeleted', 'block_useradmin').'</span>';
            }
            	
            $table->data[] = array(
            fullname($user, true),
            $user->email,
            $user->institution,
            $user->department,
            $user->city,
            $message. '<input type="hidden" name="userid['.$userid.']" value="1" />' );
             
        }
    }
    print_table($table);
    
    echo "\n<div style=\"text-align: center; width: 100%;\">";
    if ($multi_action == 'delete') {
        echo '<br/><div class="useradmin_error">'.get_string('userswillbedeleted', 'block_useradmin').'</div><br/>';
    }    
    echo "<input type=\"submit\" name=\"cancel\" value=\"".get_string('cancel')."\"  />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
    echo "<input type=\"submit\" name=\"execute\" value=\"".get_string('execute','block_useradmin' )."\" />";
    if ( $multi_action == 'role_assign') {
        echo ' ('.get_string('wrongwillbeskipped', 'block_useradmin').')';
    } 
        
    echo "</div>";
    echo "</form>\n";
    
    
    print_footer($site);

?>