$Id
///////////////////////////////////////////////////////////////////////////
//                                                                       //
// NOTICE OF COPYRIGHT                                                   //
//                                                                       //
// Moodle - Modular Object-Oriented Dynamic Learning Environment         //
//          http://moodle.org                                            //
//                                                                       //
// Copyright (C) 1999-2004  Martin Dougiamas  http://dougiamas.com       //
//                                                                       //
///////////////////////////////////////////////////////////////////////////
//                                                                       //
// USER ADMINISTRATION block                                             //
// Copyright (C) 2007-2008  Lorenzo Nicora                               //
//                                                                       //
///////////////////////////////////////////////////////////////////////////
//                                                                       //
// This program is free software; you can redistribute it and/or modify  //
// it under the terms of the GNU General Public License as published by  //
// the Free Software Foundation; either version 2 of the License, or     //
// (at your option) any later version.                                   //
//                                                                       //
// This program is distributed in the hope that it will be useful,       //
// but WITHOUT ANY WARRANTY; without even the implied warranty of        //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         //
// GNU General Public License for more details:                          //
//                                                                       //
//          http://www.gnu.org/copyleft/gpl.html                         //
//                                                                       //
///////////////////////////////////////////////////////////////////////////

Useradmin Block (Gestione Utenti avanzata)
Versione per Moodle 1.9+
===========================================================================
Sviluppato da
	Lorenzo Nicora
	
	ringraziamente al CESVOT - Centro Servizi Volontariato Toscana  - 
	per aver permesso questo lavoro

---------------------------------------------------------------------------
INSTALLAZIONE:
	1) Unzippare il contenuto dello .zip nella directory principale 
	   di Moodle.
	   Attenzione: se lo zip che avete in mano si unzippa in un'unica directory
	   che si chiama "useradmin" o simili, contenente a sua volta un'unica
	   sottodirectory "blocks", allora dovete unzippare in una directory 
	   temporanea e poi copiare il _contenuto_ della directory principale 
	   (da "blocks" compreso in poi) in Moodle.
	2) Autenticati come Amministratore e vai in 
	   Amministrazione Sito->Notifiche per completare l'installazione.
	   Non installa nulla nel database di Moodle e si deve solo registrare 
	   come Blocco. 
	3) Aggiungi un blocco Gestione Utenti nella home page del sito.
	   Consiglio di portarlo più in alto possibile in modo da non dover 
	   scorrere la pagina per raggiungerlo.
	   Il blocco sarà visibile solo agli Amministratori
	4) E' consigliabile aggiungere il blocco anche nell'area Amministrazion
	   del Sito. Sarà utile per tornare velocemente all'elenco degli utenti
	   dopo avere, ad esempio, modificato il profilo di un utente.
---------------------------------------------------------------------------	   
Autorizzazioni necessarie
	Per vedere questo blocco, accedere al Elenco Utenti e allo scaricameno
	utenti su file, l'utente deve avere un Ruolo con le seguenti 
	autorizzazioni:
		moodle/site:accessallgroups	   
		moodle/site:viewfullnames
		moodle/user:viewdetails
		moodle/user:viewhiddendetails
		moodle/role:viewhiddenassigns

	Per Modificare gli utenti deve avere anche le seguenti 
	autorizzazioni:
		moodle/user:update
		moodle/user:editprofile
	
	Per Creare nuovi utenti:
		moodle/user:create
	
	Per Cancellare utenti:
		moodle/user:delete
		
	Per Assegnare/Disassegnare ruoli:
		moodle/role:assign
	
	Per caricare utenti da file:
		moodle/site:uploadusers
		moodle/user:update
		moodle/user:create
		moodle/role:assign
		
	Queste autorizzazioni devono essere possedute nel contesto 
	SYSTEM (sito).
	Di solito, solo gli Amministratori del sito hanno tutte 
	queste autorizzazioni.			   
