<?php  // $Id$
    /**
     * IFRAME containing context->role select chain
     * Corresponding fields in parent frame are updated on Submit of this form
     * (note that changes of all select except "donthaverole" cause auto-submit of this form)
     *
     * "callback" fields elementID in the parent frame are passed by parameters:
     *   parent_contextlevel, parent_contextinstanceid, parent_donthaverole, parent_roleid
     *
     * Default values at form load are passed by paramers:
     *   _contextlevel, _contextinstanceid, _donthaverole, _roleid
     *
     * "donthaverole" field may be disabled passing parameter disabledonthaverole=1
     *
     * Optional parameter "strprefix" is prepended to all i18n string names, if defined
     */
    
    
    require_once('../../config.php');
    require_once('useradminlib.php');
    
    
    require_login();
    
    if (!useradmin_has_capabilities_to_list()) {
        error("You must be an administrator to edit users this way.");
    }
    
    
    /// Get parameters
    
    // Is "donthaverole" disabled?
    // FIXME this should be useless now
    $disabledonthaverole = optional_param('disabledonthaverole',  0, PARAM_BOOL);
    
    
    // i18n String prefix
    $strprefix = optional_param('strprefix', '');
    
    // Elements ID of the parent form hidden fields, corresponding to the fields in this form
    $parent_contextlevel      = required_param('parent_contextlevel');
    $parent_contextinstanceid = required_param('parent_contextinstanceid');
    $parent_roleid            = required_param('parent_roleid');
    if ( !$disabledonthaverole ) {
        $parent_donthaverole      = required_param('parent_donthaverole');
    }
    
    // Fields value (default values at the first load)
    $contextlevel      = optional_param('_contextlevel', '', PARAM_INT);
    $contextinstanceid = optional_param('_contextinstanceid', '', PARAM_INT);
    $roleid       	   = optional_param('_roleid','', PARAM_INT);
    if ( !$disabledonthaverole ) {
        $donthaverole 	   = optional_param('_donthaverole',  0, PARAM_INT);
    }
    
    // If ContextLevel changed, clear ContextInstanceID
    if ( $contextlevel != optional_param('_prev_contextlevel', $contextlevel, PARAM_INT) ) {
        $contextinstanceid = '';
    }
    // If ContextInstanceID changed, clear RoleID
    if ( $contextinstanceid != optional_param('_prev_contextinstanceid', $contextinstanceid, PARAM_INT) ) {
        $roleid = '';
    }
    
    print_header();
    
    
    echo "<script type=\"text/javascript\" >
    			function openerElement(id) {
    				return parent.document.getElementById(id);
    			}
    
        		function updateMainForm() {
    				openerElement('$parent_contextlevel').value = document.getElementById('_contextlevel').value;
    				openerElement('$parent_contextinstanceid').value = document.getElementById('_contextinstanceid').value;";
    if ( !$disabledonthaverole ) {
        echo "       openerElement('$parent_donthaverole').value = document.getElementById('_donthaverole').value;";
    }
    echo "		openerElement('$parent_roleid').value = document.getElementById('_roleid').value;
    				return true;
    			}
    		</script>
     	    ";
    
     
    echo "<form id=\"ctxfilterform\" action=\"contextfilterframe.php\" method=\"get\" onSubmit=\"return updateMainForm()\" >";
    
    // Post back previous filter values
    echo "<input type=\"hidden\" name=\"_prev_contextlevel\" value=\"$contextlevel\" />";
    echo "<input type=\"hidden\" name=\"_prev_contextinstanceid\" value=\"$contextinstanceid\" />";
    
    // Pass thru disabledonthaverole and strprefix
    echo "<input type=\"hidden\" name=\"disabledonthaverole\" value=\"$disabledonthaverole\" />";
    echo "<input type=\"hidden\" name=\"strprefix\" value=\"$strprefix\" />";
    
    // Passthru parent (callback) field
    echo "<input type=\"hidden\" name=\"parent_contextlevel\" value=\"$parent_contextlevel\" />";
    echo "<input type=\"hidden\" name=\"parent_contextinstanceid\" value=\"$parent_contextinstanceid\" />";
    echo "<input type=\"hidden\" name=\"parent_roleid\" value=\"$parent_roleid\" />";
    echo "<input type=\"hidden\" name=\"parent_donthaverole\" value=\"$parent_donthaverole\" />";
    
    // TODO why it ignores some styles in the IFRAME
    echo '<span style="font-size: 80%; font-family: Arial, Helvetica, sans-serif;">';
    
    /// Render fields
    
    // TODO think about reimplementing with 'choose_from_menu_nested()', removing the IFRAME
    echo get_string("${strprefix}contextlevel_label",'block_useradmin') . ' ';
    $contextlevels = useradmin_get_context_levels();
    choose_from_menu($contextlevels, "_contextlevel", $contextlevel, 'choose',
                    "updateMainForm(); document.getElementById('ctxfilterform').submit();",
                    '0', false, false, 0, '_contextlevel');
    
    echo get_string("${strprefix}context_label",'block_useradmin') . ' ';
    $contexts = useradmin_get_contexts_by_level($contextlevel);
    choose_from_menu($contexts, "_contextinstanceid", $contextinstanceid, 'choose',
                    "updateMainForm(); document.getElementById('ctxfilterform').submit();",
                    '0', false, false, 0, '_contextinstanceid' );
    if ( !$disabledonthaverole ) {
        echo ' ';
        $optionshaveornot = array();
        $optionshaveornot[0] = get_string("${strprefix}have", "block_useradmin");
        $optionshaveornot[1] = get_string("${strprefix}havenot", "block_useradmin");
        if ( ($havehiddenstr = get_string("${strprefix}havehidden", "block_useradmin")) != "[[${strprefix}havehidden]]"  ) {
            $optionshaveornot[2] = $havehiddenstr;
        }
        
        echo get_string("${strprefix}donthaverole_label",'block_useradmin') . ' ';
        choose_from_menu($optionshaveornot, "_donthaverole", $donthaverole,'',
                    "updateMainForm();",
                    '0', false, false, 0, "_donthaverole" );
    }
    
    echo ' '.get_string("${strprefix}role_label","block_useradmin"). ' ';
    $available_roles = useradmin_get_available_roles($contextlevel, $contextinstanceid);
    choose_from_menu($available_roles, "_roleid", $roleid, 'choose',
                    "updateMainForm(); document.getElementById('ctxfilterform').submit();",
                    '0', false, false, 0, "_roleid" );
    
    echo '</span>';                  
    echo "</form>";
?>