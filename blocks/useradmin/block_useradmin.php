<?php //$Id$
require_once('useradminlib.php');

class block_useradmin extends block_list {
    function init() {
        $this->title = get_string('blockname','block_useradmin');
        $this->version = 2008072500;
    }

//    /**
//     * Check permissions 
//     * @return boolean True for access / False for denied
//     * @author Mark Nielsen
//     **/
//    function check_permission() {
//		return $this->check_required_roles();
//    }
    
    
    function get_content() {

        if($this->content !== NULL) {
            return $this->content;
        }

        $this->content = new stdClass;
        $this->content->items = array();
        $this->content->icons = array();
        $this->content->footer = '';

        // Show block only if the user has base roles needed
		if ( useradmin_has_capabilities_to_list() ) {
			$this->load_content();
        }		
        return $this->content;
    }

    function load_content() {
        global $CFG;
    	
        $this->content->items[] = '<a href="'.$CFG->wwwroot.'/blocks/useradmin/manage_user.php?show_fullform=1">'.get_string('userlist','block_useradmin').'</a>';
        $this->content->icons[] = '<img src="'.$CFG->pixpath.'/i/users.gif" alt="" />';
        
        // Check additional capabilities to upload users
        if ( useradmin_has_capabilities_to_upload() ) {
            $this->content->items[] = '<a href="'.$CFG->wwwroot.'/blocks/useradmin/uploaduser.php">'.get_string('uploaduser','block_useradmin').'</a>';
            $this->content->icons[] = '<img src="'.$CFG->pixpath.'/i/restore.gif" alt="" />';
        }
        
        $this->content->items[] = '<a href="'.$CFG->wwwroot.'/blocks/useradmin/downloaduser.php">'.get_string('downloaduser','block_useradmin') .'</a>';
        $this->content->icons[] = '<img src="'.$CFG->pixpath.'/i/backup.gif" alt="" />';
    }


    /**
     * Site only
     */
    function applicable_formats() {
        if ( useradmin_has_capabilities_to_list() ) {
            return array('site' => true, 'admin' => true);
        } else {
            return array('site' => true );
        }
    }
    
//    /**
//     * Check if current user has all required roles
//     * As admin block allows many operations, required roles are:
//     *   moodle/user:create
//     *   moodle/user:delete
//     *   moodle/user:update
//     *   moodle/user:viewdetails
//     *   moodle/role:assign
//     */
//    function check_required_roles() {
//        return useradmin_has_required_capabilities();
//    }

    
}

?>
