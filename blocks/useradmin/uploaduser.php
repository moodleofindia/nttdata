<?php // $Id$
	
	/// Bulk user registration script from a comma separated file
	/// Returns list of users with their user ids
	/// Based on admin/userupload.php.
	/// Modified by Lorenzo Nicora and included in useradmin block
	
	require_once('../../config.php');
	require_once($CFG->libdir.'/uploadlib.php');
	require_once('./useradminlib.php');
	
	if (! $site = get_site()) {
		error("Could not find site-level course");
	}
	if (!$adminuser = get_admin()) {
		error("Could not find site admin");
	}
	
	require_login();

	// Require Upload User capability
//	require_capability('moodle/site:uploaduers', get_context_instance(CONTEXT_SYSTEM));
    useradmin_require_capabilities_to_upload();
	
	
	
	/// Set defaults
	$defaultseparatoropt = 0;
	if (isset($CFG->CSV_DELIMITER)) {
		foreach ($fieldseparatoroptions as $opt => $sep) {
			if ( $CFG->CSV_DELIMITER == $sep ) {
				$defaultseparatoropt = $opt;
			}
		}
	}
	
	$defaultnomail = 'NOMAIL';
	if ( isset($CFG->CSV_NOMAIL) ) {
		$defaultnomail = $CFG->CSV_NOMAIL;
	}
	
	$defaultfakedomain = 'NO.MAIL';
	if ( isset($CFG->CSV_FAKEMAILDOMAIN) ) {
		$defaultfakedomain = $CFG->CSV_FAKEMAILDOMAIN;
	}
	
	$defaultmaxnumberofcoursefield = 5;
	$defaultfilencodingopt = 0; // UTF-8
	
	// Get parameters
	$createpassword 	= optional_param('createpassword', 0, PARAM_BOOL);
	$updateaccounts 	= optional_param('updateaccounts', 0, PARAM_BOOL);
	$allowrenames   	= optional_param('allowrenames', 0, PARAM_BOOL);
	$separatoropt		= optional_param('separatoropt', $defaultseparatoropt, PARAM_INT);
	$csv_nomail 		= optional_param('nomail', $defaultnomail, PARAM_ALPHANUM);
	$csv_fakemaildomain = optional_param('fakemaildomain', $defaultfakedomain, PARAM_HOST);
	$maxnumberofcoursefield = optional_param('maxnumberofcoursefield', $defaultmaxnumberofcoursefield, PARAM_INT);
	$filencodingopt     = optional_param('filencodingopt', $defaultfilencodingopt, PARAM_INT );
	
	// CSV separator and encoding
	if ( !array_key_exists($separatoropt, $fieldseparatoroptions)  ) {
		error("Invalid field separator");
	}
	$csv_delimiter2 = $fieldseparatoroptions[$separatoropt];
	$csv_delimiter = "\\".$csv_delimiter2;
	$csv_encode = '/\&\#' . $separatorencodings[$separatoropt] . '/';
	
	// Need to convert to UTF-8?
	$latin2utf8 = False;
	if ( $filencodingopt ) {
	    $latin2utf8 = True;
	}
	
	$streditmyprofile = get_string("editmyprofile");
	$stradministration = get_string("administration");
	$strfile = get_string("file");
	$struser = get_string("user");
	$strusers = get_string("users");
	$strusersnew = get_string("usersnew");
	$strusersupdated = get_string("usersupdated");
	$struploadusers = get_string("uploaduser", 'block_useradmin');
	$straddnewuser = get_string("importuser");
	
	
	
	/// Print the header
	$struploaduser = get_string('uploaduser','block_useradmin');
	$strblockname = get_string('blockname','block_useradmin');
	
    $navlinks = array();
    $navlinks[] = array('name' => $strblockname, 'link' => 'manage_user.php', 'type' => 'misc');
    $navlinks[] = array('name' => $struploaduser, 'link' => '', 'type' => 'misc');
    $navigation = build_navigation($navlinks);
	
	print_header($site->fullname.': '.$struploaduser,
	$site->fullname,
//	"$struploaduser",
	$navigation,
	'', '', true);
	
	
	/// If a file has been uploaded, then process it
	
	
	$um = new upload_manager('userfile',false,false,null,false,0);
	
	if ($um->preprocess_files() && confirm_sesskey()) {
		$filename = $um->files['userfile']['tmp_name'];
	
		// Large files are likely to take their time and memory. Let PHP know
		// that we'll take longer, and that the process should be recycled soon
		// to free up memory.
		@ini_set('max_execution_time',0);
		@raise_memory_limit("128M");
		if (function_exists('apache_child_terminate')) {
			@apache_child_terminate();
		}
	
		/// Fix file
		$text = my_file_get_contents($filename);
	
		// Trim UTF-8 BOM
		$textlib = new textlib();
		$text = $textlib->trim_utf8_bom($text);
	
		//Fix mac/dos newlines
		$text = preg_replace('!\r\n?!',"\n",$text);
	
		// Save the file back
		$fp = fopen($filename, "w");
		fwrite($fp,$text);
		fclose($fp);
	
		// Reopen the file
		$fp = fopen($filename, "r");
	
		// make arrays of valid fields for error checking
		$required = array(  "username" => 1,
                    		"password" => !$createpassword,
                    		"firstname" => 1,
                    		"lastname" => 1,
                    		"email" => 1);
		$optionalDefaults = array(  "mnethostid" => 1,
                            		"institution" => 1,
                            		"department" => 1,
                            		"city" => 1,
							"location" => 1,
							"grade" => 1,
							"vertical" => 1,
							"account" => 1,
							"empid" => 1,									
                            		"country" => 1,
                            		"lang" => 1,
                            		"auth" => 1,
                            		"timezone" => 1);
		$optional = array(  "idnumber" => 1,
                    		"icq" => 1,
                    		"skype" => 1,
                    		"yahoo" => 1,
                    		"msn" => 1,
                    		"aim" => 1,
                    		"phone1" => 1,
                    		"phone2" => 1,
                    		"address" => 1,
                    		"url" => 1,
                    		"description" => 1,
                    		"mailformat" => 1,
                    		"maildisplay" => 1,
                    		"htmleditor" => 1,
                    		"autosubscribe" => 1,
                    		"password" => $createpassword,
                    		"oldusername" => $allowrenames,
                    		"emailstop" => 1,
                    		"trackforums" => 1,
                    		"screenreader" => 1 );
	
            
	    // String fields length        
		$field_length = array ( "username" => 100,
				    		"password" => 32,
				    		"firstname" => 100,
				    		"lastname" => 100,
				    		"email" => 100,
				    		"institution" => 40,
				    		"department" => 30,
				    		"city" => 50,
							"location" => 50,
							"grade" => 10,
							"vertical" => 255,
							"account" => 255,
							"empid" => 20,
				    		"lang" => 30,
				    		"auth" => 20,
				    		"timezone" => 100,
				    		"idnumber" => 64,
				    		"icq" => 15,
				    		"skype" => 50,
				    		"yahoo" => 50,
				    		"msn" => 50,
				    		"aim" => 50,
				    		"phone1" => 20,
				    		"phone2" => 20,
				    		"address" => 70,
				    		"url" => 255,
				    		"description" => 255,
				    		"oldusername" => 100);
                    		
		// course, group, type and role fields
		for( $c=1; $c<=$maxnumberofcoursefield; $c++) {
			$optional["course$c"] = 1;
			$optional["group$c"] = 1;
			$optional["type$c"] = 1;
			$optional["role$c"] = 1;
		}
	
		// --- get header (field names) ---
		$header = split($csv_delimiter, fgets($fp,1024));
		// check for valid field names
		foreach ($header as $i => $h) {
			$h = trim($h); $header[$i] = $h; // remove whitespace
			if (!(isset($required[$h]) or isset($optionalDefaults[$h]) or isset($optional[$h]))) {
				error(get_string('invalidfieldname_areyousure', 'block_useradmin', $h), 'uploaduser.php?sesskey='.$USER->sesskey);
			}
			if ($required[$h]) {
				$required[$h] = 0;
			}
		}
		// check for required fields
		foreach ($required as $key => $value) {
			if ($value) { //required field missing
				error(get_string('fieldrequired', 'error', $key), 'uploaduser.php?sesskey='.$USER->sesskey);
			}
		}
	
		$linenum = 1; // since header is line 0
	
		// Prepare counts
		$usersnew     = 0;
		$usersupdated = 0;
		$userserrors  = 0;
		$renames      = 0;
		$renameerrors = 0;
		$fakemails    = 0;
		$invalidmails = 0;
		$duplicatemails= 0;
	
		// Will use this course array a lot
		// so fetch it early and keep it in memory
		$courses = get_courses('all','c.sortorder','c.id,c.shortname,c.fullname,c.sortorder,c.teacher');
	
		// Preload all available Roles
		$roles = get_records('role','','','','id, shortname, name');
		
		while (!feof ($fp)) {
	
			echo '<hr />';
	
			// setup optional-defaults fields using administrator data
			foreach ($optionalDefaults as $key => $value) {
				$user->$key = addslashes($adminuser->$key);
			}
	
			//Note: separator within a field should be encoded as &#XX (for semicolon separated csv files)
			$line = split($csv_delimiter, fgets($fp,1024));
			foreach ($line as $key => $value) {
				//decode encoded separator
				$record[$header[$key]] = preg_replace($csv_encode,$csv_delimiter2,trim($value));
			}
			if ($record[$header[0]]) {    // The line is not empty
	
				// add fields to object $user
				foreach ($record as $name => $value) {
					// If needed, convert to UTF8
					if ( $latin2utf8 ) {
					    $value = utf8_encode($value);    
					}
				    
                    // Trim fields
                   	$value = trim($value);
                   	// Truncate string fields
                   	if ( isset($field_length[$name]) && strlen($value) > $field_length[$name] ) {
                    	$value = substr($value, 0, $field_length[$name] ); 
                    	$a = new object();
                    	$a->fieldname = $name;
                   		$a->length = $field_length[$name];
                    	useradmin_uploaduser_notify_error($linenum, get_string('truncatefield','block_useradmin', $a), NULL, NULL, NULL  );
                    }
					
                    // TODO add other fields validation
                    
					// check for required values
					if ($required[$name] and !$value) {
						error(get_string('missingfield', 'error', $name). " ".
						get_string('erroronline', 'error', $linenum) .". ".
						get_string('processingstops', 'error'),
						'uploaduser.php?sesskey='.$USER->sesskey);
					}
	
					// password (needs to be encrypted)
					else if ($name == "password" && !empty($value)) {
						$user->password = hash_internal_user_password($value);
					}
					// Username (escape and force lowercase)
					else if ($name == "username") {
						$user->username = addslashes(moodle_strtolower($value));
					}
					// normal entry (escape only)
					else {
						$user->{$name} = addslashes($value);
					}
				}
	
				// By default the user is confirmed and modified now
				$user->confirmed = 1;
				$user->timemodified = time();
	
				$linenum++;
	
				$username = $user->username;
	
				/// Gets course, group, type and role fields
	
				$addcourse = array();
				$addgroup = array();
				$addtype = array();
				$addrole = array();
				$course = array();
				$role = array();	
				for( $c=1; $c<=$maxnumberofcoursefield; $c++) {
					// consider groupX, typeX and roleX fields only if courseX is defined
					if ( isset($user->{"course$c"}) ) {
						$addcourse[$c] = $user->{"course$c"};
						$addgroup[$c] = $user->{"group$c"};
						$addtype[$c] = $user->{"type$c"};
						$addrole[$c] = $user->{"role$c"};
					}
					// TODO warning if groupX or typeX or roleX, but no courseX has been specified
					// TODO warning if both typeX and roleX are specified
	
					$course[$i]=NULL;
				}
	
				// Gets shortname of the course
				foreach ($courses as $eachcourse) {
					for ($i=0; $i<$maxnumberofcoursefield; $i++) {
						if ($eachcourse->shortname == $addcourse[$i]) {
							$course[$i] = $eachcourse;
						}
					}
					// We do not need to warn for unmatching courses here, 
					// as $addcourse[$i] without $course[$i] will be checked later
				}
				// Get shortname of the role
				foreach ( $roles as $eachrole ) {
				    for ($i=0; $i<$maxnumberofcoursefield; $i++) {
				        if ( $addrole[$i] && $eachrole->shortname == $addrole[$i] )  {
				            $role[$i] = $eachrole;
				        }
				    }
				}
	
				// check if truing to upload 'changeme' user. If not, skip the line
				if ($user->username === 'changeme') {
					useradmin_uploaduser_notify_error( $linenum, get_string('invaliduserchangeme', 'admin'), NULL, $user->username, TRUE );
					
					$userserrors++;
					
					continue; // Skip line
				}
	
				// Check No-mail, set fakemail and disable
				if ( $user->email == $csv_nomail ) {
					$user->email = $user->username . '@' . $csv_fakemaildomain;
					
					// Force emailstop and disable maildisplay
					$user->emailstop = 1;
					$user->maildisplay = 0;
					useradmin_uploaduser_notify_success($linenum, get_string('fakemail', 'block_useradmin') .": $user->email, ".get_string('emaildisable', 'block_useradmin'), NULL, $user->username );
	                
					$fakemails++;
				}
				// If a real mail has been specified, check it is a valid address (if not, skip line)
				else if ( !validate_email( $user->email ) ) {
					useradmin_uploaduser_notify_error($linenum, get_string('invalidemail').": $user->email", NULL, $user->username, TRUE  );
	
					$invalidmails++;
					$userserrors++;					
					continue;  // Skip line
				}
				// Check duplicate mail
				else if ($otheruser = get_record("user", "email", $user->email )) {
					if ($otheruser && $otheruser->username <> $user->username) {
						useradmin_uploaduser_notify_error($linenum, get_string("emailexists").": $user->email", NULL, $user->username, TRUE );
	
						$duplicatemails++;
						$userserrors++;						
						continue; // Skip line
					}
				}
				
				// If mnethost ist not localhost, check if mnethost exist
				if ( $user->mnethostid != $CFG->mnet_localhost_id && !record_exists('mnet_host','id',$user->mnethostid) ) {
					useradmin_uploaduser_notify_error($linenum, get_string('mnethostidnotexists', 'block_useradmin', $user->mnethostid), NULL, $user->username, TRUE );

					$userserrors++;
				    continue;
				}
	
				// before insert/update, check whether we should be updating
				// an old record instead (if allowrenames)
				if ($allowrenames && !empty($user->oldusername) ) {
					$user->oldusername = moodle_strtolower($user->oldusername);
//					if ($olduser = get_record('user','username',$user->oldusername)) {
                    if ($olduser = get_record('user', 'username', $user->oldusername, 'mnethostid', $user->mnethostid)) {
						// Immediately rename the user
						if (set_field('user', 'username', $user->username, 'username', $user->oldusername)) {
							useradmin_uploaduser_notify_success($linenum, get_string('userrenamed', 'admin')." : $user->oldusername ---> $user->username", NULL, $user->username  );
							$renames++;
						}
						else {
							// An error is probably caused by violation of unique key (username+mnethosti)
							useradmin_uploaduser_notify_error($linenum, get_string('usernotrenamedexists', 'error')." : $user->oldusername -X--> $user->username", NULL, $user->username, TRUE );

							$renameerrors++;
							continue; // skip line
						}
					}
					// If the user you are trying to rename does not exists, skip line 				
					else {
						useradmin_uploaduser_notify_error($linenum, get_string('usernotrenamedmissing', 'error')." : $user->oldusername -?--> $user->username", NULL, $user->username, TRUE );

						$renameerrors++;
						continue; // skip line
					}
				}
	
				// Check if username already exists
				if ($olduser = get_record("user","username",$username)) {
					// If update is allowed, update record
					$user->id = $olduser->id;
					if ($updateaccounts) {
						// Record is being updated
						if (update_record('user', $user)) {
							useradmin_uploaduser_notify_success($linenum, get_string('useraccountupdated', 'admin') , $user->id, $user->username );
							$usersupdated++;
						} else {
							useradmin_uploaduser_notify_error($linenum, get_string('usernotupdatederror', 'block_useradmin'), $user->id, $user->username, TRUE );
							$userserrors++;
							continue;
						}
					}
					// If update is not allowed, skip line. 
					else {
						useradmin_uploaduser_notify_error($linenum, get_string('usernotaddedregistered', 'block_useradmin'), $user->id, $user->username, FALSE );
						$userserrors++;
						// Do not skip line, as enrolments and groups should be processed 
					}
				}
				// username does not exists, so create a new user 
				else { // new user
					if ($user->id = insert_record("user", $user)) {
						useradmin_uploaduser_notify_success($linenum, get_string('newuseradded','block_useradmin'), $user->id, $user->username );
						$usersnew++;
						if (empty($user->password) && $createpassword) {
							// passwords will be created and sent out on cron
							insert_record('user_preferences', array( userid => $user->id,
										'name'   => 'create_password',
										'value'  => 1));
							insert_record('user_preferences', array( userid => $user->id,
										'name'   => 'auth_forcepasswordchange',
										'value'  => 1));
						}
					} else {
						// Record not added -- possibly some other error
						useradmin_uploaduser_notify_error($linenum, get_string('usernotaddederror', 'block_useradmin'), $user->id, $user->username, TRUE );
						$userserrors++;
						continue;
					}
				}
				
				/// Process courses, groups and roles
				
				// Check if required courses match with any existing course
				for ($i=0; $i<$maxnumberofcoursefield; $i++) {
					if ($addcourse[$i] && !$course[$i]) {
						// If the course does not exist, just warn as group, role and type will not be processed if $course[$i] is empty
						useradmin_uploaduser_notify_error($linenum, get_string('unknowncourse', 'error', $addcourse[$i]), $user->id, $user->username );
					}
				}
				
				// Get groups 
				// (Just retrieve them and warn if do not exist, actual adding to group will happen later)
				for ($i=0; $i<$maxnumberofcoursefield; $i++) {
					$groupid[$i] = 0;
					if ($addgroup[$i]) {
						if (!$course[$i]) {
							useradmin_uploaduser_notify_error($linenum, get_string('coursegroupunknown','error',$addgroup[$i]), $user->id, $user->username );
						} else {
	//						if ($group = get_record("groups","courseid",$course[$i]->id,"name",$addgroup[$i])) {
	                        if ($gid = groups_get_group_by_name($course[$i]->id, $addgroup[$i])) {
	       						$groupid[$i] = $gid;
							} else {
								useradmin_uploaduser_notify_error($linenum, get_string('groupunknown','error',$addgroup[$i]), $user->id, $user->username );
							}
						}
					}
				}
				
				
				// Enrol/Assign
				for ($i=0; $i<$maxnumberofcoursefield; $i++) {   /// Enrol into courses if necessary
					if ($course[$i]) {
						// Prepare for messages
						$a = new object();
						$a->course = $addcourse[$i];
						$a->role = $addrole[$i];
						
						// If a role has been specified, assign it
                        if ( $role[$i] ) {
							$coursecontext = get_context_instance(CONTEXT_COURSE, $course[$i]->id);
							
//							if (!user_can_assign($coursecontext, $addrole[$i])) {
							if (!user_can_assign($coursecontext, $role[$i]->id)) {
							    useradmin_uploaduser_notify_error($linenum, get_string('cannotassignroleincourse','block_useradmin',$a), $user->id, $user->username );
							}
							// Assign role
//							$ret = role_assign($addrole[$i], $user->id, 0, $coursecontext->id);
							$ret = role_assign($role[$i]->id, $user->id, 0, $coursecontext->id);
							useradmin_uploaduser_notify_success($linenum, '-->'.get_string('assignedroleincourse','block_useradmin',$a), $user->id, $user->username );							
						}
						// otherwise, if type has been defined, assign legacy roles
						else if (isset($addtype[$i])) {
							
							// TODO should get actual legacy role names for notfy messages
							switch ($addtype[$i]) {
								case 2:   // teacher
									$ret = add_teacher($user->id, $course[$i]->id, 1);
									useradmin_uploaduser_notify_success($linenum, '-->'.get_string('enrolledasteacher','block_useradmin',$addrole[$i]), $user->id, $user->username );							
									break;
						
								case 3:   // non-editing teacher
									$ret = add_teacher($user->id, $course[$i]->id, 0);
									useradmin_uploaduser_notify_success($linenum, '-->'.get_string('enrolledasnoneditingteacher','block_useradmin',$addrole[$i]), $user->id, $user->username );							
									break;
						
								default:  // student
									$ret = enrol_student($user->id, $course[$i]->id);
									useradmin_uploaduser_notify_success($linenum, '-->'.get_string('enrolledasstudent','block_useradmin',$addrole[$i]), $user->id, $user->username );							
									break;
							}
						} else {
							// Enroll
							$ret = enrol_student($user->id, $course[$i]->id);
							useradmin_uploaduser_notify_success($linenum, '-->'.get_string('enrolledwithdefaultrole','block_useradmin',$addrole[$i]), $user->id, $user->username );							
						}
	
					}
				}
				// Add to groups
				for ($i=0; $i<$maxnumberofcoursefield; $i++) {   // Add user to groups if necessary
					if ($course[$i] && $groupid[$i]) {
						$coursecontext = get_context_instance(CONTEXT_COURSE, $course[$i]->id);

						$a = new object();
						$a->group = $addgroup[$i];
						$a->course = $addcourse[$i];
						
						if (count(get_user_roles($coursecontext, $user->id))) {
							if (add_user_to_group($groupid[$i], $user->id)) {
								useradmin_uploaduser_notify_success($linenum, '-->'.get_string('addedtogroup','block_useradmin',$a), $user->id, $user->username );
							} else {
								useradmin_uploaduser_notify_success($linenum, '-->'.get_string('addedtogroupnot','block_useradmin',$a), $user->id, $user->username );
							}
						} else {
							useradmin_uploaduser_notify_error($linenum, '-->'.get_string('addedtogroupnotenrolled','block_useradmin',$a), $user->id, $user->username );
						}
	
					}
				}
	
				unset ($user);
			}
		}
		fclose($fp);
		
		// Print a small report
		echo '<hr height="2" />';
		notify("$strusersnew: $usersnew");
		notify(get_string('usersupdated', 'admin') . ": $usersupdated");
		notify(get_string('errors', 'admin') . ": $userserrors");
		if ($allowrenames) {
			notify(get_string('usersrenamed', 'admin') . ": $renames");
			notify(get_string('renameerrors', 'admin') . ": $renameerrors");
		}
		notify( get_string('fakemails', 'block_useradmin') . ": $fakemails" );
		notify( get_string('invalidmails', 'block_useradmin') . ": $invalidmails" );
		notify( get_string('duplicatemails', 'block_useradmin') . ": $duplicatemails" );
		echo '<hr />';
	}
	
	/// Print the form
	print_heading_with_help($struploadusers, 'uploaduser', 'block_useradmin');
	
	$noyesoptions = array( get_string('no'), get_string('yes') );
	
	$maxuploadsize = get_max_upload_file_size();
	echo '<center>';
	
	$csvparams->separator = $csv_delimiter2;
	$csvparams->nomail = $csv_nomail;
	// print_simple_box(get_string('explain', 'block_useradmin', $csvparams), 'center');
	
	echo '<form method="post" enctype="multipart/form-data" action="uploaduser.php">'.
	$strfile.'&nbsp;<input type="hidden" name="MAX_FILE_SIZE" value="'.$maxuploadsize.'">'.
	'<input type="hidden" name="sesskey" value="'.$USER->sesskey.'">'.
	'<input type="file" name="userfile" size="30">';
	
	echo ' <input type="submit" value="'.get_string('uploadfile','block_useradmin' ).'">';
	
	print_heading(get_string('settings'));
	
	
	print_simple_box_start('center');
	
	echo '<center><table>';
	
	// Choose Separator
	echo '<tr><td>' . get_string('fieldseparator', 'block_useradmin');
	helpbutton('fieldseparator', get_string('fieldseparator','block_useradmin'), 'block_useradmin');
	echo '</td><td>';
	choose_from_menu($fieldseparatormenuoptions, 'separatoropt', $separatoropt, '');
	echo '</td></tr>';
	
	// choose file encoding
	echo '<tr><td>' . get_string('filencoding', 'block_useradmin');
	helpbutton('filencoding', get_string('filencoding','block_useradmin'), 'block_useradmin');
	echo '</td><td>';
	choose_from_menu($filencodingmenuoptions, 'filencodingopt', $filencodingopt, '');
	echo '</td></tr>';
	
	
	// No-mail placeholder
	echo '<tr><td>' . get_string('nomailplaceholder', 'block_useradmin');
	helpbutton('fakemail', get_string('fakemailgeneration','block_useradmin'), 'block_useradmin');
	echo '</td><td>';
	echo "<input type=\"text\" name=\"nomail\" value=\"".s($csv_nomail, true)."\" size=\"20\" />";
	echo ' (' . get_string('onlyalphanum','block_useradmin') . ')';
	echo '</td></tr>';
	
	// Fake-mail domain
	echo '<tr><td>' . get_string('fakemaildomain', 'block_useradmin');
	helpbutton('fakemail', get_string('fakemailgeneration','block_useradmin'), 'block_useradmin');
	echo '</td><td>';
	echo "<input type=\"text\" name=\"fakemaildomain\" value=\"".s($csv_fakemaildomain, true)."\" size=\"20\" />";
	echo '</td></tr>';
	
	// Max Number of course, group and type fields
	echo '<tr><td>' . get_string('maxnumberofcoursefield', 'block_useradmin');
	helpbutton('maxnumberofcoursefield', get_string('maxnumberofcoursefield','block_useradmin'), 'block_useradmin');
	echo '</td><td>';
	$maxnumberofcoursefieldopstion = array( 5=>5, 10=>10, 20=>20);
	choose_from_menu($maxnumberofcoursefieldopstion, 'maxnumberofcoursefield', $maxnumberofcoursefield,'');
	echo '</td></tr>';
	
	echo '<tr><td colspan="2"><hr /></td><td>';
	
	// Password handling
	echo '<tr><td>' . get_string('passwordhandling', 'auth') . '</td><td>';
	$passwordopts = array( 0 => get_string('infilefield', 'auth'),
	1 => get_string('createpasswordifneeded', 'auth'),
	);
	choose_from_menu($passwordopts, 'createpassword', $createpassword);
	echo '</td></tr>';
	
	// Update Accounts
	echo '<tr><td>' . get_string('updateaccounts', 'admin') . '</td><td>';
	choose_from_menu($noyesoptions, 'updateaccounts', $updateaccounts);
	echo '</td></tr>';
	
	// Allow rename
	echo '<tr><td>' . get_string('allowrenames', 'admin') . '</td><td>';
	choose_from_menu($noyesoptions, 'allowrenames', $allowrenames);
	echo '</td></tr>';
	
	echo '</table>';
	
	echo '</center>';
	print_simple_box_end();
	
	echo '</form>';
	
	echo '</center>';
	
	print_footer();
	
	
	
	function my_file_get_contents($filename, $use_include_path = 0) {
		/// Returns the file as one big long string
	
		$data = "";
		$file = @fopen($filename, "rb", $use_include_path);
		if ($file) {
			while (!feof($file)) {
				$data .= fread($file, 1024);
			}
			fclose($file);
		}
		return $data;
	}

?>

