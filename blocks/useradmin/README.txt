$Id
///////////////////////////////////////////////////////////////////////////
//                                                                       //
// NOTICE OF COPYRIGHT                                                   //
//                                                                       //
// Moodle - Modular Object-Oriented Dynamic Learning Environment         //
//          http://moodle.org                                            //
//                                                                       //
// Copyright (C) 1999-2004  Martin Dougiamas  http://dougiamas.com       //
//                                                                       //
///////////////////////////////////////////////////////////////////////////
//                                                                       //
// USER ADMINISTRATION block                                             //
// Copyright (C) 2007-2008  Lorenzo Nicora                               //
//                                                                       //
///////////////////////////////////////////////////////////////////////////
//                                                                       //
// This program is free software; you can redistribute it and/or modify  //
// it under the terms of the GNU General Public License as published by  //
// the Free Software Foundation; either version 2 of the License, or     //
// (at your option) any later version.                                   //
//                                                                       //
// This program is distributed in the hope that it will be useful,       //
// but WITHOUT ANY WARRANTY; without even the implied warranty of        //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         //
// GNU General Public License for more details:                          //
//                                                                       //
//          http://www.gnu.org/copyleft/gpl.html                         //
//                                                                       //
///////////////////////////////////////////////////////////////////////////

Useradmin Block (enhanced User Administration)
Version for Moodle 1.9+
===========================================================================
Created by:
	Lorenzo Nicora
	
	thanks to CESVOT - Centro Servizi Volontariato Toscana  - 
	for supporting this work

---------------------------------------------------------------------------
INSTALLATION
	1) Unzip the content in main Moodle directory.
	   Please note that if the zipfile you downloaded unzip in a directory named
	   "useradmin" (or similar) containig a single subdirectory "blocks", you need
	   to unzip in a separate directory and copy the content of the main dir
	   (from "blocks", included) to Moodle directory.
	2) Log in as Administrator and enter Site Admin->Notice area to complete 
	   installation. 
	   This block actually install nothing in the database except 
	   registering itself.
	3) Add the User Administration block to the home page of the site. 
	   I suggest to put it as high as you can, to need no scrolling to 
	   reach it. 
	   Don't worry: it will be visible to Administrators only
	4) You may also add the block in Site Administration area. 
	   It will be useful to get back to user list after, for example, 
	   editing a user profile
---------------------------------------------------------------------------
Capabilities needed
	To see the block and use User Download, a user must have a Role with ALL these 
	capabilities:
		moodle/site:accessallgroups	   
		moodle/site:viewfullnames
		moodle/user:viewdetails
		moodle/user:viewhiddendetails
		moodle/role:viewhiddenassigns

	To Edit users must have:
		moodle/user:update
		moodle/user:editprofile
	
	To Create a new user must have:
		moodle/user:create
	
	To Delete users must have:
		moodle/user:delete
		
	To Assign/Unassign Roles must have:
		moodle/role:assign
	
	To Upload users must have also:
		moodle/site:uploadusers
		moodle/user:update
		moodle/user:create
		moodle/role:assign
	
		
	All capabilities are checked in SYSTEM (site) context.		
	Usually only site Administrators have all of them