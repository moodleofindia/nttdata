<?php  //$Id: upgrade.php,v 1.4.2.2 2007/12/06 07:07:32 toyomoyo Exp $

// This file keeps track of upgrades to 

function xmldb_block_activity_locking_upgrade($oldversion=0) {

    global $CFG, $THEME, $db;

    $result = true;

    if ($result) {

       $table = new XMLDBTable('course_modules');
        $field = new XMLDBField('delay');
        $field->setAttributes(XMLDB_TYPE_CHAR, '10', null, null, null, null, null, null, 'added');
        $result = $result && add_field($table, $field);

        $table = new XMLDBTable('course_modules');
        $field = new XMLDBField('visiblewhenlocked');
        $field->setAttributes(XMLDB_TYPE_INTEGER, '2', XMLDB_UNSIGNED, XMLDB_NOTNULL, null, null, null, '0', 'visible');
        $result = $result && add_field($table, $field);

        $table = new XMLDBTable('course_modules');
        $field = new XMLDBField('checkboxforcomplete');
        $field->setAttributes(XMLDB_TYPE_INTEGER, '2', XMLDB_UNSIGNED, XMLDB_NOTNULL, null, null, null, '0', 'visible');
        $result = $result && add_field($table, $field);

        $table = new XMLDBTable('course_modules');
        $field = new XMLDBField('checkboxesforprereqs');
        $field->setAttributes(XMLDB_TYPE_INTEGER, '2', XMLDB_UNSIGNED, XMLDB_NOTNULL, null, null, null, '0', 'visible');
        $result = $result && add_field($table, $field);

            if ($result) {
                notify('Added fields to course_modules table.', 'notifysuccess');
            }
    }
	
	
    return $result;
}

?>