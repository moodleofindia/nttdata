<?php 

class block_coursetotal extends block_base {

    function init() {
        $this->title = 'Course completion';
        $this->version = 2007101509;
    }

    function applicable_formats() {
        return array('all' => true);
    }

    function specialization() {
        $this->title = isset($this->config->title) ? format_string($this->config->title) : 'Course completion';
    }

    function instance_allow_multiple() {
        return true;
    }

    function get_content() {
	  global $CFG, $COURSE, $USER;
        if ($this->content !== NULL) {
            return $this->content;
        }
		  $course = get_record('course', 'id', $COURSE->id);
		$filteropt = new stdClass;
        $filteropt->noclean = true;
		$this->content = new stdClass;
		$graderecords = get_records_sql("SELECT gg.finalgrade,gi.grademax 
		FROM mdl_course AS c
		JOIN mdl_grade_items gi on gi.courseid=c.id
		JOIN mdl_grade_grades AS gg ON gi.id = gg.itemid
		WHERE c.id =$COURSE->id and gg.userid =$USER->id
		AND gi.itemtype = 'course'");
		foreach ($graderecords as $graderecord) {
			if($graderecord->finalgrade == $graderecord->grademax)
			{  
				$this->content->text='<p><font size="2" color="#46BA11"><b>Status :</b> Complete </font></P>
				<a href="'.$CFG->wwwroot.'/grade/report/user/index.php?id='.$COURSE->id.'"><p><font size="1">You have sucessfully completed '.$course->fullname.'</font></a>';

            }
			else if($graderecord->finalgrade >0)
			{
				$this->content->text='<p><font size="2" color="#F88017"><b>Status :</b> In Progress </font></P>
				<a href="'.$CFG->wwwroot.'/grade/report/user/index.php?id='.$COURSE->id.'"><p><font size="1">You are yet to complete '.$course->fullname.'</font></a>';	
			}
			else
			{
				$this->content->text='<p><font size="2" color="#038FFF"><b>Status :</b> Started </font></P>
				<a href="'.$CFG->wwwroot.'/grade/report/user/index.php?id='.$COURSE->id.'"><p><font size="1">You are yet to complete '.$course->fullname.'</font></a> ';	
			}
        }
	
        unset($filteropt); 
        return $this->content;
    }


    function get_backup_encoded_config() {
        
        if (empty($this->config)) {
            return parent::get_backup_encoded_config();
        }
        $data = clone($this->config);
        $data->text = backup_encode_absolute_links($data->text);
        return base64_encode(serialize($data));
    }


    function decode_content_links_caller($restore) {
        global $CFG;

        if ($restored_blocks = get_records_select("backup_ids","table_name = 'block_instance' AND backup_code = $restore->backup_unique_code AND new_id > 0", "", "new_id")) {
            $restored_blocks = implode(',', array_keys($restored_blocks));
            $sql = "SELECT bi.*
                      FROM {$CFG->prefix}block_instance bi
                           JOIN {$CFG->prefix}block b ON b.id = bi.blockid
                     WHERE b.name = 'coursetotal' AND bi.id IN ($restored_blocks)"; 

            if ($instances = get_records_sql($sql)) {
                foreach ($instances as $instance) {
                    $blockobject = block_instance('coursetotal', $instance);
                    $blockobject->config->text = restore_decode_absolute_links($blockobject->config->text);
                    $blockobject->config->text = restore_decode_content_links_worker($blockobject->config->text, $restore);
                    $blockobject->instance_config_commit($blockobject->pinned);
                }
            }
        }

        return true;
    }
}
?>
