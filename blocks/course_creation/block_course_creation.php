<?PHP //$Id: block_course_creation.php v 1 $

class block_course_creation extends block_base {
    function init() {
        $this->title = get_string('title', 'block_course_creation');
        $this->content_type = BLOCK_TYPE_TEXT;
        $this->version = 2006072500;
    }
    
	function load_content_for_site() {
    	global $CFG, $USER;
	}

	function applicable_formats() {
    	return array('site' => true, 'my' =>true);
	}

	function get_content() {
		// added - bkg
		global $CFG;
    	if ($this->content !== NULL) {
    	    return $this->content;
    	}
    

	    if (isadmin()) {
    	    if ($pending = get_records("course_request")) {
    			$this->content->text = '<a href="'.$CFG->wwwroot.'/course/pending.php?">'.get_string('approve', 'block_course_creation').'</a>';
        		return $this->content;
			}
        	else {
    			$this->content->text = get_string('nocourses', 'block_course_creation');
        		return $this->content;
			}
		}
    if (isteacherinanycourse()) {
    	$this->content->text = '<a href="'.$CFG->wwwroot.'/course/request.php">'.get_string('request', 'block_course_creation').'</a>';
        return $this->content;
}
    	$this->content->footer = '';

	}
}
?>
