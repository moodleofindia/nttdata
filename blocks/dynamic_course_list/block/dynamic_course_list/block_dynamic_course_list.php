<?php

class block_dynamic_course_list extends block_list {
    
    function init() {
        $this->title   = get_string('title', 'block_dynamic_course_list');
        $this->version = 2009032401;
    }

    function specialization() {
        global $CFG, $COURSE;
        $this->course = $COURSE;        
        
      
      $this->config->course_id = $this->course->id;
      
      if(!empty($this->config->title)){
        $this->title = $this->config->title;
      }else{
        $this->config->title = get_string('sometext', 'block_dynamic_course_list');
      }  
    }          
      
    function get_content() {
      global $CFG, $USER;        

      if ($this->content !== NULL) {
        return $this->content;
      }     
     
      $this->content         = new stdClass;
      $this->content->items  = array();
      $this->content->icons  = array();
      $this->content->footer = '';

      $icon  = "<img src=\"$CFG->pixpath/i/course.gif\"".
                 " class=\"icon\" alt=\"".get_string("coursecategory")."\" />";
                      

      if (!empty($USER->id) and 
            !isguest()) { 

               //If we add it to the child_course
               $is_parent = is_parent($this->config->course_id);
               if (empty($is_parent)){
                 $mainparent = $this->config->course_id;                                                                 
               }
               else{
                 $mainparent = $is_parent->child_course;
               }                                   

               $sql = 'select *
                       from '. $CFG->prefix .'course
                       where '. $CFG->prefix .'course.id in (select '. $CFG->prefix .'course_meta.parent_course
                                                             from '. $CFG->prefix .'course_meta
                                                             where '. $CFG->prefix .'course_meta.child_course = '.$mainparent.')
                       order by '. $CFG->prefix .'course.sortorder';

               //After selecting the parent course we'll make a link to it                 
               $pcourse = get_parent($mainparent);
               $linkcss = $pcourse->visible ? "" : " class=\"dimmed\" ";    
               if ($pcourse->id == $this->config->course_id)
               {               
                 $this->content->items[]="<b><a $linkcss title=\"" . format_string($pcourse->shortname) . "\" ".
                    "href=\"$CFG->wwwroot/course/view.php?id=$pcourse->id\">" . format_string($pcourse->fullname) . "</b></a>";
               }
               else
               {
                 $this->content->items[]="<a $linkcss title=\"" . format_string($pcourse->shortname) . "\" ".
                    "href=\"$CFG->wwwroot/course/view.php?id=$pcourse->id\">" . format_string($pcourse->fullname) . "</a>";                   
               }
               $this->content->icons[]=$icon;                       
                
              //Add an empty line after the parent name of a course
              $this->content->items[]="";
              $this->content->icons[]="";     
                                                  
               if ($courses = get_records_sql($sql)) {
                foreach ($courses as $course) {
                    if ($course->id == SITEID){
                        continue;
                    }
                    $linkcss = $course->visible ? "" : " class=\"dimmed\" ";
                    if (($course->id == $this->config->course_id) || stristr($course->shortname, get_string('theme', 'block_dynamic_course_list')))
                    {
                      $this->content->items[]="<b><a $linkcss title=\"" . format_string($course->shortname) . "\" ".
                               "href=\"$CFG->wwwroot/course/view.php?id=$course->id\">" . format_string($course->fullname) . "</b></a>";                                                           
                    }
                    else
                    {
                      $this->content->items[]="<a $linkcss title=\"" . format_string($course->shortname) . "\" ".
                               "href=\"$CFG->wwwroot/course/view.php?id=$course->id\">" . format_string($course->fullname) . "</a>";                                                      
                        
                    }      
                    //Theme is reserved for our block. When we use theme for the fullname of our course this will result in a new presentation.
                    if (stristr($course->shortname, get_string('theme', 'block_dynamic_course_list'))){
                      //I don't use icons, but you can if you want.   
                      $this->content->icons[]=$icon;                        
                    }
                    else
                    {         
                      $this->content->icons[]="&nbsp;&nbsp;";
                    }
            }
          }
        }   
      return $this->content;
    }
      
      
    function instance_allow_config() {
      return true;
    }      
   
    
    //Main instance_config_save method 
    function instance_config_save($data) {
      $data = stripslashes_recursive($data);
      $this->config = $data;
      return set_field('block_instance', 
                       'configdata',
                        base64_encode(serialize($data)),
                       'id', 
                       $this->instance->id);
    }    
    
      
}//main class 

    //Not in the main class methods
    function get_parent($course_id){
      global $CFG;
        $sql = 'SELECT * from '. $CFG->prefix .'course Where id = '.$course_id;        
        return get_record_sql($sql);
               
    } 

    function is_parent($parent_id){
      global $CFG;
        $sql = 'SELECT * from '. $CFG->prefix .'course_meta Where parent_course = '.$parent_id;        
        return get_record_sql($sql); 
    }

?>