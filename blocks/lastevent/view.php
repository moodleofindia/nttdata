<?php  // $Id: view.php,v 1.6 2007/09/03 12:23:36 jamiesensei Exp $
/**
 * This page prints a particular instance of lastevent
 *
 * @author
 * @version $Id: view.php,v 1.6 2007/09/03 12:23:36 jamiesensei Exp $
 * @package lastevent
 **/

/// (Replace lastevent with the name of your module)

    require_once("../../config.php");
    require_once("lib.php");

    $id = optional_param('id', 0, PARAM_INT); // Course Module ID, or
    $a  = optional_param('a', 0, PARAM_INT);  // lastevent ID

    if ($id) {
        if (! $cm = get_record("course_modules", "id", $id)) {
            error("Course Module ID was incorrect");
        }

        if (! $course = get_record("course", "id", $cm->course)) {
            error("Course is misconfigured");
        }

        if (! $lastevent = get_record("lastevent", "id", $cm->instance)) {
            error("Course module is incorrect");
        }

    } else {
        if (! $lastevent = get_record("lastevent", "id", $a)) {
            error("Course module is incorrect");
        }
        if (! $course = get_record("course", "id", $lastevent->course)) {
            error("Course is misconfigured");
        }
        if (! $cm = get_coursemodule_from_instance("lastevent", $lastevent->id, $course->id)) {
            error("Course Module ID was incorrect");
        }
    }

    require_login($course->id);

    add_to_log($course->id, "lastevent", "view", "view.php?id=$cm->id", "$lastevent->id");

/// Print the page header
    $strlastevents = get_string("modulenameplural", "lastevent");
    $strlastevent  = get_string("modulename", "lastevent");

    $navlinks = array();
    $navlinks[] = array('name' => $strlastevents, 'link' => "index.php?id=$course->id", 'type' => 'activity');
    $navlinks[] = array('name' => format_string($lastevent->name), 'link' => '', 'type' => 'activityinstance');

    $navigation = build_navigation($navlinks);

    print_header_simple(format_string($lastevent->name), "", $navigation, "", "", true,
                  update_module_button($cm->id, $course->id, $strlastevent), navmenu($course, $cm));

/// Print the main part of the page

    echo "YOUR CODE GOES HERE";


/// Finish the page
    print_footer($course);
?>
