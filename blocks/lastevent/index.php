<?php
  /**
   * This page lists all the instances of lastevent in a particular course
   *
   * @author Wallner Ádám
   * @version 1.0
   * @package lastevent
   **/
  require_once("../../config.php");

  if (!$USER->id || $USER->username=='guest') {
    echo "<script type=\"text/javascript\">
        location.href=\"../../login/index.php\";
      </script>
      <noscript>
        You need JavaScript support for this function to work.
      </noscript>
    ";
    die();
  }

  if (isset($_GET['module'])) {
    $module="'".$_GET['module']."'";
  } else $module="'quiz', 'resource'";
  //echo $module;

  $rows =  get_records_sql("
    SELECT url, module
    FROM {$CFG->prefix}log
    WHERE userid='{$USER->id}'
      AND action IN ('view', 'attempt', 'review')
      AND module IN ($module)
    ORDER BY time DESC
    LIMIT 1
  ");
  foreach ($rows as $r) {
    header("Location: {$CFG->wwwroot}/mod/{$r->module}/{$r->url}");
    die();
  }

  $navlinks = array();
  $navlinks[] = array('name' => 'lastevent', 'link' => '', 'type' => 'activity');
  $navigation = build_navigation($navlinks);
  print_header_simple("$strlastevents", "", $navigation, "", "", true, "", navmenu($course));

  print_footer($course);
?>