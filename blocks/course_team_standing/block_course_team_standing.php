<?php


   require_once($CFG->dirroot.'/mod/assignment/lib.php');

   class block_course_team_standing extends block_base {

      function init () {
         global $CFG;
	 $this->title = get_string('blockname', 'block_course_team_standing');
	 $this->version = 2007031700;
	 $this->config->maxchars = 12;

	 $this->config_save($this->config);
      } // function init()

      function has_config() {
         return true;
      } // function has_config()

      function get_content() {

         global $db, $COURSE,$USER, $CFG, $config;

         if ($this->content !== NULL) {
	    return $this->content;
         }
	  
	 $this->content = new stdClass;

	 // get courses of the current user
	 $courses = get_my_courses($USER->id);
         if ((empty($courses) || !is_array($courses) || count($courses) == 0)) {
            $this->content->text .= get_string('no courses', 'block_course_team_standing');
         } else {

            // get assignments of the current user in his courses
			
			$Teammembers = get_records_sql("SELECT u.username,concat(u.firstname,' ',u.lastname) as
			name,round(max(gg.finalgrade)/gi.grademax*100,0) as grade
			FROM mdl_course AS c JOIN mdl_context AS ctx
			ON c.id = ctx.instanceid and c.visible=1 JOIN mdl_role_assignments AS ra
			ON ra.contextid = ctx.id  JOIN mdl_user AS u
			ON u.id = ra.userid JOIN mdl_grade_grades AS gg
			ON gg.userid = u.id JOIN mdl_grade_items AS gi
			ON gi.id = gg.itemid WHERE  gi.courseid = $COURSE->id
      AND gi.itemtype = 'course' and u.id != $USER->id
      and u.manager_portalid=(select manager_portalid
      from mdl_user where id=$USER->id) group by u.username");
			
			
			$members = array();
			if ($Teammembers) {
			foreach($Teammembers as $record) {
                $members[] = $record;

                continue;
				}
			}
			else
			{
			$this->content->text .= '<table border="0" cellspacing="2" cellpadding="0" >';
             $this->content->text .= '<tr style="font-size:12px;padding:1px">';
			 $this->content->text .= 'No members are available';
			 $this->content->text .= '</tr></table>';
			
			}



$this->content->text .= '<table border="0" cellspacing="2" cellpadding="0" >';
		foreach ($Teammembers as $member) {
             $this->content->text .= '<tr style="font-size:12px;padding:1px">
                <td align="left" width="130px" >'.$member->name.'</td>';
				if($member->grade<10)
				{
				$this->content->text .= '<td align="centre" ><img src="'.$wwwroot.'\blocks\course_team_standing\1.png" height="20" width="20"> '.$member->grade.'% </td>';
				}
				else if(($member->grade<60)&&($member->grade>10))
				{
				$this->content->text .= '<td align="centre" ><img src="'.$wwwroot.'\blocks\course_team_standing\2.png" height="20" width="20"> '.$member->grade.'% </td>';
				}
				else if(($member->grade<100)&&($member->grade>60))
				{
				$this->content->text .= '<td align="centre" ><img src="'.$wwwroot.'\blocks\course_team_standing\3.png" height="20" width="20"> '.$member->grade.'% </td>';
				}
				else if (($member->grade>100)||($member->grade==100))
				{
				$this->content->text .= '<td align="centre" ><img src="'.$wwwroot.'\blocks\course_team_standing\4.png" height="20" width="20"> '.$member->grade.'% </td>';
				}
				
				$this->content->text .='</tr>';       

		} 
$this->content->text .= '</table>';
$this->content->footer = '';
            


	 } // if courses

         return $this->content;

      } // function get_content()

      function specialisation() {
         $this->maxchars = $this->config->maxchars;
      }

      function instance_allow_config() {
         return true;
      } // function instance_allow_config()

      function config_save($data) {
         foreach ($data as $name => $value) {
            set_config($name, $value);
         }
         return true;
      } // function config_save()


   } // class block_course_team_standing

?>
