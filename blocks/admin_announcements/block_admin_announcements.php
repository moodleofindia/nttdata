<?php //Created:Roy Philip 3-5-2012,Admin Announcement block.

class block_admin_announcements extends block_base {

    function init () {
        $this->title = get_string('blocktitle', 'block_admin_announcements');
        $this->version = 2004052600;
    }

    function specialization() {
        global $course;

        $this->course = $course;

        if (!empty($this->config->displaytitle)) {
            $this->title = $this->config->displaytitle;
        } else {
            $this->title = get_string('displaytitle', 'block_admin_announcements');
        }
    }

    function instance_allow_config() {
        return true;
    }

    function get_content() {
              global $CFG, $USER, $COURSE;

        if ($this->content !== NULL) {
            return $this->content;
        }

        $this->content = new stdClass;
        $this->content->text = '';
        $this->content->footer = '';

        if (empty($this->instance)) {
            return $this->content;
        }
		
       $text = '';
if ($postusers = get_records_sql("SELECT u.firstname,u.lastname,u.id as id FROM mdl_user u join mdl_role_assignments a on a.userid=u.id and a.roleid in(1,10) and a.contextid=1"))
{
$this->content->text .= '<table width="100%" cellpadding="0" cellspacing="0" border="0">'."\n";
   foreach ($postusers as $postuser) {
   
            //$discussions = get_records_sql("SELECT subject,id FROM mdl_post where userid=$postuser->id");
			$discussions = get_records_sql("SELECT subject,id,lastmodified as modified FROM mdl_post where userid=$postuser->id order by id desc limit 5");
    
            $strftimerecent = get_string('strftimerecent');
            $strmore = 'More';
        
            
                foreach ($discussions as $discussion) {
                    $this->content->text .= '<tr>';

                    $this->content->text .= '<td valign="top">';

                        $this->content->text .= '<img src="'.$CFG->wwwroot.'/blocks/admin_announcements/images/closed.png" /> ';

                    $this->content->text .= '</td>';

                    $this->content->text .= '<td class="smallinfo">';
					$this->content->text .='<div class="date">'.fullname($postuser).' on '.userdate($discussion->modified, $strftimerecent).'</div>';                    
                    $this->content->text .= '<div class="info">'.$discussion->subject;
                    $this->content->text .= " <a href=\"$CFG->wwwroot/blog/index.php?postid=$discussion->id\">";
                    $this->content->text .= $strmore."...</a></div><br />";
                    $this->content->text .= '</td>';
                    $this->content->text .= '</tr>';
                }
			
		}
		$this->content->text .= '</table>'."\n";
		return $this->content;
	}
	else
	{
				$text .= 'No news from Administrators';
                $this->content->text = $text;
				$this->content->text .= '</table>'."\n";
                return $this->content;
	}
}


}
?>