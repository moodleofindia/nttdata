<?php
/**
 * Course ratings block
 *
 * @copyright &copy; 2008 The Open University
 * @author j.m.gray@open.ac.uk
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 */
$string['courserating'] = 'Course ratings';

$string['completed'] = 'Thank you.  You have already given this unit a rating.';
$string['giverating'] = 'Give a rating';
$string['intro'] = 'Please indicate how highly you rate this unit.';
$string['submit'] = 'Submit my rating';
$string['noguestuseage'] = 'To rate this course you will need to log in.';
$string['rate_course:rate'] ='Give a rating to a course';
$string['rating_alt0'] ='Course rating: No ratings given.';
$string['rating_altnum'] ='Course rating: $a stars.';
$string['rating_users'] ='Rated by $a user(s)';
?>
