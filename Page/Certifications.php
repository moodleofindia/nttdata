<html>
<head>

<meta http-equiv="content-type" content="text/html; charset=iso-8859-1"/>
	<title>jQuery treeView</title>
	
	<link rel="stylesheet" href="jquery.treeview.css" />

	<link rel="stylesheet" href="screen.css" />
	
	<script type="text/javascript" src="jquery.min.js"></script>
	<script src="./lib/jquery.cookie.js" type="text/javascript"></script>
	<script src="jquery.treeview.js" type="text/javascript"></script>
	
	<script type="text/javascript">
	$(document).ready(function(){
		$("#browser").treeview({
			toggle: function() {
				console.log("%s was toggled.", $(this).find(">span").text());
			}
		});
		
		$("#add").click(function() {
			var branches = $("<li><span class='folder'>New Sublist</span><ul>" + 
				"<li><span class='file'>Item1</span></li>" + 
				"<li><span class='file'>Item2</span></li></ul></li>").appendTo("#browser");
			$("#browser").treeview({
				add: branches
			});
		});
	});
	</script>
</head>
<body>
<?php



    require_once('../config.php');
    require_once("../course/lib.php");
    require_once("$CFG->libdir/blocklib.php");


    function csverror($message, $link='') {
        global $CFG, $SESSION;
    
        print_header(get_string('error'));
        echo '<br />';
    
        $message = clean_text($message);
    
        print_simple_box('<span style="font-family:monospace;color:#000000;">'.$message.'</span>', 'center', '', '#FFBBBB', 5, 'errorbox');
    
        print_footer();
        die;
    }




    require_login();
    
    if (!isadmin()) {
        csverror('You must be an administrator to edit courses in this way.');
    }

/*    if (!confirm_sesskey()) {
        csverror(get_string('confirmsesskeybad', 'error'));
    } */

    if (! $site = get_site()) {
        csverror('Could not find site-level course');
    }

    if (!$adminuser = get_admin()) {
        csverror('Could not find site admin');
    }



/// Print the header

    print_header("$site->shortname: $struploadcourses", $site->fullname, 
                 "<a href=\"index.php\">$stradministration</a> -> $struploadcourses");

 ?>

<div id="main">
	<ul id="browser" class="filetree treeview-famfamfam">
		<li><span class="folder">ADM</span>
			<ul>
				<li><span class="folder">Java</span>
					<ul>
						<li><span class="file">Beginner(Grade 1)</span>
						<ul>
						<li><span class="file"><a href="http://klci.keane.com/course/view.php?id=170">Java Developer Certification - Level 2</a></span></li>
						<li><span class="file"><a href="http://klci.keane.com/course/view.php?id=199">Java Developer Certification - Level 3</a></span></li>
						</ul>
						</li>
						<li><span class="file">Advanced(Grade 2)</span>
						<ul>
						<li><span class="file"><a href="http://klci.keane.com/course/view.php?id=197">Java Advance Developer Certification - Level 1</a></span></li>
						<li><span class="file"><a href="http://klci.keane.com/course/view.php?id=334">Java Advance Developer Certification - Level 2</a></span></li>
						<li><span class="file"><a href="http://klci.keane.com/course/view.php?id=335">Java Advance Developer Certification - Level 3</a></span></li>
						</ul>
						</li>
					</ul>
				</li>
				<li><span class="folder">Dot Net</span>
					<ul>
						<li><span class="file">Beginner(Grade 1)</span>
						<ul>
						<li><span class="file"><a href="http://klci.keane.com/course/view.php?id=171">.net Developer Certification - Level 2</a></span></li>
						<li><span class="file"><a href="http://klci.keane.com/course/view.php?id=336">.net Developer Certification - Level 3</a></span></li>
						</ul>
						</li>
						<li><span class="file">Advanced(Grade 2)</span>
						<ul>
						<li><span class="file"><a href="http://klci.keane.com/course/view.php?id=201">.net Advance Developer Certification - Level 1</a></span></li>
						<li><span class="file"><a href="http://klci.keane.com/course/view.php?id=337">.net Advance Developer Certification - Level 2</a></span></li>
						<li><span class="file"><a href="http://klci.keane.com/course/view.php?id=338">.net Advance Developer Certification - Level 3</a></span></li>
						</ul>
						</li>
					</ul>
				</li>
				<li><span class="folder">Junior Designer</span>
					<ul>
				<li><span class="file"><a href="http://klci.keane.com/course/view.php?id=200">Junior Designer - Certification Level 1</a></span></li>
				</ul>
				</li>
			</ul>
		</li>
		<li><span class="folder">Oracle</span>
			<ul>
				<li><span class="folder"><a href="http://klci.keane.com/course/view.php?id=215">Oracle Apps Technical - Advanced Certification</a></span></li>
				<li><span class="folder"><a href="http://klci.keane.com/course/view.php?id=183">Oracle Apps Technical - Basic Certification</a></span></li>
				</ul>
		</li>
		<li><span class="folder">Project Management</span>
			<ul>
				<li><span class="folder"><a href="http://klci.keane.com/course/view.php?id=179">Project Management Certification - Level 1</a></span></li>
				</ul>
		</li>
		<li><span class="folder">Testing</span>
			<ul>
				<li><span class="folder">Keane Certified Test Manager</span>
				<ul>
						<li><span class="file"><a href="http://klci.keane.com/course/view.php?id=357">Keane Certified Test Manager - Beginner</a></span></li>
						<li><span class="file"><a href="http://klci.keane.com/course/view.php?id=358">Keane Certified Test Manager - Practitioner</a></span></li>
						</ul>
				</li>
				<li><span class="folder">Keane Certified Test Professional</span>
				<ul>
						<li><span class="file"><a href="http://klci.keane.com/course/view.php?id=206">Keane Certified Test Professional - Advanced</a></span></li>
						<li><span class="file"><a href="http://klci.keane.com/course/view.php?id=204">Keane Certified Test Professional - Beginner</a></span></li>
						</ul>
				</li>
				</ul>
		</li>
	</ul>	
</div>
</body>
</html>