<?php 

if( isset($_POST['login']) && isset($_POST['password']) ) 
{ 
    //LDAP stuff here. 
    $username = trim($_POST['login']); 
    $password = trim($_POST['password']); 


    $ds = ldap_connect("KRESDC801.in.kres.keane.com"); 
ldap_set_option($ds, LDAP_OPT_PROTOCOL_VERSION, 3);
ldap_set_option($ds, LDAP_OPT_REFERRALS,0);

     
    //Can't connect to LDAP. 
    if( !ds ) 
    { 
        echo "Error in contacting the LDAP server -- contact "; 
        echo "technical services!  (Debug 1)"; 
    } 
     
    //Connection made -- bind anonymously and get dn for username. 
    $bind = @ldap_bind($ds); 
     
    //Check to make sure we're bound. 
    if( !bind ) 
    { 
        echo "Anonymous bind to LDAP FAILED.  Contact Tech Services! (Debug 2)"; 

    } 
     
    $search = ldap_list($ds, "DC=IN,DC=KRES,DC=kEANE,DC=com"); 
     
    //Make sure only ONE result was returned -- if not, they might've thrown a * into the username.  Bad user! 
    if( ldap_count_entries($ds,$search) <= 0 ) 
    { 
        echo "Error processing username -- please try to login again. (Debug 3)"; 
 
    } 
     
    $info = ldap_get_entries($ds, $search); 
     
    //Now, try to rebind with their full dn and password. 
    $bind = @ldap_bind($ds, $info[0][dn], $password); 
    if( !$bind || !isset($bind)) 
    { 
        echo "Login failed -- please try again. (Debug 4)"; 

        exit; 
    } 
     
    //Now verify the previous search using their credentials. 
    $search = ldap_search($ds, "dc=corp,dc=sample,dc=com", "uid=$username"); 
         
    $info = ldap_get_entries($ds, $search); 
    if( $username == $info[0][uid][0] ) 
    { 
        echo "Authenticated."; 

    } 
    else 
    { 
        echo "Login failed -- please try again."; 

    } 
    ldap_close($ds); 

} 
?> 

<form action=ldapuser.php method=post name=Auth> 


Please log in using your user name and your 
portal password:<p> 

<table cellspacing=3 cellpadding=3 class=ContentBodyTable> 
   <tr> 
      <td>Username: </td> 
      <td><input type=text name=login size=16 maxlength=15 class=textInput></td> 
   </tr> 
   <tr> 
      <td>Password: </td> 
      <td><input type=password name=password size=16 maxlength=15 class=textInput></td> 
   </tr> 
   <tr> 
      <td colspan=2><input type=submit value=Authenticate class=SubmitInput style='width:100'></td> 
   </tr> 
</table> 
</form> 