<?php  /// Moodle Configuration File 

unset($CFG);

$CFG->dbtype    = 'mysql';
$CFG->dbhost    = 'lms101.nttdatainc.com';
//$CFG->dbhost    = 'localhost';
$CFG->dbname    = 'lms';
$CFG->dbuser    = 'AdminUser';
$CFG->dbpass    = 'Keane@123';
$CFG->dbpersist =  false;
$CFG->prefix    = 'mdl_';

$CFG->wwwroot   = 'http://lms101.nttdatainc.com';
//$CFG->wwwroot   = 'https://lmsqa.portal.nttdatainc.com';
$CFG->dirroot   = 'C:\\inetpub\\wwwroot';
$CFG->dataroot  = 'C:\inetpub\LMSdata';
$CFG->admin     = 'admin';


$CFG->allow_mass_enroll_feature=1;

$CFG->directorypermissions = 00777;  // try 02777 on a server in Safe Mode

require_once("$CFG->dirroot/lib/setup.php");
$CFG->debug = 2047;

//Below 4 lines are trace codes to debug the php errors.
//@error_reporting(1023);
//@ini_set('display_errors', '1');
//$CFG->debug = 38911;
//$CFG->debugdisplay = true;

// MAKE SURE WHEN YOU EDIT THIS FILE THAT THERE ARE NO SPACES, BLANK LINES,
// RETURNS, OR ANYTHING ELSE AFTER THE TWO CHARACTERS ON THE NEXT LINE.
?>