<?PHP // $Id: manager_gradereport.php,v 1.2.2.1 2009/04/22 08:20:25 nicolasconnault Exp $

$string['modulename'] = "Team's Grader report";
$string['grader:manage'] = "Manage the team's grader report";
$string['grader:view'] = "View the Team's grader report";
$string['preferences'] = "Team's Grader report preferences";

?>