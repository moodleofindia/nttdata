<?PHP // $Id: enrol_KLCI.php,v 1.3.2.2 2008/12/06 21:21:55 skodak Exp $ 
      // enrol_KLCI.php - created with Moodle 1.7 beta + (2006101003)

$string['description'] = 'This enrollment allows access only for legacy Optimal Group employees.';
$string['enrolmentdenied'] = 'This course is available only for Legacy Optimal Group employees. For more information please send an e-mail to Certification.Helpdesk@nttdata.com.';
$string['enrolmentkeyerror'] = 'That enrolment key was incorrect, please try again.';
$string['enrolname'] = 'Optimal Course Enrolment';
$string['enrol_Intelli_requirekey'] = 'Require course enrolment keys in new courses and prevent removing of existing keys.';
$string['enrol_Intelli__showhint'] = 'Enable this setting to reveal the first character of the enrolment key as a hint if one enters an incorrect key.';
$string['enrol_Intelli__usepasswordpolicy'] = 'Use current user password policy for course enrolment keys.';
$string['keyholderrole' ] = 'The role of the user that holds the enrolment key for a course. Displayed to students attempting to enroll on the course.';

?>
