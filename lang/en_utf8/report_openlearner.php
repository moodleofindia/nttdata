<?PHP // $Id: quiz_textual.php,v 1.9.6.3 2008/09/10 15:45:30 tasoulis7 Exp $ 
      // report_openlearner.php - created with Moodle 1.9.2 + (20080723)


$string['adaptivetrue'] = 'Adaptive mode enabled:';
$string['allattempts'] = 'Showing all attempts, only for users with student role for this quiz';
$string['analysis'] = 'Analysis by category';
$string['attempt'] = 'Attempt #';
$string['attemptcomparison'] = 'Attempt Comparison ';
$string['attemptno'] = 'Attempt number:';
$string['average'] = 'All attempts average score:';
$string['averageattempt'] = 'Average attempt downloadpdf of students';
$string['averagelevel'] = 'Average difficulty level:';
$string['averageperformance'] = 'Average performance of students';
$string['catequal'] = 'Your performance in this category is equal to the average performance';
$string['catgreater'] = 'Your performance in this category is greater than the average performance';
$string['catless'] = 'Your performance in this category is less than the average performance';
$string['comparison'] = 'Attempt Comparison';
$string['comparisondownload'] = 'Attempt Comparison download';
$string['correctanswers'] = 'Correct answers';
$string['downloadpdf'] = 'Download in PDF format';
$string['downloadexcel'] = 'Download in Excel format';
$string['allattempts'] = 'Attempts by category';
$string['equal'] = 'Your overall performance is equal to the average performance';
$string['false'] = 'False answers';
$string['fullname'] = 'Full name:';
$string['graphical'] = 'Category Wise Scores';
$string['graphicaldownload'] = 'Category wise scores download';
$string['greater'] = 'Your overall performance is greater than the average performance';
$string['knowntopics'] = 'Known topics';
$string['less'] = 'Your overall performance is less than the average performance';
$string['maximum'] = 'All attempts maximum score:';
$string['minimum'] = 'All attempts minimum score:';
$string['misconceptions'] = 'Misconceptions';
$string['multiple'] = 'with multiple responses:';
$string['noanswer'] = 'Not answered';
$string['notanswers'] = 'not answered:';
$string['partial'] = 'Partially correct answers';
$string['penalty'] = 'applied penalty:';
$string['problematic'] = 'Problematic topics';
$string['progress'] = 'Progress Comparison';
$string['progressdownload'] = 'Progress Comparison download';
$string['qtypes'] = 'Question types';
$string['questionsposed'] = 'Total questions posed:';
$string['questionstotal'] = 'Total questions:';
$string['questionstype'] = 'Types of questions posed:';
$string['quizname'] = 'Quiz name:';
$string['quizreport'] = 'Report Analysis';
$string['quizreportanalysis'] = 'Quiz Report Analysis';
$string['score'] = 'Score';
$string['section'] = 'Section:';
$string['stats'] = 'Total Stats';
$string['studentsnumber'] = 'Number of students that attempted the quiz:';
$string['textual'] = 'Attempt Summary';
$string['textualdownload'] = 'Attempt Summary download';
$string['total'] = 'Total score:';
$string['types'] = 'Types of questions:';
$string['userattempt'] = 'Your attempt progress';
$string['userperformance'] = 'Your performance';

?>
