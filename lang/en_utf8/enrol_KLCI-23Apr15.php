<?PHP // $Id: enrol_KLCI.php,v 1.3.2.2 2008/12/06 21:21:55 skodak Exp $ 
      // enrol_KLCI.php - created with Moodle 1.7 beta + (2006101003)


$string['description'] = 'This enrollment allows non India users to be enrolled into the KLCI certification programs.';
$string['enrolmentdenied'] = 'You are not enrolled into the program/course. To access the content you need to be enrolled into the course/program. Please send e-mail for certification.helpdesk@keane.com for more details.';
$string['enrolmentkeyerror'] = 'That enrolment key was incorrect, please try again.';
$string['enrolname'] = 'KLCI Enrolment';
$string['enrol_KLCI_requirekey'] = 'Require course enrolment keys in new courses and prevent removing of existing keys.';
$string['enrol_KLCI_showhint'] = 'Enable this setting to reveal the first character of the enrolment key as a hint if one enters an incorrect key.';
$string['enrol_KLCI_usepasswordpolicy'] = 'Use current user password policy for course enrolment keys.';
$string['keyholderrole' ] = 'The role of the user that holds the enrolment key for a course. Displayed to students attempting to enrol on the course.';
?>
