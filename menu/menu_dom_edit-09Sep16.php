<?php 
    require_once('../config.php');
    require_once($CFG->dirroot .'../course/lib.php');
    require_once($CFG->dirroot .'../lib/blocklib.php');

    if (empty($SITE)) {
        redirect($CFG->wwwroot .'/'. $CFG->admin .'/index.php');
    }
    // Bounds for block widths
    // more flexible for theme designers taken from theme config.php
    $lmin = (empty($THEME->block_l_min_width)) ? 100 : $THEME->block_l_min_width;
    $lmax = (empty($THEME->block_l_max_width)) ? 210 : $THEME->block_l_max_width;
    $rmin = (empty($THEME->block_r_min_width)) ? 100 : $THEME->block_r_min_width;
    $rmax = (empty($THEME->block_r_max_width)) ? 210 : $THEME->block_r_max_width;

    define('BLOCK_L_MIN_WIDTH', $lmin);
    define('BLOCK_L_MAX_WIDTH', $lmax);
    define('BLOCK_R_MIN_WIDTH', $rmin);
    define('BLOCK_R_MAX_WIDTH', $rmax);

    // check if major upgrade needed - also present in login/index.php
    if ((int)$CFG->version < 2006101100) { //1.7 or older
        @require_logout();
        redirect("$CFG->wwwroot/$CFG->admin/");
    }
    // Trigger 1.9 accesslib upgrade?
    if ((int)$CFG->version < 2007092000 
        && isset($USER->id) 
        && is_siteadmin($USER->id)) { // this test is expensive, but is only triggered during the upgrade
        redirect("$CFG->wwwroot/$CFG->admin/");
    }

    if ($CFG->forcelogin) {
        require_login();
    } else {
        user_accesstime_log();
    }

    if ($CFG->rolesactive) { // if already using roles system
        if (has_capability('moodle/site:config', get_context_instance(CONTEXT_SYSTEM))) {
            if (moodle_needs_upgrading()) {
                redirect($CFG->wwwroot .'/'. $CFG->admin .'/index.php');
            }
        } else if (!empty($CFG->mymoodleredirect)) {    // Redirect logged-in users to My Moodle overview if required
            if (isloggedin() && $USER->username != 'guest') {
                redirect($CFG->wwwroot .'/my/index.php');
            }
        }
    } else { // if upgrading from 1.6 or below
        if (isadmin() && moodle_needs_upgrading()) {
            redirect($CFG->wwwroot .'/'. $CFG->admin .'/index.php');
        }
    }


    if (get_moodle_cookie() == '') {
        set_moodle_cookie('nobody');   // To help search for cookies on login page
    }

    if (!empty($USER->id)) {
        add_to_log(SITEID, 'course', 'view', 'view.php?id='.SITEID, SITEID);
    }

    if (empty($CFG->langmenu)) {
        $langmenu = '';
    } else {
        $currlang = current_language();
        $langs = get_list_of_languages();
        $langlabel = get_accesshide(get_string('language'));
        $langmenu = popup_form($CFG->wwwroot .'/index.php?lang=', $langs, 'chooselang', $currlang, '', '', '', true, 'self', $langlabel);
    }

    $PAGE       = page_create_object(PAGE_COURSE_VIEW, SITEID);
    $pageblocks = blocks_setup($PAGE);
    $editing    = $PAGE->user_is_editing();
    $preferred_width_left  = bounded_number(BLOCK_L_MIN_WIDTH, blocks_preferred_width($pageblocks[BLOCK_POS_LEFT]),
                                            BLOCK_L_MAX_WIDTH);
    $preferred_width_right = bounded_number(BLOCK_R_MIN_WIDTH, blocks_preferred_width($pageblocks[BLOCK_POS_RIGHT]),
                                            BLOCK_R_MAX_WIDTH);
	print_header($SITE->fullname, $SITE->fullname, 'home', '',
                 '<meta name="description" content="'. strip_tags(format_text($SITE->summary, FORMAT_HTML)) .'" />',
                 true, '', user_login_string($SITE).$langmenu);
	
?>
<?php
$pid = $_REQUEST['pid'];
$select= mysql_fetch_array(mysql_query("SELECT name,parent_id,link FROM treeview_items1 where Id='$pid'"));
if(isset($_POST['submit_val']))
{
$nodename =mysql_real_escape_string($_POST['nodename']);
$nodeurl =mysql_real_escape_string($_POST['nodeurl']);
$menu_dom_node_val =mysql_real_escape_string($_POST['menu_dom_node_val']);
$timenow=time();
if($nodename!="" && $menu_dom_node_val!=""){
$update =execute_sql("UPDATE treeview_items1 SET `name`='$nodename',`link`='$nodeurl',`parent_id`='$menu_dom_node_val',`timemodified`='$timenow' WHERE Id='$pid'",false);
  if($update){
       echo '<script language="javascript">alert("Data Updated Successfully")</script>';
       echo '<script>window.location.href="'.$CFG->wwwroot.'/menu/menu_dom.php";</script>';
  }
 } else {
 $update =execute_sql("UPDATE treeview_items1 SET `name`='$nodename',`link`='$nodeurl',`timemodified`='$timenow' WHERE Id='$pid'",false);
  echo '<script language="javascript">alert("Data Updated Successfully")</script>';
  echo '<script>window.location.href="'.$CFG->wwwroot.'/menu/menu_dom.php";</script>';
 }
}
?>
<style type="text/css">
table.gridtable {
	font-family: verdana,arial,sans-serif;
	font-size:12px;
	color:#333333;
	border-width: 1px;
	border-color: #666666;
	border-collapse: collapse;
	width:1000px;
}
table.gridtable th {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #666666;
	background-color: #dedede;
}
table.gridtable td {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #666666;
	background-color: #ffffff;
}
</style>
<?php if(get_records_sql("SELECT id,userid,roleid FROM mdl_role_assignments  where roleid in('1')  and contextid=1 and userid='$USER->id'"))
		{ ?>
<fieldset>
   <legend><b>Update (India Menu):</b></legend>
    <a href="<?php $CFG->wwwroot; ?>/menu/menu_dom.php"><span style="margin-left: 1120px;color:red;"><b>Go Back</b></span></a>
<table class="gridtable" align="center">
<form name="pform" action="" method="post" onsubmit="return validation();">
				<div style="padding-bottom:10px;">
				<tr><td>Node</td>
				<td><input name="nodename" type="text" value='<?php echo $select['name']; ?>' id="nodename" style="width:430px;" placeholder="Enter Node Name" autocomplete="off"/></td>
				</tr>
				<?php 
				$resp = mysql_fetch_array(mysql_query("select name from treeview_items1 where Id='".$select['parent_id']."'"));
				?>
				<tr><td>Root Node</td>
				<?php if($resp!="") { ?>
					<td><input value='<?php echo $resp['name']; ?>' readonly="readonly" type="text" style="width:430px;" placeholder="Enter Node URL" autocomplete="off"/></td>
				<?php } else { ?>
				<td><input value='<?php echo "Root Node"; ?>' readonly="readonly" type="text" style="width:430px;" placeholder="Enter Node URL" autocomplete="off"/></td>
                <?php } ?>				
					</tr>
				<tr><td>URL</td>
					<td><input name="nodeurl" id="nodeurl" value='<?php echo $select['link']; ?>' type="text" style="width:430px;" placeholder="Enter Node URL" autocomplete="off"/></td>
					</tr>
				
				<tr>
					
					<td>Select Node</td>
					<td><select name="menu_dom_node_val" id="menu_dom_node_val" style="width: 150px">
					<option value=""><?php echo "Select Node"; ?></option>
					<option value="0"><?php echo "Root Node"; ?></option>
					<?php  $res = mysql_query("select Id,name from treeview_items1");
					       while($rw=mysql_fetch_array($res))
                          { ?>
						  <option value="<?php echo $rw['Id']; ?>"><?php echo $rw['name']; ?></option>
				    <?php  } ?>
						  
						  </select>
					 </td>
					</tr>
					<tr><td></td>
					<td><input type="submit" value="Add Node" name="submit_val"/></td>
					</tr>
				</div>		
		</td>
	</tr>
	</form>
</table><br>

<!-- Table goes in the document BODY -->
</fieldset>
<?php } ?>
<script type="text/javascript">
            function validation()
            { 
                $nodename = $("#nodename").val();
                if($nodename=="")
                {
                    $("#nodename").css("border-color","#ffe6e6");
                    $("#nodename").attr("placeholder","Node Name Required.");
                    $("#nodename").focus();
                    return false;
                }
                else
                {
                    $("#nodename").css("background","white");
                }
            }
                
               
</script>
<?php print_footer('home');  ?>
    


