<?php
    require_once('../config.php');
    require_once($CFG->dirroot .'../course/lib.php');
    require_once($CFG->dirroot .'../lib/blocklib.php');

    if (empty($SITE)) {
        redirect($CFG->wwwroot .'/'. $CFG->admin .'/index.php');
    }
    // Bounds for block widths
    // more flexible for theme designers taken from theme config.php
    $lmin = (empty($THEME->block_l_min_width)) ? 100 : $THEME->block_l_min_width;
    $lmax = (empty($THEME->block_l_max_width)) ? 210 : $THEME->block_l_max_width;
    $rmin = (empty($THEME->block_r_min_width)) ? 100 : $THEME->block_r_min_width;
    $rmax = (empty($THEME->block_r_max_width)) ? 210 : $THEME->block_r_max_width;

    define('BLOCK_L_MIN_WIDTH', $lmin);
    define('BLOCK_L_MAX_WIDTH', $lmax);
    define('BLOCK_R_MIN_WIDTH', $rmin);
    define('BLOCK_R_MAX_WIDTH', $rmax);

    // check if major upgrade needed - also present in login/index.php
    if ((int)$CFG->version < 2006101100) { //1.7 or older
        @require_logout();
        redirect("$CFG->wwwroot/$CFG->admin/");
    }
    // Trigger 1.9 accesslib upgrade?
    if ((int)$CFG->version < 2007092000 
        && isset($USER->id) 
        && is_siteadmin($USER->id)) { // this test is expensive, but is only triggered during the upgrade
        redirect("$CFG->wwwroot/$CFG->admin/");
    }

    if ($CFG->forcelogin) {
        require_login();
    } else {
        user_accesstime_log();
    }

    if ($CFG->rolesactive) { // if already using roles system
        if (has_capability('moodle/site:config', get_context_instance(CONTEXT_SYSTEM))) {
            if (moodle_needs_upgrading()) {
                redirect($CFG->wwwroot .'/'. $CFG->admin .'/index.php');
            }
        } else if (!empty($CFG->mymoodleredirect)) {    // Redirect logged-in users to My Moodle overview if required
            if (isloggedin() && $USER->username != 'guest') {
                redirect($CFG->wwwroot .'/my/index.php');
            }
        }
    } else { // if upgrading from 1.6 or below
        if (isadmin() && moodle_needs_upgrading()) {
            redirect($CFG->wwwroot .'/'. $CFG->admin .'/index.php');
        }
    }


    if (get_moodle_cookie() == '') {
        set_moodle_cookie('nobody');   // To help search for cookies on login page
    }

    if (!empty($USER->id)) {
        add_to_log(SITEID, 'course', 'view', 'view.php?id='.SITEID, SITEID);
    }

    if (empty($CFG->langmenu)) {
        $langmenu = '';
    } else {
        $currlang = current_language();
        $langs = get_list_of_languages();
        $langlabel = get_accesshide(get_string('language'));
        $langmenu = popup_form($CFG->wwwroot .'/index.php?lang=', $langs, 'chooselang', $currlang, '', '', '', true, 'self', $langlabel);
    }

    $PAGE       = page_create_object(PAGE_COURSE_VIEW, SITEID);
    $pageblocks = blocks_setup($PAGE);
    $editing    = $PAGE->user_is_editing();
    $preferred_width_left  = bounded_number(BLOCK_L_MIN_WIDTH, blocks_preferred_width($pageblocks[BLOCK_POS_LEFT]),
                                            BLOCK_L_MAX_WIDTH);
    $preferred_width_right = bounded_number(BLOCK_R_MIN_WIDTH, blocks_preferred_width($pageblocks[BLOCK_POS_RIGHT]),
                                            BLOCK_R_MAX_WIDTH);
	print_header($SITE->fullname, $SITE->fullname, 'home', '',
                 '<meta name="description" content="'. strip_tags(format_text($SITE->summary, FORMAT_HTML)) .'" />',
                 true, '', user_login_string($SITE).$langmenu);
	
?>
<?php
$select= mysql_query("SELECT Id,name,parent_id,link,timecreated,status FROM treeview_items1");
if(isset($_POST['submit_val']))
{
$nodename =mysql_real_escape_string($_POST['nodename']);
$nodeurl =mysql_real_escape_string($_POST['nodeurl']);
$menu_dom_node_val =mysql_real_escape_string($_POST['menu_dom_node_val']);
$ip =$_SERVER['REMOTE_ADDR'];
$timenow=time();
$sel = mysql_num_rows(mysql_query("SELECT name FROM treeview_items1 where `name`='$nodename'"));
if($sel>0 || $nodename=="" )
{
echo '<script language="javascript">alert("Menu Name Already Exists")</script>';
echo '<script>window.location.href="'.$CFG->wwwroot.'/menu/menu_dom.php";</script>';
} else {
$sql =execute_sql("INSERT INTO treeview_items1 (`name`, `link`, `status`,`parent_id`, `ipaddress`, `timecreated`) VALUES ('$nodename','$nodeurl','1','$menu_dom_node_val','$ip','$timenow')",false);
  if($sql){
       echo '<script language="javascript">alert("Data Inserted Successfully")</script>';
       echo '<script>window.location.href="'.$CFG->wwwroot.'/menu/menu_dom.php";</script>';
  }
 }
 }
?>
<?php
if(isset($_REQUEST['Action'])=='status')
{
$pid=$_REQUEST['pid'];
$select=mysql_fetch_array(mysql_query("select status from treeview_items1 where Id='$pid'"));
$vals=$select['status'];
if($vals=='0')
{
$status_state=1;
}
else
{
$status_state=0;
}
$update=mysql_query("update treeview_items1 set status='$status_state' where Id='$pid'");
if($update){
       echo '<script>window.location.href="'.$CFG->wwwroot.'/menu/menu_dom.php";</script>';
  }
}
?>
<style type="text/css">
table.gridtable {
	font-family: verdana,arial,sans-serif;
	font-size:12px;
	color:#333333;
	border-width: 1px;
	border-color: #666666;
	border-collapse: collapse;
	width:1100px;
}
table.gridtable th {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #666666;
	background-color: #dedede;
}
table.gridtable td {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #666666;
	background-color: #ffffff;
}
</style>
<?php if(get_records_sql("SELECT id,userid,roleid FROM mdl_role_assignments  where roleid in('1')  and contextid=1 and userid='$USER->id'"))
		{ ?>
<fieldset>
   <legend><b>Dashboard (India NLCI Menu):</b></legend>
   <a href="<?php $CFG->wwwroot; ?>/menu/menu_intl.php"><span style="margin-left: 930px;color:red;"><b>Click here for Rest of the World NLCI Menu</b></span></a>
<table class="gridtable" align="center">
<form name="pform" action="" method="post" onsubmit="return validation();">
				<div style="padding-bottom:10px;">
				<tr><td>Node</td>
				<td><input name="nodename" type="text" id="nodename" style="width:430px;" placeholder="Enter Node Name" autocomplete="off"/></td>
				</tr>
				<tr><td>URL</td>
					<td><input name="nodeurl" id="nodeurl" type="text" style="width:430px;" placeholder="Enter Node URL" autocomplete="off"/></td>
					</tr>
			<tr>
					
					<td>Select Node</td>
					<td><select name="menu_dom_node_val" id="menu_dom_node_val" style="width: 150px">
					<option value="0"><?php echo "Root Node"; ?></option>
					<?php  $res = mysql_query("select Id,name from treeview_items1");
					       while($rw=mysql_fetch_array($res))
                          { ?>
						  <option value="<?php echo $rw['Id']; ?>"><?php echo $rw['name']; ?></option>
				    <?php  } ?>
						  
						  </select>
					 </td>
					</tr>
					
					<tr><td></td>
					<td><input type="submit" value="Add Node" name="submit_val"/></td>
					</tr>
				</div>		
		</td>
	</tr>
	</form>
</table><br>

<!-- Table goes in the document BODY -->
<table class="gridtable" align="center">
<tr>
    <th>S. NO.</th>
	<th>Node Name</th>
	<th>Parent Node Name</th>
	<th>Node URL</th>
	<th>Created On</th>
	<th>Action</th>
</tr>
<?php 
$count=mysql_num_rows($select);
if($count>0){
$i=1;
while($vars=mysql_fetch_array($select))
{ $parent_id = $vars['parent_id'];
$pname =mysql_fetch_array(mysql_query("select name from treeview_items1 where Id='$parent_id'"));
?>
<tr>
    <td><?php echo $i; ?></td>
    <td><?php echo $vars['name']; ?></td>
	<?php if($pname['name']) { ?>
	<td><?php echo $pname['name']; ?></td>
	<?php } else { ?>
	<td><font size="1.5" color="blue"><b><?php echo "Root Node"; ?></b></font></td>
	<?php } ?>
	<?php if($vars['link']){ ?>
	<td><?php echo $vars['link']; ?></td>
	<?php } else { ?>
	<td>--</td>
	<?php } ?>
	<td><?php echo date ("Y-m-d H:i:s", $vars['timecreated']); ?></td>
	<td align="center">
	<a href="menu_dom_edit.php?pid=<?php echo $vars['Id']; ?>" title="Click Here to Edit"><img src="<?php echo $CFG->wwwroot; ?>/theme/aardvark-svc/pix/t/edit.png" style="width:18px;"></a>&emsp;&emsp;
	 <?php
     if($vars['status']=='0')
     {
     ?>
     <a href="menu_dom.php?pid=<?php echo $vars['Id'];?>&Action=status" title="Click Here to Activate" onclick="return confirm('Are You Sure to Activate <?php echo $vars['name']; ?>');"> <img src="<?php echo $CFG->wwwroot; ?>/theme/aardvark-svc/pix/t/show.png" style="width:18px;"> </a>
     <?php
     }
     if($vars['status']=='1')
    {
    ?>
    <a href="menu_dom.php?pid=<?php echo $vars['Id'];?>&Action=status" title="Click Here to De-Activate" onclick="return confirm('Are You Sure to De-activate <?php echo $vars['name'];?>');"> <img src="<?php echo $CFG->wwwroot; ?>/theme/aardvark-svc/pix/t/hide.png" style="width:18px;"></a>
    <?php
    }
    ?>
</td>
</tr>
<?php $i++; } } else {?>
<tr>
    <td colspan="6"><center><b><?php echo "No Records Found"; ?></b></center></td>
	</tr>
<?php } ?>
</table>
</fieldset>
<?php } ?>
<script type="text/javascript">
            function validation()
            { 
                $nodename = $("#nodename").val();
                if($nodename=="")
                {
                    $("#nodename").css("border-color","#ffe6e6");
                    $("#nodename").attr("placeholder","Node Name Required.");
                    $("#nodename").focus();
                    return false;
                }
                else
                {
                    $("#nodename").css("background","white");
                }
				
            }
                
               
</script>
<?php print_footer('home');  ?>
    


