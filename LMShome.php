<?php  

    if (!file_exists('./config.php')) {
        header('Location: install.php');
        die;
    }
	
    require_once('config.php');
    require_once($CFG->dirroot .'/course/lib.php');
    require_once($CFG->dirroot .'/lib/blocklib.php');

        redirect($CFG->wwwroot .'/index.php');

?>
