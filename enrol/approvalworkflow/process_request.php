<?php
	ini_set("display_errors","1");
ERROR_REPORTING(E_ALL);
    require("../../config.php");
	require_once $CFG->libdir.'/accesslib.php';

    // Allow access only to admin 
 //  require_capability('moodle/legacy:admin', get_context_instance(CONTEXT_SYSTEM), NULL, false);
    
    // Get mandatory params
    $cid = required_param('cid', PARAM_INT); // course
    $uid = required_param('uid', PARAM_INT); // user
    $action = required_param('action', PARAM_ACTION); // enrol OR delete
    
    $course = get_record("course", "id", $cid);
    $user = get_record("user", "id", $uid);     
   
    if($action=='delete'){
		delete_records('enrol_approvalworkflow','userid',$uid,'courseid',$cid); 
		// Send email to student
		$a->site = $SITE->shortname;
        $a->course = $course->shortname;        

        $subject = get_string('applicationsubject','enrol_approvalworkflow',$a);
        $body = get_string('applicationbodyrejected','enrol_approvalworkflow');
email_to_user($user, $SITE->shortname, $subject, $body);
redirect($CFG->wwwroot.'/enrol/approvalworkflow/show_requests.php',get_string('applicationrejected','enrol_approvalworkflow'),3);    
    }
   
   if ($action=='enrol'){
        if (!enrol_into_course($course, $user, 'manual')) {
	       print_error('couldnotassignrole');
        }
		delete_records('enrol_approvalworkflow','userid',$uid,'courseid',$cid); 
		 No needo to send email because enrol_into_course() sends it.
		redirect($CFG->wwwroot.'/enrol/approvalworkflow/show_requests.php',get_string('applicationaproved','approvalworkflow'),3);    
    } 
?>