<?php

    require("../../config.php");
	require_login();
	$sql = "SELECT em.*, u.id AS uid, u.firstname, u.lastname, u.username, c.shortname, em.reason
            FROM {$CFG->prefix}enrol_skillsoft em
            LEFT OUTER JOIN {$CFG->prefix}user u ON u.id = em.userid
            LEFT OUTER JOIN {$CFG->prefix}course c ON c.id = em.courseid
		    WHERE em.userid=$USER->id
            ORDER BY em.created";

    $navlinks = array();
    $navlinks[] = array('name' => 'Enrolment Requests', 'link' => ".", 'type' => 'misc');
    $navigation = build_navigation($navlinks);

    $rows = get_records_sql($sql);
    $output = '<table class="generaltable boxaligncenter" width="100%" cellspacing="1" cellpadding="5">';
    $output .= '<tr>
                <th align="left">PortalID</th>
				<th align="left">Employee Name</th>
				<th align="left">Grade</th>              				
				<th align="left">Course</th>
				<th align="left">Reason</th>
				<th align="left">Request Staus</th>
                <th align="left">Requested Date</th>
                </tr>';
    
    if(!empty($rows)){
        foreach ($rows as $row){
            $url = $CFG->wwwroot.'/enrol/skillsoft/process_request.php?cid='.$row->courseid.'&uid='.$row->userid;
        	$output .= '<tr>';
			$output .= "<td>$row->username</td>";
            $output .= "<td>$row->firstname $row->lastname</td>";
			$output .= "<td>$row->grade</td>";      
			$output .= "<td>$row->shortname</td>";
			$output .= "<td>$row->reason</td>";
			if($row->status==0)
			{
			$output .= "<td>Open</td>";
			}
			else if($row->status==1)
			{
            $output .= "<td>Approved by manager, Access pending</td>";
			}
			else if($row->status==2)
			{
            $output .= "<td>Access Granted</td>";
			}
			$output .= "<td>".userdate($row->created, '%d %b %y')."</td>";
          
            $output .= '</tr>';    
    	}
    }    
    $output .= '</table>';
    
    print_header(get_string('applicationsenrolment','enrol_skillsoft'),get_string('applicationsenrolment','enrol_skillsoft'), $navigation);

echo "<p>Skillsoft is an external eLearning partner that provides courseware for business, IT and desktop skills for NTT DATA.  Access into the system is granted on an annual basis for each individual.  At the end of the year, the license will either renew automatically or be put on hold depending on the number of licenses available and the number of people needing access for learning programs.  All of the training in Skillsoft system is self-directed learning.  The courses are used in certification programs and learning programs throughout the company.  Preference is given to those who do not have a physical classroom locally available.</p>"; 
 
 if($rows)
 {
 print_box($output);
 }
 else
 {
 error('Currently you have not raised any SkillSoft request.');
 }
   
    print_footer();
?>