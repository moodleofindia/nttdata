
<?php

    require("../../config.php");
	require_login();

    // Allow access only to admin
	
    $rowusername=get_record('user','id',$USER->id);
	$admins=get_records_sql("SELECT U.id FROM mdl_role_capabilities C join mdl_role_assignments A
on C.roleid=A.roleid and C.capability='moodle/site:approvecourse'
and C.permission=1 and A.contextid=1 join mdl_user U on U.id = A.userid and U.id=$USER->id");

	if($admins)
	{
	$sql = "SELECT em.*, u.id AS uid, u.firstname, u.lastname, u.username,u.grade, c.shortname, em.reason
            FROM {$CFG->prefix}enrol_skillsoft em
            LEFT OUTER JOIN {$CFG->prefix}user u ON u.id = em.userid
            LEFT OUTER JOIN {$CFG->prefix}course c ON c.id = em.courseid
		    WHERE status = 0 or status=1
            ORDER BY em.created";
	}
	else
	{
	$sql = "SELECT em.*, u.id AS uid, u.firstname, u.lastname, u.username,u.grade, c.shortname, em.reason,em.created
            FROM {$CFG->prefix}enrol_skillsoft em
            LEFT OUTER JOIN {$CFG->prefix}user u ON u.id = em.userid
            LEFT OUTER JOIN {$CFG->prefix}course c ON c.id = em.courseid
		    WHERE status = 0 and u.manager_portalid=$rowusername->username
            ORDER BY em.created";
	}
    
    $navlinks = array();
    $navlinks[] = array('name' => 'Enrolment Requests', 'link' => ".", 'type' => 'misc');
    $navigation = build_navigation($navlinks);

    $rows = get_records_sql($sql);
    $output = '<table class="generaltable boxaligncenter" width="100%" cellspacing="1" cellpadding="5">';
    $output .= '<tr>
                <th align="left">PortalID</th>
				<th align="left">Employee Name</th>
				<th align="left">Grade</th>              				
				<th align="left">Course</th>
				<th align="left">Reason</th>
				<th align="left">Request Status</th>
                <th align="left">Requested Date</th>
                <th align="left">Action</th>
                </tr>';
    
    if(!empty($rows)){
        foreach ($rows as $row){
            $url = $CFG->wwwroot.'/enrol/skillsoft/process_request.php?cid='.$row->courseid.'&uid='.$row->userid;
        	$output .= '<tr>';
			$output .= "<td>$row->username</td>";
            $output .= "<td>$row->firstname $row->lastname</td>";
			$output .= "<td>$row->grade</td>";      
			$output .= "<td>$row->shortname</td>";
			$output .= "<td>$row->reason</td>";
			if($row->status==0)
			{
			$output .= "<td>Open</td>";
			}
			else if($row->status==1)
			{
            $output .= "<td>Approved(M)</td>";
			}
			$output .= "<td>".userdate($row->created, '%d %b %y')."</td>";
			if($admins)
			{
            $output .= "<td><a href='{$url}&action=approve'>".get_string('aprove','enrol_skillsoft')."</a> ";
			$output .= "&nbsp;&nbsp;<a href='{$url}&action=reject'>".get_string('reject','enrol_skillsoft')."</a></td>";
			}
			else
			{
			$output .= "<td><a href='{$url}&action=enrol'>".get_string('aprove','enrol_skillsoft')."</a> ";
			$output .= "&nbsp;&nbsp;<a href='{$url}&action=delete'>".get_string('reject','enrol_skillsoft')."</a></td>";
			}           
            $output .= '</tr>';    
    	}
    }    
    $output .= '</table>';
    
    print_header(get_string('applicationsenrolment','enrol_skillsoft'),get_string('applicationsenrolment','enrol_skillsoft'), $navigation);
echo "<p>Your employee(s) below are requesting access to the Skillsoft system.  Please review and approve or reject the access request.  Please note that manager approval is required before an employee can be issued a Skillsoft license.  Employees must be in Grade 7 or higher with a valid business reason as determined by the manager.  All requests are subject to the system availability.</p>"; 
echo "<p>Skillsoft is an external eLearning partner that provides courseware for business, IT and desktop skills for NTT DATA.  Access into the system is granted on an annual basis for each individual.  At the end of the year, the license will either renew automatically or be put on hold depending on the number of licenses available and the number of people needing access for learning programs.  All of the training in Skillsoft system is self-directed learning.  The courses are used in certification programs and learning programs throughout the company.  Preference is given to those who do not have a physical classroom locally available.</p>"; 
 
 if($rows)
 {
 print_box($output);
 }
 else
 {
 error('Currently you have no SkillSoft enrollment request to approve/reject.');
 }
   
    print_footer();
?>