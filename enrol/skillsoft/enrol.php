<?php  // $Id: enrol.php,v 1.0 2009/10/19 19:21:11 skodak Exp $
       // Implements all the main code for the approvalworkflow enrolment

require_once("$CFG->dirroot/enrol/enrol.class.php");

class enrolment_plugin_skillsoft{

var $errormsg;


/**
* Prints the entry form/page for this enrolment
*
* This is only called from course/enrol.php
* Most plugins will probably override this to print payment ...
*
* @param    course  current course object
*/
function print_entry($course) {
    global $CFG, $USER, $SESSION, $THEME, $SITE;

    $strloginto = get_string('loginto', '', $course->shortname);
    $strcourses = get_string('courses');

    $context = get_context_instance(CONTEXT_SYSTEM);

    $navlinks = array();
    $navlinks[] = array('name' => $strcourses, 'link' => ".", 'type' => 'misc');
    $navlinks[] = array('name' => $strloginto, 'link' => null, 'type' => 'misc');
    $navigation = build_navigation($navlinks);

    if (has_capability('moodle/legacy:guest', $context, $USER->id, false)) {
        add_to_log($course->id, 'course', 'guest', 'view.php?id='.$course->id, getremoteaddr());
        return;
    }
    
    if (empty($_GET['confirm']) && empty($_GET['cancel'])) {
	$newuser = get_record('user', 'id', $USER->id);
			
	if(get_record('enrol_skillsoft','userid',$USER->id,'status',2))
	{
	
	  if (!enrol_into_course($course, $USER, 'manual')) {
                print_error('couldnotassignrole');
            }
            // force a refresh of mycourses
            unset($USER->mycourses);
			      $destination = "$CFG->wwwroot/course/view.php?id=$course->id";
        
        redirect($destination);
	
	}
	else if (($newuser->country == 'US' || $newuser->country == 'CA') && ($newuser->employee_type == 'Employee' ||$newuser->employee_type == 'EMPLOYEE' || $newuser->employee_type == 'E' || $newuser->employee_type =='REGULAR EMPLOYEE'))
	{
        print_header($strloginto, $course->fullname, $navigation);
        echo '<br />';
		 print_box_start('generalbox', 'notice');

		echo get_string('usenrolmentrequest','enrol_skillsoft');
		
		echo '<div class="buttons">';
		echo '<form action="enrol.php?id='.$course->id.'&amp;confirm=1" method="post">';
		echo '<div>';
		echo '<Input type = "Radio" Name ="reason" value= "Project-related Training">Project-related Training';
		echo '<Input type = "Radio" Name ="reason" value= "Career Development">Career Development';
		echo '<Input type = "Radio" Name ="reason" value= "Personal Development">Personal Development';
		echo '<br/>';
		echo '<br/>';
		echo '<input type="submit" value="Apply"   />';
		echo '</div>';
		echo '</form>';
		echo '<form action="enrol.php?id='.$course->id.'&amp;cancel=1" method="post">';
		echo '<div>';
		echo '<input type="submit" value="Cancel"   />';
		echo '</div>';
		echo '</form>';
		echo '</div>';

        print_footer();
        return;
	}

	else if ($newuser->grade>=7||$newuser->grade=='UD'||$newuser->grade=='UA'||$newuser->grade=='UB'||$newuser->grade=='Senior Management')
	{
        print_header($strloginto, $course->fullname, $navigation);
        echo '<br />';
		 print_box_start('generalbox', 'notice');

		echo get_string('enrolmentrequest','enrol_skillsoft');
		
		echo '<div class="buttons">';
		echo '<form action="enrol.php?id='.$course->id.'&amp;confirm=1" method="post">';
		echo '<div>';
		echo '<Input type = "Radio" Name ="reason" value= "Project-related Training">Project-related Training';
		echo '<Input type = "Radio" Name ="reason" value= "Career Development">Career Development';
		echo '<Input type = "Radio" Name ="reason" value= "Personal Development">Personal Development';
		echo '<br/>';
		echo '<br/>';
		echo '<input type="submit" value="Apply"   />';
		echo '</div>';
		echo '</form>';
		echo '<form action="enrol.php?id='.$course->id.'&amp;cancel=1" method="post">';
		echo '<div>';
		echo '<input type="submit" value="Cancel"   />';
		echo '</div>';
		echo '</form>';
		echo '</div>';

        print_footer();
        return;
	}
		
	else if ($newuser->grade<7||$newuser->grade=='A'||$newuser->grade=='B'||$newuser->grade=='Contractor')
	{
		print_header($strloginto, $course->fullname, $navigation);
        echo '<br />';
		print_box_start('generalbox', 'notice');
		echo 'In order to apply for a Skillsoft license, you must be Grade 7 or above and not a contractor';
		print_continue($CFG->wwwroot, $return=false);
		print_footer();
	return;

	}
	
	else
	{
		print_header($strloginto, $course->fullname, $navigation);
        echo '<br />';
		print_box_start('generalbox', 'notice');
		echo 'User eligibility for a Skillsoft license cannot be determined based on the system data available.  Please contact Training Helpdesk for assistance and exception processing.';
		print_continue($CFG->wwwroot, $return=false);
		print_footer();
	return;
	}
    }
		
    if (!empty($_GET['confirm'])) {
        print_header($strloginto, $course->fullname, $navigation);
        print_box(get_string('application','enrol_skillsoft'));
        
        
        // Send email to student
        $a->site = $SITE->shortname;
        $a->course = $course->shortname;
		$a->urlu = $CFG->wwwroot.'/enrol/skillsoft/show_requests_user.php';
        $subject = get_string('applicationsubject','enrol_skillsoft');
		$body = '<html xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:w="urn:schemas-microsoft-com:office:word" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns:p="urn:schemas-microsoft-com:office:powerpoint" xmlns:a="urn:schemas-microsoft-com:office:access" xmlns:dt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882" xmlns:s="uuid:BDC6E3F0-6DA3-11d1-A2A3-00AA00C14882" xmlns:rs="urn:schemas-microsoft-com:rowset" xmlns:z="#RowsetSchema" xmlns:b="urn:schemas-microsoft-com:office:publisher" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:c="urn:schemas-microsoft-com:office:component:spreadsheet" xmlns:odc="urn:schemas-microsoft-com:office:odc" xmlns:oa="urn:schemas-microsoft-com:office:activation" xmlns:html="http://www.w3.org/TR/REC-html40" xmlns:q="http://schemas.xmlsoap.org/soap/envelope/" xmlns:rtc="http://microsoft.com/officenet/conferencing" xmlns:D="DAV:" xmlns:Repl="http://schemas.microsoft.com/repl/" xmlns:mt="http://schemas.microsoft.com/sharepoint/soap/meetings/" xmlns:x2="http://schemas.microsoft.com/office/excel/2003/xml" xmlns:ppda="http://www.passport.com/NameSpace.xsd" xmlns:ois="http://schemas.microsoft.com/sharepoint/soap/ois/" xmlns:dir="http://schemas.microsoft.com/sharepoint/soap/directory/" xmlns:ds="http://www.w3.org/2000/09/xmldsig#" xmlns:dsp="http://schemas.microsoft.com/sharepoint/dsp" xmlns:udc="http://schemas.microsoft.com/data/udc" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:sub="http://schemas.microsoft.com/sharepoint/soap/2002/1/alerts/" xmlns:ec="http://www.w3.org/2001/04/xmlenc#" xmlns:sp="http://schemas.microsoft.com/sharepoint/" xmlns:sps="http://schemas.microsoft.com/sharepoint/soap/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:udcs="http://schemas.microsoft.com/data/udc/soap" xmlns:udcxf="http://schemas.microsoft.com/data/udc/xmlfile" xmlns:udcp2p="http://schemas.microsoft.com/data/udc/parttopart" xmlns:wf="http://schemas.microsoft.com/sharepoint/soap/workflow/" xmlns:dsss="http://schemas.microsoft.com/office/2006/digsig-setup" xmlns:dssi="http://schemas.microsoft.com/office/2006/digsig" xmlns:mdssi="http://schemas.openxmlformats.org/package/2006/digital-signature" xmlns:mver="http://schemas.openxmlformats.org/markup-compatibility/2006" xmlns:m="http://schemas.microsoft.com/office/2004/12/omml" xmlns:mrels="http://schemas.openxmlformats.org/package/2006/relationships" xmlns:spwp="http://microsoft.com/sharepoint/webpartpages" xmlns:ex12t="http://schemas.microsoft.com/exchange/services/2006/types" xmlns:ex12m="http://schemas.microsoft.com/exchange/services/2006/messages" xmlns:pptsl="http://schemas.microsoft.com/sharepoint/soap/SlideLibrary/" xmlns:spsl="http://microsoft.com/webservices/SharePointPortalServer/PublishedLinksService" xmlns:Z="urn:schemas-microsoft-com:" xmlns:st="&#1;" xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<meta name=Generator content="Microsoft Word 12 (filtered medium)">
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
@font-face
	{font-family:Tahoma;
	panose-1:2 11 6 4 3 5 4 4 2 4;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin:0in;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";}
a:link, span.MsoHyperlink
	{mso-style-priority:99;
	color:blue;
	text-decoration:underline;}
a:visited, span.MsoHyperlinkFollowed
	{mso-style-priority:99;
	color:purple;
	text-decoration:underline;}
p
	{mso-style-priority:99;
	mso-margin-top-alt:auto;
	margin-right:0in;
	mso-margin-bottom-alt:auto;
	margin-left:0in;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";}
span.EmailStyle18
	{mso-style-type:personal-reply;
	font-family:"Calibri","sans-serif";
	color:#1F497D;}
.MsoChpDefault
	{mso-style-type:export-only;}
@page Section1
	{size:8.5in 11.0in;
	margin:1.0in 1.0in 1.0in 1.0in;}
div.Section1
	{page:Section1;}
-->
</style>
<!--[if gte mso 9]><xml>
 <o:shapedefaults v:ext="edit" spidmax="1026" />
</xml><![endif]--><!--[if gte mso 9]><xml>
 <o:shapelayout v:ext="edit">
  <o:idmap v:ext="edit" data="1" />
 </o:shapelayout></xml><![endif]-->
</head>

<body lang=EN-US link=blue vlink=purple>



<p>Your enrollment request will be reviewed as soon as possible by your manager. <br/>
Once your manager responds to the request, it will be submitted to the Skillport Admin queue for review. Please note that the process may take up to five business days for review.<br/>
You may check the status of this request at any time by following this link '.$a->urlu.' <br/><br/>
Thanks,<br>
Training Administration Team<o:p></o:p></p>

</div>

</body>

</html>
';
	
		$emailuser = get_record('user', 'id', $USER->id);
        if(!email_to_user($emailuser,'', $subject,'',$body))
		{
		print_string('Mail could not sent, Kindly contact Training Helpdesk.');
		}
        
        // Send email to admin
        $a->url = $CFG->wwwroot.'/enrol/skillsoft/show_requests.php';
        $a->user = $USER->username.': '.$USER->firstname.' '.$USER->lastname;
		$a->course = $course->shortname;
		$a->name = $USER->firstname.' '.$USER->lastname;
		$a->reason =  $_POST['reason'];
		$full = $str1.$str2.$str3;
		
		$body = '<html xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:w="urn:schemas-microsoft-com:office:word" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns:p="urn:schemas-microsoft-com:office:powerpoint" xmlns:a="urn:schemas-microsoft-com:office:access" xmlns:dt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882" xmlns:s="uuid:BDC6E3F0-6DA3-11d1-A2A3-00AA00C14882" xmlns:rs="urn:schemas-microsoft-com:rowset" xmlns:z="#RowsetSchema" xmlns:b="urn:schemas-microsoft-com:office:publisher" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:c="urn:schemas-microsoft-com:office:component:spreadsheet" xmlns:odc="urn:schemas-microsoft-com:office:odc" xmlns:oa="urn:schemas-microsoft-com:office:activation" xmlns:html="http://www.w3.org/TR/REC-html40" xmlns:q="http://schemas.xmlsoap.org/soap/envelope/" xmlns:rtc="http://microsoft.com/officenet/conferencing" xmlns:D="DAV:" xmlns:Repl="http://schemas.microsoft.com/repl/" xmlns:mt="http://schemas.microsoft.com/sharepoint/soap/meetings/" xmlns:x2="http://schemas.microsoft.com/office/excel/2003/xml" xmlns:ppda="http://www.passport.com/NameSpace.xsd" xmlns:ois="http://schemas.microsoft.com/sharepoint/soap/ois/" xmlns:dir="http://schemas.microsoft.com/sharepoint/soap/directory/" xmlns:ds="http://www.w3.org/2000/09/xmldsig#" xmlns:dsp="http://schemas.microsoft.com/sharepoint/dsp" xmlns:udc="http://schemas.microsoft.com/data/udc" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:sub="http://schemas.microsoft.com/sharepoint/soap/2002/1/alerts/" xmlns:ec="http://www.w3.org/2001/04/xmlenc#" xmlns:sp="http://schemas.microsoft.com/sharepoint/" xmlns:sps="http://schemas.microsoft.com/sharepoint/soap/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:udcs="http://schemas.microsoft.com/data/udc/soap" xmlns:udcxf="http://schemas.microsoft.com/data/udc/xmlfile" xmlns:udcp2p="http://schemas.microsoft.com/data/udc/parttopart" xmlns:wf="http://schemas.microsoft.com/sharepoint/soap/workflow/" xmlns:dsss="http://schemas.microsoft.com/office/2006/digsig-setup" xmlns:dssi="http://schemas.microsoft.com/office/2006/digsig" xmlns:mdssi="http://schemas.openxmlformats.org/package/2006/digital-signature" xmlns:mver="http://schemas.openxmlformats.org/markup-compatibility/2006" xmlns:m="http://schemas.microsoft.com/office/2004/12/omml" xmlns:mrels="http://schemas.openxmlformats.org/package/2006/relationships" xmlns:spwp="http://microsoft.com/sharepoint/webpartpages" xmlns:ex12t="http://schemas.microsoft.com/exchange/services/2006/types" xmlns:ex12m="http://schemas.microsoft.com/exchange/services/2006/messages" xmlns:pptsl="http://schemas.microsoft.com/sharepoint/soap/SlideLibrary/" xmlns:spsl="http://microsoft.com/webservices/SharePointPortalServer/PublishedLinksService" xmlns:Z="urn:schemas-microsoft-com:" xmlns:st="&#1;" xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<meta name=Generator content="Microsoft Word 12 (filtered medium)">
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
@font-face
	{font-family:Tahoma;
	panose-1:2 11 6 4 3 5 4 4 2 4;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin:0in;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";}
a:link, span.MsoHyperlink
	{mso-style-priority:99;
	color:blue;
	text-decoration:underline;}
a:visited, span.MsoHyperlinkFollowed
	{mso-style-priority:99;
	color:purple;
	text-decoration:underline;}
p
	{mso-style-priority:99;
	mso-margin-top-alt:auto;
	margin-right:0in;
	mso-margin-bottom-alt:auto;
	margin-left:0in;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";}
span.EmailStyle18
	{mso-style-type:personal-reply;
	font-family:"Calibri","sans-serif";
	color:#1F497D;}
.MsoChpDefault
	{mso-style-type:export-only;}
@page Section1
	{size:8.5in 11.0in;
	margin:1.0in 1.0in 1.0in 1.0in;}
div.Section1
	{page:Section1;}
-->
</style>
<!--[if gte mso 9]><xml>
 <o:shapedefaults v:ext="edit" spidmax="1026" />
</xml><![endif]--><!--[if gte mso 9]><xml>
 <o:shapelayout v:ext="edit">
  <o:idmap v:ext="edit" data="1" />
 </o:shapelayout></xml><![endif]-->
</head>

<body lang=EN-US link=blue vlink=purple>



<p>Employee '.$a->user.' is requesting access to the Skillsoft '.$course->shortname.' for '.$a->reason.'.<br/> 

You are receiving this workflow because you are listed in the HR Data Mart as '.$a->user.' manager. Please note that manager approval is required before an employee can be issued a Skillsoft license. <br/> Employees must be in Grade 7 or higher with a valid business reason as determined by the manager.  All requests are subject to the system availability. 
To process use this link '.$a->url.'<br/><br/>
Thanks,<br>
Training Administration Team<o:p></o:p></p>

</div>

</body>

</html>
';
 
		

		
		$destMailRecord = get_records_sql("SELECT * FROM mdl_user
			where username=(SELECT manager_portalid FROM mdl_user where id=$USER->id)"); 

			if($destMailRecord)
			{
				$destMail = $destMailRecord;
			}
			else
			{
			$destMail = get_records_sql("SELECT u.* FROM mdl_role_capabilities C join mdl_role_assignments A
			on C.roleid=A.roleid and C.capability='moodle/site:approvecourse'
			and C.permission=1 and A.contextid=1 join mdl_user U on U.id = A.userid"); 
			}

			foreach ($destMail as $u) {
			if(!email_to_user($u,'', $subject,'',$body))
			{
			print_string('Mail could not sent to the manager, Kindly contact Training Helpdesk.');
			}
			$managerfname=$u->firstname;
			$managerlname=$u->lastname;
			print_box('A mail has been sent to '.$managerfname.' '.$managerlname.' for approval. If you are unable to find a conformation mail in your inbox, Please check your junk folder.');
									}           
        
      

        // Check if user has already requested enrolment for the same course
        if (!get_record('enrol_skillsoft','userid',$USER->id)){
                // Create db entry
                $data = new object();
                $data->courseid = $course->id;
                $data->userid = $USER->id;
				$data->reason =  $_POST['reason'];
                $data->created = time();
                $data->updated = time();
                $data->status = 0; // pending 
                insert_record('enrol_skillsoft', addslashes_object($data), false);		            	
        } 
		print_continue($CFG->wwwroot, $return=false);
		print_footer();		
        return;
    }
	else
	{
	print 'error..';
	}
            
    if (!empty($_GET['cancel'])) {
        unset($SESSION->wantsurl);
        if (!empty($SESSION->enrolcancel)) {
            $destination = $SESSION->enrolcancel;
            unset($SESSION->enrolcancel);
        } else {
            $destination = $CFG->wwwroot;
        }
        redirect($destination);
    }
}



/**
* The other half to print_entry, this checks the form data
*
* This function checks that the user has completed the task on the
* enrolment entry page and then enrolls them.
*
* @param    form    the form data submitted, as an object
* @param    course  the current course, as an object
*/
function check_entry($form, $course) {
    global $CFG, $USER, $SESSION, $THEME;

}


/**
* Check if the given enrolment key matches a group enrolment key for the given course
*
* @param    courseid  the current course id
* @param    password  the submitted enrolment key
*/
function check_group_entry ($courseid, $password) {

    if ($groups = groups_get_all_groups($courseid)) {
        foreach ($groups as $group) {
            if ( !empty($group->enrolmentkey) and (stripslashes($password) == $group->enrolmentkey) ) {
                return $group->id;
            }
        }
    }

    return false;
}


/**
* Prints a form for configuring the current enrolment plugin
*
* This function is called from admin/enrol.php, and outputs a
* full page with a form for defining the current enrolment plugin.
*
* @param    frm  an object containing all the data for this page
*/
function config_form($frm) {
    global $CFG;

    if (!isset( $frm->enrol_approvalworkflow_keyholderrole )) {
        $frm->enrol_approvalworkflow_keyholderrole = '';
    }

    if (!isset($frm->enrol_approvalworkflow_showhint)) {
        $frm->enrol_approvalworkflow_showhint = 1;
    }

    if (!isset($frm->enrol_approvalworkflow_usepasswordpolicy)) {
        $frm->enrol_approvalworkflow_usepasswordpolicy = 0;
    }

    if (!isset($frm->enrol_approvalworkflow_requirekey)) {
        $frm->enrol_approvalworkflow_requirekey = 0;
    }

    include ("$CFG->dirroot/enrol/skillsoft/config.html");
}


/**
* Processes and stored configuration data for the enrolment plugin
*
* @param    config  all the configuration data as entered by the admin
*/
function process_config($config) {

    $return = true;

    foreach ($config as $name => $value) {
        if (!set_config($name, $value)) {
            $return = false;
        }
    }

    return $return;
}



/**
* Returns the relevant icons for a course
*
* @param    course  the current course, as an object
*/
function get_access_icons($course) {
    global $CFG;

    global $strallowguests;
    global $strrequireskey;

    if (empty($strallowguests)) {
        $strallowguests = get_string('allowguests');
        $strrequireskey = get_string('requireskey');
    }

    $str = '';

    if (!empty($course->guest)) {
        $str .= '<a title="'.$strallowguests.'" href="'.$CFG->wwwroot.'/course/view.php?id='.$course->id.'">';
        $str .= '<img class="accessicon" alt="'.$strallowguests.'" src="'.$CFG->pixpath.'/i/guest.gif" /></a>&nbsp;&nbsp;';
    }
    if (!empty($course->password)) {
        $str .= '<a title="'.$strrequireskey.'" href="'.$CFG->wwwroot.'/course/view.php?id='.$course->id.'">';
        $str .= '<img class="accessicon" alt="'.$strrequireskey.'" src="'.$CFG->pixpath.'/i/key.gif" /></a>';
    }

    return $str;
}

/**
 * Prints the message telling you were to get the enrolment key
 * appropriate for the prevailing circumstances
 * A bit clunky because I didn't want to change the standard strings
 */
function print_enrolmentkeyfrom($course) {
    global $CFG;
    global $USER;

    $context = get_context_instance(CONTEXT_SYSTEM);
    $guest = has_capability('moodle/legacy:guest', $context, $USER->id, false);

    // if a keyholder role is defined we list teachers in that role (if any exist)
    $contactslisted = false;
    $canseehidden = has_capability('moodle/role:viewhiddenassigns', $context);
    if (!empty($CFG->enrol_approvalworkflow_keyholderrole)) {
        if ($contacts = get_role_users($CFG->enrol_approvalworkflow_keyholderrole, get_context_instance(CONTEXT_COURSE, $course->id),true,'','u.lastname ASC',$canseehidden  )) {
            // guest user has a slightly different message
            if ($guest) {
                print_string('enrolmentkeyfromguest', '', ':<br />' );
            }
            else {
                print_string('enrolmentkeyfrom', '', ':<br />');
            }
            foreach ($contacts as $contact) {
                $contactname = "<a href=\"../user/view.php?id=$contact->id&course=".SITEID."\">".fullname($contact)."</a>.";
                echo "$contactname<br />";
            }
            $contactslisted = true;
        }
    }

    // if no keyholder role is defined OR nobody is in that role we do this the 'old' way
    // (show the first person with update rights)
    if (!$contactslisted) {
        if ($teachers = get_users_by_capability(get_context_instance(CONTEXT_COURSE, $course->id), 'moodle/course:update',
            'u.*', 'u.id ASC', 0, 1, '', '', false, true)) {
            $teacher = array_shift($teachers);
        }
        if (!empty($teacher)) {
            $teachername = "<a href=\"../user/view.php?id=$teacher->id&course=".SITEID."\">".fullname($teacher)."</a>.";
        } else {
            $teachername = strtolower( get_string('defaultcourseteacher') ); //get_string('yourteacher', '', $course->teacher);
        }

        // guest user has a slightly different message
        if ($guest) {
            print_string('enrolmentkeyfromguest', '', $teachername );
        }
        else 
		{
            print_string('enrolmentkeyfrom', '', $teachername);
        }
    }
}

} /// end of class

?>
