<?PHP 

$string['enrolname'] = 'Skillsoft Approval Workflow Enrolment';
$string['description'] = 'Skillsoft Approval Workflow enrolment:
<ol>
<li>The employee applies for enrolment.</li>
<li>The employee receives an email confirmin g the application.</li>
<li>The Skillsoft Admin/Manager receives a mail with the application data.</li>
<li>The Skillsoft Admin/Manager approves or denies the enrolment application.</li>
<li>The employee receives an activation or denial email.</li>
</ul>';
$string['enrolmentrequest'] = "<p>This course is offered through Skillsoft.  You must be at least a Grade 7 employee to request Skillport Access.  Management approval is required before access can be granted to this course.  Once you have a Skillsoft license, you may take any of the courses in the system.</p>  <p>Please specify the reason for your request</p>";
$string['usenrolmentrequest'] = "<p>This course is offered through Skillsoft.  Management approval is required before access can be granted to this course.  Once you have a Skillsoft license, you may take any of the courses in the system.</p>  <p>Please specify the reason for your request</p>";
$string['enrol_Approval Workflow_moderatoremail'] = 'Email of the SkillSoft admin where the application is send to. Do not leave this field empty.';
$string['application'] = '</p>Your enrollment request will be reviewed as soon as possible by your manager.  Once your manager responds to the request, it will be submitted to the Skillport Admin queue for review. Please note that the process may take up to 5 business days for review, approval, and upload procedures.</p>';

$string['applicationsubject'] = 'Applications for enrolment in SkillSoft courses';


$string['reason'] = 'Reason';
$string['aprove'] = 'Aprove';
$string['reject'] = 'Reject';
$string['applicationsenrolment'] = 'Applications for enrolment';
$string['showapplications'] = 'Show applications';

?>
