<?PHP 

$string['enrolname'] = 'User Registration Workflow Enrolment';
$string['description'] = 'User Registration Workflow Enrolment Workflow enrolment:
<ol>
<li>The employee applies for enrolment.</li>
<li>The employee receives an email confirmin g the application.</li>
<li>The Course Helpdesk/Manager receives a mail with the application data.</li>
<li>The Course Helpdesk/Manager approves or denies the enrolment application.</li>
<li>The employee receives an activation or denial email.</li>
</ul>';

$string['enrol_Approval Workflow_moderatoremail'] = 'Email of the Course admin where the application is send to. Do not leave this field empty.';


$string['applicationsubject'] = 'Applications for enrolment in Course/Program';


$string['reason'] = 'Reason';
$string['aprove'] = 'Aprove';
$string['reject'] = 'Reject';
$string['applicationsenrolment'] = 'Applications for enrolment';
$string['showapplications'] = 'Show applications';
$string['applicationaproved']= 'Application approved';
$string['applicationrejected']= 'Application rejected';

?>
