
<?php

    require("../../config.php");
	require_login();

    // Allow access only to admin
	
    $rowusername=get_record('user','id',$USER->id);
	$contentedit=get_records_sql("SELECT c.id FROM mdl_course c  JOIN mdl_context cx ON c.id = cx.instanceid JOIN mdl_role_assignments ra ON cx.id = ra.contextid AND ra.roleid = '14' and ra.userid=$USER->id");

	if($contentedit)
	{
	$sql = "SELECT em.*, u.id AS uid, u.firstname, u.lastname, u.username,u.grade, c.shortname, em.coursegroup
            FROM mdl_enrol_userregister em
            LEFT OUTER JOIN mdl_user u ON u.id = em.userid
            LEFT OUTER JOIN mdl_course c ON c.id = em.courseid
		    WHERE status = 0 and em.courseid in (SELECT c.id
FROM mdl_course c  JOIN mdl_context cx ON c.id = cx.instanceid
 JOIN mdl_role_assignments ra ON cx.id = ra.contextid AND ra.roleid = '14' and ra.userid=$USER->id)";
	}
	else
	{
	$sql = "SELECT em.*, u.id AS uid, u.firstname, u.lastname, u.username,u.grade, c.shortname, em.coursegroup,em.created
            FROM {$CFG->prefix}enrol_userregister em
            LEFT OUTER JOIN {$CFG->prefix}user u ON u.id = em.userid
            LEFT OUTER JOIN {$CFG->prefix}course c ON c.id = em.courseid
		    WHERE status = 0 and u.manager_portalid=$rowusername->username
            ORDER BY em.created";
	}
    
    $navlinks = array();
    $navlinks[] = array('name' => 'Enrolment Requests', 'link' => ".", 'type' => 'misc');
    $navigation = build_navigation($navlinks);

    $rows = get_records_sql($sql);
    $output = '<table class="generaltable boxaligncenter" width="100%" cellspacing="1" cellpadding="5">';
    $output .= '<tr>
                <th align="left">PortalID</th>
				<th align="left">Employee Name</th>
				<th align="left">Grade</th>              				
				<th align="left">Course</th>
				<th align="left">Group</th>
				<th align="left">Request Staus</th>
                <th align="left">Requested Date</th>
                <th align="left">Action</th>
                </tr>';
    
    if(!empty($rows)){
        foreach ($rows as $row){
			$group_name=groups_get_group_name($row->coursegroup);
            $url = $CFG->wwwroot.'/enrol/userregister/process_request.php?cid='.$row->courseid.'&uid='.$row->userid.'&gid='.$row->coursegroup;
        	$output .= '<tr>';
			$output .= "<td>$row->username</td>";
            $output .= "<td>$row->firstname $row->lastname</td>";
			$output .= "<td>$row->grade</td>";      
			$output .= "<td>$row->shortname</td>";
			$output .= "<td>$group_name</td>";
			if($row->status==0)
			{
			$output .= "<td>Open</td>";
			}
			else if($row->status==1)
			{
            $output .= "<td>Approved</td>";
			}
			$output .= "<td>".userdate($row->created, '%d %b %y')."</td>";

            $output .= "<td><a href='{$url}&action=approve'>".get_string('aprove','enrol_userregister')."</a> ";
			$output .= "&nbsp;&nbsp;<a href='{$url}&action=reject'>".get_string('reject','enrol_userregister')."</a></td>";

          
            $output .= '</tr>';    
    	}
    }    
    $output .= '</table>';
    
    print_header(get_string('applicationsenrolment','enrol_userregister'),get_string('applicationsenrolment','enrol_userregister'), $navigation);
echo "<p>Your employee(s) below are requesting access to courses/programs on the LMS.  Please review and approve or reject the access request."; 
 
 if($rows)
 {
 print_box($output);
 }
 else
 {
 error('Currently you have no userregister enrollment request to approve/reject.');
 }
   
    print_footer();
?>