<?php
// $Id: inscriptions_massives.php 356 2010-02-27 13:15:34Z ppollet $
/**
 * A bulk enrolment plugin that allow teachers to massively enrol existing accounts to their courses,
 * with an option of adding every user to a group.
 * courtesy of Patrick POLLET & Valery FREMAUX  France, February 2010
*/

require ('../../../../config.php');
require_once ($CFG->dirroot . '/local/course/admin/mass_enroll/mass_enroll_form.php');

// $locallangroot = $CFG->dirroot.'/local/course/admin/mass_enroll/lang/';
// since the help file MUST be copied to moodledata/lang/xx_utf8, let's also require
// that the translation file be there

$locallangroot = '';

/// Get params

$id = required_param('id', PARAM_INT);

if (!$course = get_record('course', 'id', $id)) {
	error("Course is misconfigured");
}

/// Security and access check

require_login($course);

$context = get_context_instance(CONTEXT_COURSE, $course->id);
require_capability('moodle/course:update', $context);

/// Start making page

$strinscriptions = get_string('mass_enroll', 'mass_enroll', '', $locallangroot);

$navlinks = array (
	array (
		'name' => $strinscriptions,
		'link' => null,
		'type' => 'misc'
	)
);
$navigation = build_navigation($navlinks);

// a directory within moodleldata (required to be there !)
$destination_directory = 'tmp';

$mform = new mass_enroll_form($CFG->wwwroot . '/local/course/admin/mass_enroll/mass_enroll.php', array (
	'course' => $course,
	'context' => $context
));

if ($mform->is_cancelled()) {
	redirect($CFG->wwwroot . '/course/view.php?id=' . $id);
} else
	if ($data = $mform->get_data(false)) { // no magic quotes

		require_once ($CFG->dirroot . '/group/lib.php');
		print_header($course->fullname . ': ' . $strinscriptions, $course->fullname . ': ' . $strinscriptions, $navigation);
		print_heading($strinscriptions);

		$mform->save_files($destination_directory);
		$newfilename = $mform->get_new_filename();

		$result = mass_enroll($CFG->dataroot . '/' . $destination_directory . '/' . $newfilename, $id, $context, $data);

		if ($data->mailreport) {
			$a = new StdClass();
			$a->course = $course->fullname;
			$a->report = $result;
			email_to_user($USER, $USER, get_string('mail_enrolment_subject', 'mass_enroll', $CFG->wwwroot, $locallangroot), get_string('mail_enrolment', 'mass_enroll', $a, $locallangroot));
			$result .= "\n" . get_string('email_sent', 'mass_enroll', $USER->email, $locallangroot);
		}

		print_simple_box(nl2br($result), 'center', '90%');
		print_continue($CFG->wwwroot . '/course/view.php?id=' . $course->id); // Back to course page
		print_footer($course);
		exit;
	}

print_header($course->fullname . ': ' . $strinscriptions, $course->fullname . ': ' . $strinscriptions, $navigation);

$icon = '<img class="icon" src="' . $CFG->pixpath . '/i/admin.gif" alt="' . get_string('mass_enroll', 'mass_enroll', '', $locallangroot) . '"/>';
print_heading_with_help($strinscriptions, 'mass_enroll', 'mass_enroll', $icon);
print_simple_box(get_string('mass_enroll_info', 'mass_enroll', '', $locallangroot), 'center', '90%');

$mform->display();
print_footer($course);
exit;

/**
* realizes the mass enrolment
*/
function mass_enroll($file, $courseid, $context, $data) {
	global $CFG, $locallangroot;

	$result = get_string('im:opening_file', 'mass_enroll', $file, $locallangroot) . "\n";

	$roleid = $data->roleassign;
	$useridfield = $data->firstcolumn;

	$enrollablecount = 0;
	$createdgroupscount = 0;
	$createdgroupingscount = 0;
	$createdgroups = '';
	$createdgroupings = '';

	$handle = fopen($file, 'rb');
	if ($handle) {
		$contents = fread($handle, filesize($file));
		// caution with files produced on Macs (only \r)
		$contents = preg_replace('/\r\n|\r/', "\n", $contents);
		$lines = explode("\n", $contents);

		foreach ($lines as $line) {

			$a = new StdClass();

			// convert separators ' or \t to ;
			$line = preg_replace('/\t|,/', ';', $line);
			// get rid on eventual double quotes
			$line = str_replace('"', '', $line);
			//and leading, ending spaces
			$line = trim($line);
			//
			if (empty ($line))
				continue;
			$fields = explode(';', $line);
			// Naga added to trigger automated mails after bulk enrollment
		$erge=get_record_sql("select fullname,sendmessage,enrolmentmessage from mdl_course where id=$courseid");
					
					
		if($roleid==5 && $erge->sendmessage==1)
		  {
		 
			foreach ($fields as $user->id) {
			
			$addusers =get_record_sql("select id,firstname,lastname,username from mdl_user where username=$user->id");
			 if(!user_has_role_assignment($addusers->id, $roleid, $context->id)){
			 
				   $subject1='You are enrolled into the';
				   $subcourse='training course';
				   $sub=$erge->fullname;
				   if($courseid==8403){
				   $subject='You are enrolled into the Code of Business Conduct training course';
				   $dear='<p class="MsoNormal">Global Code of Business Conduct| Mandatory Training</p> <p class="MsoNormal" style="margin-top:10px;">Dear';
				   $name=$addusers->firstname.'  '.$addusers->lastname.' PortalID '.$addusers->username.",</p>";
				   }else{
				   $subject= $subject1.'   '.$sub.' '. $subcourse;
				   $dear='Dear';
				   $name=$addusers->firstname.'  '.$addusers->lastname.' PortalID '.$addusers->username.",";
				   }
				   // $subject= $subject1.' '.$sub.' '.$subcourse;
				   
				   $message=str_replace("<p>","<p class='MsoNormal'>",$erge->enrolmentmessage);
					 //srinu added code for email body with images
				   $doc = new DOMDocument();
				   libxml_use_internal_errors(true);
				   $doc->loadHTML( $message );
				   $xpath = new DOMXPath($doc);
				   $imgs = $xpath->query("//img");
				   $attachment="";
				   $attachname="";
				   
				   for ($i=0; $i < $imgs->length; $i++) {
				   $img = $imgs->item($i);
				   $src = $img->getAttribute("src");
				   
				   
				   if($src !="")
					{
					$expst=array();
				    $expst=explode("/",$src);
				    $strss="cid:image".$i;
				    $cids="image".$i;
				    $message=str_replace($src,$strss,$message);
					$attachment.=$expst[4].'\\'.$expst[5].'|D|'.$cids.'|M|';
					$attachname.=$expst[5].'|M|';
					}
				   
				   }
					 //end code by srinu
				  //$erol=$dear.'    '.$name.',                                                                           '.$message;
				  $erolmessage= '<html xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:w="urn:schemas-microsoft-com:office:word" xmlns:m="http://schemas.microsoft.com/office/2004/12/omml" xmlns="http://www.w3.org/TR/REC-html40"><head><META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=us-ascii"><meta name=Generator content="Microsoft Word 14 (filtered medium)"><style><!--
/* Font Definitions */
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
/* Style Definitions */
p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin:0in;
	margin-bottom:.0001pt;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";}
a:link, span.MsoHyperlink
	{mso-style-priority:99;
	color:blue;
	text-decoration:underline;}
a:visited, span.MsoHyperlinkFollowed
	{mso-style-priority:99;
	color:purple;
	text-decoration:underline;}
span.EmailStyle17
	{mso-style-type:personal-compose;
	font-family:"Calibri","sans-serif";
	color:windowtext;}
.MsoChpDefault
	{mso-style-type:export-only;
	font-family:"Calibri","sans-serif";}
	font-family:"Calibri","sans-serif";}
@page WordSection1
	{size:8.5in 11.0in;
	margin:1.0in 1.0in 1.0in 1.0in;}
div.WordSection1
	{page:WordSection1;}
--></style><!--[if gte mso 9]><xml>

<o:shapedefaults v:ext="edit" spidmax="1026" />
</xml><![endif]--><!--[if gte mso 9]><xml>
<o:shapelayout v:ext="edit">
<o:idmap v:ext="edit" data="1" />
</o:shapelayout></xml><![endif]--></head><body lang=EN-US link=blue vlink=purple><div class=WordSection1><p class=MsoNormal>'.$dear.'    '.$name.'                                                                           '.$message.'<o:p></o:p></p></div></body></html>';



		  $touser = get_record('user','id',$addusers->id);  // to give touser mail address
		  $USER1  = get_record('user','id','1');
		 //srinu added code for email with images
				if($attachment !="")
				{
				email_to_user_enrol( $touser,$USER1,trim($subject), $erolmessage,$erolmessage,$attachment,$attachname);
				}
				else
				{
				email_to_user( $touser,$USER1,trim($subject), $erolmessage,$erolmessage);
				}
		 //end code by srinu
				//email_to_user( $touser,$USER1,$subject, $erolmessage,$erolmessage);
				}
				
		 }
			}
			// completed to trigger automated mails after bulk enrollment
			
			// 1rst column = id Moodle (idnumber,username or email)
			if (!$user = get_record('user', $useridfield, trim($fields[0]))) {
				$result .= get_string('im:user_unknown', 'mass_enroll', $fields[0], $locallangroot) . "\n";
				continue;
			}
			//already enroled ?
			if (user_has_role_assignment($user->id, $roleid, $context->id)) {
				$result .= get_string('im:already_in', 'mass_enroll', fullname($user), $locallangroot);

			} else {
				if (!role_assign($roleid, $user->id, null, $context->id, 0, 0, 0, 'flatfile')) {
					$result .= get_string('im:error_in', 'mass_enroll', fullname($user), $locallangroot) . "\n";
					continue;
				}
				$result .= get_string('im:enrolled_ok', 'mass_enroll', fullname($user), $locallangroot);
				$enrollablecount++;
			}

			$group = trim($fields[1]);
			// 2nd column ?
			if (empty ($group)) {
				$result .= "\n";
				continue; // no group for this one
			}

			// create group if needed
			if (!($gid = mass_enroll_group_exists($group, $courseid))) {
				if ($data->creategroups) {
					if (!($gid = mass_enroll_add_group($group, $courseid))) {
						$a->group = $group;
						$a->courseid = $courseid;
						$result .= get_string('im:error_addg', 'mass_enroll', $a, $locallangroot) . "\n";
						continue;
					}
					$createdgroupscount++;
					$createdgroups .= " $group";
				} else {
					$result .= get_string('im:error_g_unknown', 'mass_enroll', $group, $locallangroot) . "\n";
					continue;
				}
			}

			// if groupings are enabled on the site (should be ?)
			if ($CFG->enablegroupings) {
				if (!($gpid = mass_enroll_grouping_exists($group, $courseid))) {
					if ($data->creategroupings) {
						if (!($gpid = mass_enroll_add_grouping($group, $courseid))) {
							$a->group = $group;
							$a->courseid = $courseid;
							$result .= get_string('im:error_add_grp', 'mass_enroll', $a, $locallangroot) . "\n";
							continue;
						}
						$createdgroupingscount++;
						$createdgroupings .= " $group";
					} else {
						// don't complains,
						// just do the enrolment to group
					}
				}
				// if grouping existed or has just been created
				if ($gpid && !(mass_enroll_group_in_grouping($gid, $gpid))) {
					if (!(mass_enroll_add_group_grouping($gid, $gpid))) {
						$a->group = $group;
						$result .= get_string('im:error_add_g_grp', 'mass_enroll', $a, $locallangroot) . "\n";
						continue;
					}
				}
			}

			// finally add to group if needed
			if (!groups_is_member($gid, $user->id)) {
				$ok = groups_add_member($gid, $user->id);
				if ($ok) {
					$result .= get_string('im:and_added_g', 'mass_enroll', $group, $locallangroot) . "\n";
				} else {
					$result .= get_string('im:error_adding_u_g', 'mass_enroll', $group, $locallangroot) . "\n";
				}
			} else {
				$result .= get_string('im:already_in_g', 'mass_enroll', $group, $locallangroot) . "\n";
			}

		}
		fclose($handle);
		unlink($file); //  clean up moodledata tmp area
		//recap final
		$result .= get_string('im:stats_i', 'mass_enroll', $enrollablecount, $locallangroot) . "\n";
		$a->nb = $createdgroupscount;
		$a->what = $createdgroups;
		$result .= get_string('im:stats_g', 'mass_enroll', $a, $locallangroot) . "\n";
		$a->nb = $createdgroupingscount;
		$a->what = $createdgroupings;
		$result .= get_string('im:stats_grp', 'mass_enroll', $a, $locallangroot) . "\n";
	} else {
		$result .= get_string('im:err_opening_file', 'mass_enroll', $file, $locallangroot) . "\n";
	}
	return $result;
}

/**
*
*
*/
function mass_enroll_add_group($newgroupname, $courseid) {
	$newgroup->name = $newgroupname;
	$newgroup->courseid = $courseid;
	$newgroup->lang = current_language();
	return groups_create_group($newgroup);
}

/**
*
*
*/
function mass_enroll_add_grouping($newgroupingname, $courseid) {
	$newgrouping->name = $newgroupingname;
	$newgrouping->courseid = $courseid;
	return groups_create_grouping($newgrouping);
}

/**
* @param string $name group name
* @param int $courseid course
*/
function mass_enroll_group_exists($name, $courseid) {
	return groups_get_group_by_name($courseid, $name);
}

/**
* @param string $name group name
* @param int $courseid course
*/
function mass_enroll_grouping_exists($name, $courseid) {
	return groups_get_grouping_by_name($courseid, $name);

}

/**
* @param int $gid group ID
* @param int $gpid grouping ID
*/
function mass_enroll_group_in_grouping($gid, $gpid) {
	global $CFG;
	$sql =<<<EOF
   select * from {$CFG->prefix}groupings_groups
   where groupingid = $gpid
   and groupid = $gid
EOF;
	return get_record_sql($sql);
}

/**
* @param int $gid group ID
* @param int $gpid grouping ID
*/
function mass_enroll_add_group_grouping($gid, $gpid) {
	$new->groupid = $gid;
	$new->groupingid = $gpid;
	$new->timeadded = time();
	return insert_record('groupings_groups', $new);
}
?>
