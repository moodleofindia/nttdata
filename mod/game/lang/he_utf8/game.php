<?php // $Id: game.php,v 1.3.2.10 2011/08/27 19:40:29 bdaloukas Exp $
      //Hebrew translation by Nadav Kavalerchik 

//bookquiz/importodt.php
$string[ 'bookquiz_import_odt'] = 'יבוא מקובץ אופן אופיס (odt)';
$string[ 'bookquiz_not_select_book'] = 'לא בחרתם ספר';
$string[ 'bookquiz_subchapter'] = 'יצירת תת פרקים';

//bookquiz/play.php
$string[ 'bookquiz_empty'] = 'הספר ריק';

//bookquiz/questions.php
$string[ 'bookquiz_categories'] = 'סיווגים';
$string[ 'bookquiz_chapters'] = 'פרקים';
$string[ 'bookquiz_questions'] = 'קישור סיווג שאלות לפרק בספר';

//cross/cross_class.php
$string[ 'lettersall'] = 'אבגדהוזחטיכלמנסעפצקרשתךףץןם ';

//cross/crossdb_class.php
$string[ 'and'] = 'וגם';
$string[ 'cross_correct'] = 'נכון';
$string[ 'cross_corrects'] = 'נכונים';
$string[ 'cross_error'] = 'טעות';
$string[ 'cross_errors'] = 'טעויות';
$string[ 'cross_found_many'] = 'מספר התאמות';
$string[ 'cross_found_one'] = 'התאמה בודדת';
$string[ 'grade'] = 'ציון';

//cross/play.php
$string[ 'cross_across'] = 'מאוזן';
$string[ 'cross_checkbutton'] = 'בדיקת תשבץ';
$string[ 'cross_down'] = 'מאונך';
$string[ 'cross_endofgamebutton'] = 'סיום משחק התשבץ';
$string[ 'cross_error_containsbadchars'] = 'המילה מכילה תווים לא תקינים';
$string[ 'cross_error_wordlength1'] = 'המילה הנכונה מכילה ';
$string[ 'cross_error_wordlength2'] = ' אותיות.';
$string[ 'cross_pleasewait'] = 'אנא המתינו בזמן שהתשבץ נטען';
$string[ 'cross_welcome'] = '<h3>ברוכים הבאים!</h3><p>לחצו על המשבצת הראשונה של המילה בה אתם מעונינים להתחיל.</p>';
$string[ 'finish'] = 'המשחק הסתיים';
$string[ 'letter'] = 'אות';
$string[ 'letters'] = 'אותיות';
$string[ 'nextgame'] = 'משחק חדש';
$string[ 'no_words'] = 'לא נמצאו מילים';
$string[ 'print'] = 'הדפסה';
$string[ 'win'] = 'כל הכבוד !!!';

//db/access.php

//hangman/play.php
$string[ 'hangman_correct_phrase'] = 'המשפט הנכון הוא: ';
$string[ 'hangman_correct_word'] = 'המילה הנכונה היא: ';
$string[ 'hangman_gradeinstance'] = 'הציון הכללי במשחק';
$string[ 'hangman_letters'] = 'אותיות: ';
$string[ 'hangman_restletters_many'] = 'יש לכם <b>$a</b> נסיונות';
$string[ 'hangman_restletters_one'] = 'יש לכם ניסיון <b>אחד</b> בלבד';
$string[ 'hangman_wrongnum'] = 'טעויות: %%d מתוך %%d';
$string[ 'nextword'] = 'מילה חדשה';

//hiddenpicture/play.php
$string[ 'hiddenpicture_mainsubmit'] = 'בדיקת התשובה לשאלה הראשית';
$string[ 'hiddenpicture_nomainquestion'] = 'לא נמצאו מונחים באגרון $a->name אשר צורפו אליהם תמונה. <br/> אנא בחרו ארגון אחר, בו לכל מונח מצורפת תמונה';
$string[ 'must_select_glossary'] = 'יש לבחור אגרון';

//millionaire/play.php
$string[ 'millionaire_must_select_questioncategory'] = 'יש לבחור סיווג שאלות אחד ';
$string[ 'millionaire_must_select_quiz'] = 'יש לבחור מבחן אחד';

//report/overview/report.php
$string[ 'allattempts'] = 'הציגו את כל הניסיונות';
$string[ 'allstudents'] = 'הציגו את כל $a';
$string[ 'attemptsonly'] = 'הציגו רק תלמידים עם ניסיונות';
$string[ 'deleteattemptcheck'] = 'האם אתם בטוחים שאתם מעוניינים למחוק את התשובות המסומנות?';
$string[ 'displayoptions'] = 'תצוגת מאפיינים';
$string[ 'downloadods'] = 'הורידו גליון אלקטרוני בתצורת ODF';
$string[ 'feedback'] = 'משוב';
$string[ 'noattemptsonly'] = 'הציגו $a שטרם ענו, בלבד';
$string[ 'numattempts'] = '$a->studentnum $a->studentstring ביצעו $a->attemptnum נסיונות';
$string[ 'overview'] = 'תצוגה כללית';
$string[ 'pagesize'] = 'מספר השאלות בדף:';
$string[ 'selectall'] = 'בחירה כוללת';
$string[ 'selectnone'] = 'ביטול סימון כולל';
$string[ 'showdetailedmarks'] = 'הציגו פירוט עבור המסומנים';
$string[ 'startedon'] = 'זמן תחילה';
$string[ 'timecompleted'] = 'הושלם';
$string[ 'timetaken'] = 'זמן המשחק';
$string[ 'unfinished'] = 'פתוח';
$string[ 'withselected'] = 'עם התשובות המסומנות';

//snakes/play.php

//sudoku/create.php
$string[ 'sudoku_create_count'] = 'מספר לוחות סודוקו העומדים להיווצר';
$string[ 'sudoku_create_start'] = 'התחלת יצירת לוחות סודוקו';
$string[ 'sudoku_creating'] = 'מייצר <b>$a</b> לוחות סודוקו';

//sudoku/play.php
$string[ 'sudoku_finishattemptbutton'] = 'סיום משחק סודוקו';
$string[ 'sudoku_guessnumber'] = 'נחשו את המספר הנכון';
$string[ 'sudoku_noentriesfound'] = 'לא נמצאו מילים באגרון';

//attempt.php

//backuplib.php
$string[ 'modulenameplural'] = 'משחקים';

//export.php
$string[ 'export'] = 'Export';
$string[ 'html_hascheckbutton'] = 'קיים כפתור אישור:';
$string[ 'html_hasprintbutton'] = 'קיים כפתור הדפסה:';
$string[ 'html_title'] = 'כותרת מעוצבת:';
$string[ 'only_teachers'] = 'רק מורה יכול לראות דף זה';

//exporthtml_hangman.php
$string[ 'hangman_loose'] = '<BIG><B>המשחק הסתיים</B></BIG>';

//exporthtml_millionaire.php
$string[ 'millionaire_info_people'] = 'אנשים אומרים';
$string[ 'millionaire_info_telephone'] = 'אני חושב/ת שהתשובה הנכונה היא ';
$string[ 'millionaire_info_wrong_answer'] = 'תשובתכם שגוייה<br>התשובה הנכונה היא:';
$string[ 'millionaire_sourcemodule_must_quiz_question'] = 'עבור משחק המיליונר מקור השאלות צריך להיות $a או שאלות ולא';
$string[ 'must_select_questioncategory'] = 'יש לבחור סיווג לשאלות';
$string[ 'must_select_quiz'] = 'יש לבחור תשבץ';

//exporthtml_snakes.php
$string[ 'score'] = 'ניקוד';

//header.php
$string[ 'modulename'] = 'משחק';

//index.php
$string[ 'attempts'] = 'נסיונות';

//lib.php
$string[ 'attempt'] = 'ניסיון $a';
$string[ 'game_bookquiz'] = 'ספר עם שאלות';
$string[ 'game_cross'] = 'תשבץ';
$string[ 'game_hangman'] = 'הצילו את האיש התלוי';
$string[ 'game_hiddenpicture'] = 'תמונה נסתרת';
$string[ 'game_millionaire'] = 'מי רוצה להיות מיליונר';
$string[ 'game_snakes'] = 'סולמות ונחשים';
$string[ 'game_sudoku'] = 'סודוקו';
$string[ 'noattempts'] = 'לא נעשו נסיונות לפתור תשבץ זה';
$string[ 'percent'] = 'אחוז';

//locallib.php
$string[ 'attemptfirst'] = 'ניסיון ראשון';
$string[ 'attemptlast'] = 'ניסיון אחרון';
$string[ 'gradeaverage'] = 'ציון ממוצע';
$string[ 'gradehighest'] = 'הציון הגובה ביותר';

//mod.html
$string[ 'bottomtext'] = 'המלל בתחתית';
$string[ 'cross_layout'] = 'מבנה תצורה';
$string[ 'cross_layout0'] = 'המשפט יופיע תחת התשבץ';
$string[ 'cross_layout1'] = 'המשפט יופיע מימין לתשבץ';
$string[ 'cross_maxcols'] = 'מספר עמודות מירבי בתשבץ';
$string[ 'cross_maxwords'] = 'מספר מילים מירבי בתשבץ';
$string[ 'cryptex_maxcols'] = 'מספר העמודות/שורות במשחק cryptex';
$string[ 'cryptex_maxwords'] = 'מספר המילים במשחק cryptex';
$string[ 'grademethod'] = 'שיטת מתן ציונים';
$string[ 'hangman_allowspaces'] = 'אפשרות לרווחים במילים';
$string[ 'hangman_allowsub'] = 'אפשרות לתו - במילים';
$string[ 'hangman_imageset'] = 'בחרו תמונה למשחק האיש התלוי';
$string[ 'hangman_language'] = 'שפת המילים';
$string[ 'hangman_maxtries'] = 'מספר מילים בכל משחק';
$string[ 'hangman_showcorrectanswer'] = 'הצגת התשובה הנכונה בסוף כל משחק';
$string[ 'hangman_showfirst'] = 'הצגת האות הראשונה במשחק האיש התלוי';
$string[ 'hangman_showlast'] = 'הצגת האות האחרונה במשחק האיש התלוי';
$string[ 'hangman_showquestion'] = 'להציג את השאלות ?';
$string[ 'hiddenpicture_across'] = 'תאים אופקיים';
$string[ 'hiddenpicture_down'] = 'תאים אנחיים';
$string[ 'hiddenpicture_height'] = 'קבעו גובה תמונה ל';
$string[ 'hiddenpicture_pictureglossary'] = 'האגרון לשאלה הראשית ולתמונות';
$string[ 'hiddenpicture_pictureglossarycategories'] = 'הסיווג באגרון של השאלה הראשית';
$string[ 'hiddenpicture_width'] = 'קבעו רוחב תמונה ל';
$string[ 'snakes_background'] = 'רקע';
$string[ 'sourcemodule'] = 'מקור השאלות';
$string[ 'sourcemodule_book'] = 'בחרו ספר';
$string[ 'sourcemodule_glossary'] = 'בחרו אגרון';
$string[ 'sourcemodule_glossarycategory'] = 'בחרו סיווג עבור אגרון';
$string[ 'sourcemodule_question'] = 'שאלות';
$string[ 'sourcemodule_questioncategory'] = 'בחרו סיווג לשאלות';
$string[ 'sourcemodule_quiz'] = 'בחרו מבחן';
$string[ 'sudoku_maxquestions'] = 'מספר שאלות מירבי';

//mod_form.php

//preview.php
$string[ 'formatdatetime'] = '%%d %%b %%Y, %%I:%%M %%p';
$string[ 'lastip'] = 'כתובת ה IP של התלמיד';
$string[ 'preview'] = 'תצוגה מקדימה';
$string[ 'showsolution'] = 'פתרון';
$string[ 'timefinish'] = 'המשחק הסתיים';
$string[ 'timelastattempt'] = 'ניסיון אחרון';
$string[ 'timestart'] = 'התחלה';

//review.php
$string[ 'completedon'] = 'הסתיים בתאריך';
$string[ 'outof'] = '$a->grade מתוך הציון המירבי : $a->maxgrade';
$string[ 'review'] = 'בדיקה';
$string[ 'reviewofattempt'] = 'בדיקת ניסיון $a';
$string[ 'startagain'] = 'התחילו משחק זה מההתחלה';

//showanswers.php
$string[ 'feedbacks'] = 'ההודעה במצב של תשובה נכונה';

//tabs.php
$string[ 'info'] = 'מידע';
$string[ 'results'] = 'תוצאות';
$string[ 'showanswers'] = 'הצגת תשובות';

//view.php
$string[ 'attemptgamenow'] = 'שחקו עכשיו';
$string[ 'continueattemptgame'] = 'המשיכו ניסיון משחק קודם שלכם';
$string[ 'gradesofar'] = '$a->method: $a->mygrade / $a->gamegrade.';
$string[ 'marks'] = 'ציונים';
$string[ 'nomoreattempts'] = 'אין אפשרות לנסיונות נוספים';
$string[ 'reattemptgame'] = 'שחקו שוב';
$string[ 'yourfinalgradeis'] = 'הציון הסופי שלכם למשחק זה הוא $a.';

//Untranslated
//bookquiz/play.php $string[ 'sudoku_submit'] = 'Grade answers';
//db/access.php $string[ 'game:attempt'] = 'Play games';
//db/access.php $string[ 'game:deleteattempts'] = 'Delete attempts';
//db/access.php $string[ 'game:grade'] = 'Grade games manually';
//db/access.php $string[ 'game:manage'] = 'Manage';
//db/access.php $string[ 'game:preview'] = 'Preview Games';
//db/access.php $string[ 'game:reviewmyattempts'] = 'reviewmyattempts';
//db/access.php $string[ 'game:view'] = 'View game information';
//db/access.php $string[ 'game:viewreports'] = 'View game reports';
//hiddenpicture/play.php $string[ 'hiddenpicture_nocols'] = 'Please specify the number of horizontal columns';
//hiddenpicture/play.php $string[ 'hiddenpicture_norows'] = 'Please specify the number of vertical columns';
//hiddenpicture/play.php $string[ 'no_questions'] = "There are no questions";
//hiddenpicture/play.php $string[ 'noglossaryentriesfound'] = 'No glossary entries found';
//snakes/play.php $string[ 'snakes_dice'] = 'Dice, $a spots.';
//snakes/play.php $string[ 'snakes_player'] = 'Player, position: $a.';
//attempt.php $string[ 'gameclosed'] = 'This game closed on {$a}';
//attempt.php $string[ 'gamenotavailable'] = 'The game will not be available until {$a}';
//export.php $string[ 'javame_createdby'] = 'Created by:';
//export.php $string[ 'javame_description'] = 'Description:';
//export.php $string[ 'javame_filename'] = 'Filename:';
//export.php $string[ 'javame_icon'] = 'Icon:';
//export.php $string[ 'javame_maxpictureheight'] = 'Max picture height:';
//export.php $string[ 'javame_maxpicturewidth'] = 'Max picture width:';
//export.php $string[ 'javame_name'] = 'Name:';
//export.php $string[ 'javame_type'] = 'Type:';
//export.php $string[ 'javame_vendor'] = 'Vendor:';
//export.php $string[ 'javame_version'] = 'Version:';
//exporthtml_hangman.php $string[ 'html_hangman_new'] = 'New';
//exporthtml_millionaire.php $string[ 'millionaire_helppeople'] = 'Ask the audience';
//exporthtml_millionaire.php $string[ 'millionaire_quit'] = 'Quit';
//exporthtml_millionaire.php $string[ 'millionaire_telephone'] = 'Call a friend';
//exporthtml_snakes.php $string[ 'html_snakes_check'] = 'Check';
//exporthtml_snakes.php $string[ 'html_snakes_correct'] = 'Correct!';
//exporthtml_snakes.php $string[ 'html_snakes_no_selection'] = 'Have to select something!';
//exporthtml_snakes.php $string[ 'html_snakes_wrong'] = "Your answer isn't correct. Stay on the same seat.";
//lib.php $string[ 'game_cryptex'] = 'Cryptex';
//lib.php $string[ 'reset_game_all'] = 'Delete tries from all games';
//lib.php $string[ 'reset_game_deleted_course'] = 'Delete tries from deleted courses';
//mod.html $string[ 'cryptex_maxtries'] = 'Maximum tries';
//mod.html $string[ 'millionaire_background'] = 'Background color';
//mod.html $string[ 'millionaire_shuffle'] = 'Randomize questions';
//mod.html $string[ 'selectgame'] = 'Select game';
//mod.html $string[ 'sourcemodule_include_subcategories'] = 'Include subcategories';
//mod.html $string[ 'toptext'] = 'Text at the top';
//mod_form.php $string[ 'cross_options'] = 'Crossword options';
//mod_form.php $string[ 'cryptex_options'] = 'Cryptex options';
//mod_form.php $string[ 'gameclose'] = 'Close the game';
//mod_form.php $string[ 'gameopen'] = 'Open the game';
//mod_form.php $string[ 'hangman_maximum_number_of_errors'] = 'Maximum number or errors (have to be images named hangman_0.jpg, hangman_1.jpg, ...)';
//mod_form.php $string[ 'hangman_options'] = 'Hangman options';
//mod_form.php $string[ 'headerfooter_options'] = 'Header/Footer options';
//mod_form.php $string[ 'hiddenpicture_options'] = '\'Hidden Picture\' options';
//mod_form.php $string[ 'millionaire_options'] = 'Millionaire options';
//mod_form.php $string[ 'snakes_board'] = 'Board';
//mod_form.php $string[ 'snakes_cols'] = 'Cols';
//mod_form.php $string[ 'snakes_file'] = 'File for background';
//mod_form.php $string[ 'snakes_footerx'] = 'Space at bootom left';
//mod_form.php $string[ 'snakes_footery'] = 'Space at bottom right';
//mod_form.php $string[ 'snakes_headerx'] = 'Space at up left';
//mod_form.php $string[ 'snakes_headery'] = 'Space at up right';
//mod_form.php $string[ 'snakes_options'] = '\'Snakes and Ladders\' options';
//mod_form.php $string[ 'snakes_rows'] = 'Rows';
//mod_form.php $string[ 'sudoku_options'] = 'Sudoku options';
//mod_form.php $string[ 'userdefined'] = "User defined ";
//showanswers.php $string[ 'clearrepetitions'] = 'Clear statistics';
//showanswers.php $string[ 'computerepetitions'] = 'Compute statistics again';
//showanswers.php $string[ 'repetitions'] = 'Repetitions';
//tabs.php $string[ 'export_to_html'] = 'Export to HTML';
//tabs.php $string[ 'export_to_javame'] = 'Export to Javame';
//tabs.php $string[ 'showattempts'] = 'Show attempts';
//view.php $string[ 'gamecloses'] = 'Game closes';
//view.php $string[ 'gameopens'] = 'Game opens';
