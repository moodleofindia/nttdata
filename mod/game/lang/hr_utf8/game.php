<?php // $Id: game.php,v 1.3.2.10 2011/08/27 19:40:30 bdaloukas Exp $
      // translated to Hrvatski by Valentin Vidić

//bookquiz/importodt.php
$string[ 'bookquiz_import_odt'] = 'Uvezi iz OpenOffice datoteke (odt)';
$string[ 'bookquiz_not_select_book'] = 'Niste odabrali knjigu';
$string[ 'bookquiz_subchapter'] = 'Napravi podpoglavlje';

//bookquiz/play.php
$string[ 'bookquiz_empty'] = 'Knjiga je prazna';
$string[ 'sudoku_submit'] = 'Ocjeni odgovore';

//bookquiz/questions.php
$string[ 'bookquiz_categories'] = 'Kategorije';
$string[ 'bookquiz_chapters'] = 'Poglavlja';
$string[ 'bookquiz_questions'] = 'Pridruži kategorije pitanja poglavlju knjige';

//cross/cross_class.php
$string[ 'lettersall'] = 'ABCČĆDĐEFGHIJKLMNOPRSŠTUVZŽ';

//cross/crossdb_class.php
$string[ 'and'] = 'i';
$string[ 'cross_correct'] = 'ispravna';
$string[ 'cross_corrects'] = 'ispravnih';
$string[ 'cross_error'] = 'greška';
$string[ 'cross_errors'] = 'grešaka';
$string[ 'cross_found_many'] = 'Pronađeno';
$string[ 'cross_found_one'] = 'Pronađena';
$string[ 'grade'] = 'Ocjena';

//cross/play.php
$string[ 'cross_across'] = 'Vodoravno';
$string[ 'cross_checkbutton'] = 'Provjeri križaljku';
$string[ 'cross_down'] = 'Okomito';
$string[ 'cross_endofgamebutton'] = 'Kraj igre';
$string[ 'cross_error_containsbadchars'] = 'Riječ sadrži nedozvoljene znakove';
$string[ 'cross_error_wordlength1'] = 'Točna riječ sadrži ';
$string[ 'cross_error_wordlength2'] = ' slova.';
$string[ 'cross_pleasewait'] = 'Molimo pričekajte dok se križaljka učita';
$string[ 'cross_welcome'] = '<h3>Dobrodošli!</h3><p>Za početak igre odaberite riječ.</p>';
$string[ 'finish'] = 'Kraj igre';
$string[ 'letter'] = 'slovo';
$string[ 'letters'] = 'slova';
$string[ 'nextgame'] = 'Nova igra';
$string[ 'no_words'] = 'Riječi nisu pronađene';
$string[ 'print'] = 'Ispiši';
$string[ 'win'] = 'Čestitamo !!!';

//db/access.php

//hangman/play.php
$string[ 'hangman_correct_phrase'] = 'Tražena fraza je: ';
$string[ 'hangman_correct_word'] = 'Tražena riječ je: ';
$string[ 'hangman_gradeinstance'] = 'Ocjena u cijeloj igri';
$string[ 'hangman_letters'] = 'Slova: ';
$string[ 'hangman_restletters_many'] = 'Imate još <b>$a</b> pokušaja';
$string[ 'hangman_restletters_one'] = 'Imate još <b>SAMO 1</b> pokušaj';
$string[ 'hangman_wrongnum'] = 'Krivo: %%d od %%d';
$string[ 'nextword'] = 'Sljedeća riječ';

//hiddenpicture/play.php
$string[ 'hiddenpicture_mainsubmit'] = 'Ocjeni glavni odgovor';
$string[ 'hiddenpicture_nocols'] = 'Treba zadati broj stupaca vodoravno';
$string[ 'hiddenpicture_nomainquestion'] = 'Nije pronađen ni jedan zapis u rječniku $a->name sa pridruženom slikom';
$string[ 'hiddenpicture_norows'] = 'Treba zadati broj stupaca okomito';
$string[ 'must_select_glossary'] = 'Morate odabrati rječnik';
$string[ 'noglossaryentriesfound'] = 'U rječniku nije pronađen ni jedan pojam';

//millionaire/play.php
$string[ 'millionaire_must_select_questioncategory'] = 'Morate odabrati kategoriju pitanja';
$string[ 'millionaire_must_select_quiz'] = 'Morate odabrati tip igre';

//report/overview/report.php
$string[ 'allattempts'] = 'Prikaži sve pokušaje';
$string[ 'allstudents'] = 'Prikaži sve $a';
$string[ 'attemptsonly'] = 'Prikaži samo studente sa pokušajima';
$string[ 'deleteattemptcheck'] = 'Dali ste sigurni da želite nepovratno obrisati ove pokušaje?';
$string[ 'displayoptions'] = 'Postavke prikaza';
$string[ 'downloadods'] = 'Preuzmi u ODS formatu';
$string[ 'feedback'] = 'Povratna informacija';
$string[ 'noattemptsonly'] = 'Prikaži samo $a bez pokušaja';
$string[ 'numattempts'] = '$a->studentnum $a->studentstring su imali $a->attemptnum pokušaja';
$string[ 'overview'] = 'Pregled';
$string[ 'pagesize'] = 'Pitanja po stranici:';
$string[ 'selectall'] = 'Označi sve';
$string[ 'selectnone'] = 'Odznači sve';
$string[ 'showdetailedmarks'] = 'Prikaži detalje ocjena';
$string[ 'startedon'] = 'Započeto';
$string[ 'timecompleted'] = 'Završeno';
$string[ 'timetaken'] = 'Proteklo vrijeme';
$string[ 'unfinished'] = 'nije završeno';
$string[ 'withselected'] = 'Sa odabranim';

//snakes/play.php

//sudoku/create.php
$string[ 'sudoku_create_count'] = 'Broju sudoku zadataka koji će se napraviti';
$string[ 'sudoku_create_start'] = 'Započni pravljenje sudou zadataka';
$string[ 'sudoku_creating'] = 'Pravim <b>$a</b> sudoku';

//sudoku/play.php
$string[ 'sudoku_finishattemptbutton'] = 'Kraj igre';
$string[ 'sudoku_guessnumber'] = 'Pogodite točan broj';
$string[ 'sudoku_noentriesfound'] = 'U rječniku nije pronađena ni jedna riječ';

//attempt.php

//backuplib.php
$string[ 'modulenameplural'] = 'Igre';

//export.php
$string[ 'export'] = 'Izvoz';
$string[ 'html_hascheckbutton'] = 'Sadrži tipku za provjeru:';
$string[ 'html_hasprintbutton'] = 'Sadrži tipku za ispis:';
$string[ 'html_title'] = 'Naslov stranice:';
$string[ 'javame_createdby'] = 'Autor:';
$string[ 'javame_description'] = 'Opis:';
$string[ 'javame_filename'] = 'Ime datoteke:';
$string[ 'javame_icon'] = 'Ikona:';
$string[ 'javame_maxpictureheight'] = 'Maksimalna visina slike:';
$string[ 'javame_maxpicturewidth'] = 'Maksimalna širina slike:';
$string[ 'javame_name'] = 'Ime:';
$string[ 'javame_type'] = 'Tip:';
$string[ 'javame_vendor'] = 'Prodavač:';
$string[ 'javame_version'] = 'Verzija:';
$string[ 'only_teachers'] = 'Samo učitelj može vidjeti ovu stranicu';

//exporthtml_hangman.php
$string[ 'hangman_loose'] = '<BIG><B>Kraj igre</B></BIG>';
$string[ 'html_hangman_new'] = 'Nova igra';

//exporthtml_millionaire.php
$string[ 'millionaire_helppeople'] = 'Pitaj publiku';
$string[ 'millionaire_info_people'] = 'Publika kaže';
$string[ 'millionaire_info_telephone'] = 'Mislim da je točan odgovor ';
$string[ 'millionaire_info_wrong_answer'] = 'Vaš odgovor je pogrešan<br>Točan odgovor je:';
$string[ 'millionaire_quit'] = 'Kraj';
$string[ 'millionaire_sourcemodule_must_quiz_question'] = 'Za Milijunaša izvor mora biti $a ili pitanja i ne';
$string[ 'millionaire_telephone'] = 'Pomoć zovi';
$string[ 'must_select_questioncategory'] = 'Morate odabrati kategoriju pitanja';
$string[ 'must_select_quiz'] = 'Morate odabrati igru';

//exporthtml_snakes.php
$string[ 'score'] = 'Rezultat';

//header.php
$string[ 'modulename'] = 'Igre';

//index.php
$string[ 'attempts'] = 'Pokušaji';

//lib.php
$string[ 'attempt'] = 'Pokušaj $a';
$string[ 'game_bookquiz'] = 'Knjiga sa pitanjima';
$string[ 'game_cross'] = 'Križaljka';
$string[ 'game_cryptex'] = 'Cryptex';
$string[ 'game_hangman'] = 'Vješalo';
$string[ 'game_hiddenpicture'] = 'Skrivena slika';
$string[ 'game_millionaire'] = 'Milijunaš';
$string[ 'game_snakes'] = 'Zmije i Ljestve';
$string[ 'game_sudoku'] = 'Sudoku';
$string[ 'noattempts'] = 'Ovu igru nitko još nije pokušao igrati';
$string[ 'percent'] = 'Postotak';

//locallib.php
$string[ 'attemptfirst'] = 'Prvi pokušaj';
$string[ 'attemptlast'] = 'Zadnji pokušaj';
$string[ 'convertfrom'] = '';      //Special convertation to capital letters
$string[ 'convertto'] = '';        //It is needed for some languages
$string[ 'gradeaverage'] = 'Prosječna ocjena';
$string[ 'gradehighest'] = 'Najveća ocjena';

//mod.html
$string[ 'bottomtext'] = 'Tekst na dnu';
$string[ 'cross_layout'] = 'Raspored';
$string[ 'cross_layout0'] = 'Fraze ispod križaljke';
$string[ 'cross_layout1'] = 'Fraze desno od križaljke';
$string[ 'cross_maxcols'] = 'Maksimaln broj stupaca u križaljci';
$string[ 'cross_maxwords'] = 'Maksimalan broj riječi u križaljci';
$string[ 'cryptex_maxcols'] = 'Maksimalan broj stupaca/redova cryptexa';
$string[ 'cryptex_maxtries'] = 'Maksimalan broj pokušaja';
$string[ 'cryptex_maxwords'] = 'Maksimalan broj riječi cryptexa';
$string[ 'grademethod'] = 'Metoda ocjenjivanja';
$string[ 'hangman_allowspaces'] = 'Dozvoli razmake u riječima';
$string[ 'hangman_allowsub'] = 'Dozvoli znak - u riječima';
$string[ 'hangman_imageset'] = 'Odaberite sliku vješala';
$string[ 'hangman_language'] = 'Jezik riječi';
$string[ 'hangman_maxtries'] = 'Broj riječi po igri';
$string[ 'hangman_showcorrectanswer'] = 'Prikaži ispravan odgovor nakon završetka';
$string[ 'hangman_showfirst'] = 'Prikaži prvo slovo vješala';
$string[ 'hangman_showlast'] = 'Prikaži zadnje slovo vješala';
$string[ 'hangman_showquestion'] = 'Prikaži pitanje?';
$string[ 'hiddenpicture_across'] = 'Ćelije vodoravno';
$string[ 'hiddenpicture_down'] = 'Ćelije okomito';
$string[ 'hiddenpicture_height'] = 'Postavi visinu slike na';
$string[ 'hiddenpicture_pictureglossary'] = 'Rječnik za glavno pitanje i sliku';
$string[ 'hiddenpicture_pictureglossarycategories'] = 'Kategorija rječnika za glavno pitanje';
$string[ 'hiddenpicture_width'] = 'Postavi širinu slike na';
$string[ 'millionaire_background'] = 'Boja pozadine';
$string[ 'millionaire_shuffle'] = 'Izmješaj pitanja';
$string[ 'snakes_background'] = 'Pozadina';
$string[ 'sourcemodule'] = 'Izvor pitanja';
$string[ 'sourcemodule_book'] = 'Odaberite knjigu';
$string[ 'sourcemodule_glossary'] = 'Odaberite rječnik';
$string[ 'sourcemodule_glossarycategory'] = 'Odaberite kategoriju rječnika';
$string[ 'sourcemodule_include_subcategories'] = 'Uključi podkategorije';
$string[ 'sourcemodule_question'] = 'Pitanja';
$string[ 'sourcemodule_questioncategory'] = 'Kategorija pitanja';
$string[ 'sourcemodule_quiz'] = 'Odaberite igru';
$string[ 'sudoku_maxquestions'] = 'Maksimalan broj pitanja';
$string[ 'toptext'] = 'Tekst na vrhu';

//mod_form.php
$string[ 'cross_options'] = 'Postavke križaljke';
$string[ 'cryptex_options'] = 'Cryptex postavke';
$string[ 'hangman_options'] = 'Postavke vješala';
$string[ 'headerfooter_options'] = 'Postavke zaglavlja/podnožja';
$string[ 'hiddenpicture_options'] = 'Postavke skrivene slike';
$string[ 'millionaire_options'] = 'Postavke Milijunaša';
$string[ 'snakes_options'] = 'Postavke \'Zmije i ljestve\'';
$string[ 'sudoku_options'] = 'Sudoku postavke';

//preview.php
$string[ 'formatdatetime'] = '%%d.%%m.%%Y %%I:%%M';
$string[ 'lastip'] = 'Adresa računala';
$string[ 'preview'] = 'Pregled';
$string[ 'showsolution'] = 'Rješenje';
$string[ 'timefinish'] = 'Kraj igre';
$string[ 'timelastattempt'] = 'Posljednji pokušaj';
$string[ 'timestart'] = 'Početak';

//review.php
$string[ 'completedon'] = 'Završeno';
$string[ 'outof'] = '$a->grade od maksimalno $a->maxgrade';
$string[ 'review'] = 'Pregled';
$string[ 'reviewofattempt'] = 'Pregled pokušaja $a';
$string[ 'startagain'] = 'Ponovo igraj';

//showanswers.php
$string[ 'clearrepetitions'] = 'Obriši statistiku';
$string[ 'computerepetitions'] = 'Ponovo izračunaj statistiku';
$string[ 'feedbacks'] = 'Poruke ispravnih odgovora';
$string[ 'repetitions'] = 'Ponavljanja';

//tabs.php
$string[ 'export_to_html'] = 'Izvoz u HTML';
$string[ 'export_to_javame'] = 'Izvoz u Javame';
$string[ 'info'] = 'Informacije';
$string[ 'results'] = 'Rezultati';
$string[ 'showanswers'] = 'Prikaži odgovore';
$string[ 'showattempts'] = 'Prikaži pokušaje';

//view.php
$string[ 'attemptgamenow'] = 'Pokušaj ponovo';
$string[ 'continueattemptgame'] = 'Nastavi sa prethodnim pokušajem ove igre';
$string[ 'gradesofar'] = '$a->method: $a->mygrade / $a->gamegrade.';
$string[ 'marks'] = 'Oznake';
$string[ 'nomoreattempts'] = 'Novi pokušaji nisu dozvoljeni';
$string[ 'reattemptgame'] = 'Ponovni pokušaj igre';
$string[ 'yourfinalgradeis'] = 'Vaša konačna ocjena za ovu igru je $a.';

//Untranslated
//db/access.php $string[ 'game:attempt'] = 'Play games';
//db/access.php $string[ 'game:deleteattempts'] = 'Delete attempts';
//db/access.php $string[ 'game:grade'] = 'Grade games manually';
//db/access.php $string[ 'game:manage'] = 'Manage';
//db/access.php $string[ 'game:preview'] = 'Preview Games';
//db/access.php $string[ 'game:reviewmyattempts'] = 'reviewmyattempts';
//db/access.php $string[ 'game:view'] = 'View game information';
//db/access.php $string[ 'game:viewreports'] = 'View game reports';
//hiddenpicture/play.php $string[ 'no_questions'] = "There are no questions";
//snakes/play.php $string[ 'snakes_dice'] = 'Dice, $a spots.';
//snakes/play.php $string[ 'snakes_player'] = 'Player, position: $a.';
//attempt.php $string[ 'gameclosed'] = 'This game closed on {$a}';
//attempt.php $string[ 'gamenotavailable'] = 'The game will not be available until {$a}';
//exporthtml_snakes.php $string[ 'html_snakes_check'] = 'Check';
//exporthtml_snakes.php $string[ 'html_snakes_correct'] = 'Correct!';
//exporthtml_snakes.php $string[ 'html_snakes_no_selection'] = 'Have to select something!';
//exporthtml_snakes.php $string[ 'html_snakes_wrong'] = "Your answer isn't correct. Stay on the same seat.";
//lib.php $string[ 'reset_game_all'] = 'Delete tries from all games';
//lib.php $string[ 'reset_game_deleted_course'] = 'Delete tries from deleted courses';
//mod.html $string[ 'selectgame'] = 'Select game';
//mod_form.php $string[ 'gameclose'] = 'Close the game';
//mod_form.php $string[ 'gameopen'] = 'Open the game';
//mod_form.php $string[ 'hangman_maximum_number_of_errors'] = 'Maximum number or errors (have to be images named hangman_0.jpg, hangman_1.jpg, ...)';
//mod_form.php $string[ 'snakes_board'] = 'Board';
//mod_form.php $string[ 'snakes_cols'] = 'Cols';
//mod_form.php $string[ 'snakes_file'] = 'File for background';
//mod_form.php $string[ 'snakes_footerx'] = 'Space at bootom left';
//mod_form.php $string[ 'snakes_footery'] = 'Space at bottom right';
//mod_form.php $string[ 'snakes_headerx'] = 'Space at up left';
//mod_form.php $string[ 'snakes_headery'] = 'Space at up right';
//mod_form.php $string[ 'snakes_rows'] = 'Rows';
//mod_form.php $string[ 'userdefined'] = "User defined ";
//view.php $string[ 'gamecloses'] = 'Game closes';
//view.php $string[ 'gameopens'] = 'Game opens';
