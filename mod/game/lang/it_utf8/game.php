<?php // $Id: game.php,v 1.4.2.10 2011/08/27 19:40:30 bdaloukas Exp $
      // game.php - created with Moodle 1.9.5
      // translated by Aristogeiton Ferentinos, Myrto Papakonstantinoy and Luciano Biondo

//bookquiz/importodt.php
$string[ 'bookquiz_import_odt'] = 'importa da un archivio OpenOffice(odt)';
$string[ 'bookquiz_not_select_book'] = 'Non avete selezionato un libro';
$string[ 'bookquiz_subchapter'] = 'creazione di capitoli';

//bookquiz/play.php
$string[ 'bookquiz_empty'] = 'Il libro e vuoto';
$string[ 'sudoku_submit'] = 'assegna un voto alle risposte';

//bookquiz/questions.php
$string[ 'bookquiz_categories'] = ' categorie';
$string[ 'bookquiz_chapters'] = 'capitoli';
$string[ 'bookquiz_questions'] = 'Categoria di domande relativa al capitolo del libro';

//cross/cross_class.php
$string[ 'lettersall'] = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

//cross/crossdb_class.php
$string[ 'and'] = 'e';
$string[ 'cross_correct'] = 'esatto';
$string[ 'cross_corrects'] = 'correttamente';
$string[ 'cross_error'] = 'errore';
$string[ 'cross_errors'] = 'errori';
$string[ 'cross_found_many'] = 'sono stati trovati';
$string[ 'cross_found_one'] = 'è stato trovato';
$string[ 'grade'] = 'voto';

//cross/play.php
$string[ 'cross_across'] = 'orizzontale';
$string[ 'cross_checkbutton'] = 'controllo del cruciverba';
$string[ 'cross_down'] = 'verticale';
$string[ 'cross_endofgamebutton'] = 'Fine del cruciverba';
$string[ 'cross_error_containsbadchars'] = 'la parola che avete scritto contiene caratteri non accettabili';
$string[ 'cross_error_wordlength1'] = 'la parola giusta contiene ';
$string[ 'cross_error_wordlength2'] = ' lettere.';
$string[ 'cross_pleasewait'] = 'per favore, attendi, stiamo caricando il cruciverba';
$string[ 'cross_welcome'] = '<h3>benvenuti!</h3><p>clicca una parola per cominciare .</p>';
$string[ 'finish'] = 'fine del gioco';
$string[ 'letter'] = 'lettera';
$string[ 'letters'] = 'lettere';
$string[ 'nextgame'] = 'nuovo gioco';
$string[ 'no_words'] = 'una o più parole non sono state trovate';
$string[ 'print'] = 'stampa';
$string[ 'win'] = 'congratulazioni!!!';

//db/access.php

//hangman/play.php
$string[ 'hangman_correct_phrase'] = 'la frase giusta era: ';
$string[ 'hangman_correct_word'] = 'la parola giusta era: ';
$string[ 'hangman_gradeinstance'] = 'punti a tutto il gioco ';
$string[ 'hangman_letters'] = ' lettere : ';
$string[ 'hangman_restletters_many'] = 'avete ancora <b>$a</b> delle opportunità';
$string[ 'hangman_restletters_one'] = 'avete<b>solo una</b> opportunità';
$string[ 'hangman_wrongnum'] = 'Sbagliato: %%d out of %%d';
$string[ 'nextword'] = 'parola prossima';

//hiddenpicture/play.php
$string[ 'hiddenpicture_mainsubmit'] = 'Valuta la risposta relativa alla figura nascosta';
$string[ 'hiddenpicture_nocols'] = 'dovete definire il numero di righe orizzontali';
$string[ 'hiddenpicture_nomainquestion'] = 'non c’è voce di glossario che $a->name contenga una figura';
$string[ 'hiddenpicture_norows'] = 'dovete definire il numero di colonne verticali';
$string[ 'must_select_glossary'] = 'dovete scegliere un glossario';
$string[ 'noglossaryentriesfound'] = 'Non sono state trovate voci di glossario';

//millionaire/play.php
$string[ 'millionaire_must_select_questioncategory'] = 'Dovrete scegliere una categoria di domande';
$string[ 'millionaire_must_select_quiz'] = 'dovrete scegliere un quiz';

//report/overview/report.php
$string[ 'allattempts'] = 'mostra tutti i tentativi';
$string[ 'allstudents'] = 'mostra tutti $a';
$string[ 'attemptsonly'] = 'indica solo gli studenti che hanno svolto tentativi';
$string[ 'deleteattemptcheck'] = 'Sei sicbookquiz_emptyuro che tu vuoi eliminare completamente questi tentativi?';
$string[ 'displayoptions'] = 'mostrare i fattori';
$string[ 'downloadods'] = 'salvare in formato ODS';
$string[ 'feedback'] = 'risposta';
$string[ 'noattemptsonly'] = 'mostra solo $a che non presentano tentativi';
$string[ 'numattempts'] = '$a->studentnum $a->studentstring Hanno fatto $a->attemptnum tentativi';
$string[ 'overview'] = 'sommario';
$string[ 'pagesize'] = 'domande per pagina:';
$string[ 'selectall'] = 'seleziona tutti';
$string[ 'selectnone'] = 'deseleziona tutti';
$string[ 'showdetailedmarks'] = 'mostra i dettagli delle valutazioni';
$string[ 'startedon'] = 'inziato alle';
$string[ 'timecompleted'] = ' completo';
$string[ 'timetaken'] = 'tempo impiegato';
$string[ 'unfinished'] = 'non concluso';
$string[ 'withselected'] = 'con i file selezionati';

//snakes/play.php

//sudoku/create.php
$string[ 'sudoku_create_count'] = 'numero di Sudoku che saranno creati';
$string[ 'sudoku_create_start'] = 'inizia a creare i sudoku';
$string[ 'sudoku_creating'] = 'creazione del <b>$a</b> sudoku';

//sudoku/play.php
$string[ 'sudoku_finishattemptbutton'] = 'fine del gioco';
$string[ 'sudoku_guessnumber'] = 'indovina il numero corretto';
$string[ 'sudoku_noentriesfound'] = 'non si sono trovate parole nel glossario';

//attempt.php

//backuplib.php
$string[ 'modulenameplural'] = 'giochi';

//export.php
$string[ 'export'] = 'esporta';
$string[ 'html_hascheckbutton'] = 'Includi il comando per controllare la risposta:';
$string[ 'html_hasprintbutton'] = 'ilcludi il comando di stampa:';
$string[ 'html_title'] = 'titolo della pagina:';
$string[ 'javame_createdby'] = 'creato da:';
$string[ 'javame_description'] = ' descrizione:';
$string[ 'javame_filename'] = 'nome del file:';
$string[ 'javame_icon'] = 'icona:';
$string[ 'javame_maxpictureheight'] = 'massima altezza della figura:';
$string[ 'javame_maxpicturewidth'] = 'massima larghezza della figura:';
$string[ 'javame_name'] = 'nome:';
$string[ 'javame_type'] = 'Tipo:';
$string[ 'javame_vendor'] = 'Vendor:';
$string[ 'javame_version'] = 'versione:';
$string[ 'only_teachers'] = 'solo gli insegnanti possono vedere questa pagina';

//exporthtml_hangman.php
$string[ 'hangman_loose'] = '<BIG><B>fine del gioco</B></BIG>';
$string[ 'html_hangman_new'] = 'nuovo';

//exporthtml_millionaire.php
$string[ 'millionaire_helppeople'] = 'chiedi al pubblico';
$string[ 'millionaire_info_people'] = 'il pubblico dice che';
$string[ 'millionaire_info_telephone'] = 'ritengo che la giusta risposta sia la…';
$string[ 'millionaire_info_wrong_answer'] = 'la tua risposta è sbagliata<br> La risposta corretta è :';
$string[ 'millionaire_quit'] = 'esci';
$string[ 'millionaire_sourcemodule_must_quiz_question'] = 'peri il gioco del milionario, la fonte dovrebbe essere $a o Domande e non …';
$string[ 'millionaire_telephone'] = 'chiedi aiuto a casa';
$string[ 'must_select_questioncategory'] = ' dovete scegliere una categoria di domande';
$string[ 'must_select_quiz'] = 'dovete scegliere un quiz';

//exporthtml_snakes.php
$string[ 'score'] = 'punti';

//header.php
$string[ 'modulename'] = 'gioco';

//index.php
$string[ 'attempts'] = 'prove';

//lib.php
$string[ 'attempt'] = ' tentativo $a';
$string[ 'game_bookquiz'] = 'libro con le domande';
$string[ 'game_cross'] = 'cruciverba';
$string[ 'game_cryptex'] = 'criptoverba';
$string[ 'game_hangman'] = 'gioco dell\'impiccato';
$string[ 'game_hiddenpicture'] = 'la pittura nascosta ';
$string[ 'game_millionaire'] = 'gioco del milionario';
$string[ 'game_snakes'] = 'serpentino';
$string[ 'game_sudoku'] = 'sudoku';
$string[ 'noattempts'] = 'nessun tentativo è stato fatto in questo gioco';
$string[ 'percent'] = 'percentuale';

//locallib.php
$string[ 'attemptfirst'] = 'primo tentativo';
$string[ 'attemptlast'] = 'ultimo tentativo';
$string[ 'gradeaverage'] = 'media dei voti';
$string[ 'gradehighest'] = 'il voto più alto';

//mod.html
$string[ 'bottomtext'] = 'testo sul fondo della pagina';
$string[ 'cross_layout'] = 'schema del cruciverba';
$string[ 'cross_layout0'] = 'definizioni sotto il cruciverba';
$string[ 'cross_layout1'] = 'definizioni a destra del cruciverba';
$string[ 'cross_maxcols'] = 'Massimo numero di linee / colonne nel cruciverba';
$string[ 'cross_maxwords'] = 'Massimo numero di parole nel cruciverba';
$string[ 'cryptex_maxcols'] = 'Massimo numero di line / colone nel criptoverba';
$string[ 'cryptex_maxtries'] = 'massimo numero di tentativi';
$string[ 'cryptex_maxwords'] = 'Massimo numero di parole nel criptoverba';
$string[ 'grademethod'] = 'metodo di valutazione';
$string[ 'hangman_allowspaces'] = 'consenti spazi vuoti fra le parole';
$string[ 'hangman_allowsub'] = 'consenti il simbolo – nelle parole';
$string[ 'hangman_imageset'] = 'scegliete compilazione di fotografie';
$string[ 'hangman_language'] = 'lingua delle parole';
$string[ 'hangman_maxtries'] = 'numero di parole per gioco';
$string[ 'hangman_showcorrectanswer'] = 'mostra la risposta esatta dopo la fine del gioco';
$string[ 'hangman_showfirst'] = 'mostra la prima lettera della parola';
$string[ 'hangman_showlast'] = 'mostra l\'ultima lettera della parola';
$string[ 'hangman_showquestion'] = 'mostra le domande ;';
$string[ 'hiddenpicture_across'] = 'numero di caselle orizzontali';
$string[ 'hiddenpicture_down'] = 'numero di caselle vertcali';
$string[ 'hiddenpicture_height'] = 'dimensione della altezza della figura';
$string[ 'hiddenpicture_pictureglossary'] = 'glossario di riferimento per la figura nascosta e relativa domanda';
$string[ 'hiddenpicture_pictureglossarycategories'] = 'glossario di riferimento per le varie domande';
$string[ 'hiddenpicture_width'] = 'dimensioni della larghezza della figura';
$string[ 'millionaire_background'] = 'colore di sfondo';
$string[ 'millionaire_shuffle'] = 'sorteggia l\'ordine delle domande';
$string[ 'selectgame'] = 'seleziona il gioco ι';
$string[ 'snakes_background'] = 'livello';
$string[ 'sourcemodule'] = 'sorgente delle domande';
$string[ 'sourcemodule_book'] = 'scegli un libro ';
$string[ 'sourcemodule_glossary'] = 'scegli un glossario';
$string[ 'sourcemodule_glossarycategory'] = 'scegli una categoria di glossari';
$string[ 'sourcemodule_include_subcategories'] = 'includere le sotto-categorie';
$string[ 'sourcemodule_question'] = 'domande';
$string[ 'sourcemodule_questioncategory'] = 'seleziona una categoria di domande';
$string[ 'sourcemodule_quiz'] = 'seleziona un quiz';
$string[ 'sudoku_maxquestions'] = 'Massimo numero di domande';
$string[ 'toptext'] = 'testo in cima';

//mod_form.php
$string[ 'cross_options'] = 'opzioni di incrocio';
$string[ 'cryptex_options'] = 'opzioni nel criptoverba';
$string[ 'hangman_options'] = 'opzioni del gioco dell\'impiccato';
$string[ 'headerfooter_options'] = 'opzioni nell\'header e footer';
$string[ 'hiddenpicture_options'] = 'opzioni della figura nascosta';
$string[ 'millionaire_options'] = 'opzioni del gioco del milionario';
$string[ 'snakes_options'] = 'opzioni del gioco delle serpentine';
$string[ 'sudoku_options'] = 'opzioni del gioco del Sudoku';

//preview.php
$string[ 'formatdatetime'] = '%%d %%b %%Y, %%H:%%M';
$string[ 'lastip'] = 'IP dello studente';
$string[ 'preview'] = 'anteprima';
$string[ 'showsolution'] = 'soluzione';
$string[ 'timefinish'] = 'fine del gioco';
$string[ 'timelastattempt'] = 'ultima prova';
$string[ 'timestart'] = 'inizio';

//review.php
$string[ 'completedon'] = 'Completato alle';
$string[ 'outof'] = '$a->voto su un massimo di $a->maxgrade';
$string[ 'review'] = 'mostra';
$string[ 'reviewofattempt'] = ';mostra il tentativo $a';
$string[ 'startagain'] = 'ricomincia';

//showanswers.php
$string[ 'clearrepetitions'] = 'Clear statistics';
$string[ 'computerepetitions'] = 'ricalcola le statistiche';
$string[ 'feedbacks'] = 'messaggio della risposta corretta';
$string[ 'repetitions'] = 'ripetizioni';

//tabs.php
$string[ 'export_to_html'] = 'esporta come HTML';
$string[ 'export_to_javame'] = 'esporta come JAVAME per celle';
$string[ 'info'] = ' informazioni';
$string[ 'results'] = 'risultati';
$string[ 'showanswers'] = 'mostra le risposte';
$string[ 'showattempts'] = 'mostra i tentativi';

//view.php
$string[ 'attemptgamenow'] = 'vuoi fare l\'esercizio ora?';
$string[ 'continueattemptgame'] = 'continua un precedente tentativo';
$string[ 'gradesofar'] = '$a->method: $a->mygrade / $a->gamegrade.';
$string[ 'marks'] = 'voti';
$string[ 'nomoreattempts'] = 'non sono permessi altri tentativi';
$string[ 'reattemptgame'] = 'nuovo tentativo nel gioco';
$string[ 'yourfinalgradeis'] = 'La tua valutazione finale per questo gioco è $a';

//Untranslated
//db/access.php $string[ 'game:attempt'] = 'Play games';
//db/access.php $string[ 'game:deleteattempts'] = 'Delete attempts';
//db/access.php $string[ 'game:grade'] = 'Grade games manually';
//db/access.php $string[ 'game:manage'] = 'Manage';
//db/access.php $string[ 'game:preview'] = 'Preview Games';
//db/access.php $string[ 'game:reviewmyattempts'] = 'reviewmyattempts';
//db/access.php $string[ 'game:view'] = 'View game information';
//db/access.php $string[ 'game:viewreports'] = 'View game reports';
//hiddenpicture/play.php $string[ 'no_questions'] = "There are no questions";
//snakes/play.php $string[ 'snakes_dice'] = 'Dice, $a spots.';
//snakes/play.php $string[ 'snakes_player'] = 'Player, position: $a.';
//attempt.php $string[ 'gameclosed'] = 'This game closed on {$a}';
//attempt.php $string[ 'gamenotavailable'] = 'The game will not be available until {$a}';
//exporthtml_snakes.php $string[ 'html_snakes_check'] = 'Check';
//exporthtml_snakes.php $string[ 'html_snakes_correct'] = 'Correct!';
//exporthtml_snakes.php $string[ 'html_snakes_no_selection'] = 'Have to select something!';
//exporthtml_snakes.php $string[ 'html_snakes_wrong'] = "Your answer isn't correct. Stay on the same seat.";
//lib.php $string[ 'reset_game_all'] = 'Delete tries from all games';
//lib.php $string[ 'reset_game_deleted_course'] = 'Delete tries from deleted courses';
//mod_form.php $string[ 'gameclose'] = 'Close the game';
//mod_form.php $string[ 'gameopen'] = 'Open the game';
//mod_form.php $string[ 'hangman_maximum_number_of_errors'] = 'Maximum number or errors (have to be images named hangman_0.jpg, hangman_1.jpg, ...)';
//mod_form.php $string[ 'snakes_board'] = 'Board';
//mod_form.php $string[ 'snakes_cols'] = 'Cols';
//mod_form.php $string[ 'snakes_file'] = 'File for background';
//mod_form.php $string[ 'snakes_footerx'] = 'Space at bootom left';
//mod_form.php $string[ 'snakes_footery'] = 'Space at bottom right';
//mod_form.php $string[ 'snakes_headerx'] = 'Space at up left';
//mod_form.php $string[ 'snakes_headery'] = 'Space at up right';
//mod_form.php $string[ 'snakes_rows'] = 'Rows';
//mod_form.php $string[ 'userdefined'] = "User defined ";
//view.php $string[ 'gamecloses'] = 'Game closes';
//view.php $string[ 'gameopens'] = 'Game opens';
