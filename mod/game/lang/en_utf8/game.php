<?php // $Id: game.php,v 1.24.2.20 2012/01/16 21:45:05 bdaloukas Exp $
      // translated by Vasilis Daloukas, Jim Gary

//bookquiz/importodt.php
$string[ 'bookquiz_import_odt'] = 'Import from an OpenOffice file (odt)';
$string[ 'bookquiz_not_select_book'] = 'You have not selected a book';
$string[ 'bookquiz_subchapter'] = 'Create subchapters';

//bookquiz/play.php
$string[ 'bookquiz_empty'] = 'The book is empty';
$string[ 'sudoku_submit'] = 'Grade answers';

//bookquiz/questions.php
$string[ 'bookquiz_categories'] = 'Categories';
$string[ 'bookquiz_chapters'] = 'Chapters';
$string[ 'bookquiz_questions'] = 'Associate question categories with book chapter';

//cross/cross_class.php
$string[ 'lettersall'] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ ";

//cross/crossdb_class.php
$string[ 'and'] = 'and';
$string[ 'cross_correct'] = 'Correct';
$string[ 'cross_corrects'] = 'Correct';
$string[ 'cross_error'] = 'Error';
$string[ 'cross_errors'] = 'Errors';
$string[ 'cross_found_many'] = 'Found';
$string[ 'cross_found_one'] = 'Found';
$string[ 'grade'] = 'Grade';

//cross/play.php
$string[ 'cross_across'] = 'Across';
$string[ 'cross_checkbutton'] = 'Check crossword';
$string[ 'cross_down'] = 'Down';
$string[ 'cross_endofgamebutton'] = 'End Game';
$string[ 'cross_error_containsbadchars'] = 'The word contains illegal characters';
$string[ 'cross_error_wordlength1'] = 'The correct word contains ';
$string[ 'cross_error_wordlength2'] = ' letters.';
$string[ 'cross_pleasewait'] = 'Please wait while puzzle is loading';
$string[ 'cross_welcome'] = '<h3>Welcome!</h3><p>Click on a word to begin.</p>';
$string[ 'finish'] = 'End of game';
$string[ 'letter'] = 'letter';
$string[ 'letters'] = 'letters';
$string[ 'nextgame'] = 'New game';
$string[ 'no_words'] = 'There are no words';
$string[ 'print'] = 'Print';
$string[ 'win'] = 'Congratulations !!!!';

//db/access.php
$string[ 'game:attempt'] = 'Play games';
$string[ 'game:deleteattempts'] = 'Delete attempts';
$string[ 'game:grade'] = 'Grade games manually';
$string[ 'game:manage'] = 'Manage';
$string[ 'game:preview'] = 'Preview Games';
$string[ 'game:reviewmyattempts'] = 'reviewmyattempts';
$string[ 'game:view'] = 'View game information';
$string[ 'game:viewreports'] = 'View game reports';

//hangman/play.php
$string[ 'hangman_correct_phrase'] = 'The correct phrase was: ';
$string[ 'hangman_correct_word'] = 'The correct word was: ';
$string[ 'hangman_gradeinstance'] = 'Overall Grade';
$string[ 'hangman_letters'] = 'Letters: ';
$string[ 'hangman_restletters_many'] = 'You have <b>$a</b> tries';
$string[ 'hangman_restletters_one'] = 'You have <b>ONLY 1</b> try';
$string[ 'hangman_wrongnum'] = 'Wrong: %%d out of %%d';
$string[ 'nextword'] = 'Next word';

//hiddenpicture/play.php
$string[ 'hiddenpicture_mainsubmit'] = 'Grade main answer';
$string[ 'hiddenpicture_nocols'] = 'Please specify the number of horizontal columns';
$string[ 'hiddenpicture_nomainquestion'] = 'There are no glossary entries in glossary $a->name with an attached picture';
$string[ 'hiddenpicture_norows'] = 'Please specify the number of vertical columns';
$string[ 'must_select_glossary'] = 'You must select a glossary';
$string[ 'no_questions'] = "There are no questions";
$string[ 'noglossaryentriesfound'] = 'No glossary entries found';

//millionaire/play.php
$string[ 'millionaire_must_select_questioncategory'] = 'You must select one question category';
$string[ 'millionaire_must_select_quiz'] = 'You must select one quiz';

//report/overview/report.php
$string[ 'allattempts'] = 'Show all tries';
$string[ 'allstudents'] = 'Show all $a';
$string[ 'attemptsonly'] = 'Show only students with attempts';
$string[ 'deleteattemptcheck'] = 'Are you absolutely sure you want to completely delete these attempts?';
$string[ 'displayoptions'] = 'Display options';
$string[ 'downloadods'] = 'Download in ODS format';
$string[ 'feedback'] = 'Feedback';
$string[ 'noattemptsonly'] = 'Show $a with no attempts only';
$string[ 'numattempts'] = '$a->studentnum $a->studentstring have made $a->attemptnum attempts';
$string[ 'overview'] = 'Overview';
$string[ 'pagesize'] = 'Questions per page:';
$string[ 'selectall'] = 'Select all';
$string[ 'selectnone'] = 'Deselect all';
$string[ 'showdetailedmarks'] = 'Show mark details';
$string[ 'startedon'] = 'Started on';
$string[ 'timecompleted'] = 'Completed';
$string[ 'timetaken'] = 'Time taken';
$string[ 'unfinished'] = 'open';
$string[ 'withselected'] = 'With selected';

//snakes/play.php
$string[ 'snakes_dice'] = 'Dice, $a spots.';
$string[ 'snakes_player'] = 'Player, position: $a.';

//sudoku/create.php
$string[ 'sudoku_create_count'] = 'Number of sudokus that will be created';
$string[ 'sudoku_create_start'] = 'Start creating sudokus';
$string[ 'sudoku_creating'] = 'Creating <b>$a</b> sudoku';

//sudoku/play.php
$string[ 'sudoku_finishattemptbutton'] = 'End of sudoku game';
$string[ 'sudoku_guessnumber'] = 'Guess the correct number';
$string[ 'sudoku_noentriesfound'] = 'No words found in glossary';

//attempt.php
$string[ 'gameclosed'] = 'This game closed on {$a}';
$string[ 'gamenotavailable'] = 'The game will not be available until {$a}';

//backuplib.php
$string[ 'modulenameplural'] = 'Games';

//export.php
$string[ 'export'] = 'Export';
$string[ 'html_hascheckbutton'] = 'Include check button:';
$string[ 'html_hasprintbutton'] = 'Include print button:';
$string[ 'html_title'] = 'Title of html:';
$string[ 'javame_createdby'] = 'Created by:';
$string[ 'javame_description'] = 'Description:';
$string[ 'javame_filename'] = 'Filename:';
$string[ 'javame_icon'] = 'Icon:';
$string[ 'javame_maxpictureheight'] = 'Max picture height:';
$string[ 'javame_maxpicturewidth'] = 'Max picture width:';
$string[ 'javame_name'] = 'Name:';
$string[ 'javame_type'] = 'Type:';
$string[ 'javame_vendor'] = 'Vendor:';
$string[ 'javame_version'] = 'Version:';
$string[ 'only_teachers'] = 'Only teacher can see this page';

//exporthtml_hangman.php
$string[ 'hangman_loose'] = '<BIG><B>Game over</B></BIG>';
$string[ 'html_hangman_new'] = 'New';

//exporthtml_millionaire.php
$string[ 'millionaire_helppeople'] = 'Ask the audience';
$string[ 'millionaire_info_people'] = 'Audience says';
$string[ 'millionaire_info_telephone'] = 'I think the correct answer is ';
$string[ 'millionaire_info_wrong_answer'] = 'Your answer is wrong<br>The right answer is:';
$string[ 'millionaire_quit'] = 'Quit';
$string[ 'millionaire_sourcemodule_must_quiz_question'] = 'For the millionaire the source must be $a or questions and not';
$string[ 'millionaire_telephone'] = 'Call a friend';
$string[ 'must_select_questioncategory'] = 'You must select a question category';
$string[ 'must_select_quiz'] = 'You must select a quiz';

//exporthtml_snakes.php
$string[ 'html_snakes_check'] = 'Check';
$string[ 'html_snakes_correct'] = 'Correct!';
$string[ 'html_snakes_no_selection'] = 'Have to select something!';
$string[ 'html_snakes_wrong'] = "Your answer isn't correct. Stay on the same seat.";
$string[ 'score'] = 'Score';

//header.php
$string[ 'modulename'] = 'Game';

//index.php
$string[ 'attempts'] = 'Attempts';

//lib.php
$string[ 'attempt'] = 'Attempt $a';
$string[ 'game_bookquiz'] = 'Book with questions';
$string[ 'game_contest'] = 'Contest';
$string[ 'game_cross'] = 'Crossword';
$string[ 'game_cryptex'] = 'Cryptex';
$string[ 'game_hangman'] = 'Hang man';
$string[ 'game_hiddenpicture'] = 'Hidden Picture';
$string[ 'game_millionaire'] = 'Millionaire';
$string[ 'game_snakes'] = 'Snakes and Ladders';
$string[ 'game_sudoku'] = 'Sudoku';
$string[ 'noattempts'] = 'No attempts have been made on this game';
$string[ 'percent'] = 'Percent';
$string[ 'reset_game_all'] = 'Delete tries from all games';
$string[ 'reset_game_deleted_course'] = 'Delete tries from deleted courses';

//locallib.php
$string[ 'attemptfirst'] = 'First attempt';
$string[ 'attemptlast'] = 'Last attempt';
$string[ 'convertfrom'] = '';      //Special convertation to capital letters. It is needed for some languages
$string[ 'convertto'] = '';        //Special convertation to capital letters. It is needed for some languages
$string[ 'gradeaverage'] = 'Average grade';
$string[ 'gradehighest'] = 'Highest grade';

//mod.html
$string[ 'bottomtext'] = 'Text at the bottom';
$string[ 'cross_layout'] = 'Layout';
$string[ 'cross_layout0'] = 'Phrases on the bottom of puzzle';
$string[ 'cross_layout1'] = 'Phrases on the right of puzzle';
$string[ 'cross_maxcols'] = 'Maximum number of columns';
$string[ 'cross_maxwords'] = 'Maximum number of words';
$string[ 'cryptex_maxcols'] = 'Maximum number of columns/rows in cryptex';
$string[ 'cryptex_maxtries'] = 'Maximum tries';
$string[ 'cryptex_maxwords'] = 'Maximum number of words in cryptex';
$string[ 'grademethod'] = 'Grading method';
$string[ 'hangman_allowspaces'] = 'Allow spaces in words';
$string[ 'hangman_allowsub'] = 'Allow the symbol - in words';
$string[ 'hangman_imageset'] = 'Select hangman image';
$string[ 'hangman_language'] = 'Language';
$string[ 'hangman_maxtries'] = 'Number of words per game';
$string[ 'hangman_showcorrectanswer'] = 'Show the correct answer after the end';
$string[ 'hangman_showfirst'] = 'Show first letter of hangman';
$string[ 'hangman_showlast'] = 'Show last letter of hangman';
$string[ 'hangman_showquestion'] = 'Show the questions ?';
$string[ 'hiddenpicture_across'] = 'Cells horizontal';
$string[ 'hiddenpicture_down'] = 'Cells down';
$string[ 'hiddenpicture_height'] = 'Set height of picture to';
$string[ 'hiddenpicture_pictureglossary'] = 'Source glossary for main question and picture';
$string[ 'hiddenpicture_pictureglossarycategories'] = 'Glossary category for main question';
$string[ 'hiddenpicture_width'] = 'Set width of picture to';
$string[ 'millionaire_background'] = 'Background color';
$string[ 'millionaire_shuffle'] = 'Randomize questions';
$string[ 'selectgame'] = 'Select game';
$string[ 'snakes_background'] = 'Background';
$string[ 'sourcemodule'] = 'Question Source';
$string[ 'sourcemodule_book'] = 'Select a book';
$string[ 'sourcemodule_glossary'] = 'Select glossary';
$string[ 'sourcemodule_glossarycategory'] = 'Select glossary category';
$string[ 'sourcemodule_include_subcategories'] = 'Include subcategories';
$string[ 'sourcemodule_question'] = 'Questions';
$string[ 'sourcemodule_questioncategory'] = 'Select question category';
$string[ 'sourcemodule_quiz'] = 'Select quiz';
$string[ 'sudoku_maxquestions'] = 'Maximum number of questions';
$string[ 'toptext'] = 'Text at the top';

//mod_form.php
$string[ 'contest_has_notes'] = 'Has notes';
$string[ 'contest_options'] = 'Contest options';
$string[ 'cross_options'] = 'Crossword options';
$string[ 'cryptex_options'] = 'Cryptex options';
$string[ 'gameclose'] = 'Close the game';
$string[ 'gameopen'] = 'Open the game';
$string[ 'hangman_maximum_number_of_errors'] = 'Maximum number or errors (have to be images named hangman_0.jpg, hangman_1.jpg, ...)';
$string[ 'hangman_options'] = 'Hangman options';
$string[ 'headerfooter_options'] = 'Header/Footer options';
$string[ 'hiddenpicture_options'] = '\'Hidden Picture\' options';
$string[ 'millionaire_options'] = 'Millionaire options';
$string[ 'snakes_board'] = 'Board';
$string[ 'snakes_cols'] = 'Cols';
$string[ 'snakes_file'] = 'File for background';
$string[ 'snakes_footerx'] = 'Space at bootom left';
$string[ 'snakes_footery'] = 'Space at bottom right';
$string[ 'snakes_headerx'] = 'Space at up left';
$string[ 'snakes_headery'] = 'Space at up right';
$string[ 'snakes_options'] = '\'Snakes and Ladders\' options';
$string[ 'snakes_rows'] = 'Rows';
$string[ 'sudoku_options'] = 'Sudoku options';
$string[ 'userdefined'] = "User defined ";

//preview.php
$string[ 'formatdatetime'] = '%%d %%b %%Y, %%I:%%M %%p';
$string[ 'lastip'] = 'IP student';
$string[ 'preview'] = 'Preview';
$string[ 'showsolution'] = 'Solution';
$string[ 'timefinish'] = 'End of game';
$string[ 'timelastattempt'] = 'Last attempt';
$string[ 'timestart'] = 'Start';

//review.php
$string[ 'completedon'] = 'Completed on';
$string[ 'outof'] = '$a->grade out of a maximum of $a->maxgrade';
$string[ 'review'] = 'Review';
$string[ 'reviewofattempt'] = 'Review of Attempt $a';
$string[ 'startagain'] = 'Start again';

//showanswers.php
$string[ 'clearrepetitions'] = 'Clear statistics';
$string[ 'computerepetitions'] = 'Compute statistics again';
$string[ 'feedbacks'] = 'Messages correct answer';
$string[ 'repetitions'] = 'Repetitions';

//tabs.php
$string[ 'export_to_html'] = 'Export to HTML';
$string[ 'export_to_javame'] = 'Export to Javame';
$string[ 'info'] = 'Info';
$string[ 'results'] = 'Results';
$string[ 'showanswers'] = 'Show answers';
$string[ 'showattempts'] = 'Show attempts';

//view.php
$string[ 'attemptgamenow'] = 'Attempt game now';
$string[ 'contest_not_solved'] = 'Not solved yet';
$string[ 'contest_solved'] = 'Solved';
$string[ 'contest_top'] = 'Top grades';
$string[ 'continueattemptgame'] = 'Continue a previous attempt';
$string[ 'gamecloses'] = 'Game closes';
$string[ 'gameopens'] = 'Game opens';
$string[ 'gradesofar'] = '$a->method: $a->mygrade / $a->gamegrade.';
$string[ 'marks'] = 'Marks';
$string[ 'nomoreattempts'] = 'No more attempts are allowed';
$string[ 'reattemptgame'] = 'Re-attempt game';
$string[ 'yourfinalgradeis'] = 'Your final grade for this game is $a.';
