<?php // $Id: game.php,v 1.1.2.9 2011/08/27 19:40:28 bdaloukas Exp $ 
      // game.php - created with Moodle 1.9.1+ (Build: 20080702) (2007101514)
      // translation to catalan (ca_utf8). Thanks to Josep Ramon.

//bookquiz/importodt.php
$string[ 'bookquiz_import_odt'] = 'Importar arxiu OpenOffice (odt)';
$string[ 'bookquiz_not_select_book'] = 'Vd no ha seleccionat un llibre';
$string[ 'bookquiz_subchapter'] = 'Crei subcapítols';

//bookquiz/play.php
$string[ 'bookquiz_empty'] = 'El llibre està buid';
$string[ 'sudoku_submit'] = 'Avaluar respostes';

//bookquiz/questions.php
$string[ 'bookquiz_categories'] = 'Categories';
$string[ 'bookquiz_chapters'] = 'Capítols';
$string[ 'bookquiz_questions'] = 'Associa categories de preguntes amb capítuls del llibre';

//cross/cross_class.php
$string[ 'lettersall'] = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

//cross/crossdb_class.php
$string[ 'and'] = 'y';
$string[ 'cross_correct'] = 'correcte';
$string[ 'cross_corrects'] = 'correctes';
$string[ 'cross_error'] = 'error';
$string[ 'cross_errors'] = 'errors';
$string[ 'cross_found_many'] = 'Trobat';
$string[ 'cross_found_one'] = 'Trobat';
$string[ 'grade'] = 'Nota';

//cross/play.php
$string[ 'cross_across'] = 'Horitzontal';
$string[ 'cross_checkbutton'] = 'Revisar mots encreuats';
$string[ 'cross_down'] = 'Avall';
$string[ 'cross_endofgamebutton'] = 'Fí del joc dels mots encreuats';
$string[ 'cross_error_containsbadchars'] = 'La paraula conté caracters no autoritzats';
$string[ 'cross_error_wordlength1'] = 'La paraula correcta conté ';
$string[ 'cross_error_wordlength2'] = ' lletres.';
$string[ 'cross_pleasewait'] = 'Si et plau espera mentre es carrega l\'encreuat';
$string[ 'cross_welcome'] = '<h3>¡Benvingut!</h3><p>Fes clic en una paraula per començar.</p>';
$string[ 'finish'] = 'Fi del joc';
$string[ 'letter'] = 'lletra';
$string[ 'letters'] = 'lletres';
$string[ 'nextgame'] = 'Següent joc';
$string[ 'no_words'] = 'Cap paraula trobada';
$string[ 'print'] = 'Imprimir';

//db/access.php
$string[ 'game:attempt'] = 'Juga el joc';
$string[ 'game:deleteattempts'] = 'Esborrar intents';
$string[ 'game:manage'] = 'Administrar';
$string[ 'game:reviewmyattempts'] = 'Revisar els meus intents';
$string[ 'game:view'] = 'Veure';
$string[ 'game:viewreports'] = 'Veure informes';

//hangman/play.php
$string[ 'hangman_correct_phrase'] = 'La frase correcta era: ';
$string[ 'hangman_correct_word'] = 'La paraula correcta era: ';
$string[ 'hangman_gradeinstance'] = 'Nivell en el joc complet';
$string[ 'hangman_letters'] = 'Lletres: ';
$string[ 'hangman_restletters_many'] = 'Vd. té <b>$a</b> intents';
$string[ 'hangman_restletters_one'] = 'Vd. té <b>ÚNICAMENT 1</b> intent';
$string[ 'hangman_wrongnum'] = 'Dolentes: %%d de %%d';
$string[ 'nextword'] = 'Següent paraula';

//hiddenpicture/play.php
$string[ 'hiddenpicture_mainsubmit'] = 'Qualificació de la pregunta principal';
$string[ 'hiddenpicture_nocols'] = 'Cal especificar el nombre de fileres horitzontals';
$string[ 'hiddenpicture_nomainquestion'] = 'No hi ha entrades en el glossari $a->name amb una imatge annexada';
$string[ 'hiddenpicture_norows'] = 'Cal especificar el nombre de columnes verticals';
$string[ 'must_select_glossary'] = 'Vd ha de seleccionar un glossari';
$string[ 'noglossaryentriesfound'] = 'No hi ha entrades de glossari';

//millionaire/play.php
$string[ 'millionaire_must_select_questioncategory'] = 'Cal seleccionar una categoria de preguntes';
$string[ 'millionaire_must_select_quiz'] = 'Cal seleccionar un qüestionari';

//report/overview/report.php
$string[ 'allattempts'] = 'Mostrar tots els intents';
$string[ 'allstudents'] = 'Mostrar tots $a';
$string[ 'attemptsonly'] = 'Mostrar únicament estudiants amb intents';
$string[ 'deleteattemptcheck'] = 'Està absolutament segur de voler esborrar completament aquests intents?';
$string[ 'displayoptions'] = 'Mostrar opcions';
$string[ 'downloadods'] = 'Descarregar en format ODS';
$string[ 'feedback'] = 'Retroalimentació';
$string[ 'noattemptsonly'] = 'Mostrar $a únicament sense intents';
$string[ 'numattempts'] = '$a->studentnum $a->studentstring ha fet $a->attemptnum intents';
$string[ 'overview'] = 'Llistat';
$string[ 'pagesize'] = 'Preguntes per pàgina:';
$string[ 'selectall'] = 'Seleccionar tots';
$string[ 'selectnone'] = 'Desmarcar tots';
$string[ 'showdetailedmarks'] = 'Mostrar detalls de marca';
$string[ 'startedon'] = 'Començà en';
$string[ 'timecompleted'] = 'Completat';
$string[ 'timetaken'] = 'Temps utilitzat';
$string[ 'unfinished'] = 'obert';
$string[ 'withselected'] = 'Amb seleccionats';

//snakes/play.php

//sudoku/create.php
$string[ 'sudoku_create_count'] = 'Nombre de sudokus que seran creats';
$string[ 'sudoku_create_start'] = 'Començar creant sudokus';
$string[ 'sudoku_creating'] = 'Creant <b>$a</b> sudoku';

//sudoku/play.php
$string[ 'sudoku_finishattemptbutton'] = 'Fi del joc de sudoku';
$string[ 'sudoku_guessnumber'] = 'Endevini el nombre correcte';
$string[ 'sudoku_noentriesfound'] = 'Cap paraula trobada al glossari';

//attempt.php

//backuplib.php
$string[ 'modulenameplural'] = 'Jocs';

//export.php
$string[ 'export'] = 'Exporta a mòbil';
$string[ 'html_hascheckbutton'] = 'Botó de prova:';
$string[ 'html_hasprintbutton'] = 'Botó impresió:';
$string[ 'html_title'] = 'Títol d\'html:';
$string[ 'javame_createdby'] = 'Creat per:';
$string[ 'javame_description'] = 'Descripció:';
$string[ 'javame_filename'] = 'Nom de l\'arxiu:';
$string[ 'javame_icon'] = 'Icona:';
$string[ 'javame_maxpictureheight'] = 'Alçada màxima de la imatge:';
$string[ 'javame_maxpicturewidth'] = 'Amplada màxima de la imatge:';
$string[ 'javame_name'] = 'Nom:';
$string[ 'javame_type'] = 'Tipus:';
$string[ 'javame_vendor'] = 'Venedor:';
$string[ 'javame_version'] = 'Versió';
$string[ 'only_teachers'] = 'Només el professor pot veure aquesta pàgina';

//exporthtml_hangman.php
$string[ 'hangman_loose'] = '<BIG><B>Fí del joc</B></BIG>';
$string[ 'html_hangman_new'] = 'Nou';

//exporthtml_millionaire.php
$string[ 'millionaire_helppeople'] = 'Ajut del públic';
$string[ 'millionaire_info_people'] = 'La gent diu';
$string[ 'millionaire_info_telephone'] = 'Jo penso que la resposta correcta és';
$string[ 'millionaire_info_wrong_answer'] = 'La seva resposta és incorrecta<br>La resposta correcta és:';
$string[ 'millionaire_quit'] = 'Sortir';
$string[ 'millionaire_sourcemodule_must_quiz_question'] = 'Pel millonari la font és $a o preguntes i no';
$string[ 'millionaire_telephone'] = 'Trucada telefònica';
$string[ 'must_select_questioncategory'] = 'Cal seleccionar una categoria de preguntes';
$string[ 'must_select_quiz'] = 'Cal seleccionar un qüestionari';

//exporthtml_snakes.php
$string[ 'score'] = 'Punts';

//header.php
$string[ 'modulename'] = 'Joc';

//index.php
$string[ 'attempts'] = 'Intents';

//lib.php
$string[ 'attempt'] = 'Intent $a';
$string[ 'game_bookquiz'] = 'Llibre amb preguntes';
$string[ 'game_cross'] = 'Mots encreuats';
$string[ 'game_cryptex'] = 'Sopa de Lletres';
$string[ 'game_hangman'] = 'Penjat';
$string[ 'game_hiddenpicture'] = 'Imatge amagada';
$string[ 'game_millionaire'] = 'Millionari';
$string[ 'game_snakes'] = 'Serps i Escales';
$string[ 'game_sudoku'] = 'Sudoku';
$string[ 'noattempts'] = 'No s\'ha fet cap intent en aquest qüestionari';
$string[ 'percent'] = 'Percentatge';

//locallib.php
$string[ 'attemptfirst'] = 'Primer intent';
$string[ 'attemptlast'] = 'Darrer intent';
$string[ 'gradeaverage'] = 'Nota promig';
$string[ 'gradehighest'] = 'Nota més alta';

//mod.html
$string[ 'bottomtext'] = 'Text al final';
$string[ 'cross_layout'] = 'Disseny';
$string[ 'cross_layout0'] = 'Frases a la part inferior dels mots encreuats';
$string[ 'cross_layout1'] = 'Frases a la part dreta dels mots encreuats';
$string[ 'cross_maxcols'] = 'Nombre màxim de columnes dels mots encreuats';
$string[ 'cross_maxwords'] = 'Màxim nombre de paraules dels mots encreuats';
$string[ 'cryptex_maxcols'] = 'Máxim nombre de columnes/fileres en la Sopa de Lletres';
$string[ 'cryptex_maxtries'] = 'Nombre màxim d\'intents';
$string[ 'cryptex_maxwords'] = 'Màxim nombre de paraules en Sopa de Lletres';
$string[ 'grademethod'] = 'Mètode de qüalificació';
$string[ 'hangman_allowspaces'] = 'Permetre espais en les paraules';
$string[ 'hangman_allowsub'] = 'Permetre símbols en las paraules';
$string[ 'hangman_imageset'] = 'Seleccioni les imatges pel penjat';
$string[ 'hangman_language'] = 'Idioma de les paraules';
$string[ 'hangman_maxtries'] = 'Nombre de paraules per joc';
$string[ 'hangman_showcorrectanswer'] = 'Mostrar la resposta correcta després del final';
$string[ 'hangman_showfirst'] = 'Mostrar la primera lletra del penjat';
$string[ 'hangman_showlast'] = 'Mostrar la darrera lletra del penjat';
$string[ 'hangman_showquestion'] = '¿ Mostrar les preguntes ?';
$string[ 'hiddenpicture_across'] = 'Caselles horizontals';
$string[ 'hiddenpicture_down'] = 'Caselles verticals';
$string[ 'hiddenpicture_height'] = 'Establir l\'alçada de la imatge en';
$string[ 'hiddenpicture_pictureglossary'] = 'El glossari per la qüestió principal';
$string[ 'hiddenpicture_pictureglossarycategories'] = 'La categoría de glossari per la qüestió principal';
$string[ 'hiddenpicture_width'] = 'Establir l\'amplada de la imatge en';
$string[ 'millionaire_background'] = 'Color de fons';
$string[ 'millionaire_shuffle'] = 'Barrejar preguntes';
$string[ 'selectgame'] = 'Seleccionar joc';
$string[ 'snakes_background'] = 'Fons';
$string[ 'sourcemodule'] = 'Font de preguntes';
$string[ 'sourcemodule_book'] = 'Seleccioni un llibre';
$string[ 'sourcemodule_glossary'] = 'Seleccioni un glossari';
$string[ 'sourcemodule_glossarycategory'] = 'Seleccioni una categoria del glossario.';
$string[ 'sourcemodule_include_subcategories'] = 'Incloure subcategories';
$string[ 'sourcemodule_question'] = 'Preguntes';
$string[ 'sourcemodule_questioncategory'] = 'Seleccioni una categoria de preguntas';
$string[ 'sourcemodule_quiz'] = 'Seleccioni un qüestionari';
$string[ 'sudoku_maxquestions'] = 'Máxim nombre de preguntes';
$string[ 'toptext'] = 'Text de la part superior';

//mod_form.php
$string[ 'cross_options'] = 'Opcions dels Mots encreuats';
$string[ 'cryptex_options'] = 'Opcions del Criptograma';
$string[ 'hangman_options'] = 'Opcions del Penjat';
$string[ 'headerfooter_options'] = 'caOpcions de Capçalera/Peu de pàgina';
$string[ 'hiddenpicture_options'] = '\'Imatge Amagada\' opcions';
$string[ 'millionaire_options'] = 'Opcions del Millionari';
$string[ 'snakes_board'] = 'Tauler';
$string[ 'snakes_cols'] = 'Columnes';
$string[ 'snakes_footerx'] = 'Espai inferior esquerre';
$string[ 'snakes_footery'] = 'Espai inferior dret';
$string[ 'snakes_headerx'] = 'Espai superior esquerre';
$string[ 'snakes_headery'] = 'Espai superior dret';
$string[ 'snakes_options'] = '\'Serps i Escales\' opcions';
$string[ 'snakes_rows'] = 'Fileres';
$string[ 'sudoku_options'] = 'Opcions del Sudoku';
$string[ 'userdefined'] = 'Definit per l\'usuari';

//preview.php
$string[ 'formatdatetime'] = '%%d %%b %%Y, %%I:%%M %%p';
$string[ 'lastip'] = 'IP de l\'estudiant';
$string[ 'preview'] = 'Visualitzar';
$string[ 'showsolution'] = 'solució';
$string[ 'timefinish'] = 'Fi del joc';
$string[ 'timelastattempt'] = 'Darrer intent';
$string[ 'timestart'] = 'Començament';

//review.php
$string[ 'completedon'] = 'Completat en';
$string[ 'outof'] = '$a->grade de un màxim de $a->maxgrade';
$string[ 'review'] = 'Revisar';
$string[ 'reviewofattempt'] = 'Revisar intents $a';
$string[ 'startagain'] = 'Començar de nou';

//showanswers.php
$string[ 'clearrepetitions'] = 'Esborrar estadístiques';
$string[ 'computerepetitions'] = 'Recalcular estadístiques';
$string[ 'feedbacks'] = 'Missatges de resposta correcta';
$string[ 'repetitions'] = 'Repeticions';

//tabs.php
$string[ 'export_to_html'] = 'Exportar a HTML';
$string[ 'export_to_javame'] = 'Exportar a Javame';
$string[ 'info'] = 'Info';
$string[ 'results'] = 'Resultats';
$string[ 'showanswers'] = 'Mostrar respostes';
$string[ 'showattempts'] = 'Mostrar els intents';

//view.php
$string[ 'attemptgamenow'] = 'Intentar jugar ara';
$string[ 'continueattemptgame'] = 'Continueu un intent previ de joc';
$string[ 'gradesofar'] = '$a->method: $a->mygrade / $a->gamegrade.';
$string[ 'marks'] = 'Marques';
$string[ 'nomoreattempts'] = 'No es permeten més intents';
$string[ 'reattemptgame'] = 'Joc de reintent';
$string[ 'yourfinalgradeis'] = 'La seva nota fina en aquest joc és $a.';

//Untranslated
//cross/play.php $string[ 'win'] = 'Congratulations !!!!';
//db/access.php $string[ 'game:grade'] = 'Grade games manually';
//db/access.php $string[ 'game:preview'] = 'Preview Games';
//hiddenpicture/play.php $string[ 'no_questions'] = "There are no questions";
//snakes/play.php $string[ 'snakes_dice'] = 'Dice, $a spots.';
//snakes/play.php $string[ 'snakes_player'] = 'Player, position: $a.';
//attempt.php $string[ 'gameclosed'] = 'This game closed on {$a}';
//attempt.php $string[ 'gamenotavailable'] = 'The game will not be available until {$a}';
//exporthtml_snakes.php $string[ 'html_snakes_check'] = 'Check';
//exporthtml_snakes.php $string[ 'html_snakes_correct'] = 'Correct!';
//exporthtml_snakes.php $string[ 'html_snakes_no_selection'] = 'Have to select something!';
//exporthtml_snakes.php $string[ 'html_snakes_wrong'] = "Your answer isn't correct. Stay on the same seat.";
//lib.php $string[ 'reset_game_all'] = 'Delete tries from all games';
//lib.php $string[ 'reset_game_deleted_course'] = 'Delete tries from deleted courses';
//mod_form.php $string[ 'gameclose'] = 'Close the game';
//mod_form.php $string[ 'gameopen'] = 'Open the game';
//mod_form.php $string[ 'hangman_maximum_number_of_errors'] = 'Maximum number or errors (have to be images named hangman_0.jpg, hangman_1.jpg, ...)';
//mod_form.php $string[ 'snakes_file'] = 'File for background';
//view.php $string[ 'gamecloses'] = 'Game closes';
//view.php $string[ 'gameopens'] = 'Game opens';
