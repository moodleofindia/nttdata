<?php // $Id: game.php,v 1.5.2.10 2011/08/27 19:40:30 bdaloukas Exp $ 
      // game.php - created with Moodle 1.6.3
      // translated by Ивченко Анатолий

//bookquiz/importodt.php
$string[ 'bookquiz_import_odt'] = 'Импортировать из файла OpenOffice(odt)';
$string[ 'bookquiz_not_select_book'] = 'You have not select book';
$string[ 'bookquiz_subchapter'] = 'Создать подглавы';

//bookquiz/play.php
$string[ 'bookquiz_empty'] = 'Книга пуста';
$string[ 'sudoku_submit'] = 'Оценить ответы';

//bookquiz/questions.php
$string[ 'bookquiz_categories'] = 'Категории';
$string[ 'bookquiz_chapters'] = 'Главы';
$string[ 'bookquiz_questions'] = 'Связать категории вопросов с главой книги';

//cross/cross_class.php
$string[ 'lettersall'] = 'АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЬЫЪЭЮЯ';

//cross/crossdb_class.php
$string[ 'and'] = 'и';
$string[ 'cross_correct'] = 'правильный';
$string[ 'cross_corrects'] = 'правильные';
$string[ 'cross_error'] = 'ошибка';
$string[ 'cross_errors'] = 'ошибки';
$string[ 'cross_found_many'] = 'Найденные';
$string[ 'cross_found_one'] = 'Найденный';
$string[ 'grade'] = 'Оценка';

//cross/play.php
$string[ 'cross_across'] = 'По горизонтали';
$string[ 'cross_checkbutton'] = 'Проверить кроссворд';
$string[ 'cross_down'] = 'По вертикали';
$string[ 'cross_endofgamebutton'] = 'Окончание игры \"Кроссворд\"';
$string[ 'cross_error_containsbadchars'] = 'Слово содержит недопустимые символы';
$string[ 'cross_error_wordlength1'] = 'Правильное слово содержит ';
$string[ 'cross_error_wordlength2'] = ' буквы.';
$string[ 'cross_pleasewait'] = 'Пожалуйста, обождите, кроссворд загружается';
$string[ 'cross_welcome'] = '<h2>Добро пожаловать!</h2><p>Сначала щелкните по любой из клеток, содержащих слово, затем введите в появившемся окне ответ.</p>';
$string[ 'finish'] = 'Конец игры';
$string[ 'letter'] = 'буква';
$string[ 'letters'] = 'буквы';
$string[ 'nextgame'] = 'Новая игра';
$string[ 'no_words'] = 'Не найдены слова';
$string[ 'win'] = 'Поздравляем !!!';

//db/access.php

//hangman/play.php
$string[ 'hangman_correct_phrase'] = 'Правильная фраза: ';
$string[ 'hangman_correct_word'] = 'Правильное слово: ';
$string[ 'hangman_gradeinstance'] = 'Оценить всю игру';
$string[ 'hangman_letters'] = 'Буквы: ';
$string[ 'hangman_restletters_many'] = 'У Вас <b>$a</b> попыток';
$string[ 'hangman_restletters_one'] = 'У Вас <b>только 1</b> попытка';
$string[ 'hangman_wrongnum'] = 'Неправильно: %%d из %%d';
$string[ 'nextword'] = 'Следующее слово';

//hiddenpicture/play.php
$string[ 'hiddenpicture_mainsubmit'] = 'Оценить главный ответ';
$string[ 'hiddenpicture_nocols'] = 'Необходимо указать количество колонок по горизонтали';
$string[ 'hiddenpicture_norows'] = 'Необходимо указать количество срок по вертикали';
$string[ 'must_select_glossary'] = 'Вы должны выбрать глоссарий';

//millionaire/play.php
$string[ 'millionaire_must_select_questioncategory'] = 'Вы должны выбрать одну из категорий вопросов';
$string[ 'millionaire_must_select_quiz'] = 'Вы должны выбрать тест';

//report/overview/report.php
$string[ 'allattempts'] = 'Все попытки';
$string[ 'allstudents'] = 'Все студенты $a';
$string[ 'attemptsonly'] = 'Показать только студентов с попытками';
$string[ 'deleteattemptcheck'] = 'Вы уверены, что хотите полностью удалить эти попытки?';
$string[ 'displayoptions'] = 'Отобразить дополнительно';
$string[ 'downloadods'] = 'Загрузить в формате ODS';
$string[ 'feedback'] = 'Комментарий';
$string[ 'noattemptsonly'] = 'Показать $a только, у которых нет попыток';
$string[ 'numattempts'] = '$a->studentnum $a->studentstring сделали $a->attemptnum попыток';
$string[ 'overview'] = 'Обзор';
$string[ 'pagesize'] = 'Вопросов на страницу:';
$string[ 'selectall'] = 'Выбрать все';
$string[ 'selectnone'] = 'Снять выделение со всех';
$string[ 'showdetailedmarks'] = 'Показать детали оценок';
$string[ 'startedon'] = 'Начато';
$string[ 'timecompleted'] = 'Выполнено';
$string[ 'timetaken'] = 'Время выполнения';
$string[ 'unfinished'] = 'открыто';
$string[ 'withselected'] = 'С выделенным';

//snakes/play.php

//sudoku/create.php
$string[ 'sudoku_create_count'] = 'Количество созданных \"Судоку\"';
$string[ 'sudoku_create_start'] = 'Начало создания \"Судоку\"';
$string[ 'sudoku_creating'] = 'Создание <b>$a</b> \"Судоку\"';

//sudoku/play.php
$string[ 'sudoku_finishattemptbutton'] = 'Окончание игры \"Судоку\"';
$string[ 'sudoku_guessnumber'] = 'Введите правильное число';
$string[ 'sudoku_noentriesfound'] = 'В глоссарии нет слов';

//attempt.php

//backuplib.php
$string[ 'modulenameplural'] = 'Игры';

//export.php
$string[ 'only_teachers'] = 'Только учителя могут просматривать эту страницу';

//exporthtml_hangman.php
$string[ 'hangman_loose'] = '<BIG><B>Игра окончена</B></BIG>';

//exporthtml_millionaire.php
$string[ 'millionaire_info_people'] = 'Люди говорят';
$string[ 'millionaire_info_telephone'] = 'Я думаю, что правильный ответ ';
$string[ 'millionaire_info_wrong_answer'] = 'Ваш ответ неверный<br>Правильный ответ:';
$string[ 'millionaire_sourcemodule_must_quiz_question'] = 'Для игры Миллионер источником вопросов должен быть $a, а не';
$string[ 'must_select_questioncategory'] = 'Вы должны выбрать категорию вопросов';
$string[ 'must_select_quiz'] = 'Вы должны выбрать тест';

//exporthtml_snakes.php
$string[ 'score'] = 'Очки';

//header.php
$string[ 'modulename'] = 'Игра';

//index.php
$string[ 'attempts'] = 'Попытки';

//lib.php
$string[ 'attempt'] = 'Попытка $a';
$string[ 'game_bookquiz'] = 'Книга с вопросами';
$string[ 'game_cross'] = 'Кроссворд';
$string[ 'game_cryptex'] = 'Криптекст';
$string[ 'game_hangman'] = 'Виселица';
$string[ 'game_hiddenpicture'] = 'Спрятанная картинка';
$string[ 'game_millionaire'] = 'Миллионер';
$string[ 'game_snakes'] = 'Змеи и лестницы';
$string[ 'game_sudoku'] = 'Судоку';
$string[ 'noattempts'] = 'В этом тесте не сделано попыток';

//locallib.php
$string[ 'attemptfirst'] = 'Первая попытка';
$string[ 'attemptlast'] = 'Последняя попытка';
$string[ 'gradeaverage'] = 'Оценивание по среднему баллу';
$string[ 'gradehighest'] = 'Оценивание по высшему баллу';

//mod.html
$string[ 'bottomtext'] = 'Текст внизу экрана игры';
$string[ 'cross_layout'] = 'Слой';
$string[ 'cross_layout0'] = 'Расшифровка внизу кроссворда';
$string[ 'cross_layout1'] = 'Расшифровка справа от кроссворда';
$string[ 'cross_maxcols'] = 'Максимальное число колонок в кроссворде';
$string[ 'cross_maxwords'] = 'Максимальное число слов в кроссворде';
$string[ 'cryptex_maxcols'] = 'Макимальное число колонок/рядов в Криптексте';
$string[ 'cryptex_maxwords'] = 'Макимальное число слов в Криптексте';
$string[ 'grademethod'] = 'Метод оценивания';
$string[ 'hangman_allowspaces'] = 'Разрешить в словах пробелы';
$string[ 'hangman_allowsub'] = 'Разрешить дефис в словах';
$string[ 'hangman_imageset'] = 'Выберите изображение виселицы';
$string[ 'hangman_language'] = 'Язык слов';
$string[ 'hangman_maxtries'] = 'Число попыток в игре';
$string[ 'hangman_showcorrectanswer'] = 'Показать правильные ответы после окончания игры';
$string[ 'hangman_showfirst'] = 'Показать первую букву';
$string[ 'hangman_showlast'] = 'Показать последнюю букву';
$string[ 'hangman_showquestion'] = 'Показать вопросы?';
$string[ 'hiddenpicture_across'] = 'Клетки по горизонтали';
$string[ 'hiddenpicture_down'] = 'Клетки по вертикали';
$string[ 'hiddenpicture_pictureglossary'] = 'Глоссарий для главного вопроса';
$string[ 'hiddenpicture_pictureglossarycategories'] = 'Категория для главного вопроса';
$string[ 'selectgame'] = 'Выберите игру';
$string[ 'snakes_background'] = 'Фон';
$string[ 'sourcemodule'] = 'Источник вопросов';
$string[ 'sourcemodule_book'] = 'Выберите книгу';
$string[ 'sourcemodule_glossary'] = 'Выберите глоссарий';
$string[ 'sourcemodule_glossarycategory'] = 'Выберите категорию глоссария';
$string[ 'sourcemodule_question'] = 'Вопросы';
$string[ 'sourcemodule_questioncategory'] = 'Выберите категорию вопросов';
$string[ 'sourcemodule_quiz'] = 'Выберите тест';
$string[ 'sudoku_maxquestions'] = 'Максимальное число вопросов';

//mod_form.php

//preview.php
$string[ 'formatdatetime'] = '%%d %%b %%Y, %%I:%%M %%p';
$string[ 'lastip'] = 'IP студента';
$string[ 'preview'] = 'Предварительный просмотр';
$string[ 'showsolution'] = 'решение';
$string[ 'timefinish'] = 'Конец игры';
$string[ 'timelastattempt'] = 'Последняя попытка';
$string[ 'timestart'] = 'Начало';

//review.php
$string[ 'completedon'] = 'Выполнено';
$string[ 'outof'] = '$a->grade out of a maximum of $a->maxgrade';
$string[ 'review'] = 'Повторный просмотр';
$string[ 'reviewofattempt'] = 'Повторный промотр попыток $a';
$string[ 'startagain'] = 'Начать снова';

//showanswers.php
$string[ 'feedbacks'] = 'Текст сообщения о правильном ответе';

//tabs.php
$string[ 'info'] = 'Информация';
$string[ 'results'] = 'Результаты';
$string[ 'showanswers'] = 'Показать ответы';

//view.php
$string[ 'attemptgamenow'] = 'Начать новую попытку игры';
$string[ 'continueattemptgame'] = 'Продолжить предыдущую попытку этой игры';
$string[ 'gradesofar'] = '$a->method: $a->mygrade / $a->gamegrade.';
$string[ 'marks'] = 'Оценки';
$string[ 'nomoreattempts'] = 'Больше попытки не разрешены';
$string[ 'reattemptgame'] = 'Сделать попытку в этой игре';
$string[ 'yourfinalgradeis'] = 'Ваша последняя оценка за эту игру: $a.';

//Untranslated
//cross/play.php $string[ 'print'] = 'Print';
//db/access.php $string[ 'game:attempt'] = 'Play games';
//db/access.php $string[ 'game:deleteattempts'] = 'Delete attempts';
//db/access.php $string[ 'game:grade'] = 'Grade games manually';
//db/access.php $string[ 'game:manage'] = 'Manage';
//db/access.php $string[ 'game:preview'] = 'Preview Games';
//db/access.php $string[ 'game:reviewmyattempts'] = 'reviewmyattempts';
//db/access.php $string[ 'game:view'] = 'View game information';
//db/access.php $string[ 'game:viewreports'] = 'View game reports';
//hiddenpicture/play.php $string[ 'hiddenpicture_nomainquestion'] = 'There are no glossary entries in glossary $a->name with an attached picture';
//hiddenpicture/play.php $string[ 'no_questions'] = "There are no questions";
//hiddenpicture/play.php $string[ 'noglossaryentriesfound'] = 'No glossary entries found';
//snakes/play.php $string[ 'snakes_dice'] = 'Dice, $a spots.';
//snakes/play.php $string[ 'snakes_player'] = 'Player, position: $a.';
//attempt.php $string[ 'gameclosed'] = 'This game closed on {$a}';
//attempt.php $string[ 'gamenotavailable'] = 'The game will not be available until {$a}';
//export.php $string[ 'export'] = 'Export';
//export.php $string[ 'html_hascheckbutton'] = 'Include check button:';
//export.php $string[ 'html_hasprintbutton'] = 'Include print button:';
//export.php $string[ 'html_title'] = 'Title of html:';
//export.php $string[ 'javame_createdby'] = 'Created by:';
//export.php $string[ 'javame_description'] = 'Description:';
//export.php $string[ 'javame_filename'] = 'Filename:';
//export.php $string[ 'javame_icon'] = 'Icon:';
//export.php $string[ 'javame_maxpictureheight'] = 'Max picture height:';
//export.php $string[ 'javame_maxpicturewidth'] = 'Max picture width:';
//export.php $string[ 'javame_name'] = 'Name:';
//export.php $string[ 'javame_type'] = 'Type:';
//export.php $string[ 'javame_vendor'] = 'Vendor:';
//export.php $string[ 'javame_version'] = 'Version:';
//exporthtml_hangman.php $string[ 'html_hangman_new'] = 'New';
//exporthtml_millionaire.php $string[ 'millionaire_helppeople'] = 'Ask the audience';
//exporthtml_millionaire.php $string[ 'millionaire_quit'] = 'Quit';
//exporthtml_millionaire.php $string[ 'millionaire_telephone'] = 'Call a friend';
//exporthtml_snakes.php $string[ 'html_snakes_check'] = 'Check';
//exporthtml_snakes.php $string[ 'html_snakes_correct'] = 'Correct!';
//exporthtml_snakes.php $string[ 'html_snakes_no_selection'] = 'Have to select something!';
//exporthtml_snakes.php $string[ 'html_snakes_wrong'] = "Your answer isn't correct. Stay on the same seat.";
//lib.php $string[ 'percent'] = 'Percent';
//lib.php $string[ 'reset_game_all'] = 'Delete tries from all games';
//lib.php $string[ 'reset_game_deleted_course'] = 'Delete tries from deleted courses';
//mod.html $string[ 'cryptex_maxtries'] = 'Maximum tries';
//mod.html $string[ 'hiddenpicture_height'] = 'Set height of picture to';
//mod.html $string[ 'hiddenpicture_width'] = 'Set width of picture to';
//mod.html $string[ 'millionaire_background'] = 'Background color';
//mod.html $string[ 'millionaire_shuffle'] = 'Randomize questions';
//mod.html $string[ 'sourcemodule_include_subcategories'] = 'Include subcategories';
//mod.html $string[ 'toptext'] = 'Text at the top';
//mod_form.php $string[ 'cross_options'] = 'Crossword options';
//mod_form.php $string[ 'cryptex_options'] = 'Cryptex options';
//mod_form.php $string[ 'gameclose'] = 'Close the game';
//mod_form.php $string[ 'gameopen'] = 'Open the game';
//mod_form.php $string[ 'hangman_maximum_number_of_errors'] = 'Maximum number or errors (have to be images named hangman_0.jpg, hangman_1.jpg, ...)';
//mod_form.php $string[ 'hangman_options'] = 'Hangman options';
//mod_form.php $string[ 'headerfooter_options'] = 'Header/Footer options';
//mod_form.php $string[ 'hiddenpicture_options'] = '\'Hidden Picture\' options';
//mod_form.php $string[ 'millionaire_options'] = 'Millionaire options';
//mod_form.php $string[ 'snakes_board'] = 'Board';
//mod_form.php $string[ 'snakes_cols'] = 'Cols';
//mod_form.php $string[ 'snakes_file'] = 'File for background';
//mod_form.php $string[ 'snakes_footerx'] = 'Space at bootom left';
//mod_form.php $string[ 'snakes_footery'] = 'Space at bottom right';
//mod_form.php $string[ 'snakes_headerx'] = 'Space at up left';
//mod_form.php $string[ 'snakes_headery'] = 'Space at up right';
//mod_form.php $string[ 'snakes_options'] = '\'Snakes and Ladders\' options';
//mod_form.php $string[ 'snakes_rows'] = 'Rows';
//mod_form.php $string[ 'sudoku_options'] = 'Sudoku options';
//mod_form.php $string[ 'userdefined'] = "User defined ";
//showanswers.php $string[ 'clearrepetitions'] = 'Clear statistics';
//showanswers.php $string[ 'computerepetitions'] = 'Compute statistics again';
//showanswers.php $string[ 'repetitions'] = 'Repetitions';
//tabs.php $string[ 'export_to_html'] = 'Export to HTML';
//tabs.php $string[ 'export_to_javame'] = 'Export to Javame';
//tabs.php $string[ 'showattempts'] = 'Show attempts';
//view.php $string[ 'gamecloses'] = 'Game closes';
//view.php $string[ 'gameopens'] = 'Game opens';
