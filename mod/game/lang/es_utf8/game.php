<?php // $Id: game.php,v 1.7.2.11 2011/08/27 19:40:29 bdaloukas Exp $ 
      // game.php - created with Moodle 1.9.1+ (Build: 20080702) (2007101514)
      // translation to es. Thanks to Wilmack Sanchez, Antonio Vicent, Juan Ezeiza, Carolina Avila

//bookquiz/importodt.php
$string[ 'bookquiz_import_odt'] = 'Importar de archivo OpenOffice (odt)';
$string[ 'bookquiz_not_select_book'] = 'Ud no ha seleccionado un libro';
$string[ 'bookquiz_subchapter'] = 'Cree subcapítulos';

//bookquiz/play.php
$string[ 'bookquiz_empty'] = 'El libro está vacío';
$string[ 'sudoku_submit'] = 'Evaluar respuestas';

//bookquiz/questions.php
$string[ 'bookquiz_categories'] = 'Categorías';
$string[ 'bookquiz_chapters'] = 'Capítulos';
$string[ 'bookquiz_questions'] = 'Asocie categorías de preguntas con capítulos del libro';

//cross/cross_class.php
$string[ 'lettersall'] = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

//cross/crossdb_class.php
$string[ 'and'] = 'y';
$string[ 'cross_correct'] = 'correcta';
$string[ 'cross_corrects'] = 'correctas';
$string[ 'cross_error'] = 'error';
$string[ 'cross_errors'] = 'errores';
$string[ 'cross_found_many'] = 'Encontró';
$string[ 'cross_found_one'] = 'Encontró';
$string[ 'grade'] = 'Nota';

//cross/play.php
$string[ 'cross_across'] = 'Horizontal';
$string[ 'cross_checkbutton'] = 'Revisar crucigrama';
$string[ 'cross_down'] = 'Abajo';
$string[ 'cross_endofgamebutton'] = 'Fin del juego de crucigrama';
$string[ 'cross_error_containsbadchars'] = 'La palabra contiene caracteres no autorizados';
$string[ 'cross_error_wordlength1'] = 'La palabra correcta contiene ';
$string[ 'cross_error_wordlength2'] = ' letras.';
$string[ 'cross_pleasewait'] = 'Por favor espere mientras se carga el crucigrama';
$string[ 'cross_welcome'] = '<h3>¡Bienvenido!</h3><p>Haga clic en una palabra para comenzar.</p>';
$string[ 'finish'] = 'Fin del juego';
$string[ 'letter'] = 'letra';
$string[ 'letters'] = 'letras';
$string[ 'nextgame'] = 'Siguiente juego';
$string[ 'no_words'] = 'Ninguna palabra encontrada';
$string[ 'print'] = 'Imprimir';
$string[ 'win'] = '¡¡¡ Felicitaciones !!!';

//db/access.php
$string[ 'game:attempt'] = 'Juega el juego';
$string[ 'game:deleteattempts'] = 'Borrar intentos';
$string[ 'game:manage'] = 'Administrar';
$string[ 'game:reviewmyattempts'] = 'Revisar mis intentos';
$string[ 'game:view'] = 'Ver';
$string[ 'game:viewreports'] = 'Ver reportes';

//hangman/play.php
$string[ 'hangman_correct_phrase'] = 'La frase correcta fué: ';
$string[ 'hangman_correct_word'] = 'La palabra correcta fué: ';
$string[ 'hangman_gradeinstance'] = 'Nivel en el juego completo';
$string[ 'hangman_letters'] = 'Letras: ';
$string[ 'hangman_restletters_many'] = 'Ud. tiene <b>$a</b> intentos';
$string[ 'hangman_restletters_one'] = 'Ud. tienen <b>ÚNICAMENTE 1</b> intento';
$string[ 'hangman_wrongnum'] = 'Malas: %%d de %%d';
$string[ 'nextword'] = 'Siguiente palabra';

//hiddenpicture/play.php
$string[ 'hiddenpicture_mainsubmit'] = 'Calificación de la pregunta principal';
$string[ 'hiddenpicture_nocols'] = 'Tiene que especificar el número de filas horizontales';
$string[ 'hiddenpicture_nomainquestion'] = 'No hay entradas en el glosario glosario $a->name con una imagen adjunta';
$string[ 'hiddenpicture_norows'] = 'Tiene que especificar el número de columnas verticales';
$string[ 'must_select_glossary'] = 'Ud debe seleccionar un glosario';
$string[ 'noglossaryentriesfound'] = 'No se han encontrado entradas de glosario';

//millionaire/play.php
$string[ 'millionaire_must_select_questioncategory'] = 'Usted debe seleccionar una categoría de preguntas';
$string[ 'millionaire_must_select_quiz'] = 'Usted debe seleccionar un cuestionario';

//report/overview/report.php
$string[ 'allattempts'] = 'Mostrar todos los intentos';
$string[ 'allstudents'] = 'Mostrar todos $a';
$string[ 'attemptsonly'] = 'Mostrar únicamente estudiantes con intentos';
$string[ 'deleteattemptcheck'] = 'Esta absolútamente seguro de querer borrar completamente estos intentos?';
$string[ 'displayoptions'] = 'Mostrar opciones';
$string[ 'downloadods'] = 'Descargar en formato ODS';
$string[ 'feedback'] = 'Retroalimentación';
$string[ 'noattemptsonly'] = 'Mostrar $a unicamente sin intentos';
$string[ 'numattempts'] = '$a->studentnum $a->studentstring ha hecho $a->attemptnum intentos';
$string[ 'overview'] = 'Listado';
$string[ 'pagesize'] = 'Preguntas por página:';
$string[ 'selectall'] = 'Seleccione todos';
$string[ 'selectnone'] = 'Des-marcar todos';
$string[ 'showdetailedmarks'] = 'Mostrar detalles de marca';
$string[ 'startedon'] = 'Comenzó en';
$string[ 'timecompleted'] = 'Completado';
$string[ 'timetaken'] = 'Tiempo utilizado';
$string[ 'unfinished'] = 'abierto';
$string[ 'withselected'] = 'Con seleccionados';

//snakes/play.php

//sudoku/create.php
$string[ 'sudoku_create_count'] = 'Número de sudokus que serán creados';
$string[ 'sudoku_create_start'] = 'Comenzar creando sudokus';
$string[ 'sudoku_creating'] = 'Creando <b>$a</b> sudoku';

//sudoku/play.php
$string[ 'sudoku_finishattemptbutton'] = 'Fin del juego de sudoku';
$string[ 'sudoku_guessnumber'] = 'Adivine el número correcto';
$string[ 'sudoku_noentriesfound'] = 'Ninguna palabra encontrada en el glosario';

//attempt.php

//backuplib.php
$string[ 'modulenameplural'] = 'Juegos';

//export.php
$string[ 'export'] = 'Exporta a móvil';
$string[ 'html_hascheckbutton'] = 'Botón de prueba:';
$string[ 'html_hasprintbutton'] = 'Botón de impresión:';
$string[ 'html_title'] = 'Título de html:';
$string[ 'javame_createdby'] = 'Creado por:';
$string[ 'javame_description'] = 'Descripción:';
$string[ 'javame_filename'] = 'Nombe del archivo:';
$string[ 'javame_icon'] = 'Icono:';
$string[ 'javame_maxpictureheight'] = 'Altura máxima de la imagen:';
$string[ 'javame_maxpicturewidth'] = 'Anchura máxima de la imagen:';
$string[ 'javame_name'] = 'Nombre:';
$string[ 'javame_type'] = 'Tipo:';
$string[ 'javame_vendor'] = 'Vendedor:';
$string[ 'javame_version'] = 'Versión';
$string[ 'only_teachers'] = 'Sólo el profesor puede ver esta página';

//exporthtml_hangman.php
$string[ 'hangman_loose'] = '<BIG><B>Fin del juego</B></BIG>';
$string[ 'html_hangman_new'] = 'Nuevo';

//exporthtml_millionaire.php
$string[ 'millionaire_helppeople'] = 'Ayuda del público';
$string[ 'millionaire_info_people'] = 'La gente dice';
$string[ 'millionaire_info_telephone'] = 'Yo pienso que la respuesta correcta es';
$string[ 'millionaire_info_wrong_answer'] = 'Su respuesta es incorrecta<br>La respuesta correcta es:';
$string[ 'millionaire_quit'] = 'Salir';
$string[ 'millionaire_sourcemodule_must_quiz_question'] = 'Para millonario la fuente debe ser $a o preguntas y no';
$string[ 'millionaire_telephone'] = 'Llamada telefónica';
$string[ 'must_select_questioncategory'] = 'Ud debe seleccionar una categoría de preguntas';
$string[ 'must_select_quiz'] = 'Ud debe seleccionar un cuestionario';

//exporthtml_snakes.php
$string[ 'score'] = 'Puntaje';

//header.php
$string[ 'modulename'] = 'Juego';

//index.php
$string[ 'attempts'] = 'Intentos';

//lib.php
$string[ 'attempt'] = 'Intento $a';
$string[ 'game_bookquiz'] = 'Libro con preguntas';
$string[ 'game_cross'] = 'Crucigrama';
$string[ 'game_cryptex'] = 'Sopa de Letras';
$string[ 'game_hangman'] = 'Ahorcado';
$string[ 'game_hiddenpicture'] = 'Imagen oculta';
$string[ 'game_millionaire'] = 'Millonario';
$string[ 'game_snakes'] = 'Serpientes y Escaleras';
$string[ 'game_sudoku'] = 'Sudoku';
$string[ 'noattempts'] = 'Ningún intento ha sido hecho en este cuestionario';
$string[ 'percent'] = 'Porcentaje';

//locallib.php
$string[ 'attemptfirst'] = 'Primer intento';
$string[ 'attemptlast'] = 'Último intento';
$string[ 'gradeaverage'] = 'Nota promedio';
$string[ 'gradehighest'] = 'Nota más alta';

//mod.html
$string[ 'bottomtext'] = 'Texto al final';
$string[ 'cross_layout'] = 'Diseño';
$string[ 'cross_layout0'] = 'Frases en la parte inferior del crucigrama';
$string[ 'cross_layout1'] = 'Frases en la parte derecha del crucigrama';
$string[ 'cross_maxcols'] = 'Número máximo de columnas del crucigrama';
$string[ 'cross_maxwords'] = 'Máximo número de palabras del crucigrama';
$string[ 'cryptex_maxcols'] = 'Máximo número de columnas/filas en Sopa de Letras';
$string[ 'cryptex_maxtries'] = 'Número máximo de intentos';
$string[ 'cryptex_maxwords'] = 'Máximo número de palabras en Sopa de Letras';
$string[ 'grademethod'] = 'Método de calificación';
$string[ 'hangman_allowspaces'] = 'Permitir espacios en las palabras';
$string[ 'hangman_allowsub'] = 'Permitir símbolos en las palabras';
$string[ 'hangman_imageset'] = 'Seleccione las imágenes para el ahorcado';
$string[ 'hangman_language'] = 'Idioma de las palabras';
$string[ 'hangman_maxtries'] = 'Número de palabras por juego';
$string[ 'hangman_showcorrectanswer'] = 'Mostrar la respuesta correcta después del final';
$string[ 'hangman_showfirst'] = 'Mostrar la primera letra de ahorcado';
$string[ 'hangman_showlast'] = 'Mostrar la última letra del ahorcado';
$string[ 'hangman_showquestion'] = '¿ Mostrar las preguntas ?';
$string[ 'hiddenpicture_across'] = 'Celdas horizontales';
$string[ 'hiddenpicture_down'] = 'Celdas verticales';
$string[ 'hiddenpicture_height'] = 'Establecer la altura de la imagen en';
$string[ 'hiddenpicture_pictureglossary'] = 'El glosario para la cuestión principal';
$string[ 'hiddenpicture_pictureglossarycategories'] = 'La categoría de glosario para la cuestión principal';
$string[ 'hiddenpicture_width'] = 'Establecer el ancho de la imagen en';
$string[ 'millionaire_background'] = 'Color de fondo';
$string[ 'millionaire_shuffle'] = 'Mezclar preguntas';
$string[ 'selectgame'] = 'Seleccionar juego';
$string[ 'snakes_background'] = 'Fondo';
$string[ 'sourcemodule'] = 'Fuente de preguntas';
$string[ 'sourcemodule_book'] = 'Seleccione un libro';
$string[ 'sourcemodule_glossary'] = 'Seleccione un glosario';
$string[ 'sourcemodule_glossarycategory'] = 'Seleccione una categoría del glosario.';
$string[ 'sourcemodule_include_subcategories'] = 'Include subcategories';
$string[ 'sourcemodule_question'] = 'Preguntas';
$string[ 'sourcemodule_questioncategory'] = 'Seleccione una categoría de preguntas';
$string[ 'sourcemodule_quiz'] = 'Seleccione un cuestionario';
$string[ 'sudoku_maxquestions'] = 'Máximo número de preguntas';
$string[ 'toptext'] = 'Texto de la parte superior';

//mod_form.php
$string[ 'cross_options'] = 'Opciones del Crucigrama';
$string[ 'cryptex_options'] = 'Opciones del Criptograma';
$string[ 'hangman_options'] = 'Opciones del Ahorcado';
$string[ 'headerfooter_options'] = 'Opciones de Encabezado/Pie de página';
$string[ 'hiddenpicture_options'] = '\'Imagen Oculta\' opciones';
$string[ 'millionaire_options'] = 'Opciones del Millonario';
$string[ 'snakes_board'] = 'Tablero';
$string[ 'snakes_cols'] = 'Columnas';
$string[ 'snakes_footerx'] = 'Espacio inferior izquierdo';
$string[ 'snakes_footery'] = 'Espacio inferior derecho';
$string[ 'snakes_headerx'] = 'Espacio superior izquierdo';
$string[ 'snakes_headery'] = 'Espacio superior derecho';
$string[ 'snakes_options'] = '\'Serpientes y Escaleras\' opciones';
$string[ 'snakes_rows'] = 'Filas';
$string[ 'sudoku_options'] = 'Opciones del Sudoku';
$string[ 'userdefined'] = 'Definido por el usuario';

//preview.php
$string[ 'formatdatetime'] = '%%d %%b %%Y, %%I:%%M %%p';
$string[ 'lastip'] = 'IP del estudiante';
$string[ 'preview'] = 'Visualizar';
$string[ 'showsolution'] = 'solución';
$string[ 'timefinish'] = 'Fin del juego';
$string[ 'timelastattempt'] = 'Último intento';
$string[ 'timestart'] = 'Comienzo';

//review.php
$string[ 'completedon'] = 'Completado en';
$string[ 'outof'] = '$a->grade out of a maximum of $a->maxgrade';
$string[ 'review'] = 'Revisar';
$string[ 'reviewofattempt'] = 'Revisar intentos $a';
$string[ 'startagain'] = 'Comenzar de nuevo';

//showanswers.php
$string[ 'clearrepetitions'] = 'Borrar estadísticas';
$string[ 'computerepetitions'] = 'Recalcular estadísticas';
$string[ 'feedbacks'] = 'Mensajes de respuesta correcta';
$string[ 'repetitions'] = 'Repeticiones';

//tabs.php
$string[ 'export_to_html'] = 'Exportar a HTML';
$string[ 'export_to_javame'] = 'Exportar a Javame';
$string[ 'info'] = 'Info';
$string[ 'results'] = 'Resultados';
$string[ 'showanswers'] = 'Mostrar respuestas';
$string[ 'showattempts'] = 'Mostrar los intentos';

//view.php
$string[ 'attemptgamenow'] = 'Intente jugar ahora';
$string[ 'continueattemptgame'] = 'Continue un intento previo de juego';
$string[ 'gradesofar'] = '$a->method: $a->mygrade / $a->gamegrade.';
$string[ 'marks'] = 'Marcas';
$string[ 'nomoreattempts'] = 'No se permiten más intentos';
$string[ 'reattemptgame'] = 'Juego de reintento';
$string[ 'yourfinalgradeis'] = 'Su nota final en este juego es $a.';

//Untranslated
//db/access.php $string[ 'game:grade'] = 'Grade games manually';
//db/access.php $string[ 'game:preview'] = 'Preview Games';
//hiddenpicture/play.php $string[ 'no_questions'] = "There are no questions";
//snakes/play.php $string[ 'snakes_dice'] = 'Dice, $a spots.';
//snakes/play.php $string[ 'snakes_player'] = 'Player, position: $a.';
//attempt.php $string[ 'gameclosed'] = 'This game closed on {$a}';
//attempt.php $string[ 'gamenotavailable'] = 'The game will not be available until {$a}';
//exporthtml_snakes.php $string[ 'html_snakes_check'] = 'Check';
//exporthtml_snakes.php $string[ 'html_snakes_correct'] = 'Correct!';
//exporthtml_snakes.php $string[ 'html_snakes_no_selection'] = 'Have to select something!';
//exporthtml_snakes.php $string[ 'html_snakes_wrong'] = "Your answer isn't correct. Stay on the same seat.";
//lib.php $string[ 'reset_game_all'] = 'Delete tries from all games';
//lib.php $string[ 'reset_game_deleted_course'] = 'Delete tries from deleted courses';
//mod_form.php $string[ 'gameclose'] = 'Close the game';
//mod_form.php $string[ 'gameopen'] = 'Open the game';
//mod_form.php $string[ 'hangman_maximum_number_of_errors'] = 'Maximum number or errors (have to be images named hangman_0.jpg, hangman_1.jpg, ...)';
//mod_form.php $string[ 'snakes_file'] = 'File for background';
//view.php $string[ 'gamecloses'] = 'Game closes';
//view.php $string[ 'gameopens'] = 'Game opens';
