<?php // $Id: game.php,v 1.5.2.13 2011/08/27 19:40:28 bdaloukas Exp $
      // translated to Deutsch (de) by Dimitris Mpelos, Joachim Vogelgesang and Klaus Steitz

//bookquiz/importodt.php
$string[ 'bookquiz_import_odt'] = 'OpenOffice Datei hinzufügen (odt)';
$string[ 'bookquiz_not_select_book'] = 'Sie haben kein Buch gewählt!';
$string[ 'bookquiz_subchapter'] = 'Unterkapitel erstellen';

//bookquiz/play.php
$string[ 'bookquiz_empty'] = 'Das Buch ist leer.';
$string[ 'sudoku_submit'] = 'Antworten bewerten';

//bookquiz/questions.php
$string[ 'bookquiz_categories'] = 'Kategorien';
$string[ 'bookquiz_chapters'] = 'Kapitel';
$string[ 'bookquiz_questions'] = 'Fragekategorien mit Buchkapiteln verknüpfen';

//cross/cross_class.php
$string[ 'lettersall'] = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

//cross/crossdb_class.php
$string[ 'and'] = 'und';
$string[ 'cross_correct'] = 'richtig';
$string[ 'cross_corrects'] = 'richtige';
$string[ 'cross_error'] = 'Fehler';
$string[ 'cross_errors'] = 'Fehler';
$string[ 'cross_found_many'] = 'gefunden';
$string[ 'cross_found_one'] = 'gefunden';
$string[ 'grade'] = 'Bewertung';

//cross/play.php
$string[ 'cross_across'] = 'waagerecht';
$string[ 'cross_checkbutton'] = 'Kreuzworträtsel kontrollieren';
$string[ 'cross_down'] = 'senkrecht';
$string[ 'cross_endofgamebutton'] = 'Ende des Kreuzworträtselspiels';
$string[ 'cross_error_containsbadchars'] = 'Das Wort enthält ungültige Symbole';
$string[ 'cross_error_wordlength1'] = 'Das richtige Wort enthält ';
$string[ 'cross_error_wordlength2'] = ' Buchstaben.';
$string[ 'cross_pleasewait'] = 'Bitte warten, Rätsel wird geladen';
$string[ 'cross_welcome'] = '<h3>Willkommen!</h3><p>Klicken Sie auf ein Wort um zu beginnen.</p>';
$string[ 'finish'] = 'Spiel beenden';
$string[ 'letter'] = 'Buchstabe';
$string[ 'letters'] = 'Buchstaben';
$string[ 'nextgame'] = 'Neues Spiel';
$string[ 'no_words'] = 'Keine Wörter gefunden!';
$string[ 'print'] = 'Drucken';
$string[ 'win'] = 'Glückwunsch !!!';

//db/access.php
$string[ 'game:attempt'] = 'Spiel starten';
$string[ 'game:deleteattempts'] = 'Versuche löschen';
$string[ 'game:grade'] = 'Spiele manuell bewerten';
$string[ 'game:manage'] = 'Verwalten';
$string[ 'game:preview'] = 'Vorschau der Spiele';
$string[ 'game:reviewmyattempts'] = 'Meine Versuche wiederholen';
$string[ 'game:view'] = 'ansehen';
$string[ 'game:viewreports'] = 'Berichte ansehen';

//hangman/play.php
$string[ 'hangman_correct_phrase'] = 'Der richtige Satz war: ';
$string[ 'hangman_correct_word'] = 'Das richtige Wort war: ';
$string[ 'hangman_gradeinstance'] = 'Bewertung des gesamten Spiels';
$string[ 'hangman_letters'] = 'Buchstaben: ';
$string[ 'hangman_restletters_many'] = 'Sie haben <b>$a</b> Versuche';
$string[ 'hangman_restletters_one'] = 'Sie haben <b>ONLY 1</b> Versuche';
$string[ 'hangman_wrongnum'] = 'Falsch: %%d von %%d';
$string[ 'nextword'] = 'nächstes Wort';

//hiddenpicture/play.php
$string[ 'hiddenpicture_mainsubmit'] = 'Meine Antwort bewerten';
$string[ 'hiddenpicture_nocols'] = 'Die Anzahl der horizontalen Spalten muss angegeben werden';
$string[ 'hiddenpicture_nomainquestion'] = 'Es gibt keine Wörterbucheinträge im Wörterbuch $a->name mit angefügtem Bild';
$string[ 'hiddenpicture_norows'] = 'Die Anzahl der vertikalen Spalten muss angegeben werden.';
$string[ 'must_select_glossary'] = 'Wähle ein Glossar';
$string[ 'no_questions'] = "Es sind keine Fragen vorhanden";
$string[ 'noglossaryentriesfound'] = 'Keine Wörterbucheinträge gefunden';

//millionaire/play.php
$string[ 'millionaire_must_select_questioncategory'] = 'Sie müssen eine Kategorie wählen';
$string[ 'millionaire_must_select_quiz'] = 'Sie müssen ein Quiz wählen';

//report/overview/report.php
$string[ 'allattempts'] = 'Alle Versuche anzeigen';
$string[ 'allstudents'] = 'Zeige alle $a';
$string[ 'attemptsonly'] = 'Nur Teilnehmer mit Versuchen anzeigen';
$string[ 'deleteattemptcheck'] = 'Sind Sie sicher, dass Sie diese(n) Versuch(e) löschen wollen?';
$string[ 'displayoptions'] = 'Anzeige-Optionen';
$string[ 'downloadods'] = 'Herunterladen im ODS format';
$string[ 'feedback'] = 'Rückmeldung';
$string[ 'noattemptsonly'] = 'Nur $a ohne Versuche anzeigen';
$string[ 'numattempts'] = '$a->studentnum $a->studentstring absolvierte(n) {$a->attemptnum} Versuch(e)';
$string[ 'overview'] = 'Übersicht';
$string[ 'pagesize'] = 'Fragen je Seite:';
$string[ 'selectall'] = 'Alle auswählen';
$string[ 'selectnone'] = 'Alle abwählen';
$string[ 'showdetailedmarks'] = 'Bewertungsdetails anzeigen';
$string[ 'startedon'] = 'Gestartet am';
$string[ 'timecompleted'] = 'Abgeschlossen';
$string[ 'timetaken'] = 'Zeitaufwand';
$string[ 'unfinished'] = 'offen';
$string[ 'withselected'] = 'ausgewählte';

//snakes/play.php
$string[ 'snakes_dice'] = 'Würfle, $a Augen.';
$string[ 'snakes_player'] = 'Spieler, Position: $a.';

//sudoku/create.php
$string[ 'sudoku_create_count'] = 'Anzahl der Sudokus, die erstellt werden';
$string[ 'sudoku_create_start'] = 'Beginne mit dem Erstellen';
$string[ 'sudoku_creating'] = 'Erstelle <b>$a</b> Sudoku';

//sudoku/play.php
$string[ 'sudoku_finishattemptbutton'] = 'Ende des Sudoku Spiels';
$string[ 'sudoku_guessnumber'] = 'Die richtige Nummer erraten';
$string[ 'sudoku_noentriesfound'] = 'Keine Wörter im Wörterbuch gefunden!';

//attempt.php

//backuplib.php
$string[ 'modulenameplural'] = 'Spiele';

//export.php
$string[ 'export'] = 'Export';
$string[ 'html_hascheckbutton'] = 'Prüftaste:';
$string[ 'html_hasprintbutton'] = 'Drucktaste:';
$string[ 'html_title'] = 'Titel der html-Datei (Browser-Titelzeile):';
$string[ 'javame_createdby'] = 'Erstellt von:';
$string[ 'javame_description'] = 'Beschreibung:';
$string[ 'javame_filename'] = 'Dateiname:';
$string[ 'javame_icon'] = 'Icon:';
$string[ 'javame_maxpictureheight'] = 'Größte Bildhöhe:';
$string[ 'javame_maxpicturewidth'] = 'Größte Bildbreite:';
$string[ 'javame_name'] = 'Name:';
$string[ 'javame_type'] = 'Typ:';
$string[ 'javame_vendor'] = 'Anbieter:';
$string[ 'javame_version'] = 'Version:';
$string[ 'only_teachers'] = 'Nur Lehrende können diese Seite sehen';

//exporthtml_hangman.php
$string[ 'hangman_loose'] = '<BIG><B>Sie haben verloren!</B></BIG>';
$string[ 'html_hangman_new'] = 'Neu';

//exporthtml_millionaire.php
$string[ 'millionaire_helppeople'] = 'Publikums-Joker';
$string[ 'millionaire_info_people'] = 'Ergebnis';
$string[ 'millionaire_info_telephone'] = 'Ich glaube, die richtige Antwort ist';
$string[ 'millionaire_info_wrong_answer'] = 'Ihre Antwort ist falsch!<br>Die richtige Antwort ist:';
$string[ 'millionaire_quit'] = 'Spiel beenden';
$string[ 'millionaire_sourcemodule_must_quiz_question'] = 'Für Wer wird Millionär: die Quelle der Fragen muss {$a} oder Fragen sein, und nicht';
$string[ 'millionaire_telephone'] = 'Telefon-Joker';
$string[ 'must_select_questioncategory'] = 'Sie müssen eine Fragenkategorie wählen!';
$string[ 'must_select_quiz'] = 'Sie müssen einen Test auswählen';

//exporthtml_snakes.php
$string[ 'html_snakes_check'] = 'Wähle';
$string[ 'html_snakes_correct'] = 'Richtig!';
$string[ 'html_snakes_no_selection'] = 'Bitte wählen Sie etwas aus';
$string[ 'html_snakes_wrong'] = "Ihre Antwort ist falsch. Sie bleiben auf dem gleichen Feld.";
$string[ 'score'] = 'Bewertung';

//header.php
$string[ 'modulename'] = 'Spiel';

//index.php
$string[ 'attempts'] = 'Versuche';

//lib.php
$string[ 'attempt'] = 'Versuch $a';
$string[ 'game_bookquiz'] = 'Buch mit Fragen';
$string[ 'game_cross'] = 'Kreuzworträtsel';
$string[ 'game_cryptex'] = 'Suchrätsel';
$string[ 'game_hangman'] = 'Galgenmännchen';
$string[ 'game_hiddenpicture'] = 'Verstecktes Bild';
$string[ 'game_millionaire'] = 'Wer wird Millionär';
$string[ 'game_snakes'] = 'Schlangen und Leitern';
$string[ 'game_sudoku'] = 'Sudoku';
$string[ 'noattempts'] = 'Für dieses Spiel wurden keine Versuche gemacht';
$string[ 'percent'] = 'Prozent';
$string[ 'reset_game_all'] = 'Versuche aller Spiele löschen';
$string[ 'reset_game_deleted_course'] = 'Versuche gelöschter Kurse löschen';

//locallib.php
$string[ 'attemptfirst'] = 'Erster Versuch';
$string[ 'attemptlast'] = 'Letzter Versuch';
$string[ 'gradeaverage'] = 'Durchschnittsbewertung';
$string[ 'gradehighest'] = 'Höchste Bewertung';

//mod.html
$string[ 'bottomtext'] = 'Text am Ende der Seite';
$string[ 'cross_layout'] = 'Layout';
$string[ 'cross_layout0'] = 'Sätze auf der Unterseite des Kreuzes';
$string[ 'cross_layout1'] = 'Sätze rechts des Kreuzes';
$string[ 'cross_maxcols'] = 'Höchstzahl an Zeilen des Kreuzworträtsels';
$string[ 'cross_maxwords'] = 'Höchstzahl von Wörtern des Kreuzworträtsels';
$string[ 'cryptex_maxcols'] = 'Höchstzahl an Zeilen des Rätsels';
$string[ 'cryptex_maxtries'] = 'Maximale Versuche';
$string[ 'cryptex_maxwords'] = 'Höchstzahl von Wörtern des Rätsels';
$string[ 'grademethod'] = 'Bewertungsmethode';
$string[ 'hangman_allowspaces'] = 'Wortzwischenräume erlauben';
$string[ 'hangman_allowsub'] = 'Symbol in Wörtern erlauben';
$string[ 'hangman_imageset'] = 'Das Bild des Spiels wählen';
$string[ 'hangman_language'] = 'Wortsprache';
$string[ 'hangman_maxtries'] = 'Wörterzahl pro Spiel';
$string[ 'hangman_showcorrectanswer'] = 'Die richtige Anwort erscheint am Ende des Spiels';
$string[ 'hangman_showfirst'] = 'Ersten Buchstaben des Wortes zeigen';
$string[ 'hangman_showlast'] = 'Letzten Buchstaben des Wortes zeigen';
$string[ 'hangman_showquestion'] = 'Fragen zeigen?';
$string[ 'hiddenpicture_across'] = 'Horizontale Zellen';
$string[ 'hiddenpicture_down'] = 'Vertikale Zellen';
$string[ 'hiddenpicture_height'] = 'Höhe des Bildes';
$string[ 'hiddenpicture_pictureglossary'] = 'Wörterbuch für Hauptfrage und Bild';
$string[ 'hiddenpicture_pictureglossarycategories'] = 'Die Kategorie des Glossars für die Haupt-Frage';
$string[ 'hiddenpicture_width'] = 'Breite des Bildes';
$string[ 'millionaire_background'] = 'Hintergrundfarbe';
$string[ 'millionaire_shuffle'] = 'Zufallsfragen';
$string[ 'selectgame'] = 'Wähle ein Spiel';
$string[ 'snakes_background'] = 'Hintergrund';
$string[ 'sourcemodule'] = 'Fragen-Quelle';
$string[ 'sourcemodule_book'] = 'Ein Buch wählen';
$string[ 'sourcemodule_glossary'] = 'Wörtebuch wählen';
$string[ 'sourcemodule_glossarycategory'] = 'Wörtebuch-Kategorie wählen!';
$string[ 'sourcemodule_include_subcategories'] = 'Unterkategorien einschließen';
$string[ 'sourcemodule_question'] = 'Fragen';
$string[ 'sourcemodule_questioncategory'] = 'Fragenkategorie wählen';
$string[ 'sourcemodule_quiz'] = 'Quiz wählen';
$string[ 'sudoku_maxquestions'] = 'Maximale Anzahl von Fragen erreicht';
$string[ 'toptext'] = 'Text oben';

//mod_form.php
$string[ 'cross_options'] = 'Kreuzworträtsel-Optionen';
$string[ 'cryptex_options'] = 'Rätsel-Optionen';
$string[ 'hangman_maximum_number_of_errors'] = 'Maximale Anzahl von Versuchen (Bilder mit Namensschema hangman_0.jpg, hangman_1.jpg, ...)';
$string[ 'hangman_options'] = 'Galgenmännchen-Optionen';
$string[ 'headerfooter_options'] = 'Kopfzeile/Fußzeile Optionen';
$string[ 'hiddenpicture_options'] = '\'Verstecktes Bild\' Optionen';
$string[ 'millionaire_options'] = 'Millionär-Optionen';
$string[ 'snakes_board'] = 'Spielbrett';
$string[ 'snakes_cols'] = 'Spalten';
$string[ 'snakes_file'] = 'Datei für Hintergrund';
$string[ 'snakes_footerx'] = 'Raum unten links';
$string[ 'snakes_footery'] = 'Raum unten rechts';
$string[ 'snakes_headerx'] = 'Raum nach links oben';
$string[ 'snakes_headery'] = 'Raum nach links unten';
$string[ 'snakes_options'] = '\'Schlangen und Leitern\' Optionen';
$string[ 'snakes_rows'] = 'Reihen';
$string[ 'sudoku_options'] = 'Sudoku-Optionen';
$string[ 'userdefined'] = 'benutzerdefiniert';

//preview.php
$string[ 'formatdatetime'] = '%%d %%b %%Y, %%I:%%M %%p';
$string[ 'lastip'] = 'Benutzer IP';
$string[ 'preview'] = 'Vorschau';
$string[ 'showsolution'] = 'Lösung';
$string[ 'timefinish'] = 'Spiel beendet';
$string[ 'timelastattempt'] = 'Letzter Versuch';
$string[ 'timestart'] = 'Start';

//review.php
$string[ 'completedon'] = 'Beendet am';
$string[ 'outof'] = '$a->grade von maximal $a->maxgrade';
$string[ 'review'] = 'Überprüfung';
$string[ 'reviewofattempt'] = 'Überprüfung eines Versuchs $a';
$string[ 'startagain'] = 'Neu starten';

//showanswers.php
$string[ 'clearrepetitions'] = 'Statistiken löschen';
$string[ 'computerepetitions'] = 'Statistiken neu berechnen';
$string[ 'feedbacks'] = 'Meldungen bei korrekter Antwort';
$string[ 'repetitions'] = 'Wiederholungen';

//tabs.php
$string[ 'export_to_html'] = 'Export nach HTML';
$string[ 'export_to_javame'] = 'Export nach Javame';
$string[ 'info'] = 'Info';
$string[ 'results'] = 'Ergebnisse';
$string[ 'showanswers'] = 'Antworten zeigen';
$string[ 'showattempts'] = 'Versuche anzeigen';

//view.php
$string[ 'attemptgamenow'] = 'Jetzt Spiel starten';
$string[ 'continueattemptgame'] = 'Einen früheren Versuch des Spiels fortführen';
$string[ 'gradesofar'] = '$a->method: $a->mygrade / $a->gamegrade.';
$string[ 'marks'] = 'Markierungen';
$string[ 'nomoreattempts'] = 'Es sind keine weiteren Versuche mehr erlaubt';
$string[ 'reattemptgame'] = 'Spiel nochmals probieren';
$string[ 'yourfinalgradeis'] = 'Ihre Endbewertung für dieses Spiel ist $a.';

//Untranslated
//attempt.php $string[ 'gameclosed'] = 'This game closed on {$a}';
//attempt.php $string[ 'gamenotavailable'] = 'The game will not be available until {$a}';
//mod_form.php $string[ 'gameclose'] = 'Close the game';
//mod_form.php $string[ 'gameopen'] = 'Open the game';
//view.php $string[ 'gamecloses'] = 'Game closes';
//view.php $string[ 'gameopens'] = 'Game opens';
