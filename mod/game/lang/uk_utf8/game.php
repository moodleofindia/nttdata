<?php // $Id: game.php,v 1.5.2.9 2011/08/27 19:40:31 bdaloukas Exp $
      // game.php - created with Moodle 1.9.5
      // translated to uk_utf8 by Anastasiya Kolomoytseva

//bookquiz/importodt.php
$string[ 'bookquiz_import_odt'] = 'Імпорт з файлу OpenOffice (odt)';
$string[ 'bookquiz_not_select_book'] = 'Ви не вибрали книгу';
$string[ 'bookquiz_subchapter'] = 'Створення підрозділів';

//bookquiz/play.php
$string[ 'bookquiz_empty'] = 'Книга порожня';
$string[ 'sudoku_submit'] = 'Оцінювання відповідей';

//bookquiz/questions.php
$string[ 'bookquiz_categories'] = 'Категорія';
$string[ 'bookquiz_chapters'] = 'Заголовна';
$string[ 'bookquiz_questions'] = 'Відповідні категорії питань у розділах книги';

//cross/cross_class.php
$string[ 'lettersall'] = 'АБВГҐДЕЄЖЗИІЇЙКЛМНОПРСТУФХЧШЩЬЮЯ';

//cross/crossdb_class.php
$string[ 'and'] = 'і';
$string[ 'cross_correct'] = 'Вірно';
$string[ 'cross_corrects'] = 'Вірно';
$string[ 'cross_error'] = 'Помилка';
$string[ 'cross_errors'] = 'помилки';
$string[ 'cross_found_many'] = 'Знайшлось';
$string[ 'cross_found_one'] = 'Знайшлось';
$string[ 'grade'] = 'Оцінка';

//cross/play.php
$string[ 'cross_across'] = 'По горизонталі';
$string[ 'cross_checkbutton'] = 'Перевірити кросворд';
$string[ 'cross_down'] = 'По вертикалі';
$string[ 'cross_endofgamebutton'] = 'Кросворд завершено';
$string[ 'cross_error_containsbadchars'] = 'Слово, яке ви набрали містить неприпустимі символи';
$string[ 'cross_error_wordlength1'] = 'Правильне слово містить ';
$string[ 'cross_error_wordlength2'] = ' літери.';
$string[ 'cross_pleasewait'] = 'Будь ласка, зачекайте завантаження кросворду';
$string[ 'cross_welcome'] = '<h3>Ласкаво просимо!</h3><p>Щоб почати, натисніть на слово.</p>';
$string[ 'finish'] = 'Гру закінчено';
$string[ 'letter'] = 'літера';
$string[ 'letters'] = 'літери';
$string[ 'nextgame'] = 'Нова гра';
$string[ 'no_words'] = 'Не знайшлось жодного слова';
$string[ 'print'] = 'Віддрукувати';
$string[ 'win'] = 'Вітання!!!';

//db/access.php

//hangman/play.php
$string[ 'hangman_correct_phrase'] = 'Вірна фраза була: ';
$string[ 'hangman_correct_word'] = 'Вірне слово було: ';
$string[ 'hangman_gradeinstance'] = 'Оцінка протягом усієї гри';
$string[ 'hangman_letters'] = 'Літери: ';
$string[ 'hangman_restletters_many'] = 'Ви маєте ще<b>$a</b> спроб';
$string[ 'hangman_restletters_one'] = 'Ви маєте ще<b>ТІЛЬКИ 1</b> спробу';
$string[ 'hangman_wrongnum'] = 'Не вірно: %%d з %%d';
$string[ 'nextword'] = 'Наступне слово';

//hiddenpicture/play.php
$string[ 'hiddenpicture_mainsubmit'] = "Оцініть питання";
$string[ 'hiddenpicture_nocols'] = 'Ви повинні вказати кількість горизонтальних клітин';
$string[ 'hiddenpicture_nomainquestion'] = 'Не існує жодного запису в словнику $a->name який має зображення';
$string[ 'hiddenpicture_norows'] = 'Ви повинні вказати кількість клітин по вертикалі';
$string[ 'must_select_glossary'] = 'Ви повинні вибрати οπωσδήποτε один словник';

//millionaire/play.php
$string[ 'millionaire_must_select_questioncategory'] = 'Виберіть категорію запитань';
$string[ 'millionaire_must_select_quiz'] = 'Зробіть вибір вікторини';

//report/overview/report.php
$string[ 'allattempts'] = 'Показати всі зусилля';
$string[ 'allstudents'] = 'Показати всі $a';
$string[ 'attemptsonly'] = 'Показати тільки учнів які намагалися';
$string[ 'deleteattemptcheck'] = 'Ви абсолютно впевнені,що хочете,повністю видалити ці спроби?';
$string[ 'displayoptions'] = 'Відображити параметри';
$string[ 'downloadods'] = 'Зберегти як ODS';
$string[ 'feedback'] = 'Відповідь';
$string[ 'noattemptsonly'] = 'Відображити $a які не мають жодної спроби';
$string[ 'numattempts'] = '$a->studentnum $a->studentstring зробили $a->attemptnum спроб';
$string[ 'overview'] = 'Πерегляд';
$string[ 'pagesize'] = 'Питання на сторінці:';
$string[ 'selectall'] = 'Виділити все';
$string[ 'selectnone'] = ' Відмінити все';
$string[ 'showdetailedmarks'] = 'Інформація питань';
$string[ 'startedon'] = 'Гра почалася в ';
$string[ 'timecompleted'] = 'Завершенно';
$string[ 'timetaken'] = 'Час який знадобився';
$string[ 'unfinished'] = 'Нескінченно';
$string[ 'withselected'] = 'З вибраним';

//snakes/play.php

//sudoku/create.php
$string[ 'sudoku_create_count'] = 'Кількість судоку для створення';
$string[ 'sudoku_create_start'] = 'Приступити до створення судоку';
$string[ 'sudoku_creating'] = 'Створенно  <b>$a</b> судоку';

//sudoku/play.php
$string[ 'sudoku_finishattemptbutton'] = 'Гра закінчена';
$string[ 'sudoku_guessnumber'] = 'Вгадайте число';
$string[ 'sudoku_noentriesfound'] = 'Слів не знайдено в словнику';

//attempt.php

//backuplib.php
$string[ 'modulenameplural'] = 'Гри';

//export.php
$string[ 'export'] = 'Експорт';
$string[ 'html_hascheckbutton'] = 'Перевірити наявність кнопки правильної відповіді:';
$string[ 'html_hasprintbutton'] = 'Перевірити наявність кнопки "Віддрукувати":';
$string[ 'html_title'] = 'Назва сайту:';
$string[ 'javame_createdby'] = 'Створено:';
$string[ 'javame_description'] = 'Описання:';
$string[ 'javame_filename'] = 'Назва файлу:';
$string[ 'javame_icon'] = 'Ікона:';
$string[ 'javame_maxpictureheight'] = 'Максимальний розмір висоти зображення:';
$string[ 'javame_maxpicturewidth'] = 'Максимальний розмір ширини зображення:';
$string[ 'javame_name'] = 'Назва:';
$string[ 'javame_type'] = 'Тип:';
$string[ 'javame_vendor'] = 'Постачальник:';
$string[ 'javame_version'] = 'Версія:';
$string[ 'only_teachers'] = 'Тільки вчитель може переглянути цю сторінку';

//exporthtml_hangman.php
$string[ 'hangman_loose'] = '<BIG><B>Гру закінчено</B></BIG>';
$string[ 'html_hangman_new'] = 'Новини';

//exporthtml_millionaire.php
$string[ 'millionaire_info_people'] = 'відповідь аудиторії';
$string[ 'millionaire_info_telephone'] = 'Я вважаю правильною віповіддю є';
$string[ 'millionaire_info_wrong_answer'] = 'Ваша відповідь невірна<br>Вірна відповідь :';
$string[ 'millionaire_sourcemodule_must_quiz_question'] = 'Для мільйонера, джерело питань мають бути $a';
$string[ 'must_select_questioncategory'] = 'Ви повинні, безумовно, вибрати категорію питань';
$string[ 'must_select_quiz'] = 'Ви повинні вибрати вікторину';

//exporthtml_snakes.php
$string[ 'score'] = 'Оцінка';

//header.php
$string[ 'modulename'] = 'Гра';

//index.php
$string[ 'attempts'] = 'Спроб';

//lib.php
$string[ 'attempt'] = 'Спроба $a';
$string[ 'game_bookquiz'] = 'Книга питань';
$string[ 'game_cross'] = 'Кросворд';
$string[ 'game_cryptex'] = 'Філворд';
$string[ 'game_hangman'] = 'Шибениця';
$string[ 'game_hiddenpicture'] = "Приховане зображення";
$string[ 'game_millionaire'] = 'Мільйонер';
$string[ 'game_snakes'] = 'Змія ';
$string[ 'game_sudoku'] = 'Судоку';
$string[ 'noattempts'] = 'Не було зроблено жодних спроб для цієї гри';
$string[ 'percent'] = 'Відсоток';

//locallib.php
$string[ 'attemptfirst'] = 'Перша спроба';
$string[ 'attemptlast'] = 'Остання спроба';
$string[ 'gradeaverage'] = 'Середній бал';
$string[ 'gradehighest'] = 'Найвища оцінка';

//mod.html
$string[ 'bottomtext'] = 'Текст в нижній частині сторінки';
$string[ 'cross_layout'] = 'Спосіб відображення';
$string[ 'cross_layout0'] = 'Розмістіть питання під кросвордом';
$string[ 'cross_layout1'] = 'Розмістіть питання на правій сторінці кросворду';
$string[ 'cross_maxcols'] = 'Максимальна кількість рядків / стовпців у кросворді';
$string[ 'cross_maxwords'] = 'Максимальна кількість слів у кросворді';
$string[ 'cryptex_maxcols'] = 'Максимальна кількість рядків / стовпців у філворді';
$string[ 'cryptex_maxtries'] = 'Максимальна кількість спроб';
$string[ 'cryptex_maxwords'] = 'Максимальна кількість слів  у філворді';
$string[ 'grademethod'] = 'Метод оцінування';
$string[ 'hangman_allowspaces'] = 'Дозволити прогалини в словах';
$string[ 'hangman_allowsub'] = 'Відображати символи в словах';
$string[ 'hangman_imageset'] = 'Виберіть колекцію фотографій';
$string[ 'hangman_language'] = 'Визначте мову слів';
$string[ 'hangman_maxtries'] = 'Кількість слів у грі';
$string[ 'hangman_showcorrectanswer'] = 'Відображати вірні відповіді після закінчення гри ?';
$string[ 'hangman_showfirst'] = 'Відображати першу літеру слова';
$string[ 'hangman_showlast'] = 'Відображати останню літеру слова';
$string[ 'hangman_showquestion'] = 'Відобразити всі запитання?';
$string[ 'hiddenpicture_across'] = "Кількість горизонтальних клітин";
$string[ 'hiddenpicture_down'] = "Кількість вертикальних клітин";
$string[ 'hiddenpicture_height'] = 'Встановити висоту зображення';
$string[ 'hiddenpicture_pictureglossary'] = 'Словник для конкретного питання';
$string[ 'hiddenpicture_pictureglossarycategories'] = 'Катигорія словника для конкретного питання';
$string[ 'hiddenpicture_width'] = 'Встановити ширину зображення';
$string[ 'millionaire_shuffle'] = 'Перемішати питання';
$string[ 'selectgame'] = 'Виберіть гру';
$string[ 'snakes_background'] = 'Рівень';
$string[ 'sourcemodule'] = 'Джерело питань';
$string[ 'sourcemodule_book'] = 'Виберіть книгу';
$string[ 'sourcemodule_glossary'] = 'Виберіть словник';
$string[ 'sourcemodule_glossarycategory'] = 'Виберіть категорію словника';
$string[ 'sourcemodule_include_subcategories'] = 'Включати підкатегорії';
$string[ 'sourcemodule_question'] = 'Питання';
$string[ 'sourcemodule_questioncategory'] = 'Виберіть категорію питань';
$string[ 'sourcemodule_quiz'] = 'Виберіть вікторіну';
$string[ 'sudoku_maxquestions'] = 'Максимальний ряд питань';

//mod_form.php

//preview.php
$string[ 'formatdatetime'] = '%%d %%b %%Y, %%H:%%M';
$string[ 'lastip'] = 'IP учня';
$string[ 'preview'] = 'Πерегляд';
$string[ 'showsolution'] = 'Рішення';
$string[ 'timefinish'] = 'Гру закінчено';
$string[ 'timelastattempt'] = 'Остання спроба';
$string[ 'timestart'] = 'Початок';

//review.php
$string[ 'completedon'] = 'Завершено в';
$string[ 'outof'] = '$a->grade з максимального $a->maxgrade';
$string[ 'review'] = 'Перегляд';
$string[ 'reviewofattempt'] = 'Перегляд спроби $a';
$string[ 'startagain'] = 'Почати з нову';

//showanswers.php
$string[ 'feedbacks'] = 'Повідомлення правильної відповіді';

//tabs.php
$string[ 'export_to_html'] = 'Експорт у форматі HTML';
$string[ 'export_to_javame'] = 'Експорт у форматі JavaMe для мобілних телефонів';
$string[ 'info'] = 'Інформація';
$string[ 'results'] = 'Результати';
$string[ 'showanswers'] = 'Відображати питання';

//view.php
$string[ 'attemptgamenow'] = 'Спробувати гру зараз';
$string[ 'continueattemptgame'] = 'Продовжити гру';
$string[ 'gradesofar'] = '$a->method: $a->mygrade / $a->gamegrade.';
$string[ 'marks'] = 'Оцінки';
$string[ 'nomoreattempts'] = 'Інші зусилля не дозволяються';
$string[ 'reattemptgame'] = 'Почати гру знову';
$string[ 'yourfinalgradeis'] = 'Підсумкова оцінка для цієї вікторини є $a';

//Untranslated
//db/access.php $string[ 'game:attempt'] = 'Play games';
//db/access.php $string[ 'game:deleteattempts'] = 'Delete attempts';
//db/access.php $string[ 'game:grade'] = 'Grade games manually';
//db/access.php $string[ 'game:manage'] = 'Manage';
//db/access.php $string[ 'game:preview'] = 'Preview Games';
//db/access.php $string[ 'game:reviewmyattempts'] = 'reviewmyattempts';
//db/access.php $string[ 'game:view'] = 'View game information';
//db/access.php $string[ 'game:viewreports'] = 'View game reports';
//hiddenpicture/play.php $string[ 'no_questions'] = "There are no questions";
//hiddenpicture/play.php $string[ 'noglossaryentriesfound'] = 'No glossary entries found';
//snakes/play.php $string[ 'snakes_dice'] = 'Dice, $a spots.';
//snakes/play.php $string[ 'snakes_player'] = 'Player, position: $a.';
//attempt.php $string[ 'gameclosed'] = 'This game closed on {$a}';
//attempt.php $string[ 'gamenotavailable'] = 'The game will not be available until {$a}';
//exporthtml_millionaire.php $string[ 'millionaire_helppeople'] = 'Ask the audience';
//exporthtml_millionaire.php $string[ 'millionaire_quit'] = 'Quit';
//exporthtml_millionaire.php $string[ 'millionaire_telephone'] = 'Call a friend';
//exporthtml_snakes.php $string[ 'html_snakes_check'] = 'Check';
//exporthtml_snakes.php $string[ 'html_snakes_correct'] = 'Correct!';
//exporthtml_snakes.php $string[ 'html_snakes_no_selection'] = 'Have to select something!';
//exporthtml_snakes.php $string[ 'html_snakes_wrong'] = "Your answer isn't correct. Stay on the same seat.";
//lib.php $string[ 'reset_game_all'] = 'Delete tries from all games';
//lib.php $string[ 'reset_game_deleted_course'] = 'Delete tries from deleted courses';
//mod.html $string[ 'millionaire_background'] = 'Background color';
//mod.html $string[ 'toptext'] = 'Text at the top';
//mod_form.php $string[ 'cross_options'] = 'Crossword options';
//mod_form.php $string[ 'cryptex_options'] = 'Cryptex options';
//mod_form.php $string[ 'gameclose'] = 'Close the game';
//mod_form.php $string[ 'gameopen'] = 'Open the game';
//mod_form.php $string[ 'hangman_maximum_number_of_errors'] = 'Maximum number or errors (have to be images named hangman_0.jpg, hangman_1.jpg, ...)';
//mod_form.php $string[ 'hangman_options'] = 'Hangman options';
//mod_form.php $string[ 'headerfooter_options'] = 'Header/Footer options';
//mod_form.php $string[ 'hiddenpicture_options'] = '\'Hidden Picture\' options';
//mod_form.php $string[ 'millionaire_options'] = 'Millionaire options';
//mod_form.php $string[ 'snakes_board'] = 'Board';
//mod_form.php $string[ 'snakes_cols'] = 'Cols';
//mod_form.php $string[ 'snakes_file'] = 'File for background';
//mod_form.php $string[ 'snakes_footerx'] = 'Space at bootom left';
//mod_form.php $string[ 'snakes_footery'] = 'Space at bottom right';
//mod_form.php $string[ 'snakes_headerx'] = 'Space at up left';
//mod_form.php $string[ 'snakes_headery'] = 'Space at up right';
//mod_form.php $string[ 'snakes_options'] = '\'Snakes and Ladders\' options';
//mod_form.php $string[ 'snakes_rows'] = 'Rows';
//mod_form.php $string[ 'sudoku_options'] = 'Sudoku options';
//mod_form.php $string[ 'userdefined'] = "User defined ";
//showanswers.php $string[ 'clearrepetitions'] = 'Clear statistics';
//showanswers.php $string[ 'computerepetitions'] = 'Compute statistics again';
//showanswers.php $string[ 'repetitions'] = 'Repetitions';
//tabs.php $string[ 'showattempts'] = 'Show attempts';
//view.php $string[ 'gamecloses'] = 'Game closes';
//view.php $string[ 'gameopens'] = 'Game opens';
