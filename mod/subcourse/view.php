<?php  // $Id$

/**
 * This page redirects to the sub course

 */

require_once(dirname(dirname(dirname(__FILE__))).'/config.php');
require_once(dirname(__FILE__).'/lib.php');
require_once($CFG->libdir.'/gradelib.php');

$id = optional_param('id', 0, PARAM_INT); // Course Module ID, or
$a  = optional_param('a', 0, PARAM_INT);  // subcourse ID
$fetchnow = optional_param('fetchnow', 0, PARAM_INT);   // manual fetch

if ($id) {
    if (! $cm = get_record("course_modules", "id", $id)) {
        error("Course Module ID was incorrect");
    }

    if (! $course = get_record("course", "id", $cm->course)) {
        error("Course is misconfigured");
    }

    if (! $subcourse = get_record("subcourse", "id", $cm->instance)) {
        error("Course module is incorrect");
    }

} else {
    if (! $subcourse = get_record("subcourse", "id", $a)) {
        error("Course module is incorrect");
    }
    if (! $course = get_record("course", "id", $subcourse->course)) {
        error("Course is misconfigured");
    }
    if (! $cm = get_coursemodule_from_instance("subcourse", $subcourse->id, $course->id)) {
        error("Course Module ID was incorrect");
    }
}

$context = get_context_instance(CONTEXT_MODULE, $cm->id);
$course_context = get_context_instance(CONTEXT_COURSE, $course->id);

if (! $refcourse = get_record("course", "id", $subcourse->refcourse)) {
    print_error("errinvalidrefcourse", "subcourse");
}



$refcourselink = new stdClass();
$refcourselink->name = $refcourse->fullname;
$refcourselink->href = $CFG->wwwroot.'/course/view.php?id='.$refcourse->id;


header( 'Location: '.$refcourselink->href.'' ) ;

?>
