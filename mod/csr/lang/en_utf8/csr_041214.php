<?php
$string['addmanageremailaddress'] = 'Add manager email address';
$string['addmanageremailinstruction'] = 'You have not previously entered your manager\'s email address. Please enter it below to sign-up for this event. ';
$string['addsession'] = 'Add a new event';
$string['addingsession'] = 'Adding a new event in $a';
$string['addnewfield'] = 'Add a new custom field';
$string['addnewfieldlink'] = 'Create a new custom field';
$string['addnewnotice'] = 'Add a new site notice';
$string['addnewnoticelink'] = 'Create a new site notice';
$string['addremoveattendees'] = 'Add/remove attendees';
$string['addstudent'] = 'Add student';
$string['alllocations'] = 'All locations';
$string['guidelines'] = 'Guidelines';
$string['contactperson'] = 'Event contact person';
$string['eventname'] = 'Name of Event';
$string['allowoverbook'] = 'Allow overbooking';
$string['allsessionsin'] = 'All events in $a';
$string['alreadysignedup'] = 'You have already signed-up for this CSR activity.';
$string['answer'] = 'Sign in';
$string['answercancel'] = 'Sign out';
$string['approvalreqd'] = 'Approval required';
$string['approve'] = 'Approve';
$string['assessmentyour'] = 'Your assessment';
$string['attendance'] = 'Attendance';
$string['attendanceinstructions'] = 'Select users who attended the event:';
$string['attendedsession'] = 'Attended event';
$string['attendees'] = 'Attendees';
$string['attendeestablesummary'] = 'People planning on or having attended this event.';
$string['requeststablesummary'] = 'People requesting to attended this event.';
$string['booked'] = 'Booked';
$string['bookingcancelled'] = 'Your booking has been cancelled.';
$string['bookingcompleted'] = 'Your booking has been completed.';
$string['bookingfull'] = 'Booking full';
$string['bookingopen'] = 'Booking open';
$string['bookingstatus'] = 'You are booked for the following event';
$string['calendareventdescriptionbooking'] = 'You are booked for this <a href=\"$a\">CSR Event</a>.';
$string['calendareventdescriptionsession'] = 'You have created this <a href=\"$a\">CSR Event</a>.';
$string['calendaroptions'] = 'Calendar options';
$string['cancelbooking'] = 'Cancel booking';
$string['cancelbookingfor'] = 'Cancel booking for $a';
$string['cancellationsent'] = 'You should immediately receive a cancellation email.';
$string['cancellationsentmgr'] = 'You and your manager should immediately receive a cancellation email.';
$string['cancellationstablesummary'] = 'List of people who have cancelled their event signups.';
$string['cancelreason'] = 'Reason';
$string['capacity'] = 'Capacity';
$string['changemanageremailaddress'] = 'Change manager email address';
$string['changemanageremailinstruction'] = 'Please enter the email address for your current manager below.';
$string['clearall'] = 'Clear all';
$string['closed'] = 'Closed';
$string['conditions'] = 'Conditions';
$string['conditionsexplanation'] = 'All of these criteria must be met for the notice to be shown on the training calendar:';
$string['confirm'] = 'Confirm';
$string['confirmanager'] = 'Confirm manager\'s email address';
$string['confirmmanageremailaddress'] = 'Confirm manager email address';
$string['confirmmanageremailaddressquestion'] = 'Is <b>$a</b> still your manager\'s email address?';
$string['confirmmanageremailinstruction1'] = 'You previously entered the following as your manager\'s email address:';
$string['confirmmanageremailinstruction2'] = 'Is this still your manager\'s email address?';
$string['confirmation'] = 'Confirmation';
$string['confirmationmessage'] = 'Confirmation message';
$string['confirmationsent'] = 'You should immediately receive a confirmation email.';
$string['confirmationsentmgr'] = 'You and your manager should immediately receive a confirmation email.';
$string['copyingsession'] = 'Copying as a new event in $a';
$string['copysession'] = 'Copy event';
$string['copy'] = 'Copy';
$string['cost'] = 'Cost';
$string['cancelbooking'] = 'Cancel booking';
$string['cancellation'] = 'Cancellation';
$string['cancellations'] = 'Cancellations';
$string['cancellationmessage'] = 'Cancellation message';
$string['cancellationconfirm'] = 'Are you sure you want to cancel your booking to this event?';
$string['costheading'] = 'event Cost';
$string['currentstatus'] = 'Current status';
$string['customfieldsheading'] = 'Custom event Fields';
$string['date'] = 'Date';
$string['dateadd'] = 'Add a new date';
$string['dateremove'] = 'Remove this date';
$string['datetext'] = 'You are signed in for date';
$string['datetimeknownhinttext'] = '';
$string['decidelater'] = 'Decide Later';
$string['deleteall'] = 'Delete all';
$string['deletesession'] = 'Delete event';
$string['deletesessionconfirm'] = 'Are you completely sure you want to delete this event and all sign-ups for this event?';
$string['deletingsession'] = 'Deleting event in $a';
$string['decline'] = 'Decline';
$string['description'] = 'Introduction text';
$string['details'] = 'Description of Event';
$string['goals'] = 'Event Goals and Objectives';
$string['category'] = 'Event Category ';
$string['subcategory'] = 'Event Subcategory ';
$string['discountcode'] = 'Discount code';
$string['discountcost'] = 'Discount cost';
$string['discountcosthinttext'] = '';
$string['due'] = 'due';
$string['duration'] = 'Duration';
$string['early'] = '\$a early';
$string['edit'] = 'Edit';
$string['editsession'] = 'Edit event';
$string['editingsession'] = 'Editing event in $a';
$string['emailmanager'] = 'Send notice to manager';
$string['email:instrmngr'] = 'Notice for manager';
$string['email:message'] = 'Message';
$string['email:subject'] = 'Subject';
$string['emptylocation'] = 'Location was empty';
$string['enrolled'] = 'enrolled';
$string['error:addalreadysignedupattendee'] = '$a is already signed-up for this CSR activity.';
$string['error:addattendee'] = 'Could not add $a to the event.';
$string['error:cancelbooking'] = 'There was a problem cancelling your booking';
$string['error:cannotemailmanager'] = 'Sent reminder mail for submission id $a->submissionid to user $a->userid, but could not send the message for the user\'s manager email address ($a->manageremail).';
$string['error:cannotemailuser'] = 'Could not send out mail for submission id $a->submissionid to user $a->userid ($a->useremail).';
$string['error:cannotsendconfirmationmanager'] = 'A confirmation message was sent to your email account, but there was a problem sending the confirmation messsage to your manager\'s email address.';
$string['error:cannotsendconfirmationthirdparty'] = 'A confirmation message was sent to your email account and your manager\'s email account, but there was a problem sending the confirmation messsage to the third party\'s email address.';
$string['error:cannotsendconfirmationuser'] = 'There was a problem sending the confirmation message to your email account.';
$string['error:cannotsendrequestuser'] = 'There was a problem sending the signup request message to your email account.';
$string['error:cannotsendrequestmanager'] = 'There was a problem sending the signup request message to your manager\'s email account.';
$string['error:cannotsendconfirmationusermanager'] = 'A confirmation message could not be sent to your email address and to your manager\'s email address.';
$string['error:couldnotaddfield'] = 'Could not add custom event field.';
$string['error:couldnotaddnotice'] = 'Could not add site notice.';
$string['error:couldnotaddsession'] = 'Could not add event';
$string['error:couldnotcopysession'] = 'Could not copy event';
$string['error:couldnotdeletefield'] = 'Could not delete custom event field';
$string['error:couldnotdeletenotice'] = 'Could not delete site notice';
$string['error:couldnotdeletesession'] = 'Could not delete event';
$string['error:couldnotfindsession'] = 'Could not find the newly inserted event';
$string['error:couldnotsavecustomfield'] = 'Could not save custom field';
$string['error:couldnotupdatecalendar'] = 'Could not update event event in the calendar.';
$string['error:couldnotupdatefield'] = 'Could not update custom event field.';
$string['error:couldnotupdatemanageremail'] = 'Could not update manager email address.';
$string['error:couldnotupdatenotice'] = 'Could not update site notice.';
$string['error:couldnotupdatesession'] = 'Could not update event';
$string['error:coursemisconfigured'] = 'CSR event is misconfigured';
$string['error:cronprefix'] = 'Error: CSR cron:';
$string['error:emptymanageremail'] = 'Manager email address empty.';
$string['error:emptylocation'] = 'Location was empty.';
$string['error:emptyvenue'] = 'Venue was empty.';
$string['error:enrolmentfailed'] = 'Could not enrol $a into the CSR event.';
$string['error:eventoccurred'] = 'You cannot cancel an event that has already occurred.';
$string['error:incorrectcoursemodule'] = 'CSR event module is incorrect';
$string['error:incorrectcoursemoduleid'] = 'CSR event Module ID was incorrect';
$string['error:incorrectcoursemodulesession'] = 'CSR event Module CSR Event was incorrect';
$string['error:incorrectcsrid'] = 'CSR Event ID was incorrect';
$string['error:incorrectnotificationtype'] = 'Incorrect notification type supplied';
$string['error:invaliduserid'] = 'Invalid user ID';
$string['error:manageremailaddressmissing'] = 'You are currently not assigned to a manager in the system. Please contact the site administrator.';
$string['error:mustspecifycoursemodulecsr'] = 'Must specify a CSR event module or a CSR Event ID';
$string['error:nomanageremail'] = 'You didn\'t provide an email address for your manager';
$string['error:nomanagersemailset'] = 'No manager email is set';
$string['error:nopermissiontosignup'] = 'You don\'t have permission to signup to this CSR Event event.';
$string['error:problemsigningup'] = 'There was a problem signing you up.';
$string['error:removeattendee'] = 'Could not remove $a from the event.';
$string['error:sessionstartafterend'] = 'Event start date/time is after end.';
$string['error:signedupinothersession'] = 'You are already signed up in another event for this activity. You can only sign up for one event per CSR activity.';
$string['error:unknownbuttonclicked'] = 'No action associated with the button that was clicked';
$string['excelformat'] = 'Excel';
$string['export'] = 'Export';
$string['exporttofile'] = 'Export to file';
$string['exportattendance'] = 'Export attendance';
$string['csr'] = 'CSR Events';
$string['csr:addattendees'] = 'Add attendees to a CSR event';
$string['csr:editsessions'] = 'Add, edit, copy and delete CSR event';
$string['csr:overbook'] = 'Sign-up to full events.';
$string['csr:removeattendees'] = 'Remove attendees from a CSR event';
$string['csr:signup'] = 'Sign-up for a CSR event';
$string['csr:takeattendance'] = 'Take attendance';
$string['csr:view'] = 'View CSR activities and events';
$string['csr:viewattendees'] = 'View attendance list and attendees';
$string['csr:viewcancellations'] = 'View cancellations';
$string['csr:viewemptyactivities'] = 'View empty CSR activities';
$string['csrbooking'] = 'CSR event booking';
$string['csrname'] = 'CSR name';
$string['csrsession'] = 'CSR event';
$string['feedback'] = 'Feedback';
$string['feedbackupdated'] = 'Feedback updated for \$a people';
$string['field:text'] = 'Text';
$string['field:multiselect'] = 'Multiple selection';
$string['field:select'] = 'Menu of choices';
$string['fielddeleteconfirm'] = 'Delete field \'$a\' and all event data associated with it?';
$string['floor'] = 'Floor';
$string['format'] = 'Format';
$string['full'] = 'Date is fully occupied';
$string['goback'] = 'Go back';
$string['guestsno'] = 'Sorry, guests are not allowed to sign up for events.';
$string['icalendarheading'] = 'iCalendar Attachments';
$string['import'] = 'Import';
$string['info'] = 'Info';
$string['late'] = '\$a late';
$string['location'] = 'Location';
$string['lookfor'] = 'Search';
$string['manageradded'] = 'Your manager\'s email address has been accepted.';
$string['managerchanged'] = 'Your manager\'s email address has been changed.';
$string['manageremail'] = 'Manager\'s email';
$string['manageremailaddress'] = 'Manager\'s email address';
$string['manageremailformat'] = 'The email address must be of the format \'$a\' to be accepted.';
$string['manageremailheading'] = 'Manager Emails';
$string['manageremailinstruction'] = 'In order to sign-up for a training event, a confirmation email must be sent to your email address and copied to your manager\'s email address.';
$string['manageremailinstructionconfirm'] = 'Please confirm that this is your manager\'s email address:';
$string['managername'] = 'Manager\'s name';
$string['managerupdated'] = 'Your manager\'s email address has been updated.';
$string['maximumpoints'] = 'Maximum number of points';
$string['maximumsize'] = 'Maximum number of attendees';
$string['message'] = 'Change in booking in the CSR event $a->coursename!

There has been a free place in the event on $a->duedate ($a->name) in the CSR event $a->coursename.
You have been registered. If the date does not suit you anymore, please unregister at <a href=\'$a->url\'>$a->url</a>.';
$string['modulename'] = 'CSR';
$string['modulenameplural'] = 'CSR';
$string['moreinfo'] = 'More info';
$string['multiday'] = 'multi-day';
$string['newmanageremailaddress'] = 'Manager\'s email address';
$string['noactionableunapprovedrequests'] = 'No actionable unapproved requests';
$string['nocustomfields'] = '<p>No custom fields are defined.</p>';
$string['nocsrs'] = 'There are no CSR events';
$string['nositenotices'] = '<p>No site notices are defined.</p>';
$string['none'] = 'none';
$string['normalcost'] = 'Normal cost';
$string['normalcosthinttext'] = '';
$string['noremindersneedtobesent'] = 'No reminders need to be sent.';
$string['nosignedupusers'] = 'No users have signed-up for this event.';
$string['note'] = 'Note';
$string['notefull'] = 'Even if the event is fully booked you can still register. You will be queued (marked in red). If someone signs out, the first student in the queue will be moved into registeres students and a notification will be sent to him/her by mail.';
$string['notificationtype'] = 'Notification Type';
$string['notificationboth'] = 'Email Notification and iCalendar Appointment';
$string['notificationemail'] = 'Email Notification only';
$string['notificationical'] = 'iCalendar Appointment only';
$string['noticedeleteconfirm'] = 'Delete site notice \'$a->name\'?<br/><blockquote>$a->text</blockquote>';
$string['noticetext'] = 'Notice text';
$string['location'] = 'Location';
$string['venue'] = 'venue';
$string['notsignedup'] = 'You are not signed up for this event.';
$string['notsubmittedyet'] = 'Not yet evaluated';
$string['noupcoming'] = '<p><i>No upcoming events</i></p>';
$string['odsformat'] = 'OpenDocument';
$string['onehour'] = '1 hour';
$string['oneminute'] = '1 minute';
$string['options'] = 'Options';
$string['or'] = 'or';
$string['order'] = 'Order';
$string['place'] = 'Room';
$string['placeholder:csrname'] = '[csrname]';
$string['placeholder:firstname'] = '[firstname]';
$string['placeholder:lastname'] = '[lastname]';
$string['placeholder:eventname'] = '[eventname]';
$string['placeholder:contactperson'] = '[contactperson]';
$string['placeholder:location'] = '[location]';
$string['placeholder:venue'] = '[venue]';
$string['placeholder:goals'] = '[goals]';
$string['placeholder:cost'] = '[cost]';
$string['placeholder:alldates'] = '[alldates]';
$string['placeholder:sessiondate'] = '[sessiondate]';
$string['placeholder:starttime'] = '[starttime]';
$string['placeholder:finishtime'] = '[finishtime]';
$string['placeholder:duration'] = '[duration]';
$string['placeholder:details'] = '[details]';
$string['placeholder:reminderperiod'] = '[reminderperiod]';
$string['placeholder:attendeeslink'] = '[attendeeslink]';
$string['points'] = 'Points';
$string['pointsplural'] = 'Points';
$string['previoussessions'] = 'Previous events';
$string['previoussessionslist'] = 'List of all past events for this CSR activity';
$string['printversionid'] = 'Print version: without name';
$string['printversionname'] = 'Print version: with name';
$string['really'] = 'Do you really want to delete all results for this CSR activity?';
$string['registeredon'] = 'Registered On';
$string['registrations'] = 'Registrations';
$string['reminder'] = 'Reminder';
$string['remindermessage'] = 'Reminder message';
$string['reminderperiod'] = 'Days before message is sent';
$string['requestmessage'] = 'Request message';
$string['room'] = 'Room';
$string['saveallfeedback'] = 'Save all responses';
$string['saveattendance'] = 'Save attendance';
$string['scheduledsession'] = 'Scheduled event';
$string['scheduledsessions'] = 'Scheduled events';
$string['seatsavailable'] = 'Seats available';
$string['seeattendees'] = 'See attendees';
$string['sentremindermanager'] = 'Sent reminder email to user manager';
$string['sentreminderuser'] = 'Sent reminder email to user';
$string['sessiondate'] = 'Event date';
$string['sessiondatetime'] = 'Event date/time';
$string['sessiondatetimeknown'] = 'Event date/time known';
$string['sessionsdetailstablesummary'] = 'Full description of the current event.';
$string['sessionfinishtime'] = 'Event finish time';
$string['sessioninprogress'] = 'Event in progress';
$string['sessionisfull'] = 'This event is now full. You will need to pick another time or talk to the instructor.';
$string['sessionover'] = 'Event over';
$string['sessions'] = 'Events';
$string['sessionsoncoursepage'] = 'Events displayed on course page';
$string['sessionrequiresmanagerapproval'] = 'This event requires manager approval to book.';
$string['sessionroles'] = 'event roles';
$string['sessionstartdate'] = 'event start date';
$string['sessionstarttime'] = 'event start time';
$string['sessionvenue'] = 'event venue';
$string['setting:addchangemanageremail'] = 'Ask users for their manager\'s email address.';
$string['setting:addchangemanageremaildefault'] = 'Ask users for their manager\'s email address.';
$string['setting:addchangemanageremail_caption'] = 'Manager\'s email:';
$string['setting:defaultcancellationinstrmngr'] = 'Default cancellation message sent to managers.';
$string['setting:defaultcancellationinstrmngr_caption'] = 'Cancellation message (managers)';
$string['setting:defaultcancellationinstrmngrdefault'] = "*** Advice only ****

This is to advise that [firstname] [lastname] is no longer signed-up for the following CSR event and listed you as their Team Leader / Manager.

*** [firstname] [lastname]'s booking cancellation is copied below ****
";
$string['setting:defaultcancellationmessage'] = 'Default cancellation message sent to the user.';
$string['setting:defaultcancellationmessage_caption'] = 'Cancellation message';
$string['setting:defaultcancellationmessagedefault'] = 'This is to advise that your booking on the following CSR event has been cancelled:

You booking for the CSR event is cancelled.

Participant:   [firstname] [lastname]
CSR Activity:   [eventname]
Contact person: [contactperson]

Duration:    [duration]
Date(s):
[alldates]

Location:   [location], [venue]

Goals and Objectives
[goals]
';
$string['setting:defaultcancellationsubject'] = 'Default subject line for cancellation emails.';
$string['setting:defaultcancellationsubject_caption'] = 'Cancellation subject';
$string['setting:defaultcancellationsubjectdefault'] = 'CSR event booking cancellation';
$string['setting:defaultconfirmationinstrmngr'] = 'Default confirmation message sent to managers.';
$string['setting:defaultconfirmationinstrmngr_caption'] = 'Confirmation message (managers)';
$string['setting:defaultconfirmationinstrmngrdefault'] = "*** Advice only ****

This is to advise that [firstname] [lastname] has been booked for the following CSR event and listed you as their Team Leader / Manager.

If you are not their Team Leader / Manager and believe you have received this email by mistake please reply to this email.  If have concerns about your staff member taking this CSR event please discuss this with them directly.

*** [firstname] [lastname]'s booking confirmation is copied below ****
";
$string['setting:defaultconfirmationmessage'] = 'Default confirmation message sent to users.';
$string['setting:defaultconfirmationmessage_caption'] = 'Confirmation message';
$string['setting:defaultconfirmationmessagedefault'] = 'Please open the attachment and accept the invite so that it updates your outlook calendar.
This is to confirm that you are now booked on the following CSR event:

OPEN THE 
Participant:   [firstname] [lastname]
CSR Activity:   [eventname]
Contact person: [contactperson]

Duration:    [duration]
Date(s):
[alldates]

Location:   [location], [venue]

Goals and Objectives
[goals]

[details]

To cancel your booking, Please use MyCSR link on the portal. You will receive a reminder [reminderperiod] business days before this event.
';
$string['setting:defaultconfirmationsubject'] = 'Default subject line for confirmation emails.';
$string['setting:defaultconfirmationsubject_caption'] = 'Confirmation subject';
$string['setting:defaultconfirmationsubjectdefault'] = 'CSR event booking confirmation: [csrname], [starttime]-[finishtime], [sessiondate]';
$string['setting:defaultreminderinstrmngr'] = 'Default reminder message sent to managers.';
$string['setting:defaultreminderinstrmngr_caption'] = 'Reminder message (managers)';
$string['setting:defaultreminderinstrmngrdefault'] = "*** Reminder only ****

Your staff member [firstname] [lastname] is booked to attend and above CSR event and has also received this reminder email.

If you are not their Team Leader / Manager and believe you have received this email by mistake please reply to this email.

*** [firstname] [lastname]'s reminder email is copied below ****
";
$string['setting:defaultremindermessage'] = 'Default reminder message sent to users.';
$string['setting:defaultremindermessage_caption'] = 'Reminder message';
$string['setting:defaultremindermessagedefault'] = 'This is a reminder that you are booked on the following CSR event:

Participant:   [firstname] [lastname]
CSR Activity:   [eventname]
Contact person: [contactperson]

Duration:    [duration]
Date(s):
[alldates]

Location:   [location], [venue]

Goals and Objectives
[goals]

[details]

To cancel your booking, Please use MyCSR link on the portal.



You will receive a reminder [reminderperiod] business days before this event.

';
$string['setting:defaultremindersubject'] = 'Default subject line for reminder emails.';
$string['setting:defaultremindersubject_caption'] = 'Reminder subject';
$string['setting:defaultremindersubjectdefault'] = 'CSR event booking reminder: [csrname], [starttime]-[finishtime], [sessiondate]';
$string['setting:defaultrequestinstrmngrdefault'] = 'This is to advise that [firstname] [lastname] has requested to be booked into the following CSR event, and you are listed as their Team Leader / Manager.

Participant:   [firstname] [lastname]
CSR Activity:   [eventname]
Contact person: [contactperson]

Duration:    [duration]
Date(s):
[alldates]

Location:   [location], [venue]

Goals and Objectives
[goals]


You will receive a reminder [reminderperiod] business days before this event.

Please follow the link below to approve the request:
[attendeeslink]#unapproved


*** [firstname] [lastname]\'s booking request is copied below ****
';
$string['setting:defaultrequestmessagedefault'] = 'Your request to book into the following CSR event has been sent to your manager:

Participant:   [firstname] [lastname]
CSR Activity:   [eventname]
Contact person: [contactperson]

Duration:    [duration]
Date(s):
[alldates]

Location:   [location], [venue]

Goals and Objectives
[goals]

';
$string['setting:defaultrequestsubjectdefault'] = 'CSR event booking request: [csrname], [starttime]-[finishtime]';
$string['setting:defaultvalue'] = 'Default value';
$string['setting:defaultwaitlistedmessage'] = 'Default wait-listed message sent to users.';
$string['setting:defaultwaitlistedmessage_caption'] = 'Wait-listed message';
$string['setting:defaultwaitlistedmessagedefault'] = 'This is to advise that you been added to the waitlist for:

Participant:   [firstname] [lastname]
CSR Activity:   [eventname]
Contact person: [contactperson]

Duration:    [duration]
Date(s):
[alldates]

Location:   [location], [venue]

Goals and Objectives
[goals]

';
$string['setting:defaultwaitlistedsubject'] = 'Default subject line for wait-listed emails.';
$string['setting:defaultwaitlistedsubject_caption'] = 'Wait-listed subject';
$string['setting:defaultwaitlistedsubjectdefault'] = 'Waitlisting advice for [csrname]';
$string['setting:disableicalcancel'] = 'Disable cancellation emails with an iCalendar attachment.';
$string['setting:disableicalcancel_caption'] = 'Disable iCalendar cancellations:';
$string['setting:fromaddress'] = 'What will appear in the From field of email reminders sent by this module.';
$string['setting:fromaddress_caption'] = 'Sender address:';
$string['setting:fromaddressdefault'] = 'moodle@example.com';
$string['setting:manageraddressformat'] = 'Suffix which must be present in the email address of the manager in order to be considered valid.';
$string['setting:manageraddressformat_caption'] = 'Required suffix:';
$string['setting:manageraddressformatdefault'] = '';
$string['setting:manageraddressformatreadable'] = 'Short description of the restrictions on a manager\'s email address.  This setting has no effect if the previous one is not set.';
$string['setting:manageraddressformatreadable_caption'] = 'Format example:';
$string['setting:manageraddressformatreadabledefault'] = 'firstname.lastname@company.com';
$string['setting:oneemailperday'] = 'Send multiple confirmation emails for multi-day events.';
$string['setting:oneemailperday_caption'] = 'One message per day:';
$string['setting:hidecost'] = 'Hide the cost and discount code fields.';
$string['setting:hidecost_caption'] = 'Hide cost and discount:';
$string['setting:hidediscount'] = 'Hide only the discount code field.';
$string['setting:hidediscount_caption'] = 'Hide discount:';
$string['setting:isfilter'] = 'Display as a filter';
$string['setting:possiblevalues'] = 'List of possible values';
$string['setting:showinsummary'] = 'Show in exports and lists';
$string['setting:sessionroles'] = 'Users assigned to the selected roles in a CSR event can be tracked with each CSR event';
$string['setting:sessionroles_caption'] = 'event roles:';
$string['setting:type'] = 'Field type';
$string['showbylocation'] = 'Show by location';
$string['showoncalendar'] = 'Show on the calendar';
$string['signup'] = 'Sign-up';
$string['signups'] = 'Sign-ups';
$string['signupfor'] = 'Sign-up for $a';
$string['signupforsession'] = 'Sign-up for an available upcoming event';
$string['signupforthissession'] = 'Sign-up for this CSR event';
$string['sign-ups'] = 'Sign-ups';
$string['sitenoticesheading'] = 'Site Notices';
$string['subject'] = 'Change in booking in the CSR event $a->coursename ($a->duedate)';
$string['submissions'] = 'Submissions';
$string['submitted'] = 'Submitted';
$string['submit'] = 'Submit';
$string['suppressemail'] = 'Suppress email notification';
$string['status'] = 'Status';
$string['status_booked'] = 'Booked';
$string['status_fully_attended'] = 'Fully attended';
$string['status_no_show'] = 'No show';
$string['status_partially_attended'] = 'Partially attended';
$string['status_requested'] = 'Requested';
$string['status_user_cancelled'] = 'User Cancelled';
$string['status_waitlisted'] = 'Wait-listed';
$string['status_approved'] = 'Approved';
$string['status_declined'] = 'Declined';
$string['status_session_cancelled'] = 'event Cancelled';
$string['summary'] = 'Summary';
$string['takeattendance'] = 'Take attendance';
$string['time'] = 'Time';
$string['timedue'] = 'Registration deadline';
$string['timefinish'] = 'Finish time';
$string['timestart'] = 'Start time';
$string['timecancelled'] = 'Time Cancelled';
$string['timerequested'] = 'Time Requested';
$string['timesignedup'] = 'Time Signed Up';
$string['thirdpartyemailaddress'] = 'Third-party email address(es)';
$string['thirdpartywaitlist'] = 'Notify third-party about wait-listed events';
$string['unapprovedrequests'] = 'Unapproved Requests';
$string['unknowndate'] = '(unknown date)';
$string['unknowntime'] = '(unknown time)';
$string['upcomingsessions'] = 'Upcoming events';
$string['upcomingsessionslist'] = 'List of all upcoming events for this CSR activity';
$string['usercancelledon'] = 'User cancelled on $a';
$string['usernotsignedup'] = 'Status: not signed up';
$string['usersignedup'] = 'Status: signed up';
$string['usersignedupon'] = 'User signed up on $a';
$string['userwillbewaitlisted'] = 'This event is currently full. By clicking the \"Sign-up\" button, you will be placed on the events\'s wait-list.';
$string['validation:needatleastonedate'] = 'You need to provide at least one date or mark the event as wait-listed.';
$string['venue'] = 'Venue';
$string['viewallsessions'] = 'View all events';
$string['viewsubmissions'] = 'View submissions';
$string['waitlistedmessage'] = 'Wait-listed message';
$string['wait-list'] = 'Wait-list';
$string['wait-listed'] = 'Wait-listed';
$string['xhours'] = '$a hours';
$string['xminutes'] = '$a minutes';
$string['youarebooked'] = 'You are booked for the following event';
$string['youremailaddress'] = 'Your email address';
$string['error:shortnametaken'] = 'Custom field with this short name already exists.';
