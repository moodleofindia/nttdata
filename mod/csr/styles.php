csr.mod-csr table.f2fsession td.c0 {
    font-weight: bold;
}
csr.mod-csr table.f2fsession td.c1 {
    width: 100%;
}
csr#mod-csr-signup div#content div.generalbox {
    text-align: center;
}
csr#mod-csr-view div#content div.generalbox {
    text-align: center;
}
csr#mod-csr-view div#content div.generalbox div#description {
    text-align: left;
}
csr#mod-csr-attendees div#content div.generalbox {
    text-align: center;
}
tr.highlight td.cell {
    background-color: #AAFFAA;
}
a.f2fsessionlinks, td.csrsessionnotice, span.csrsessionnotice {
    font-size: 11px;
    font-weight: bold;
    line-height: 14px;
}
