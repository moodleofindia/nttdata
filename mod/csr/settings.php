<?php

require_once "$CFG->dirroot/mod/csr/lib.php";

$settings->add(new admin_setting_configtext('csr_fromaddress', get_string('setting:fromaddress_caption', 'csr'),get_string('setting:fromaddress', 'csr'), get_string('setting:fromaddressdefault', 'csr'), "/^((?:[\w\.\-])+\@(?:(?:[a-zA-Z\d\-])+\.)+(?:[a-zA-Z\d]{2,4}))$/",30));

// Load roles
$choices = array();
if ($roles = get_records('role')) {
    foreach($roles as $role) {
        $choices[$role->id] = format_string($role->name);
    }
}

$settings->add(new admin_setting_configmultiselect('csr_session_roles', get_string('setting:sessionroles_caption', 'csr'), get_string('setting:sessionroles', 'csr'), array(), $choices));


$settings->add(new admin_setting_heading('csr_manageremail_header', get_string('manageremailheading', 'csr'), ''));

$settings->add(new admin_setting_configcheckbox('csr_addchangemanageremail', get_string('setting:addchangemanageremail_caption', 'csr'),get_string('setting:addchangemanageremail', 'csr'), 0));

$settings->add(new admin_setting_configtext('csr_manageraddressformat', get_string('setting:manageraddressformat_caption', 'csr'),get_string('setting:manageraddressformat', 'csr'), get_string('setting:manageraddressformatdefault', 'csr'), PARAM_TEXT));

$settings->add(new admin_setting_configtext('csr_manageraddressformatreadable', get_string('setting:manageraddressformatreadable_caption', 'csr'),get_string('setting:manageraddressformatreadable', 'csr'), get_string('setting:manageraddressformatreadabledefault', 'csr'), PARAM_NOTAGS));


$settings->add(new admin_setting_heading('csr_cost_header', get_string('costheading', 'csr'), ''));

$settings->add(new admin_setting_configcheckbox('csr_hidecost', get_string('setting:hidecost_caption', 'csr'),get_string('setting:hidecost', 'csr'), 0));

$settings->add(new admin_setting_configcheckbox('csr_hidediscount', get_string('setting:hidediscount_caption', 'csr'),get_string('setting:hidediscount', 'csr'), 0));


$settings->add(new admin_setting_heading('csr_icalendar_header', get_string('icalendarheading', 'csr'), ''));

$settings->add(new admin_setting_configcheckbox('csr_oneemailperday', get_string('setting:oneemailperday_caption', 'csr'),get_string('setting:oneemailperday', 'csr'), 0));

$settings->add(new admin_setting_configcheckbox('csr_disableicalcancel', get_string('setting:disableicalcancel_caption', 'csr'),get_string('setting:disableicalcancel', 'csr'), 0));


// List of existing custom fields
$html = csr_list_of_customfields();
$html .= '<p><a href="'.$CFG->wwwroot.'/mod/csr/customfield.php?id=0">' . get_string('addnewfieldlink', 'csr') . '</a></p>';

$settings->add(new admin_setting_heading('csr_customfields_header', get_string('customfieldsheading', 'csr'), $html));

// List of existing site notices
$html = csr_list_of_sitenotices();
$html .= '<p><a href="'.$CFG->wwwroot.'/mod/csr/sitenotice.php?id=0">' . get_string('addnewnoticelink', 'csr') . '</a></p>';

$settings->add(new admin_setting_heading('csr_sitenotices_header', get_string('sitenoticesheading', 'csr'), $html));
