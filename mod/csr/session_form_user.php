<script type="text/javascript" src="jquery-1.12.0.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
$("select[name='timefinish[day]']").change(function(){
var day=$(this).val();
var month=$("select[name='timefinish[month]']").val();
var year=$("select[name='timefinish[year]']").val();
var hour=$("select[name='timefinish[hour]']").val();
var minute=$("select[name='timefinish[minute]']").val();
var ed = new Date(year+'-'+month+'-'+day+' '+hour+':'+minute);
var eday=$("select[name='timestart[day]']").val();
var emonth=$("select[name='timestart[month]']").val();
var eyear=$("select[name='timestart[year]']").val();
var ehour=$("select[name='timestart[hour]']").val();
var eminute=$("select[name='timestart[minute]']").val();
var sd = new Date(eyear+'-'+emonth+'-'+eday+' '+ehour+':'+eminute);
var date1_ms = sd.getTime();
var date2_ms = ed.getTime();
var difference_ms = date2_ms - date1_ms;
if(difference_ms < 0)
{
alert("To Finish Date cannot be smaller than Start Date");
return false;
}
var seconds = (difference_ms / 1000);
var minutes = (seconds / 60);
$("input[name='duration']").val(minutes);
});
$("select[name='timefinish[month]']").change(function(){
var day=$("select[name='timefinish[day]']").val();
var month=$("select[name='timefinish[month]']").val();
var year=$("select[name='timefinish[year]']").val();
var hour=$("select[name='timefinish[hour]']").val();
var minute=$("select[name='timefinish[minute]']").val();
var ed = new Date(year+'-'+month+'-'+day+' '+hour+':'+minute);
var eday=$("select[name='timestart[day]']").val();
var emonth=$("select[name='timestart[month]']").val();
var eyear=$("select[name='timestart[year]']").val();
var ehour=$("select[name='timestart[hour]']").val();
var eminute=$("select[name='timestart[minute]']").val();
var sd = new Date(eyear+'-'+emonth+'-'+eday+' '+ehour+':'+eminute);
var date1_ms = sd.getTime();
var date2_ms = ed.getTime();
var difference_ms = date2_ms - date1_ms;
if(difference_ms < 0)
{
alert("To Finish Date cannot be smaller than Start Date");
return false;
}
var seconds = (difference_ms / 1000);
var minutes = (seconds / 60);
$("input[name='duration']").val(minutes);
});
$("select[name='timefinish[year]']").change(function(){
var day=$("select[name='timefinish[day]']").val();
var month=$("select[name='timefinish[month]']").val();
var year=$("select[name='timefinish[year]']").val();
var hour=$("select[name='timefinish[hour]']").val();
var minute=$("select[name='timefinish[minute]']").val();
var ed = new Date(year+'-'+month+'-'+day+' '+hour+':'+minute);
var eday=$("select[name='timestart[day]']").val();
var emonth=$("select[name='timestart[month]']").val();
var eyear=$("select[name='timestart[year]']").val();
var ehour=$("select[name='timestart[hour]']").val();
var eminute=$("select[name='timestart[minute]']").val();
var sd = new Date(eyear+'-'+emonth+'-'+eday+' '+ehour+':'+eminute);
var date1_ms = sd.getTime();
var date2_ms = ed.getTime();
var difference_ms = date2_ms - date1_ms;
if(difference_ms < 0)
{
alert("To Finish Date cannot be smaller than Start Date");
return false;
}
var seconds = (difference_ms / 1000);
var minutes = (seconds / 60);
$("input[name='duration']").val(minutes);
});
$("select[name='timefinish[hour]']").change(function(){
var day=$("select[name='timefinish[day]']").val();
var month=$("select[name='timefinish[month]']").val();
var year=$("select[name='timefinish[year]']").val();
var hour=$("select[name='timefinish[hour]']").val();
var minute=$("select[name='timefinish[minute]']").val();
var ed = new Date(year+'-'+month+'-'+day+' '+hour+':'+minute);
var eday=$("select[name='timestart[day]']").val();
var emonth=$("select[name='timestart[month]']").val();
var eyear=$("select[name='timestart[year]']").val();
var ehour=$("select[name='timestart[hour]']").val();
var eminute=$("select[name='timestart[minute]']").val();
var sd = new Date(eyear+'-'+emonth+'-'+eday+' '+ehour+':'+eminute);
var date1_ms = sd.getTime();
var date2_ms = ed.getTime();
var difference_ms = date2_ms - date1_ms;
if(difference_ms < 0)
{
alert("To Finish Date cannot be smaller than Start Date");
return false;
}
var seconds = (difference_ms / 1000);
var minutes = (seconds / 60);
$("input[name='duration']").val(minutes);
});
$("select[name='timefinish[minute]']").change(function(){
var day=$("select[name='timefinish[day]']").val();
var month=$("select[name='timefinish[month]']").val();
var year=$("select[name='timefinish[year]']").val();
var hour=$("select[name='timefinish[hour]']").val();
var minute=$("select[name='timefinish[minute]']").val();
var ed = new Date(year+'-'+month+'-'+day+' '+hour+':'+minute);
var eday=$("select[name='timestart[day]']").val();
var emonth=$("select[name='timestart[month]']").val();
var eyear=$("select[name='timestart[year]']").val();
var ehour=$("select[name='timestart[hour]']").val();
var eminute=$("select[name='timestart[minute]']").val();
var sd = new Date(eyear+'-'+emonth+'-'+eday+' '+ehour+':'+eminute);
var date1_ms = sd.getTime();
var date2_ms = ed.getTime();
var difference_ms = date2_ms - date1_ms;
if(difference_ms < 0)
{
alert("To Finish Date cannot be smaller than Start Date");
return false;
}
var seconds = (difference_ms / 1000);
var minutes = (seconds / 60);
$("input[name='duration']").val(minutes);
});
});
</script>

<?php

require_once "$CFG->dirroot/lib/formslib.php";
require_once 'lib.php';

class mod_csr_session_form extends moodleform {

    function definition()
    {
        global $CFG;

        $mform =& $this->_form;

        $mform->addElement('hidden', 'id', $this->_customdata['id']);
        $mform->addElement('hidden', 'f', $this->_customdata['f']);
        $mform->addElement('hidden', 's', $this->_customdata['s']);
        $mform->addElement('hidden', 'c', $this->_customdata['c']);

        $mform->addElement('header', 'general', 'Individual user activity');
		$mform->addElement('text', 'eventname', get_string('eventname', 'csr'), 'size="30"');
		$mform->addElement('text', 'location', get_string('location', 'csr'), 'size="30"');
		$mform->addElement('text', 'venue', get_string('venue', 'csr'), 'size="30"');


        // Hack to put help files on these custom fields.
        // TODO: add to the admin page a feature to put help text on custom fields
        if ($mform->elementExists('custom_location')){
            $mform->setHelpButton('custom_location',array('location',get_string('location','csr'),'csr'));
        }
        if ($mform->elementExists('custom_venue')){
            $mform->setHelpButton('custom_venue',array('venue',get_string('venue','csr'),'csr'));
        }
        if ($mform->elementExists('custom_room')){
            $mform->setHelpButton('custom_room',array('room',get_string('room','csr'),'csr'));
        }


							   
		$mform->addElement('date_time_selector', 'timestart', get_string('timestart', 'csr'));
        $mform->addElement('date_time_selector', 'timefinish', get_string('timefinish', 'csr'));
		 $mform->setType('timestart', PARAM_INT);
        $mform->setType('timefinish', PARAM_INT);
		$script = "onchange='changeContent(this.value)'";
		$category=array();
		$category['']='Select a Category';
		$category['Community Volunteerism']='Community Volunteerism';
		$category['Disaster Relief']='Disaster Relief';
		$category['Internal Programs']='Internal Programs';		
		$mform->addElement('select', 'category', get_string('category', 'csr'), $category,$script);
		$mform->addElement('html', '<div class="fitem"><div class="fitemtitle"><label>Sub Category </label></div><div class="felement fselect" id="qheader"> <select name="subcategory"><option>Select a Sub Category</option></select></div></div>');
		$mform->addElement('hidden', 'subcategory', '');
        $mform->addElement('text', 'duration', get_string('duration', 'csr'), 'size="5"');
        $mform->setType('duration', PARAM_TEXT);
        $mform->setHelpButton('duration', array('duration', get_string('duration', 'csr'), 'csr'));



        $mform->addElement('htmleditor', 'details', get_string('details', 'csr'), '');
        $mform->setType('details', PARAM_RAW);
        $mform->setHelpButton('details', array('details', get_string('details', 'csr'), 'csr'));


        $this->add_action_buttons();
    }

   
}
