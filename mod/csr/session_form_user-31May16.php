

<?php

require_once "$CFG->dirroot/lib/formslib.php";
require_once 'lib.php';

class mod_csr_session_form extends moodleform {

    function definition()
    {
        global $CFG;

        $mform =& $this->_form;

        $mform->addElement('hidden', 'id', $this->_customdata['id']);
        $mform->addElement('hidden', 'f', $this->_customdata['f']);
        $mform->addElement('hidden', 's', $this->_customdata['s']);
        $mform->addElement('hidden', 'c', $this->_customdata['c']);

        $mform->addElement('header', 'general', 'Individual user activity');
		$mform->addElement('text', 'eventname', get_string('eventname', 'csr'), 'size="30"');
		$mform->addElement('text', 'location', get_string('location', 'csr'), 'size="30"');
		$mform->addElement('text', 'venue', get_string('venue', 'csr'), 'size="30"');


        // Hack to put help files on these custom fields.
        // TODO: add to the admin page a feature to put help text on custom fields
        if ($mform->elementExists('custom_location')){
            $mform->setHelpButton('custom_location',array('location',get_string('location','csr'),'csr'));
        }
        if ($mform->elementExists('custom_venue')){
            $mform->setHelpButton('custom_venue',array('venue',get_string('venue','csr'),'csr'));
        }
        if ($mform->elementExists('custom_room')){
            $mform->setHelpButton('custom_room',array('room',get_string('room','csr'),'csr'));
        }


							   
		$mform->addElement('date_time_selector', 'timestart', get_string('timestart', 'csr'));
        $mform->addElement('date_time_selector', 'timefinish', get_string('timefinish', 'csr'));
		 $mform->setType('timestart', PARAM_INT);
        $mform->setType('timefinish', PARAM_INT);
		$script = "onchange='changeContent(this.value)'";
		$category=array();
		$category['']='Select a Category';
		$category['Community Volunteerism']='Community Volunteerism';
		$category['Disaster Relief']='Disaster Relief';
		$category['Internal Programs']='Internal Programs';		
		$mform->addElement('select', 'category', get_string('category', 'csr'), $category,$script);
		$mform->addElement('html', '<div class="fitem"><div class="fitemtitle"><label>Sub Category </label></div><div class="felement fselect" id="qheader"> <select name="subcategory"><option>Select a Sub Category</option></select></div></div>');
		$mform->addElement('hidden', 'subcategory', '');
        $mform->addElement('text', 'duration', get_string('duration', 'csr'), 'size="5"');
        $mform->setType('duration', PARAM_TEXT);
        $mform->setHelpButton('duration', array('duration', get_string('duration', 'csr'), 'csr'));



        $mform->addElement('htmleditor', 'details', get_string('details', 'csr'), '');
        $mform->setType('details', PARAM_RAW);
        $mform->setHelpButton('details', array('details', get_string('details', 'csr'), 'csr'));


        $this->add_action_buttons();
    }

   
}
