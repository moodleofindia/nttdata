<?php

require_once '../../config.php';
require_once 'lib.php';

define('MAX_USERS_PER_PAGE', 5000);

$s              = required_param('s', PARAM_INT); // csr session ID
$add            = optional_param('add', 0, PARAM_BOOL);
$remove         = optional_param('remove', 0, PARAM_BOOL);
$showall        = optional_param('showall', 0, PARAM_BOOL);
$searchtext     = optional_param('searchtext', '', PARAM_RAW); // search string
$suppressemail  = optional_param('suppressemail', false, PARAM_BOOL); // send email notifications
$previoussearch = optional_param('previoussearch', 0, PARAM_BOOL);
$backtoallsessions = optional_param('backtoallsessions', 0, PARAM_INT); // csr activity to go back to

if (!$session = csr_get_session($s)) {
    print_error('error:incorrectcoursemodulesession', 'csr');
}
if (!$csr = get_record('csr', 'id', $session->csr)) {
    print_error('error:incorrectcsrid', 'csr');
}
if (!$course = get_record('course', 'id', $csr->course)) {
    print_error('error:coursemisconfigured', 'csr');
}
if (!$cm = get_coursemodule_from_instance('csr', $csr->id, $course->id)) {
    print_error('error:incorrectcoursemodule', 'csr');
}

/// Check essential permissions
require_course_login($course);
$context = get_context_instance(CONTEXT_COURSE, $course->id);
require_capability('mod/csr:viewattendees', $context);

/// Get some language strings
$strsearch = get_string('search');
$strshowall = get_string('showall');
$strsearchresults = get_string('searchresults');
$strcsrs = get_string('modulenameplural', 'csr');
$strcsr = get_string('modulename', 'csr');

$errors = array();

/// Handle the POST actions sent to the page
if ($frm = data_submitted()) {

    // Add button
    if ($add and !empty($frm->addselect) and confirm_sesskey()) {
        require_capability('mod/csr:addattendees', $context);

        foreach ($frm->addselect as $adduser) {
            if (!$adduser = clean_param($adduser, PARAM_INT)) {
                continue; // invalid userid
            }

            // Make sure that the user is enroled in the course
            if (!has_capability('moodle/course:view', $context, $adduser)) {
                $user = get_record('user', 'id', $adduser);

                if (!enrol_into_course($course, $user, 'manual')) {
                    $errors[] = get_string('error:enrolmentfailed', 'csr', fullname($user));
                    $errors[] = get_string('error:addattendee', 'csr', fullname($user));
                    continue; // don't sign the user up
                }
            }

            if (csr_get_user_session_submissions($session->id, $adduser)) {
                $erruser = get_record('user', 'id', $adduser, '','','','', 'id, firstname, lastname');
                $errors[] = get_string('error:addalreadysignedupattendee', 'csr', fullname($erruser));
            }
            else {
                if (!csr_session_has_capacity($session, $context)) {
                    $errors[] = get_string('full', 'csr');
                    break; // no point in trying to add other people
                }

                // Check if we are waitlisting or booking
                if ($session->datetimeknown) {
                    $status = MDL_CSR_STATUS_BOOKED;
                } else {
                    $status = MDL_CSR_STATUS_WAITLISTED;
                }

                if (!csr_user_signup($session, $csr, $course, '', MDL_CSR_BOTH,
                                                $status, $adduser, !$suppressemail)) {
                    $erruser = get_record('user', 'id', $adduser, '','','','', 'id, firstname, lastname');
                    $errors[] = get_string('error:addattendee', 'csr', fullname($erruser));
                }
            }
        }
    }
    // Remove button
    else if ($remove and !empty($frm->removeselect) and confirm_sesskey()) {
        require_capability('mod/csr:removeattendees', $context);

        foreach ($frm->removeselect as $removeuser) {
            if (!$removeuser = clean_param($removeuser, PARAM_INT)) {
                continue; // invalid userid
            }

            if (csr_user_cancel($session, $removeuser, true, $cancelerr)) {
                // Notify the user of the cancellation if the session hasn't started yet
                $timenow = time();
                if (!$suppressemail and !csr_has_session_started($session, $timenow)) {
                    csr_send_cancellation_notice($csr, $session, $removeuser);
                }
            }
            else {
                $errors[] = $cancelerr;
                $erruser = get_record('user', 'id', $removeuser, '','','','', 'id, firstname, lastname');
                $errors[] = get_string('error:removeattendee', 'csr', fullname($erruser));
            }
        }

        // Update attendees
        csr_update_attendees($session);
    }
    // "Show All" button
    elseif ($showall) {
        $searchtext = '';
        $previoussearch = 0;
    }
}

/// Main page
$pagetitle = format_string($csr->name);
$navlinks[] = array('name' => $strcsrs, 'link' => "index.php?id=$course->id", 'type' => 'title');
$navlinks[] = array('name' => $pagetitle, 'link' => "view.php?f=$csr->id", 'type' => 'activityinstance');
$navlinks[] = array('name' => get_string('attendees', 'csr'), 'link' => "attendees.php?s=$session->id", 'type' => 'activityinstance');
$navlinks[] = array('name' => get_string('addremoveattendees', 'csr'), 'link' => '', 'type' => 'title');
$navigation = build_navigation($navlinks);
print_header_simple($pagetitle, '', $navigation, '', '', true,
                    update_module_button($cm->id, $course->id, $strcsr), navmenu($course, $cm));

print_box_start();
print_heading(get_string('addremoveattendees', 'csr'), 'center');

/// Get the list of currently signed-up users
$existingusers = csr_get_attendees($session->id);
$existingcount = $existingusers ? count($existingusers) : 0;

$select  = "username <> 'guest' AND deleted = 0 AND confirmed = 1";

/// Apply search terms
$searchtext = trim($searchtext);
if ($searchtext !== '') {   // Search for a subset of remaining users
    $LIKE      = sql_ilike();
    $FULLNAME  = sql_fullname();

    $selectsql = " AND ($FULLNAME $LIKE '%$searchtext%' OR
                            email $LIKE '%$searchtext%' OR
                         idnumber $LIKE '%$searchtext%' OR
                         username $LIKE '%$searchtext%') ";
    $select  .= $selectsql;
}

/// All non-signed up system users
$availableusers = get_recordset_sql('SELECT id, firstname, lastname, email
                                       FROM '.$CFG->prefix.'user
                                      WHERE '.$select.'
                                        AND id NOT IN
                                          (
                                            SELECT u.id
                                              FROM '.$CFG->prefix.'csr_signups s
                                              JOIN '.$CFG->prefix.'csr_signups_status ss ON s.id = ss.signupid
                                              JOIN '.$CFG->prefix.'user u ON u.id=s.userid
                                             WHERE s.sessionid='.$session->id.'
                                               AND ss.statuscode >= '.MDL_CSR_STATUS_BOOKED.'
                                               AND ss.superceded = 0
                                          )
                                          ORDER BY lastname ASC, firstname ASC');

$usercount = count_records_select('user', $select) - $existingcount;


// Get all signed up non-attendees
$nonattendees = 0;
$nonattendees_rs = get_recordset_sql(
    "
        SELECT
            u.id,
            u.firstname,
            u.lastname,
            u.email,
            ss.statuscode
        FROM
            {$CFG->prefix}csr_sessions s
        JOIN
            {$CFG->prefix}csr_signups su
         ON s.id = su.sessionid
        JOIN
            {$CFG->prefix}csr_signups_status ss
         ON su.id = ss.signupid
        JOIN
            {$CFG->prefix}user u
         ON u.id = su.userid
        WHERE
            s.id = {$session->id}
        AND ss.superceded != 1
        AND ss.statuscode = ".MDL_CSR_STATUS_REQUESTED."
        ORDER BY
            u.lastname, u.firstname
    "
);

$table = new object();
$table->head = array(get_string('name'), get_string('email'), get_string('status'));
$table->align = array('left');
$table->size = array('50%');
$table->width = '70%';

while ($user = rs_fetch_next_record($nonattendees_rs)) {
    $data = array();
    $data[] = fullname($user);
    $data[] = $user->email;
    $data[] = get_string('status_'.csr_get_status($user->statuscode), 'csr');

    $table->data[] = $data;
    $nonattendees++;
}


/// Prints a form to add/remove users from the session
include('editattendees.html');

if (!empty($errors)) {
    $msg = '<p>';
    foreach ($errors as $e) {
        $msg .= $e.'<br />';
    }
    $msg .= '</p>';
    print_simple_box_start('center');
    notify($msg);
    print_simple_box_end();
}

// Bottom of the page links
print '<p style="text-align: center">';
$url = $CFG->wwwroot.'/mod/csr/attendees.php?s='.$session->id.'&amp;backtoallsessions='.$backtoallsessions;
print '<a href="'.$url.'">'.get_string('goback', 'csr').'</a></p>';

print_box_end();
print_footer($course);
