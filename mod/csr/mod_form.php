<?php

require_once "$CFG->dirroot/course/moodleform_mod.php";

class mod_csr_mod_form extends moodleform_mod {

    function definition()
    {
        global $CFG;

        $mform =& $this->_form;

        // GENERAL
        $mform->addElement('header', 'general', get_string('general', 'form'));

        $mform->addElement('text', 'name', get_string('name'), array('size'=>'64'));
        if (!empty($CFG->formatstringstriptags)) {
            $mform->setType('name', PARAM_TEXT);
        } else {
            $mform->setType('name', PARAM_CLEAN);
        }
        $mform->addRule('name', null, 'required', null, 'client');

        $mform->addElement('htmleditor', 'description', get_string('description'), array('rows'  => 10, 'cols'  => 64));
        $mform->setType('description', PARAM_RAW);
        $mform->setHelpButton('description', array('description', get_string('description'), 'csr'));

		
		$mform->addElement('htmleditor', 'guidelines', get_string('guidelines'), array('rows'  => 10, 'cols'  => 64));
        $mform->setType('guidelines', PARAM_RAW);
        $mform->addElement('text', 'thirdparty', get_string('thirdpartyemailaddress', 'csr'), array('size'=>'64'));
        $mform->setType('name', PARAM_NOTAGS);
        $mform->setHelpButton('thirdparty', array('thirdpartyemailaddress', get_string('thirdpartyemailaddress', 'csr'), 'csr'));

        $mform->addElement('checkbox', 'thirdpartywaitlist', get_string('thirdpartywaitlist', 'csr'));
        $mform->setHelpButton('thirdpartywaitlist', array('thirdpartywaitlist', get_string('thirdpartywaitlist', 'csr'), 'csr'));

        $display = array();
        for ($i=0; $i<=18; $i += 2) {
            $display[] = $i;
        }
        $mform->addElement('select', 'display', get_string('sessionsoncoursepage', 'csr'), $display);
        $mform->setDefault('display', 3); // 3th element is 6
        $mform->setHelpButton('display', array('sessionsoncoursepage', get_string('sessionsoncoursepage', 'csr'), 'csr'));

        $mform->addElement('checkbox', 'approvalreqd', get_string('approvalreqd', 'csr'));
        $mform->setHelpButton('approvalreqd', array('approvalreqd', get_string('approvalreqd','csr'), 'csr'));

        $mform->addElement('header', 'calendaroptions', get_string('calendaroptions', 'csr'));

        $mform->addElement('checkbox', 'showoncalendar', get_string('showoncalendar', 'csr'));
        $mform->setDefault('showoncalendar', true);
        $mform->setHelpButton('showoncalendar', array('showoncalendar', get_string('showoncalendar', 'csr'), 'csr'));

        $mform->addElement('text', 'shortname', get_string('shortname'), array('size' => 32, 'maxlength' => 32));
        $mform->setType('shortname', PARAM_TEXT);
        $mform->setHelpButton('shortname', array('shortname', get_string('shortname'), 'csr'));
        $mform->disabledIf('shortname', 'showoncalendar');
        $mform->addRule('shortname', null, 'maxlength', 32);

        // REQUEST MESSAGE
        $mform->addElement('header', 'request', get_string('requestmessage', 'csr'));
        $mform->setHelpButton('request', array('requestmessage', get_string('requestmessage', 'csr'), 'csr'));

        $mform->addElement('text', 'requestsubject', get_string('email:subject', 'csr'), array('size'=>'55'));
        $mform->setType('requestsubject', PARAM_TEXT);
        $mform->setDefault('requestsubject', get_string('setting:defaultrequestsubjectdefault', 'csr'));
        $mform->disabledIf('requestsubject', 'approvalreqd');

        $mform->addElement('textarea', 'requestmessage', get_string('email:message', 'csr'), 'wrap="virtual" rows="15" cols="70"');
        $mform->setDefault('requestmessage', get_string('setting:defaultrequestmessagedefault', 'csr'));
        $mform->disabledIf('requestmessage', 'approvalreqd');

        $mform->addElement('textarea', 'requestinstrmngr', get_string('email:instrmngr', 'csr'), 'wrap="virtual" rows="10" cols="70"');
        $mform->setDefault('requestinstrmngr', get_string('setting:defaultrequestinstrmngrdefault', 'csr'));
        $mform->disabledIf('requestinstrmngr', 'approvalreqd');

        // CONFIRMATION MESSAGE
        $mform->addElement('header', 'confirmation', get_string('confirmationmessage', 'csr'));
        $mform->setHelpButton('confirmation', array('confirmationmessage', get_string('confirmationmessage', 'csr'), 'csr'));

        $mform->addElement('text', 'confirmationsubject', get_string('email:subject', 'csr'), array('size'=>'55'));
        $mform->setType('confirmationsubject', PARAM_TEXT);
        $mform->setDefault('confirmationsubject', get_string('setting:defaultconfirmationsubjectdefault', 'csr'));

        $mform->addElement('textarea', 'confirmationmessage', get_string('email:message', 'csr'), 'wrap="virtual" rows="15" cols="70"');
        $mform->setDefault('confirmationmessage', get_string('setting:defaultconfirmationmessagedefault', 'csr'));

        $mform->addElement('checkbox', 'emailmanagerconfirmation', get_string('emailmanager', 'csr'));
        $mform->setHelpButton('emailmanagerconfirmation', array('emailmanagerconfirmation', get_string('emailmanager','csr'), 'csr'));

        $mform->addElement('textarea', 'confirmationinstrmngr', get_string('email:instrmngr', 'csr'), 'wrap="virtual" rows="4" cols="70"');
        $mform->setHelpButton('confirmationinstrmngr',array('confirmationinstrmngr',get_string('email:instrmngr','csr'),'csr'));
        $mform->disabledIf('confirmationinstrmngr', 'emailmanagerconfirmation');
        $mform->setDefault('confirmationinstrmngr', get_string('setting:defaultconfirmationinstrmngrdefault', 'csr'));

        // REMINDER MESSAGE
        $mform->addElement('header', 'reminder', get_string('remindermessage', 'csr'));
        $mform->setHelpButton('reminder', array('remindermessage', get_string('remindermessage', 'csr'), 'csr'));

        $mform->addElement('text', 'remindersubject', get_string('email:subject', 'csr'), array('size'=>'55'));
        $mform->setType('remindersubject', PARAM_TEXT);
        $mform->setDefault('remindersubject', get_string('setting:defaultremindersubjectdefault', 'csr'));

        $mform->addElement('textarea', 'remindermessage', get_string('email:message', 'csr'), 'wrap="virtual" rows="15" cols="70"');
        $mform->setDefault('remindermessage', get_string('setting:defaultremindermessagedefault', 'csr'));

        $mform->addElement('checkbox', 'emailmanagerreminder', get_string('emailmanager', 'csr'));
        $mform->setHelpButton('emailmanagerreminder',array('emailmanagerreminder',get_string('emailmanager','csr'),'csr'));

        $mform->addElement('textarea', 'reminderinstrmngr', get_string('email:instrmngr', 'csr'), 'wrap="virtual" rows="4" cols="70"');
        $mform->setHelpButton('reminderinstrmngr',array('reminderinstrmngr',get_string('email:instrmngr','csr'),'csr'));
        $mform->disabledIf('reminderinstrmngr', 'emailmanagerreminder');
        $mform->setDefault('reminderinstrmngr', get_string('setting:defaultreminderinstrmngrdefault', 'csr'));

        $reminderperiod = array();
        for ($i=1; $i<=20; $i += 1) {
            $reminderperiod[$i] = $i;
        }
        $mform->addElement('select', 'reminderperiod', get_string('reminderperiod', 'csr'), $reminderperiod);
        $mform->setDefault('reminderperiod', 2);
        $mform->setHelpButton('reminderperiod', array('reminderperiod', get_string('reminderperiod', 'csr'), 'csr'));

        // WAITLISTED MESSAGE
        $mform->addElement('header', 'waitlisted', get_string('waitlistedmessage', 'csr'));
        $mform->setHelpButton('waitlisted', array('waitlistedmessage', get_string('waitlistedmessage', 'csr'), 'csr'));

        $mform->addElement('text', 'waitlistedsubject', get_string('email:subject', 'csr'), array('size'=>'55'));
        $mform->setType('waitlistedsubject', PARAM_TEXT);
        $mform->setDefault('waitlistedsubject', get_string('setting:defaultwaitlistedsubjectdefault', 'csr'));

        $mform->addElement('textarea', 'waitlistedmessage', get_string('email:message', 'csr'), 'wrap="virtual" rows="15" cols="70"');
        $mform->setDefault('waitlistedmessage', get_string('setting:defaultwaitlistedmessagedefault', 'csr'));

        // CANCELLATION MESSAGE
        $mform->addElement('header', 'cancellation', get_string('cancellationmessage', 'csr'));
        $mform->setHelpButton('cancellation', array('cancellationmessage', get_string('cancellationmessage', 'csr'), 'csr'));

        $mform->addElement('text', 'cancellationsubject', get_string('email:subject', 'csr'), array('size'=>'55'));
        $mform->setType('cancellationsubject', PARAM_TEXT);
        $mform->setDefault('cancellationsubject', get_string('setting:defaultcancellationsubjectdefault', 'csr'));

        $mform->addElement('textarea', 'cancellationmessage', get_string('email:message', 'csr'), 'wrap="virtual" rows="15" cols="70"');
        $mform->setDefault('cancellationmessage', get_string('setting:defaultcancellationmessagedefault', 'csr'));

        $mform->addElement('checkbox', 'emailmanagercancellation', get_string('emailmanager', 'csr'));
        $mform->setHelpButton('emailmanagercancellation',array('emailmanagercancellation',get_string('emailmanager','csr'),'csr'));

        $mform->addElement('textarea', 'cancellationinstrmngr', get_string('email:instrmngr', 'csr'), 'wrap="virtual" rows="4" cols="70"');
        $mform->setHelpButton('cancellationinstrmngr',array('cancellationinstrmngr',get_string('email:instrmngr','csr'),'csr'));
        $mform->disabledIf('cancellationinstrmngr', 'emailmanagercancellation');
        $mform->setDefault('cancellationinstrmngr', get_string('setting:defaultcancellationinstrmngrdefault', 'csr'));

        $features = new stdClass;
        $features->groups = false;
        $features->groupings = false;
        $features->groupmembersonly = false;
        $features->outcomes = false;
        $features->gradecat = false;
        $features->idnumber = true;
        $this->standard_coursemodule_elements($features);

        $this->add_action_buttons();
    }

    function data_preprocessing(&$default_values)
    {
        // Fix manager emails
        if (empty($default_values['confirmationinstrmngr'])) {
            $default_values['confirmationinstrmngr'] = null;
        }
        else {
            $default_values['emailmanagerconfirmation'] = 1;
        }

        if (empty($default_values['reminderinstrmngr'])) {
            $default_values['reminderinstrmngr'] = null;
        }
        else {
            $default_values['emailmanagerreminder'] = 1;
        }

        if (empty($default_values['cancellationinstrmngr'])) {
            $default_values['cancellationinstrmngr'] = null;
        }
        else {
            $default_values['emailmanagercancellation'] = 1;
        }
    }
}
