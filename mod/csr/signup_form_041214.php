<?php

require_once "$CFG->dirroot/lib/formslib.php";

class mod_csr_signup_form extends moodleform {

    function definition()
    {
        $mform =& $this->_form;
        $manageremail = $this->_customdata['manageremail'];
        $showdiscountcode = $this->_customdata['showdiscountcode'];

        $mform->addElement('hidden', 's', $this->_customdata['s']);
        $mform->addElement('hidden', 'backtoallsessions', $this->_customdata['backtoallsessions']);

        if ($manageremail === false) {
            $mform->addElement('hidden', 'manageremail', '');
        }
        else {
            $mform->addElement('html', get_string('manageremailinstructionconfirm', 'csr')); // instructions

            $mform->addElement('text', 'manageremail', get_string('manageremail', 'csr'), 'size="35"');
            $mform->addRule('manageremail', null, 'required', null, 'client');
            $mform->addRule('manageremail', null, 'email', null, 'client');
            $mform->setType('manageremail', PARAM_TEXT);
        }

        if ($showdiscountcode) {
            $mform->addElement('text', 'discountcode', get_string('discountcode', 'csr'), 'size="6"');
            $mform->addRule('discountcode', null, 'required', null, 'client');
            $mform->setType('discountcode', PARAM_TEXT);
        }
        else {
            $mform->addElement('hidden', 'discountcode', '');
        }

        $options = array(MDL_CSR_ICAL => get_string('notificationical', 'csr'),
						MDL_CSR_BOTH => get_string('notificationboth', 'csr'),
                        MDL_CSR_TEXT => get_string('notificationemail', 'csr')
                         );
        $mform->addElement('select', 'notificationtype', get_string('notificationtype', 'csr'), $options);
        $mform->setHelpButton('notificationtype',array('notificationtype',get_string('notificationtype','csr'),'csr'));
        $mform->addRule('notificationtype', null, 'required', null, 'client');
        $mform->setDefault('notificationtype', 0);

        $this->add_action_buttons(true, get_string('signup', 'csr'));
    }

    function validation($data, $files)
    {
        $errors = parent::validation($data, $files);

        $manageremail = $data['manageremail'];
        if (!empty($manageremail)) {
            if (!csr_check_manageremail($manageremail)) {
                $errors['manageremail'] = csr_get_manageremailformat();
            }
        }

        return $errors;
    }
}
