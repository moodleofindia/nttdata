<?php

/**
 * API function called by the Moodle restore system
 */
function csr_restore_mods($mod, $restore)
{
    global $CFG;

    $status = true;

    $data = backup_getid($restore->backup_unique_code, $mod->modtype, $mod->id);
    if ($data) {
        $info = $data->info;

        $csr->course                = $restore->course_id;
        $csr->name                  = backup_todb($info['MOD']['#']['NAME']['0']['#']);
        $csr->thirdparty            = backup_todb($info['MOD']['#']['THIRDPARTY']['0']['#']);
        $csr->display               = backup_todb($info['MOD']['#']['DISPLAY']['0']['#']);
        $csr->confirmationsubject   = backup_todb($info['MOD']['#']['CONFIRMATIONSUBJECT']['0']['#']);
        $csr->confirmationinstrmngr = backup_todb($info['MOD']['#']['CONFIRMATIONINSTRMNGR']['0']['#']);
        $csr->confirmationmessage   = backup_todb($info['MOD']['#']['CONFIRMATIONMESSAGE']['0']['#']);
        $csr->waitlistedsubject     = backup_todb($info['MOD']['#']['WAITLISTEDSUBJECT']['0']['#']);
        $csr->waitlistedmessage     = backup_todb($info['MOD']['#']['WAITLISTEDMESSAGE']['0']['#']);
        $csr->cancellationsubject   = backup_todb($info['MOD']['#']['CANCELLATIONSUBJECT']['0']['#']);
        $csr->cancellationinstrmngr = backup_todb($info['MOD']['#']['CANCELLATIONINSTRMNGR']['0']['#']);
        $csr->cancellationmessage   = backup_todb($info['MOD']['#']['CANCELLATIONMESSAGE']['0']['#']);
        $csr->remindersubject       = backup_todb($info['MOD']['#']['REMINDERSUBJECT']['0']['#']);
        $csr->reminderinstrmngr     = backup_todb($info['MOD']['#']['REMINDERINSTRMNGR']['0']['#']);
        $csr->remindermessage       = backup_todb($info['MOD']['#']['REMINDERMESSAGE']['0']['#']);
        $csr->reminderperiod        = backup_todb($info['MOD']['#']['REMINDERPERIOD']['0']['#']);
        $csr->timecreated           = backup_todb($info['MOD']['#']['TIMECREATED']['0']['#']);
        $csr->timemodified          = backup_todb($info['MOD']['#']['TIMEMODIFIED']['0']['#']);

        $newid = insert_record ('csr', $csr);

        if (!defined('RESTORE_SILENTLY')) {
            echo '<li>'.get_string('modulename','csr').' "'.format_string(stripslashes($csr->name),true).'"</li>';
        }
        backup_flush(300);

        if ($newid) {
            backup_putid($restore->backup_unique_code, $mod->modtype, $mod->id, $newid);

            // Table: csr_sessions
            $status &= restore_csr_sessions($newid, $info, $restore);

            if (restore_userdata_selected($restore, 'csr', $mod->id)) {
                if (!defined('RESTORE_SILENTLY')) {
                    echo '<br />';
                }

                // Table: csr_signups_
                $status &= restore_csr_signups($newid, $info, $restore);
            }
        }
        else {
            $status = false;
        }
    }
    else {
        $status = false;
    }

    return $status;
}

/**
 * Restore the csr_signups table entries for a given
 * csr activity
 *
 * @param integer $newcsrid ID of the csr activity we're creating
 */
function restore_csr_signups($newcsrid, $info, $restore)
{
    $status = true;

    if (empty($info['MOD']['#']['SIGNUPS'])) {
        return $status; // Nothing to restore
    }

    $signups = $info['MOD']['#']['SIGNUPS']['0']['#']['SIGNUP'];
    foreach ($signups as $signupinfo) {
        $oldid = backup_todb($signupinfo['#']['ID']['0']['#']);

        $signup = new Object();
        $signup->sessionid          = backup_todb($signupinfo['#']['SESSIONID']['0']['#']);
        $signup->userid             = backup_todb($signupinfo['#']['USERID']['0']['#']);
        $signup->mailedreminder     = backup_todb($signupinfo['#']['MAILEDREMINDER']['0']['#']);
        $signup->discountcode       = backup_todb($signupinfo['#']['DISCOUNTCODE']['0']['#']);
        $signup->notificationtype   = backup_todb($signupinfo['#']['NOTIFICATIONTYPE']['0']['#']);

        // Fix the sessionid
        $session = backup_getid($restore->backup_unique_code, 'csr_sessions', $signup->sessionid);
        if ($session) {
            $signup->sessionid = $session->new_id;
        }

        // Fix the userid
        $user = backup_getid($restore->backup_unique_code, 'user', $signup->userid);
        if ($user) {
            $signup->userid = $user->new_id;
        }

        // Fix the discount code
        if (empty($signup->discountcode)) {
            $signup->discountcode = null;
        }

        $newid = insert_record('csr_signups', $signup);

        // Progress bar
        if (!defined('RESTORE_SILENTLY')) {
            if ($newid) {
                echo '.';
            }
            else {
                echo 'X';
            }
        }
        backup_flush(300);

        if ($newid) {
            backup_putid($restore->backup_unique_code, 'csr_signups', $oldid, $newid);
        }
        else {
            $status = false;
        }

        $status &= restore_csr_signups_status($signupinfo, $restore);
    }

    return $status;
}

/**
 * Restore the csr_signups_status table entries for a given
 * csr activity
 */
function restore_csr_signups_status($info, $restore) {

    $status = true;

    if (empty($info['#']['SIGNUPS_STATUS'])) {
        return $status; // Nothing to restore
    }

    $signups = $info['#']['SIGNUPS_STATUS']['0']['#']['STATUS'];
    foreach ($signups as $signupinfo) {

        $signup = new Object();
        $signup->signupid           = backup_todb($signupinfo['#']['SIGNUPID']['0']['#']);
        $signup->statuscode         = backup_todb($signupinfo['#']['STATUSCODE']['0']['#']);
        $signup->superceded         = backup_todb($signupinfo['#']['SUPERCEDED']['0']['#']);
        $signup->grade              = backup_todb($signupinfo['#']['GRADE']['0']['#']);
        $signup->note               = backup_todb($signupinfo['#']['NOTE']['0']['#']);
        $signup->advice             = backup_todb($signupinfo['#']['ADVICE']['0']['#']);
        $signup->createdby          = backup_todb($signupinfo['#']['CREATEDBY']['0']['#']);
        $signup->timecreated        = backup_todb($signupinfo['#']['TIMECREATED']['0']['#']);
        $signup->mailed = 0;

        // Fix the signupid
        $signupid = backup_getid($restore->backup_unique_code, 'csr_signups', $signup->signupid);
        $signup->signupid = $signupid->new_id;

        $newid = insert_record('csr_signups_status', $signup);

        // Progress bar
        if (!defined('RESTORE_SILENTLY')) {
            if ($newid) {
                echo '.';
            } else {
                echo 'X';
            }
        }
        backup_flush(300);

        if (!$newid) {
            $status = false;
        }
    }

    return $status;
}

/**
 * Restore the csr_sessions table entries for a given
 * csr activity
 *
 * @param integer $newcsrid ID of the csr activity we're creating
 */
function restore_csr_sessions($newcsrid, $info, $restore)
{
    $status = true;

    if (empty($info['MOD']['#']['SESSIONS'])) {
        return $status; // Nothing to restore
    }

    $sessions = $info['MOD']['#']['SESSIONS']['0']['#']['SESSION'];
    foreach ($sessions as $sessioninfo) {
        $oldid = backup_todb($sessioninfo['#']['ID']['0']['#']);

        $session->csr    = $newcsrid;
        $session->capacity      = backup_todb($sessioninfo['#']['CAPACITY']['0']['#']);
        $session->details       = backup_todb($sessioninfo['#']['DETAILS']['0']['#']);
        $session->datetimeknown = backup_todb($sessioninfo['#']['DATETIMEKNOWN']['0']['#']);
        $session->duration      = backup_todb($sessioninfo['#']['DURATION']['0']['#']);
        $session->normalcost    = backup_todb($sessioninfo['#']['NORMALCOST']['0']['#']);
        $session->discountcost  = backup_todb($sessioninfo['#']['DISCOUNTCOST']['0']['#']);
        $session->timecreated   = backup_todb($sessioninfo['#']['TIMECREATED']['0']['#']);
        $session->timemodified  = backup_todb($sessioninfo['#']['TIMEMODIFIED']['0']['#']);

        $newid = insert_record('csr_sessions', $session);

        // Progress bar
        if (!defined('RESTORE_SILENTLY')) {
            if ($newid) {
                echo '.';
            }
            else {
                echo 'X';
            }
        }
        backup_flush(300);

        if ($newid) {
            backup_putid($restore->backup_unique_code, 'csr_sessions', $oldid, $newid);

            // Table: csr_session_roles
            $status &= restore_csr_session_roles($newid, $sessioninfo, $restore);

            // Table: csr_sessions_dates
            $status &= restore_csr_sessions_dates($newid, $sessioninfo, $restore);

            // Table: csr_session_data
            $status &= restore_csr_session_data($newid, $sessioninfo, $restore);
        }
        else {
            $status = false;
        }
    }

    return $status;
}

/**
 * Restore the csr_session_roles table entries for a given
 * csr session
 *
 * @param integer $newsessionid ID of the session we are creating
 */
function restore_csr_session_roles($newsessionid, $sessioninfo, $restore) {

    $status = true;

    if (empty($sessioninfo['#']['ROLES'])) {
        return $status; // Nothing to restore
    }

    $roles = $sessioninfo['#']['ROLES']['0']['#']['ROLE'];
    foreach ($roles as $roleinfo) {
        $oldid = backup_todb($roleinfo['#']['ID']['0']['#']);

        $role = new Object();
        $role->sessionid  = $newsessionid;
        $role->roleid  = backup_todb($roleinfo['#']['ROLEID']['0']['#']);
        $role->userid = backup_todb($roleinfo['#']['USERID']['0']['#']);

        $newid = insert_record('csr_session_roles', $role);

        // Progress bar
        if (!defined('RESTORE_SILENTLY')) {
            if ($newid) {
                echo '.';
            } else {
                echo 'X';
            }
        }
        backup_flush(300);

        if ($newid) {
            backup_putid($restore->backup_unique_code, 'csr_session_roles', $oldid, $newid);
        } else {
            $status = false;
        }
    }

    return $status;
}

/**
 * Restore the csr_sessions_dates table entries for a given
 * csr session
 *
 * @param integer $newsessionid ID of the session we are creating
 */
function restore_csr_sessions_dates($newsessionid, $sessioninfo, $restore)
{
    $status = true;

    if (empty($sessioninfo['#']['DATES'])) {
        return $status; // Nothing to restore
    }

    $dates = $sessioninfo['#']['DATES']['0']['#']['DATE'];
    foreach ($dates as $dateinfo) {
        $oldid = backup_todb($dateinfo['#']['ID']['0']['#']);

        $date = new object();
        $date->sessionid  = $newsessionid;
        $date->timestart  = backup_todb($dateinfo['#']['TIMESTART']['0']['#']);
        $date->timefinish = backup_todb($dateinfo['#']['TIMEFINISH']['0']['#']);

        $newid = insert_record('csr_sessions_dates', $date);

        // Progress bar
        if (!defined('RESTORE_SILENTLY')) {
            if ($newid) {
                echo '.';
            }
            else {
                echo 'X';
            }
        }
        backup_flush(300);

        if ($newid) {
            backup_putid($restore->backup_unique_code, 'csr_sessions_dates', $oldid, $newid);
        }
        else {
            $status = false;
        }
    }

    return $status;
}

/**
 * Restore the csr_session_data table entries for a given
 * csr session
 *
 * @param integer $newsessionid ID of the session we are creating
 */
function restore_csr_session_data($newsessionid, $sessioninfo, $restore)
{
    $status = true;

    if (empty($sessioninfo['#']['DATA'])) {
        return $status; // Nothing to restore
    }

    $fieldids = get_records('csr_session_field', '', '', '', 'shortname, id');

    $data = $sessioninfo['#']['DATA']['0']['#']['DATUM'];
    foreach ($data as $datuminfo) {
        $fieldshortname = backup_todb($datuminfo['#']['FIELDSHORTNAME']['0']['#']);

        if (empty($fieldids[$fieldshortname])) {
            // Custom field not defined on destination site
            if (!defined('RESTORE_SILENTLY')) {
                echo 'S';
            }
            continue;
        }

        $oldid = backup_todb($datuminfo['#']['ID']['0']['#']);

        $datum = new object();
        $datum->sessionid = $newsessionid;
        $datum->fieldid   = $fieldids[$fieldshortname]->id;
        $datum->data      = backup_todb($datuminfo['#']['DATA']['0']['#']);

        $newid = insert_record('csr_session_data', $datum);

        // Progress bar
        if (!defined('RESTORE_SILENTLY')) {
            if ($newid) {
                echo '.';
            }
            else {
                echo 'X';
            }
        }
        backup_flush(300);

        if ($newid) {
            backup_putid($restore->backup_unique_code, 'csr_session_data', $oldid, $newid);
        }
        else {
            $status = false;
        }
    }

    return $status;
}
