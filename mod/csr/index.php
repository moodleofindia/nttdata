<?php

require_once '../../config.php';
require_once 'lib.php';

$id = required_param('id', PARAM_INT); // Course Module ID

if (!$course = get_record('course', 'id', $id)) {
    print_error('error:coursemisconfigured', 'csr');
}

require_course_login($course);
$context = get_context_instance(CONTEXT_COURSE, $course->id);
require_capability('mod/csr:view', $context);

add_to_log($course->id, 'csr', 'view all', "index.php?id=$course->id");

$strcsrs = get_string('modulenameplural', 'csr');
$strcsr = get_string('modulename', 'csr');
$strcsrname = get_string('csrname', 'csr');
$strweek = get_string('week');
$strtopic = get_string('topic');
$strcourse = get_string('course');
$strname = get_string('name');

$pagetitle = format_string($strcsrs);
$navlinks[] = array('name' => $pagetitle, 'link' => '', 'type' => 'title');
$navigation = build_navigation($navlinks);
print_header_simple($pagetitle, '', $navigation, '', '', true, '', navmenu($course));

if (!$csrs = get_all_instances_in_course('csr', $course)) {
    notice(get_string('nocsrs', 'csr'), "../../course/view.php?id=$course->id");
    die;
}

$timenow = time();

if ($course->format == 'weeks' && has_capability('mod/csr:viewattendees', $context)) {
    $table->head  = array ($strweek, $strcsrname, get_string('sign-ups', 'csr'));
    $table->align = array ('center', 'left', 'center');
}
elseif ($course->format == 'weeks') {
    $table->head  = array ($strweek, $strcsrname);
    $table->align = array ('center', 'left', 'center', 'center');
}
elseif ($course->format == 'topics' && has_capability('mod/csr:viewattendees', $context)) {
    $table->head  = array ($strcourse, $strcsrname, get_string('sign-ups', 'csr'));
    $table->align = array ('center', 'left', 'center');
}
elseif ($course->format == 'topics') {
    $table->head  = array ($strcourse, $strcsrname);
    $table->align = array ('center', 'left', 'center', 'center');
}
else {
    $table->head  = array ($strcsrname);
    $table->align = array ('left', 'left');
}

$currentsection = '';

foreach ($csrs as $csr) {

    $submitted = get_string('no');

    if (!$csr->visible) {
        //Show dimmed if the mod is hidden
        $link = "<a class=\"dimmed\" href=\"view.php?f=$csr->id\">$csr->name</a>";
    }
    else {
        //Show normal if the mod is visible
        $link = "<a href=\"view.php?f=$csr->id\">$csr->name</a>";
    }

    $printsection = '';
    if ($csr->section !== $currentsection) {
        if ($csr->section) {
            $printsection = $csr->section;
        }
        $currentsection = $csr->section;
    }

    $totalsignupcount = 0;
    if ($sessions = csr_get_sessions($csr->id)) {
        foreach($sessions as $session) {
            if (!csr_has_session_started($session, $timenow)) {
                $signupcount = csr_get_num_attendees($session->id);
                $totalsignupcount += $signupcount;
            }
        }
    }
        
    $courselink = '<a title="'.$course->shortname.'" href="'.$CFG->wwwroot.'/course/view.php?id='.$course->id.'">'.$course->shortname.'</a>';
    if ($course->format == 'weeks' or $course->format == 'topics') {
        if (has_capability('mod/csr:viewattendees', $context)) {
            $table->data[] = array ($courselink, $link, $totalsignupcount);
        }
        else {
            $table->data[] = array ($courselink, $link);
        }
    }
    else {
        $table->data[] = array ($link, $submitted);
    }
}

echo "<br />";

print_table($table);
print_footer($course);
