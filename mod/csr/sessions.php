<?php

require_once '../../config.php';
require_once 'lib.php';
require_once 'session_form.php';

$id = optional_param('id', 0, PARAM_INT); // Course Module ID
$f = optional_param('f', 0, PARAM_INT); // csr Module ID
$s = optional_param('s', 0, PARAM_INT); // csr session ID
$c = optional_param('c', 0, PARAM_INT); // copy session
$d = optional_param('d', 0, PARAM_INT); // delete session
$confirm = optional_param('confirm', false, PARAM_BOOL); // delete confirmation

$nbdays = 1; // default number to show

$session = null;
if ($id) {
    if (!$cm = get_record('course_modules', 'id', $id)) {
        error(get_string('error:incorrectcoursemoduleid', 'csr'));
    }
    if (!$course = get_record('course', 'id', $cm->course)) {
        error(get_string('error:coursemisconfigured', 'csr'));
    }
    if (!$csr = get_record('csr', 'id', $cm->instance)) {
        error(get_string('error:incorrectcoursemodule', 'csr'));
    }
}
elseif ($s) {
     if (!$session = csr_get_session($s)) {
         error(get_string('error:incorrectcoursemodulesession', 'csr'));
     }
     if (!$csr = get_record('csr', 'id', $session->csr)) {
         error(get_string('error:incorrectcsrid', 'csr'));
     }
     if (!$course = get_record('course', 'id', $csr->course)) {
         error(get_string('error:coursemisconfigured', 'csr'));
     }
     if (!$cm = get_coursemodule_from_instance('csr', $csr->id, $course->id)) {
         error(get_string('error:incorrectcoursemoduleid', 'csr'));
     }

     $nbdays = count($session->sessiondates);
}
else {
    if (!$csr = get_record('csr', 'id', $f)) {
        error(get_string('error:incorrectcsrid', 'csr'));
    }
    if (!$course = get_record('course', 'id', $csr->course)) {
        error(get_string('error:coursemisconfigured', 'csr'));
    }
    if (!$cm = get_coursemodule_from_instance('csr', $csr->id, $course->id)) {
        error(get_string('error:incorrectcoursemoduleid', 'csr'));
    }
}

require_course_login($course);
$errorstr = '';
$context = get_context_instance(CONTEXT_COURSE, $course->id);
require_capability('mod/csr:editsessions', $context);

$returnurl = "view.php?f=$csr->id";

// Handle deletions
if ($d and $confirm) {
    if (!confirm_sesskey()) {
        print_error('confirmsesskeybad', 'error');
    }

    if (csr_delete_session($session)) {
        add_to_log($course->id, 'csr', 'delete session', 'sessions.php?s='.$session->id, $csr->id, $cm->id);
    }
    else {
        add_to_log($course->id, 'csr', 'delete session (FAILED)', 'sessions.php?s='.$session->id, $csr->id, $cm->id);
        print_error('error:couldnotdeletesession', 'csr', $returnurl);
    }
    redirect($returnurl);
}

$customfields = csr_get_session_customfields();

$mform = new mod_csr_session_form(null, compact('id', 'f', 's', 'c', 'nbdays', 'customfields', 'course'));
if ($mform->is_cancelled()){
    redirect($returnurl);
}

if ($fromform = $mform->get_data()) { // Form submitted

    if (empty($fromform->submitbutton)) {
        print_error('error:unknownbuttonclicked', 'csr', $returnurl);
    }

    // Pre-process fields
    if (empty($fromform->allowoverbook)) {
        $fromform->allowoverbook = 0;
    }
    if (empty($fromform->duration)) {
        $fromform->duration = 0;
    }
    if (empty($fromform->normalcost)) {
        $fromform->normalcost = 0;
    }
    if (empty($fromform->discountcost)) {
        $fromform->discountcost = 0;
    }

    $sessiondates = array();
    for ($i = 0; $i < $fromform->date_repeats; $i++) {
        if (!empty($fromform->datedelete[$i])) {
            continue; // skip this date
        }

        $timestartfield = "timestart[$i]";
        $timefinishfield = "timefinish[$i]";
        if (!empty($fromform->$timestartfield) and !empty($fromform->$timefinishfield)) {
            $date = new object();
            $date->timestart = $fromform->$timestartfield;
            $date->timefinish = $fromform->$timefinishfield;
            $sessiondates[] = $date;
        }
    }

    $todb = new object();
    $todb->csr = $csr->id;
	$todb->eventname = $fromform->eventname;
    $todb->datetimeknown = $fromform->datetimeknown;
    $todb->capacity = $fromform->capacity;
    $todb->allowoverbook = $fromform->allowoverbook;
    $todb->duration = $fromform->duration;
    $todb->normalcost = $fromform->normalcost;
    $todb->discountcost = $fromform->discountcost;
    $todb->details = trim($fromform->details);
	$todb->goals = trim($fromform->goals);
	$todb->category = $fromform->category;
	$todb->subcategory = $fromform->subcategory;
	$todb->location = $fromform->location;
	$todb->venue = $fromform->venue;
	$todb->contactperson = $fromform->contactperson;

    $sessionid = null;
    begin_sql();

    $update = false;
    if (!$c and $session != null) {
        $update = true;
        $sessionid = $session->id;

        $todb->id = $session->id;
        if (!csr_update_session($todb, $sessiondates)) {
            rollback_sql();
            add_to_log($course->id, 'csr', 'update session (FAILED)', "sessions.php?s=$session->id", $csr->id, $cm->id);
            print_error('error:couldnotupdatesession', 'csr', $returnurl);
        }

        // Remove old site-wide calendar entry
        if (!csr_remove_session_from_site_calendar($session)) {
            rollback_sql();
            print_error('error:couldnotupdatecalendar', 'csr', $returnurl);
        }
    }
    else {
        if (!$sessionid = csr_add_session($todb, $sessiondates)) {
            rollback_sql();
            add_to_log($course->id, 'csr', 'add session (FAILED)', 'sessions.php?f='.$csr->id, $csr->id, $cm->id);
            print_error('error:couldnotaddsession', 'csr', $returnurl);
        }
    }

    foreach ($customfields as $field) {
        $fieldname = "custom_$field->shortname";
        if (!isset($fromform->$fieldname)) {
            $fromform->$fieldname = ''; // need to be able to clear fields
        }

        if (!csr_save_customfield_value($field->id, $fromform->$fieldname, $sessionid, 'session')) {
            rollback_sql();
            print_error('error:couldnotsavecustomfield', 'csr', $returnurl);
        }
    }

    // Save trainer roles
    if (isset($fromform->trainerrole)) {
        csr_update_trainers($sessionid, $fromform->trainerrole);
    }

    // Retrieve record that was just inserted/updated
    if (!$session = csr_get_session($sessionid)) {
        rollback_sql();
        print_error('error:couldnotfindsession', 'csr', $returnurl);
    }

    // Put the session in the site-wide calendar (needs customfields to be up to date)
    if (!csr_add_session_to_site_calendar($session, $csr)) {
        rollback_sql();
        print_error('error:couldnotupdatecalendar', 'csr', $returnurl);
    }

    if ($update) {
        add_to_log($course->id, 'csr', 'updated session', "sessions.php?s=$session->id", $csr->id, $cm->id);
    }
    else {
        add_to_log($course->id, 'csr', 'added session', 'csr', 'sessions.php?f='.$csr->id, $csr->id, $cm->id);
    }

    commit_sql();
    redirect($returnurl);
}
elseif ($session != null) { // Edit mode
    // Set values for the form
    $toform = new object();
	$toform->eventname = $session->eventname;
    $toform->datetimeknown = (1 == $session->datetimeknown);
    $toform->capacity = $session->capacity;
    $toform->allowoverbook = $session->allowoverbook;
    $toform->duration = $session->duration;
    $toform->normalcost = $session->normalcost;
    $toform->discountcost = $session->discountcost;
    $toform->details = $session->details;
	$toform->goals = $session->goals;
	$toform->category = $session->category;
	$toform->subcategory = $session->subcategory;
	$toform->location = $session->location;
	$toform->venue = $session->venue;
	$toform->contactperson = $session->contactperson;

    if ($session->sessiondates) {
        $i = 0;
        foreach ($session->sessiondates as $date) {
            $idfield = "sessiondateid[$i]";
            $timestartfield = "timestart[$i]";
            $timefinishfield = "timefinish[$i]";
            $toform->$idfield = $date->id;
            $toform->$timestartfield = $date->timestart;
            $toform->$timefinishfield = $date->timefinish;
            $i++;
        }
    }

    foreach ($customfields as $field) {
        $fieldname = "custom_$field->shortname";
        $toform->$fieldname = csr_get_customfield_value($field, $session->id, 'session');
    }

    $mform->set_data($toform);
}

if ($c) {
    $heading = get_string('copyingsession', 'csr', $csr->name);
}
else if ($d) {
    $heading = get_string('deletingsession', 'csr', $csr->name);
}
else if ($id or $f) {
    $heading = get_string('addingsession', 'csr', $csr->name);
}
else {
    $heading = get_string('editingsession', 'csr', $csr->name);
}

$pagetitle = format_string($csr->name);
$navlinks[] = array('name' => get_string('modulenameplural', 'csr'), 'link' => "index.php?id=$course->id", 'type' => 'title');
$navlinks[] = array('name' => $pagetitle, 'link' => "view.php?f=$csr->id", 'type' => 'activityinstance');
$navlinks[] = array('name' => $heading, 'link' => '', 'type' => 'title');
$navigation = build_navigation($navlinks);
print_header_simple($pagetitle, '', $navigation, '', '', true,
                    update_module_button($cm->id, $course->id, get_string('modulename', 'csr')), navmenu($course, $cm));

print_box_start();
print_heading($heading, 'center');

if (!empty($errorstr)) {
    echo '<div class="notifyproblem" align="center"><span style="font-size: 12px; line-height: 18px;">'.$errorstr.'</span></div>';
}

if ($d) {
    $viewattendees = has_capability('mod/csr:viewattendees', $context);
    csr_print_session($session, $viewattendees);
    notice_yesno(get_string('deletesessionconfirm', 'csr', format_string($csr->name)),
                 "sessions.php?s=$session->id&amp;d=1&amp;confirm=1&amp;sesskey=$USER->sesskey", $returnurl);
}
else {
    $mform->display();
}

print_box_end();
print_footer($course);
