<?php

require_once '../../config.php';
require_once 'lib.php';

$id = optional_param('id', 0, PARAM_INT); // Course Module ID
$f = optional_param('f', 0, PARAM_INT); // csr ID
$location = optional_param('location', '', PARAM_TEXT); // location
$download = optional_param('download', '', PARAM_ALPHA); // download attendance

if ($id) {
    if (!$cm = get_record('course_modules', 'id', $id)) {
        print_error('error:incorrectcoursemoduleid', 'csr');
    }
    if (!$course = get_record('course', 'id', $cm->course)) {
        print_error('error:coursemisconfigured', 'csr');
    }
    if (!$csr = get_record('csr', 'id', $cm->instance)) {
        print_error('error:incorrectcoursemodule', 'csr');
    }
}
elseif ($f) {
    if (!$csr = get_record('csr', 'id', $f)) {
        print_error('error:incorrectcsrid', 'csr');
    }
    if (!$course = get_record('course', 'id', $csr->course)) {
        print_error('error:coursemisconfigured', 'csr');
    }
    if (!$cm = get_coursemodule_from_instance('csr', $csr->id, $course->id)) {
        print_error('error:incorrectcoursemoduleid', 'csr');
    }
}
else {
    print_error('error:mustspecifycoursemodulecsr', 'csr');
}

$context = get_context_instance(CONTEXT_MODULE, $cm->id);

if (!empty($download)) {
    require_capability('mod/csr:viewattendees', $context);
    csr_download_attendance($csr->name, $csr->id, $location, $download);
    exit();
}

require_course_login($course);
require_capability('mod/csr:view', $context);

add_to_log($course->id, 'csr', 'view', "view.php?id=$cm->id", $csr->id, $cm->id);

$pagetitle = format_string($csr->name);
$navlinks[] = array('name' => get_string('modulenameplural', 'csr'), 'link' => "index.php?id=$course->id", 'type' => 'title');
$navlinks[] = array('name' => $pagetitle, 'link' => '', 'type' => 'activityinstance');
$navigation = build_navigation($navlinks);
print_header_simple($pagetitle, '', $navigation, '', '', true,
                    update_module_button($cm->id, $course->id, get_string('modulename', 'csr')), navmenu($course, $cm));

if (empty($cm->visible) and !has_capability('mod/csr:viewemptyactivities', $context)) {
    notice(get_string('activityiscurrentlyhidden'));
}

print_box_start();


if ($csr->description) {
    print_box_start('generalbox','description');
    echo format_text($csr->description, FORMAT_HTML);
    print_box_end();
}

$locations = get_locations($csr->id);
if (count($locations) > 2) {
    echo '<form method="get" action="view.php">';
    echo '<div><input type="hidden" name="f" value="'.$csr->id.'"/>';
    choose_from_menu($locations, 'location', $location, '');
    echo '<input type="submit" value="'.get_string('showbylocation','csr').'"/>';
    echo '</div></form>';
}

print_session_list($course->id, $csr->id, $location);

if (has_capability('mod/csr:viewattendees', $context)) {
    echo '<form method="get" action="view.php">';
    echo '<div align="right"><input type="hidden" name="f" value="'.$csr->id.'"/>';
    echo 'Export attendence' . '&nbsp;';
    $formats = array('excel' => get_string('excelformat', 'csr'),
                     'ods' => get_string('odsformat', 'csr'));
    choose_from_menu($formats, 'download', 'excel', '');
    echo '<input type="submit" value="'.get_string('exporttofile','csr').'"/>';
    echo '</div></form>';
}

print_box_end();
print_footer($course);

function print_session_list($courseid, $csrid, $location)
{
    global $USER;
    global $CFG;

    $timenow = time();

    $context = get_context_instance(CONTEXT_COURSE, $courseid, $USER->id);
    $viewattendees = has_capability('mod/csr:viewattendees', $context);
    $editsessions = has_capability('mod/csr:editsessions', $context);

    $bookedsession = null;
    if ($submissions = csr_get_user_submissions($csrid, $USER->id)) {
        $submission = array_shift($submissions);
        $bookedsession = $submission;
    }

    $customfields = csr_get_session_customfields();
  if ($editsessions) {
        echo '<p>
		
		<a href="sessions.php?f='.$csrid.'"><img  src="add.jpg" alt="'.get_string('addsession', 'csr').'" /></a></p>';
    }
    // Table headers
    $tableheader = array();
	$tableheader[] = get_string('eventname', 'csr');
	$tableheader[] = get_string('location', 'csr');
	$tableheader[] = get_string('venue', 'csr');
    $tableheader[] = get_string('date', 'csr');
    $tableheader[] = get_string('time', 'csr');
    if ($viewattendees) {
        $tableheader[] = get_string('capacity', 'csr');
    }
    else {
        $tableheader[] = get_string('seatsavailable', 'csr');
    }
    $tableheader[] = get_string('status', 'csr');
    $tableheader[] = get_string('options', 'csr');

    $upcomingdata = array();
    $upcomingtbddata = array();
    $previousdata = array();
    $upcomingrowclass = array();
    $upcomingtbdrowclass = array();
    $previousrowclass = array();

    if ($sessions = csr_get_sessions($csrid, $location) ) {
        foreach($sessions as $session) {
            $sessionrow = array();

            $sessionstarted = false;
            $sessionfull = false;
            $sessionwaitlisted = false;
            $isbookedsession = false;
			$sessionrow[]=$session->eventname;
			$sessionrow[]=$session->location;
			$sessionrow[]=$session->venue;


			
            // Dates/times
            $allsessiondates = '';
            $allsessiontimes = '';
            if ($session->datetimeknown) {
                foreach ($session->sessiondates as $date) {
                    if (!empty($allsessiondates)) {
                        $allsessiondates .= '<br/>';
                    }
                    $allsessiondates .= userdate($date->timestart, get_string('strftimedate'));
                    if (!empty($allsessiontimes)) {
                        $allsessiontimes .= '<br/>';
                    }
                    $allsessiontimes .= userdate($date->timestart, get_string('strftimetime')).
                        ' - '.userdate($date->timefinish, get_string('strftimetime'));
                }
            }
            else {
                $allsessiondates = get_string('wait-listed', 'csr');
                $allsessiontimes = get_string('wait-listed', 'csr');
                $sessionwaitlisted = true;
            }
            $sessionrow[] = $allsessiondates;
            $sessionrow[] = $allsessiontimes;

            // Capacity
            $signupcount = csr_get_num_attendees($session->id, MDL_CSR_STATUS_APPROVED);
            $stats = $session->capacity - $signupcount;
            if ($viewattendees){
                $stats = $signupcount.' / '.$session->capacity;
            }
            else {
                $stats = max(0, $stats);
            }
            $sessionrow[] = $stats;

            // Status
            $status  = get_string('bookingopen', 'csr');
            if ($session->datetimeknown && csr_has_session_started($session, $timenow) && csr_is_session_in_progress($session, $timenow)) {
                $status = get_string('sessioninprogress', 'csr');
                $sessionstarted = true;
            }
            elseif ($session->datetimeknown && csr_has_session_started($session, $timenow)) {
                $status = get_string('sessionover', 'csr');
                $sessionstarted = true;
            }
            elseif ($bookedsession && $session->id == $bookedsession->sessionid) {
                $signupstatus = csr_get_status($bookedsession->statuscode);

                $status = get_string('status_'.$signupstatus, 'csr');
                $isbookedsession = true;
            }
            elseif ($signupcount >= $session->capacity) {
                $status = get_string('bookingfull', 'csr');
                $sessionfull = true;
            }

            $sessionrow[] = $status;

            // Options
            $options = '';
            if ($editsessions) {
                $options .= ' <a href="sessions.php?s='.$session->id.'" title="'.get_string('editsession', 'csr').'">'
                    . '<img src="'.$CFG->pixpath.'/t/edit.gif" class="iconsmall" alt="'.get_string('edit', 'csr').'" /></a> '
                    . '<a href="sessions.php?s='.$session->id.'&amp;c=1" title="'.get_string('copysession', 'csr').'">'
                    . '<img src="'.$CFG->pixpath.'/t/copy.gif" class="iconsmall" alt="'.get_string('copy', 'csr').'" /></a> '
                    . '<a href="sessions.php?s='.$session->id.'&amp;d=1" title="'.get_string('deletesession', 'csr').'">'
                    . '<img src="'.$CFG->pixpath.'/t/delete.gif" class="iconsmall" alt="'.get_string('delete').'" /></a><br />';
            }
            if ($viewattendees){
                $options .= '<a href="attendees.php?s='.$session->id.'&amp;backtoallsessions='.$csrid.'" title="'.get_string('seeattendees', 'csr').'"><img class="iconsmall" src="att.png" alt="'.get_string('seeattendees', 'csr').'" /></a><br />';
            }
            

            if (empty($options)) {
                $options = get_string('none', 'csr');
            }
            $sessionrow[] = $options;

            // Set the CSS class for the row
            $rowclass = '';
            if ($sessionstarted) {
                $rowclass = 'dimmed_text';
            }
            elseif ($isbookedsession) {
                $rowclass = 'highlight';
            }
            elseif ($sessionfull) {
                $rowclass = 'dimmed_text';
            }

            // Put the row in the right table
            if ($sessionstarted) {
                $previousrowclass[] = $rowclass;
                $previousdata[] = $sessionrow;
            }
            elseif ($sessionwaitlisted) {
                $upcomingtbdrowclass[] = $rowclass;
                $upcomingtbddata[] = $sessionrow;
            }
            else { // Normal scheduled session
                $upcomingrowclass[] = $rowclass;
                $upcomingdata[] = $sessionrow;
            }
        }
    }

    // Upcoming sessions
    print_heading(get_string('upcomingsessions', 'csr'));
    if (empty($upcomingdata) and empty($upcomingtbddata)) {
        print_string('noupcoming', 'csr');
    }
    else {
        $upcomingtable = new object();
        $upcomingtable->summary = get_string('upcomingsessionslist', 'csr');
        $upcomingtable->head = $tableheader;
        $upcomingtable->rowclass = array_merge($upcomingrowclass, $upcomingtbdrowclass);
        $upcomingtable->width = '100%';
        $upcomingtable->data = array_merge($upcomingdata, $upcomingtbddata);
        print_table($upcomingtable);
    }

  

    // Previous sessions
    if (!empty($previousdata)) {
        print_heading(get_string('previoussessions', 'csr'));
        $previoustable = new object();
        $previoustable->summary = get_string('previoussessionslist', 'csr');
        $previoustable->head = $tableheader;
        $previoustable->rowclass = $previousrowclass;
        $previoustable->width = '100%';
        $previoustable->data = $previousdata;
        print_table($previoustable);
    }
}

/**
 * Get csr locations
 *
 * @param   interger    $csrid
 * @return  array
 */
function get_locations($csrid)
{
    global $CFG;

    $locationfieldid = get_field('csr_session_field', 'id', 'shortname', 'location');
    if (!$locationfieldid) {
        return array();
    }

    $sql = "SELECT DISTINCT d.data AS location
              FROM {$CFG->prefix}csr f
              JOIN {$CFG->prefix}csr_sessions s ON s.csr = f.id
              JOIN {$CFG->prefix}csr_session_data d ON d.sessionid = s.id
             WHERE f.id = $csrid AND d.fieldid = $locationfieldid";

    if ($records = get_records_sql($sql)) {
        $locationmenu[''] = get_string('alllocations', 'csr');

        $i=1;
        foreach ($records as $record) {
            $locationmenu[$record->location] = $record->location;
            $i++;
        }

        return $locationmenu;
    }

    return array();
}
