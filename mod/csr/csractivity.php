
<?PHP // $Id: view.php,v 1.168.2.28 2009/10/02 06:28:06 moodler Exp $

//  Display profile for a particular user

    require_once("../../config.php");
    require_once($CFG->dirroot.'/user/profile/lib.php');
    require_once($CFG->dirroot.'/tag/lib.php');

    $id      = optional_param('id',     0,      PARAM_INT);   // user id
    $course  = optional_param('course', SITEID, PARAM_INT);   // course id (defaults to Site)
    $enable  = optional_param('enable', '');                  // enable email
    $disable = optional_param('disable', '');                 // disable email

    if (empty($id)) {         // See your own profile by default
        require_login();
        $id = $USER->id;
    }

    if (! $user = get_record("user", "id", $id) ) {
        error("No such user in this course");
    }

    if (! $course = get_record("course", "id", $course) ) {
        error("No such course id");
    }

/// Make sure the current user is allowed to see this user

    if (empty($USER->id)) {
       $currentuser = false;
    } else {
       $currentuser = ($user->id == $USER->id);
    }

   
        $coursecontext = get_context_instance(CONTEXT_COURSE, $course->id);   // Course context
        $usercontext   = get_context_instance(CONTEXT_USER, $user->id);       // User context
		$systemcontext = get_context_instance(CONTEXT_SYSTEM);   // SYSTEM context

    if (!empty($CFG->forcelogin) || $course->id != SITEID) {
        // do not force parents to enrol
        if (!get_record('role_assignments', 'userid', $USER->id, 'contextid', $usercontext->id)) {
            require_login($course->id);
        }
    }

    if (!empty($CFG->forceloginforprofiles)) {
        require_login();
        if (isguest()) {
            redirect("$CFG->wwwroot/login/index.php");
        }
    }

    $strpersonalprofile = get_string('personalprofile');
    $strparticipants = get_string("participants");
    $struser = get_string("user");

    $fullname = fullname($user, has_capability('moodle/site:viewfullnames', $coursecontext));

    $navlinks = array();
    if (has_capability('moodle/course:viewparticipants', $coursecontext) || has_capability('moodle/site:viewparticipants', $systemcontext)) {
        $navlinks[] = array('name' => $strparticipants, 'link' => "index.php?id=$course->id", 'type' => 'misc');
    }

/// If the user being shown is not ourselves, then make sure we are allowed to see them!

    if (!$currentuser) {
        if ($course->id == SITEID) {  // Reduce possibility of "browsing" userbase at site level
            if ($CFG->forceloginforprofiles and !isteacherinanycourse()
                    and !isteacherinanycourse($user->id)
                    and !has_capability('moodle/user:viewdetails', $usercontext)) {  // Teachers can browse and be browsed at site level. If not forceloginforprofiles, allow access (bug #4366)

                $navlinks[] = array('name' => $struser, 'link' => null, 'type' => 'misc');
                $navigation = build_navigation($navlinks);

                print_header("$strpersonalprofile: ", "$strpersonalprofile: ", $navigation, "", "", true, "&nbsp;", navmenu($course));
                print_heading(get_string('usernotavailable', 'error'));
                print_footer($course);
                exit;
            }
        } else {   // Normal course
            // check capabilities
            if (!has_capability('moodle/user:viewdetails', $coursecontext) && 
                !has_capability('moodle/user:viewdetails', $usercontext)) {
                print_error('cannotviewprofile');
            }

            if (!has_capability('moodle/course:view', $coursecontext, $user->id, false)) {
                if (has_capability('moodle/course:view', $coursecontext)) {
                    $navlinks[] = array('name' => $fullname, 'link' => null, 'type' => 'misc');
                    $navigation = build_navigation($navlinks);
                    print_header("$strpersonalprofile: ", "$strpersonalprofile: ", $navigation, "", "", true, "&nbsp;", navmenu($course));
                    print_heading(get_string('notenrolled', '', $fullname));
                } else {
                    $navlinks[] = array('name' => $struser, 'link' => null, 'type' => 'misc');
                    $navigation = build_navigation($navlinks);
                    print_header("$strpersonalprofile: ", "$strpersonalprofile: ", $navigation, "", "", true, "&nbsp;", navmenu($course));
                    print_heading(get_string('notenrolledprofile'));
                }
                print_continue($_SERVER['HTTP_REFERER']);
                print_footer($course);
                exit;
            }
        }



    }


/// We've established they can see the user's name at least, so what about the rest?

    $navlinks[] = array('name' => $fullname, 'link' => null, 'type' => 'misc');

    $navigation = build_navigation($navlinks);

    print_header();
echo'<IMG SRC="feature.jpg">';
echo'<DIV STYLE="position:absolute; top:50px; left:70px; width:1127px; height:232px">';
echo'<b><FONT SIZE="+3" COLOR="ffffff">CSR Registration Tool </FONT></b></DIV>';


	$currenttab = 'summary';
    include('csrtabs.php');



/// Get the hidden field list
    if (has_capability('moodle/user:viewhiddendetails', $coursecontext)) {
        $hiddenfields = array();
    } else {
        $hiddenfields = array_flip(explode(',', $CFG->hiddenuserfields));
    }




    if (is_mnet_remote_user($user)) {
        $sql = "
             SELECT DISTINCT
                 h.id,
                 h.name,
                 h.wwwroot,
                 a.name as application,
                 a.display_name
             FROM
                 {$CFG->prefix}mnet_host h,
                 {$CFG->prefix}mnet_application a
             WHERE
                 h.id = '{$user->mnethostid}' AND
                 h.applicationid = a.id
             ORDER BY
                 a.display_name,
                 h.name";

        $remotehost = get_record_sql($sql);

        echo '<p class="errorboxcontent">'.get_string('remoteappuser', $remotehost->application)." <br />\n";

    }

	$recordscompleted = get_records_sql("SELECT
            se.*,
            min(DATE_FORMAT(FROM_UNIXTIME(da.timestart),'%W, %M %e, %Y - %h:%i ')) as StartTime,
            max(DATE_FORMAT(FROM_UNIXTIME(da.timefinish),'%W, %M %e, %Y - %h:%i')) as EndTime,
            se.normalcost,
            se.discountcost,
            se.details,
            se.datetimeknown
        FROM
            mdl_csr_signups su
        INNER JOIN
            mdl_csr_signups_status sus
         ON su.id = sus.signupid
        AND sus.superceded = 0 and sus.grade=100
        
        JOIN
            mdl_csr_sessions se
         ON su.sessionid = se.id
        JOIN
            mdl_csr_sessions_dates da
            ON da.sessionid = se.id

        WHERE se.datetimeknown = 1 and su.userid=$USER->id group by se.id"
		 );
		
		$usercsrc = array();
		
		if ($recordscompleted) {
			foreach($recordscompleted as $recordc) {
                $usercsrc[] = $recordc;
                continue;
			}
		}
		
	$recordscompletedind = get_records_sql("SELECT
            se.*,
            min(DATE_FORMAT(FROM_UNIXTIME(da.timestart),'%W, %M %e, %Y - %h:%i ')) as StartTime,
            max(DATE_FORMAT(FROM_UNIXTIME(da.timefinish),'%W, %M %e, %Y - %h:%i')) as EndTime,
            se.normalcost,
            se.discountcost,
            se.details,
            se.datetimeknown
        FROM
            mdl_csr_user_signups sus
		 
        JOIN
            mdl_csr_sessions se
         ON sus.sessionid = se.id and sus.grade=100
        JOIN
            mdl_csr_sessions_dates da
            ON da.sessionid = se.id

        WHERE se.datetimeknown = 1 and sus.userid=$USER->id group by se.id"
		 );
		
		$usercsri = array();
		
		if ($recordscompletedind) {
			foreach($recordscompletedind as $recordi) {
                $usercsri[] = $recordi;
                continue;
			}
		}

		$records = get_records_sql("SELECT
            se.*,
            min(DATE_FORMAT(FROM_UNIXTIME(da.timestart),'%W, %M %e, %Y - %h:%i ')) as StartTime,
            max(DATE_FORMAT(FROM_UNIXTIME(da.timefinish),'%W, %M %e, %Y - %h:%i')) as EndTime,
            se.normalcost,
            se.discountcost,
            se.details,
            se.datetimeknown
        FROM
            mdl_csr_signups su
        INNER JOIN
            mdl_csr_signups_status sus
         ON su.id = sus.signupid
        AND sus.superceded = 0 and sus.statuscode=70
        
        JOIN
            mdl_csr_sessions se
         ON su.sessionid = se.id
        JOIN
            mdl_csr_sessions_dates da
            ON da.sessionid = se.id

        WHERE se.datetimeknown = 1 and su.userid=$USER->id group by se.id"
		 );
		
		$usercsr = array();
		
		if ($records) {
			foreach($records as $record) {
               $usercsr[] = $record;
                continue;
			}
		}
		
		
		
		
		
		
		
	$contributions = get_records_sql("SELECT se.category as category,
  sum((se.duration/60)) as duration,count(se.duration) as events
        FROM
        mdl_csr_signups su
        INNER JOIN
        mdl_csr_signups_status sus
        ON su.id = sus.signupid
        AND sus.superceded = 0
        AND sus.grade>=100 
        JOIN  mdl_csr_sessions se
        ON su.sessionid = se.id and su.userid=$USER->id
        WHERE se.datetimeknown = 1 
  group by se.category");
  
  //
  $contcsr = array();
  $totalduration=0;
  $eventscompleted=0;
		if ($contributions) {
			foreach($contributions as $contribution) {
                $contcsr[] = $contribution;
				$totalduration+=$contribution->duration;
				$eventscompleted+=$contribution->events;
                continue;
			}
		
		
		echo '<table width="100%" class="userinfobox">';
		echo '<tr>';
		echo '<td class="content">';
		echo '<div  style="background-image:url(sidebanner.png);margin:0px 0px 0px 0px">';
		echo '<div>';
		echo '<table border="0" width="100%" cellpadding="0" cellspacing="0">';
		echo '<tr><td>
		<center>
		<font style="color:#e6e6fa;text-align:center;font-family:georgia,serif;" size="6">'.$eventscompleted.'</font>
		events completed.<br/><font style="color:#ff8c00;">'.$totalduration.'</font> hours of social activity.
		</center>
		</td></tr>';

		echo '</table>';
		echo '</div>';
		echo '</div>';
		echo '<br/>';

		echo '<div style="background:url(FuInzwB-1.png) no-repeat top right;">';
		echo '<div style="background-color:#447563;margin:0px 15px 0px 15px;">';
		echo '<div style="background-image:url(Qy4HVl4-0.png);margin: 0px 15px 0px -15px;width:15px;height:15px;"></div>';
		echo '</div>';
		echo '&nbsp;&nbsp;&nbsp;<font size="4">Contributions</font>';
		echo '<div style="background-color:#447563;padding-top: 2px; padding-right: 5px; padding-bottom: 2px; padding-left: 5px;">';
		
		echo '<table style="color:white" border="0" width="100%" cellpadding="3" cellspacing="2">';
		
		foreach ($contcsr as $ccsr) 
		{
		echo '<tr><td>'.format_string($ccsr->category).'</td><td>'.format_string($ccsr->duration).'</td>';
		
		}
		
		echo '</tr>';
		echo '</table>';
		echo '</div>';
	
		echo '<div style="background:url(w6veLOV-3.png) no-repeat top right;">';	
		echo '<div style="background-color:#447563;margin:0px 15px 0px 15px;">';
		echo '<div style="background-image:url(Oz0SFho-2.png);margin:0px 15px 0px -15px;width:15px;height:15px;"></div>';
		echo '</div>';
		echo '</div>';
		echo '</div>';
	}	
		
		echo '</td><td class="content">';
	
	
	echo '<div  style="background:url(ditPw2G-1.png) no-repeat top right;">';
	echo '<div style="background-color:#8DB2E3;margin:0px 25px 0px 25px;">';
	echo '<div style="background-image:url(4i1TjZ2-0.png);margin: 0px 25px 0px -25px;width:25px;height:25px;"></div>';
	echo '</div>';
	echo '&nbsp;&nbsp;&nbsp;<font size="4">Registered activities</font>';
echo '<div style="background-color:#8DB2E3;padding-top: 8px; padding-right: 10px; padding-bottom: 2px; padding-left: 10px;">';

	echo '<table border="0" width="100%" cellpadding="3" cellspacing="0">';
	    
	if(!$usercsr)
	{
	echo '<tr style="background-color: #fbf6e7">
	
					<td style="text-align: center;">
						<img alt="" src="registerimg.jpg" /></td>
					<td>
						Currently you have not registered for any events, Please use the Calander tab to register for upcomming events.

		</tr>';
	

	}
	foreach ($usercsr as $ucsr) 
	{
	
		echo '<tr style="background-color: #fbf6e7">';
			echo '<td border="0" colspan="2"><font style="color:#ff2c00;">'.format_string($ucsr->StartTime).' >> '.format_string($ucsr->EndTime).'</font></td>';
		echo '</tr>';
		echo '<tr >';
		echo '<td border="0" width="90%">';
		
			echo '<table  border="0" width="100%">';
			echo '<tr style="background-color: #ffffff">';
			echo '<td width="10%"></td>';
			echo '<td width="90%"><b>'.format_string($ucsr->eventname).'</b><br/>'.
			format_string($ucsr->location).','.format_string($ucsr->venue).'<br/>'.
			'<font style="color:#990000;">Contact person :'.format_string($ucsr->contactperson).'</font><br/><br/>'.
			'<u>Goals and Objectives</u><br/>'.
			format_string($ucsr->goals).'<br/>'.
			'</td>';
			
			echo '</td>';
			
			echo '</tr>';
			echo '</table>';
			
		echo '<td style="background-color: #fbf6e7" width="10%"><a href="'.$CFG->wwwroot.'/mod/csr/cancelsignup.php?s='.$ucsr->id.'&backtoallsessions=0"><img src="cancel.png" alt="Cancel session" height="28" width="80"></a></td>';
		echo '</tr>';
		echo '<tr>';
		echo '<td><br/></td>';
		echo '<td><br/></td>';
		echo '</tr>';
		
	}
	echo '</table>';
	echo '</div>';
	echo '<div style="background:url(Qchz97q-3.png) no-repeat top right;">';
	echo '<div style="background-color:#8DB2E3;margin:0px 25px 0px 25px;">';
	echo '<div style="background-image:url(5wkJF8v-2.png);margin:0px 25px 0px -25px;width:25px;height:25px;"></div>';
	echo '</div>';
	echo '</div>';
	echo '</div>';	
	echo '</br>';	

//completed events

	echo '<div  style="background:url(u2lE90Y-1.png) no-repeat top right;">';
	echo '<div style="background-color:#80cacf;margin:0px 25px 0px 25px;">';
	echo '<div style="background-image:url(yKV5II7-0.png);margin: 0px 25px 0px -25px;width:25px;height:25px;"></div>';
	echo '</div>';
	echo '&nbsp;&nbsp;&nbsp;<font size="4">Completed activities</font>';
	echo '<div style="background-color:#80cacf;padding-top: 8px; padding-right: 10px; padding-bottom: 2px; padding-left: 10px;">';

	echo '<table border="0" width="100%" cellpadding="3" cellspacing="0">';
	    
	if(!$usercsrc)
	{
		echo '<tr style="background-color: #fbf6e7">
	
					<td style="text-align: center;">
						<img alt="" src="completeimg.jpg" /></td>
					<td>
						You have no completed CSR activity, You would like to take part in one, Please use the Calander tab to register for upcomming events.</td>

		</tr>';
	}
	foreach ($usercsrc as $ucsrc) 
	{
	
		echo '<tr style="background-color: #fbf6e7" > ';
			echo '<td border="0" colspan="2"><font style="color:#00aa2c;">'.format_string($ucsrc->StartTime).' >> '.format_string($ucsrc->EndTime).'</font></td>';
		echo '</tr>';
		echo '<tr style="background-color: #ffffff">';
		echo '<td border="0" width="85%">';
		
			echo '<table border="0" width="100%">';
			echo '<tr >';
			echo '<td width="10%"></td>';
			echo '<td width="90%"><b>'.format_string($ucsrc->eventname).'</b><br/>'.
			format_string($ucsrc->location).','.format_string($ucsrc->venue).'<br/>'.
			'</td>';
			
			echo '</td>';
			
			echo '</tr>';
			echo '</table>';
			
	//	echo '<td width="15%">'.format_string($ucsrc->duration/60).'<br/> hrs of work</td>';
		echo '<td width="15%"></td>';
		echo '</tr>';
		echo '<tr>';
		echo '<td><br/></td>';
		echo '<td><br/></td>';
		echo '</tr>';
		
	}
	echo '</table>';
	echo '</div>';
	echo '<div style="background:url(ffR1bbK-3.png) no-repeat top right;">';
	echo '<div style="background-color:#80cacf;margin:0px 25px 0px 25px;">';
	echo '<div style="background-image:url(X1vdkrq-2.png);margin:0px 25px 0px -25px;width:25px;height:25px;"></div>';
	echo '</div>';
	echo '</div>';
	echo '</div>';
	echo '</br>';		



//individual events

	echo '<div  style="background:url(7neTA9P-1.png) no-repeat top right;">';
	echo '<div style="background-color:#CE96D9;margin:0px 25px 0px 25px;">';
	echo '<div style="background-image:url(dOsrmEb-0.png);margin: 0px 25px 0px -25px;width:25px;height:25px;"></div>';
	echo '</div>';
	echo '&nbsp;&nbsp;&nbsp;<font size="4">Individual activities</font>';
	echo '<div style="background-color:#CE96D9;padding-top: 8px; padding-right: 10px; padding-bottom: 2px; padding-left: 10px;">';

	echo '<table border="0" width="100%" cellpadding="3" cellspacing="0">';
	    
	if(!$usercsri)
	{
	echo '<tr style="background-color: #fbf6e7">
	
					<td style="text-align: center;">
						<img alt="" src="userimg.jpg" /></td>
					<td>
						Currently you have no individual events registered. Please use the Individual Activity tab to register your voluntary activity.

		</tr>';

	}
	foreach ($usercsri as $ucsri) 
	{
	
		echo '<tr style="background-color: #fbf6e7" > ';
			echo '<td border="0" colspan="2"><font style="color:#00aa2c;">'.format_string($ucsri->StartTime).' >> '.format_string($ucsri->EndTime).'</font></td>';
		echo '</tr>';
		echo '<tr style="background-color: #ffffff">';
		echo '<td border="0" width="85%">';
		
			echo '<table border="0" width="100%">';
			echo '<tr >';
			echo '<td width="10%"></td>';
			echo '<td width="90%"><b>'.format_string($ucsri->eventname).'</b><br/>'.
			format_string($ucsri->location).','.format_string($ucsri->venue).'<br/>'.
			'</td>';
			
			echo '</td>';
			
			echo '</tr>';
			echo '</table>';
			
	//	echo '<td width="15%">'.format_string($ucsrc->duration/60).'<br/> hrs of work</td>';
		echo '<td width="15%"></td>';
		echo '</tr>';
		echo '<tr>';
		echo '<td><br/></td>';
		echo '<td><br/></td>';
		echo '</tr>';
		
	}
	echo '</table>';
	echo '</div>';
	echo '<div style="background:url(ZtB5zOL-3.png) no-repeat top right;">';
	echo '<div style="background-color:#CE96D9;margin:0px 25px 0px 25px;">';
	echo '<div style="background-image:url(vnX0Ojd-2.png);margin:0px 25px 0px -25px;width:25px;height:25px;"></div>';
	echo '</div>';
	echo '</div>';
	echo '</div>';	

    echo "</td></tr></table>";

    echo "</div>\n";
    echo "</td></tr></table>";

    echo "</div>\n";
   // print_footer($course);
echo'<table class="footer_panel" width="100%" border="0" cellpadding="15" cellspacing="0">
  <tr> 
    <div align="center"><hr /></div>
<div align="center"><font size="1" color="#666666">Best viewed in 1024x786 resolution. NTT Data Inc. All rights reserved.</font><font size="1" color="#666666"><br /></font><font size="1" face="Arial" color="#006699"></div>
<div align="center"><hr /></div>
     </td>
       
  </tr>
</table>';

function print_row($left, $right) {
    echo "\n<tr><td class=\"label c0\">$left</td><td class=\"info c1\">$right</td></tr>\n";
}

?>
