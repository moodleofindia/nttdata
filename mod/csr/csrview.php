<?PHP // $Id: view.php,v 1.168.2.28 2009/10/02 06:28:06 moodler Exp $

//  Display profile for a particular user

    require_once("../../config.php");
    require_once($CFG->dirroot.'/user/profile/lib.php');
    require_once($CFG->dirroot.'/tag/lib.php');

    $id      = optional_param('id',     0,      PARAM_INT);   // user id
    $course  = optional_param('course', SITEID, PARAM_INT);   // course id (defaults to Site)
    $enable  = optional_param('enable', '');                  // enable email
    $disable = optional_param('disable', '');                 // disable email

    if (empty($id)) {         // See your own profile by default
        require_login();
        $id = $USER->id;
    }

    if (! $user = get_record("user", "id", $id) ) {
        error("No such user in this course");
    }

    if (! $course = get_record("course", "id", $course) ) {
        error("No such course id");
    }

/// Make sure the current user is allowed to see this user

    if (empty($USER->id)) {
       $currentuser = false;
    } else {
       $currentuser = ($user->id == $USER->id);
    }

   
        $coursecontext = get_context_instance(CONTEXT_COURSE, $course->id);   // Course context
        $usercontext   = get_context_instance(CONTEXT_USER, $user->id);       // User context
		$systemcontext = get_context_instance(CONTEXT_SYSTEM);   // SYSTEM context

    if (!empty($CFG->forcelogin) || $course->id != SITEID) {
        // do not force parents to enrol
        if (!get_record('role_assignments', 'userid', $USER->id, 'contextid', $usercontext->id)) {
            require_login($course->id);
        }
    }

    if (!empty($CFG->forceloginforprofiles)) {
        require_login();
        if (isguest()) {
            redirect("$CFG->wwwroot/login/index.php");
        }
    }

    $strpersonalprofile = get_string('personalprofile');
    $strparticipants = get_string("participants");
    $struser = get_string("user");

    $fullname = fullname($user, has_capability('moodle/site:viewfullnames', $coursecontext));

    $navlinks = array();
    if (has_capability('moodle/course:viewparticipants', $coursecontext) || has_capability('moodle/site:viewparticipants', $systemcontext)) {
        $navlinks[] = array('name' => $strparticipants, 'link' => "index.php?id=$course->id", 'type' => 'misc');
    }

/// If the user being shown is not ourselves, then make sure we are allowed to see them!

    if (!$currentuser) {
        if ($course->id == SITEID) {  // Reduce possibility of "browsing" userbase at site level
            if ($CFG->forceloginforprofiles and !isteacherinanycourse()
                    and !isteacherinanycourse($user->id)
                    and !has_capability('moodle/user:viewdetails', $usercontext)) {  // Teachers can browse and be browsed at site level. If not forceloginforprofiles, allow access (bug #4366)

                $navlinks[] = array('name' => $struser, 'link' => null, 'type' => 'misc');
                $navigation = build_navigation($navlinks);

                print_header("$strpersonalprofile: ", "$strpersonalprofile: ", $navigation, "", "", true, "&nbsp;", navmenu($course));
                print_heading(get_string('usernotavailable', 'error'));
                print_footer($course);
                exit;
            }
        } else {   // Normal course
            // check capabilities
            if (!has_capability('moodle/user:viewdetails', $coursecontext) && 
                !has_capability('moodle/user:viewdetails', $usercontext)) {
                print_error('cannotviewprofile');
            }

            if (!has_capability('moodle/course:view', $coursecontext, $user->id, false)) {
                if (has_capability('moodle/course:view', $coursecontext)) {
                    $navlinks[] = array('name' => $fullname, 'link' => null, 'type' => 'misc');
                    $navigation = build_navigation($navlinks);
                    print_header("$strpersonalprofile: ", "$strpersonalprofile: ", $navigation, "", "", true, "&nbsp;", navmenu($course));
                    print_heading(get_string('notenrolled', '', $fullname));
                } else {
                    $navlinks[] = array('name' => $struser, 'link' => null, 'type' => 'misc');
                    $navigation = build_navigation($navlinks);
                    print_header("$strpersonalprofile: ", "$strpersonalprofile: ", $navigation, "", "", true, "&nbsp;", navmenu($course));
                    print_heading(get_string('notenrolledprofile'));
                }
                print_continue($_SERVER['HTTP_REFERER']);
                print_footer($course);
                exit;
            }
        }



    }


/// We've established they can see the user's name at least, so what about the rest?

    $navlinks[] = array('name' => $fullname, 'link' => null, 'type' => 'misc');

    $navigation = build_navigation($navlinks);



				     print_header();
echo'<IMG SRC="feature.jpg">';
echo'<DIV STYLE="position:absolute; top:50px; left:70px; width:1127px; height:232px">';
echo'<b><FONT SIZE="+3" COLOR="ffffff">CSR Registration Tool </FONT></b></DIV>';
    if (($course->id != SITEID) and ! isguest() ) {   // Need to have access to a course to see that info
        if (!has_capability('moodle/course:view', $coursecontext, $user->id)) {
            print_heading(get_string('notenrolled', '', $fullname));
            print_footer($course);
            die;
        }
    }

    if ($user->deleted) {
        print_heading(get_string('userdeleted'));
        if (!has_capability('moodle/user:update', $coursecontext)) {
            print_footer($course);
            die;
        }
    }




/// Get the hidden field list
    if (has_capability('moodle/user:viewhiddendetails', $coursecontext)) {
        $hiddenfields = array();
    } else {
        $hiddenfields = array_flip(explode(',', $CFG->hiddenuserfields));
    }

/// Print tabs at top
/// This same call is made in:
///     /user/view.php
///     /user/edit.php
///     /course/user.php

    $currenttab = 'profile';
    $showroles = 1;
    if (!$user->deleted) {
        include('csrtabs.php');
    }

    if (is_mnet_remote_user($user)) {
        $sql = "
             SELECT DISTINCT
                 h.id,
                 h.name,
                 h.wwwroot,
                 a.name as application,
                 a.display_name
             FROM
                 {$CFG->prefix}mnet_host h,
                 {$CFG->prefix}mnet_application a
             WHERE
                 h.id = '{$user->mnethostid}' AND
                 h.applicationid = a.id
             ORDER BY
                 a.display_name,
                 h.name";

        $remotehost = get_record_sql($sql);

        echo '<p class="errorboxcontent">'.get_string('remoteappuser', $remotehost->application)." <br />\n";
        if ($USER->id == $user->id) {
            if ($remotehost->application =='moodle') {
                echo "Remote {$remotehost->display_name}: <a href=\"{$remotehost->wwwroot}/user/edit.php\">{$remotehost->name}</a> ".get_string('editremoteprofile')." </p>\n";
            } else {
                echo "Remote {$remotehost->display_name}: <a href=\"{$remotehost->wwwroot}/\">{$remotehost->name}</a> ".get_string('gotoyourserver')." </p>\n";
            }
        } else {
            echo "Remote {$remotehost->display_name}: <a href=\"{$remotehost->wwwroot}/\">{$remotehost->name}</a></p>\n";
        }
    }

    echo '<table width="80%" class="userinfobox" summary="">';
    echo '<tr>';
    echo '<td class="side">';
    print_user_picture($user, $course->id, $user->picture, true, false, false);
    echo '</td><td class="content">';

    // Print the description

    if ($user->description && !isset($hiddenfields['description'])) {
        $has_courseid = ($course->id != SITEID);
        if (!$has_courseid && !empty($CFG->profilesforenrolledusersonly) && !record_exists('role_assignments', 'userid', $id)) {
            echo get_string('profilenotshown', 'moodle').'<hr />';
        } else {
            echo format_text($user->description, FORMAT_MOODLE)."<hr />";
        }
    }

    // Print all the little details in a list

    echo '<table class="list">';

    if (! isset($hiddenfields['country']) && $user->country) {
        $countries = get_list_of_countries();
        print_row(get_string('country') . ':', $countries[$user->country]);
    }

    if (! isset($hiddenfields['city']) && $user->city) {
        print_row(get_string('city') . ':', $user->city);
    }

    if (has_capability('moodle/user:viewhiddendetails', $coursecontext)) {
        if ($user->address) {
            print_row(get_string("address").":", "$user->address");
        }
        if ($user->phone1) {
            print_row(get_string("phone").":", "$user->phone1");
        }
        if ($user->phone2) {
            print_row(get_string("phone2").":", "$user->phone2");
        }
    }



        

        print_row(get_string("email").":",$user->email);
    

    if ($user->url && !isset($hiddenfields['webpage'])) {
        $url = $user->url;
        if (strpos($user->url, '://') === false) {
            $url = 'http://'. $url;
        }
        print_row(get_string("webpage") .":", "<a href=\"$url\">$user->url</a>");
    }




    /// Print the Custom User Fields
    profile_display_fields($user->id);



    if (!isset($hiddenfields['firstaccess'])) {
        if ($user->firstaccess) {
            $datestring = userdate($user->firstaccess)."&nbsp; (".format_time(time() - $user->firstaccess).")";
        } else {
            $datestring = get_string("never");
        }
        print_row(get_string("firstaccess").":", $datestring);
    }
    if (!isset($hiddenfields['lastaccess'])) {
        if ($user->lastaccess) {
            $datestring = userdate($user->lastaccess)."&nbsp; (".format_time(time() - $user->lastaccess).")";
        } else {
            $datestring = get_string("never");
        }
        print_row(get_string("lastaccess").":", $datestring);
    }



/// Printing Interests
	if( !empty($CFG->usetags)) {
	    if ( $interests = tag_get_tags_csv('user', $user->id) ) { 
            print_row(get_string('interests') .": ", $interests);
        }
    }
/// End of Printing Interests

    echo "</table>";

    echo "</td></tr></table>";

    $userauth = get_auth_plugin($user->auth);



//  Print other functions
    echo '<div class="buttons">';

    if ($course->id != SITEID && empty($course->metacourse)) {   // Mostly only useful at course level

        $canunenrol = false;

        if ($user->id == $USER->id) { // Myself
            $canunenrol = has_capability('moodle/course:view', $coursecontext, NULL) &&              // Course participant
                          has_capability('moodle/role:unassignself', $coursecontext, NULL, false) && // Can unassign myself
                          get_user_roles($coursecontext, $user->id, false);                          // Must have role in course

        } else if (has_capability('moodle/role:assign', $coursecontext, NULL)) { // I can assign roles
            if ($roles = get_user_roles($coursecontext, $user->id, false)) {
                $canunenrol = true;
                foreach($roles as $role) {
                    if (!user_can_assign($coursecontext, $role->roleid)) {
                        $canunenrol = false; // I can not unassign all roles in this course :-(
                        break;
                    }
                }
            }
        }

        if ($canunenrol) {
            echo '<form action="'.$CFG->wwwroot.'/course/unenrol.php" method="get">';
            echo '<div>';
            echo '<input type="hidden" name="id" value="'.$course->id.'" />';
            echo '<input type="hidden" name="user" value="'.$user->id.'" />';
            echo '<input type="submit" value="'.s(get_string('unenrolme', '', $course->shortname)).'" />';
            echo '</div>';
            echo '</form>';
        }
    }

    if (!$user->deleted and $USER->id != $user->id  && empty($USER->realuser) && has_capability('moodle/user:loginas', $coursecontext) &&
                                 ! has_capability('moodle/site:doanything', $coursecontext, $user->id, false)) {
        echo '<form action="'.$CFG->wwwroot.'/course/loginas.php" method="get">';
        echo '<div>';
        echo '<input type="hidden" name="id" value="'.$course->id.'" />';
        echo '<input type="hidden" name="user" value="'.$user->id.'" />';
        echo '<input type="hidden" name="sesskey" value="'.sesskey().'" />';
        echo '<input type="submit" value="'.get_string('loginas').'" />';
        echo '</div>';
        echo '</form>';
    }


    // Authorize.net: User Payments
    if ($course->enrol == 'authorize' || (empty($course->enrol) && $CFG->enrol == 'authorize')) {
        echo "<form action=\"../enrol/authorize/index.php\" method=\"get\">";
        echo "<div>";
        echo "<input type=\"hidden\" name=\"course\" value=\"$course->id\" />";
        echo "<input type=\"hidden\" name=\"user\" value=\"$user->id\" />";
        echo "<input type=\"submit\" value=\"".get_string('payments')."\" />";
        echo "</div>";
        echo "</form>";
    }
    echo "</div>\n";

    if ($CFG->debugdisplay && debugging('', DEBUG_DEVELOPER) && $USER->id == $user->id) {  // Show user object
        echo '<hr />';
        print_heading('DEBUG MODE:  User session variables');
        print_object($USER);
    }

  //  print_footer($course);
  echo'<table class="footer_panel" width="100%" border="0" cellpadding="15" cellspacing="0">
  <tr> 
    <div align="center"><hr /></div>
<div align="center"><font size="1" color="#666666">Best viewed in 1024x786 resolution. NTT Data Inc. All rights reserved.</font><font size="1" color="#666666"><br /></font><font size="1" face="Arial" color="#006699"></div>
<div align="center"><hr /></div>
     </td>
       
  </tr>
</table>';

/// Functions ///////

function print_row($left, $right) {
    echo "\n<tr><td class=\"label c0\">$left</td><td class=\"info c1\">$right</td></tr>\n";
}

?>
