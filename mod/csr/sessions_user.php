<?php

require_once '../../config.php';
require_once 'lib.php';
require_once 'session_form_user.php';

$f = optional_param('f', 0, PARAM_INT); // csr Module ID

$confirm = optional_param('confirm', false, PARAM_BOOL); // delete confirmation

$nbdays = 1; // default number to show
$sucessstr=0;
$session = null;

$errorstr = '';
$context = get_context_instance(CONTEXT_COURSE, $course->id);

$returnurl = $CFG->wwwroot.'/mod/csr/csractivity.php?course=1&user='.$USER->id;

$mform = new mod_csr_session_form(null, compact('id', 'f', 's', 'c', 'nbdays', 'customfields', 'course'));
if ($mform->is_cancelled()){
    redirect($returnurl);
}

if ($fromform = $mform->get_data()) { // Form submitted

    if (empty($fromform->submitbutton)) {
        print_error('error:unknownbuttonclicked', 'csr', $returnurl);
    }

    // Pre-process fields
    if (empty($fromform->allowoverbook)) {
        $fromform->allowoverbook = 0;
    }
    if (empty($fromform->duration)) {
        $fromform->duration = 0;
    }
    if (empty($fromform->normalcost)) {
        $fromform->normalcost = 0;
    }
    if (empty($fromform->discountcost)) {
        $fromform->discountcost = 0;
    }

    $sessiondates = array();

   //    if (!empty($fromform->timestart) and !empty($fromform->timefinish)) {
            $date = new object();
            $date->timestart = $fromform->timestart;
            $date->timefinish = $fromform->timefinish;
            $sessiondates[] = $date;
   //    }


    $todb = new object();
    $todb->csr = 1;
	$todb->eventname = $fromform->eventname;
    $todb->datetimeknown = 1;
    $todb->capacity = $fromform->capacity;
    $todb->allowoverbook = $fromform->allowoverbook;
    $todb->duration = $fromform->duration;
    $todb->normalcost = $fromform->normalcost;
    $todb->discountcost = $fromform->discountcost;
    $todb->details = trim($fromform->details);
	$todb->goals = trim($fromform->goals);
	$todb->category = $fromform->category;
	$todb->subcategory = $fromform->subcategory;
	$todb->location = $fromform->location;
	$todb->venue = $fromform->venue;
	$todb->userevent = 1;

    $sessionid = null;
    begin_sql();

   
        if (!$sessionid = csr_add_session($todb, $sessiondates)) {
            rollback_sql();
            print_error('error:couldnotaddsession', 'csr', $returnurl);
        }
		else
		{
			if (!$usersignupid = csr_indivigual_user_attend($sessionid)) 
			{
			rollback_sql();
            print_error('error:couldnotaddsession', 'csr', $returnurl);			
			}
		
		}



    // Retrieve record that was just inserted/updated
    if (!$session = csr_get_session($sessionid)) {
        rollback_sql();
        print_error('error:couldnotfindsession', 'csr', $returnurl);
    }


    commit_sql();
	$sucessstr=1;
   redirect($returnurl);
}




print_header();
echo'<IMG SRC="feature.jpg">';
echo'<DIV STYLE="position:absolute; top:50px; left:70px; width:1127px; height:232px">';
echo'<b><FONT SIZE="+3" COLOR="ffffff">CSR Registration Tool </FONT></b></DIV>';

print_box_start();
$currenttab = 'indivigual';
    $showroles = 1;
    if (!$user->deleted) {
        include('csrtabs.php');
    }



if (!empty($errorstr)) {
    echo '<div class="notifyproblem" align="center"><span style="font-size: 12px; line-height: 18px;">'.$errorstr.'</span></div>';
}
echo '<FONT COLOR="#8B8989">This page allows you to register for a social event that you had initiated personally. Please note that, in order for the registration to be successful, you must specify a valid start and finish time along with duration.</font>';
    $mform->display();

print_box_end();
if ($sucessstr) {
    echo '<div align="center"><span style="font-size: 12px; line-height: 18px;">Event created sucessfully.</span></div>';
}
//print_footer($course);
echo'<table class="footer_panel" width="100%" border="0" cellpadding="15" cellspacing="0">
  <tr> 
    <div align="center"><hr /></div>
<div align="center"><font size="1" color="#666666">Best viewed in 1024x786 resolution. NTT Data Inc. All rights reserved.</font><font size="1" color="#666666"><br /></font><font size="1" face="Arial" color="#006699"></div>
<div align="center"><hr /></div>
     </td>
       
  </tr>
</table>';
