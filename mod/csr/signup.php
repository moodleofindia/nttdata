<?php

require_once '../../config.php';
require_once 'lib.php';
require_once 'signup_form.php';

$s = required_param('s', PARAM_INT); // csr session ID
$backtoallsessions = optional_param('backtoallsessions', 0, PARAM_INT);

if (!$session = csr_get_session($s)) {
    print_error('error:incorrectcoursemodulesession', 'csr');
}
if (!$csr = get_record('csr', 'id', $session->csr)) {
    print_error('error:incorrectcsrid', 'csr');
}
if (!$course = get_record('course', 'id', $csr->course)) {
    print_error('error:coursemisconfigured', 'csr');
}
if (!$cm = get_coursemodule_from_instance("csr", $csr->id, $course->id)) {
    print_error('error:incorrectcoursemoduleid', 'csr');
}

require_course_login($course);
$context = get_context_instance(CONTEXT_COURSE, $course->id);
require_capability('mod/csr:view', $context);

$returnurl = "$CFG->wwwroot/mod/csr/csractivity.php?course=1&user=".$USER->id."";
if ($backtoallsessions) {
    $returnurl = "$CFG->wwwroot/mod/csr/csractivity.php?course=1&user=".$USER->id."";
}

$pagetitle = format_string($csr->name);
$navlinks[] = array('name' => get_string('modulenameplural', 'csr'), 'link' => "index.php?id=$course->id", 'type' => 'title');
$navlinks[] = array('name' => $pagetitle, 'link' => '', 'type' => 'activityinstance');
$navigation = build_navigation($navlinks);

// Guests can't signup for a session, so offer them a choice of logging in or going back.
if (isguestuser()) {
    $loginurl = $CFG->wwwroot.'/login/index.php';
    if (!empty($CFG->loginhttps)) {
        $loginurl = str_replace('http:','https:', $loginurl);
    }

    print_header_simple($pagetitle, '', $navigation, '', '', true,
                    update_module_button($cm->id, $course->id, get_string('modulename', 'csr')));
    notice_yesno('<p>' . get_string('guestsno', 'csr') . "</p>\n\n</p>" .
        get_string('liketologin') . '</p>', $loginurl, get_referer(false));
    print_footer();
    exit();
}

$manageremail = false;
if (get_config(NULL, 'csr_addchangemanageremail')) {
    $manageremail = csr_get_manageremail($USER->id);
}

$showdiscountcode = ($session->discountcost > 0);

$mform = new mod_csr_signup_form(null, compact('s', 'backtoallsessions', 'manageremail', 'showdiscountcode'));
if ($mform->is_cancelled()){
    redirect($returnurl);
}

if ($fromform = $mform->get_data()) { // Form submitted

    if (empty($fromform->submitbutton)) {
        print_error('error:unknownbuttonclicked', 'csr', $returnurl);
    }

    // User can not update Manager's email (depreciated functionality)
    if (!empty($fromform->manageremail)) {
		$setmemail=csr_set_manageremail($fromform->manageremail);
     //   add_to_log($course->id, 'csr', 'update manageremail (FAILED)', "signup.php?s=$session->id", $csr->id, $cm->id);
    }
	
	 if (!empty($fromform->phonenumber)) {
		//$setmephone=csr_set_phonenumber($fromform->phonenumber);
		
		 $data = new stdclass();
    $data->userid = $USER->id;
    $data->fieldid = $fromform->notificationtype;
   $data->data = $fromform->manageremail;
	$data->phone = $fromform->phonenumber;
	

    if ($dataid = get_field('user_info_data', 'id', 'userid', $USER->id)) {
        $data->id = $dataid;
        if (!update_record('user_info_data', $data)) {
            error_log('CSR: could not update existing custom field data');
            rollback_sql();
            return false;
        }
    }
		//csr_set_managerphone($fromform->phonenumber);
     //   add_to_log($course->id, 'csr', 'update manageremail (FAILED)', "signup.php?s=$session->id", $csr->id, $cm->id);
    }

    // Get signup type
    if (!$session->datetimeknown) {
        $statuscode = MDL_CSR_STATUS_WAITLISTED;
    } elseif (csr_get_num_attendees($session->id) < $session->capacity) {
        // Save available
        $statuscode = MDL_CSR_STATUS_BOOKED;
    } else {
        $statuscode = MDL_CSR_STATUS_WAITLISTED;
    }

    if (!csr_session_has_capacity($session, $context) && (!$session->allowoverbook)) {
        print_error('sessionisfull', 'csr', $returnurl);
    }
    elseif (csr_get_user_session_submissions($session->id, $USER->id)) {
        print_error('alreadysignedup', 'csr', $returnurl);
    }
    elseif (csr_manager_needed($csr) && !csr_get_manageremail($USER->id)){
        print_error('error:manageremailaddressmissing', 'csr', $returnurl);
    }
    elseif ($submissionid = csr_user_signup($session, $csr, $course, $fromform->discountcode, $fromform->notificationtype, $statuscode)) {
        add_to_log($course->id, 'csr','signup',"signup.php?s=$session->id", $session->id, $cm->id);

        $message = get_string('bookingcompleted', 'csr');
        if ($session->datetimeknown && $csr->confirmationinstrmngr) {
            $message .= '<br /><br />'.get_string('confirmationsentmgr', 'csr');
        }
        else {
            $message .= '<br /><br />'.get_string('confirmationsent', 'csr');
        }

        $timemessage = 4;
        redirect($returnurl, $message, $timemessage);
    }
    else {
        add_to_log($course->id, 'csr','signup (FAILED)',"signup.php?s=$session->id", $session->id, $cm->id);
        print_error('error:problemsigningup', 'csr', $returnurl);
    }

    redirect($returnurl);
}
elseif ($manageremail !== false) {
    // Set values for the form
    $toform = new object();
    $toform->manageremail = $manageremail;
    $mform->set_data($toform);
}

				     print_header();
echo'<IMG SRC="feature.jpg">';
echo'<DIV STYLE="position:absolute; top:50px; left:70px; width:1127px; height:232px">';
echo'<b><FONT SIZE="+3" COLOR="ffffff">CSR Registration Tool </FONT></b></DIV>';

$heading = get_string('signupfor', 'csr', $csr->name);

$viewattendees = has_capability('mod/csr:viewattendees', $context);
$signedup = csr_check_signup($session->id);

$currenttab = 'summary';
    $showroles = 1;
    if (!$user->deleted) {
        include('csrtabs.php');
    }


print_box_start();
print_heading($heading, 'center');

if (!$signedup && !csr_session_has_capacity($session, $context) && (!$session->allowoverbook)) {
    print_error('sessionisfull', 'csr', $returnurl);
    print_box_end();
    print_footer($course);
    exit;
}

csr_print_session($session, $viewattendees);

if ($signedup) {
    // Cancellation link
    echo '<a href="'.$CFG->wwwroot.'/mod/csr/cancelsignup.php?s='.$session->id.'&amp;backtoallsessions='.$backtoallsessions.'" title="'.get_string('cancelbooking','csr').'">'.get_string('cancelbooking', 'csr').'</a>';

    // See attendees link
    if ($viewattendees) {
        echo ' &ndash; <a href="'.$CFG->wwwroot.'/mod/csr/attendees.php?s='.$session->id.'&amp;backtoallsessions='.$backtoallsessions.'" title="'.get_string('seeattendees', 'csr').'">'.get_string('seeattendees', 'csr').'</a>';
    }

    echo '<br/><a href="'.$returnurl.'" title="'.get_string('goback', 'csr').'">'.get_string('goback', 'csr').'</a>';
}
// Don't allow signup to proceed if a manager is required
elseif (csr_manager_needed($csr) && !csr_get_manageremail($USER->id)){
    // Check to see if the user has a managers email set
    echo '<p><strong>'.get_string('error:manageremailaddressmissing', 'csr').'</strong></p>';
    echo '<br/><a href="'.$returnurl.'" title="'.get_string('goback', 'csr').'">'.get_string('goback', 'csr').'</a>';
} elseif (!has_capability('mod/csr:signup', $context)) {
    echo '<p><strong>'.get_string('error:nopermissiontosignup', 'csr').'</strong></p>';
    echo '<br/><a href="'.$returnurl.'" title="'.get_string('goback', 'csr').'">'.get_string('goback', 'csr').'</a>';
} else {
    // Signup form
    $mform->display();
}

print_box_end();
//print_footer($course);
echo'<table class="footer_panel" width="100%" border="0" cellpadding="15" cellspacing="0">
  <tr> 
    <div align="center"><hr /></div>
<div align="center"><font size="1" color="#666666">Best viewed in 1024x786 resolution. NTT Data Inc. All rights reserved.</font><font size="1" color="#666666"><br /></font><font size="1" face="Arial" color="#006699"></div>
<div align="center"><hr /></div>
     </td>
       
  </tr>
</table>';
