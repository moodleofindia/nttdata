<?php

require_once '../../config.php';
require_once 'lib.php';
require_once 'cancelsignup_form.php';

$s  = required_param('s', PARAM_INT); // csr session ID
$confirm           = optional_param('confirm', false, PARAM_BOOL);
$backtoallsessions = optional_param('backtoallsessions', 0, PARAM_INT);

if (!$session = csr_get_session($s)) {
    print_error('error:incorrectcoursemodulesession', 'csr');
}
if (!$csr = get_record('csr', 'id', $session->csr)) {
    print_error('error:incorrectcsrid', 'csr');
}
if (!$course = get_record('course', 'id', $csr->course)) {
    print_error('error:coursemisconfigured', 'csr');
}
if (!$cm = get_coursemodule_from_instance("csr", $csr->id, $course->id)) {
    print_error('error:incorrectcoursemoduleid', 'csr');
}

require_course_login($course);
$context = get_context_instance(CONTEXT_COURSE, $course->id);
require_capability('mod/csr:view', $context);

$returnurl = "$CFG->wwwroot/mod/csr/csractivity.php?course=1&userid=$USER->id";
if ($backtoallsessions) {
    $returnurl = "$CFG->wwwroot/mod/csr/csractivity.php?course=1&userid=$USER->id";
}

$mform = new mod_csr_cancelsignup_form(null, compact('s', 'backtoallsessions'));
if ($mform->is_cancelled()){
    redirect($returnurl);
}

if ($fromform = $mform->get_data()) { // Form submitted

    if (empty($fromform->submitbutton)) {
        print_error('error:unknownbuttonclicked', 'csr', $returnurl);
    }

    $timemessage = 4;

    $errorstr = '';
    if (csr_user_cancel($session, false, false, $errorstr, $fromform->cancelreason)) {
        add_to_log($course->id, 'csr', 'cancel booking', "cancelsignup.php?s=$session->id", $csr->id, $cm->id);

        $message = get_string('bookingcancelled', 'csr');

        if ($session->datetimeknown) {
            $error = csr_send_cancellation_notice($csr, $session, $USER->id);
            if (empty($error)) {
                if ($session->datetimeknown && $csr->cancellationinstrmngr) {
                    $message .= '<br /><br />'.get_string('cancellationsentmgr', 'csr');
                }
                else {
                    $message .= '<br /><br />'.get_string('cancellationsent', 'csr');
                }
            }
            else {
                error($error);
            }
        }

        redirect($returnurl, $message, $timemessage);
    }
    else {
        add_to_log($course->id, 'csr', "cancel booking (FAILED)", "cancelsignup.php?s=$session->id", $csr->id, $cm->id);
        redirect($returnurl, $errorstr, $timemessage);
    }

    redirect($returnurl);
}

$pagetitle = format_string($csr->name);
$navlinks[] = array('name' => get_string('modulenameplural', 'csr'), 'link' => "index.php?id=$course->id", 'type' => 'title');
$navlinks[] = array('name' => $pagetitle, 'link' => '', 'type' => 'activityinstance');
$navigation = build_navigation($navlinks);
				     print_header();
echo'<IMG SRC="feature.jpg">';
echo'<DIV STYLE="position:absolute; top:50px; left:70px; width:1127px; height:232px">';
echo'<b><FONT SIZE="+3" COLOR="ffffff">CSR Registration Tool </FONT></b></DIV>';

$heading = get_string('cancelbookingfor', 'csr', $csr->name);

$viewattendees = has_capability('mod/csr:viewattendees', $context);
$signedup = csr_check_signup($session->id);
$currenttab = 'summary';
    $showroles = 1;
    if (!$user->deleted) {
        include('csrtabs.php');
    }
print_box_start();
print_heading($heading, 'center');

if ($signedup) {

    $mform->display();
}
else {
    print_error('notsignedup', 'csr', $returnurl);
}

print_box_end();
//print_footer($course);
echo'<table class="footer_panel" width="100%" border="0" cellpadding="15" cellspacing="0">
  <tr> 
    <div align="center"><hr /></div>
<div align="center"><font size="1" color="#666666">Best viewed in 1024x786 resolution. NTT Data Inc. All rights reserved.</font><font size="1" color="#666666"><br /></font><font size="1" face="Arial" color="#006699"></div>
<div align="center"><hr /></div>
     </td>
       
  </tr>
</table>';