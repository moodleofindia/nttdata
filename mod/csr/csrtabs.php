<style> 
.csruserinfobox {
  margin-bottom:5px;
  border-width: 0px;
  border-style: solid;
  border-collapse: separate;
}

.csruserinfobox .left,
.csruserinfobox .side {
  padding: 10px;
  width: 100px;
  vertical-align: top;
}

.csruserinfobox .userpicture {
  width: 100px;
  height: 100px;
}

.csruserinfobox .content {
  padding: 10px;
  vertical-align: top;
}

.csruserinfobox .links {
  width: 100px;
  padding: 5px;
  vertical-align: bottom;
}

.csruserinfobox .list td {
  padding: 3px;
}



table.csruserinfobox {
  width: 80%;
  margin-left: 10%;
  margin-right: 10%;
}
</style> 

<?php  // $Id: tabs.php,v 1.43.2.8 2008/12/19 01:44:48 moodler Exp $
/// This file to be included so we can assume config.php has already been included.
/// We also assume that $user, $course, $currenttab have been set

    if (!isset($filtertype)) {
        $filtertype = '';
    }
    if (!isset($filterselect)) {
        $filterselect = '';
    }

    //make sure everything is cleaned properly
    $filtertype   = clean_param($filtertype, PARAM_ALPHA);
    $filterselect = clean_param($filterselect, PARAM_INT);


    if (($filtertype == 'site' && $filterselect) || ($filtertype=='user' && $filterselect)) {
        $user = get_record('user','id',$filterselect);
    }
	if ($user->lastaccess) {

    $lastaccess = '<FONT  COLOR="#FFD700">active from '.format_time(time() - $user->lastaccess, $datestring).' ago </font>';
    } else {
    $lastaccess = '<FONT  COLOR="#FFD700">currently online</font>';
    }

    $inactive = NULL;
    $activetwo = NULL;
    $toprow = array();



            $user = get_record('user','id', $USER->id);

		echo '<table width="80%" class="csruserinfobox">';
		echo '<tr>';
		echo '<td class="side">';
		print_user_picture($user, $course->id, $user->picture, true, false, false);
		echo '</td><td class="content">';
		echo '<h2>'.$user->firstname.' '.$user->lastname.'</h2>';
		echo $lastaccess;
		echo '</td>';
		echo '</table>';
        

        $systemcontext   = get_context_instance(CONTEXT_SYSTEM);
        $coursecontext   = get_context_instance(CONTEXT_COURSE, $course->id);
        $personalcontext = get_context_instance(CONTEXT_USER, $user->id);

		 $toprow[] = new tabobject('summary', $CFG->wwwroot.'/mod/csr/csractivity.php?course=1&amp;user=' . $user->id, 'Summary');
		 $toprow[] = new tabobject('calendar', $CFG->wwwroot.'/mod/csr/calendar/view.php', 'Calendar');
		 $toprow[] = new tabobject('indivigual', $CFG->wwwroot.'/mod/csr/sessions_user.php?f=1', 'Individual Activity');
         $toprow[] = new tabobject('profile', $CFG->wwwroot.'/mod/csr/csrview.php?id='.$user->id, get_string('profile'));


           
 

    /// Find out if user allowed to see all reports of this user (usually parent) or individual course reports

        $myreports  = ($course->showreports and $USER->id == $user->id);
        $anyreport  = has_capability('moodle/user:viewuseractivitiesreport', $personalcontext);



    if (!empty($secondrow)) {
        $tabs = array($toprow, $secondrow);
    } else {
        $tabs = array($toprow);
    }

      /// Print out the tabs and continue!
      print_tabs($tabs, $currenttab, $inactive, $activetwo);


?>
