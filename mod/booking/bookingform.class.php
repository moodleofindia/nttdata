<?php
require_once $CFG->libdir.'/formslib.php';

class mod_booking_bookingform_form extends moodleform {
	function definition() {
		global $CFG;
		$cid=$_SESSION['cid'];
		$mform =& $this->_form;

		// visible elements

		$mform->addElement('header', '', get_string('addeditbooking','booking'));

		$mform->addElement('text', 'text', get_string('booking','booking'), array('size'=>'64'));
		$mform->addRule('text', get_string('required'), 'required', null,'client');
		if (!empty($CFG->formatstringstriptags)) {
			$mform->setType('text', PARAM_TEXT);
		} else {
			$mform->setType('text', PARAM_CLEAN);
		}
		$mform->addElement('checkbox', 'limitanswers', get_string('limitanswers','booking'));

		$mform->addElement('text', 'maxanswers', get_string('maxparticipantsnumber','booking'));
		$mform->setType('maxanswers', PARAM_INT);
		$mform->disabledIf('maxanswers', 'limitanswers', 'notchecked');

		$mform->addElement('text', 'maxoverbooking', get_string('maxoverbooking','booking'));
		$mform->setType('maxoverbooking', PARAM_INT);
		$mform->disabledIf('maxoverbooking', 'limitanswers', 'notchecked');

		$mform->addElement('checkbox', 'restrictanswerperiod', get_string('timerestrict', 'booking'));

		$mform->addElement('date_time_selector', 'bookingclosingtime', get_string("bookingclose", "booking"));
		$mform->disabledIf('bookingclosingtime', 'restrictanswerperiod', 'notchecked');
		
		$coursearray = array();
		$coursearray[0] = get_string('donotselectcourse', 'booking');
		$allcourses = get_records_sql("SELECT id,name as shortname FROM mdl_quiz where course=$cid");
		foreach ($allcourses as $id => $courseobject) {
			$coursearray[$id] = $courseobject->shortname;
		}
		$mform->addElement('select', 'courseid', get_string('choosecourse','booking'), $coursearray);

		$mform->addElement('checkbox', 'startendtimeknown', get_string('startendtimeknown','booking'));

		$mform->addElement('date_time_selector', 'coursestarttime', get_string("coursestarttime", "booking"));
		$mform->setType('coursestarttime', PARAM_INT);
		$mform->disabledIf('coursestarttime', 'startendtimeknown', 'notchecked');

		$mform->addElement('date_time_selector', 'courseendtime', get_string("courseendtime", "booking"));
		$mform->setType('courseendtime', PARAM_INT);
		$mform->disabledIf('courseendtime', 'startendtimeknown', 'notchecked');

$mform->addElement('text', 'location', get_string('location','booking'), array('size'=>'40'));
$mform->addElement('text', 'venue', get_string('venue','booking'), array('size'=>'40'));
$mform->addElement('text', 'room', get_string('room','booking'), array('size'=>'40'));
		$mform->addElement('htmleditor', 'description', 'Additional Instructions');
		$mform->setType('description', PARAM_RAW);

		//hidden elements
		$mform->addElement('hidden', 'id');
		$mform->setType('id', PARAM_INT);
		
		$mform->addElement('hidden', 'bookingid');
		$mform->setType('bookingid', PARAM_INT);

		$mform->addElement('hidden', 'optionid');
		$mform->setType('optionid', PARAM_INT);
		//-------------------------------------------------------------------------------
		// buttons
		//
		$buttonarray=array();
		$buttonarray[] = &$mform->createElement('submit', 'submitbutton', get_string('savechangesanddisplay'));
		$buttonarray[] = &$mform->createElement("submit",'submittandaddnew', get_string('submitandaddnew','booking'));
		$buttonarray[] = &$mform->createElement('cancel');
		$mform->addGroup($buttonarray, 'buttonar', '', array(' '), false);
		$mform->closeHeaderBefore('buttonar');
		//$this->add_action_buttons();

	}
}
?>