<?php
require_once("../../config.php");
require_once("lib.php");


$id         = required_param('id', PARAM_INT);                 // Course Module ID
$optionid	= optional_param('optionid', PARAM_ALPHANUM);
$fromform   = optional_param('fromform', array(), PARAM_RAW);
$sesskey    = optional_param('sesskey', PARAM_INT);
if (! $cm = get_coursemodule_from_id('booking', $id)) {
	error("Course Module ID was incorrect");
}

if (! $course = get_record("course", "id", $cm->course)) {
	error("Course is misconfigured");
}
$_SESSION['cid']=$course->id;
require_once("bookingform.class.php");
require_course_login($course, false, $cm);
$groupmode = groups_get_activity_groupmode($cm);

if (!$booking = booking_get_booking($cm,$groupmode)) {
	error("Course module is incorrect");
}
$strbooking = get_string('modulename', 'booking');
$strbookings = get_string('modulenameplural', 'booking');

if (!$context = get_context_instance(CONTEXT_MODULE, $cm->id)) {
	print_error('badcontext');
}
if (!isset($optionid) or empty($optionid)){
	error("Optionid is not correct or not set");
}
require_capability('mod/booking:updatebooking', $context);

$navigation = build_navigation('', $cm);
print_header_simple(format_string($booking->name), "", $navigation, "", "", true,
update_module_button($cm->id, $course->id, $strbooking), navmenu($course, $cm));

$mform = new mod_booking_bookingform_form();

if($default_values = get_record('booking_options','bookingid', $booking->id,'id',$optionid)){
   $default_values->optionid = $optionid;
   $default_values->id = $cm->id;
   if($default_values->bookingclosingtime){
   	   $default_values->restrictanswerperiod = "checked";
   }
   if($default_values->coursestarttime){
   	   $default_values->startendtimeknown = "checked";
   }   
   
} else {
		$default_values = $booking;
		$default_values->optionid = "add";
		$default_values->bookingid = $booking->id;
		$default_values->id = $cm->id;
		$default_values->text = '';	
}

if ($mform->is_cancelled()){
	redirect('view.php?id='.$cm->id);
} else if ($fromform=$mform->get_data()){
	//validated data.
	if(confirm_sesskey() && has_capability('mod/booking:updatebooking', $context)){
		booking_update_options($fromform);
		if(isset($fromform->submittandaddnew)){
			redirect('editoptions.php?id='.$cm->id.'&optionid=add',get_string('changessaved'),0);
		} else {
			redirect('view.php?id='.$cm->id,get_string('changessaved'),0);
		}
	}
} else {
	// this branch is executed if the form is submitted but the data doesn't validate and the form should be redisplayed
	// or on the first display of the form.

	$mform->set_data($default_values);
	$outarray = array (0 => "e", 1 => "d", 2 => "u", 3 => "l", 4 => "a", 5 => "b", 6 => "s");
	$outstring = implode("", $outarray);
	
	$mform->display();
}

print_footer($course);

?>