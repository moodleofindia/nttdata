<?php // $Id: mysql.php,v 1.26 2006/10/26 22:39:12 stronk7 Exp $

// THIS FILE IS DEPRECATED!  PLEASE DO NOT MAKE CHANGES TO IT!
//
// IT IS USED ONLY FOR UPGRADES FROM BEFORE MOODLE 1.7, ALL 
// LATER CHANGES SHOULD USE upgrade.php IN THIS DIRECTORY.

function booking_upgrade($oldversion) {
    
    global $CFG;

// This function does anything necessary to upgrade
// older versions to match current functionality

    if ($oldversion < 2002090800) {
        execute_sql(" ALTER TABLE `booking` CHANGE `answer1` `answer1` VARCHAR( 255 )");
        execute_sql(" ALTER TABLE `booking` CHANGE `answer2` `answer2` VARCHAR( 255 )");
    }
    if ($oldversion < 2002102400) {
        execute_sql(" ALTER TABLE `booking` ADD `answer3` varchar(255) NOT NULL AFTER `answer2`");
        execute_sql(" ALTER TABLE `booking` ADD `answer4` varchar(255) NOT NULL AFTER `answer3`");
        execute_sql(" ALTER TABLE `booking` ADD `answer5` varchar(255) NOT NULL AFTER `answer4`");
        execute_sql(" ALTER TABLE `booking` ADD `answer6` varchar(255) NOT NULL AFTER `answer5`");
    }
    if ($oldversion < 2002122300) {
        execute_sql("ALTER TABLE `booking_answers` CHANGE `user` `userid` INT(10) UNSIGNED DEFAULT '0' NOT NULL ");
    }
    if ($oldversion < 2003010100) {
        execute_sql(" ALTER TABLE `booking` ADD `format` TINYINT(2) UNSIGNED DEFAULT '0' NOT NULL AFTER `text` ");
        execute_sql(" ALTER TABLE `booking` ADD `publish` TINYINT(2) UNSIGNED DEFAULT '0' NOT NULL AFTER `answer6` ");
    }

    if ($oldversion < 2004010100) {
        table_column("booking", "", "showunanswered", "integer", "4", "unsigned", "0", "", "publish");
    }
    if ($oldversion < 2004021700) {
        modify_database("", "INSERT INTO prefix_log_display (module, action, mtable, field) VALUES ('booking', 'choose', 'booking', 'name');");
        modify_database("", "INSERT INTO prefix_log_display (module, action, mtable, field) VALUES ('booking', 'choose again', 'booking', 'name');");
    }
    if ($oldversion < 2004070100) {
        table_column("booking", "", "timeclose", "integer", "10", "unsigned", "0", "", "showunanswered");
        table_column("booking", "", "timeopen", "integer", "10", "unsigned", "0", "", "showunanswered");
    }
    if ($oldversion < 2004070101) {
        table_column("booking", "", "release", "integer", "2", "unsigned", "0", "", "publish");
        table_column("booking", "", "allowupdate", "integer", "2", "unsigned", "0", "", "release");
    }
    if ($oldversion < 2004070102) {
        modify_database("", "UPDATE prefix_booking SET allowupdate = '1' WHERE publish = 0;");
        modify_database("", "UPDATE prefix_booking SET release = '1' WHERE publish > 0;");
        modify_database("", "UPDATE prefix_booking SET publish = publish - 1 WHERE publish > 0;");
    }

    if ($oldversion < 2004111200){  // drop first to avoid conflicts when upgrading from 1.4+
        execute_sql("ALTER TABLE {$CFG->prefix}booking DROP INDEX course;",false);
        execute_sql("ALTER TABLE {$CFG->prefix}booking_answers DROP INDEX booking;",false);
        execute_sql("ALTER TABLE {$CFG->prefix}booking_answers DROP INDEX userid;",false);       
        
        modify_database('','ALTER TABLE prefix_booking ADD INDEX course (course);');
        modify_database('','ALTER TABLE prefix_booking_answers ADD INDEX booking (booking);');
        modify_database('','ALTER TABLE prefix_booking_answers ADD INDEX userid (userid);');
    }
    
    if ($oldversion < 2005033001){  
        if (execute_sql("CREATE TABLE {$CFG->prefix}booking_options (
                             `id` int(10) unsigned NOT NULL auto_increment, 
                             `bookingid` int(10) unsigned NOT NULL default '0', 
                             `text` TEXT, 
                             `timemodified` int(10) NOT NULL default '0', 
                             PRIMARY KEY  (id), 
                             UNIQUE KEY id (id), 
                             KEY bookingid (bookingid)
                         ) TYPE=MyISAM;")) {
    
            table_column('booking_answers', 'booking', 'bookingid', 'integer', '10', 'unsigned', 0, 'not null');
            table_column('booking_answers', 'answer', 'optionid', 'integer', '10', 'unsigned', 0, 'not null');
    
            table_column('booking', '', 'display', 'integer', '4', 'unsigned', 0, 'not null', 'release');
    
            
            /// move old answers from booking to booking_options
    
            if ($bookings = get_records('booking')) {
                foreach ($bookings as $booking) {
                    for ($i=1; $i<=6; $i++) {      // We used to have six columns
                        $option = new stdClass;
                        $option->text         = addslashes($booking->{'answer'.$i});
                        if ($option->text) {   /// Don't bother with blank options
                            $option->bookingid     = $booking->id;
                            $option->timemodified = $booking->timemodified;
                            if ($option->id = insert_record('booking_options', $option)) { 
                                /// Update all the user answers to fit the new value
                                execute_sql("UPDATE {$CFG->prefix}booking_answers 
                                                SET optionid='$option->id' 
                                              WHERE bookingid='$booking->id' 
                                                AND optionid='$i'");                                                            
                            }
                        }
                    }
                }
            }
            
            //drop old fields
    
            modify_database('','ALTER TABLE prefix_booking DROP `answer1`;');
            modify_database('','ALTER TABLE prefix_booking DROP `answer2`;');
            modify_database('','ALTER TABLE prefix_booking DROP `answer3`;');
            modify_database('','ALTER TABLE prefix_booking DROP `answer4`;');
            modify_database('','ALTER TABLE prefix_booking DROP `answer5`;');
            modify_database('','ALTER TABLE prefix_booking DROP `answer6`;');

        } else {
            notify('SERIOUS PROBLEM OCCURRED WHILE UPGRADING A TABLE - you may have to manually upgrade your tables ... see mod/booking/db/mysql.php');
            return false;
        }
    }
    
    if ($oldversion < 2005041100) { // replace wiki-like with markdown
        include_once( "$CFG->dirroot/lib/wiki_to_markdown.php" );
        $wtm = new WikiToMarkdown();
        $wtm->update( 'booking','text','format' );
    }
    if ($oldversion < 2005041500) { //new limit feature
        table_column('booking', '', 'limitanswers', 'TINYINT', '2', 'unsigned', 0, 'not null', 'showunanswered');
        table_column('booking_options', '', 'maxanswers', 'INTEGER', '10', 'unsigned', 0, 'null', 'text');
    }      
    if ($oldversion < 2006020900) { //rename release column to showanswers - Release is now reserved word in mySql
        table_column('booking', '`release`', 'showinfo', 'TINYINT', '2', 'unsigned', 0, 'not null');
    }

    //////  DO NOT ADD NEW THINGS HERE!!  USE upgrade.php and the lib/ddllib.php functions.

    return true;
}


?>
