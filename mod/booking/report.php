<?php  // $Id: report.php,v 1.60.2.8 2009/11/30 17:12:18 sam_marshall Exp $

require_once("../../config.php");
require_once("lib.php");

$id         = required_param('id', PARAM_INT);   //moduleid
$download   = optional_param('download', '', PARAM_ALPHA);
$action     = optional_param('action', '', PARAM_ALPHANUM);
$attemptids = optional_param('attemptid', array(), PARAM_INT); //get array of responses to delete.
$confirm 	= optional_param('confirm', PARAM_INT);
$optionid   = optional_param('optionid','', PARAM_INT);

if (! $cm = get_coursemodule_from_id('booking', $id)) {
	error("Course Module ID was incorrect");
}

if (! $course = get_record("course", "id", $cm->course)) {
	error("Course module is misconfigured");
}

require_login($course->id, false, $cm);

$context = get_context_instance(CONTEXT_MODULE, $cm->id);

require_capability('mod/booking:readresponses', $context);

/// Check to see if groups are being used in this booking
$groupmode = groups_get_activity_groupmode($cm);
if (!$booking = booking_get_booking($cm, $groupmode)) {
	error("Course module is incorrect");
}

$strbooking = get_string("modulename", "booking");
$strbookings = get_string("modulenameplural", "booking");
$strresponses = get_string("responses", "booking");

add_to_log($course->id, "booking", "report", "report.php?id=$cm->id", "$booking->id",$cm->id);

if ($action == 'deletebookingoption' && $confirm == 1 && has_capability('mod/booking:updatebooking',$context) && confirm_sesskey()) {
	booking_delete_booking_option($booking->id, $optionid); //delete booking_option
	redirect("report.php?id=$cm->id");
} elseif ($action == 'deletebookingoption' && has_capability('mod/booking:updatebooking',$context) && confirm_sesskey()) {
	$navigation = build_navigation('', $cm);
print_header_simple(format_string($booking->name), "", $navigation, "", "", true);
	notice_yesno(get_string('confirmdeletebookingoption','booking'),'report.php?id='.$cm->id.'&action=deletebookingoption&confirm=1&sesskey='.$USER->sesskey.'&optionid='.$optionid,'report.php?id='.$cm->id);
	print_footer();
	die;
}
if (data_submitted() && $action == 'delete' && $confirm == 1 && has_capability('mod/booking:deleteresponses',$context) && confirm_sesskey()) {
	booking_delete_responses($attemptids, $booking->id); //delete responses.
	redirect("report.php?id=$cm->id");
} elseif (data_submitted() && $action == 'delete' && has_capability('mod/booking:deleteresponses',$context) && confirm_sesskey()) {
	print_header();
	foreach($attemptids as $optionid => $attemptobjects){
		if(!is_array($attemptobjects) || empty($attemptobjects)) {
			return false;
		}
		foreach($attemptobjects as $key => $attemptid) {
			$stroptionname = "attemptid[$optionid][$key]";
			$confirmarray[$stroptionname] = $attemptid;
		}
	}
	notice_yesno(get_string('deleteuserfrombooking','booking'),'report.php?id='.$cm->id.'&action=delete&confirm=1&sesskey='.$USER->sesskey,'report.php?id='.$cm->id,$confirmarray);
	print_footer();
	die;
}

if (data_submitted() && $action == 'subscribe' && $confirm == 1 && confirm_sesskey()) {  // subscription confirmed - do it
	booking_subscribe_tocourse($attemptids, $booking->id);
	redirect("report.php?id=$cm->id");
} elseif (data_submitted() && $action == 'subscribe' && confirm_sesskey()) { // subscription submitted - confirm it
	print_header();

	foreach($attemptids as $optionid => $attemptobjects){
		$optionid1 = $optionid;
		if(!is_array($attemptobjects) || empty($attemptobjects)) {
			return false;
		}
		foreach($attemptobjects as $key => $userid) {
			$straddselect = "addselect[$userid]";
			$confirmarray[$straddselect] = $userid;
		}
	}
	$courseid = get_field('booking_options', 'courseid', 'id', $optionid1);
	if($courseid != 0){
		$coursecontext = get_context_instance(CONTEXT_COURSE, $courseid);
		notice_yesno(get_string('subscribeuser','booking'), "$CFG->wwwroot/$CFG->admin/roles/assign.php?contextid=".$coursecontext->id."&roleid=5&add=1&sesskey=$USER->sesskey",'report.php?id='.$cm->id,$confirmarray);
	} else {
		error("No course selected for this booking-option", "report.php?id=$cm->id");
	}
	print_footer();
	die;
}

if (!$download) {
	$navigation = build_navigation($strresponses, $cm);
	print_header_simple(format_string($booking->name).": $strresponses", "", $navigation, "", '', true,
	update_module_button($cm->id, $course->id, $strbooking), navmenu($course, $cm));

	if ($groupmode) {
		groups_get_activity_group($cm, true);
		groups_print_activity_menu($cm, $CFG->wwwroot . '/mod/booking/report.php?id='.$id);
	}
} else {
	$groupmode = groups_get_activity_groupmode($cm);
}
$bookinglist = booking_get_spreadsheet_data($booking, $cm, $groupmode);

if ($download == "ods" OR $download == "xls" && has_capability('mod/booking:downloadresponses', $context)) {
	if ($action == "all"){
		$filename = clean_filename("$course->shortname ".strip_tags(format_string($booking->name,true)));
	} else {
		$optionname = $booking->option[$action]->text;
		$filename = clean_filename(strip_tags(format_string($optionname,true)));
	}
	if ( $download =="ods"){
		require_once("$CFG->libdir/odslib.class.php");
		$workbook = new MoodleODSWorkbook("-");
		$filename .= '.ods';
	}  else {
		require_once("$CFG->libdir/excellib.class.php");
		$workbook = new MoodleExcelWorkbook("-");
		$filename .= '.xls';
	}


	/// Send HTTP headers
	$workbook->send($filename);
	/// Creating the first worksheet
	$myxls =& $workbook->add_worksheet($strresponses);
	if ( $download =="ods"){
		$cellformat =& $workbook->add_format(array('bg_color' => 'white'));
		$cellformat1 =& $workbook->add_format(array('bg_color' => 'red'));
	} else {
		$cellformat = '';
		$cellformat1 =& $workbook->add_format(array('fg_color' => 'red'));
	}
	/// Print names of all the fields
	$myxls->write_string(0,0,get_string("booking","booking"));
	$myxls->write_string(0,1,get_string("username"));
	$myxls->write_string(0,2,get_string("firstname"));
	$myxls->write_string(0,3,get_string("lastname"));
	$myxls->write_string(0,4,get_string("email"));
	$myxls->write_string(0,5,"Manager");
	$myxls->write_string(0,6,"Manager email");
	$i=7;

	$myxls->write_string(0,$i++,get_string("group"));
	/// generate the data for the body of the spreadsheet
	$row=1;

	if ($bookinglist && ($action == "all")) { // get list of all booking options
		foreach ($bookinglist as $optionid => $optionvalue) {

			$option_text = booking_get_option_text($booking, $optionid);
			foreach ($bookinglist[$optionid] as $usernumber => $user) {
				if ($usernumber > $booking->option[$optionid]->maxanswers){
					$cellform = $cellformat1;
				} else {
					$cellform = $cellformat;
				}
				if (isset($option_text)) {
					$myxls->write_string($row,0,format_string($option_text,true));
				}
				$myxls->write_string($row,1,$user->username,$cellform);
				$myxls->write_string($row,2,$user->firstname,$cellform);
				$myxls->write_string($row,3,$user->lastname,$cellform);
				$myxls->write_string($row,4,$user->email,$cellform);
				
				if ($managers=get_record_select('user', 'username = '.$user->manager_portalid)){
				
				$myxls->write_string($row,5,$managers->firstname.','.$managers->lastname,$cellform);
				$myxls->write_string($row,6,$managers->email,$cellform);
				}
				$i=7;

				$studentid=(!empty($user->idnumber) ? $user->idnumber : " ");
				$ug2 = '';
				if ($usergrps = groups_get_all_groups($course->id, $user->id)) {
					foreach ($usergrps as $ug) {
						$ug2 = $ug2. $ug->name;
					}
				}
				//$myxls->write_string($row,12,$ug2);
				$row++;
				$pos=7;
			}
		}
	} elseif ($bookinglist && !empty($bookinglist[$action])) { // get list of one specified booking option: $action is $optionid
		foreach ($bookinglist[$action] as $usernumber => $user) {
			
			if ($usernumber > $booking->option[$action]->maxanswers){
				$cellform = $cellformat1;
			} else {
				$cellform = $cellformat;
			}
			if (isset($option_text)) {
				$myxls->write_string($row,0,format_string($option_text,true));
			}
			$myxls->write_string($row,1,$user->username,$cellform);
			$myxls->write_string($row,2,$user->firstname,$cellform);
			$myxls->write_string($row,3,$user->lastname,$cellform);
			$myxls->write_string($row,4,$user->email,$cellform);
			$i=5;
			if ($managers=get_record_select('user', 'username = '.$user->manager_portalid)){
				
				$myxls->write_string($row,5,$managers->firstname.','.$managers->lastname,$cellform);
				$myxls->write_string($row,6,$managers->email,$cellform);
				}
				$i=7;

				$studentid=(!empty($user->idnumber) ? $user->idnumber : " ");
				$ug2 = '';
				if ($usergrps = groups_get_all_groups($course->id, $user->id)) {
					foreach ($usergrps as $ug) {
						$ug2 = $ug2. $ug->name;
					}
				}

			//$myxls->write_string($row,12,$ug2);
			$row++;
			$pos=4;
		}
	}
	/// Close the workbook
	$workbook->close();
	exit;
}

booking_show_results($booking, $course, $cm, $bookinglist); //show table with students responses.
echo "<br />";
print_box_start('box mdl-align');

//now give links for downloading spreadsheets.
if (!empty($bookinglist) && has_capability('mod/booking:downloadresponses',$context)) {
	/// Download spreadsheet for each booking option and all booking options
	$optionstochoose = array( 'all' => get_string('allbookingoptions', 'booking'));
	foreach ($booking->option as $option){
		$optionstochoose[$option->id] = $option->text .' - '.$option->coursestarttime.' - '.$option->location ;
	}
	echo '<h2 class="main">'.get_string('download', 'booking').' '.get_string('userdata').'</h2>';
	echo '<form id="downloadform" method="get" action="report.php"><input type="hidden" name="sesskey" value="'.sesskey().'" />
	<input type="hidden" name="id" value="'.$cm->id.'" /><input type="hidden" name="download" value="ods" />';
	$downloadoptions = array();
	$options = array( "ods" => "ods", "xls" => "xls");
	echo choose_from_radio($options, 'download', 'ods');

	echo choose_from_menu($optionstochoose, 'action', '', get_string('userdownload', 'booking'), 'if(this.selectedIndex > 0) submitFormById(\'downloadform\');', '', true);
	echo '<noscript id="noscriptmenuactiondownload" style="display: inline;">';
	echo '<div>';
	echo '<input type="submit" value="'.get_string('go').'" /></div></noscript>';
	echo '<script type="text/javascript">'."\n<!--\n".'document.getElementById("noscriptmenuactiondownload").style.display = "none";'."\n-->\n".'</script>';
	echo "</form>";
}
print_box_end();
print_footer($course);

?>
