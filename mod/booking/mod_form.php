<?php
require_once ($CFG->dirroot.'/course/moodleform_mod.php');

class mod_booking_mod_form extends moodleform_mod {

	function definition() {
		global $CFG, $BOOKING_DISPLAY;

		$mform    =& $this->_form;
		$outarray = array (0 => "e", 1 => "d", 2 => "u", 3 => "l", 4 => "a", 5 => "b", 6 => "s");
		$outstring = implode("", $outarray);

		$mform->addElement('header', 'general', get_string('general', 'form'));

		$mform->addElement('text', 'name', get_string('bookingname', 'booking'), array('size'=>'64'));
		if (!empty($CFG->formatstringstriptags)) {
			$mform->setType('name', PARAM_TEXT);
		} else {
			$mform->setType('name', PARAM_CLEAN);
		}
		$mform->addRule('name', null, 'required', null, 'client');

		$mform->addElement('htmleditor', 'text', get_string('bookingtext', 'booking'));
		$mform->setType('text', PARAM_RAW);
		$mform->addRule('text', null, 'required', null, 'client');
		$mform->setHelpButton('text', array('writing', 'questions', 'richtext'), false, 'editorhelpbutton');

		$mform->addElement('format', 'format', get_string('format'));

		//-------------------------------------------------------------------------------
		$menuoptions=array();
		$menuoptions[0] = get_string('disable');
		$menuoptions[1] = get_string('enable');
		
		//default options for booking options
		$mform->addElement('header', '', get_string('defaultbookingoption','booking'));
		
		$mform->addElement('select', 'limitanswers', get_string('limitanswers', 'booking'), $menuoptions);
		
		$mform->addElement('text', 'maxanswers', get_string('maxparticipantsnumber','booking'),0);
		$mform->disabledIf('maxanswers', 'limitanswers', 0);
		$mform->setType('maxanswers', PARAM_INT);
		
		$mform->addElement('text', 'maxoverbooking', get_string('maxoverbooking','booking'),0);
		$mform->disabledIf('maxoverbooking', 'limitanswers', 0);
		$mform->setType('maxoverbooking', PARAM_INT);
		
		//-------------------------------------------------------------------------------
		$mform->addElement('header', 'timerestricthdr', get_string('timerestrict', 'booking'));
		$mform->addElement('checkbox', 'timerestrict', get_string('timerestrict', 'booking'));
		$mform->setHelpButton('timerestrict', array("timerestrict", get_string("timerestrict","booking"), "booking"));


		$mform->addElement('date_time_selector', 'timeopen', get_string("bookingopen", "booking"));
		$mform->disabledIf('timeopen', 'timerestrict');

		$mform->addElement('date_time_selector', 'timeclose', get_string("bookingclose", "booking"));
		$mform->disabledIf('timeclose', 'timerestrict');
		
		//-------------------------------------------------------------------------------
		// CONFIRMATION MESSAGE
        $mform->addElement('header', 'confirmation', get_string('confirmationmessagesettings', 'booking'));
        $mform->setHelpButton('confirmation', array('confirmationmessage', get_string('confirmationmessagesettings', 'booking'), 'booking'));
		
        $mform->addElement('selectyesno', 'sendmail', get_string("sendconfirmmail", "booking"));
		
        $mform->addElement('selectyesno', 'copymail', get_string("sendconfirmmailtobookingmanger", "booking"));
        
        
		$mform->addElement('text', 'bookingmanager', get_string('usernameofbookingmanager', 'booking'));
        $mform->setType('bookingmanager', PARAM_TEXT);
		$mform->setDefault('bookingmanager', 'admin');
		$mform->disabledIf('bookingmanager', 'copymail', 0);
		

		//-------------------------------------------------------------------------------
		$mform->addElement('header', 'miscellaneoussettingshdr', get_string('miscellaneoussettings', 'form'));

		$mform->addElement('selectyesno', 'allowupdate', get_string("allowdelete", "booking"));

		$mform->addElement('htmleditor', 'agb', get_string('agb', 'booking'));
		$mform->setType('agb', PARAM_RAW);
		
		//-------------------------------------------------------------------------------
		$features = new stdClass;
		$features->groups = true;
		$features->groupings = true;
		$features->groupmembersonly = true;
		$features->gradecat = false;
		$this->standard_coursemodule_elements($features);
		//-------------------------------------------------------------------------------
		$this->add_action_buttons();
	}

	function data_preprocessing(&$default_values){
		if (empty($default_values['timeopen'])) {
			$default_values['timerestrict'] = 0;
		} else {
			$default_values['timerestrict'] = 1;
		}

	}

}
?>
