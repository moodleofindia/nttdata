<?PHP // $Id: booking.php,v 1.6.4.5 2011-02-01 23:05:09 dasistwas Exp $


$string['addmorebookings'] = 'Add more bookings';
$string['allowupdate'] = 'Allow booking to be updated';
$string['answered'] = 'Answered';
$string['booking'] = 'Booking';
$string['location'] = 'Location';
$string['venue'] = 'Venue';
$string['room'] = 'Room';
$string['booking:choose'] = 'Boook';
$string['booking:deleteresponses'] = 'Delete responses';
$string['booking:downloadresponses'] = 'Download responses';
$string['booking:readresponses'] = 'Read responses';
$string['booking:updatebooking'] = 'Manage booking options';
$string['bookingclose'] = 'Until';
$string['bookingfull'] = 'There are no available places';
$string['bookingname'] = 'Booking name';
$string['bookingopen'] = 'Open';
$string['bookingtext'] = 'Booking text';
$string['expired'] = 'Sorry, this activity closed on $a and is no longer available';
$string['fillinatleastoneoption'] = 'You need to provide at least two possible answers.';
$string['full'] = 'Full';
$string['havetologin'] = 'You have to log in before you can submit your booking';
$string['limit'] = 'Limit';
$string['modulename'] = 'Booking';
$string['modulenameplural'] = 'Bookings';
$string['mustchooseone'] = 'You must choose an option before saving.  Nothing was saved.';
$string['noguestchoose'] = 'Sorry, guests are not allowed to enter data';
$string['noresultsviewable'] = 'The results are not currently viewable.';
$string['notopenyet'] = 'Sorry, this activity is not available until $a';
$string['removeresponses'] = 'Remove all responses';
$string['responses'] = 'Responses';
$string['responsesto'] = 'Responses to $a';
$string['spaceleft'] = 'space available';
$string['spacesleft'] = 'spaces available';
$string['taken'] = 'Taken';
$string['timerestrict'] = 'Restrict exam booking to this time period';
$string['viewallresponses'] = 'Manage $a responses';
$string['yourselection'] = 'Your selection';

// view.php
$string['coursedate'] = 'Date';
$string['select'] = 'Selection';
$string['availability'] = 'Still available';
$string['booknow'] = 'Book now';
$string['notbooked'] = 'Not yet booked';
$string['available'] = 'Places available';
$string['placesavailable'] = 'Places available';
$string['waitingplacesavailable'] = 'Waitinglist places available';
$string['confirmbookingoffollowing'] = 'Please confirm your booking';
$string['agreetobookingpolicy'] = 'I have read and agree to the following booking policies';
$string['bookingsaved'] = 'Your booking was successfully saved. Your booking status: $a<br /> Please contact training helpdesk for more information.';
$string['booked'] = 'Booked';
$string['cancelbooking'] = 'Cancel booking';
$string['deletebooking'] = 'Do you really want to cancel your booking ? <br /><br /> <b>$a</b>';
$string['bookingdeleted'] = 'Your booking was cancelled';
$string['nobookingselected'] = 'No booking option selected';
$string['updatebooking'] = 'Edit this booking option';
$string['managebooking'] = 'Manage';
$string['downloadusersforthisoptionods'] = 'Download users as .ods';
$string['downloadusersforthisoptionxls'] = 'Download users as .xls';
$string['download'] = 'Download';
$string['userdownload'] = 'Download users';
$string['allbookingoptions'] = 'Download users for all booking options';
$string['subscribetocourse'] = 'Subscribe users to the course';
$string['closed'] = 'Booking closed';
$string['waitspaceavailable'] = 'Places on waitinglist available';
$string['onwaitinglist'] = 'You are on the waitinglist';
$string['bookingmeanwhilefull'] = 'Meanwhile someone took already the last place';
$string['unlimited'] = 'Unlimited';
$string['starttimenotset'] = 'Start date not set';
$string['endtimenotset'] = 'End date not set';
$string['mustfilloutuserinfobeforebooking'] = 'Befor proceeding to the booking form, please fill in some personal booking information';
$string['subscribeuser'] = 'Do you really want to subscribe the users to the course?';
$string['deleteuserfrombooking'] = 'Do you really want to delete the users from the booking?';
$string['showallbookings'] = 'Show booking overview for all bookings';
$string['showmybookings'] = 'Show only my bookings';
$string['mailconfirmationsent'] = 'You will shortly receive a confirmation e-mail';
$string['deletebookingoption'] = 'Delete this booking option';
$string['confirmdeletebookingoption'] = 'Do you really want to delete this booking option?';


// mod_form
$string['limitanswers'] = 'Limit the number of participants';
$string['maxparticipantsnumber'] = 'Max. number of participants';
$string['maxoverbooking'] = 'Max. number of places on waitinglist';
$string['defaultbookingoption'] = 'Default exam booking options';
$string['sendconfirmmail'] = 'Send confirmation email';
$string['sendconfirmmailtobookingmanger'] = 'Send confirmation email to booking manager';
$string['allowdelete'] = 'Allow users to cancel their booking themselves';
$string['agb'] = 'Booking policy';
$string['confirmationmessagesettings'] = 'Confirmation email settings';
$string['usernameofbookingmanager'] = 'Portal id of test coordinator';


// editoptions.php
$string['submitandaddnew'] = 'Save and add new';
$string['choosecourse'] = 'Choose an test module';
$string['startendtimeknown'] = 'Start and end time of test are known';
$string['coursestarttime'] = 'Start time of the test';
$string['courseendtime'] = 'End time of the test';
$string['addeditbooking'] = 'Edit booking';
$string['donotselectcourse'] = 'No test selected';
$string['waitinglisttaken'] = 'On the waitinglist';
$string['addnewbookingoption'] = 'Add a new exam booking option';


// Confirmation mail
$string['deletedbookingsubject'] = 'Booking cancelled: $a->bookingname ';
$string['deletedbookingmessage'] = 'The following booking is cancelled: $a->bookingname
Your booking request for $a->bookingname has been cancelled.

Participant:  $a->name
Date: $a->date
Location & venue: $a->location - $a->venue - $a->room

Regards,
Training Helpdesk

';

$string['deletedbookingmessagemanager'] = 'Your team member exam booking is cancelled: $a->bookingname
$a->name booking request for $a->bookingname has been cancelled.

Participant:  $a->name
Date: $a->date
Location & venue: $a->location - $a->venue - $a->room

Regards,
Training Helpdesk

';
$string['confirmationsubject'] = 'Booking confirmation for $a->bookingname';

$string['confirmationupdatesubject'] = 'Booking updated for $a->bookingname';
$string['confirmationupdatemessage'] = 'Your booking is updated, Please find the update for your booking below.

Booking status: $a->status
Participant:   $a->name
Date: $a->date
Location & venue: $a->location - $a->venue - $a->room


Regards,
Training Helpdesk

';


$string['confirmationremindersubject'] = 'Reminder -  Booking confirmation for $a->bookingname';
$string['confirmationremindermessage'] = 'Reminder for your booking $a->bookingname .

Booking status: $a->status
Participant:   $a->name
Date: $a->date
Location & venue: $a->location - $a->venue - $a->room


Regards,
Training Helpdesk

';
$string['confirmationsubjectbookingmanager'] = 'New booking for $a->bookingname by $a->name';
$string['confirmationmessage'] = 'Your booking has been registered

Your booking request for $a->bookingname has been confirmed. Your manager $a->manager is notified.

Booking status: $a->status
Participant:   $a->name
Date: $a->date
Location & venue: $a->location - $a->venue - $a->room
Exam policy
$a->agb

To view your booking click on the following link: $a->link

Regards,
Training Helpdesk

';
$string['confirmationmessagemanager'] = '$a->name booking request for $a->bookingname has been confirmed. User manager $a->manager is notified.

Booking status: $a->status
Participant:   $a->name
Date: $a->date
Location & venue: $a->location - $a->venue - $a->room
Exam policy
$a->agb

To view your booking click on the following link: $a->link

Regards,
Training Helpdesk

';
$string['confirmationsubjectwaitinglist'] = 'Booking status for $a->bookingname';
$string['confirmationsubjectwaitinglistmanager'] = 'Booking status for $a->bookingname';
$string['confirmationmessagewaitinglist'] = 'Dear $a->name,

Your booking request for $a->bookingname has been waitlisted.

Booking status: $a->status
Participant:   $a->name
Date: $a->date
Location & venue: $a->location - $a->venue - $a->room

To view your booking click on the following link: $a->link

Regards,
Training Helpdesk
';
$string['statuschangebookedsubject'] = 'Booking status changed for $a->bookingname';
$string['statuschangebookedmessage'] = 'Dear $a->name,
Your booking status has changed. Your manager $a->manager is notified. You are now registered in $a->bookingname.

Booking status: $a->status
Participant:   $a->name
Date: $a->date
Location & venue: $a->location - $a->venue - $a->room
To view your booking please click on the following link: $a->link

Regards,
Training Helpdesk

';
$string['deletedbookingusersubject'] = 'Booking for $a->bookingname cancelled';
$string['deletedbookingusermessage'] = 'Dear $a->name,
Your booking for $a->bookingname was cancelled. Your manager $a->manager is notified.

Participant:   $a->name
Date: $a->date
Location & venue: $a->location - $a->venue - $a->room

Regards,
Training Helpdesk
';


$string['deletedbookingmanagersubject'] = '$a->name has cancelled booking for $a->bookingname';
$string['deletedbookingmanagermessage'] = '$a->name has cancelled booking for $a->bookingname. User manager $a->manager is notified.

Participant:   $a->name
Date: $a->date
Location & venue: $a->location - $a->venue - $a->room

Regards,
Training Helpdesk
';




$string['error:failedtosendconfirmation'] = 'The following user did not receive a confirmation mail

Booking status: $a->status
Participant:   $a->name
Course:   $a->bookingname
Date: $a->date
Link: $a->link

';


?>
