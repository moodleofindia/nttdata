<?php  // $Id: view.php,v 1.102.2.11 2011/02/04 17:12:18 dasistwas Exp $

require_once("../../config.php");
require_once("lib.php");

$id         = required_param('id', PARAM_INT);                 // Course Module ID
$action     = optional_param('action', '', PARAM_ALPHA);
$attemptids = optional_param('attemptid', array(), PARAM_INT); // array of attempt ids for delete action
$optionid = optional_param('optionid', PARAM_INT); // array of attempt ids for delete action
$confirm = optional_param('confirm', PARAM_INT); // array of attempt ids for delete action
$answer = optional_param('answer', PARAM_INT);

if (! $cm = get_coursemodule_from_id('booking', $id)) {
	error("Course Module ID was incorrect");
}

if (! $course = get_record("course", "id", $cm->course)) {
	error("Course is misconfigured");
}

require_course_login($course, false, $cm);

/// Check to see if groups are being used in this booking
$groupmode = groups_get_activity_groupmode($cm);


if (!$booking = booking_get_booking($cm, $groupmode)) {
	error("Course module is incorrect");
}
$strbooking = get_string('modulename', 'booking');
$strbookings = get_string('modulenameplural', 'booking');

if (!$context = get_context_instance(CONTEXT_MODULE, $cm->id)) {
	print_error('badcontext');
}
// check if booking options have already been set or if they are still empty
$records = get_records('booking_options','bookingid', $booking->id);
if (empty($records)) {      // Brand new database!
	if (has_capability('mod/booking:updatebooking', $context)) {
		redirect($CFG->wwwroot.'/mod/booking/editoptions.php?id='.$cm->id.'&optionid=add');  // Redirect to field entry
	} else {
		error("There are no booking options available yet");
	}
}


// check if data has been submitted to be processed
if ($action == 'delbooking' and confirm_sesskey() && $confirm == 1 and has_capability('mod/booking:choose', $context) and ($booking->allowupdate or has_capability('mod/booking:deleteresponses', $context))) {
			$navigation = build_navigation('', $cm);
print_header_simple(format_string($booking->name), "", $navigation, "", "", true);
	if ($answer = get_record('booking_answers', 'bookingid', $booking->id, 'userid', $USER->id, 'optionid', $optionid)) {
		$newbookeduser = booking_check_statuschange($optionid, $booking->id, $USER->id, $cm->id);
		if(booking_delete_singlebooking($answer->id,$booking,$optionid,$newbookeduser,$cm->id)){
			notice(get_string('bookingdeleted','booking'), 'view.php?id='.$cm->id);
		}
	} 
} elseif ($action == 'delbooking'  and confirm_sesskey() and has_capability('mod/booking:choose', $context) and ($booking->allowupdate or has_capability('mod/booking:deleteresponses', $context))){    //print confirm delete form
		$navigation = build_navigation('', $cm);
print_header_simple(format_string($booking->name), "", $navigation, "", "", true);
		$options = array('action' => 'delbooking', 'confirm' => 1, 'optionid' => $optionid, 'sesskey' => $USER->sesskey);
		$deletemessage = $booking->option[$optionid]->text."<br />".$booking->option[$optionid]->coursestarttime." - ".$booking->option[$optionid]->courseendtime;
		notice_yesno(get_string('deletebooking','booking',$deletemessage),'view.php?id='.$cm->id,'view.php?id='.$cm->id,$options);
		print_footer();
		die;
}

// before processing data user has to agree to booking policy and confirm booking
if (has_capability('mod/booking:choose', $context) && $confirm != 1 && $answer && $action == 'book') {
	booking_confirm_booking($answer, $booking, $USER, $cm);
	die;
}

$navigation = build_navigation('', $cm);
print_header_simple(format_string($booking->name), "", $navigation, "", "", true,
update_module_button($cm->id, $course->id, $strbooking), navmenu($course, $cm));

// check if custom user profile fields are required and redirect to complete them if necessary
if (!has_capability('moodle/legacy:guest', $context, NULL, false) and booking_check_user_profile_fields($USER->id) and !has_capability('moodle/site:doanything', $context)){
	notice(get_string('mustfilloutuserinfobeforebooking','booking'),"edituserprofile.php?cmid=$cm->id&course=$course->id");
}

/// Submit any new data if there is any
if ($form = data_submitted() && has_capability('mod/booking:choose', $context)) {
	$timenow = time();
	if (!empty($answer)) {
		if(booking_user_submit_response($answer, $booking, $USER, $course->id, $cm)){
			$messageoption = booking_get_user_status($USER->id, $answer, $booking->id, $cm->id);
			$message = get_string('bookingsaved','booking', $messageoption).".";
			if ($booking->sendmail){
				$message .= "<br />".get_string('mailconfirmationsent', 'booking').".";
			}
			notice($message,'view.php?id='.$cm->id);	
		} elseif (is_int($answer)) {
			$errormessage = get_string('bookingmeanwhilefull','booking')." ".$booking->option[$answer]->text;
			notice($errormessage,'view.php?id='.$cm->id);
		}
	} else {
		notice(get_string('nobookingselected','booking'),'view.php?id='.$cm->id);
	}
}
// we have to refresh $booking as it is modified by submitted data;
$booking = booking_get_booking($cm,$groupmode);

/// Display the booking and possibly results
add_to_log($course->id, "booking", "view", "view.php?id=$cm->id", $booking->id, $cm->id);

if ($groupmode) {
	groups_get_activity_group($cm, true);
	groups_print_activity_menu($cm, $CFG->wwwroot . '/mod/booking/view.php?id='.$id);
}
$bookinglist = booking_get_spreadsheet_data($booking, $cm, $groupmode);
if (has_capability('mod/booking:readresponses', $context)) {
	booking_show_reportlink($bookinglist, $cm);
}

echo '<div class="clearer"></div>';

if ($booking->text) {
	print_box(format_text($booking->text, $booking->format), 'generalbox', 'intro');
}

$current = false;  // Initialise for later
//if user has already made a selection, show their selected answer.

/// Print the form
$bookingopen = true;
$timenow = time();
if ($booking->timeclose !=0) {
	if ($booking->timeopen > $timenow ) {
		print_simple_box(get_string("notopenyet", "booking", userdate($booking->timeopen)), "center");
		print_footer($course);
		exit;
	} else if ($timenow > $booking->timeclose) {
		print_simple_box(get_string("expired", "booking", userdate($booking->timeclose)), "center");
		$bookingopen = false;
	}
}

if ( !$current and $bookingopen and has_capability('mod/booking:choose', $context) ) {
	
	if ($action=='mybooking'){
		$message = "<a href=\"view.php?id=$cm->id\">".get_string('showallbookings','booking')."</a>";
		print_box($message,'box mdl-align');

		booking_show_form($booking, $USER, $cm, $bookinglist,1);
	} else {


		booking_show_form($booking, $USER, $cm, $bookinglist);
	}

	$bookingformshown = true;
} else {
	$bookingformshown = false;
}

if (!$bookingformshown) {

	$sitecontext = get_context_instance(CONTEXT_SYSTEM);

	if (has_capability('moodle/legacy:guest', $sitecontext, NULL, false)) {      // Guest on whole site
		$wwwroot = $CFG->wwwroot.'/login/index.php';
		if (!empty($CFG->loginhttps)) {
			$wwwroot = str_replace('http:','https:', $wwwroot);
		}
		notice_yesno(get_string('noguestchoose', 'booking').'<br /><br />'.get_string('liketologin'),
		$wwwroot, $_SERVER['HTTP_REFERER']);

	} else if (has_capability('moodle/legacy:guest', $context, NULL, false)) {   // Guest in this course only
		$SESSION->wantsurl = $FULLME;
		$SESSION->enrolcancel = $_SERVER['HTTP_REFERER'];

		print_box_start('generalbox', 'notice');
		echo '<p align="center">'. get_string('noguestchoose', 'booking') .'</p>';
		echo '<div class="continuebutton">';
		print_single_button($CFG->wwwroot.'/course/enrol.php?id='.$course->id, NULL,
		get_string('enrolme', '', format_string($course->shortname)), 'post', $CFG->framename);
		echo '</div>'."\n";
		print_box_end();

	}
}

print_footer($course);


?>
