<?php // $Id: lib.php,v 1.59.2.29 2011/02/01 11:09:31 dasistwas Exp $

require_once($CFG->dirroot.'/mod/booking/icallib.php');
$COLUMN_HEIGHT = 300;


/// Standard functions /////////////////////////////////////////////////////////

function booking_user_outline($course, $user, $mod, $booking) {
	if ($answer = get_record('booking_answers', 'bookingid', $booking->id, 'userid', $user->id)) {
		$result->info = "'".format_string(booking_get_option_text($booking, $answer->optionid))."'";
		$result->time = $answer->timemodified;
		return $result;
	}
	return NULL;
}


function booking_user_complete($course, $user, $mod, $booking) {
	if ($answer = get_record('booking_answers', "bookingid", $booking->id, "userid", $user->id)) {
		$result->info = "'".format_string(booking_get_option_text($booking, $answer->optionid))."'";
		$result->time = $answer->timemodified;
		echo get_string("answered", "booking").": $result->info. ".get_string("updated", '', userdate($result->time));
	} else {
		print_string("notanswered", "booking");
	}
}


function booking_add_instance($booking) {
	// Given an object containing all the necessary data,
	// (defined by the form in mod.html) this function
	// will create a new instance and return the id number
	// of the new instance.

	$booking->timemodified = time();

	if (empty($booking->timerestrict)) {
		$booking->timeopen = 0;
		$booking->timeclose = 0;
	}

	//insert answer options from mod_form
	return insert_record("booking", $booking);
}


function booking_update_instance($booking) {
	// Given an object containing all the necessary data,
	// (defined by the form in mod.html) this function
	// will update an existing instance with new data.

	// we have to prepare the bookingclosingtimes as an $arrray, currently they are in $booking as $key (string)
	$bookingarray = get_object_vars($booking);

	$booking->id = $booking->instance;
	$booking->timemodified = time();


	if (empty($booking->timerestrict)) {
		$booking->timeopen = 0;
		$booking->timeclose = 0;
	}
	//update, delete or insert answers

	return update_record('booking', $booking);

}

function booking_update_options($optionvalues){
	$option = new object();
	$option->bookingid = $optionvalues->bookingid;
	$option->text = trim($optionvalues->text);
	if (isset($optionvalues->limitanswers)){
		$option->maxanswers = $optionvalues->maxanswers;
		$option->maxoverbooking = $optionvalues->maxoverbooking;
	}
	if(isset($optionvalues->restrictanswerperiod)){
		$option->bookingclosingtime = $optionvalues->bookingclosingtime;
	} else {
		$option->bookingclosingtime = 0;
	}
	$option->location = $optionvalues->location;
	$option->venue = $optionvalues->venue;
	$option->room = $optionvalues->room;
	$option->courseid = $optionvalues->courseid;
	if (isset($optionvalues->startendtimeknown)){
		$option->coursestarttime = $optionvalues->coursestarttime;
		$option->courseendtime = $optionvalues->courseendtime;
	} else {
		$option->coursestarttime = 0;
		$option->courseendtime = 0;
	}

	$option->description = $optionvalues->description;
	$option->limitanswers = $optionvalues->limitanswers;
	$option->timemodified = time();

	if (isset($optionvalues->optionid) && !empty($optionvalues->optionid) && $optionvalues->id != "add"){//existing booking record
		$option->id=$optionvalues->optionid;
		if (isset($optionvalues->text) && $optionvalues->text <> '') {
				$students = get_records_sql("SELECT distinct u.id
                                 FROM mdl_user u,
                                 mdl_booking_answers a
                                 WHERE a.optionid = $option->id and
                                       u.id = a.userid");
			
			if (update_record("booking_options", $option))
			{
				foreach($students as $u)
				{				
				$user = get_record("user", "id", $u->id);
				booking_send_update_email($user, $option->id);
				}
				return true;
			}
			else{
				return false;
			}
		}
	} elseif (isset($optionvalues->text) && $optionvalues->text <> '') {
		return insert_record("booking_options", $option);
	}
}
/**
 * Checks the status of the specified user
 * @param $userid userid of the user
 * @param $optionid booking option to check
 * @param $bookingid booking id
 * @param $cmid course module id
 * @return localised string of user status
 */
function booking_get_user_status($userid,$optionid,$bookingid,$cmid){
	$option = get_record('booking_options', 'id', $optionid);
	$current = get_record('booking_answers', 'bookingid', $bookingid, 'userid', $userid, 'optionid', $optionid);
	$allresponses = get_records_select('booking_answers', "bookingid = $bookingid AND optionid = $optionid", 'timemodified', 'userid');
	$context  = get_context_instance(CONTEXT_MODULE,$cmid);
	$i=1;
	foreach($allresponses as $answer){
		if (has_capability('mod/booking:choose', $context, $answer->userid)){
			$sortedresponses[$i++] = $answer->userid;
		}
	}
	$useridaskey = array_flip($sortedresponses);
	if($useridaskey[$userid] > $option->maxanswers + $option->maxoverbooking){
		$status = "Problem, please contact the admin";
	} elseif (($useridaskey[$userid]) > $option->maxanswers) { // waitspaceavailable
		$status = get_string('onwaitinglist','booking');
	} elseif ($useridaskey[$userid] <= $option->maxanswers) {
		$status = get_string('booked','booking');
	} else {
		$status = get_string('notbooked','booking');
	}
	return $status;
}

function booking_show_form($booking, $user, $cm, $allresponses,$singleuser=0) {

	//$optiondisplay is an array of the display info for a booking $cdisplay[$optionid]->text  - text name of option.
	//                                                                            ->maxanswers -maxanswers for this option
	//                                                                            ->full - whether this option is full or not. 0=not full, 1=full
	//									      ->maxoverbooking - waitinglist places dor option
	//									      ->waitingfull - whether waitinglist is full or not 0=not, 1=full
	$bookingfull = false;
	$cdisplay = new object();

	if ($booking->limitanswers) { //set bookingfull to true by default if limitanswers.
		$bookingfull = true;
		$waitingfull = true;
	}

	$context = get_context_instance(CONTEXT_MODULE, $cm->id);
	$table = NULL;

	$displayoptions->para = false;
	$tabledata = array();
	$current = array();
	$hiddenfields = array('id' => $cm->id, 'sesskey' => sesskey());
	foreach ($booking->option as $option) {
		$optiondisplay->delete = "";
		$optiondisplay->button = "";
		// determine the ranking in order of booking time. necessary to decide whether user is on waitinglist or in regular booking
		if(@$allresponses[$option->id]){
			foreach($allresponses[$option->id] as $rank => $userobject){
				if ($user->id == $userobject->id){
					$current[$option->id] = $rank; //ranking of the user in order of subscription time
				}
			}
		}

		if (!empty($current[$option->id])) {
			if (!$option->limitanswers){
				$optiondisplay->booked = get_string('booked','booking');
				$table->rowclass[] = "mod-booking-booked";
				if(($booking->allowupdate and $option->status != 'closed') or has_capability('mod/booking:deleteresponses', $context)){
					$buttonoptions = array('id' => $cm->id, 'action' => 'delbooking', 'optionid' => $option->id, 'sesskey' => $user->sesskey);
					$optiondisplay->delete = '<a href="view.php?id='.$cm->id.',&sesskey='.sesskey().'&optionid='.$option->id.'&action=delbooking"><img src="cancel.png" title="Cancel booking." alt="Cancel" height="25" width="80"></a>';
				}
			} elseif ($current[$option->id] > $option->maxanswers + $option->maxoverbooking){
				$optiondisplay->booked = '<img src="red.png" title="Error !. Contact training helpdesk." alt="Problem" height="25" width="25">';
			} elseif ($current[$option->id] > $option->maxanswers) { // waitspaceavailable
				$optiondisplay->booked = '<img src="yellow.png" title="The booking is waitlisted." alt="Waitlisted" height="25" width="25">';
				$table->rowclass[] = "mod-booking-watinglist";
				if(($booking->allowupdate and $option->status != 'closed') or has_capability('mod/booking:deleteresponses', $context)){
					$buttonoptions = array('id' => $cm->id, 'action' => 'delbooking', 'optionid' => $option->id, 'sesskey' => $user->sesskey);
					$optiondisplay->delete = '<a href="view.php?id='.$cm->id.',&sesskey='.sesskey().'&optionid='.$option->id.'&action=delbooking"><img src="cancel.png" title="Cancel booking." alt="Cancel" height="25" width="80"></a>';
				}
			} elseif ($current[$option->id] <= $option->maxanswers) {
				$optiondisplay->booked = '<img src="green.png" title="You are booked for this." alt="Booked" height="25" width="25">';
				$table->rowclass[] = "mod-booking-booked";
				if(($booking->allowupdate and $option->status != 'closed') or has_capability('mod/booking:deleteresponses', $context)){
					$buttonoptions = array('id' => $cm->id, 'action' => 'delbooking', 'optionid' => $option->id, 'sesskey' => $user->sesskey);
					$optiondisplay->delete = '<a href="view.php?id='.$cm->id.',&sesskey='.sesskey().'&optionid='.$option->id.'&action=delbooking"><img src="cancel.png" title="Cancel booking." alt="Cancel" height="25" width="80"></a>';
				}
			}
			if(!$booking->allowupdate){
				$optiondisplay->button = "";
			}
		} else {
			$optiondisplay->booked = '<img src="grey.png" title="You are not booked for this." alt="Booked" height="25" width="25">';
			if (!$singleuser){
				$table->rowclass[] = "";
			} else {
				$table->rowclass[] = "mod-booking-invisible";
			}

			$hiddenfields["answer"] = $option->id;
			$closingtime=get_record_sql("select bookingclosingtime from mdl_booking_options where id=$option->id");
			$answerecord = get_record_sql("SELECT * FROM mdl_booking_answers where bookingid=$booking->id and userid=$user->id");
			$quizatt = get_record_sql("SELECT count(*) as count FROM mdl_quiz_attempts where quiz=$option->courseid and userid=$user->id");
			$bookopt = get_record_sql("SELECT count(*) as count FROM mdl_booking_answers where bookingid=$booking->id and userid=$user->id");
			$maxatt = get_record_sql("SELECT attempts FROM mdl_quiz where id=$option->courseid");

			if($bookopt->count >=$maxatt->attempts)
			{
				$optiondisplay->button = '<img src="bookgrey.jpg" title="You have expired all attempts." alt="NoBook" height="25" width="80">';
			}
			else if ($quizatt->count >= $maxatt->attempts)
			{
				$optiondisplay->button = '<img src="bookgrey.jpg" title="You have expired all attempts." alt="NoBook" height="25" width="80">';
			}
			else
			{

				$optiondisplay->button = '<a href="view.php?id='.$cm->id.',&sesskey='.sesskey().'&answer='.$option->id.'&action=book"><img src="book.png" title="Book seat for test." alt="Book" height="25" width="80"></a>';							

			
			}

						
		print_single_button('view.php', $hiddenfields, get_string('booknow','booking'),'post','',true);
		}
		if ( $booking->option[$option->id]->limitanswers &&	($booking->option[$option->id]->status == "full")) {
			$optiondisplay->button = '';
		} elseif ($booking->option[$option->id]->status == "closed") {
			$optiondisplay->button = '';
		}
		// check if user ist logged in
	//	if (has_capability('mod/booking:choose', $context, $user->id, false)) { //don't show save button if the logged in user is the guest user.
			$bookingbutton = $optiondisplay->button;
	//	} 
		if (!$option->limitanswers){
			$stravailspaces = get_string("unlimited", 'booking');
		} else {
			$stravailspaces = get_string("placesavailable", "booking").": ".$option->availspaces." / ".$option->maxanswers."<br />".get_string("waitingplacesavailable", "booking").": ".$option->availwaitspaces." / ".$option->maxoverbooking;
		}
		
		if($option->status=='available')
		{
			$option->status='<img src="green.png" title="Seats are available." alt="Available" height="25" width="25">';
		}
		else if($option->status=='waitspaceavailable')
		{
			$option->status='<img src="yellow.png" title="Places on waitinglist available." alt="Waitlist" height="25" width="25">';
		}
		else if($option->status=='full')
		{
			$option->status='<img src="red.png" title="Booking Full." alt="Full" height="25" width="25">';
		}
		else if($option->status=='closed')
		{
			$option->status='<img src="grey.png" title="Booking closed." alt="closed" height="25" width="25">';
		}
		
		
	
		if(time() > $closingtime->bookingclosingtime and $closingtime != 0){
				
				$optiondisplay->delete = '<img src="cancelgrey.jpg" title="Not allowed to cancel." alt="Cancel" height="25" width="80">';
				
			}
		
		$tabledata[] = array (
			"<b>".format_text($option->text. ' ', FORMAT_MOODLE, $displayoptions)."</b>"."<p>".$option->description."</p>", 
		 $option->coursestarttime."<br/>".$option->courseendtime,$option->location,$option->venue,$option->room,$optiondisplay->booked." ".$option->status." ".$bookingbutton.$optiondisplay->delete);
	}

	
	$strbooking = get_string("booking", "booking");
	$strdate = get_string("coursedate", "booking");
	$strlocation = get_string("location", "booking");
	$strvenue = get_string("venue", "booking");
	$strroom = get_string("room", "booking");
	$strselect =  get_string("select", "booking");

	$table->head = array( $strbooking, $strdate,$strlocation,$strvenue,$strroom,$strselect);
	$table->align = array ("left", "left", "left", "left");
	$table->size = array ("", "", "", "", "");
	$table->data = $tabledata;
	print_table($table);

}

/**
 * Saves the booking for the user
 * @return true if booking was possible, false if meanwhile the booking got full
 */
function booking_user_submit_response($optionid, $booking, $user, $courseid, $cm) {
	$context = get_context_instance(CONTEXT_MODULE, $cm->id);
	// check if optionid exists as real option
	if(!get_field('booking_options','id','id',$optionid)){
		return false;
	}
	if($booking->option[$optionid]->limitanswers) {
		// Find out whether groups are being used and enabled
		if (groups_get_activity_groupmode($cm) > 0) {
			$currentgroup = groups_get_activity_group($cm);
		} else {
			$currentgroup = 0;
		}
		$answers = array();

		$countanswers[$optionid]=0;
		if($currentgroup) {
			// If groups are being used, retrieve responses only for users in
			// current group
			global $CFG;
			$answers[$optionid] = get_records_sql("SELECT ca.* FROM
			{$CFG->prefix}booking_answers ca
	    INNER JOIN {$CFG->prefix}groups_members gm ON ca.userid=gm.userid
	WHERE
	    optionid=$optionid
	    AND gm.groupid=$currentgroup");
		} else {
			// Groups are not used, retrieve all answers for this option ID
			$answers[$optionid] = get_records("booking_answers", "optionid", $optionid);
		}

		if ($answers[$optionid]) {
			foreach ($answers[$optionid] as $a) { //only return enrolled users.
				if (has_capability('mod/booking:choose', $context, $a->userid, false)) {
					$countanswers[$optionid]++;
				}
			}
		}
		$maxans[$optionid] = $booking->option[$optionid]->maxanswers + $booking->option[$optionid]->maxoverbooking;

	}
	// if answers for one option are limited and total answers are not exceeded then
	if (!($booking->option[$optionid]->limitanswers && ($countanswers[$optionid] >= $maxans[$optionid]) )) {
		// check if actual answer is also already made by this user
		if(!($currentanswerid = get_field('booking_answers','id', 'userid', $user->id, 'optionid', $optionid))){
			$newanswer->bookingid = $booking->id;
			$newanswer->userid = $user->id;
			$newanswer->optionid = $optionid;
			$newanswer->timemodified = time();
			if (!insert_record("booking_answers", $newanswer)) {
				error("Could not register your booking because of a database error");
			}
		}
		add_to_log($courseid, "booking", "choose", "view.php?id=$cm->id", $booking->id, $cm->id);
		if ($booking->sendmail){
			booking_send_confirmation_email($user, $booking, $optionid,$cm->id);
		}
		return true;
	} else { //check to see if current booking already selected - if not display error
		$optionname = get_field('booking_options', 'text', 'id', $optionid);
		return false;
	}
}

function booking_show_reportlink($user, $cm) {
	$responsecount =0;
	foreach($user as $optionid => $userlist) {
		if ($optionid) {
			$responsecount += count($userlist);
		}
	}

	echo '<div class="reportlink">';
	echo '<a href="editoptions.php?id='.$cm->id.'&optionid=add"><img src="add.png" title="Add a new booking." alt="Add" height="30" width="100"></a> ';
	echo '<a href="report.php?id='.$cm->id.'"><img src="manage.png" title="Manage Bookings." alt="Manage" height="30" width="100"></a>';
	echo '</div>';
}

function booking_show_results($booking, $course, $cm, $allresponses) {
	global $CFG, $COLUMN_HEIGHT, $USER;
	print_heading(get_string("responses", "booking"));

	$context = get_context_instance(CONTEXT_MODULE, $cm->id);

	$hascapfullnames = has_capability('moodle/site:viewfullnames', $context);

	$viewresponses = has_capability('mod/booking:readresponses', $context);

	$count = 0;
	foreach ($booking->option as $optionid => $option) {

		// column 0 = bookingtitle and stats, column 1 = booked users and waitinglist users, columnn 2 = action options
		$tabledata[$count][0] = '<p style="font-weight:bold;">'.format_string($option->text).'</p>';
		if ($option->limitanswers) {
			$tabledata[$count][0] .=$option->location." - ".$option->venue." - ".$option->room."<br/>".get_string("taken", "booking").": $option->taken / ".$option->maxanswers." ";
		} else {
			if (isset($allresponses[$optionid])) {
				$tabledata[$count][0] .= $option->location." - ".$option->venue." - ".$option->room."<br/>".get_string("taken", "booking")." ".$option->taken;
			}
		}
		$tabledata[$count][1] = '<div id="container'.$option->id.'">';
		$sesskey = sesskey();
		if ($viewresponses) {
			$tabledata[$count][1] .= '<form id="attemptsform'.$option->id.'" method="post" action="'.$_SERVER['PHP_SELF'].'" onsubmit="var menu = document.getElementById(\'menuaction'.$option->id.'\'); return (menu.options[menu.selectedIndex].value == \'delete\' ? \''.addslashes(get_string('deleteattemptcheck','quiz')).'\' : true);">';
			$tabledata[$count][1] .= '<div>';
			$tabledata[$count][1] .='<input type="hidden" name="id" value="'.$cm->id.'" />';
			$tabledata[$count][1] .= '<input type="hidden" name="sesskey" value="'.$sesskey.'" />';
			$tabledata[$count][1] .= '<input type="hidden" name="mode" value="overview" /></div>';
		}
		if (isset($allresponses[$optionid])) {
			$waitlistusers = array();
			$i=1;
			$tabledata[$count][1] .= '<div style="display:block">';
			foreach ($allresponses[$optionid] as $user) {
				if ($i <= $option->maxanswers || !$option->limitanswers){ //booked user
					$tabledata[$count][1] .= '<table class="mod-booking-inlinetable"><tr><td class="attemptcell">';
					if ($viewresponses and has_capability('mod/booking:deleteresponses',$context)) {
						$tabledata[$count][1] .= '<input type="checkbox" name="attemptid['.$optionid.']['.$i.']" value="'. $user->id. '" />';
					}
					$tabledata[$count][1] .= '</td><td class="picture">';
					$tabledata[$count][1] .= print_user_picture($user->id, $course->id, $user->picture,'',1);
					$tabledata[$count][1] .= '</td><td class="fullname">';
					$tabledata[$count][1] .= "<a href=\"$CFG->wwwroot/user/view.php?id=$user->id&amp;course=$course->id\">";
					$tabledata[$count][1] .= fullname($user, $hascapfullnames).'</a></td></tr></table>';
				} else if ($i <= $option->maxoverbooking + $option->maxanswers){ //waitlistusers;
					$waitlistusers[$i] = $user;
				}
				$i++;
			}
			$tabledata[$count][1] .= '</div><div class="mod-booking-waitlist">';
			if($booking->limitanswers && ($option->maxoverbooking > 0)) {
				if (!empty($waitlistusers)){
					foreach ($waitlistusers as $user) {
						$tabledata[$count][1] .= '<table class="mod-booking-inlinetable"><tr><td class="attemptcell">';
						if ($viewresponses and has_capability('mod/booking:deleteresponses',$context)) {
							$tabledata[$count][1] .= '<input type="checkbox" name="attemptid['.$optionid.']['.$i++.']" value="'. $user->id. '" />';
						}
						$tabledata[$count][1] .= '</td><td class="picture">';
						$tabledata[$count][1] .= print_user_picture($user->id, $course->id, $user->picture,'',1);
						$tabledata[$count][1] .= '</td><td class="fullname">';
						$tabledata[$count][1] .= "<a href=\"$CFG->wwwroot/user/view.php?id=$user->id&amp;course=$course->id\">";
						$tabledata[$count][1] .= fullname($user, $hascapfullnames);
						$tabledata[$count][1] .=  '</a></td></tr></table>';
					}
				}
			}
			$tabledata[$count][1] .= '</div>';
			if ($option->limitanswers){
				$tabledata[$count][0] .= "<br />".get_string("waitinglisttaken", "booking").": ".count($waitlistusers)." / $option->maxoverbooking";
			}

		}

		//display on the side of each option
		if ($viewresponses and has_capability('mod/booking:deleteresponses',$context)) {
			$tabledata[$count][1] .=  '<a href="javascript:select_all_in(\'DIV\',null,\'container'.$option->id.'\');">'.get_string('selectall', 'quiz').'</a> / ';
			$tabledata[$count][1] .=  '<a href="javascript:deselect_all_in(\'DIV\',null,\'container'.$option->id.'\');">'.get_string('selectnone', 'quiz').'</a> ';
			$tabledata[$count][1] .=  '&nbsp;&nbsp;';
			$options = array('delete' => get_string('delete'));
		
			$tabledata[$count][1] .=  choose_from_menu($options, 'action', '', get_string('withselected', 'quiz'), 'if(this.selectedIndex > 0) submitFormById(\'attemptsform'.$option->id.'\');', '', true,'','','menuaction'.$option->id);
			$tabledata[$count][1] .=  '<noscript id="noscriptmenuaction'.$option->id.'" style="display: inline;">';
			$tabledata[$count][1] .=  '<div>';
			$tabledata[$count][1] .=  '<input type="submit" value="'.get_string('go').'" /></div></noscript>';
			$tabledata[$count][1] .=  '<script type="text/javascript">'."\n<!--\n".'document.getElementById("noscriptmenuaction'.$option->id.'").style.display = "none";'."\n-->\n".'</script>';
			if ($viewresponses) {
				$tabledata[$count][1] .=  "</form></div>";
			}
		}
		$tabledata[$count][2] = "";
		if (has_capability('mod/booking:updatebooking', $context)){
			$tabledata[$count][2] .= '<a href="editoptions.php?id='.$cm->id.'&optionid='.$option->id.'"><img src="edit.png" title="Edit booking." alt="Edit" height="25" width="25"></a> ';
			$tabledata[$count][2] .= '<a href="report.php?id='.$cm->id.'&optionid='.$option->id.'&action=deletebookingoption&sesskey='.$sesskey.'"><img src="delete.png" title="Delete booking." alt="Delete" height="25" width="25"></a>';

			
		}

		$count++;
	}

	/// Print "Select all" etc.
	$bookingtitle =  get_string("booking", "booking");
	$strparticipants = get_string("participants");
	$strwaitinglist = get_string("waitinglist", "booking");
	$stroptions = get_string('managebooking', 'booking');

	$table = NULL;
	$table->head = array($bookingtitle, $strparticipants, $stroptions);
	$table->align = array ("left", "left", "left");
	$table->size = array ("", "", "");
	$table->data = $tabledata;
	print_table($table);

}


function booking_cron () 
	{
	
	$students = get_records_sql("SELECT * FROM mdl_booking_answers 
	join mdl_booking_options where coursestarttime > UNIX_TIMESTAMP(now()) 
	and coursestarttime < UNIX_TIMESTAMP(DATE_ADD(NOW(), INTERVAL 1 DAY))");
			foreach($students as $u)
			{
				
				$user = get_record("user", "id", $u->userid);
				booking_send_reminder_email($user, $u->optionid);
			}
	
	}



// this function is not yet implemented and needs to be changed a lot before using it
function booking_show_statistic (){

	echo "<table cellpadding=\"5\" cellspacing=\"0\" class=\"results anonymous\">";
	echo "<tr>";

	foreach ($booking->option as $optionid => $option) {
		echo "<th class=\"col$count header\" scope=\"col\">";
		echo format_string($option->text);
		echo "</th>";

		$column[$optionid] = 0;
		if (isset($allresponses[$optionid])) {
			$column[$optionid] = count($allresponses[$optionid]);
			if ($column[$optionid] > $maxcolumn) {
				$maxcolumn = $column[$optionid];
			}
		} else {
			$column[$optionid] = 0;
		}
	}
	echo "</tr><tr>";

	$height = 0;


	$count = 1;
	foreach ($booking->option as $optionid => $option) {
		if ($maxcolumn) {
			$height = $COLUMN_HEIGHT * ((float)$column[$optionid] / (float)$maxcolumn);
		}
		echo "<td style=\"vertical-align:bottom\" align=\"center\" class=\"col$count data\">";
		echo "<img src=\"column.png\" height=\"$height\" width=\"49\" alt=\"\" />";
		echo "</td>";
		$count++;
	}
	echo "</tr><tr>";



	$count = 1;
	foreach ($booking->option as $optionid => $option) {
		echo "<td align=\"center\" class=\"col$count count\">";
		if ($booking->limitanswers) {
			echo get_string("taken", "booking").":";
			echo $column[$optionid].'<br />';
			echo get_string("limit", "booking").":";
			$option = get_record("booking_options", "id", $optionid);
			echo $option->maxanswers;
		} else {
			echo $column[$optionid];
			echo '<br />('.format_float(((float)$column[$optionid]/(float)$totalresponsecount)*100.0,1).'%)';
		}
		echo "</td>";
		$count++;
	}
	echo "</tr></table>";
}

function booking_confirm_booking($optionid, $booking, $user, $cm){
	$navigation = build_navigation('', $cm);
print_header_simple(format_string($booking->name), "", $navigation, "", "", true);

	$optionidarray['answer'] = $optionid;
	$optionidarray['confirm'] = 1;
	$optionidarray['sesskey'] = $user->sesskey;
	$optionidarray['id'] = $cm->id;
	$requestedcourse = "<br /><b>".$booking->option[$optionid]->text."</b>";
	if($booking->option[$optionid]->coursestarttime != get_string('starttimenotset','booking')){
		$requestedcourse .= "<br />".$booking->option[$optionid]->coursestarttime." - ".$booking->option[$optionid]->courseendtime;
		$requestedcourse .= "<br /><i>".$booking->option[$optionid]->location." - ".$booking->option[$optionid]->venue." - ".$booking->option[$optionid]->room.'</i>';
	}
	$message = "<h2>".get_string('confirmbookingoffollowing','booking')."</h2>".$requestedcourse;
	$message .= "<p><b>".get_string('agreetobookingpolicy','booking').":</b></p>";
	$message .= "<p>".$booking->agb."<p>";
	notice_yesno($message,'view.php','view.php?id='.$cm->id,$optionidarray);
	print_footer();
}
/**
 * deletes a single booking of a user if user cancels the booking, sends mail to supportuser and newbookeduser
 * @return true if booking was deleted successfully, otherwise false
 */
function booking_delete_singlebooking($answerid,$booking,$optionid,$newbookeduserid,$cmid) {
	global $COURSE, $USER, $CFG;
	if(!delete_records('booking_answers', 'id', $answerid)){
		return false;
	}
	
	$bookingmanager = get_record('user', 'username', $booking->bookingmanager);
	if(!$bookingmanager)
	{		
		$bookingmanager = get_record('user', 'username', 'manager');
		$bookingmanager->email=$booking->bookingmanager;
	}
	$cuser = new object();
	$cuser->firstname = "Certification";
	$cuser->lastname = "Helpdesk";
	$cuser->email = "Certification.Helpdesk@nttdata.com";
	$usermanager = get_record('user', 'username', $USER->manager_portalid);
	
	
	$supportuser = generate_book_email_supportuser();
	$supportuserparams = new object;
	$supportuserparams->bookingname = $booking->option[$optionid]->text;
	$supportuserparams->manager = fullname($usermanager);
	$supportuserparams->name = fullname($USER);
	
	$supportuserparams->date = $booking->option[$optionid]->coursestarttime. " - ".$booking->option[$optionid]->courseendtime;
	$supportuserparams->location=$booking->option[$optionid]->location;
	$supportuserparams->venue=$booking->option[$optionid]->venue;
	$supportuserparams->room=$booking->option[$optionid]->room;
	
	$supportuserparams->link = $CFG->wwwroot .'/user/view.php?id='.$USER->id.'&course='.$COURSE->id;
	$messagetext = get_string('deletedbookingmessage','booking',$supportuserparams);
	$deletedbookingmessage = get_string('deletedbookingusermessage','booking', $supportuserparams);
	$deletedbookingusermessage = get_string('deletedbookingusermessage', 'booking', $supportuserparams);
	
	$subjectmanager = get_string('deletedbookingmanagersubject','booking', $supportuserparams);			
	$messagemanager = get_string('deletedbookingmanagermessage','booking', $supportuserparams);
	$messagemanagerhtml = text_to_html($messagemanager, false, false, true);
	$messagehtml = text_to_html($deletedbookingusermessage, false, false, true);
	
	$to = get_record('user','username', $booking->bookingmanager);
	// Generate ical attachment to go with the message.
		$attachname = '';
		
		$ical = new booking_ical($booking, $booking->option[$optionid], $USER, $supportuser);
		if ($attachment = $ical->get_attachment(true)) {
			$attachname = $ical->get_name(true);
		}
	if (email_to_user($USER, $supportuser, get_string('deletedbookingusersubject','booking', $supportuserparams), $deletedbookingusermessage, $messagehtml, $attachment, $attachname)) {
		if ($booking->copymail){
			email_to_user($usermanager, $supportuser, $subjectmanager, $messagemanager, $messagemanagerhtml);
			email_to_user($cuser, $supportuser, $subjectmanager, $messagemanager, $messagemanagerhtml);
			return email_to_user($bookingmanager, $supportuser, $subjectmanager, $message, $messagemanagerhtml);
		} else {
			return true;
		}
	} else {
		return false;
	}
	
	
	
	
	if ($booking->limitanswers == 1 && $booking->sendmail == 1 && $newbookeduserid){
		$newbookeduser = get_record('user', 'id', $newbookeduserid);
		$messageparams = new object;
		$messageparams->status = booking_get_user_status($newbookeduserid, $optionid, $booking->id, $cmid);
		$messageparams->date = $booking->option[$optionid]->coursestarttime. " - ".$booking->option[$optionid]->courseendtime;
		$messageparams->name = fullname($newbookeduser);
		$messageparams->link = $CFG->wwwroot .'/mod/booking/view.php?id='.$cmid;
		$messageparams->bookingname = $booking->option[$optionid]->text;
		$messageparams->canceluntil = $booking->option[$optionid]->bookingclosingtime;
		$messagetextnewuser = get_string('statuschangebookedmessage','booking',$messageparams);
		email_to_user($newbookeduser, $supportuser, get_string('statuschangebookedsubject','booking', $messageparams), $messagetextnewuser);
	}
	return true;
}

function booking_delete_responses($attemptidsarray, $bookingid) {
	if(!is_array($attemptidsarray) || empty($attemptidsarray)) {
		return false;
	}
	foreach($attemptidsarray as $optionid => $attemptids){
		if(!is_array($attemptids) || empty($attemptids)) {
			return false;
		}
		foreach($attemptids as $num => $attemptid) {
			if(empty($attemptid)) {
				unset($attemptids[$num]);
			}
		}
		foreach($attemptids as $attemptid) {
			if ($todelete = get_record('booking_answers', 'bookingid', $bookingid, 'userid', $attemptid, 'optionid', $optionid)) {
				delete_records('booking_answers', 'bookingid', $bookingid, 'userid', $attemptid, 'optionid', $optionid);
			}
		}
	}
	return true;
}


function booking_delete_instance($id) {
	// Given an ID of an instance of this module,
	// this function will permanently delete the instance
	// and any data that depends on it.

	if (! $booking = get_record("booking", "id", "$id")) {
		return false;
	}

	$result = true;

	if (! delete_records("booking_answers", "bookingid", "$booking->id")) {
		$result = false;
	}

	if (! delete_records("booking_options", "bookingid", "$booking->id")) {
		$result = false;
	}

	if (! delete_records("booking", "id", "$booking->id")) {
		$result = false;
	}

	return $result;
}

function booking_delete_booking_option($bookingid, $optionid) {
	// Given an ID of an instance of this module,
	// this function will permanently delete the instance
	// and any data that depends on it.

	if (! $option = get_record("booking_options", "id", "$optionid")) {
		return false;
	}

	$result = true;
			$students = get_records_sql("SELECT distinct u.id
                                 FROM mdl_user u,
                                 mdl_booking_answers a
                                 WHERE a.optionid = $option->id and
                                       u.id = a.userid");
			foreach($students as $u)
			{
				
				$user = get_record("user", "id", $u->id);
				booking_send_delete_email($user, $option->id);
			}
			
	if (! delete_records("booking_answers", "bookingid", $bookingid, "optionid", $optionid)) {
		$result = false;
	}

	if (! delete_records("booking_options", "id", $optionid)) {
		$result = false;
	}
	return $result;
}

function booking_get_participants($bookingid) {
	//Returns the users with data in one booking
	//(users with records in booking_responses, students)

	global $CFG;

	//Get students
	$students = get_records_sql("SELECT DISTINCT u.id, u.id
                                 FROM {$CFG->prefix}user u,
                                 {$CFG->prefix}booking_answers a
                                 WHERE a.bookingid = '$bookingid' and
                                       u.id = a.userid");

                                 //Return students array (it contains an array of unique users)
                                 return ($students);
}


function booking_get_option_text($booking, $id) {
	// Returns text string which is the answer that matches the id
	if ($result = get_record("booking_options", "id", $id)) {
		return $result->text;
	} else {
		return get_string("notanswered", "booking");
	}
}

function booking_get_groupmodedata() {

}
/**
 * Gets the principal information of booking status and booking options
 * to be used by other functions
 * @param $bookingid id of the module
 * @return object with $booking->option as an array for the booking option valus for each booking option
 */
function booking_get_booking($cm,$groupmode) {
	$bookingid = $cm->instance;
	// Gets a full booking record
	$context = get_context_instance(CONTEXT_MODULE, $cm->id);

	/// Get the current group
	if ($groupmode > 0) {
		$currentgroup = groups_get_activity_group($cm);
	} else {
		$currentgroup = 0;
	}

	/// Initialise the returned array, which is a matrix:  $allresponses[responseid][userid] = responseobject
	$allresponses = array();
	/// bookinglist $bookinglist[optionid][sortnumber] = userobject;
	$bookinglist = array();

	/// First get all the users who have access here
	$allresponses = get_users_by_capability($context, 'mod/booking:choose', 'u.id, u.picture, u.firstname, u.lastname, u.idnumber, u.email', 'u.lastname ASC, u.firstname ASC', '', '', $currentgroup, '', true, true);

	if (($options = get_records_sql("SELECT * FROM mdl_booking_options where
coursestarttime > UNIX_TIMESTAMP(now()) and bookingid=$bookingid order by coursestarttime")) && ($booking = get_record("booking", "id", $bookingid))) {
		$answers = get_records('booking_answers', 'bookingid', $bookingid, 'id');
		foreach ($options as $option){
			$booking->option[$option->id] = $option;
			if(!$option->coursestarttime == 0){
				$booking->option[$option->id]->coursestarttime = userdate($option->coursestarttime, get_string('strftimedatetime'));
			} else {
				$booking->option[$option->id]->coursestarttime = get_string("starttimenotset", 'booking');
			}
			if(!$option->courseendtime == 0){
				$booking->option[$option->id]->courseendtime = userdate($option->courseendtime, get_string('strftimedatetime'),'',false);
			} else {
				$booking->option[$option->id]->courseendtime = get_string("endtimenotset", 'booking');
			}
			// we have to change $taken is different from booking_show_results
			$answerstocount = array();
			if($answers){
				foreach($answers as $answer){
					if ($answer->optionid == $option->id && isset($allresponses[$answer->userid])){
						$answerstocount[] = $answer;
					}
				}
			}
			$taken = count($answerstocount);
			$totalavailable = $option->maxanswers + $option->maxoverbooking;
			if (!$option->limitanswers){
				$booking->option[$option->id]->status = "available";
				$booking->option[$option->id]->taken = $taken;
				$booking->option[$option->id]->availspaces = "unlimited";
			} else {
				if ($taken < $option->maxanswers) {
					$booking->option[$option->id]->status = "available";
					$booking->option[$option->id]->availspaces = $option->maxanswers - $taken;
					$booking->option[$option->id]->taken = $taken;
					$booking->option[$option->id]->availwaitspaces = $option->maxoverbooking;
				} elseif ($taken >= $option->maxanswers && $taken < $totalavailable ){
					$booking->option[$option->id]->status = "waitspaceavailable";
					$booking->option[$option->id]->availspaces = 0;
					$booking->option[$option->id]->taken = $option->maxanswers;
					$booking->option[$option->id]->availwaitspaces = $option->maxoverbooking - ($taken - $option->maxanswers);
				} elseif ($taken >= $totalavailable){
					$booking->option[$option->id]->status = "full";
					$booking->option[$option->id]->availspaces = 0;
					$booking->option[$option->id]->taken = $option->maxanswers;
					$booking->option[$option->id]->availwaitspaces = 0;
				}
			}
			if(time() > $booking->option[$option->id]->bookingclosingtime and $booking->option[$option->id]->bookingclosingtime != 0){
				$booking->option[$option->id]->status = "closed";
			}
			if ($option->bookingclosingtime){
				$booking->option[$option->id]->bookingclosingtime = userdate($option->bookingclosingtime, get_string('strftimedate'),'',false);
			} else {
				$booking->option[$option->id]->bookingclosingtime = false;
			}
		}
		return $booking;
	} elseif ($booking = get_record("booking", "id", $bookingid)) {
		return $booking;
	}
	return false;
}

function booking_get_view_actions() {
	return array('view','view all','report');
}

function booking_get_post_actions() {
	return array('choose','choose again');
}


/**
 * Implementation of the function for printing the form elements that control
 * whether the course reset functionality affects the booking.
 * @param $mform form passed by reference
 */
function booking_reset_course_form_definition(&$mform) {
	$mform->addElement('header', 'bookingheader', get_string('modulenameplural', 'booking'));
	$mform->addElement('advcheckbox', 'reset_booking', get_string('removeresponses','booking'));
}

/**
 * Course reset form defaults.
 */
function booking_reset_course_form_defaults($course) {
	return array('reset_booking'=>1);
}

/**
 * Actual implementation of the rest coures functionality, delete all the
 * booking responses for course $data->courseid.
 * @param $data the data submitted from the reset course.
 * @return array status array
 */
function booking_reset_userdata($data) {
	global $CFG;

	$componentstr = get_string('modulenameplural', 'booking');
	$status = array();

	if (!empty($data->reset_booking)) {
		$bookingssql = "SELECT ch.id
                         FROM {$CFG->prefix}booking ch
                        WHERE ch.course={$data->courseid}";

		delete_records_select('booking_answers', "bookingid IN ($bookingssql)");
		$status[] = array('component'=>$componentstr, 'item'=>get_string('removeresponses', 'booking'), 'error'=>false);
	}

	/// updating dates - shift may be negative too
	if ($data->timeshift) {
		shift_course_mod_dates('booking', array('timeopen', 'timeclose'), $data->timeshift, $data->courseid);
		$status[] = array('component'=>$componentstr, 'item'=>get_string('datechanged'), 'error'=>false);
	}
	return $status;
}

function booking_get_spreadsheet_data($booking, $cm, $groupmode) {
	global $CFG, $USER;
	$bookinglistsorted = array();
	$context = get_context_instance(CONTEXT_MODULE, $cm->id);

	/// Get the current group
	if ($groupmode > 0) {
		$currentgroup = groups_get_activity_group($cm);
	} else {
		$currentgroup = 0;
	}

	/// Initialise the returned array, which is a matrix:  $allresponses[responseid][userid] = responseobject
	$allresponses = array();
	/// bookinglist $bookinglist[optionid][sortnumber] = userobject;
	$bookinglist = array();

	/// First get all the users who have access here
	$allresponses = get_users_by_capability($context, 'mod/booking:choose', 'u.id, u.picture, u.firstname,u.username,u.manager_portalid, u.lastname, u.idnumber, u.email', 'u.lastname ASC, u.firstname ASC', '', '', $currentgroup, '', true, true);

	/// Get all the recorded responses for this booking
	$rawresponses = get_records('booking_answers', 'bookingid', $booking->id, "optionid, timemodified ASC");
	$optionids = get_records_select('booking_options', "bookingid = $booking->id",'id','id');
	/// Use the responses to move users into the correct column
	$sortnumber = 1;
	if ($rawresponses) {
		foreach ($rawresponses as $response) {
			if (isset($allresponses[$response->userid])) {   // This person is enrolled and in correct group
				$bookinglist[$response->optionid][$sortnumber++] = $allresponses[$response->userid];
			}
		}
	}
	if (empty($bookinglist)) {
		unset($bookinglist);
	} else {
		foreach($optionids as $optionid => $optionobject){
			if(!empty($bookinglist[$optionid])){
				$userperbookingoption = count($bookinglist[$optionid]);
				$i = 1;
				foreach($bookinglist[$optionid] as $key => $value){
					unset($bookinglist[$optionid][$key]);
					$bookinglistsorted[$optionid][$i++] = $value;
				}
			} else {
				unset($bookinglist[$optionid]);
			}
		}
	}
	return $bookinglistsorted;
}

function generate_book_email_supportuser() {

    global $CFG;

    static $supportuser;

    if (!empty($supportuser)) {
        return $supportuser;
    }

    $supportuser = new object;
    $supportuser->email = $CFG->supportemail ? $CFG->supportemail : 'Training.Helpdesk@nttdata.com';
    $supportuser->firstname = $CFG->supportname ? $CFG->supportname : 'NTT Data LMS - Exam Booking';
    $supportuser->lastname = '';
    $supportuser->maildisplay = true;

    return $supportuser;
}

function booking_send_confirmation_email($user,$booking,$optionid,$cmid){

	global $CFG;

	$site = get_site();
	$supportuser = generate_book_email_supportuser();
	$bookingmanager = get_record('user', 'username', $booking->bookingmanager);
	if(!$bookingmanager)
	{		
		$bookingmanager = get_record('user', 'username', 'manager');
		$bookingmanager->email=$booking->bookingmanager;
	}
	$cuser = new object();
	$cuser->firstname = "Certification";
	$cuser->lastname = "Helpdesk";
	$cuser->email = "Certification.Helpdesk@nttdata.com";
	$usermanager = get_record('user', 'username', $user->manager_portalid);
	$data = new object();
	$data->name = fullname($user);
	$data->manager = fullname($usermanager);
	$data->status = booking_get_user_status($user->id,$optionid,$booking->id,$cmid);
	$data->bookingname = $booking->option[$optionid]->text;
	$data->date = $booking->option[$optionid]->coursestarttime. " - ".$booking->option[$optionid]->courseendtime;
	$data->location=$booking->option[$optionid]->location;
	$data->venue=$booking->option[$optionid]->venue;
	$data->room=$booking->option[$optionid]->room;
	$data->agb=$booking->agb;
	$data->link = $CFG->wwwroot .'/mod/booking/view.php?id='.$cmid;
	$data->canceluntil = $booking->option[$optionid]->bookingclosingtime;

	
	// Generate ical attachment to go with the message.
		$ical = new booking_ical($booking, $booking->option[$optionid], $user, $supportuser);
		if ($attachment = $ical->get_attachment()) {
			$attachname = $ical->get_name();
		}
	
	
	if ($data->status == get_string('booked', 'booking')){
		$subject = get_string('confirmationsubject','booking', $data);
		$subjectmanager = get_string('confirmationsubjectbookingmanager','booking', $data);			
		$messagemanager = get_string('confirmationmessagemanager','booking', $data);
		$messagemanagerhtml = text_to_html($messagemanager, false, false, true);		
		$message     = get_string('confirmationmessage', 'booking', $data);
	} elseif ($data->status == get_string('onwaitinglist', 'booking')){
		$subject = get_string('confirmationsubjectwaitinglist','booking', $data);
	$subjectmanager = get_string('confirmationsubjectwaitinglistmanager','booking', $data);			
		$message     = get_string('confirmationmessagewaitinglist', 'booking', $data);
	}
	$messagehtml = text_to_html($message, false, false, true);
	$errormessage = get_string('error:failedtosendconfirmation','booking', $data);
	$errormessagehtml =  text_to_html($errormessage, false, false, true);
	$user->mailformat = 1;  // Always send HTML version as well
	// send mail to user, copy to bookingmanager and if mail fails send errormessage to bookingmanager
	if (email_to_user($user, $supportuser, $subject, $message, $messagehtml,$attachment, $attachname)) {
		if ($booking->copymail){
			email_to_user($usermanager, $supportuser, $subjectmanager, $messagemanager, $messagemanagerhtml);
			email_to_user($cuser, $supportuser, $subjectmanager, $messagemanager, $messagemanagerhtml);
			return email_to_user($bookingmanager, $supportuser, $subjectmanager, $message, $messagehtml);
		} else {
			return true;
		}
	} else {
		email_to_user($bookingmanager, $supportuser, $subjectmanager, $errormessage, $errormessagehtml);
		return false;
	}
}


function booking_send_update_email($user,$optionid){

	global $CFG;
	$option = get_record('booking_options', 'id', $optionid);
	$supportuser = generate_book_email_supportuser();
	$usermanager = get_record('user', 'username', $user->manager_portalid);		
$bookingmanager = get_record('user', 'username', $booking->bookingmanager);
	if(!$bookingmanager)
	{		
		$bookingmanager = get_record('user', 'username', 'manager');
		$bookingmanager->email=$booking->bookingmanager;
	}	
	$data = new object();
	$data->name = fullname($user);
	$data->manager = fullname($usermanager);
	$data->bookingname = $option->text;
	$data->date = userdate($option->coursestarttime, get_string('strftimedatetime')). " - ".userdate($option->courseendtime, get_string('strftimedatetime'));
	$data->location=$option->location;
	$data->venue=$option->venue;
	$data->room=$option->room;
	$user->mailformat = 1;  // Always send HTML version as well
	$subject = get_string('confirmationupdatesubject','booking', $data);
	$message     = get_string('confirmationupdatemessage', 'booking', $data);
	$messagehtml = text_to_html($message, false, false, true);
	$messagemanager = get_string('confirmationmessagemanager','booking', $data);
	$messagemanagerhtml = text_to_html($messagemanager, false, false, true);
	if (email_to_user($user, $supportuser, $subject, $message, $messagehtml)) {
			email_to_user($usermanager, $supportuser, $subject, $messagemanager, $messagemanagerhtml);
	return email_to_user($bookingmanager, $supportuser, $subject, $messagemanager, $messagemanagerhtml);
	} else {

		return false;
	}
}

function booking_send_delete_email($user,$optionid){

	global $CFG;
	$option = get_record('booking_options', 'id', $optionid);
	$supportuser = generate_book_email_supportuser();
	$usermanager = get_record('user', 'username', $user->manager_portalid);						
	$bookingmanager = get_record('user', 'username', $booking->bookingmanager);
	if(!$bookingmanager)
	{		
		$bookingmanager = get_record('user', 'username', 'manager');
		$bookingmanager->email=$booking->bookingmanager;
	}
	$cuser = new object();
	$cuser->firstname = "Certification";
	$cuser->lastname = "Helpdesk";
	$cuser->email = "Certification.Helpdesk@nttdata.com";
	$data = new object();
	$data->name = fullname($user);
	$data->manager = fullname($usermanager);
	$data->bookingname = $option->text;
	$data->date = userdate($option->coursestarttime, get_string('strftimedatetime')). " - ".userdate($option->courseendtime, get_string('strftimedatetime'));
	$data->location=$option->location;
	$data->venue=$option->venue;
	$data->room=$option->room;
	$user->mailformat = 1;  // Always send HTML version as well
	$subject = get_string('deletedbookingsubject','booking', $data);
	$message     = get_string('deletedbookingmessage', 'booking', $data);
	$messagehtml = text_to_html($message, false, false, true);
	$messagemanager = get_string('deletedbookingmessagemanager','booking', $data);
	$messagemanagerhtml = text_to_html($messagemanager, false, false, true);
	if (email_to_user($user, $supportuser, $subject, $message, $messagehtml)) {
email_to_user($usermanager, $supportuser, $subject, $messagemanager, $messagemanagerhtml);
email_to_user($cuser, $supportuser, $subject, $messagemanager, $messagemanagerhtml);
	return email_to_user($bookingmanager, $supportuser, $subject, $messagemanager, $messagemanagerhtml);
	} else {

		return false;
	}
}

function booking_send_reminder_email($user,$optionid){

	global $CFG;
	$option = get_record('booking_options', 'id', $optionid);
	$supportuser = generate_book_email_supportuser();
										   
	$data = new object();
	$data->name = fullname($user);
	$data->bookingname = $option->text;
	$data->date = userdate($option->coursestarttime, get_string('strftimedatetime')). " - ".userdate($option->courseendtime, get_string('strftimedatetime'));
	$data->location=$option->location;
	$data->venue=$option->venue;
	$data->room=$option->room;
	$user->mailformat = 1;  // Always send HTML version as well
	$subject = get_string('confirmationremindersubject','booking', $data);
	$message     = get_string('confirmationremindermessage', 'booking', $data);
	$messagehtml = text_to_html($message, false, false, true);
	if (email_to_user($user, $supportuser, $subject, $message, $messagehtml)) {

		return true;
	} else {

		return false;
	}
}
/**
 * Checks if user on waitinglist gets normal place if a user is deleted
 * @param $userid of user who will be deleted
 * @return false if no user gets from waitinglist to booked list or userid of user now on booked list
 */
function booking_check_statuschange($optionid,$bookingid,$cancelleduserid,$cmid) {
	if (booking_get_user_status($cancelleduserid, $optionid, $bookingid,$cmid) != get_string('booked','booking')) {
		return false;
	}
	$option = get_record('booking_options', 'id', $optionid);
	$current = get_record('booking_answers', 'bookingid', $bookingid, 'userid', $cancelleduserid, 'optionid', $optionid);
	$allresponses = get_records_select('booking_answers', "bookingid = $bookingid AND optionid = $optionid", 'timemodified', 'userid');
	$context  = get_context_instance(CONTEXT_MODULE,$cmid);
	$firstuseronwaitinglist = $option->maxanswers + 1;
	$i=1;
	foreach($allresponses as $answer){
		if (has_capability('mod/booking:choose', $context, $answer->userid)){
			$sortedresponses[$i++] = $answer->userid;
		}
	}
	if (count($sortedresponses) <= $option->maxanswers){
		return false;
	} else if (isset($sortedresponses[$firstuseronwaitinglist])) {
		return $sortedresponses[$firstuseronwaitinglist];
	} else {
		return false;
	}
}
/**
 * Checks if required user profile fields are filled out
 * @param $userid of user to be checked
 * @return false if no redirect necessery true if necessary
 */
function booking_check_user_profile_fields($userid){
	$redirect = false;
	if ($categories = get_records_select('user_info_category', '', 'sortorder ASC')) {
		foreach ($categories as $category) {
			if ($fields = get_records_select('user_info_field', "categoryid=$category->id", 'sortorder ASC')) {
				// check first if *any* fields will be displayed and if there are required fields
				$requiredfields = array();
				$redirect = false;
				foreach ($fields as $field) {
					if ($field->visible != 0 && $field->required == 1) {
						if (!$userdata = get_field('user_info_data','data', "userid", $userid,"fieldid",$field->id)){
							$redirect = true;
						}
					}
				}
			}
		}
	}
	return $redirect;
}

function booking_profile_definition(&$mform) {
	global $CFG;

	// if user is "admin" fields are displayed regardless
	$update = has_capability('moodle/user:update', get_context_instance(CONTEXT_SYSTEM));

	if ($categories = get_records_select('user_info_category', '', 'sortorder ASC')) {
		foreach ($categories as $category) {
			if ($fields = get_records_select('user_info_field', "categoryid=$category->id", 'sortorder ASC')) {

				// check first if *any* fields will be displayed
				$display = false;
				foreach ($fields as $field) {
					if ($field->visible != PROFILE_VISIBLE_NONE) {
						$display = true;
					}
				}

				// display the header and the fields
				if ($display or $update) {
					$mform->addElement('header', 'category_'.$category->id, format_string($category->name));
					foreach ($fields as $field) {
						require_once($CFG->dirroot.'/user/profile/field/'.$field->datatype.'/field.class.php');
						$newfield = 'profile_field_'.$field->datatype;
						$formfield = new $newfield($field->id);
						$formfield->edit_field($mform);
					}
				}
			}
		}
	}
}

/**
 * Returns all other caps used in module
 */
function booking_get_extra_capabilities() {
	return array('moodle/site:accessallgroups');
}

?>
