
<?php


class quiz_report extends quiz_default_report  {

    /**
     * Display the report.
     */

    function display($quiz, $cm, $course) {
        global $CFG;
		$data = array();
        $qcategories=null;
        // A particular attempt ID
       // $attemptno = required_param('attempt', PARAM_INT);    

	   $allattsql = "SELECT * FROM mdl_quiz_attempts where quiz=$quiz->id";
		 if (!$allattempt = get_records_sql($allattsql)) {
            error('No attempts found');
        } 
  // load the questions categories needed by page 
        $sql = 'SELECT name, info FROM '.$CFG->prefix.'question_categories WHERE id in ('.'SELECT DISTINCT q.category FROM '.
               $CFG->prefix.'question q, '.$CFG->prefix.'quiz_question_instances i WHERE i.quiz = '.$quiz->id.
               ' AND q.id = i.question )';
               
        if (!$qcategories = get_records_sql($sql)) {
            error('No questions categories found');
        }  
        // Only print headers if not asked to download data
        if (!$download = optional_param('download', NULL)) {
          $this->print_header_and_tabs($cm, $course, $quiz, "quizcategory");
        }
		 $html2 .= '<table class="allsession">';
			$html2 .='<th class="allsession">PortalID</th>';
			$html2 .='<th class="allsession">Firstname/Lastname</th>';
			$html2 .='<th class="allsession">Completed</th>';
			$html2 .='<th class="allsession">Grade(%)</th>';
			
			foreach ($qcategories  as $currobj) {
             $html2 .= '
			 <th class="allsession">'.$currobj->name.'</th>';
		
  			 
        }
		foreach ($allattempt as $attempt) {      

       
        // Define some strings
        $strtimeformat = get_string('strftimedatetime');
        $reporturl = $CFG->wwwroot.'/mod/quiz/report.php';
        $userid = get_field('quiz_attempts', 'userid', 'id', $attempt->uniqueid);
        $login = get_field('user', 'username', 'id', $userid);
        $firstname = get_field('user', 'firstname', 'id', $userid);
        $lastname = get_field('user', 'lastname', 'id', $userid);         
        $pagelist = quiz_questions_in_quiz($attempt->layout);
        $pagequestions = explode(',', $pagelist); 
                  
        $html2 .= '<tr class="allsession" align="center">';
		$html2 .= '<td class="allsession" >'.$login.'</td>';
		$html2 .= '<td class="allsession">'.$firstname.' '.$lastname.'</td>';
		$html2 .= '<td class="allsession">'.userdate($attempt->timefinish, $strtimeformat).'</td>';
		$html2 .= '<td class="allsession">'.$percentage.'</td>';
       
        // load the questions categories needed by page 
        $sql = 'SELECT name, info FROM '.$CFG->prefix.'question_categories WHERE id in ('.'SELECT DISTINCT q.category FROM '.
               $CFG->prefix.'question q, '.$CFG->prefix.'quiz_question_instances i WHERE i.quiz = '.$quiz->id.
               ' AND q.id = i.question AND q.id in ('.$pagelist.'))';
               
        if (!$qcategories = get_records_sql($sql)) {
            error('No questions categories found');
        }      
           
        foreach ($qcategories  as $currobj) {
            $currobj->qtype = array();
            $currobj->known = array();
            $currobj->misconc = array();
            $currobj->problem = array();
            
            foreach ($qtypes as $type) {      
                $currobj->qtype[$type->qtype] = 0;
            }
            
            $currobj->length = $currobj->grade = $currobj->maxgrade = $currobj->percentage = 0; 
            $currobj->right = $currobj->partial = $currobj->wrong = $currobj->noanswer = 0;              
        } 
         
        // load all the questions needed by page 
        $sql = "SELECT q.*, i.grade AS maxgrade, i.id AS instance".
               "  FROM {$CFG->prefix}question q,".
               "       {$CFG->prefix}quiz_question_instances i".
               " WHERE i.quiz = '$quiz->id' AND q.id = i.question".
               "   AND q.id IN ($pagelist)";
               
        if (!$questions = get_records_sql($sql)) {
            error('No questions found');
        }

        // Load the question options
        if (!get_question_options($questions)) {
            error('Could not load question options');
        }

        // Restore the question sessions to their most recent states
        // creating new sessions where required
        if (!$states = get_question_states($questions, $quiz, $attempt)) {
            error('Could not restore question sessions');
        }
        
        $grade = quiz_rescale_grade($attempt->sumgrades, $quiz);
        $questionsno = count_records('question_sessions','attemptid',$attempt->uniqueid); 
        $correct = $partial = $false = $penalty = $multiple = $noanswer = 0;     
        // create an object which holds data about attempt's final grade 
        $a = new stdClass;
        $a->grade = round($grade,$quiz->decimalpoints);
        $a->maxgrade = $quiz->grade;
        $percentage = round(($attempt->sumgrades/$quiz->sumgrades)*100, $quiz->decimalpoints);   

        
        // get all attempts for this student
        $attempts = quiz_get_user_attempts($quiz->id, $userid);
        $sum = $count = $maximum = 0;
        $minimum = $quiz->sumgrades;
        
        foreach ($attempts as $attemptitem) {    
            $sum += round(($attemptitem->sumgrades/$quiz->sumgrades)*100, $quiz->decimalpoints);
            ++$count;
            
            if ($attemptitem->sumgrades < $minimum) {
                $minimum = $attemptitem->sumgrades;
            }
            if ($attemptitem->sumgrades > $maximum) {
                $maximum = $attemptitem->sumgrades;
            }
        }
       

        
        // Calculate results for each question category and type
        foreach ($pagequestions as $i) {          
            if ($questions[$i]->qtype == 'description') {
                --$questionsno;
                continue;
            }
            
            ++$qtypes[$questions[$i]->qtype]->length;
            $questions[$i]->category = get_field('question_categories', 'name', 'id', $questions[$i]->category);
            ++$qcategories[$questions[$i]->category]->length;
            $qcategories[$questions[$i]->category]->grade += $states[$i]->last_graded->raw_grade;
            $qcategories[$questions[$i]->category]->maxgrade += $questions[$i]->maxgrade;
            ++$qcategories[$questions[$i]->category]->qtype[$questions[$i]->qtype]; 
            
            // check if the answer is correct   
            if ($states[$i]->last_graded->raw_grade >= $questions[$i]->maxgrade/1.01) { // We divide by 1.01 so that rounding errors dont matter.
                ++$correct;
                $qpenalty = ($questions[$i]->maxgrade) - (round($states[$i]->last_graded->grade, $quiz->decimalpoints));
                $penalty += $qpenalty;
                ++$qcategories[$questions[$i]->category]->right;
                
                if ($quiz->optionflags & QUESTION_ADAPTIVE and $qpenalty) {
                    $qcategories[$questions[$i]->category]->misconc[] = $questions[$i]->name;
                } else {      
                    $qcategories[$questions[$i]->category]->known[] = $questions[$i]->name;
                }                  
            } else if ($states[$i]->last_graded->raw_grade > 0) { // check if the answer is partially correct 
                ++$partial;
                ++$qcategories[$questions[$i]->category]->partial;
                $qcategories[$questions[$i]->category]->misconc[] = $questions[$i]->name;
            } else { // check if the answer is wrong
                ++$false;
                ++$qcategories[$questions[$i]->category]->wrong;
                $qcategories[$questions[$i]->category]->problem[] = $questions[$i]->name;
                
                // Count the not answered questions depending on their question type
                $sql = 'SELECT id,answer FROM '.$CFG->prefix.'question_states'.' WHERE attempt = '.$attempt->uniqueid.'  AND question = '.
                        $questions[$i]->id.'  AND seq_number != 0 ORDER BY id DESC';
                        
                $answer = get_record_sql($sql,true);
                
                if ($answer) {        
                    if (($questions[$i]->qtype == 'match') or ($questions[$i]->qtype == 'randomsamatch') or ($questions[$i]->qtype == 'multianswer')) {
                        $answers = explode(',',$answer->answer);
                        $noanswerflag = 0;  
                                   
                        foreach ($answers as $value) {
                            $items = explode('-',$value);
                            if ((($questions[$i]->qtype == 'match') or ($questions[$i]->qtype == 'randomsamatch')) and ($items[1] != '0')) {
                                ++$noanswerflag;
                                break;
                            } else if (($questions[$i]->qtype == 'multianswer') and ($items[1] != '')) {
                                ++$noanswerflag;
                                break;
                            }      
                        }
                        
                        if ($noanswerflag == 0) {
                            ++$noanswer; 
                            $qcategories[$questions[$i]->category]->noanswer++;     
                        }             
                    } else if ($questions[$i]->qtype == 'multichoice') {
                        $answers = explode(':',$answer->answer); 
                                 
                        if ($answers[1] == '') {
                            ++$noanswer; 
                            $qcategories[$questions[$i]->category]->noanswer++;     
                        }
                    } else if ($questions[$i]->qtype == 'calculated') {
                        $answers = explode('-',$answer->answer); 
                                
                        if ($answers[1] == '') {
                            ++$noanswer; 
                            $qcategories[$questions[$i]->category]->noanswer++;     
                        }                   
                    } else {
                        if ($answer->answer == '') { 
                            ++$noanswer;  
                            $qcategories[$questions[$i]->category]->noanswer++; 
                        }
                    }        
                }
            }
            
            if ($states[$i]->last_graded->raw_grade > $states[$i]->last_graded->grade){
               ++$multiple;       
            }
        }  
        
        unset($pagequestions);
        // calculate score for each category
        foreach ($qcategories  as $currobj) {    
            $currobj->percentage = round(($currobj->grade/$currobj->maxgrade)*100, $quiz->decimalpoints);
        }         
        
        // Now check if asked download of data
        if ($download) {
            $filename = clean_filename("$course->shortname ".format_string($quiz->name,true)." $login ".$attempt->attempt);
            $sort = '';
        } 

		foreach ($qcategories  as $currobj) {
			 $html2 .= '<td class="allsession" align="center">'.$currobj->percentage.'</td>';
		

			 }
			 $html2 .= '</tr>';


    }  
	     //   unset($qcategories); 
        $html2 .= '</table>';
	 if (!$download) {
           // echo $html;
			echo $html2;

            echo '<table class="boxaligncenter"><tr>';
            $options = array();
            $options['q'] = $quiz->id;
            $options['attempt'] = $attempt->uniqueid;
            $options['mode'] = 'quizcategory';
            $options['sesskey'] = sesskey();
            $options['noheader'] = 'yes';
            echo '<td>';
            $options['download'] = 'XLS';
            print_single_button($reporturl, $options, get_string('downloadexcel', 'report_openlearner'));
            echo '</td><td>';
            helpbutton('textualdownload', get_string('textualdownload', 'report_openlearner'), 'quiz');
            echo '</td></tr></table>';
            
        } else  {

  // filename for download
  $filename = $course->fullname." quiz_" . date('Y/m/d') . ".xls";

  header("Content-Disposition: attachment; filename=\"$filename\"");
  header("Content-Type: application/vnd.ms-excel");

  echo ($html2);

  exit;
            }
			
	 return (true);
}    
}   
?>
