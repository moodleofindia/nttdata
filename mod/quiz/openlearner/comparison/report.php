<?php
/**
 * Attempt comparison of quiz report analysis displays in graphical format
 * a student's performance in an quiz attempt compared to the average performance
 * of all students in this quiz.
 *
 * @version $Id: report.php,v 1.9.6.2 2008/08/27 11:08:13 tasoulis7 Exp $
 * @author Martin Dougiamas, Tim Hunt and others.
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 * @package quiz
 * @subpackage openlearner
 */

class openlearner_report extends openlearner_default_report {

    /**
     * Display the report.
     */
    function display($quiz, $cm, $course) {
        global $USER, $CFG;
        
        // A particular attempt ID for graphical report
        $attemptno = required_param('attempt', PARAM_INT);    

        if (!$attempt = get_record('quiz_attempts', 'uniqueid', $attemptno)) {
            error('No such attempt ID exists');
        }
        
        if (!count_records('question_sessions', 'attemptid', $attempt->uniqueid)) {
            // this question has not yet been upgraded to the new model
            quiz_upgrade_states($attempt);
        }
        
		// Only print headers if not asked to download data
        if (!$download = optional_param('download', NULL)) {
            $this->print_header_and_tabs($cm, $course, $quiz, $attemptno, $reportmode='comparison');
        }
        
        // Define some strings
        $reporturl = $CFG->wwwroot.'/mod/quiz/openlearner.php';
        $reporturlwithoptions = $reporturl.'?mode=comparison&amp;id='.$cm->id .'&amp;attempt='.$attempt->uniqueid;
        $userid = get_field('quiz_attempts', 'userid', 'id', $attempt->uniqueid);
        $login = get_field('user', 'username', 'id', $userid);
        $firstname = get_field('user', 'firstname', 'id', $userid);
        $lastname = get_field('user', 'lastname', 'id', $userid); 
        
        // create temp directory for page's images
        $dir = $CFG->dataroot.'/openlearner/';
        if (!file_exists($dir)) {
            mkdir($dir);
        }
        $dir = $CFG->dataroot.'/openlearner/comparison/';
        if (!file_exists($dir)) {
            mkdir($dir);
        }
        $dir = $CFG->dataroot.'/openlearner/comparison/'.$USER->id.'/';
        if (!file_exists($dir)) {
            mkdir($dir);
        }
        $dir = $CFG->dataroot.'/openlearner/comparison/'.$USER->id.'/'.$userid.'/';
        if (!file_exists($dir)) {
            mkdir($dir);
        }
        $filename = $CFG->dataroot.'/openlearner/comparison/'.$USER->id.'/'.$userid.'/'.$attemptno.'_comp0.png';
        $src = $CFG->wwwroot.'/mod/quiz/openlearner/pix.php/comparison/'.$USER->id.'/'.$userid.'/'.$attemptno.'_comp0.png';
        
        $pagelist = quiz_questions_in_quiz($attempt->layout);
        $pagequestions = explode(',', $pagelist);
        
        // find out current groups mode
        $currentgroup = groups_get_activity_group($cm, true);
        
        // Only print groups used if not asked to download data
        if (!$download = optional_param('download', NULL)) {
            groups_print_activity_menu($cm, $reporturlwithoptions); 
        }
         
        // load the questions categories needed by page            
        $sql = 'SELECT name, info FROM '.$CFG->prefix.'question_categories WHERE id in ('.'SELECT DISTINCT q.category FROM '.
                $CFG->prefix.'question q, '.$CFG->prefix.'quiz_question_instances i WHERE i.quiz = '.$quiz->id.
                
               ' AND q.id = i.question AND q.id in ('.$pagelist.'))';
        if (!$qcategories = get_records_sql($sql)) {
            error('No questions categories found');
        }      
           
        foreach ($qcategories as $key => $value) {
            $qcategories[$key]->qtype = array();
            $qcategories[$key]->known = array();
            $qcategories[$key]->misconc = array();
            $qcategories[$key]->problem = array();
            $qcategories[$key]->firstknown = array();
            $qcategories[$key]->firstmisconc = array();
            
            $qcategories[$key]->length = $qcategories[$key]->grade = $qcategories[$key]->maxgrade = $qcategories[$key]->percentage = 0; 
            $qcategories[$key]->right = $qcategories[$key]->partial = $qcategories[$key]->wrong = $qcategories[$key]->noanswer = 0;
            $qcategories[$key]->firstright = $qcategories[$key]->firstpartial = $qcategories[$key]->firstwrong = $qcategories[$key]->firstnoanswer = 0;
            $qcategories[$key]->avgscore = $qcategories[$key]->avgright = $qcategories[$key]->avgpartial = $qcategories[$key]->avgwrong = 0; 
            $qcategories[$key]->avgnoanswer = $qcategories[$key]->avgknown = $qcategories[$key]->avgmisconc = 0;                  
        } 
        
        // create an object which holds data about current attempt
        $stats = new stdClass;
        $stats->questionsno = count_records('question_sessions','attemptid',$attempt->uniqueid);
        $stats->correct = $stats->partial = $stats->wrong = $stats->noanswer = $stats->known = $stats->miscon = 0;
        
        // calculate category data stats for current student attempt 
        calculate($quiz, $attempt, $stats, $qcategories); 
               
        // calculate stats for each category               
        foreach ($qcategories as $key => $value) {
            $qcategories[$key]->percentage = round(($qcategories[$key]->grade/$qcategories[$key]->maxgrade)*100, $quiz->decimalpoints);
            $qcategories[$key]->firstright = $qcategories[$key]->right;
            $qcategories[$key]->firstpartial = $qcategories[$key]->partial;
            $qcategories[$key]->firstwrong = $qcategories[$key]->wrong;
            $qcategories[$key]->firstnoanswer = $qcategories[$key]->noanswer;
            $qcategories[$key]->firstknown = $qcategories[$key]->known;
            $qcategories[$key]->firstmisconc = $qcategories[$key]->misconc;
        }       
        
        // create an object which holds data about current attempt    
        $totalstats = new stdClass;    
        $totalstats->percentage = round(($attempt->sumgrades/$quiz->sumgrades)*100, $quiz->decimalpoints);
        $totalstats->correct = round(($stats->correct/$stats->questionsno)*100, $quiz->decimalpoints);
        $totalstats->partial = round(($stats->partial/$stats->questionsno)*100, $quiz->decimalpoints);
        $totalstats->wrong = round(($stats->wrong/$stats->questionsno)*100, $quiz->decimalpoints);
        $totalstats->noanswer = round(($stats->noanswer/$stats->questionsno)*100, $quiz->decimalpoints);
        $totalstats->known = round(($stats->known/$stats->questionsno)*100, $quiz->decimalpoints);
        $totalstats->miscon = round(($stats->miscon/$stats->questionsno)*100, $quiz->decimalpoints);         
        $avgscore = $avgcorrect = $avgpartial = $avgfalse = $avgnoanswer = $avgknown = $avgmiscon = 0;    
        
        // load the users who attempted the quiz
        if ($currentgroup) {                      // all users who can attempted quiz and are in the currently selected group    
            $sql = 'SELECT DISTINCT qa.userid FROM '.$CFG->prefix.'quiz_attempts qa JOIN '.$CFG->prefix.'groups_members gm ON qa.userid = gm.userid '.
                   'WHERE quiz = '.$quiz->id.' AND preview = 0 AND timefinish != 0 AND groupid = '.$currentgroup;
        } else { 
            $sql = 'SELECT DISTINCT userid FROM '.$CFG->prefix.'quiz_attempts WHERE quiz = '.$quiz->id.' AND preview = 0 AND timefinish != 0';    
        } 
        
        if (!$users = get_records_sql($sql)) {
            error('No users found');
        }
                                 
        switch ($quiz->grademethod) {
            case QUIZ_GRADEHIGHEST:                  
                // load the attempt with the highest grade for each user for this quiz
                foreach ($users as $user) {      
                    // select user's attempt with the highest grade for this quiz (if there are more than one, select the last of them)
                    $sql = 'SELECT uniqueid FROM '.$CFG->prefix.'quiz_attempts WHERE sumgrades = (SELECT MAX(sumgrades) FROM '.$CFG->prefix.
                           'quiz_attempts WHERE quiz = '.$quiz->id.' AND userid = '.$user->userid.' AND preview = 0 AND timefinish != 0)'.
                           ' AND quiz = '.$quiz->id.' AND userid = '.$user->userid.' AND preview = 0 AND timefinish != 0 ORDER BY attempt DESC';
               
                    if (!$id = get_record_sql($sql,true)) {
                        error('No attempts found');
                    }    
                            
                    if (!$userattempt = get_record('quiz_attempts', 'uniqueid', $id->uniqueid)) {
                        error('No such attempt ID exists');
                    }  
                              
                    if (!count_records('question_sessions', 'attemptid', $userattempt->uniqueid)) {
                        // this question has not yet been upgraded to the new model
                        quiz_upgrade_states($userattempt);
                    } 
                
                    $stats->questionsno = count_records('question_sessions','attemptid',$userattempt->uniqueid);
                    $stats->correct = $stats->partial = $stats->wrong = $stats->noanswer = $stats->known = $stats->miscon = 0;
                    
                    foreach ($qcategories as $key => $value) {    
                        $qcategories[$key]->known = array();
                        $qcategories[$key]->misconc = array();
                        $qcategories[$key]->length = $qcategories[$key]->grade = $qcategories[$key]->maxgrade = 0;
                        $qcategories[$key]->right = $qcategories[$key]->partial = 0;
                        $qcategories[$key]->wrong = $qcategories[$key]->noanswer = 0;          
                    }
                    
                    // calculate category data stats for current student attempt 
                    calculate($quiz, $userattempt, $stats, $qcategories);
            
                    $avgscore += $userattempt->sumgrades/$quiz->sumgrades;
                    $avgcorrect += $stats->correct;
                    $avgpartial += $stats->partial;
                    $avgfalse += $stats->wrong;
                    $avgnoanswer += $stats->noanswer;
                    $avgknown += $stats->known; 
                    $avgmiscon += $stats->miscon;
                    
                    // calculate stats for each category 
                    foreach ($qcategories as $key => $value) {    
                        $qcategories[$key]->avgscore += $qcategories[$key]->grade/$qcategories[$key]->maxgrade;
                        $qcategories[$key]->avgright += $qcategories[$key]->right;
                        $qcategories[$key]->avgpartial += $qcategories[$key]->partial;
                        $qcategories[$key]->avgwrong += $qcategories[$key]->wrong;
                        $qcategories[$key]->avgnoanswer += $qcategories[$key]->noanswer;
                        $qcategories[$key]->avgknown += count($qcategories[$key]->known);
                        $qcategories[$key]->avgmisconc += count($qcategories[$key]->misconc);
                    }
                }  
            
                $total = count($users);
                break;     
            case QUIZ_GRADEAVERAGE:     
                // load all user attempts for this quiz 
                $sql = 'SELECT uniqueid FROM '.$CFG->prefix.'quiz_attempts WHERE quiz = '.$quiz->id.' AND preview = 0 AND timefinish != 0';
                
                if (!$userattempts = get_records_sql($sql)) {
                    error('No user attempts found');
                }
                
                foreach ($userattempts as $id) {          
                    if (!$userattempt = get_record('quiz_attempts', 'uniqueid', $id->uniqueid)) {
                        error('No such attempt ID exists');
                    }
                    
                    if (!count_records('question_sessions', 'attemptid', $userattempt->uniqueid)) {
                        // this question has not yet been upgraded to the new model
                        quiz_upgrade_states($userattempt);
                    }
                    
                    $stats->questionsno = count_records('question_sessions','attemptid',$userattempt->uniqueid);
                    $stats->correct = $stats->partial = $stats->wrong = $stats->noanswer = $stats->known = $stats->miscon = 0;
            
                    foreach ($qcategories as $key => $value) {    
                        $qcategories[$key]->known = array();
                        $qcategories[$key]->misconc = array();
                        $qcategories[$key]->length = $qcategories[$key]->grade = $qcategories[$key]->maxgrade = 0;
                        $qcategories[$key]->right = $qcategories[$key]->partial = 0;
                        $qcategories[$key]->wrong = $qcategories[$key]->noanswer = 0;          
                    }
                    
                    // calculate category data stats for current student attempt
                    calculate($quiz, $userattempt, $stats, $qcategories);
            
                    $avgscore += $userattempt->sumgrades/$quiz->sumgrades;
                    $avgcorrect += $stats->correct;
                    $avgpartial += $stats->partial;
                    $avgfalse += $stats->wrong;
                    $avgnoanswer += $stats->noanswer;
                    $avgknown += $stats->known; 
                    $avgmiscon += $stats->miscon;
                    
                    // calculate stats for each category 
                    foreach ($qcategories as $key => $value) {    
                        $qcategories[$key]->avgscore += $qcategories[$key]->grade/$qcategories[$key]->maxgrade;
                        $qcategories[$key]->avgright += $qcategories[$key]->right;
                        $qcategories[$key]->avgpartial += $qcategories[$key]->partial;
                        $qcategories[$key]->avgwrong += $qcategories[$key]->wrong;
                        $qcategories[$key]->avgnoanswer += $qcategories[$key]->noanswer;
                        $qcategories[$key]->avgknown += count($qcategories[$key]->known);
                        $qcategories[$key]->avgmisconc += count($qcategories[$key]->misconc);
                    }
                }
                  
                $total = count($userattempts);   
                break; 
            case QUIZ_ATTEMPTFIRST:
                // load the first attempt of each user for this quiz
                foreach ($users as $user) {      
                    // select user's first attempt for this quiz
                    $sql = 'SELECT uniqueid FROM '.$CFG->prefix.'quiz_attempts WHERE quiz = '.$quiz->id.' AND userid = '.$user->userid.
                           ' AND preview = 0 AND timefinish != 0 ORDER BY attempt ASC';
               
                    if (!$id = get_record_sql($sql,true)) {
                        error('No attempts found');
                    } 
                               
                    if (!$userattempt = get_record('quiz_attempts', 'uniqueid', $id->uniqueid)) {
                        error('No such attempt ID exists');
                    } 
                               
                    if (!count_records('question_sessions', 'attemptid', $userattempt->uniqueid)) {
                        // this question has not yet been upgraded to the new model
                        quiz_upgrade_states($userattempt);
                    } 
            
                    $stats->questionsno = count_records('question_sessions','attemptid',$userattempt->uniqueid);
                    $stats->correct = $stats->partial = $stats->wrong = $stats->noanswer = $stats->known = $stats->miscon = 0;
            
                    foreach ($qcategories as $key => $value) {    
                        $qcategories[$key]->known = array();
                        $qcategories[$key]->misconc = array();
                        $qcategories[$key]->length = $qcategories[$key]->grade = $qcategories[$key]->maxgrade = 0;
                        $qcategories[$key]->right = $qcategories[$key]->partial = 0;
                        $qcategories[$key]->wrong = $qcategories[$key]->noanswer = 0;          
                    }
                    
                    // calculate category data stats for current student attempt
                    calculate($quiz, $userattempt, $stats, $qcategories);
            
                    $avgscore += $userattempt->sumgrades/$quiz->sumgrades;
                    $avgcorrect += $stats->correct;
                    $avgpartial += $stats->partial;
                    $avgfalse += $stats->wrong;
                    $avgnoanswer += $stats->noanswer;
                    $avgknown += $stats->known; 
                    $avgmiscon += $stats->miscon;
                    
                    // calculate stats for each category
                    foreach ($qcategories as $key => $value) {    
                        $qcategories[$key]->avgscore += $qcategories[$key]->grade/$qcategories[$key]->maxgrade;
                        $qcategories[$key]->avgright += $qcategories[$key]->right;
                        $qcategories[$key]->avgpartial += $qcategories[$key]->partial;
                        $qcategories[$key]->avgwrong += $qcategories[$key]->wrong;
                        $qcategories[$key]->avgnoanswer += $qcategories[$key]->noanswer;
                        $qcategories[$key]->avgknown += count($qcategories[$key]->known);
                        $qcategories[$key]->avgmisconc += count($qcategories[$key]->misconc);
                    }
                } 
             
                $total = count($users);   
                break;  
            case QUIZ_ATTEMPTLAST:      
                // load the last attempt of each user for this quiz
                foreach ($users as $user) {      
                    // select user's last attempt for this quiz
                    $sql = 'SELECT uniqueid FROM '.$CFG->prefix.'quiz_attempts WHERE quiz = '.$quiz->id.' AND userid = '.$user->userid.
                           ' AND preview = 0 AND timefinish != 0 ORDER BY attempt DESC';
               
                    if (!$id = get_record_sql($sql,true)) {
                        error('No attempts found');
                    }  
                              
                    if (!$userattempt = get_record('quiz_attempts', 'uniqueid', $id->uniqueid)) {
                        error('No such attempt ID exists');
                    } 
                               
                    if (!count_records('question_sessions', 'attemptid', $userattempt->uniqueid)) {
                        // this question has not yet been upgraded to the new model
                        quiz_upgrade_states($userattempt);
                    } 
            
                    $stats->questionsno = count_records('question_sessions','attemptid',$userattempt->uniqueid);
                    $stats->correct = $stats->partial = $stats->wrong = $stats->noanswer = $stats->known = $stats->miscon = 0;
                  
                    foreach ($qcategories as $key => $value) {    
                        $qcategories[$key]->known = array();
                        $qcategories[$key]->misconc = array();
                        $qcategories[$key]->length = $qcategories[$key]->grade = $qcategories[$key]->maxgrade = 0;
                        $qcategories[$key]->right = $qcategories[$key]->partial = 0;
                        $qcategories[$key]->wrong = $qcategories[$key]->noanswer = 0;          
                    }
                    
                    // calculate category data stats for current student attempt
                    calculate($quiz, $userattempt, $stats, $qcategories);
            
                    $avgscore += $userattempt->sumgrades/$quiz->sumgrades;
                    $avgcorrect += $stats->correct;
                    $avgpartial += $stats->partial;
                    $avgfalse += $stats->wrong;
                    $avgnoanswer += $stats->noanswer;
                    $avgknown += $stats->known; 
                    $avgmiscon += $stats->miscon;
                    
                    // calculate stats for each category
                    foreach ($qcategories as $key => $value) {    
                        $qcategories[$key]->avgscore += $qcategories[$key]->grade/$qcategories[$key]->maxgrade;
                        $qcategories[$key]->avgright += $qcategories[$key]->right;
                        $qcategories[$key]->avgpartial += $qcategories[$key]->partial;
                        $qcategories[$key]->avgwrong += $qcategories[$key]->wrong;
                        $qcategories[$key]->avgnoanswer += $qcategories[$key]->noanswer;
                        $qcategories[$key]->avgknown += count($qcategories[$key]->known);
                        $qcategories[$key]->avgmisconc += count($qcategories[$key]->misconc);
                    }
                }  
        
                $total = count($users);  
                break;
            
            default:      
        }         
        
        $totalstats->avgscore = round(($avgscore/$total)*100, $quiz->decimalpoints);
        $totalstats->avgcorrect = round(($avgcorrect/($total * $stats->questionsno))*100, $quiz->decimalpoints);
        $totalstats->avgpartial = round(($avgpartial/($total * $stats->questionsno))*100, $quiz->decimalpoints);
        $totalstats->avgfalse = round(($avgfalse/($total * $stats->questionsno))*100, $quiz->decimalpoints);
        $totalstats->avgnoanswer = round(($avgnoanswer/($total * $stats->questionsno))*100, $quiz->decimalpoints);
        $totalstats->avgknown = round(($avgknown/($total * $stats->questionsno))*100, $quiz->decimalpoints); 
        $totalstats->avgmiscon = round(($avgmiscon/($total * $stats->questionsno))*100, $quiz->decimalpoints); 
        
        // Now check if asked download of data
        if ($download) {
            $file = clean_filename("$course->shortname ".format_string($quiz->name,true)." $login ".$attempt->attempt.' comparison');
            $sort = '';
        } 
		
        $title = $firstname.' '.$lastname.' - '.$quiz->name.' ('.get_string('attempt', 'report_openlearner').
                 $attempt->attempt.')';
        $html = '<div class="quizattemptcounts"><b>'.format_string($title).'</b></div>';
        $html .= '<p align="center">'.get_string('studentsnumber', 'report_openlearner').
                 ' '.count($users).'</p>';
        $html .= '<p align="center">'.get_string('grademethod', 'quiz').': '.quiz_get_grading_option_name($quiz->grademethod).'</p>';      
        
        if ($totalstats->percentage > $totalstats->avgscore) {
            $html .= '<p align="center">'.get_string('greater', 'report_openlearner').'</p>';
        }
        else if ($totalstats->percentage < $totalstats->avgscore) {
            $html .= '<p align="center">'.get_string('less', 'report_openlearner').'</p>';
        }
        else {
            $html .= '<p align="center">'.get_string('equal', 'report_openlearner').'</p>';
        }   
        
        // show data graphs for current attempt 
        graph($totalstats, $filename);              
        $html .= '<p></p><div class="mdl-align"><img src="'.$src.'" alt="comparison_charts"></div>';     
        
        // Show category stats if we have more than one category
        if (count($qcategories) != 1) {
            $html .= '<p style="font-size:12pt;font-weight: bold" align="center" ><b>'.format_string(get_string('analysis', 'report_openlearner')).
                     '</b></p>';
            $counter = 0;
            
            foreach ($qcategories  as $currobj) {
                $html .= '<p></p><p align="center">'.get_string('category', 'quiz').': '.$currobj->name.'</p>';
                
                if ($currobj->info != '') {
                    $html .= '<p align="center">'.format_string('('.$currobj->info.')').'</p>';  
                }
                
                ++$counter;
                $totalstats->percentage = $currobj->percentage;
                $totalstats->correct = round(($currobj->firstright/$currobj->length)*100, $quiz->decimalpoints);
                $totalstats->partial = round(($currobj->firstpartial/$currobj->length)*100, $quiz->decimalpoints);
                $totalstats->wrong = round(($currobj->firstwrong/$currobj->length)*100, $quiz->decimalpoints);
                $totalstats->noanswer = round(($currobj->firstnoanswer/$currobj->length)*100, $quiz->decimalpoints);
                $totalstats->known = round((count($currobj->firstknown)/$currobj->length)*100, $quiz->decimalpoints);
                $totalstats->miscon = round((count($currobj->firstmisconc)/$currobj->length)*100, $quiz->decimalpoints);
                $totalstats->avgscore = round(($currobj->avgscore/$total) * 100, $quiz->decimalpoints);
                $totalstats->avgcorrect = round(($currobj->avgright/($total * $currobj->length))*100, $quiz->decimalpoints);
                $totalstats->avgpartial = round(($currobj->avgpartial/($total * $currobj->length))*100, $quiz->decimalpoints);
                $totalstats->avgfalse = round(($currobj->avgwrong/($total * $currobj->length))*100, $quiz->decimalpoints);
                $totalstats->avgnoanswer = round(($currobj->avgnoanswer/($total * $currobj->length))*100, $quiz->decimalpoints);
                $totalstats->avgknown = round(($currobj->avgknown/($total * $currobj->length))*100, $quiz->decimalpoints);
                $totalstats->avgmiscon = round(($currobj->avgmisconc/($total * $currobj->length))*100, $quiz->decimalpoints);
                
                if ($totalstats->percentage > $totalstats->avgscore) {
                    $html .= '<p align="center">'.get_string('catgreater', 'report_openlearner').'</p>';
                }
                else if ($totalstats->percentage < $totalstats->avgscore) {
                    $html .= '<p align="center">'.get_string('catless', 'report_openlearner').'</p>';
                }
                else {
                    $html .= '<p align="center">'.get_string('catequal', 'report_openlearner').'</p>';
                }  
                
                $filename = $CFG->dataroot.'/openlearner/comparison/'.$USER->id.'/'.$userid.'/'.$attemptno.'_comp'.$counter.'.png';
                $src = $CFG->wwwroot.'/mod/quiz/openlearner/pix.php/comparison/'.$USER->id.'/'.$userid.'/'.$attemptno.'_comp'.$counter.'.png';
                // show data graphs for current category
                graph($totalstats, $filename);   
                $html .= '<p></p><div class="mdl-align"><img src="'.$src.'" alt="comparison_charts"></div>';
            }            
        }
        
        unset($totalstats);
        unset($stats);
        unset($qcategories);
        
		if (!$download) {
            echo $html;
           
            
        } 
        return (true);   
    }   
} 

/**
 * Calculate stats about each question category for a specific student atttempt 
 *
 * @uses $CFG
 * @param mixed $quiz a quiz object
 * @param mixed $attemptid an object with student's attempt id
 * @param mixed $stats an object with data stats
 * @param mixed $qcategories an object with question category data stats
 */
function calculate($quiz, $attemptid, &$stats, &$qcategories) {
    global $CFG;
      
    $pagelist = quiz_questions_in_quiz($attemptid->layout);
    $pagequestions = explode(',', $pagelist);
         
    // load all the questions needed by page 
    $sql = "SELECT q.*, i.grade AS maxgrade, i.id AS instance".
           "  FROM {$CFG->prefix}question q,".
           "       {$CFG->prefix}quiz_question_instances i".
           " WHERE i.quiz = '$quiz->id' AND q.id = i.question".
           "   AND q.id IN ($pagelist)";
           
    if (!$questions = get_records_sql($sql)) {
        error('No questions found');
    }

    // Load the question options
    if (!get_question_options($questions)) {
        error('Could not load question options');
    }
        
    // Restore the question sessions to their most recent states
    // creating new sessions where required
    if (!$states = get_question_states($questions, $quiz, $attemptid)) {
        error('Could not restore question sessions');
    }
    
    // Calculate results for each question category and type    
    foreach ($pagequestions as $i) {          
        if ($questions[$i]->qtype == 'description') {
            --$stats->questionsno;
            continue;
        }
            
        $questions[$i]->category = get_field('question_categories', 'name', 'id', $questions[$i]->category);
        ++$qcategories[$questions[$i]->category]->length;
        $qcategories[$questions[$i]->category]->grade += $states[$i]->last_graded->raw_grade;
        $qcategories[$questions[$i]->category]->maxgrade += $questions[$i]->maxgrade;
        
        // check if the answer is correct         
        if ($states[$i]->last_graded->raw_grade >= $questions[$i]->maxgrade/1.01) { // We divide by 1.01 so that rounding errors dont matter.
            ++$stats->correct;
            $qpenalty = ($questions[$i]->maxgrade) - (round($states[$i]->last_graded->grade, $quiz->decimalpoints));
            ++$qcategories[$questions[$i]->category]->right;
                
            if ($quiz->optionflags & QUESTION_ADAPTIVE and $qpenalty) {
                $qcategories[$questions[$i]->category]->misconc[] = $questions[$i]->name;
                ++$stats->miscon;
            } else {      
                $qcategories[$questions[$i]->category]->known[] = $questions[$i]->name;
                ++$stats->known;        
            }              
        } else if ($states[$i]->last_graded->raw_grade > 0) { // check if the answer is partially correct
            ++$stats->partial;
            ++$stats->miscon;
            ++$qcategories[$questions[$i]->category]->partial;
            $qcategories[$questions[$i]->category]->misconc[] = $questions[$i]->name;
        } else { // check if the answer is wrong
            ++$stats->wrong;
            ++$qcategories[$questions[$i]->category]->wrong;
            $qcategories[$questions[$i]->category]->problem[] = $questions[$i]->name;
                
            // Count the not answered questions depending on their question type
            $sql = 'SELECT id,answer FROM '.$CFG->prefix.'question_states'.' WHERE attempt = '.$attemptid->id.'  AND question = '.
                    $questions[$i]->id.'  AND seq_number != 0 ORDER BY id DESC';
                    
            $answer = get_record_sql($sql,true);
            
            if ($answer) {        
                if (($questions[$i]->qtype == 'match') or ($questions[$i]->qtype == 'randomsamatch') or ($questions[$i]->qtype == 'multianswer')) {
                    $answers = explode(',',$answer->answer);
                    $noanswerflag = 0;  
                               
                    foreach ($answers as $value) {
                        $items = explode('-',$value);
                        
                        if ((($questions[$i]->qtype == 'match') or ($questions[$i]->qtype == 'randomsamatch')) and ($items[1] != '0')) {
                            ++$noanswerflag;
                            break;
                        } else if (($questions[$i]->qtype == 'multianswer') and ($items[1] != '')) {
                            ++$noanswerflag;
                            break;
                        }      
                    }
                    
                    if ($noanswerflag == 0) {
                        ++$stats->noanswer; 
                        ++$qcategories[$questions[$i]->category]->noanswer;     
                    } 
                } else if ($questions[$i]->qtype == 'multichoice') {
                    $answers = explode(':',$answer->answer);  
                            
                    if ($answers[1] == '') {
                        ++$stats->noanswer; 
                        ++$qcategories[$questions[$i]->category]->noanswer;     
                    }
                } else if ($questions[$i]->qtype == 'calculated') {
                    $answers = explode('-',$answer->answer);  
                            
                    if ($answers[1] == '') {
                        ++$stats->noanswer; 
                        ++$qcategories[$questions[$i]->category]->noanswer;     
                    }                   
                } else {
                    if ($answer->answer == '') { 
                        ++$stats->noanswer;  
                        ++$qcategories[$questions[$i]->category]->noanswer; 
                    }
                }        
            }
        }
    }  
            
    return (0);
} 

/**
 * Outputs graph images based on calculated data for a specific student attempt
 *
 * @uses $CFG
 * @param mixed $stats an object with data stats.
 * @param string $filename the filename for a specific graph image.
 */
function graph(&$stats, $filename) {
    global $CFG;
    
    require_once("$CFG->libdir/chartlib.class.php");

    $object = new MoodleImageGraph();  
    // create the graph
    $graph =& $object->new_graph(array(950, 425));  
    // add a TrueType font
    $font =& $object->set_font($graph, 'Verdana', 10);
    $object->add_font($graph, $font); 
    // add background color
    $object->set_bgcolor($graph, 'white');
    $plotareaa = $object->get_plotarea();
    $plotareab = $object->get_plotarea();
    $legenda = $object->get_legend();
    $legendb = $object->get_legend();  
    // create the plotarea
    $object->set_plotarea($graph, get_string('userperformance', 'report_openlearner'), get_string('averageperformance', 'report_openlearner'), 10, 10, 5, 5, 80, 80, 50, $plotareaa, $plotareab, $legenda, $legendb);
    $object->set_legend_plotarea($plotareaa, $legenda);
    $object->set_legend_plotarea($plotareab, $legendb);
    // set axisX & axisY properties
    $axisxa =& $object->get_axisX($plotareaa, false);
    $axisya =& $object->get_axisY($plotareaa, 100, 10);
    $axisxb =& $object->get_axisX($plotareab, false);
    $axisyb =& $object->get_axisY($plotareab, 100, 10);  
    // create the dataset for 1st plot
    $data11 = array(array('x'=>1, 'y'=>$stats->percentage, 'id'=> 'score'), array('x'=>2, 'y'=>0, 'id'=> 'score'),
              array('x'=>3, 'y'=>0, 'id'=> 'score'),array('x'=>4, 'y'=>0, 'id'=> 'score'),
              array('x'=>5, 'y'=>0, 'id'=> 'score'));
    $data12 = array(array('x'=>1, 'y'=>0, 'id'=> 'correct'), array('x'=>2, 'y'=>$stats->correct, 'id'=> 'correct'),
              array('x'=>3, 'y'=>0, 'id'=> 'correct'), array('x'=>4, 'y'=>0, 'id'=> 'correct'),
              array('x'=>5, 'y'=>0, 'id'=> 'correct'));
    $data13 = array(array('x'=>1, 'y'=>0, 'id'=> 'partial'), array('x'=>2, 'y'=>$stats->partial, 'id'=> 'partial'),
              array('x'=>3, 'y'=>0, 'id'=> 'partial'), array('x'=>4, 'y'=>0, 'id'=> 'partial'),
              array('x'=>5, 'y'=>0, 'id'=> 'partial'));
    $data14 = array(array('x'=>1, 'y'=>0, 'id'=> 'false'), array('x'=>2, 'y'=>0, 'id'=> 'false'),
              array('x'=>3, 'y'=>$stats->wrong, 'id'=> 'false'), array('x'=>4, 'y'=>0, 'id'=> 'false'),
              array('x'=>5, 'y'=>0, 'id'=> 'false'));      
    $data15 = array(array('x'=>1, 'y'=>0, 'id'=> 'noanswer'), array('x'=>2, 'y'=>0, 'id'=> 'noanswer'),
              array('x'=>3, 'y'=>0, 'id'=> 'noanswer'), array('x'=>4, 'y'=>$stats->noanswer, 'id'=> 'noanswer'),
              array('x'=>5, 'y'=>0, 'id'=> 'noanswer'));      
    $data16 = array(array('x'=>1, 'y'=>0, 'id'=> 'known'), array('x'=>2, 'y'=>0, 'id'=> 'known'),
              array('x'=>3, 'y'=>0, 'id'=> 'known'), array('x'=>4, 'y'=>0, 'id'=> 'known'),
              array('x'=>5, 'y'=>$stats->known, 'id'=> 'known'));
    $data17 = array(array('x'=>1, 'y'=>0, 'id'=> 'miscon'), array('x'=>2, 'y'=>0, 'id'=> 'miscon'),
              array('x'=>3, 'y'=>0, 'id'=> 'miscon'), array('x'=>4, 'y'=>0, 'id'=> 'miscon'),
              array('x'=>5, 'y'=>$stats->miscon, 'id'=> 'miscon'));
    $data18 = array(array('x'=>1, 'y'=>0, 'id'=> 'problem'), array('x'=>2, 'y'=>0, 'id'=> 'problem'),
              array('x'=>3, 'y'=>0, 'id'=> 'problem'), array('x'=>4, 'y'=>0, 'id'=> 'problem'),
              array('x'=>5, 'y'=>$stats->wrong, 'id'=> 'problem'));                 
    $dataseta = $object->create_bardataset('score', get_string('score','report_openlearner'), $data11);
    $object->set_bardataset('correct', get_string('correctanswers','report_openlearner'), $data12, $dataseta);
    $object->set_bardataset('partial', get_string('partial','report_openlearner'), $data13, $dataseta);
    $object->set_bardataset('false', get_string('false','report_openlearner'), $data14, $dataseta);
    $object->set_bardataset('noanswer', get_string('noanswer','report_openlearner'), $data15, $dataseta);
    $object->set_bardataset('known', get_string('knowntopics','report_openlearner'), $data16, $dataseta);
    $object->set_bardataset('miscon', get_string('misconceptions','report_openlearner'), $data17, $dataseta);
    $object->set_bardataset('problem', get_string('problematic','report_openlearner'), $data18, $dataseta);         
    // create the 1st plot as smoothed area chart using the 1st dataset
    $plota =& $object->add_barplot($plotareaa, 'stacked', $dataseta);
    // create a fill array 
    $fillarray =& $object->create_farray();
    $object->add_color($fillarray, 'chartreuse', 'score');
    $object->add_color($fillarray, 'blue', 'correct');
    $object->add_color($fillarray, 'dodgerblue', 'partial');
    $object->add_color($fillarray, 'red', 'false');
    $object->add_color($fillarray, 'crimson', 'noanswer');
    $object->add_color($fillarray, 'ivory', 'known');
    $object->add_color($fillarray, 'silver', 'miscon');
    $object->add_color($fillarray, 'gray', 'problem');
    // set a line color
    $object->set_line_color($plota, 'gray');
    // set a standard fill style
    $object->set_fill_style($plota, $fillarray);
    // create a Y data value marker for the 1st plot
    $markera =& $object->create_barmarker($plota, 0, 0);
    $object->set_marker($plota, $markera, 10);
    $object->set_data_selector($plota);        
    // create the dataset for 2nd plot
    $data21 = array(array('x'=>1, 'y'=>$stats->avgscore, 'id'=> 'score'), array('x'=>2, 'y'=>0, 'id'=> 'score'),
              array('x'=>3, 'y'=>0, 'id'=> 'score'),array('x'=>4, 'y'=>0, 'id'=> 'score'),
              array('x'=>5, 'y'=>0, 'id'=> 'score'));
    $data22 = array(array('x'=>1, 'y'=>0, 'id'=> 'correct'), array('x'=>2, 'y'=>$stats->avgcorrect, 'id'=> 'correct'),
              array('x'=>3, 'y'=>0, 'id'=> 'correct'), array('x'=>4, 'y'=>0, 'id'=> 'correct'),
              array('x'=>5, 'y'=>0, 'id'=> 'correct'));
    $data23 = array(array('x'=>1, 'y'=>0, 'id'=> 'partial'), array('x'=>2, 'y'=>$stats->avgpartial, 'id'=> 'partial'),
              array('x'=>3, 'y'=>0, 'id'=> 'partial'), array('x'=>4, 'y'=>0, 'id'=> 'partial'),
              array('x'=>5, 'y'=>0, 'id'=> 'partial'));
    $data24 = array(array('x'=>1, 'y'=>0, 'id'=> 'false'), array('x'=>2, 'y'=>0, 'id'=> 'false'),
              array('x'=>3, 'y'=>$stats->avgfalse, 'id'=> 'false'), array('x'=>4, 'y'=>0, 'id'=> 'false'),
              array('x'=>5, 'y'=>0, 'id'=> 'false'));      
    $data25 = array(array('x'=>1, 'y'=>0, 'id'=> 'noanswer'), array('x'=>2, 'y'=>0, 'id'=> 'noanswer'),
              array('x'=>3, 'y'=>0, 'id'=> 'noanswer'), array('x'=>4, 'y'=>$stats->avgnoanswer, 'id'=> 'noanswer'),
              array('x'=>5, 'y'=>0, 'id'=> 'noanswer'));      
    $data26 = array(array('x'=>1, 'y'=>0, 'id'=> 'known'), array('x'=>2, 'y'=>0, 'id'=> 'known'),
              array('x'=>3, 'y'=>0, 'id'=> 'known'), array('x'=>4, 'y'=>0, 'id'=> 'known'),
              array('x'=>5, 'y'=>$stats->avgknown, 'id'=> 'known'));
    $data27 = array(array('x'=>1, 'y'=>0, 'id'=> 'miscon'), array('x'=>2, 'y'=>0, 'id'=> 'miscon'),
              array('x'=>3, 'y'=>0, 'id'=> 'miscon'), array('x'=>4, 'y'=>0, 'id'=> 'miscon'),
              array('x'=>5, 'y'=>$stats->avgmiscon, 'id'=> 'miscon'));
    $data28 = array(array('x'=>1, 'y'=>0, 'id'=> 'problem'), array('x'=>2, 'y'=>0, 'id'=> 'problem'),
              array('x'=>3, 'y'=>0, 'id'=> 'problem'), array('x'=>4, 'y'=>0, 'id'=> 'problem'),
              array('x'=>5, 'y'=>$stats->avgfalse, 'id'=> 'problem'));                                       
    $datasetb = $object->create_bardataset('score', get_string('score','report_openlearner'), $data21);
    $object->set_bardataset('correct', get_string('correctanswers','report_openlearner'), $data22, $datasetb);
    $object->set_bardataset('partial', get_string('partial','report_openlearner'), $data23, $datasetb);
    $object->set_bardataset('false', get_string('false','report_openlearner'), $data24, $datasetb);
    $object->set_bardataset('noanswer', get_string('noanswer','report_openlearner'), $data25, $datasetb);
    $object->set_bardataset('known', get_string('knowntopics','report_openlearner'), $data26, $datasetb);
    $object->set_bardataset('miscon', get_string('misconceptions','report_openlearner'), $data27, $datasetb);
    $object->set_bardataset('problem', get_string('problematic','report_openlearner'), $data28, $datasetb);        
    // create the 2nd plot as smoothed area chart using the 2nd dataset
    $plotb =& $object->add_barplot($plotareab, 'stacked', $datasetb);     
    // set a line color
    $object->set_line_color($plotb, 'gray');
    // set a standard fill style
    $object->set_fill_style($plotb, $fillarray);
    // create a Y data value marker for the 2nd plot
    $markerb =& $object->create_barmarker($plotb, 0, 0);
    $object->set_marker($plotb, $markerb, 10);
    $object->set_data_selector($plotb);      
    // output the graph
    $object->draw($graph, $filename);   
}        
?>
