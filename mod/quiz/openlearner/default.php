<?php  // $Id: default.php,v 1.9.6.2 2008/08/18 12:48:10 tasoulis7 Exp $ 

// Default class for open_learner plugins                            
                                                               
// Doesn't do anything on it's own -- it needs to be extended.   
// This class displays quiz reports in Report Analysis mode. Because
// it is called from within /mod/quiz/openlearner.php you can assume
// that the page header and footer are taken care of.
 
// This file can refer to itself as openlearner.php to pass variables 
// to itself - all these will also be globally available.  You must 
// pass "id=$cm->id" or q=$quiz->id", "attempt=$attempt->uniqueid" and "mode=reportname".

// Included by ../openlearner.php

class openlearner_default_report {

    function display($quiz=NULL, $cm=NULL, $course=NULL) {     // This function just displays the report
        return true;
    }

    function print_header_and_tabs($cm, $course, $quiz, $attemptno=0, $reportmode="overview", $meta=""){
        global $CFG;
        
        // Define some strings
        $strquizzes = get_string("modulenameplural", "quiz");
        $strquiz  = get_string("modulename", "quiz");       
        /// Print the page header
        $navigation = build_navigation('', $cm); 
        print_header_simple(format_string($quiz->name), "", $navigation,'', $meta, true, update_module_button($cm->id, $course->id, $strquiz), navmenu($course, $cm));
        // Print the tabs    
        $currenttab = 'openlearner';
        $mode = $reportmode;
        require($CFG->dirroot . '/mod/quiz/tabs.php');
        // change cron value for quiz module
        $record = get_record_select('modules', "name = 'quiz'");
        if ($record->cron == 0 and $record->visible == 1) {
            $record->cron = 600;
            update_record('modules', $record); // Cron should check this module every 10 minutes
        }
        $course_context = get_context_instance(CONTEXT_COURSE, $course->id);
        if (has_capability('gradereport/grader:view', $course_context) && has_capability('moodle/grade:viewall', $course_context)) {
            echo '<div class="allcoursegrades"><a href="' . $CFG->wwwroot . '/grade/report/grader/index.php?id=' . $course->id . '">' 
                . get_string('seeallcoursegrades', 'grades') . '</a></div>';
        }
        
    }
}
?>
