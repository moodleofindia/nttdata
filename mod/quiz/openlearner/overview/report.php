<?php
/**
 * Overview of quiz report analysis displays all student attempts.
 *
 * @version $Id: report.php,v 1.9.6.2 2008/08/18 12:08:05 tasoulis7 Exp $
 * @author Martin Dougiamas, Tim Hunt and others.
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 * @package quiz
 * @subpackage openlearner
 */

require_once($CFG->libdir.'/tablelib.php');
require_once($CFG->dirroot .'/mod/quiz/pagelib.php');
require_once($CFG->dirroot .'/mod/quiz/lib.php');

class openlearner_report extends openlearner_default_report {

    /**
     * Display the report for all users except students.
     */
    function display($quiz, $cm, $course) {
        global $CFG, $db;

        // Define some strings
        $strtimeformat = get_string('strftimedatetime');
        $this->print_header_and_tabs($cm, $course, $quiz, $reportmode='overview');  
        
        $context = get_context_instance(CONTEXT_MODULE, $cm->id);
                    
        // Set table options
        $noattempts = optional_param('noattempts', 3, PARAM_INT);
        $pagesize = optional_param('pagesize', 30, PARAM_INT);
        
        // find out current groups mode
        $currentgroup = groups_get_activity_group($cm, true);
        
        $reporturl = $CFG->wwwroot.'/mod/quiz/openlearner.php?mode=overview';
        $reporturlwithoptions = $reporturl . '&amp;id=' . $cm->id . '&amp;noattempts=' . $noattempts . '&amp;pagesize=' . $pagesize;
        
        // Groups are being used
        if ($groupmode = groups_get_activity_groupmode($cm)) {   
            groups_print_activity_menu($cm, $reporturlwithoptions);       
        }
        
        // Print information on the number of existing attempts
        if ($strattemptnum = quiz_num_attempt_summary($quiz, $cm, true, $currentgroup)) {
            echo '<div class="quizattemptcounts">' . $strattemptnum . '</div>';
        }
        $nostudents = false;
        if (!$students = get_users_by_capability($context, 'mod/quiz:attempt','','','','','','',false)){
            notify(get_string('nostudentsyet'));
            $nostudents = true;
            $studentslist = '';
        } else {
            $studentslist = join(',',array_keys($students));
        }
        
        if (empty($currentgroup)) {
            // all users who can attempt quizzes
            $groupstudentslist = '';
            $allowedlist = $studentslist;
        } else {
            // all users who can attempt quizzes and who are in the currently selected group
            if (!$groupstudents = get_users_by_capability($context, 'mod/quiz:attempt','','','','',$currentgroup,'',false)){
                notify(get_string('nostudentsingroup'));
                $nostudents = true;
                $groupstudents = array();
            }
            $groupstudentslist = join(',', array_keys($groupstudents));
            $allowedlist = $groupstudentslist;
        }
        
        if (!$nostudents) {
        echo '<div class="quizattemptcounts"><p>'.get_string('allattempts', 'quiz_overview', NULL, $CFG->dirroot.'/mod/quiz/openlearner/lang/').'</p></div>';
        // Define table columns
        $tablecolumns = array('picture', 'fullname', 'timestart', 'timefinish', 'duration');
        $tableheaders = array('', get_string('name'), get_string('startedon', 'quiz'), get_string('timecompleted','quiz'), get_string('attemptduration', 'quiz'));

        if ($quiz->grade and $quiz->sumgrades) {
            $tablecolumns[] = 'sumgrades';
            $tableheaders[] = get_string('grade', 'quiz').'/'.$quiz->grade;
        }
        
        // Set up the table
        $table = new flexible_table('mod-quiz-openlearner-overview-report');
        $table->define_columns($tablecolumns);
        $table->define_headers($tableheaders);
        $table->define_baseurl($reporturlwithoptions);
        $table->sortable(true);
        $table->collapsible(true);
        $table->column_suppress('picture');
        $table->column_suppress('fullname');
        $table->column_class('picture', 'picture');
        $table->set_attribute('cellspacing', '2');
        $table->set_attribute('cellpadding', '6');
        $table->set_attribute('id', 'attempts');
        $table->set_attribute('class', 'generaltable generalbox');
        $table->setup();       
        $contextlists = get_related_contexts_string(get_context_instance(CONTEXT_COURSE, $course->id));

        // Construct the SQL
		
		//Naga commented because the below query is not listing all the case studies
        // $select = 'SELECT '.sql_concat('u.id', '\'#\'', $db->IfNull('qa.attempt', '0')).' AS uniqueid, '.
            // 'qa.uniqueid as attemptuniqueid, qa.id AS attempt, u.id AS userid, u.firstname, u.lastname, u.picture, '.
            // 'qa.sumgrades, qa.timefinish, qa.timestart, qa.timefinish - qa.timestart AS duration ';
        
		$select = 'SELECT '.sql_concat('u.id', '\'#\'', $db->IfNull('qa.attempt', '0')).' AS uniqueid, '.
            'qa.uniqueid as attemptuniqueid, qa.id AS attempt, u.id AS userid, u.firstname, u.lastname, u.picture, '.
            'qa.sumgrades, qa.timefinish, qa.timestart,
			if (qa.timefinish > qa.timestart, (qa.timefinish - qa.timestart), (qa.timestart - qa.timefinish)) AS duration ';
			
        // This part is the same for all cases - join users and quiz_attempts tables
            $from = 'FROM '.$CFG->prefix.'user u ';
            $from .= 'LEFT JOIN '.$CFG->prefix.'quiz_attempts qa ON qa.userid = u.id AND qa.quiz = '.$quiz->id;
            
        $where = ' WHERE u.id IN (' .$allowedlist. ') AND qa.preview = 0 AND qa.id IS NOT NULL';
        $countsql = 'SELECT COUNT(DISTINCT('.sql_concat('u.id', '\'#\'', 'COALESCE(qa.attempt, 0)').')) '.$from.$where;
                  
        // Add extra limits due to initials bar
        if($table->get_sql_where()) {
            $where .= ' AND '.$table->get_sql_where();
        }

        // Count the records NOW, before funky question grade sorting messes up $from
        if (!empty($countsql)) {
            $totalinitials = count_records_sql($countsql);
            
            if ($table->get_sql_where()) {
                $countsql .= ' AND '.$table->get_sql_where();
            }
            
            $total  = count_records_sql($countsql);
        }

        // Add extra limits due to sorting by question grade
        if($sort = $table->get_sql_sort()) {
            $sortparts    = explode(',', $sort);
            $newsort      = array();
            $questionsort = false;
            
            foreach($sortparts as $sortpart) {
                $sortpart = trim($sortpart);
                
                if(substr($sortpart, 0, 1) == '$') {
                    if(!$questionsort) {
                        $qid          = intval(substr($sortpart, 1));
                        $select .= ', grade ';
                        $from        .= ' LEFT JOIN '.$CFG->prefix.'question_sessions qns ON qns.attemptid = qa.id '.
                                        'LEFT JOIN '.$CFG->prefix.'question_states qs ON qs.id = qns.newgraded ';
                        $where       .= ' AND ('.sql_isnull('qns.questionid').' OR qns.questionid = '.$qid.')';
                        $newsort[]    = 'grade '.(strpos($sortpart, 'ASC')? 'ASC' : 'DESC');
                        $questionsort = true;
                    }
                } else {
                    $newsort[] = $sortpart;
                }
            }

            // Reconstruct the sort string
            $sort = ' ORDER BY '.implode(', ', $newsort);
        }

        // Fix some wired sorting
        if (empty($sort)) {
            $sort = ' ORDER BY uniqueid';
        }

        $table->pagesize($pagesize, $total);
                
        // Fetch the attempts
        if (!empty($from)) { // if we're in the site course and displaying no attempts, it makes no sense to do the query.
            $attempts = get_records_sql($select.$from.$where.$sort,$table->get_page_start(), $table->get_page_size());        
        } else {
            $attempts = array();
        }

        // Build table rows
        $table->initialbars($totalinitials>20);
        
        if(!empty($attempts) || !empty($noattempts)) {
            if ($attempts) {
                foreach ($attempts as $attempt) {

                    $picture = print_user_picture($attempt->userid, $course->id, $attempt->picture, false, true);
                    $userlink = '<a href="'.$CFG->wwwroot.'/user/view.php?id='.$attempt->userid.'&amp;course='.$course->id.'">'.fullname($attempt).'</a>';
                    $start = userdate($attempt->timestart, $strtimeformat);               
                    $row = array(
                        $picture,
                        $userlink,
                        empty($attempt->attempt) ? '-' : (empty($attempt->timefinish) ? $start : '<a href="'.$CFG->wwwroot.'/mod/quiz/openlearner.php?q='.$quiz->id.'&amp;attempt='.$attempt->attempt.'&amp;mode=textual'.'">'.$start.'</a>'),
                        empty($attempt->timefinish) ? '-' : '<a href="'.$CFG->wwwroot.'/mod/quiz/openlearner.php?q='.$quiz->id.'&amp;attempt='.$attempt->attempt.'&amp;mode=textual'.'">'.userdate($attempt->timefinish, $strtimeformat).'</a>',
                        empty($attempt->attempt) ? '-' : (empty($attempt->timefinish) ? get_string('unfinished', 'quiz') : format_time($attempt->duration)));
                 
                    if ($quiz->grade and $quiz->sumgrades) {
                        $row[] = $attempt->sumgrades === NULL ? '-' : round($attempt->sumgrades / $quiz->sumgrades * $quiz->grade, $quiz->decimalpoints);    
                    }   
                
                    $table->add_data($row);                
                }
            }
            
            echo '<div id="tablecontainer" align="center">';  
            // Print table
            $table->print_html();
            echo '</div>';
        } else {          
            $table->print_html();    
        }
          
        // Print display option
        echo '<div class="controls">';
        echo '<form id="options" action="' . $reporturl . '" method="get">';
        echo '<div>';
        echo '<input type="hidden" name="id" value="'.$cm->id.'" />';
        echo '<input type="hidden" name="q" value="'.$quiz->id.'" />';
        echo '<input type="hidden" name="noattempts" value="3" />';
        echo '<table id="overview-options" class="boxaligncenter">';
        echo '<tr align="left">';
        echo '<td><label for="pagesize">'.get_string('pagesize', 'quiz').'</label></td>';
        echo '<td><input type="text" id="pagesize" name="pagesize" size="3" value="'.$pagesize.'" /></td>';
        echo '</tr>';
        echo '<tr><td colspan="2" align="center">';
        echo '<input type="submit" value="'.get_string('go').'" />';
        echo '</td></tr></table>';
        echo '</div>';
        echo '</form>';
        echo '</div>';
        }
        return true;
    }

    /**
     * Display the report for students only.
     */
    function student_display($quiz, $cm, $course) {
        global $USER;
        
        $context = get_context_instance(CONTEXT_MODULE, $cm->id);      
        $this->print_header_and_tabs($cm, $course, $quiz, $reportmode='overview');      
        // Print quiz name
        print_heading(format_string($quiz->name),'center');       
        echo '<div class="quizinfo" align="center">';

        // Print information about number of attempts and grading method.
        if ($quiz->attempts > 1) {
            echo '<p>'.get_string('attemptsallowed', 'quiz').": $quiz->attempts</p>";
        }
        
        if ($quiz->attempts != 1) {
            echo '<p>'.get_string('grademethod', 'quiz').': '.quiz_get_grading_option_name($quiz->grademethod).'</p>';
        }

        // Print information about timings.
        $timenow = time();
        $available = ($quiz->timeopen < $timenow and ($timenow < $quiz->timeclose or !$quiz->timeclose));
        
        if ($available) {
            if ($quiz->timelimit) {
                echo '<p>'.get_string('quiztimelimit','quiz', format_time($quiz->timelimit * 60)).'</p>';
            }
            
            if ($quiz->timeopen) {
                echo '<p>', get_string('quizopens', 'quiz'), ': ', userdate($quiz->timeopen), '</p>';
            }
            
            if ($quiz->timeclose) {
                echo '<p>', get_string('quizcloses', 'quiz'), ': ', userdate($quiz->timeclose), '</p>';
            }
        } else if ($timenow < $quiz->timeopen) {
            echo '<p>'.get_string('quiznotavailable', 'quiz', userdate($quiz->timeopen)).'</p>';
        } else {
            echo '<p>'.get_string('quizclosed', 'quiz', userdate($quiz->timeclose)).'</p>';
        }
        
        echo '</div>';        
        // Get this user's attempts.
        $attempts = quiz_get_user_attempts($quiz->id, $USER->id);
        $unfinished = false;
        
        if ($unfinishedattempt = quiz_get_user_attempt_unfinished($quiz->id, $USER->id)) {
            $attempts[] = $unfinishedattempt;
            $unfinished = true;
        }
        
        $numattempts = count($attempts);
        $mygrade = quiz_get_best_grade($quiz, $USER->id);
        // Get some strings.
        $strattempt       = get_string('attempt', 'quiz');
        $strtimetaken     = get_string('timetaken', 'quiz');
        $strtimecompleted = get_string('timecompleted', 'quiz');
        $strgrade         = get_string('grade');
echo'<center>*Click on an attempt to view detailed reports and graph.</center>';
        // Print table with existing attempts
        if ($attempts) {
         
            $gradecolumn = $quiz->grade && $quiz->sumgrades;        
            // prepare table header
            $table->head = array($strattempt, $strtimecompleted);
            $table->align = array('center', 'center');
            $table->size = array('', '');           
            $table->head[] = $strtimetaken;
            $table->align[] = 'center';
            $table->size[] = '';
            
            if ($gradecolumn) {
                $table->head[] = "$strgrade / $quiz->grade";
                $table->align[] = 'center';
                $table->size[] = '';
            }
            
            // One row for each attempt
            foreach ($attempts as $attempt) {
                if (((time() - $attempt->timefinish) < 120) || $attempt->timefinish==0) {
                    $quiz_state_mask = QUIZ_REVIEW_IMMEDIATELY;
                } else if (!$quiz->timeclose or time() < $quiz->timeclose) {
                    $quiz_state_mask = QUIZ_REVIEW_OPEN;
                } else {
                    $quiz_state_mask = QUIZ_REVIEW_CLOSED;
                }
                
                $openlearner = $attempt->timefinish && ($quiz->review & $quiz_state_mask & QUIZ_REVIEW_OPENLEARNER);        
                $row = array();          
                // Add the attempt number, making it a link.
                $linktext = $attempt->attempt;
                
                if (!$openlearner) {
                    $row[] = $linktext;
                } else {
                    $url = "openlearner.php?q=$quiz->id&amp;attempt=$attempt->id&amp;mode=textual";
                    
                    if ($quiz->popup) {
                        $windowoptions = "left=0, top=0, channelmode=yes, fullscreen=yes, scrollbars=yes, resizeable=no, directories=no, toolbar=no, titlebar=no, location=no, status=no, menubar=no";
                        return link_to_popup_window('/mod/quiz/' . $url, 'quizpopup', $linktext, '+window.screen.height+', '+window.screen.width+', '', $windowoptions, true);
                    } else {
                        $row[] =  "<a href='$url'>$linktext</a>";
                    }
                }

                // prepare strings for time taken and date completed
                $timetaken = '';
                $datecompleted = '';
                
                if ($attempt->timefinish > 0) {
                    // attempt has finished
                    $timetaken = format_time($attempt->timefinish - $attempt->timestart);
                    $datecompleted = userdate($attempt->timefinish);
                    $attemptgrade = quiz_rescale_grade($attempt->sumgrades, $quiz);
                } else if ($available) {
                    // The attempt is still in progress.
                    $timetaken = format_time(time() - $attempt->timestart);
                    $datecompleted = '-';
                    $attemptgrade = '-';
                } else if ($quiz->timeclose) {
                    // The attempt was not completed but is also not available any more because the quiz is closed.
                    $timetaken = format_time($quiz->timeclose - $attempt->timestart);
                    $datecompleted = userdate($quiz->timeclose);
                    $attemptgrade = '-';
                } else {
                    // Something wheird happened.
                    $timetaken = '-';
                    $datecompleted = '-';
                    $attemptgrade = '-';
                }
                
                $row[] = $datecompleted;
                $row[] = $timetaken;
                
                if ($gradecolumn) {
                    $row[] = $attemptgrade;      
                }
                
                $table->data[] = $row;
                
            } 
            
            print_table($table);
        } else {
            echo "\n";
            print_heading(format_string(get_string('noattemptstoshow', 'quiz')));  
        }

       return (true);        
    }   
}   
?>
