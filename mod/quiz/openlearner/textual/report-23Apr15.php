<?php
/**
 * Textual presentation of quiz report analysis displays in textual format
 * a student's performance in an quiz attempt.
 *
 * @version $Id: report.php,v 1.9.6.2 2008/08/26 12:35:24 tasoulis7 Exp $
 * @author Martin Dougiamas, Tim Hunt and others.
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 * @package quiz
 * @subpackage openlearner
 */

class openlearner_report extends openlearner_default_report {

    /**
     * Display the report.
     */
    function display($quiz, $cm, $course) {
        global $CFG;
        
        // A particular attempt ID
        $attemptno = required_param('attempt', PARAM_INT);    

        if (!$attempt = get_record('quiz_attempts', 'uniqueid', $attemptno)) {
            error('No such attempt ID exists');
        }
        
        if (!count_records('question_sessions', 'attemptid', $attempt->uniqueid)) {
            // this question has not yet been upgraded to the new model
            quiz_upgrade_states($attempt);
        }
        
        // Only print headers if not asked to download data
        if (!$download = optional_param('download', NULL)) {
            $this->print_header_and_tabs($cm, $course, $quiz, $attemptno, $reportmode='textual');
        }

        // Define some strings
        $strtimeformat = get_string('strftimedatetime');
        $reporturl = $CFG->wwwroot.'/mod/quiz/openlearner.php';
        $userid = get_field('quiz_attempts', 'userid', 'id', $attempt->uniqueid);
        $login = get_field('user', 'username', 'id', $userid);
        $firstname = get_field('user', 'firstname', 'id', $userid);
        $lastname = get_field('user', 'lastname', 'id', $userid);         
        $pagelist = quiz_questions_in_quiz($attempt->layout);
        $pagequestions = explode(',', $pagelist); 
                  
        // load the questions types needed by page 
        $sql = 'SELECT DISTINCT q.qtype FROM '.$CFG->prefix.'question q, '.$CFG->prefix.'quiz_question_instances i WHERE i.quiz = '.
                $quiz->id.' AND q.id = i.question AND q.id in ('.$pagelist.')';
                
        if (!$qtypes = get_records_sql($sql)) {
            error('No questions types found');
        }   
        
        foreach ($qtypes as $currobj) {      
            $currobj->length = 0;
        }           
       
        // load the questions categories needed by page 
        $sql = 'SELECT name, info FROM '.$CFG->prefix.'question_categories WHERE id in ('.'SELECT DISTINCT q.category FROM '.
               $CFG->prefix.'question q, '.$CFG->prefix.'quiz_question_instances i WHERE i.quiz = '.$quiz->id.
               ' AND q.id = i.question AND q.id in ('.$pagelist.'))';
               
        if (!$qcategories = get_records_sql($sql)) {
            error('No questions categories found');
        }      
           
        foreach ($qcategories  as $currobj) {
            $currobj->qtype = array();
            $currobj->known = array();
            $currobj->misconc = array();
            $currobj->problem = array();
            
            foreach ($qtypes as $type) {      
                $currobj->qtype[$type->qtype] = 0;
            }
            
            $currobj->length = $currobj->grade = $currobj->maxgrade = $currobj->percentage = 0; 
            $currobj->right = $currobj->partial = $currobj->wrong = $currobj->noanswer = 0;              
        } 
         
        // load all the questions needed by page 
        $sql = "SELECT q.*, i.grade AS maxgrade, i.id AS instance".
               "  FROM {$CFG->prefix}question q,".
               "       {$CFG->prefix}quiz_question_instances i".
               " WHERE i.quiz = '$quiz->id' AND q.id = i.question".
               "   AND q.id IN ($pagelist)";
               
        if (!$questions = get_records_sql($sql)) {
            error('No questions found');
        }

        // Load the question options
        if (!get_question_options($questions)) {
            error('Could not load question options');
        }

        // Restore the question sessions to their most recent states
        // creating new sessions where required
        if (!$states = get_question_states($questions, $quiz, $attempt)) {
            error('Could not restore question sessions');
        }
        
        $grade = quiz_rescale_grade($attempt->sumgrades, $quiz);
        $questionsno = count_records('question_sessions','attemptid',$attempt->uniqueid); 
        $correct = $partial = $false = $penalty = $multiple = $noanswer = 0;     
        // create an object which holds data about attempt's final grade 
        $a = new stdClass;
        $a->grade = round($grade,$quiz->decimalpoints);
        $a->maxgrade = $quiz->grade;
        $percentage = round(($attempt->sumgrades/$quiz->sumgrades)*100, $quiz->decimalpoints);   

        
        // get all attempts for this student
        $attempts = quiz_get_user_attempts($quiz->id, $userid);
        $sum = $count = $maximum = 0;
        $minimum = $quiz->sumgrades;
        
        foreach ($attempts as $attemptitem) {    
            $sum += round(($attemptitem->sumgrades/$quiz->sumgrades)*100, $quiz->decimalpoints);
            ++$count;
            
            if ($attemptitem->sumgrades < $minimum) {
                $minimum = $attemptitem->sumgrades;
            }
            if ($attemptitem->sumgrades > $maximum) {
                $maximum = $attemptitem->sumgrades;
            }
        }
       
        // calculate the average score and the attempts with the minimum and maximun score
        $average = round($sum/$count, $quiz->decimalpoints);
        $minimum = round(($minimum/$quiz->sumgrades)*100, $quiz->decimalpoints);
        $maximum = round(($maximum/$quiz->sumgrades)*100, $quiz->decimalpoints);
        
        // Calculate results for each question category and type
        foreach ($pagequestions as $i) {          
            if ($questions[$i]->qtype == 'description') {
                --$questionsno;
                continue;
            }
            
            ++$qtypes[$questions[$i]->qtype]->length;
            $questions[$i]->category = get_field('question_categories', 'name', 'id', $questions[$i]->category);
            ++$qcategories[$questions[$i]->category]->length;
            $qcategories[$questions[$i]->category]->grade += $states[$i]->last_graded->raw_grade;
            $qcategories[$questions[$i]->category]->maxgrade += $questions[$i]->maxgrade;
            ++$qcategories[$questions[$i]->category]->qtype[$questions[$i]->qtype]; 
            
            // check if the answer is correct   
            if ($states[$i]->last_graded->raw_grade >= $questions[$i]->maxgrade/1.01) { // We divide by 1.01 so that rounding errors dont matter.
                ++$correct;
                $qpenalty = ($questions[$i]->maxgrade) - (round($states[$i]->last_graded->grade, $quiz->decimalpoints));
                $penalty += $qpenalty;
                ++$qcategories[$questions[$i]->category]->right;
                
                if ($quiz->optionflags & QUESTION_ADAPTIVE and $qpenalty) {
                    $qcategories[$questions[$i]->category]->misconc[] = $questions[$i]->name;
                } else {      
                    $qcategories[$questions[$i]->category]->known[] = $questions[$i]->name;
                }                  
            } else if ($states[$i]->last_graded->raw_grade > 0) { // check if the answer is partially correct 
                ++$partial;
                ++$qcategories[$questions[$i]->category]->partial;
                $qcategories[$questions[$i]->category]->misconc[] = $questions[$i]->name;
            } else { // check if the answer is wrong
                ++$false;
                ++$qcategories[$questions[$i]->category]->wrong;
                $qcategories[$questions[$i]->category]->problem[] = $questions[$i]->name;
                
                // Count the not answered questions depending on their question type
                $sql = 'SELECT id,answer FROM '.$CFG->prefix.'question_states'.' WHERE attempt = '.$attempt->uniqueid.'  AND question = '.
                        $questions[$i]->id.'  AND seq_number != 0 ORDER BY id DESC';
                        
                $answer = get_record_sql($sql,true);
                
                if ($answer) {        
                    if (($questions[$i]->qtype == 'match') or ($questions[$i]->qtype == 'randomsamatch') or ($questions[$i]->qtype == 'multianswer')) {
                        $answers = explode(',',$answer->answer);
                        $noanswerflag = 0;  
                                   
                        foreach ($answers as $value) {
                            $items = explode('-',$value);
                            if ((($questions[$i]->qtype == 'match') or ($questions[$i]->qtype == 'randomsamatch')) and ($items[1] != '0')) {
                                ++$noanswerflag;
                                break;
                            } else if (($questions[$i]->qtype == 'multianswer') and ($items[1] != '')) {
                                ++$noanswerflag;
                                break;
                            }      
                        }
                        
                        if ($noanswerflag == 0) {
                            ++$noanswer; 
                            $qcategories[$questions[$i]->category]->noanswer++;     
                        }             
                    } else if ($questions[$i]->qtype == 'multichoice') {
                        $answers = explode(':',$answer->answer); 
                                 
                        if ($answers[1] == '') {
                            ++$noanswer; 
                            $qcategories[$questions[$i]->category]->noanswer++;     
                        }
                    } else if ($questions[$i]->qtype == 'calculated') {
                        $answers = explode('-',$answer->answer); 
                                
                        if ($answers[1] == '') {
                            ++$noanswer; 
                            $qcategories[$questions[$i]->category]->noanswer++;     
                        }                   
                    } else {
                        if ($answer->answer == '') { 
                            ++$noanswer;  
                            $qcategories[$questions[$i]->category]->noanswer++; 
                        }
                    }        
                }
            }
            
            if ($states[$i]->last_graded->raw_grade > $states[$i]->last_graded->grade){
               ++$multiple;       
            }
        }  
        
        unset($pagequestions);
        // calculate score for each category
        foreach ($qcategories  as $currobj) {    
            $currobj->percentage = round(($currobj->grade/$currobj->maxgrade)*100, $quiz->decimalpoints);
        }         
        
        // Now check if asked download of data
        if ($download) {
            $filename = clean_filename("$course->shortname ".format_string($quiz->name,true)." $login ".$attempt->attempt);
            $sort = '';
        } 
                 
        // print textual report
        $html = '<table border="0" cellpadding="1" cellspacing="0" width="70%" align = "center"><tr><td >&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>'.
        '<td>&nbsp;</td></tr><tr><td bgcolor="#7A9EFA" align="left">'.get_string('username').':</td><td align="left">'.$login.'</td>'.
        '</tr><tr><td bgcolor="#7A9EFA" align="left">'.get_string('fullname', 'report_openlearner').'</td><td align="left">'.
        $firstname.' '.$lastname.'</td></tr><tr><td bgcolor="#7A9EFA" align="left">'.get_string('quizname', 'report_openlearner').
        '</td><td align="left">'.$quiz->name.'</td></tr><tr><td bgcolor="#7A9EFA" align="left">'.
        get_string('attemptno', 'report_openlearner').'</td><td align="left">'.$attempt->attempt.
        '</td></tr><tr><td bgcolor="#7A9EFA">&nbsp;</td><td  align="left">'.get_string('startedon', 'quiz').' '.
        userdate($attempt->timestart, $strtimeformat).' and '.get_string('timecompleted','quiz').' on '.userdate($attempt->timefinish, $strtimeformat).
        '</td></tr><tr><td bgcolor="#7A9EFA" >&nbsp;</td><td>&nbsp;</td></tr><tr><td bgcolor="#7A9EFA" align="left">'.get_string('total', 'report_openlearner').
        '</td><td align="left">'.get_string('outof', 'quiz', $a).' ('.$percentage.'%)'.'</td></tr>';
        
     
        
        $html .= '<tr><td bgcolor="#7A9EFA" align="left">'.get_string('average', 'report_openlearner').'</td>'.'<td align="left">'.$average.
        '%</td></tr><tr><td bgcolor="#7A9EFA" align="left">'.get_string('minimum', 'report_openlearner').'</td><td align="left">'.
        $minimum.'%</td></tr><tr><td bgcolor="#7A9EFA" align="left">'.get_string('maximum', 'report_openlearner').'</td><td align="left">'.
        $maximum.'%</td></tr><tr><td bgcolor="#7A9EFA" align="left">'.
        get_string('adaptivetrue', 'report_openlearner').'</td>';    
         
        if ($quiz->optionflags & QUESTION_ADAPTIVE and $quiz->penaltyscheme) {
            $html .= '<td  align="left">'.get_string('true', 'quiz').' ('.get_string('penalty', 'report_openlearner').
                     ' '.$penalty.')</td></tr>';
        } else {
            $html .= '<td  align="left">'.get_string('incorrect', 'quiz').'</td></tr>';
        }      
        
        $html .= '<tr><td bgcolor="#7A9EFA" align="left">'.get_string('questionsposed', 'report_openlearner').'</td><td align="left">'.
                 $questionsno.'</td></tr><tr><td bgcolor="#7A9EFA" align="left">'.get_string('correct', 'quiz').':</td><td align="left">'.
                 $correct;
        
        if ($quiz->optionflags & QUESTION_ADAPTIVE) {
            $html .= ' ('.get_string('multiple', 'report_openlearner').' '.$multiple.')'; 
        }      
        
        $html .= '</td></tr><tr><td bgcolor="#7A9EFA" align="left">'.get_string('partiallycorrect', 'quiz').':</td><td align="left">'.
        $partial.'</td></tr><tr><td bgcolor="#7A9EFA" align="left">'.get_string('incorrect', 'quiz').':</td><td align="left">'.$false.' ('.
        get_string('notanswers', 'report_openlearner').' '.$noanswer.')</td></tr>';
        


        $html .= '<tr></tr><tr><td></td><td>&nbsp;</td></tr><tr><td style="font-size:12pt;font-weight: bold" align="left">'.get_string('analysis', 'report_openlearner').
                 '</td><td>&nbsp;</td></tr>';
        
        foreach ($qcategories  as $currobj) {
             $html .= '<tr><td align="left">'.
             '</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td style="font-weight: bold;color:#F3861B" align="left">'.get_string('category', 'quiz').': '.$currobj->name.
             '<td>&nbsp;</td><td>&nbsp;</td><td style="font-weight: bold;color:#F3861B" align="right">'.get_string('score', 'report_openlearner').': '.
             $currobj->percentage.'% </td></tr>';
             
             if ($currobj->info != '') {
                 $html .= '<tr><td align="left">('.$currobj->info.')</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>'; 
             }
             
             $html .= '<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td align="left">'.
                      get_string('questionstotal', 'report_openlearner').' '.$currobj->length.'</td><td>&nbsp;</td><td>&nbsp;</td><td align="right">'.
                      get_string('correct', 'quiz').': '.$currobj->right.' '.get_string('partiallycorrect', 'quiz').': '.$currobj->partial.' '.
                      get_string('incorrect', 'quiz').': '.$currobj->wrong. ' ('.get_string('notanswers', 'report_openlearner').
                      ' '.$currobj->noanswer.')</td></tr><tr><td align="left">'.get_string('types', 'report_openlearner').
                      '</td><td>&nbsp;</td><td>&nbsp;</td>';
             
             foreach ($currobj->qtype as $key => $value) {
                 if ($value != 0) {
                     $html .=  '<td align="right">'.get_string($key, 'quiz').': '.$value.' </td></tr><td>&nbsp;</td><td>&nbsp;</td>'.
                               '<td>&nbsp;</td>';       
                 }  
             }
             
             $html .= '<td>&nbsp;</td></tr>'; 
             

                                 
             if (count($currobj->misconc) != 0) {
                  $html .= '<tr><td align="left">'.get_string('misconceptions', 'report_openlearner').
                           ': </td><td>&nbsp;</td><td>&nbsp;</td>';
                                  
                  foreach ($currobj->misconc  as $key => $value) {
                      $html .= '<td align="right">'.($key+1).'. '.$value.' </td></tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>';
                  }  
                  
                  $html .= '<td>&nbsp;</td></tr>'; 
             }
                       
                			 
        }
       unset($qcategories); 
        $html .= '</table>';
        
		
		  $html2 .= '<html><table>';

		foreach ($qcategories  as $currobj) {
             $html2 .= '
			 <th>'.get_string('category', 'quiz').': '.$currobj->name.'</th>';
		
  			 
        }
	
		
		
        if (!$download) {
           echo $html;
            echo '<table class="boxaligncenter"><tr>';
            $options = array();
            $options['q'] = $quiz->id;
            $options['attempt'] = $attempt->uniqueid;
            $options['mode'] = 'textual';
            $options['sesskey'] = sesskey();
            $options['noheader'] = 'yes';
            echo '<td>';
            $options['download'] = 'PDF';
            print_single_button($reporturl, $options, get_string('downloadpdf', 'report_openlearner'));
            echo '</td><td>';
            helpbutton('textualdownload', get_string('textualdownload', 'report_openlearner'), 'quiz');
            echo '</td></tr></table>';
            
        } else { // download report in PDF format
            require_once($CFG->libdir.'/pdflib.php');
            
            $filename .= '.pdf';
            // create new PDF document
            $pdf = new pdf();
            // set document information
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->SetAuthor("$firstname $lastname");
            $pdf->SetTitle("$quiz->name attempt #$attempt->attempt");
            $pdf->SetSubject(get_string('textual', 'report_openlearner'));
            $pdf->SetKeywords('TCPDF, PDF, quiz, textual, report');
            // set default header data
            $pdf->SetHeaderData('', 0, $course->fullname, '');
            // set header and footer fonts
            $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
            $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
            //set margins
            $pdf->SetMargins(5, 20, 5); 
            $pdf->SetHeaderMargin(PDF_MARGIN_HEADER); 
            $pdf->SetFooterMargin(PDF_MARGIN_FOOTER); 
            //set auto page breaks
            $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
            //set image scale factor
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
            //set some language-dependent strings
            $a = 1;
            $pdf->setLanguageArray($a); 
            //initialize document
            $pdf->AliasNbPages();
            // add a page
            $pdf->AddPage();
            // set font
            $pdf->SetFont('FreeSerif', '', 11);
            // output the HTML content
            $pdf->writeHTML($html);
            //Close and output PDF document
            $pdf->Output($filename,'D'); 
       }
        
       return (true);
    }  
}   
?>
