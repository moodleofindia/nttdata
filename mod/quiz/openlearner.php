<?php  // $Id: openlearner.php,v 1.9.6.2 2008/08/28 20:51:00 tasoulis7 Exp $

    // This script uses installed openlearner plugin to print quiz reports in Quiz Report Analysis

    require_once("../../config.php");
    require_once("locallib.php");

    $id = optional_param('id',0,PARAM_INT);    // Course Module ID, or
    $q = optional_param('q',0,PARAM_INT);     // quiz ID

    $mode = optional_param('mode', 'overview', PARAM_ALPHA);        

    if ($id) {
        if (! $cm = get_coursemodule_from_id('quiz', $id)) {
            error("There is no coursemodule with id $id");
        }

        if (! $course = get_record("course", "id", $cm->course)) {
            error("Course is misconfigured");
        }

        if (! $quiz = get_record("quiz", "id", $cm->instance)) {
            error("The quiz with id $cm->instance corresponding to this coursemodule $id is missing");
        }
    } else {
        if (! $quiz = get_record("quiz", "id", $q)) {
            error("There is no quiz with id $q");
        }
        
        if (! $course = get_record("course", "id", $quiz->course)) {
            error("The course with id $quiz->course that the quiz with id $q belongs to is missing");
        }
        
        if (! $cm = get_coursemodule_from_instance("quiz", $quiz->id, $course->id)) {
            error("The course module for the quiz with id $q is missing");
        }
    }

    require_login($course->id, false);
    $context = get_context_instance(CONTEXT_MODULE, $cm->id);
    
    // if no questions have been set up yet redirect to edit.php
    if (!$quiz->questions and has_capability('mod/quiz:manage', $context)) {
        redirect('edit.php?quizid='.$quiz->id);
    }

    // Upgrade any attempts that have not yet been upgraded to the 
    // Moodle 1.5 model (they will not yet have the timestamp set)
    if ($attempts = get_records_sql("SELECT a.*"."  FROM {$CFG->prefix}quiz_attempts a, {$CFG->prefix}question_states s".
                                    " WHERE a.quiz = '$quiz->id' AND s.attempt = a.uniqueid AND s.timestamp = 0")) {
        foreach ($attempts as $attempt) {
            quiz_upgrade_states($attempt);
        }
    }

    add_to_log($course->id, "quiz", "quizreport", "openlearner.php?id=$cm->id", "$quiz->name", "$cm->id");
    // Open the selected quiz report analysis and display it
    $mode = clean_param($mode, PARAM_SAFEDIR);

    if (! is_readable("openlearner/$mode/report.php")) {
        error("Open Learner Repor44t not known ($mode)");
    }

    include("openlearner/default.php");     // Parent class
    include("openlearner/$mode/report.php");

    $report = new openlearner_report();

    if ((has_capability('mod/quiz:attempt', $context)) and (! has_capability('mod/quiz:viewreports', $context)) and ($mode == 'overview')) { 
        if (! $report->student_display($quiz, $cm, $course)) {      // Run the report for students only
            error("Error occurred during pre-processing!");
        }
    } else {
        if (! $report->display($quiz, $cm, $course)) {              // Run the report for all other users
            error("Error occurred during pre-processing!");
        }
    }
    
    // Print footer
    print_footer($course);

?>
