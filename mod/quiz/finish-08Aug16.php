<?php  // $Id: view.php,v 1.124.2.19 2009/09/30 10:58:04 tjhunt Exp $

// This page prints a particular instance of quiz


    require_once("../../config.php");
    require_once($CFG->libdir.'/blocklib.php');
    require_once($CFG->libdir.'/gradelib.php');
    require_once($CFG->dirroot.'/mod/quiz/locallib.php');
    require_once($CFG->dirroot.'/mod/quiz/pagelib.php');

    $id   = optional_param('id', 0, PARAM_INT); // Course Module ID, or
    $q    = optional_param('q',  0, PARAM_INT);  // quiz ID
    $edit = optional_param('edit', -1, PARAM_BOOL);


    if ($id) {
        if (! $cm = get_coursemodule_from_id('quiz', $id)) {
            error("There is no coursemodule with id $id");
        }
        if (! $course = get_record("course", "id", $cm->course)) {
            error("Course is misconfigured");
        }
        if (! $quiz = get_record("quiz", "id", $cm->instance)) {
            error("The quiz with id $cm->instance corresponding to this coursemodule $id is missing");
        }
    } else {
        if (! $quiz = get_record("quiz", "id", $q)) {
            error("There is no quiz with id $q");
        }
        if (! $course = get_record("course", "id", $quiz->course)) {
            error("The course with id $quiz->course that the quiz with id $q belongs to is missing");
        }
        if (! $cm = get_coursemodule_from_instance("quiz", $quiz->id, $course->id)) {
            error("The course module for the quiz with id $q is missing");
        }
    }
    ?>
  <html>
  <head>
  <link href="<?php echo $CFG->themewww .'/'. current_theme() ?>/aardvark.css" rel="stylesheet" type="text/css">
  <link href="<?php echo $CFG->themewww .'/default' ?>/styles_layout.css" rel="stylesheet" type="text/css">
  <link href="<?php echo $CFG->themewww .'/default' ?>/styles_fonts.css" rel="stylesheet" type="text/css">
  <link href="<?php echo $CFG->themewww .'/default' ?>/styles_color.css" rel="stylesheet" type="text/css">

  <script type='text/javascript' src='//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'></script>

  <style type='text/css'>
    .blink {
    color:2266cc;
}

  </style>



<script type='text/javascript'>
$(window).load(function(){

(function($) {
    $.fn.blink = function(options) {
        var defaults = {
            delay: 500
        };
        var options = $.extend(defaults, options);

        return this.each(function() {
            var obj = $(this);
            setInterval(function() {
                if ($(obj).css("visibility") == "visible") {
                    $(obj).css('visibility', 'hidden');
                }
                else {
                    $(obj).css('visibility', 'visible');
                }
            }, options.delay);
        });
    }
}(jQuery))


$(document).ready(function() {
    $('.blink').blink(); // default is 500ms blink interval.
    $('.blink_second').blink({
        delay: 100
    }); // causes a 100ms blink interval.
    $('.blink_third').blink({
        delay: 1500
    }); // causes a 1500ms blink interval.
});

});

</script>

  </head>
  <body>
    <?php

    print_heading(format_string($quiz->name));
    $mygrade = quiz_get_best_grade($quiz, $USER->id);
    echo '<table width="100%" border="0"><tbody><tr><td width="30%" valign="top" style="text-align: center;"><img src="https://lmsqa.portal.nttdatainc.com/file.php/1/mascot1.PNG" alt="Mascot 1" title="Mascot 1" border="0" hspace="0" vspace="0" width="143" height="193" /><br /></td><td width="70%" valign="top"><div class="blink" id="blink"><p  style="text-align: center;"><p><font face="arial, helvetica, sans-serif" size="4">'.quiz_feedback_for_grade($mygrade, $quiz->id).'</p></div><br/><p  style="text-align: left;"><i><font face="arial, helvetica, sans-serif" size="3">To know more please enroll for <br/> <a href="https://lmsqa.portal.nttdatainc.com/course/view.php?id=8529" target=_blank>Vision and Values Week 2015</a> program.</font></i></p></td></tr></tbody></table>';

    $_SESSION["popupquiz"]=0;
?>
</body>
</html>
