<?php  // $Id: view.php,v 1.61 2007/01/27 19:14:23 skodak Exp $

    require_once("../../config.php");
    require_once("lib.php");
 
    $id = optional_param('id', 0, PARAM_INT);    // Course Module ID
    $r  = optional_param('r', 0, PARAM_INT);  // Resource

    if ($r) {  // Two ways to specify the resource
        if (! $resource = get_record('resource', 'id', $r)) {
            error('Resource ID was incorrect');
        }

        if (! $cm = get_coursemodule_from_instance('resource', $resource->id, $resource->course)) {
            error('Course Module ID was incorrect');
        }

    } else if ($id) {
        if (! $cm = get_coursemodule_from_id('resource', $id)) {
            error('Course Module ID was incorrect');
        }

        if (! $resource = get_record('resource', 'id', $cm->instance)) {
            error('Resource ID was incorrect');
        }
    } else {
        error('No valid parameters!!');
    }

    if (! $course = get_record('course', 'id', $cm->course)) {
        error('Incorrect course id');
    }

    require_course_login($course, true, $cm);

    require ($CFG->dirroot.'/mod/resource/type/'.$resource->type.'/resource.class.php');
    $resourceclass = 'resource_'.$resource->type;
    $resourceinstance = new $resourceclass($cm->id);

    $resourceinstance->display();

$coursedetails ="select automail from mdl_course where id=$course->id and automail=1";
$course_automail=get_record_sql($coursedetails);
$checkgrade=get_record_sql("select * from mdl_temp where username='$USER->id' and status='N'");
if($course_automail && $checkgrade)
{
?>
<script type="text/javascript">
	$(document).ready(function(){
	var rootfolder="<?php echo $CFG->wwwroot; ?>";
	$.post(rootfolder+"/coursecompletion/helloworld.php",{"userid":"<?php echo $USER->id; ?>"},function(data){
	//alert(data);
	});

	});
</script>
<?php 
} 
?>