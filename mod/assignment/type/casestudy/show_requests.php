<?php

    require("../../../../config.php");
	require_login();

    // Allow access only to admin
	
   // $rowusername=get_record('user','id',$USER->id);

	    $id   = required_param('id', PARAM_INT);          // Course module ID

    if (! $cm = get_coursemodule_from_id('assignment', $id)) {
        error("Course Module ID was incorrect");
    }

    if (! $assignment = get_record("assignment", "id", $cm->instance)) {
        error("assignment ID was incorrect");
    }

    if (! $course = get_record("course", "id", $assignment->course)) {
        error("Course is misconfigured");
    }

    require_login($course->id, false, $cm);

    if (has_capability('mod/assignment:grade', get_context_instance(CONTEXT_MODULE, $cm->id))) {
	$sql = "select t.id aid,s.id as sid,u.id as uid, t.name,u.username,u.firstname,u.lastname,u.country,u.BU,s.submissioncomment,s.timemodified,s.casestudy,s.completeddate,s.grade
from lms.mdl_assignment  t join mdl_assignment_submissions s
	on t.id=s.assignment and t.assignmenttype='casestudy' and s.grade<=100
	join mdl_user u on u.id=s.userid";

    
    $navlinks = array();
    $navlinks[] = array('name' => 'Approve Casestudy', 'link' => ".", 'type' => 'misc');
    $navigation = build_navigation($navlinks);

    $rows = get_records_sql($sql);
    $output = '<table class="generaltable boxaligncenter" width="100%" cellspacing="1" cellpadding="5">';
    $output .= '<tr>
                <th align="left">Assignment</th>
				<th align="left">Portal ID</th>
				<th align="left">Employee Name</th>              				
				<th align="left">Country</th>
				<th align="left">BU</th>
				<th align="left">Comment</th>
                <th align="left">Submitted Date</th>
                <th align="left">Action</th>
                </tr>';
    
    if(!empty($rows)){
        foreach ($rows as $row){
			//$group_name=groups_get_group_name($row->coursegroup);
            $url = $CFG->wwwroot.'/mod/assignment/type/casestudy/process_request.php?aid='.$row->aid.'&uid='.$row->uid.'&sid='.$row->sid.'&cid='.$course->id.'&id='.$id;
        	$output .= '<tr>';
			$output .= "<td>$row->name</td>";
			$output .= "<td>$row->username</td>";
            $output .= "<td>$row->firstname $row->lastname</td>";
			$output .= "<td>$row->country</td>";      
			$output .= "<td>$row->bu</td>";
			$output .= "<td>$row->casestudy</td>";

			$output .= "<td>".userdate($row->completeddate, '%d %b %y')."</td>";
			if($row->grade <0){

            $output .= "<td><a href='{$url}&action=approve'>Approve</a> ";
			$output .= "&nbsp;&nbsp;<a href='{$url}&action=reject'>Reject</a></td>";
			}
			else
			{
			$output .= "<td>Approved</td> ";
			}
			

          
            $output .= '</tr>';    
    	}
    }    
    $output .= '</table>';
    
    print_header('Approve Casestudy','Approve Casestudy', $navigation);
echo "<p>Your employee(s) below are requesting to be graded for the assignment.  Please review and approve or reject the casestudy."; 

 print_box($output);
 }
 else
 {
 error('Currently you have no rights to approve reject casestudy.');
 }
   
    print_footer();
?>