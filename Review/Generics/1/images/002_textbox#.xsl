﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:msxsl="urn:schemas-microsoft-com:xslt"
				xmlns:websoft="http://www.websoft.ru"
				version="1.0">
<!--
'*	002_textbox#.xsl
'*	Copyright (c) Websoft, 2007.  All rights reserved.
-->
<xsl:output method="xml" encoding="utf-8" omit-xml-declaration="yes"/>
<xsl:param name="imagesFolder"></xsl:param>
<!-- profiles definition start -->
<xsl:variable name="profiles">
	<corner_top_left>
		<src>textbox_tl.gif</src>
		<width>13</width>
		<height>13</height>
	</corner_top_left>
	<corner_top_right>
		<src>textbox_tr.gif</src>
		<width>13</width>
		<height>13</height>
	</corner_top_right>
	<corner_bottom_left>
		<src>textbox_bl.gif</src>
		<width>13</width>
		<height>13</height>
	</corner_bottom_left>
	<corner_bottom_right>
		<src>textbox_br.gif</src>
		<width>13</width>
		<height>13</height>
	</corner_bottom_right>
	<background_top_edge><src>textbox_bg_t.gif</src></background_top_edge>
	<background_left_edge><src>textbox_bg_l.gif</src></background_left_edge>
	<background_right_edge><src>textbox_bg_r.gif</src></background_right_edge>
	<background_bottom_edge><src>textbox_bg_b.gif</src></background_bottom_edge>
	<bgcolor>#FFFFFF</bgcolor>
</xsl:variable>
<!-- profiles definition end -->
<!--		 Template: Root    -->
<xsl:template match="/">
	<xsl:apply-templates select="params"/>
</xsl:template>
<!--		 Template: Params    -->
<xsl:template match="params">
<xsl:variable name="cur_profile" select="textbox_profile"/>
<xsl:variable name="click_hide" select="onclick_hide"/>
<xsl:variable name="top_height">13</xsl:variable>
<xsl:variable name="left_width">13</xsl:variable>
<xsl:variable name="right_width">13</xsl:variable>
<xsl:variable name="bottom_height">13</xsl:variable>
<xsl:variable name="cur_folder"><xsl:value-of select="$imagesFolder"/><xsl:value-of select="$cur_profile"/>\</xsl:variable>
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
  	<xsl:attribute name="height"><xsl:value-of select="$top_height"/></xsl:attribute>
    <td align="right" valign="bottom">
  	<xsl:attribute name="width"><xsl:value-of select="$left_width"/></xsl:attribute>
		<img>
			<xsl:attribute name="src"><xsl:value-of select="$cur_folder"/><xsl:value-of select="msxsl:node-set($profiles)/corner_top_left/src" /></xsl:attribute>
			<xsl:attribute name="width"><xsl:value-of select="msxsl:node-set($profiles)/corner_top_left/width" /></xsl:attribute>
			<xsl:attribute name="height"><xsl:value-of select="msxsl:node-set($profiles)/corner_top_left/height" /></xsl:attribute>
		</img>
	</td>
    <td width="100%">
		<xsl:attribute name="background"><xsl:value-of select="$cur_folder"/><xsl:value-of select="msxsl:node-set($profiles)/background_top_edge/src" /></xsl:attribute>
	</td>
    <td align="left" valign="bottom">
	  	<xsl:attribute name="width"><xsl:value-of select="$right_width"/></xsl:attribute>
		<img>
			<xsl:attribute name="src"><xsl:value-of select="$cur_folder"/><xsl:value-of select="msxsl:node-set($profiles)/corner_top_right/src" /></xsl:attribute>
			<xsl:attribute name="width"><xsl:value-of select="msxsl:node-set($profiles)/corner_top_right/width" /></xsl:attribute>
			<xsl:attribute name="height"><xsl:value-of select="msxsl:node-set($profiles)/corner_top_right/height" /></xsl:attribute>
		</img>
	</td>
  </tr>
  <tr>
    <td>
		<xsl:attribute name="background"><xsl:value-of select="$cur_folder"/><xsl:value-of select="msxsl:node-set($profiles)/background_left_edge/src" /></xsl:attribute>
	</td>
    <td>
			<xsl:attribute name="bgcolor"><xsl:value-of select="msxsl:node-set($profiles)/bgcolor" /></xsl:attribute>
			<div>
				<xsl:attribute name="style">
					height: 100%;
					padding: <xsl:value-of select="textbox_margin"/>px;
					<xsl:if test="overflow='fit'">overflow: visible</xsl:if>
					<xsl:if test="overflow='scroll'">overflow: scroll</xsl:if>
					<xsl:if test="overflow='scroll-y'">overflow-y: scroll; overflow-x: hidden;</xsl:if>
					<xsl:if test="overflow='scroll-x'">overflow-x: scroll; overflow-y: hidden;</xsl:if>
					<xsl:if test="overflow='auto'">overflow-x: hidden; overflow-y: auto;</xsl:if>
					<xsl:if test="overflow='hidden'">overflow: hidden;</xsl:if>
				</xsl:attribute>
				<xsl:value-of select="textbox_text" disable-output-escaping="yes"/>
			</div>
	</td>
    <td>
		<xsl:attribute name="background"><xsl:value-of select="$cur_folder"/><xsl:value-of select="msxsl:node-set($profiles)/background_right_edge/src"/></xsl:attribute>
	</td>
  </tr>
 <tr>
    <xsl:attribute name="height"><xsl:value-of select="$bottom_height"/></xsl:attribute>
    <td align="right" valign="top">
		<img>
			<xsl:attribute name="src"><xsl:value-of select="$cur_folder"/><xsl:value-of select="msxsl:node-set($profiles)/corner_bottom_left/src" /></xsl:attribute>
			<xsl:attribute name="width"><xsl:value-of select="msxsl:node-set($profiles)/corner_bottom_left/width" /></xsl:attribute>
			<xsl:attribute name="height"><xsl:value-of select="msxsl:node-set($profiles)/corner_bottom_left/height" /></xsl:attribute>
		</img>
	</td>
    <td>
		<xsl:attribute name="background"><xsl:value-of select="$cur_folder"/><xsl:value-of select="msxsl:node-set($profiles)/background_bottom_edge/src"/></xsl:attribute>
	</td>
    <td>
		<img>
			<xsl:attribute name="src"><xsl:value-of select="$cur_folder"/><xsl:value-of select="msxsl:node-set($profiles)/corner_bottom_right/src" /></xsl:attribute>
			<xsl:attribute name="width"><xsl:value-of select="msxsl:node-set($profiles)/corner_bottom_right/width" /></xsl:attribute>
			<xsl:attribute name="height"><xsl:value-of select="msxsl:node-set($profiles)/corner_bottom_right/height" /></xsl:attribute>
		</img>
	</td>
  </tr>
</table>
</xsl:template>
</xsl:stylesheet>
