<TITLE>Feedback for E-Learning course</TITLE>
<?php 
    require_once('../config.php');
    require_once($CFG->dirroot .'../course/lib.php');
    require_once($CFG->dirroot .'../lib/blocklib.php');

    if (empty($SITE)) {
        redirect($CFG->wwwroot .'/'. $CFG->admin .'/index.php');
    }

	
$timenow=time();
if(isset($_POST['submit_form']))
{
$course_id = $_POST['course_id'];
if(isset($_POST['q1']))
{
$q1va=mysql_real_escape_string($_POST['q1']);
execute_sql("insert into mdl_elearning_value(question, answer, user, course, timecreated) values('1','$q1va','$USER->id','$course_id','$timenow')",false);
}
if(isset($_POST['q2']))
{
$q2va=mysql_real_escape_string($_POST['q2']);
execute_sql("insert into mdl_elearning_value(question, answer, user, course, timecreated) values('2','$q2va','$USER->id','$course_id','$timenow')",false);
}
if(isset($_POST['q3']))
{
$q3va=mysql_real_escape_string($_POST['q3']);
execute_sql("insert into mdl_elearning_value(question, answer, user, course, timecreated) values('3','$q3va','$USER->id','$course_id','$timenow')",false);
}
if(isset($_POST['q4']))
{
$q4va=mysql_real_escape_string($_POST['q4']);
execute_sql("insert into mdl_elearning_value(question, answer, user, course, timecreated) values('4','$q4va','$USER->id','$course_id','$timenow')",false);
}
if(isset($_POST['q5']))
{
$q5va=mysql_real_escape_string($_POST['q5']);
execute_sql("insert into mdl_elearning_value(question, answer, user, course, timecreated) values('5','$q5va','$USER->id','$course_id','$timenow')",false);
}
if(isset($_POST['q6']))
{
$q6va=mysql_real_escape_string($_POST['q6']);
execute_sql("insert into mdl_elearning_value(question, answer, user, course, timecreated) values('6','$q6va','$USER->id','$course_id','$timenow')",false);
}
if(isset($_POST['q7']))
{
$q7va=mysql_real_escape_string($_POST['q7']);
execute_sql("insert into mdl_elearning_value(question, answer, user, course, timecreated) values('7','$q7va','$USER->id','$course_id','$timenow')",false);
}
echo '<script language="javascript">alert("Feedback Message Successfully Sent")</script>';
echo '<script>window.location.href="'.$CFG->wwwroot.'/course/view.php?id='.$course_id.'";</script>';
}
else
{
	
    
	if(isset($_GET['courseid'])){
    $courseid = $_GET['courseid'];
    $scormid = $_GET['scormid'];	
	$capturedetail ="select capturefeedback from mdl_scorm where course='$courseid' and id='$scormid' and capturefeedback='1'";
	$capturedetail_check=get_record_sql($capturedetail);
	$attempt=get_record_sql("select max(attempt) as attempt from mdl_scorm_scoes_track where userid='$USER->id' and scormid='$scormid'");
	$value_att="select value from mdl_scorm_scoes_track where userid='$USER->id' and scormid='$scormid' and attempt='$attempt->attempt' and (element='cmi.core.lesson_status' or element='cmi.completion_status')";
    $valuefind=get_record_sql($value_att);
    $gradedetails=("select g.finalgrade as finalgrade,g.timemodified as completedate from mdl_grade_grades g join mdl_grade_items i on i.id=g.itemid where i.itemmodule='scorm' and g.userid='$USER->id' and g.finalgrade>=i.grademax and i.courseid='$courseid' and i.iteminstance='$scormid'");
    $gradeinfo=get_record_sql($gradedetails);
    
    if(($gradeinfo->finalgrade)&& ($capturedetail_check->capturefeedback) && ($valuefind->value=='completed' || $valuefind->value=='passed') ){
	
	// Bounds for block widths
    // more flexible for theme designers taken from theme config.php
    $lmin = (empty($THEME->block_l_min_width)) ? 100 : $THEME->block_l_min_width;
    $lmax = (empty($THEME->block_l_max_width)) ? 210 : $THEME->block_l_max_width;
    $rmin = (empty($THEME->block_r_min_width)) ? 100 : $THEME->block_r_min_width;
    $rmax = (empty($THEME->block_r_max_width)) ? 210 : $THEME->block_r_max_width;

    define('BLOCK_L_MIN_WIDTH', $lmin);
    define('BLOCK_L_MAX_WIDTH', $lmax);
    define('BLOCK_R_MIN_WIDTH', $rmin);
    define('BLOCK_R_MAX_WIDTH', $rmax);

    // check if major upgrade needed - also present in login/index.php
    if ((int)$CFG->version < 2006101100) { //1.7 or older
        @require_logout();
        redirect("$CFG->wwwroot/$CFG->admin/");
    }
    // Trigger 1.9 accesslib upgrade?
    if ((int)$CFG->version < 2007092000 
        && isset($USER->id) 
        && is_siteadmin($USER->id)) { // this test is expensive, but is only triggered during the upgrade
        redirect("$CFG->wwwroot/$CFG->admin/");
    }

    if ($CFG->forcelogin) {
        require_login();
    } else {
        user_accesstime_log();
    }

    if ($CFG->rolesactive) { // if already using roles system
        if (has_capability('moodle/site:config', get_context_instance(CONTEXT_SYSTEM))) {
            if (moodle_needs_upgrading()) {
                redirect($CFG->wwwroot .'/'. $CFG->admin .'/index.php');
            }
        } else if (!empty($CFG->mymoodleredirect)) {    // Redirect logged-in users to My Moodle overview if required
            if (isloggedin() && $USER->username != 'guest') {
                redirect($CFG->wwwroot .'/my/index.php');
            }
        }
    } else { // if upgrading from 1.6 or below
        if (isadmin() && moodle_needs_upgrading()) {
            redirect($CFG->wwwroot .'/'. $CFG->admin .'/index.php');
        }
    }


    if (get_moodle_cookie() == '') {
        set_moodle_cookie('nobody');   // To help search for cookies on login page
    }

    if (!empty($USER->id)) {
        add_to_log(SITEID, 'course', 'view', 'view.php?id='.SITEID, SITEID);
    }

    if (empty($CFG->langmenu)) {
        $langmenu = '';
    } else {
        $currlang = current_language();
        $langs = get_list_of_languages();
        $langlabel = get_accesshide(get_string('language'));
        $langmenu = popup_form($CFG->wwwroot .'/index.php?lang=', $langs, 'chooselang', $currlang, '', '', '', true, 'self', $langlabel);
    }

    $PAGE       = page_create_object(PAGE_COURSE_VIEW, SITEID);
    $pageblocks = blocks_setup($PAGE);
    $editing    = $PAGE->user_is_editing();
    $preferred_width_left  = bounded_number(BLOCK_L_MIN_WIDTH, blocks_preferred_width($pageblocks[BLOCK_POS_LEFT]),
                                            BLOCK_L_MAX_WIDTH);
    $preferred_width_right = bounded_number(BLOCK_R_MIN_WIDTH, blocks_preferred_width($pageblocks[BLOCK_POS_RIGHT]),
                                            BLOCK_R_MAX_WIDTH);
	print_header($SITE->fullname, $SITE->fullname, 'home', '',
                 '<meta name="description" content="'. strip_tags(format_text($SITE->summary, FORMAT_HTML)) .'" />',
                 true, '', user_login_string($SITE).$langmenu);
	
	
$coursenames ="select name from mdl_scorm where id='$scormid' and course='$courseid'";
$course_name=get_record_sql($coursenames);


?>
<!DOCTYPE HTML> 
<html>
 <head>

 </head>
 <body>
 <div> 
 <fieldset style="width:98%">
 <legend>Feedback Form</legend>
 <p>Please provide feedback for the E-Learning course you have completed. This would help us in delivering a better learning experience. </p>
 <p>Thank you for completing <b><?php echo $course_name->name;?></b>. Please provide your feedback below.</p><br>
 <table border="0"> 
 <form method="POST" action="" name="feedform" onsubmit="return ValidateForm()";>
<input type="hidden" name="course_id" value="<?php echo $courseid; ?>">
 <tr><td><b>The E-Learning content was appropriate & helpful.</b></td></tr>
<tr><td>
<input type="radio" name="q1" value="1">&nbsp;1
<input type="radio" name="q1" value="2">&nbsp;2
<input type="radio" name="q1" value="3">&nbsp;3
<input type="radio" name="q1" value="4">&nbsp;4
<input type="radio" name="q1" value="5">&nbsp;5
</td></tr>
 <tr><td>&nbsp;</td></tr>
 
 
 <tr><td><b>The E-Learning course material was well-organized.</b></td></tr>
 <tr><td>
 <input type="radio" name="q2" value="1">&nbsp;1
 <input type="radio" name="q2" value="2">&nbsp;2
 <input type="radio" name="q2" value="3">&nbsp;3
 <input type="radio" name="q2" value="4">&nbsp;4
 <input type="radio" name="q2" value="5">&nbsp;5
 </td></tr>
 <tr><td>&nbsp;</td></tr>
 
 
 <tr><td><b>The E-Learning course material contained appropriate content with sufficient depth of the topic.</b></td></tr>
 <tr><td>
 <input type="radio" name="q3" value="1">&nbsp;1
 <input type="radio" name="q3" value="2">&nbsp;2
 <input type="radio" name="q3" value="3">&nbsp;3
 <input type="radio" name="q3" value="4">&nbsp;4
 <input type="radio" name="q3" value="5">&nbsp;5
 </td></tr>
 <tr><td>&nbsp;</td></tr>
 

 <tr><td><b>How will you use what you have learned today?</b></td></tr>
 <tr><td><textarea name="q4" id="q4" rows="4" cols="50"> </textarea></td></tr>
 <tr><td>&nbsp;</td></tr>
 
 <tr><td><b>What was the most useful part of this E-Learning course?</b></td></tr>
 <tr><td><textarea name="q5" id="q5" rows="4" cols="50"> </textarea></td></tr>
 <tr><td>&nbsp;</td></tr>

 <tr><td><b>How can we improve the course content?</b></td></tr>
 <td><textarea name="q6" id="q6" rows="4" cols="50"> </textarea></td> </tr>
 <tr><td>&nbsp;</td></tr>

 <tr><td><b>Please share any other comments here.</b></td></tr>
 <tr> <td><textarea name="q7" id="q7" rows="4" cols="50"> </textarea></td> </tr>
 <tr><td>&nbsp;</td></tr>
 

 <tr> <td><input id="button" type="submit" name="submit_form" value="Submit"></td> </tr>

 </form>
 </table> 
 </fieldset>
 </div>
 </body>
 </html>
 <script LANGUAGE="JavaScript">
  function trimfield(str) 
{ 
    return str.replace(/^\s+|\s+$/g,''); 
}
function ValidateForm(){ 
if ( ( feedform.q1[0].checked == false ) && ( feedform.q1[1].checked == false ) && ( feedform.q1[2].checked == false ) && ( feedform.q1[3].checked == false ) && ( feedform.q1[4].checked == false ))
 {
 alert ( "Please provide your feedback for : \nThe E-Learning content was appropriate & helpful?" ); 
 return false; 
 }
if ( ( feedform.q2[0].checked == false ) && ( feedform.q2[1].checked == false ) && ( feedform.q2[2].checked == false ) && ( feedform.q2[3].checked == false ) && ( feedform.q2[4].checked == false ))
 {
 alert ( "Please provide your feedback for : \nThe E-Learning course material was well-organized?" ); 
 return false; 
 }
if ( ( feedform.q3[0].checked == false ) && ( feedform.q3[1].checked == false ) && ( feedform.q3[2].checked == false ) && ( feedform.q3[3].checked == false ) && ( feedform.q3[4].checked == false ))
 {
 alert ( "Please provide your feedback for : \nThe E-Learning course material contained appropriate content with sufficient depth of the topic?" ); 
 return false; 
 }
if(trimfield(document.feedform.q4.value)=="")
  {
  alert ( "Please Enter your feedback for : \nHow will you use what you have learned today?" );   
  feedform.q4.focus();
  return false;
  }
if(trimfield(document.feedform.q5.value)=="")
  {
  alert ( "Please Enter your feedback for : \nWhat was the most useful part of this E-Learning course?" );   
  feedform.q5.focus();
  return false;
  }
if(trimfield(document.feedform.q6.value)=="")
  {
  alert ( "Please Enter your feedback for : \nHow can we improve the course content?" );   
  feedform.q6.focus();
  return false;
  }
if(trimfield(document.feedform.q7.value)=="")
  {
  alert ( "Please Enter your feedback for : \nPlease share any other comments here?" );   
  feedform.q7.focus();
  return false;
  }
} 
</script>
<?php

    print_footer('home');     // Please do not modify this line

	}
	else
	{
    echo '<script>window.location.href="'.$CFG->wwwroot.'/course/view.php?id='.$courseid.'";</script>';
	}
	}
	else
	{
    echo '<script>window.location.href="'.$CFG->wwwroot.'/index.php";</script>';
	}
	}
?>	
    


