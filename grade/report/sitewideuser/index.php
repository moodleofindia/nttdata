<?php

// The Beaux Creations Site Wide User Report is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The Beaux Creations Site Wide User Report is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

require_once '../../../config.php';
require_once $CFG->libdir.'/gradelib.php';
require_once $CFG->dirroot.'/grade/lib.php';
require_once $CFG->dirroot.'/grade/report/sitewideuser/lib.php';

$courseid = required_param('id', PARAM_INT);
$userid   = optional_param('userid', $USER->id, PARAM_INT);

/// basic access checks
if (!$course = get_record('course', 'id', $courseid)) {
    print_error('nocourseid');
}
require_login($course);

$context = get_context_instance(CONTEXT_COURSE, $course->id);
require_capability('gradereport/sitewideuser:view', $context);
print_grade_page_head($courseid, 'report', 'sitewideuser');

if (empty($userid)) {
    require_capability('moodle/grade:viewall', $context);
    
} else {
    if (!get_record('user', 'id', $userid, 'deleted', 0) or isguestuser($userid)) {
        error("Incorrect userid");
    }
}

$access = false;
if (has_capability('moodle/grade:viewall', $context)) {
//ok - can view all course grades
    $access = true;
    
} else if ($userid == $USER->id and has_capability('moodle/grade:view', $context) and $course->showgrades) {
    //ok - can view own grades
    $access = true;
    
} else if (has_capability('moodle/grade:viewall', get_context_instance(CONTEXT_USER, $userid)) and $course->showgrades) {
    // ok - can view grades of this user- parent most probably
    $access = true;
}

if (!$access) {
// no access to grades!
    error("Can not view grades.", $CFG->wwwroot.'/course/view.php?id='.$courseid); //TODO: localize
}

/// last selected report session tracking
if (!isset($USER->grade_last_report)) {
    $USER->grade_last_report = array();
}

$USER->grade_last_report[$course->id] = 'sitewideuser';

if (isset($USER->access)) {
    $accessinfo = $USER->access;
} else {
    $accessinfo = get_user_access_sitewide($USER->id);
}
$courses = get_user_courses_bycap($USER->id, 'moodle/course:view', $accessinfo, true, 'c.shortname ASC');

foreach ($courses as $thiscourse) {
    
    $courseid = $thiscourse->id;
    
    /// return tracking object
    $gpr = new grade_plugin_return(array('type'=>'report', 'plugin'=>'sitewideuser', 'courseid'=>$courseid, 'userid'=>$userid));
    //first make sure we have proper final grades - this must be done before constructing of the grade tree
    if (!$course = get_record('course', 'id', $thiscourse->id)) {
        print_error('nocourseid');
    }
    $context = get_context_instance(CONTEXT_COURSE, $thiscourse->id);
    grade_regrade_final_grades($courseid);
    
   //Students will see just their own report
        
        // Create a report instance
        $report = new grade_report_sitewideuser($courseid, $gpr, $context, $USER->id);
        
        // print the page
        print_heading('<a href="'.$CFG->wwwroot.'/grade/report/user/index.php?id='.$thiscourse->id.'">'.$thiscourse->shortname.'</a> - '.get_string('modulename', 'gradereport_sitewideuser').'- <a href="'.$CFG->wwwroot.'/grade/report/user/index.php?id='.$thiscourse->id.'&amp;userid='.$USER->id.'">'.fullname($report->user).'</a>');
        
        if ($report->fill_table()) {
            echo '<br />'.$report->print_table(true);
        }
        echo "<p style = 'page-break-after: always;'></p>";
   
}
print_footer($course);

?>