<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

require_once($CFG->dirroot.'/grade/export/lib.php');

class grade_export_xls extends grade_export {

    var $plugin = 'xls';

    /**
     * To be implemented by child classes
     */
    function print_grades() {
        global $CFG;
        require_once($CFG->dirroot.'/lib/excellib.class.php');

        $export_tracking = $this->track_exports();

        $strgrades = get_string('grades');

    /// Calculate file name
        $downloadfilename = clean_filename("{$this->course->shortname} $strgrades.xls");
    /// Creating a workbook
        $workbook = new MoodleExcelWorkbook("-");
    /// Sending HTTP headers
        $workbook->send($downloadfilename);
    /// Adding the worksheet
        $myxls =& $workbook->add_worksheet($strgrades);

   /// Print names of all the fields
   
		$myxls->write_string(0,0,get_string("portalID"));
		$myxls->write_string(0,1,get_string("employeeName"));
		$myxls->write_string(0,2,get_string("grade"));
		$myxls->write_string(0,3,get_string("country"));
		$myxls->write_string(0,4,get_string("bu"));
		$myxls->write_string(0,5,get_string("ru"));
		$myxls->write_string(0,6,get_string("costcenter"));
		$myxls->write_string(0,7,get_string("email"));
		$myxls->write_string(0,8,get_string("source"));
		$myxls->write_string(0,9,get_string("status"));
		$myxls->write_string(0,10,get_string("enrolleddate"));
		$myxls->write_string(0,11,get_string("managerportalid"));
		$myxls->write_string(0,12,"Manager Name");
		
		$pos=13;   
        foreach ($this->columns as $grade_item) {
            $myxls->write_string(0, $pos++, $this->format_column_name($grade_item));

            /// add a column_feedback column
            if ($this->export_feedback) {
                $myxls->write_string(0, $pos++, $this->format_column_name($grade_item, true));
            }
			 //Raghu code to display complete date
			 $completedate = get_user_preferences('grade_report_showcompletedate');
			 if($completedate==1)
			 {
			 $myxls->write_string(0, $pos++, 'Completed Date');
			 }
			 //Raghu code end to display complete date
        }
		
		//Naga Added to display the Completed date of the course in Grader report for the E-Learning Category courses
		$courseid=$this->course->id;
		 $id=get_record_sql("SELECT m.path FROM mdl_course_categories m join mdl_course c on c.category=m.id  where c.id=$courseid and m.path like ('/6%')");
		
		 if($id->path ){
		
		 $myxls->write_string(0, $pos++, 'Completed Date');
		 
		 }
    /// Print all the lines of data.
        $i = 0;
        $geub = new grade_export_update_buffer();
        $gui = new graded_users_iterator($this->course, $this->columns, $this->groupid);
        $gui->init();
        while ($userdata = $gui->next_user()) {
            $user = $userdata->user;
			$i++;
			
            
			$employeeName=$user->firstname." ".$user->lastname;
			$portalEmail=$user->email;
			$myxls->write_string($i,0,$user->username);
			$myxls->write_string($i,1,$employeeName);        
            $myxls->write_string($i,2,$user->grade);
			$myxls->write_string($i,3,$user->country); 
			$myxls->write_string($i,4,$user->BU);        
            $myxls->write_string($i,5,$user->RU);
            $myxls->write_string($i,6,$user->costcenter);
			$myxls->write_string($i,7,$portalEmail);
			$myxls->write_string($i,8,$user->source);
			
			if($user->auth=='nologin')
			{
			$user->auth='Exited';
			
			}else
			{
			$user->auth='Active';
			}
			
			$myxls->write_string($i,9,$user->auth);
            $myxls->write_string($i,10,date('m-d-Y',$user->enrolleddate));
			$myxls->write_string($i,11,$user->manager_portalid);
			$myxls->write_string($i,12,$user->ManagerName);
			
			//$myxls->write_string($i,7,$grade->timemodified);from_unixtime(timestart)
            $j=13;
			//Raghu code to display complete date
			$arr_val = array();
			foreach ($userdata->grades as $itemid => $grade) {
			$timemodified1=get_record_sql("SELECT m.finalgrade as finalgrade FROM mdl_grade_grades m join mdl_grade_items i on m.itemid=i.id  where i.id=$itemid and m.userid=$user->id and  m.finalgrade >= i.grademax and i.itemtype='mod'");
				if($timemodified1->finalgrade ){
				 $timemodified=get_record_sql("SELECT max(m.timemodified) as timemodified  FROM mdl_grade_grades m join mdl_grade_items i on m.itemid=i.id  where i.id=$itemid and m.userid=$user->id and  m.finalgrade >= i.grademax");
                 $arr_val[] = $timemodified->timemodified;//echo "<pre>";print_r($arr_val);
				 }
			
			}
			//Raghu code end to display complete date	
            foreach ($userdata->grades as $itemid => $grade) {
                if ($export_tracking) {
                    $status = $geub->track($grade);
                }

                $gradestr = $this->format_grade($grade);
                if (is_numeric($gradestr)) {
                    $myxls->write_number($i,$j++,$gradestr);
                }
                else {
				//if($j==12)
			//	{	
					
		//	$myxls->write_number($i,$j++,$grade);
		//		}
		//		else{
                    $myxls->write_string($i,$j++,$gradestr);
		//		}
                }

                // writing feedback if requested
                if ($this->export_feedback) {
                    $myxls->write_string($i, $j++, $this->format_feedback($userdata->feedbacks[$itemid]));
                }
			//Raghu code to display complete date
				 $completedate = get_user_preferences('grade_report_showcompletedate');
				
			 if($completedate==1)
			 {
				$timemodified1_c=get_record_sql("SELECT m.finalgrade as finalgrade FROM mdl_grade_grades m join mdl_grade_items i on m.itemid=i.id  where i.id=$itemid and m.userid=$user->id and  m.finalgrade >= i.grademax and i.itemtype='course'");
				if($timemodified1_c->finalgrade ){
				if(max($arr_val)!='' || max($arr_val)!='null'){
				$myxls->write_string($i,$j++,date('m-d-Y',max($arr_val)));
				}
				}else{
				
				$timemodified1=get_record_sql("SELECT m.finalgrade as finalgrade FROM mdl_grade_grades m join mdl_grade_items i on m.itemid=i.id  where i.id=$itemid and m.userid=$user->id and  m.finalgrade >= i.grademax and i.itemtype='mod'");
		 if($timemodified1->finalgrade ){
		 
			 $timemodified=get_record_sql("SELECT max(m.timemodified) as timemodified  FROM mdl_grade_grades m join mdl_grade_items i on m.itemid=i.id  where m.itemid=$itemid and m.userid=$user->id ");
			
			if($timemodified->timemodified=='' || $timemodified->timemodified=='null'){
			$myxls->write_string($i,$j++,'');
			
			}
			else{
		$myxls->write_string($i,$j++,date('m-d-Y',$timemodified->timemodified));
		
		 
			}
			}else {
			$myxls->write_string($i,$j++,'');
			}
			}
			}
			//Raghu code end to display complete date	
            }
				//Naga added to display the completed date in the team report
			$courseid=$this->course->id;
		 $id=get_record_sql("SELECT m.path FROM mdl_course_categories m join mdl_course c on c.category=m.id  where c.id=$courseid and m.path like ('/6%')");
		
		$timemodified1=get_record_sql("SELECT m.finalgrade as finalgrade FROM mdl_grade_grades m join mdl_grade_items i on m.itemid=i.id  where i.courseid=$courseid and m.userid=$user->id and  m.finalgrade >= i.grademax and i.itemtype='course'");
		 if($id->path && $timemodified1->finalgrade ){
		 
			 $timemodified=get_record_sql("SELECT max(m.timemodified) as timemodified  FROM mdl_grade_grades m join mdl_grade_items i on m.itemid=i.id  where i.courseid=$courseid and m.userid=$user->id ");
			
			if($timemodified->timemodified=='' || $timemodified->timemodified=='null'){
			$myxls->write_string($i,$j++,'');
			
			}
			else{
		$myxls->write_string($i,$j++,date('m-d-Y',$timemodified->timemodified));
		
		 
			}
			}
		
        }
        $gui->close();
        $geub->close();

    /// Close the workbook
        $workbook->close();

        exit;
    }
}

?>
