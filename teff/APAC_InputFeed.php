<?php
   
	ini_set('max_execution_time',0);
    $starttime = microtime();

    define('FULLME', 'cron');

    $nomoodlecookie = true;

    if (!isset($_SERVER['REMOTE_ADDR']) && isset($_SERVER['argv'][0])) {
        chdir(dirname($_SERVER['argv'][0]));
    }
	require_once(dirname(__FILE__) . '/../config.php');
    require_once($CFG->libdir.'/adminlib.php');
    require_once($CFG->libdir.'/gradelib.php');
	
	include_once($CFG->dirroot.'/admin/report/cpd/lang/en_utf8/countryco.php');
    if (!empty($CFG->showcronsql)) {
        $db->debug = true;
    }
    if (!empty($CFG->showcrondebugging)) {
        $CFG->debug = DEBUG_DEVELOPER;
        $CFG->debugdisplay = true;
    }

/// extra safety
    @session_write_close();

/// check if execution allowed
    if (isset($_SERVER['REMOTE_ADDR'])) { // if the script is accessed via the web.
        if (!empty($CFG->cronclionly)) { 
            // This script can only be run via the cli.
            print_error('cronerrorclionly', 'admin');
            exit;
        }
        // This script is being called via the web, so check the password if there is one.
        if (!empty($CFG->cronremotepassword)) {
            $pass = optional_param('password', '', PARAM_RAW);
            if($pass != $CFG->cronremotepassword) {
                // wrong password.
                print_error('cronerrorpassword', 'admin'); 
                exit;
            }
        }
    }


/// emulate normal session
    $SESSION = new object();
    $USER = get_admin();      /// Temporarily, to provide environment for this script

/// ignore admins timezone, language and locale - use site deafult instead!
    $USER->timezone = $CFG->timezone;
    $USER->lang = '';
    $USER->theme = '';
    course_setup(SITEID);

/// send mime type and encoding
    if (check_browser_version('MSIE')) {
        //ugly IE hack to work around downloading instead of viewing
        @header('Content-Type: text/html; charset=utf-8');
        echo "<xmp>"; //<pre> is not good enough for us here
    } else {
        //send proper plaintext header
        @header('Content-Type: text/plain; charset=utf-8');
    }

/// no more headers and buffers
    while(@ob_end_flush());

/// increase memory limit (PHP 5.2 does different calculation, we need more memory now)
    @ini_set('memory_limit','1000M');

/// Start output log

    $timenow  = time();

    mtrace("Server Time: ".date('r',$timenow)."\n\n");

	    $timenow  = time();
//Truncate temp table to get the details from APAC AD
$queryTruncate='truncate table mdl_user_apac_Temp';
	
$resultTruncate = execute_sql($queryTruncate, $feedback=true);


$queryinactivate="update mdl_user_apac set auth = 'nologin'";
	
$resultinactivate = execute_sql($queryinactivate, $feedback=true);

// China AD
/*$ldap_server = "ldap://10.234.7.42:389";
$auth_user = "CN=CHLMSadmin,OU=Service Accounts,OU=Admins,DC=CHINA,DC=GLOBAL,DC=NTTDATA,DC=COM";
$auth_pass = "China@2016";

$base_dn = "OU=Employees,DC=CHINA,DC=GLOBAL,DC=NTTDATA,DC=COM";
$filter = "objectClass=*";*/


//APAC AD


$ldap_server = "ldap://10.234.7.30:389";
//$ldap_server = "ldap://10.232.1.38:389";
$auth_user = "CN=LMSadmin,OU=Service Accounts,OU=Admins,DC=APAC,DC=GLOBAL,DC=NTTDATA,DC=COM";
$auth_pass = "Apac@123";

$base_dn = "OU=Employees,DC=APAC,DC=GLOBAL,DC=NTTDATA,DC=COM";
$filter = "objectClass=*";

// connect to server
if (!($connect=@ldap_connect($ldap_server))) {
     die("Could not connect to ldap server");
}

  $connresult = ldap_connect($ldap_server);
  if ($connresult)
			{
			echo " LDAP connect success";
			echo "<br>";
			}
			else{
			echo "no success";
			}
	$bind = ldap_bind($connect, $auth_user, $auth_pass);
	if 	($bind)
	{
			echo  "LDAP binded";
	}
	else
	{
			die("Unable to bind to server : " . ldap_error() . ' ' . ldap_errno($connect));
			
	}
	
	exit;

// bind to server
if (!($bind = ldap_bind($connect, $auth_user, $auth_pass))) {
     die("Unable to bind to server : " . ldap_error() . ' ' . ldap_errno($connect));
}

ldap_set_option($connect, LDAP_OPT_PROTOCOL_VERSION, 3);


//srinu added new code
	 $filter    = '(cn=*)';
     $justthese = array('cn','givenname','sn','mail','l','co','company','department','streetAddress','telephoneNumber','mobile','employeetype','extensionattribute15','countryco');

     $pageSize = 100;

     $cookie = '';
     do {
         ldap_control_paged_result($connect, $pageSize, true, $cookie);

         $result  = ldap_search($connect, $base_dn, $filter, $justthese);
         $entries = ldap_get_entries($connect, $result);
         
         foreach ($entries as $e) {
             
				$portalid= mysql_real_escape_string(utf8_encode($e["cn"][0]));
				$legacyname =mysql_real_escape_string(utf8_encode($e["extensionattribute15"][0]));
				$firstname =mysql_real_escape_string(utf8_encode($e["givenname"][0]));
				$lastname =mysql_real_escape_string(utf8_encode($e["sn"][0]));
				$country =mysql_real_escape_string(utf8_encode($e["co"][0]));
				$email =mysql_real_escape_string(utf8_encode($e["mail"][0]));
				$institution = mysql_real_escape_string(utf8_encode($e["company"][0]));
				$department =mysql_real_escape_string(utf8_encode ($e["department"][0]));
				$address = mysql_real_escape_string(utf8_encode($e["streetAddress"][0]));
				$location = mysql_real_escape_string(utf8_encode($e["l"][0]));
				$phone1 = mysql_real_escape_string(utf8_encode($e["telephoneNumber"][0]));
				$phone2 = mysql_real_escape_string(utf8_encode($e["mobile"][0]));
				$employee_type=mysql_real_escape_string(utf8_encode($e["employeetype"][0]));
				$countrycode = mysql_real_escape_string(utf8_encode($e["countryco"][0]));
				
				$queryInsert="insert into mdl_user_apac_temp (portalID,firstname,lastname,country,email,institution,address,department,phone1,phone2,location,employee_type,timecreated) values ('$portalid','$firstname','$lastname','$countrycode','$email','$institution','$address','$department','$phone1','$phone2','$location','$employee_type','$timenow');";
				
				//$queryInsert="insert into mdl_user_apac_temp (portalID) values ('$portalid');";
				$resultInsert = execute_sql($queryInsert, $feedback=true);
				if($resultInsert)
				{
				mtrace ("Portal ID: ". $portalid."\n");
				//mtrace ("Country is". $country."\n");
				mtrace ("Employee records inserted sucessfully into staging table ! \n");
				}
				else
				{
				echo 'Database Error Insert:' . mysql_error();
				mtrace ("Error: Failed to insert employee record into staging table:$queryInsert".$feedback."\n");
				flush();
				}
			 
			 
			 
			 
         }

         ldap_control_paged_result_response($connect, $result, $cookie);
       
     } while($cookie !== null && $cookie != '');

//srinu ended new code
exit;



	$queryUpdate="UPDATE mdl_user_apac u INNER JOIN mdl_user_apac_temp t ON u.username = t.portalID set u.auth = 'ldap',u.firstname=t.firstname,u.lastname=t.lastname,u.email=t.email,
				  u.institution=t.institution,u.timeupdated='$timenow'";	
	
	$queryInsert="insert into mdl_user_apac (username,firstname,lastname,email,country,institution,address,phone1,phone2,city,employee_type,timecreated)
				select portalID,firstname,lastname,email,country,institution,address,phone1,phone2,location,employee_type,timecreated
				from mdl_user_apac_temp m where m.portalid not in(select username from mdl_user_apac);";
	
	mtrace ("\n". " Employee records updating.......... \n");
	$resultUpdate = execute_sql($queryUpdate, $feedback=true);
	if($resultUpdate)
	{
	mtrace("\n Employee records updated sucessfully in user table! \n");
	}
	else
	{
	mtrace("\n Error: Failed to update employee records in user table.:$queryUpdate".$feedback."\n");
	}
	
	mtrace ("\n Inserting Employee records.......... \n");
	$resultInsert = execute_sql($queryInsert, $feedback=true);
	if($resultInsert)
	{
	mtrace("\n Employee records inserted sucessfully in user table ! \n");
	}
	else
	{
	mtrace("\n Error: Failed to insert employee records in user table.:$queryInsert".$feedback."\n");
	}	


    mtrace('Starting job to connect to the APAC HR database ...');
	//Naga added  below code to connect to the APAC HR Database
	//APAC HR - is the system DSN name . We have to create DSN entry for the target SQL server.
	$conn=odbc_connect("APAC HR", "lmsuser", "lmsuser@123");


//declare the SQL statement that will query the database
	$query = "SELECT [EID]
		  ,[EmpStatusDesc]
		  ,[PortalID]
		  ,[SalutationDesc]
		  ,[FirstName]
		  ,[LastName]
		  ,[EmailId]
		  ,[LocationDesc]
		  ,[CountryName]
		  ,[Grade]
		  ,[EmpCategoryDesc]
		  ,[ReportingManagerId]
		  ,[DOJ]
		  ,[CompanyName]
		  ,[ModifiedDate]
			FROM [APACHRIMSBKP].[dbo].[tblapacemployee]";


	if( $conn )
	 {
		 mtrace ( "Connection established.<br />");
		 $resultset=odbc_exec($conn,$query);
	}
	else
	{
		 mtrace ( "Connection could not be established.<br />");
	  
	}


    $data = array();
		while (odbc_fetch_row($resultset)) 
		{
			$data[]=array(	odbc_result ($resultset, "EID"),
							odbc_result ($resultset, "EmpStatusDesc"),
							odbc_result ($resultset, "PortalID"),
							odbc_result ($resultset, "SalutationDesc"),
							odbc_result ($resultset, "FirstName"),
							odbc_result ($resultset, "LastName"),
							odbc_result ($resultset, "EmailId"),
							odbc_result ($resultset, "LocationDesc"),
							odbc_result ($resultset, "CountryName"),
							odbc_result ($resultset, "Grade"),
							odbc_result ($resultset, "EmpCategoryDesc"),
							odbc_result ($resultset, "ReportingManagerId"),
							odbc_result ($resultset, "DOJ"),
							odbc_result ($resultset, "CompanyName"),
							odbc_result ($resultset, "ModifiedDate"));
                 
		}
	  
		
		$link = mysql_connect('10.232.1.41', 'AdminUser', 'Keane@123');
	if ($link)
	{
		mtrace ( "connected to APAC LMS mysql data base");
		
		$deletesql='delete from  mdl_apac_hr;';
		execute_sql($deletesql,$feedback=true);
		
		foreach($data as $record)
		{
		
		$record[0]=mysql_real_escape_string(utf8_encode ($record[0]));
		$record[1]=mysql_real_escape_string(utf8_encode ($record[1]));
		$record[2]=mysql_real_escape_string(utf8_encode ($record[2]));
		$record[3]=mysql_real_escape_string(utf8_encode ($record[3]));
		$record[4]=mysql_real_escape_string(utf8_encode ($record[4]));
		$record[5]=mysql_real_escape_string(utf8_encode ($record[5]));
		$record[6]=mysql_real_escape_string(utf8_encode ($record[6]));
		$record[7]=mysql_real_escape_string(utf8_encode ($record[7]));
		//country
		$record[8]=mysql_real_escape_string(utf8_encode ($record[8]));
		$record[9]=mysql_real_escape_string(utf8_encode ($record[9]));
		$record[10]=mysql_real_escape_string(utf8_encode ($record[10]));
		$record[11]=mysql_real_escape_string(utf8_encode ($record[11]));
		$record[12]=mysql_real_escape_string(utf8_encode ($record[12]));
		$record[13]=mysql_real_escape_string(utf8_encode ($record[13]));
		$record[14]=mysql_real_escape_string(utf8_encode ($record[14]));
		$country_update = '';
				// mtrace $portalid .":".$firstname.":".$lastname.":".$country.":".$email.":".$institution.":".$department.":".$address."<br>";
				if($record[8] <> null and $record[8] <> ''){
				$record[8] = get_string($record[8], 'countryco');
				$timezone = get_string($record[8], 'countryco');
				
				//Added to fix the issue with country code
				
				if(strlen($record[8])>=3)
				{
				$record[8]='';
				
				mtrace ("Country is". $portalid."\n");
				mtrace ("Country is". $record[8]."\n");
				}
				//Complted
				}
				else
				{
				mtrace ("Country blank". $record[8]."\n");
				}
					 	$resultDisableEmployees = ("insert into mdl_apac_hr  (EID
													  ,EmpStatusDesc
													  ,PortalID
													  ,SalutationDesc
													  ,FirstName
													  ,LastName
													  ,EmailId
													  ,LocationDesc
													  ,CountryName
													  ,Grade
													  ,EmpCategoryDesc
													  ,ReportingManagerId
													  ,DOJ
													  ,CompanyName
													  ,ModifiedDate) Values 
													  ('$record[0]','$record[1]','$record[2]','$record[3]','$record[4]','$record[5]','$record[6]','$record[7]','$record[8]'
													  ,'$record[9]','$record[10]','$record[11]','$record[12]','$record[13]','$record[14]');");
						
						$queries=execute_sql($resultDisableEmployees );
						
		}
	}
	else
	{
	mtrace ( "unable to connected to APAC LMS mysql data base");
	}
	$queryUpdate_hrdb="UPDATE mdl_user_apac u INNER JOIN mdl_apac_hr t ON u.username = t.PortalID set u.country=t.CountryName,u.city=t.LocationDesc,
				  u.grade=t.Grade,u.manager_portalid=t.ReportingManagerId,u.employee_type=t.EmpCategoryDesc,
				  u.hireddate=t.DOJ,u.country=t.CountryName,u.institution=t.CompanyName,u.timeupdated='$timenow'";	
				  mtrace ("\n". " Employee records updating.......... \n");
	$resultUpdate = execute_sql($queryUpdate_hrdb, $feedback=true);
	if($resultUpdate)
	{
	mtrace("\n Employee records updated sucessfully in user table from mdl_apac_hr! \n");
	}
	else
	{
	mtrace("\n Error: Failed to update employee records in user table.:$queryUpdate_hrdb".$feedback."\n");
	}
	 //Unset session variables and destroy it
    @session_unset();
    @session_destroy();


    $difftime = microtime_diff($starttime, microtime());
    mtrace("Execution took ".$difftime." seconds"); 

/// finish the IE hack
    if (check_browser_version('MSIE')) {
        echo "</xmp>";
    }
?> 