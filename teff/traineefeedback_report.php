<?php 
require_once('../config.php');


    if (empty($SITE)) {
        redirect($CFG->wwwroot .'/'. $CFG->admin .'/index.php');
    }

	

    $PAGE       = page_create_object(PAGE_COURSE_VIEW, SITEID);
    $pageblocks = blocks_setup($PAGE);
    $editing    = $PAGE->user_is_editing();
    $preferred_width_left  = bounded_number(BLOCK_L_MIN_WIDTH, blocks_preferred_width($pageblocks[BLOCK_POS_LEFT]),
                                            BLOCK_L_MAX_WIDTH);
    $preferred_width_right = bounded_number(BLOCK_R_MIN_WIDTH, blocks_preferred_width($pageblocks[BLOCK_POS_RIGHT]),
                                            BLOCK_R_MAX_WIDTH);
	print_header($SITE->fullname, $SITE->fullname, 'home', '',
                 '<meta name="description" content="'. strip_tags(format_text($SITE->summary, FORMAT_HTML)) .'" />',
                 true, '', user_login_string($SITE).$langmenu);
$squery=get_records_sql("SELECT c.id as cid,c.name as cname  FROM mdl_classroom_sessions s join mdl_classroom c on c.id=s.classroom where s.externaltraining='1' and s.status='completed'");
?>
<div style="border:1px solid gray;margin:50px 50px 50px 50px;padding:20px 20px 20px 20px;border-radius:5px;">
<div align="center"><select name="reportselect" required="requried" onchange="showreport(this.value)">
<option value="">Select the report</option>
<option value="formab">Feedback for A & B(By the Attendees)</option>
<option value="formc">Feedback for C(By the requestor)</option>
</select>
</div>
<div style="clear:both;"></div>
<div id="formab" style="display:none;">
<form action="getreport.php" method="post" onsubmit="return validate();">
<div align="center" style="margin-top:10px;">
<select name="classroomformab" required="requried" onchange="showlocations(this.value)">
<option value="">Select the classroom</option>
<?php foreach($squery as $key=>$value)
{ 
$cid=$value->cid;
$cname=$value->cname;
?>
<option value="<?php echo $cid; ?>"><?php echo $cname; ?></option>
<?php 
}
?>
</select>
</div>
<div align="center" id="locations" style="margin-top:10px;">
<select name="locationformab" required="requried" onchange="showdates(this.value)">
<option value="">Select the Location</option>
</select>
</div>
<div align="center" id="dates" style="margin-top:10px;">
<select name="session" id="session" required="requried">
<option value="">Select the Date</option>
</select>
</div>
<div id="report"></div>
<div align="center" style="margin-top:10px;">
<input type="submit" value="Download"/>
</div>
</form>
</div>
<div style="clear:both;"></div>
<div id="formc" style="display:none;">
<form action="getreport_requestor.php" method="post" onsubmit="return validateformc();">
<div align="center" style="margin-top:10px;">
<select name="classroomformc" required="requried" onchange="showlocationsformc(this.value)">
<option value="">Select the classroom</option>
<?php foreach($squery as $key=>$value)
{ 
$cid=$value->cid;
$cname=$value->cname;
?>
<option value="<?php echo $cid; ?>"><?php echo $cname; ?></option>
<?php 
}
?>
</select>
</div>
<div align="center" id="locationsformc" style="margin-top:10px;">
<select name="locationformc" required="requried" onchange="showdatesformc(this.value)">
<option value="">Select the Location</option>
</select>
</div>
<div align="center" id="datesformc" style="margin-top:10px;">
<select name="sessionformc" id="sessionformc" required="requried">
<option value="">Select the Date</option>
</select>
</div>
<div id="report"></div>
<div align="center" style="margin-top:10px;">
<input type="submit" value="Download"/>
</div>
</form>
</div>
</div>
<?php

    print_footer('home');     // Please do not modify this line

	
?>

<script type="text/javascript">
//show dates
function showdates(val)
{
if(val!='')
{
$.post("getlocations.php",{'sesid':val},function(data){
//alert(data);
$('#dates').html(data);
});
}
else if(val=='')
{
$("#dates").html('<select name="session" id="dates" required="requried"><option value="">Select the location first</option></select>');
}

}
//show locations
function showlocations(val)
{
//alert(val);
if(val!='')
{
$.post("getlocations.php",{'classid':val},function(data){
//alert(data);
$('#locations').html(data);
$("#dates").html('<select name="session" id="dates" required="requried"><option value="">select the date</option></select>');
});
}
else if(val=='')
{
$("#locations").html('<select name="session" required="requried" onchange="showdates(this.value)"><option value="">Select the classroom first</option></select>');
$("#dates").html('<select name="session" id="dates" required="requried"><option value="">select the classroom first</option></select>');
}
}

//start formc
//show dates
function showdatesformc(val)
{
if(val!='')
{
$.post("getlocations.php",{'sesidformc':val},function(data){
//alert(data);
$('#datesformc').html(data);
});
}
else if(val=='')
{
$("#datesformc").html('<select name="sessionformc" id="sessionformc" required="requried"><option value="">Select the location first</option></select>');
}

}
//show locations
function showlocationsformc(val)
{
if(val!='')
{
$.post("getlocations.php",{'classidformc':val},function(data){
//alert(data);
$('#locationsformc').html(data);
$("#datesformc").html('<select name="sessionformc" id="sessionformc" required="requried"><option value="">select the date</option></select>');
});
}
else if(val=='')
{
$("#locationsformc").html('<select name="locationsformc" required="requried" onchange="showdates(this.value)"><option value="">Select the classroom first</option></select>');
$("#datesformc").html('<select name="sessionformc" id="sessionformc" required="requried"><option value="">select the classroom first</option></select>');
}
}
//end formc

function showreport(val)
{
if(val=='formab')
{
$("#formab").show();
$("#formc").hide();
}
else if(val=='formc')
{
$("#formc").show();
$("#formab").hide();
}
else
{
$("#formab").hide();
$("#formc").hide();
}
}
function validate()
{
var sesval=$("#session").val();
$.post('getreport.php',{'session':sesval},function(data){
alert('Report Downloaded successfully');
});

}
function validateformc()
{
var sesval=$("#sessionformc").val();
$.post('getreport_requestor.php',{'session':sesval},function(data){
alert('Report Downloaded successfully');
});

}

</script>	