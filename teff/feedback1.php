<TITLE>Feedback for ILT Session</TITLE>
<?php 

    require_once('../config.php');
    require_once($CFG->dirroot .'../course/lib.php');
    require_once($CFG->dirroot .'../lib/blocklib.php');

    if (empty($SITE)) {
        redirect($CFG->wwwroot .'/'. $CFG->admin .'/index.php');
    }

	// Bounds for block widths
    // more flexible for theme designers taken from theme config.php
    $lmin = (empty($THEME->block_l_min_width)) ? 100 : $THEME->block_l_min_width;
    $lmax = (empty($THEME->block_l_max_width)) ? 210 : $THEME->block_l_max_width;
    $rmin = (empty($THEME->block_r_min_width)) ? 100 : $THEME->block_r_min_width;
    $rmax = (empty($THEME->block_r_max_width)) ? 210 : $THEME->block_r_max_width;

    define('BLOCK_L_MIN_WIDTH', $lmin);
    define('BLOCK_L_MAX_WIDTH', $lmax);
    define('BLOCK_R_MIN_WIDTH', $rmin);
    define('BLOCK_R_MAX_WIDTH', $rmax);

    // check if major upgrade needed - also present in login/index.php
    if ((int)$CFG->version < 2006101100) { //1.7 or older
        @require_logout();
        redirect("$CFG->wwwroot/$CFG->admin/");
    }
    // Trigger 1.9 accesslib upgrade?
    if ((int)$CFG->version < 2007092000 
        && isset($USER->id) 
        && is_siteadmin($USER->id)) { // this test is expensive, but is only triggered during the upgrade
        redirect("$CFG->wwwroot/$CFG->admin/");
    }

    if ($CFG->forcelogin) {
        require_login();
    } else {
        user_accesstime_log();
    }

    if ($CFG->rolesactive) { // if already using roles system
        if (has_capability('moodle/site:config', get_context_instance(CONTEXT_SYSTEM))) {
            if (moodle_needs_upgrading()) {
                redirect($CFG->wwwroot .'/'. $CFG->admin .'/index.php');
            }
        } else if (!empty($CFG->mymoodleredirect)) {    // Redirect logged-in users to My Moodle overview if required
            if (isloggedin() && $USER->username != 'guest') {
                redirect($CFG->wwwroot .'/my/index.php');
            }
        }
    } else { // if upgrading from 1.6 or below
        if (isadmin() && moodle_needs_upgrading()) {
            redirect($CFG->wwwroot .'/'. $CFG->admin .'/index.php');
        }
    }


    if (get_moodle_cookie() == '') {
        set_moodle_cookie('nobody');   // To help search for cookies on login page
    }

    if (!empty($USER->id)) {
        add_to_log(SITEID, 'course', 'view', 'view.php?id='.SITEID, SITEID);
    }

    if (empty($CFG->langmenu)) {
        $langmenu = '';
    } else {
        $currlang = current_language();
        $langs = get_list_of_languages();
        $langlabel = get_accesshide(get_string('language'));
        $langmenu = popup_form($CFG->wwwroot .'/index.php?lang=', $langs, 'chooselang', $currlang, '', '', '', true, 'self', $langlabel);
    }

    $PAGE       = page_create_object(PAGE_COURSE_VIEW, SITEID);
    $pageblocks = blocks_setup($PAGE);
    $editing    = $PAGE->user_is_editing();
    $preferred_width_left  = bounded_number(BLOCK_L_MIN_WIDTH, blocks_preferred_width($pageblocks[BLOCK_POS_LEFT]),
                                            BLOCK_L_MAX_WIDTH);
    $preferred_width_right = bounded_number(BLOCK_R_MIN_WIDTH, blocks_preferred_width($pageblocks[BLOCK_POS_RIGHT]),
                                            BLOCK_R_MAX_WIDTH);
	print_header($SITE->fullname, $SITE->fullname, 'home', '',
                 '<meta name="description" content="'. strip_tags(format_text($SITE->summary, FORMAT_HTML)) .'" />',
                 true, '', user_login_string($SITE).$langmenu);

$sessionid=$_REQUEST['s'];				 
global $USER;
$userid=$USER->id;
if(isset($_POST['submit']))
{
$q1response=$_POST['q1'];
$q1comments=$_POST['q1comments'];
$q2response=$_POST['q2'];
$q2comments=$_POST['q2comments'];
$q3comments=$_POST['q3comments'];
$date=strtotime("now");
$questquery="insert into mdl_feedback_ext_employee_response(userid, question_id, response, datecreated, datemodified, comments,seesionid) values('$userid','1','$q1response',$date,$date,'$q1comments','$sessionid'),('$userid','2','$q2response',$date,$date,'$q2comments','$sessionid'),('$userid','3','',$date,$date,'$q3comments','$sessionid')";

if(!execute_sql($questquery,false))
{
echo mysql_error();
}
else
{
echo "feed back submitted successfully";
}
}
	


$query=get_records_sql("select * from mdl_classroom_sessions where id='$sessionid'");

foreach($query as $key=>$value)
{
$programname=$value->programename;
$skillimprovearea=$value->sessioncategory;
$requestor=$value->requestor;
if($requestor!='')
{
$reqname=get_record_sql("select firstname,lastname from mdl_user where username='$requestor'");
$requestorname=$reqname->firstname.' '.$reqname->lastname;
}
else
{
$requestorname='';
}
$tlocation=$value->location;
$datess=get_record_sql("SELECT timestart,timefinish FROM mdl_classroom_sessions_dates m where sessionid='$sessionid'");
$allsessiondates = userdate($datess->timestart, get_string('strftimedate'));
$timestart= $allsessiondates.' '.userdate($datess->timestart, get_string('strftimetime'));
$timefinish= $allsessiondates.' '.userdate($datess->timefinish, get_string('strftimetime'));
$duration=$value->duration." Minutes";
}
$tname=get_record_sql("SELECT userid FROM mdl_classroom_trainners where sessionid='$sessionid'");
$tuserid=$tname->userid;
if($tuserid!='')
{
$tname=get_record_sql("select firstname,lastname from mdl_user where id='$tuserid'");
$trainername=$tname->firstname.' '.$tname->lastname;
}
else
{
$trainername='';
}

$userq=get_record_sql("select * from mdl_user where id='$userid'");

$username=$userq->username;
$empname=$userq->firstname.' '.$userq->lastname;
$city=$userq->city;
$country=$userq->country;
$ulocation=$city.",".$country;
$vertical=$userq->vertical;



?>
<!DOCTYPE HTML> 
<html>
<head>
</head>
<body>
<div style="width:100%;font-family:arial;"> 


<form method="POST" action="feedback1.php" name="feedform1" onsubmit="return ValidateForm()";>
<input type="hidden" name="s" value="<?php echo $sessionid;  ?>">
<div style="width:100%;height:5%;background-color:#548dd4;border-bottom:1px solid white;">
<div style="width:80%;float:left;text-align:center;color:white;margin-top:10px;font-weight:bold;">Training Effectiveness</div>
<div style="width:20%;float:right;color:white;margin-top:10px;font-weight:bold;">PR – 059E<br>Version 1.00</div>
</div>
<div style="clear:both;"></div>
<div style="width:100%;height:4%;border-top:1px solid white;background-color:#548dd4;">
<div style="width:80%;float:left;color:white;margin-top:10px;font-weight:bold;">Training Details</div>
<div style="width:20%;float:right;color:white;"></div>
</div>
<div style="clear:both;"></div>
<div style="width:100%;">
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#777777;float:left;">
<div style="width:50%;float:left;color:white;margin-top:10px;font-weight:bold;">Training Program Name</div>
</div>
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#FFFFFF;float:right;">
<div style="float:left;font-weight:bold;"><input type="text" style="width:500px;height:36px;" name="prgname"value="<?php echo $programname; ?>" /></div>
</div>
</div>
<div style="clear:both;";></div>
<div style="width:100%;">
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#777777;float:left;">
<div style="width:50%;float:left;color:white;margin-top:10px;font-weight:bold;">Skill Improvement Area</div>
</div>
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#FFFFFF;float:right;">
<div style="float:left;font-weight:bold;"><input type="text" style="width:500px;height:36px;" name="skillname"value="<?php echo $skillimprovearea; ?>" /></div>
</div>
</div>
<div style="clear:both;";></div>
<div style="width:100%;">
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#777777;float:left;">
<div style="width:50%;float:left;color:white;margin-top:10px;font-weight:bold;">Requestor / Nominator Name</div>
</div>
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#FFFFFF;float:right;">
<div style="float:left;font-weight:bold;"><input type="text" style="width:500px;height:36px;" name="reqname"value="<?php echo $requestorname; ?>" /></div>
</div>
</div>
<div style="clear:both;";></div>
<div style="width:100%;">
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#777777;float:left;">
<div style="width:50%;float:left;color:white;margin-top:10px;font-weight:bold;">Training Location</div>
</div>
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#FFFFFF;float:right;">
<div style="float:left;font-weight:bold;"><input type="text" style="width:500px;height:36px;" name="tlocation"value="<?php echo $tlocation; ?>" /></div>
</div>
</div>
<div style="clear:both;";></div>
<div style="width:100%;">
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#777777;float:left;">
<div style="width:50%;float:left;color:white;margin-top:10px;font-weight:bold;">Training Start Date</div>
</div>
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#FFFFFF;float:right;">
<div style="float:left;font-weight:bold;"><input type="text" style="width:500px;height:36px;" name="tstart" value="<?php echo $timestart; ?>" /></div>
</div>
</div>
<div style="clear:both;";></div>
<div style="width:100%;">
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#777777;float:left;">
<div style="width:50%;float:left;color:white;margin-top:10px;font-weight:bold;">Training End Date</div>
</div>
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#FFFFFF;float:right;">
<div style="float:left;font-weight:bold;"><input type="text" style="width:500px;height:36px;" name="tfinish" value="<?php echo $timefinish; ?>" /></div>
</div>
</div>
<div style="clear:both;";></div>
<div style="width:100%;">
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#777777;float:left;">
<div style="width:50%;float:left;color:white;margin-top:10px;font-weight:bold;">Duration</div>
</div>
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#FFFFFF;float:right;">
<div style="float:left;font-weight:bold;"><input type="text" style="width:500px;height:36px;" name="duration" value="<?php echo $duration; ?>" /></div>
</div>
</div>
<div style="clear:both;";></div>
<div style="width:100%;">
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#777777;float:left;">
<div style="width:50%;float:left;color:white;margin-top:10px;font-weight:bold;">Trainer Name</div>
</div>
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#FFFFFF;float:right;">
<div style="float:left;font-weight:bold;"><input type="text" style="width:500px;height:36px;" name="duration" value="<?php echo $trainername; ?>" /></div>
</div>
</div>
<div style="clear:both;";></div>
<div style="width:100%;height:4%;border-top:1px solid white;background-color:#548dd4;">
<div style="width:80%;float:left;color:white;margin-top:10px;font-weight:bold;">Employee Details</div>
<div style="width:20%;float:right;color:white;"></div>
</div>
<div style="clear:both;"></div>
<div style="width:100%;">
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#777777;float:left;">
<div style="width:50%;float:left;color:white;margin-top:10px;font-weight:bold;">Employee Name</div>
</div>
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#FFFFFF;float:right;">
<div style="float:left;font-weight:bold;"><input type="text" style="width:500px;height:36px;" name="empname" value="<?php echo $empname; ?>" /></div>
</div>
</div>
<div style="clear:both;";></div>
<div style="width:100%;">
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#777777;float:left;">
<div style="width:50%;float:left;color:white;margin-top:10px;font-weight:bold;">Employee Id</div>
</div>
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#FFFFFF;float:right;">
<div style="float:left;font-weight:bold;"><input type="text" style="width:500px;height:36px;" name="empid" value="" /></div>
</div>
</div>
<div style="clear:both;";></div>
<div style="width:100%;">
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#777777;float:left;">
<div style="width:50%;float:left;color:white;margin-top:10px;font-weight:bold;">Employee Portal ID</div>
</div>
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#FFFFFF;float:right;">
<div style="float:left;font-weight:bold;"><input type="text" style="width:500px;height:36px;" name="portalid" value="<?php echo $username; ?>" /></div>
</div>
</div>
<div style="clear:both;";></div>
<div style="width:100%;">
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#777777;float:left;">
<div style="width:50%;float:left;color:white;margin-top:10px;font-weight:bold;">Employee Location</div>
</div>
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#FFFFFF;float:right;">
<div style="float:left;font-weight:bold;"><input type="text" style="width:500px;height:36px;" name="ulocation" value="<?php echo $ulocation; ?>" /></div>
</div>
</div>
<div style="clear:both;";></div>
<div style="width:100%;">
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#777777;float:left;">
<div style="width:50%;float:left;color:white;margin-top:10px;font-weight:bold;">Project</div>
</div>
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#FFFFFF;float:right;">
<div style="float:left;font-weight:bold;"><input type="text" style="width:500px;height:36px;" name="project" value="" /></div>
</div>
</div>
<div style="clear:both;";></div>
<div style="width:100%;">
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#777777;float:left;">
<div style="width:50%;float:left;color:white;margin-top:10px;font-weight:bold;">Account</div>
</div>
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#FFFFFF;float:right;">
<div style="float:left;font-weight:bold;"><input type="text" style="width:500px;height:36px;" name="account" value="" /></div>
</div>
</div>
<div style="clear:both;";></div>
<div style="width:100%;">
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#777777;float:left;">
<div style="width:50%;float:left;color:white;margin-top:10px;font-weight:bold;">Vertical</div>
</div>
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#FFFFFF;float:right;">
<div style="float:left;font-weight:bold;"><input type="text" style="width:500px;height:36px;" name="vertical" value="<?php echo $vertical;?>" /></div>
</div>
</div>
<div style="clear:both;";></div>
<div style="width:100%;height:4%;border-top:1px solid white;background-color:#548dd4;">
<div style="width:80%;float:left;color:white;margin-top:10px;font-weight:bold;">Please provide the feedback below</div>
<div style="width:20%;float:right;color:white;"></div>
</div>
<div style="border:1px solid gray;border-radius:5px;">
<div style="clear:both;"></div>
<div style="width:100%;height:4%;border-top:1px solid white;background-color:#548dd4;">
<div style="width:50%;float:left;color:white;margin-top:10px;font-weight:bold;">Question</div>
<div style="width:20%;float:left;color:white;margin-top:10px;font-weight:bold;">Rating</div>
<div style="width:30%;float:left;color:white;margin-top:10px;font-weight:bold;float:right;">Comments</div>
</div>
<div style="clear:both;"></div>
<div style="width:100%;height:4%;border-top:1px solid white;">
<div style="width:50%;float:left;margin-top:10px;font-weight:bold;">Are you clear with the objectives of the Training Program?</div>
<div style="width:20%;float:left;margin-top:10px;font-weight:bold;">
<select name="q1" required>
<option value="">Choose item</option>
<option value="1">Yes</option>
<option value="2">No</option>
</select>
</div>
<div style="width:30%;float:left;margin-top:10px;font-weight:bold;float:right;"><textarea required name="q1comments" rows="4" cols="30"></textarea></div>
</div>
<div style="clear:both;"></div>
<div style="width:100%;height:4%;border-top:1px solid white;">
<div style="width:50%;float:left;margin-top:10px;font-weight:bold;">Choose your current Skill level</div>
<div style="width:20%;float:left;margin-top:10px;font-weight:bold;">
<select name="q2" required>
<option value="">Choose item</option>
<option value="0">Poor</option>
<option value="1">Average</option>
<option value="2">Good</option>
<option value="3">Excellent</option>
</select>
</div>
<div style="width:30%;float:left;margin-top:10px;font-weight:bold;float:right;"><textarea required name="q2comments" rows="4" cols="30"></textarea></div>
</div>
<div style="clear:both;"></div>
<div style="width:100%;height:4%;border-top:1px solid white;">
<div style="width:50%;float:left;margin-top:10px;font-weight:bold;">List your expectations from the Training Program</div>
<div style="width:50%;float:left;margin-top:10px;font-weight:bold;float:right;"><textarea required name="q3comments" rows="8" cols="55"></textarea></div>
</div>
<div style="clear:both;"></div>
<div style="width:100%;height:4%;border-top:1px solid white;">
<div style="width:50%;float:left;margin-top:10px;font-weight:bold;"></div>
<div style="width:50%;float:left;margin-top:10px;font-weight:bold;float:right;"><input type="submit" name="submit"  value="SUBMIT"/>&nbsp;<input type="reset" name="reset" value="RESET"/></div>
</div>
<div style="clear:both;"></div>
</div>
</form>


 </div>
 </body>
 </html>
<?php

    print_footer('home');     // Please do not modify this line

	
?>	
    


