<?php 
require_once('../config.php');
$sessionid=$_POST['session'];

//$sessionid='41432';


$query=get_records_sql("select * from mdl_classroom_sessions where id='$sessionid'");

foreach($query as $key=>$value)
{
$programname=$value->programename;
$skillimprovearea=$value->sessioncategory;
$requestor=$value->requestor;
if($requestor!='')
{
$reqname=get_record_sql("select firstname,lastname from mdl_user where username='$requestor'");
$requestorname=$reqname->firstname.' '.$reqname->lastname;
}
else
{
$requestorname='';
}
$tlocation=$value->location;
$datess=get_record_sql("SELECT timestart,timefinish FROM mdl_classroom_sessions_dates m where sessionid='$sessionid'");
$allsessiondates = userdate($datess->timestart, get_string('strftimedate'));
$timestart= $allsessiondates.' '.userdate($datess->timestart, get_string('strftimetime'));
$timefinish= $allsessiondates.' '.userdate($datess->timefinish, get_string('strftimetime'));
$duration=$value->duration." Minutes";
}
$tname=get_record_sql("SELECT userid FROM mdl_classroom_trainners where sessionid='$sessionid'");
$tuserid=$tname->userid;
if($tuserid!='')
{
$tname=get_record_sql("select firstname,lastname from mdl_user where id='$tuserid'");
$trainername=$tname->firstname.' '.$tname->lastname;
}
else
{
$trainername='';
}


$html = '<html><body><div style="width:100%;font-family:arial;"> <div style="width:100%;height:5%;background-color:#548dd4;border-bottom:1px solid white;">
<div style="width:80%;float:left;text-align:center;color:white;margin-top:10px;font-weight:bold;">Training Effectiveness</div>
<div style="width:20%;float:right;color:white;margin-top:10px;font-weight:bold;">PR – 059E<br>Version 1.00</div>
</div>
<div style="clear:both;"></div>
<div style="width:100%;height:4%;border-top:1px solid white;background-color:#548dd4;">
<div style="width:80%;float:left;color:white;margin-top:10px;font-weight:bold;">Training Details</div>
<div style="width:20%;float:right;color:white;"></div>
</div>
<div style="clear:both;"></div>
<div style="width:100%;">
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#777777;float:left;">
<div style="width:50%;float:left;color:white;margin-top:10px;font-weight:bold;">Training Program Name</div>
</div>
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#FFFFFF;float:right;">
<div style="float:left;border-top:1px solid gray;">'.$programname.'</div>
</div>
</div>
<div style="clear:both;";></div>
<div style="width:100%;">
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#777777;float:left;">
<div style="width:50%;float:left;color:white;margin-top:10px;font-weight:bold;">Skill Improvement Area</div>
</div>
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#FFFFFF;float:right;">
<div style="float:left;border-top:1px solid gray;">'.$skillimprovearea.' </div>
</div>
</div>
<div style="clear:both;";></div>
<div style="width:100%;">
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#777777;float:left;">
<div style="width:50%;float:left;color:white;margin-top:10px;font-weight:bold;">Requestor / Nominator Name</div>
</div>
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#FFFFFF;float:right;">
<div style="float:left;border-top:1px solid gray;">'.$requestorname.'</div>
</div>
</div>
<div style="clear:both;";></div>
<div style="width:100%;">
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#777777;float:left;">
<div style="width:50%;float:left;color:white;margin-top:10px;font-weight:bold;">Training Location</div>
</div>
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#FFFFFF;float:right;">
<div style="float:left;border-top:1px solid gray;">'.$tlocation.'</div>
</div>
</div>
<div style="clear:both;";></div>
<div style="width:100%;">
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#777777;float:left;">
<div style="width:50%;float:left;color:white;margin-top:10px;font-weight:bold;">Training Start Date</div>
</div>
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#FFFFFF;float:right;">
<div style="float:left;border-top:1px solid gray;">'.$timestart.' </div>
</div>
</div>
<div style="clear:both;";></div>
<div style="width:100%;">
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#777777;float:left;">
<div style="width:50%;float:left;color:white;margin-top:10px;font-weight:bold;">Training End Date</div>
</div>
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#FFFFFF;float:right;">
<div style="float:left;border-top:1px solid gray;">'.$timefinish.'</div>
</div>
</div>
<div style="clear:both;";></div>
<div style="width:100%;">
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#777777;float:left;">
<div style="width:50%;float:left;color:white;margin-top:10px;font-weight:bold;">Duration</div>
</div>
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#FFFFFF;float:right;">
<div style="float:left;border-top:1px solid gray;">'.$duration.'</div>
</div>
</div>
<div style="clear:both;";></div>
<div style="width:100%;">
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#777777;float:left;">
<div style="width:50%;float:left;color:white;margin-top:10px;font-weight:bold;">Trainer Name</div>
</div>
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#FFFFFF;float:right;">
<div style="float:left;border-top:1px solid gray;">'.$trainername.'</div>
</div>
</div>
';
$html.= '<div style="clear:both;";></div>
<div style="width:100%;height:4%;border-top:1px solid white;background-color:#548dd4;">
<div style="width:80%;float:left;color:white;margin-top:10px;font-weight:bold;">Please find the feedback details for form A below</div>
<div style="width:20%;float:right;color:white;"></div>
</div>
<div style="border:1px solid gray;border-radius:5px;">
<table border="1" cellpadding="5" cellspacing="5">
<tr>
<td>PortalID</td>
<td>User Name</td>
<td>Are you clear with the objectives of the Training Program?</td>
<td>Choose your current Skill level</td>
<td>List your expectations from the Training Program</td>
</tr>
';

$recquery=get_records_sql("select distinct(userid) from mdl_feedback_ext_employee_response where seesionid='$sessionid'");
$county=count_records_sql("select distinct(userid) from mdl_feedback_ext_employee_response where seesionid='$sessionid'");
if($county>0)
{
foreach($recquery as $rkey=>$rvalue)
{
$userdetails=get_record_sql("select username as portalid,concat(firstname,' ',lastname) as username from mdl_user where id='$rvalue->userid'");
$qquery=get_records_sql("select * from mdl_feedback_ext_employee_response where userid='$rvalue->userid' and question_id in('1','2','3') and seesionid='$sessionid' order by id");
$html.="<tr>";
$html.="<td>$userdetails->portalid</td>";
$html.="<td>$userdetails->username</td>";
foreach($qquery as $qkey=>$qvalue)
{
$itemvalue=get_record_sql("select itemname from mdl_feedback_ext_items where question_id='$qvalue->question_id' and itemvalue='$qvalue->response'");
$html.="<td>Answer: $itemvalue->itemname <br> Comments: $qvalue->comments</td>";
}
$html.="</tr>";
}
}
else
{
$html.="<tr><td colspan='6'>No user provided feed back for this session</td></tr>";
}
$html.='</table>
</div></div>';

$html.= '<div style="clear:both;";></div>
<div style="width:100%;height:4%;border-top:1px solid white;background-color:#548dd4;">
<div style="width:80%;float:left;color:white;margin-top:10px;font-weight:bold;">Please find the feedback details for form B below</div>
<div style="width:20%;float:right;color:white;"></div>
</div>
<div style="border:1px solid gray;border-radius:5px;">
<table border="1" cellpadding="5" cellspacing="5">
<tr>
<td>PortalID</td>
<td>User Name</td>
<td>Do you use this skill on the job?</td>
<td>Have you applied the Skills learnt on the job?</td>
<td>How did the training program contribute in enhancing your skill?</td>
<td>How soon were you able to apply the skill learnt on the job?</td>
</tr>
';

$recquery=get_records_sql("select distinct(userid) from mdl_feedback_ext_employee_response where seesionid='$sessionid'");
$county=count_records_sql("select distinct(userid) from mdl_feedback_ext_employee_response where seesionid='$sessionid'");
if($county>0)
{
foreach($recquery as $rkey=>$rvalue)
{
$userdetails=get_record_sql("select username as portalid,concat(firstname,' ',lastname) as username from mdl_user where id='$rvalue->userid'");
$qquery=get_records_sql("select * from mdl_feedback_ext_employee_response where userid='$rvalue->userid' and question_id in('4','5','6','7') and seesionid='$sessionid' order by id");
$html.="<tr>";
$html.="<td>$userdetails->portalid</td>";
$html.="<td>$userdetails->username</td>";
foreach($qquery as $qkey=>$qvalue)
{
$itemvalue=get_record_sql("select itemname from mdl_feedback_ext_items where question_id='$qvalue->question_id' and itemvalue='$qvalue->response'");
$html.="<td>Answer: $itemvalue->itemname <br> Comments: $qvalue->comments</td>";
}
$html.="</tr>";
}
}
else
{
$html.="<tr><td colspan='6'>No user provided feed back for this session</td></tr>";
}
$html.='</table>
</div></div></body></html>';

include("../mpdf/mpdf/mpdf.php");

$mpdf=new mPDF('c'); 

$mpdf->WriteHTML($html);
$mpdf->Output('form_A_B_report.pdf','D');
//$mpdf->Output();
exit;


?>