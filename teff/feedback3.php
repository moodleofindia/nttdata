<TITLE>Feedback for ILT Session</TITLE>
<?php 
    require_once('../config.php');
    require_once($CFG->dirroot .'../course/lib.php');
    require_once($CFG->dirroot .'../lib/blocklib.php');

    if (empty($SITE)) {
        redirect($CFG->wwwroot .'/'. $CFG->admin .'/index.php');
    }

	// Bounds for block widths
    // more flexible for theme designers taken from theme config.php
    $lmin = (empty($THEME->block_l_min_width)) ? 100 : $THEME->block_l_min_width;
    $lmax = (empty($THEME->block_l_max_width)) ? 210 : $THEME->block_l_max_width;
    $rmin = (empty($THEME->block_r_min_width)) ? 100 : $THEME->block_r_min_width;
    $rmax = (empty($THEME->block_r_max_width)) ? 210 : $THEME->block_r_max_width;

    define('BLOCK_L_MIN_WIDTH', $lmin);
    define('BLOCK_L_MAX_WIDTH', $lmax);
    define('BLOCK_R_MIN_WIDTH', $rmin);
    define('BLOCK_R_MAX_WIDTH', $rmax);

    // check if major upgrade needed - also present in login/index.php
    if ((int)$CFG->version < 2006101100) { //1.7 or older
        @require_logout();
        redirect("$CFG->wwwroot/$CFG->admin/");
    }
    // Trigger 1.9 accesslib upgrade?
    if ((int)$CFG->version < 2007092000 
        && isset($USER->id) 
        && is_siteadmin($USER->id)) { // this test is expensive, but is only triggered during the upgrade
        redirect("$CFG->wwwroot/$CFG->admin/");
    }

    if ($CFG->forcelogin) {
        require_login();
    } else {
        user_accesstime_log();
    }

    if ($CFG->rolesactive) { // if already using roles system
        if (has_capability('moodle/site:config', get_context_instance(CONTEXT_SYSTEM))) {
            if (moodle_needs_upgrading()) {
                redirect($CFG->wwwroot .'/'. $CFG->admin .'/index.php');
            }
        } else if (!empty($CFG->mymoodleredirect)) {    // Redirect logged-in users to My Moodle overview if required
            if (isloggedin() && $USER->username != 'guest') {
                redirect($CFG->wwwroot .'/my/index.php');
            }
        }
    } else { // if upgrading from 1.6 or below
        if (isadmin() && moodle_needs_upgrading()) {
            redirect($CFG->wwwroot .'/'. $CFG->admin .'/index.php');
        }
    }


    if (get_moodle_cookie() == '') {
        set_moodle_cookie('nobody');   // To help search for cookies on login page
    }

    if (!empty($USER->id)) {
        add_to_log(SITEID, 'course', 'view', 'view.php?id='.SITEID, SITEID);
    }

    if (empty($CFG->langmenu)) {
        $langmenu = '';
    } else {
        $currlang = current_language();
        $langs = get_list_of_languages();
        $langlabel = get_accesshide(get_string('language'));
        $langmenu = popup_form($CFG->wwwroot .'/index.php?lang=', $langs, 'chooselang', $currlang, '', '', '', true, 'self', $langlabel);
    }

    $PAGE       = page_create_object(PAGE_COURSE_VIEW, SITEID);
    $pageblocks = blocks_setup($PAGE);
    $editing    = $PAGE->user_is_editing();
    $preferred_width_left  = bounded_number(BLOCK_L_MIN_WIDTH, blocks_preferred_width($pageblocks[BLOCK_POS_LEFT]),
                                            BLOCK_L_MAX_WIDTH);
    $preferred_width_right = bounded_number(BLOCK_R_MIN_WIDTH, blocks_preferred_width($pageblocks[BLOCK_POS_RIGHT]),
                                            BLOCK_R_MAX_WIDTH);
	print_header($SITE->fullname, $SITE->fullname, 'home', '',
                 '<meta name="description" content="'. strip_tags(format_text($SITE->summary, FORMAT_HTML)) .'" />',
                 true, '', user_login_string($SITE).$langmenu);


$sessionid=$_REQUEST['s'];
//$sessionid='15';				 
$requestorid=get_record_sql("select requestor from mdl_classroom_sessions where id='$sessionid'");
$requestid=$requestorid->requestor;

if(isset($_POST['submit']))
{
$users=$_POST['userids'];
$q8rsps=$_POST['q8'];
$q9rsps=$_POST['q9'];
$q10rsps=$_POST['q10'];
$q11rsps=$_POST['q11'];
$q12rsps=$_POST['q12'];
$q13rsps=$_POST['q13'];

$string="";
foreach($users as $key=>$values)
{
$userid=$values;

for($i=8;$i<13;$i++)
{
$qrs=$_POST["q$i"][$key];
$comments=$q13rsps[$key];
$date=strtotime("now");
$string.="('$userid','$requestid','$i','$qrs','$date','$date','$comments','$sessionid'),";

}

}
$subsrt=substr($string,0,-1);
$questquery="insert into mdl_feedback_ext_manager_response(userid, resource_id, question_id, response, datecreated, datemodified, comments,seesionid) values$subsrt";

if(!execute_sql($questquery,false))
{
echo mysql_error();
}
else
{
echo "<div align='center' style='color:red;font-size:12px;'>Feed back submitted successfully</div>";
}
}


$query=get_records_sql("select * from mdl_classroom_sessions where id='$sessionid'");

foreach($query as $key=>$value)
{
$programname=$value->programename;
$skillimprovearea=$value->sessioncategory;
$requestor=$value->requestor;
if($requestor!='')
{
$reqname=get_record_sql("select firstname,lastname from mdl_user where username='$requestor'");
$requestorname=$reqname->firstname.' '.$reqname->lastname;
}
else
{
$requestorname='';
}
$tlocation=$value->location;
$datess=get_record_sql("SELECT timestart,timefinish FROM mdl_classroom_sessions_dates m where sessionid='$sessionid'");
$allsessiondates = userdate($datess->timestart, get_string('strftimedate'));
$timestart= $allsessiondates.' '.userdate($datess->timestart, get_string('strftimetime'));
$timefinish= $allsessiondates.' '.userdate($datess->timefinish, get_string('strftimetime'));
$duration=$value->duration." Minutes";
}
$tname=get_record_sql("SELECT userid FROM mdl_classroom_trainners where sessionid='$sessionid'");
$tuserid=$tname->userid;
if($tuserid!='')
{
$tname=get_record_sql("select firstname,lastname from mdl_user where id='$tuserid'");
$trainername=$tname->firstname.' '.$tname->lastname;
}
else
{
$trainername='';
}


?>
<!DOCTYPE HTML> 
<html>
<head>
</head>
<body>
<div style="width:100%;font-family:arial;"> 


<form method="POST" action="" name="feedform1" onsubmit="return ValidateForm()";>
<input type="hidden" name="sessionid" value="<?php echo $sessionid;  ?>">
<div style="width:100%;height:5%;background-color:#548dd4;border-bottom:1px solid white;">
<div style="width:80%;float:left;text-align:center;color:white;margin-top:10px;font-weight:bold;">Training Effectiveness</div>
<div style="width:20%;float:right;color:white;margin-top:10px;font-weight:bold;">PR – 059E<br>Version 1.00</div>
</div>
<div style="clear:both;"></div>
<div style="width:100%;height:4%;border-top:1px solid white;background-color:#548dd4;">
<div style="width:80%;float:left;color:white;margin-top:10px;font-weight:bold;">Training Details</div>
<div style="width:20%;float:right;color:white;"></div>
</div>
<div style="clear:both;"></div>
<div style="width:100%;">
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#777777;float:left;">
<div style="width:50%;float:left;color:white;margin-top:10px;font-weight:bold;">Training Program Name</div>
</div>
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#FFFFFF;float:right;">
<div style="float:left;color:white;"><input type="text" style="width:500px;height:36px;" name="prgname"value="<?php echo $programname; ?>" /></div>
</div>
</div>
<div style="clear:both;";></div>
<div style="width:100%;">
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#777777;float:left;">
<div style="width:50%;float:left;color:white;margin-top:10px;font-weight:bold;">Skill Improvement Area</div>
</div>
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#FFFFFF;float:right;">
<div style="float:left;font-weight:bold;"><input type="text" style="width:500px;height:36px;" name="skillname"value="<?php echo $skillimprovearea; ?>" /></div>
</div>
</div>
<div style="clear:both;";></div>
<div style="width:100%;">
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#777777;float:left;">
<div style="width:50%;float:left;color:white;margin-top:10px;font-weight:bold;">Requestor / Nominator Name</div>
</div>
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#FFFFFF;float:right;">
<div style="float:left;font-weight:bold;"><input type="text" style="width:500px;height:36px;" name="reqname"value="<?php echo $requestorname; ?>" /></div>
</div>
</div>
<div style="clear:both;";></div>
<div style="width:100%;">
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#777777;float:left;">
<div style="width:50%;float:left;color:white;margin-top:10px;font-weight:bold;">Training Location</div>
</div>
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#FFFFFF;float:right;">
<div style="float:left;font-weight:bold;"><input type="text" style="width:500px;height:36px;" name="tlocation"value="<?php echo $tlocation; ?>" /></div>
</div>
</div>
<div style="clear:both;";></div>
<div style="width:100%;">
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#777777;float:left;">
<div style="width:50%;float:left;color:white;margin-top:10px;font-weight:bold;">Training Start Date</div>
</div>
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#FFFFFF;float:right;">
<div style="float:left;font-weight:bold;"><input type="text" style="width:500px;height:36px;" name="tstart" value="<?php echo $timestart; ?>" /></div>
</div>
</div>
<div style="clear:both;";></div>
<div style="width:100%;">
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#777777;float:left;">
<div style="width:50%;float:left;color:white;margin-top:10px;font-weight:bold;">Training End Date</div>
</div>
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#FFFFFF;float:right;">
<div style="float:left;font-weight:bold;"><input type="text" style="width:500px;height:36px;" name="tfinish" value="<?php echo $timefinish; ?>" /></div>
</div>
</div>
<div style="clear:both;";></div>
<div style="width:100%;">
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#777777;float:left;">
<div style="width:50%;float:left;color:white;margin-top:10px;font-weight:bold;">Duration</div>
</div>
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#FFFFFF;float:right;">
<div style="float:left;font-weight:bold;"><input type="text" style="width:500px;height:36px;" name="duration" value="<?php echo $duration; ?>" /></div>
</div>
</div>
<div style="clear:both;";></div>
<div style="width:100%;">
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#777777;float:left;">
<div style="width:50%;float:left;color:white;margin-top:10px;font-weight:bold;">Trainer Name</div>
</div>
<div style="width:50%;height:4%;border-top:1px solid white;background-color:#FFFFFF;float:right;">
<div style="float:left;font-weight:bold;"><input type="text" style="width:500px;height:36px;" name="duration" value="<?php echo $trainername; ?>" /></div>
</div>
</div>
<div style="clear:both;";></div>
<div style="width:100%;height:4%;border-top:1px solid white;background-color:#548dd4;">
<div style="width:80%;float:left;color:white;margin-top:10px;font-weight:bold;">Please provide the feedback below</div>
<div style="width:20%;float:right;color:white;"></div>
</div>
<div style="border:1px solid gray;border-radius:5px;">
<div style="clear:both;"></div>
<div style="width:100%;height:10%;border-top:1px solid white;background-color:#548dd4;">
<div style="width:10%;float:left;color:white;margin-top:10px;font-weight:bold;margin-left:10px;">Attendee Name</div>
<div style="width:15%;float:left;color:white;margin-top:10px;font-weight:bold;margin-left:10px;">How often is this skill used in the project?</div>
<div style="width:15%;float:left;color:white;margin-top:10px;font-weight:bold;margin-left:10px;">Are the employees able to apply the skill learnt effectively?</div>
<div style="width:15%;float:left;color:white;margin-top:10px;font-weight:bold;margin-left:10px;">What impact did training program have on the productivity or the defect injected in the project?</div>
<div style="width:10%;float:left;color:white;margin-top:10px;font-weight:bold;margin-left:10px;">Rate the Overall effectiveness of the program</div>
<div style="width:15%;float:left;color:white;margin-top:10px;font-weight:bold;margin-left:10px;">Provide suggestions to improve effectiveness of training</div>
<div style="width:10%;float:left;color:white;margin-top:10px;font-weight:bold;margin-left:10px;">Comments</div>
</div>
<?php 
$query=get_records_sql("select u.id,u.firstname,u.lastname from mdl_classroom_submissions s join mdl_user u on u.id=s.userid where sessionid='$sessionid'");
//echo "select u.firstname,u.lastname from mdl_classroom_submissions s join mdl_user u on u.id=s.userid where sessionid='43846'";exit;

foreach($query as $key=>$value)
{

$userfirstname=$value->firstname;
$userlastname=$value->lastname;
$fullname=$userfirstname." ".$userlastname;
$userid=$value->id;
?>

<div style="width:100%;height:auto;border-top:1px solid white;margin-left:10px;">
<div style="width:10%;float:left;margin-top:10px;font-weight:bold;">
<?php echo $fullname; ?>
<input type="hidden" name="userids[]" value="<?php echo $userid;  ?>">
</div>
<div style="width:15%;float:left;margin-top:10px;font-weight:bold;margin-left:10px;">
<select name="q8[]" required>
<option value="">Choose item</option>
<option value="1">Never</option>
<option value="2">Few times</option>
<option value="3">Often</option>
<option value="4">Core Skill Required</option>
</select>
</div>
<div style="width:15%;float:left;color:white;margin-top:10px;font-weight:bold;margin-left:10px;">
<select name="q9[]" required>
<option value="">Choose item</option>
<option value="1">Yes</option>
<option value="2">No</option>
</select>
</div>
<div style="width:15%;float:left;color:white;margin-top:10px;font-weight:bold;margin-left:10px;">
<textarea name="q10[]" required rows="4" cols="20"></textarea>
</div>
<div style="width:10%;float:left;color:white;margin-top:10px;font-weight:bold;margin-left:10px;">
<select name="q11[]" required>
<option value="">Choose item</option>
<option value="1">Poor</option>
<option value="2">Satisfactory</option>
<option value="3">Good</option>
<option value="4">Excellent</option>
</select>
</div>
<div style="width:15%;float:left;color:white;margin-top:10px;font-weight:bold;margin-left:10px;">
<textarea name="q12[]" required rows="4" cols="20"></textarea>
</div>
<div style="width:10%;float:left;color:white;margin-top:10px;font-weight:bold;margin-left:10px;">
<textarea name="q13[]" required rows="4" cols="20"></textarea>
</div>
</div>
<div style="clear:both;"></div>
<?php 

}
?>

<div style="width:100%;height:4%;border-top:1px solid white;">
<div style="width:50%;float:left;margin-top:10px;font-weight:bold;"></div>
<div style="width:50%;float:left;margin-top:10px;font-weight:bold;float:right;"><input type="submit" name="submit" value="SUBMIT"/>&nbsp;<input type="reset" name="reset" value="RESET"/></div>
</div>
<div style="clear:both;"></div>
</div>
</form>


 </div>
 </body>
 </html>
<?php

    print_footer('home');     // Please do not modify this line

	
?>	
    


