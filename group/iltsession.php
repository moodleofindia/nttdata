<?php // $Id: members.php,v 1.3.2.8 2008/07/24 11:39:05 skodak Exp $
/**
 * Add/remove members from group.
 *
 * @copyright &copy; 2006 The Open University
 * @author N.D.Freear AT open.ac.uk
 * @author J.White AT open.ac.uk
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 * @package groups
 */
require_once('../config.php');
require_once('lib.php');

define("MAX_USERS_PER_PAGE", 5000);
$courseid=$_POST['courseid'];
$groupid=$_POST['groupid'];
$searchtext=$_POST['searchtext'];

$potentialmembers = array();
$potentialmembersoptions = '';
$potentialmemberscount = 0;

//***********GIVE CONDITIONS HERE***********//

$potentialmembersbyrole = groups_get_users_in_ilt_sessions ($courseid, $groupid, $searchtext);

$potentialmemberscount=0;
$potentialmembersids=array();
if (!empty($potentialmembersbyrole)) {
    foreach($potentialmembersbyrole as $roledata) {
        $potentialmemberscount+=count($roledata->users);
        $potentialmembersids=array_merge($potentialmembersids,array_keys($roledata->users));
    }
}


$potentialmembersbyrole_sessions=get_records_sql("SELECT distinct(cs.id) as sessionid,date(from_unixtime(sd.timefinish)) as completeddate,cs.classroom FROM mdl_classroom_sessions cs
												join mdl_classroom c on c.id=cs.classroom
												join mdl_classroom_sessions_dates sd on sd.sessionid=cs.id
												where cs.status='completed' and c.course=$courseid   ORDER BY sd.timefinish desc;");

$usergroups = array();

if ($potentialmemberscount <=  MAX_USERS_PER_PAGE) {

    if ($potentialmemberscount != 0) {
        // Get other groups user already belongs to
        $sql = "SELECT u.id AS userid, g.* FROM {$CFG->prefix}user u " .
                    "INNER JOIN {$CFG->prefix}groups_members gm ON u.id = gm.userid " .
                    "INNER JOIN {$CFG->prefix}groups g ON gm.groupid = g.id " .
               "WHERE u.id IN (".implode(',',$potentialmembersids).") AND g.courseid = {$course->id} ";
        $rs = get_recordset_sql($sql);
        while ($usergroup =  rs_fetch_next_record($rs)) {
            $usergroups[$usergroup->userid][$usergroup->id] = $usergroup;
        }
        rs_close($rs);
		foreach ($potentialmembersbyrole_sessions as $sessions)
		{
		$potentialmembersoptions .= '<optgroup label="Classroom Experience('.($sessions->completeddate).')">';
		$potentialmembersbyrole_sessions_list=groups_get_users_in_ilt_sessions_users ($courseid, $groupid,$sessions->sessionid, $searchtext);
        foreach($potentialmembersbyrole_sessions_list as $roleid=>$roledata) {
		
            $potentialmembersoptions .= '<optgroup label="'.htmlspecialchars($roledata->name).'">';
			//$potentialmembersoptions .= '<optgroup label="'.htmlspecialchars($roleid->completeddate).'">';
            foreach($roledata->users as $member) {
			    $name=htmlspecialchars(fullname($member, true));
				if($member->attend=='1'){
				$attend="C";
				}elseif($member->attend=='0'){
				$attend="NC";
				}else{
				$attend="CA";
				}
				// $potentialmembersoptions .= '<optgroup label="'.$member->completeddate.'">';
                $potentialmembersoptions .= '<option value="'.$member->id.
                    '" title="'.$name.'">'.$name.
                    ' ('.$attend.')</option>';
                $potentialmembers[$member->id] = $member;
            }
            $potentialmembersoptions .= '</optgroup>';
        }
		}
    } else {
        $potentialmembersoptions .= '<option>&nbsp;</option>';
    }
}
if ($potentialmemberscount > MAX_USERS_PER_PAGE) {
                $Stss='<optgroup label="'.get_string('toomanytoshow').'"><option></option></optgroup>'."\n"
                        .'<optgroup label="'.get_string('trysearching').'"><option></option></optgroup>'."\n";
            } else {
                $potentialmembersoptions=$Stss.$potentialmembersoptions;
            }

$values=array();
$values['usercount']="Potential members:".$potentialmemberscount;
$values['stringoption']=$potentialmembersoptions;
echo json_encode($values);			
// Print Javascript for showing the selected users group membership
?>

